#!/bin/bash

[ -z "$TRACKWARE_ENV_FILE" ] && echo "Need to set TRACKWARE_ENV_FILE env before run this." && exit 1;


source $TRACKWARE_ENV_FILE

export CATALYST_DEBUG=0;
export TRACKWARE_API_WORKERS=7
export TRACKWARE_WEB_WORKERS=5

mkdir -p $TRACKWARE_LOG_DIR

line (){
    perl -e "print '-' x 40, $/";
}
# WEB

WEB_PID=`ps aux|grep starman|grep ":$TRACKWARE_WEB_PORT" | grep master | grep -v grep | awk '{print $2}'`

if [ "$WEB_PID" -gt 1 ]; then
    echo "kill -HUP $WEB_PID WEB"
    kill -HUP "$WEB_PID"
else
    echo "STARMAN WEB"
    cd $TRACKWARE_APP_DIR/web
    starman -MCatalyst -MDBIx::Class -l :$TRACKWARE_WEB_PORT  --workers $TRACKWARE_WEB_WORKERS --error-log $TRACKWARE_LOG_DIR/web.error.log --daemonize webtrackware.psgi
fi

line

API_PID=`ps aux|grep starman|grep ":$TRACKWARE_API_PORT" | grep master | grep -v grep | awk '{print $2}'`

if [ "$API_PID" -gt 1 ]; then
    echo "kill -HUP $API_PID API"
    kill -HUP "$API_PID"
else
    echo "STARMAN API"

    cd $TRACKWARE_APP_DIR/api

    starman -MCatalyst -MDBIx::Class -l 0.0.0.0:$TRACKWARE_API_PORT  --workers $TRACKWARE_API_WORKERS --error-log $TRACKWARE_LOG_DIR/api.error.log --daemonize trackware.psgi
fi

echo "Sleeping for 60seconds...";
sleep 60;

line

echo "Restarting scripts...";

# WOKRERS

cd $TRACKWARE_APP_DIR/api/script/daemon

./tracker_new_consumer restart
line
./tracker_queue_consumer restart
line