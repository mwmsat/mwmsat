#!/bin/bash

# $ cp envs.sh envs_local.sh
# setup envs_local.sh
# $ TRACKWARE_ENV_FILE=deploy/envs_local.sh deploy/restart_services.sh

export PERLBREW_ROOT=/opt/perlbrew
source ${PERLBREW_ROOT}/etc/bashrc



# diretorios

# diretorio de log dos daemons
export TRACKWARE_LOG_DIR=''

# diretorio root do projeto
export TRACKWARE_APP_DIR=/home/etamu/app

# portas
export TRACKWARE_API_PORT=5000
export TRACKWARE_WEB_PORT=3000

export EC2METADATA_BIN=/usr/bin/ec2metadata
# obs: se ligar os dados de acesso, inclur tambem:
# source envs_b-datum.sh ou
# export AWS_ACCESS_KEY_ID=
# export AWS_ACCESS_KEY_SECRET=
# export AWS_SECRET_ACCESS_KEY=$AWS_ACCESS_KEY_SECRET

