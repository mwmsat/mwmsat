#!/bin/bash

export PERLBREW_ROOT=/home/ubuntu/perl5/perlbrew
source ${PERLBREW_ROOT}/etc/bashrc

fuser 8100/tcp -k
sleep 2

cd /home/ubuntu/trackware/api

starman  -l :8100  --workers 5 --preload-app --error-log /home/ubuntu/trackware/api/api.error.log --daemonize trackware.psgi

