use strict;
use warnings;
use Mojolicious::Lite -signatures;
use JSON qw(encode_json);
use Mojo::Redis2;
use Mojo::JWT;
use feature 'say';
use DDP;
use Scalar::Util qw(refaddr);

use Regexp::Wildcards;

app->config(hypnotoad =>
    {pid_file => "$ENV{HOME}/hypnotoad.pid", listen => ['http://*:3333']});

helper redis => sub { state $r = Mojo::Redis2->new };
helper jwt => sub {
  state $jwt = Mojo::JWT->new(
    secret => 'c86b8e761fcb426b57fe62ae64769f1168761450c046ccb4c53cdb79');
};

get '/' => 'chat';
my %conns = ();

websocket '/live' => sub ($c, $r = $c->redis) {

  $c->on(
    json => sub ($c, $json) {
      my $tx = $conns{refaddr $c->tx} = $c->tx;
      p($json);
      if ($json->{type} eq 'authenticate') {
        my $payload = $json->{payload};
        my $jwt     = $c->jwt->decode($payload->{token})
          or return $tx->finish(1003 => 'Unauthorized');
        p($jwt);
        my $queues = $jwt->{q};
        my @listen = ();
        if ($jwt->{a}) {    #superadmin
          if ( $payload->{current_business_unity}
            && $payload->{current_business_unity} eq 'any')
          {
            push @listen, '0.*';
          }
          else {
            push @listen, $payload->{current_business_unity} . '.*';
          }
        }
        else {
          push @listen, map {"$_.*"} grep {
            $payload->{current_business_unity}
              && ($payload->{current_business_unity} eq $_)
          } @{$queues->{bu} || []};
          push @listen, (map {"*.$_"} @{$queues->{t} || []});

        }
        p(\@listen);
        $r->psubscribe(\@listen);
        my $cb = $r->on(
          pmessage => sub {
            my ($r, $msg, $chan) = @_;
            say $msg;
	    return unless matches($chan, @listen);
            say encode_json(\@listen) . "[$chan] $msg";
            $tx->send($msg);
          }
        );
        $c->on(
          finish => sub {
            warn 'FINISH';
            delete $conns{refaddr $tx};
            $r->punsubscribe(\@listen);
          }
        );
        $c->on(error => sub { warn 'ERROR ' . @_; delete $conns{refaddr $tx}; }
        );
      }
      else {
        delete $conns{refaddr $tx};
        return $tx->finish(1003 => 'Unauthorized');
      }
    }
  );
  $c->inactivity_timeout(0);
};
my $rw = Regexp::Wildcards->new(type => 'unix');

sub matches {
  my ($chan, @listen) = @_;
  scalar grep { $chan =~ $_ }
    map { my $r = $rw->convert($_); qr/^$r$/ } @listen;
}

app->start;

__DATA__

@@ chat.html.ep

<!DOCTYPE html>
<html>
  <head><title>Mojo/Redis Chat Example</title></head>
  <body>
    <form onsubmit="sendChat(this.children[0]); return false"><input></form>
    <div id="log"></div>
    %= javascript begin
      var log = document.getElementById('log');
     var ws  = new WebSocket('<%= url_for('socket')->to_abs %>');
     var msg = {
        type: 'authenticate',
        payload: { token: 'XXX' }
      };
      ws.onopen = function(event) {
        ws.send(JSON.stringify(msg));
      };
      ws.onmessage = function (e) { log.innerHTML = '<p>'+e.data+'</p>' + log.innerHTML };
      function sendChat(input) { ws.send(input.value); input.value = '' }
    % end
  </body>
</html>
