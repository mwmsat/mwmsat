var center = new google.maps.LatLng(-23.563596, -46.653885);
var opts = {
  zoom: 13,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
  zoomControlOptions: {
    position: google.maps.ControlPosition.TOP_RIGHT
  }
};
var map = new google.maps.Map(document.getElementById("form-map"), opts);
map.setCenter(center);
var circle_count = 0;
var _manager = new google.maps.drawing.DrawingManager({
  drawingControl: true,
  drawingControlOptions: {
    drawingModes: [
      google.maps.drawing.OverlayType.CIRCLE
    ]
  },
  drawingMode: google.maps.drawing.OverlayType.CIRCLE,
  circleOptions: {
    editable: true,
    draggable: true
  }
});
_manager.setMap(map);
_manager.setDrawingMode(google.maps.drawing.OverlayType.CIRCLE);
var current_color = '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);

map.controls[google.maps.ControlPosition.TOP_LEFT].push($('<div id="area-address-wrapper"><input data-lpignore=true autocomplete="off" id="area-address-input" class="blur" type=text size=42  /></div>')[0]);

google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
  var input = document.getElementById('area-address-input');
  var autocomplete = new google.maps.places.Autocomplete(input, {
    componentRestrictions: {
      country: 'br'
    }
  });
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var place = autocomplete.getPlace();
    if (!place.types) return;
    if (place.geometry.location) map.setCenter(place.geometry.location);
  });
  $('#area-address-input').keypress(function(e) {
    var c = e.which ? e.which : e.keyCode;
    if (c == 13) {
      e.preventDefault();
      return true;
    }
    return true;
  });
  // $('#area-address-wrapper input').focus(function() {
  //   if (this.value == this.defaultValue) {
  //     $(this).toggleClass("blur");
  //     this.value = '';
  //   }
  //   if (this.value != this.defaultValue) {
  //     this.select();
  //   }
  // });
  // $('#area-address-input input').blur(function() {
  //   if ($(this).val() == '') {
  //     $(this).val(this.defaultValue ? this.defaultValue : '');
  //     $(this).toggleClass("blur");
  //   }
  // });
});


function _polygonAsString(polygon) {
  return 'Centro: %centro%, raio: %raio%m'
    .replace('%centro%', polygon.getCenter().lat().toFixed(5) + ', ' + polygon.getCenter().lng().toFixed(5))
    .replace('%raio%', parseInt(polygon.getRadius()));
}

function updatePoint(polygon) {
  return function() {
    polygon.__center_input.attr('value', polygon.getCenter().toUrlValue());
    polygon.__list_item.find('.content').html(_polygonAsString(polygon));
  };
}
google.maps.event.addListener(_manager, 'circlecomplete', function(polygon) {
  polygon.setEditable(true);
  polygon.setOptions({
    fillColor: current_color
  });

  var list_item = $('<li><div class="square"></div><div class="content">%content%</div></li>'.replace('%content%', _polygonAsString(polygon)));
  polygon.__list_item = list_item;
  list_item.find('.square').css({
    'background-color': current_color
  });
  $('form .area-list ul').append(list_item);

  var radius_input = $('<input>').attr({
    type: 'hidden',
    name: 'radius',
    value: parseInt(polygon.getRadius())
  });
  polygon.__radius_input = radius_input;
  radius_input.appendTo('form .area-list .inputs');

  var center_input = $('<input>').attr({
    type: 'hidden',
    name: 'center',
    value: polygon.getCenter().toUrlValue()
  });
  polygon.__center_input = center_input;
  center_input.appendTo('form .area-list .inputs');

  circle_count += 1;
  if (circle_count == 10) _manager.setDrawingMode(null);


  google.maps.event.addListener(polygon, 'center_changed', updatePoint(polygon));

  google.maps.event.addListener(polygon, 'radius_changed', updatePoint(polygon));

  var _polyRemoveListener = google.maps.event.addListener(polygon, 'dblclick', function(evt) {
    evt.stop();
    polygon.__list_item.remove();
    polygon.__radius_input.remove();
    polygon.__center_input.remove();
    polygon.setMap(null);
    circle_count -= 1;
  });
  current_color = '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);
});
