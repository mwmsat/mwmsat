var $utils = function () {
	
	function searchVehicleFleet(customer_id) {
		if (!customer_id) {
			return false;
		}
		
		$.ajax({
			url: "/search_vehicle_fleet/"+customer_id,
			dataType: 'html',
			success: function (result) {
				$("#elm_vehicle_fleet_id").html(result);
			},
			error: function (err) {
				alert('Não foi possível realizar a pesquisa, tente novamente mais tarde.');
			},
			complete: function () {
				var $aux = $('#vehicle_fleet_aux');
				if ($aux.length && $aux.val()) {
					$('#elm_vehicle_fleet_id').val($aux.val());
				}
				
				$("#elm_vehicle_fleet_id").on('change', function () {
					$("#vehicle_fleet_aux").val($(this).val());
				});
			}
		});
	}
	
	function searchVehicleByPlate(car_plate, id, type) {
		if (!car_plate) {
			return false;
		}
		
		if (car_plate.length != 7) {
			return false;
		}
		
		car_plate = car_plate.toLowerCase();
		
		$.ajax({
			url: "/search_by_plate/"+car_plate,
			data:{ id: id, type: type },
			dataType: 'html',
			success: function (result) {
				$("#vehicle_id").remove();
				
				$("#vehicles").html(result);
			},
			error: function (err) {
				alert('Não foi possível realizar a pesquisa, tente novamente mais tarde.');
			}
		});
	}
	
	function filterVehicle(brand_id) {
		
		if( !brand_id ) {
			return false;
		}
		
		var $me = $('#elm_vehicle_model_id');
		
		$me.removeClass('required');
		$me.addClass('input-loading');
		
		$.ajax({
			url: "/get_vehicle_models",
			data: {
				vehicle_brand_id: brand_id
			},
			dataType: 'html',
			success: function (result) {
				$("#elm_vehicle_model_id").html(result);
			},
			error: function (err) {
				alert('Não foi possível encontrar os modelos.');
			},
			complete: function () {
				$me.removeClass('input-loading');
				$me.addClass('required');
				
				var $model_aux = $('#vehicle_model_aux');
				if ($model_aux.length && $model_aux.val()) {
					$('#elm_vehicle_model_id').val($model_aux.val());
				}
				
				$("#elm_vehicle_model_id").on('change', function () {
					$("#vehicle_model_aux").val($(this).val());
				});
			}
		});
	}
	
	function getCommandLayouts(tracker_id) {
		var $url;
		console.log(tracker_id)
		
		$.ajax({
			url: "/tracker-command-layout/list/",
			dataType: 'html',
			success: function (result) {
				$('.modal-body').html(result);
			},
			error: function (err) {
				console.log(err);
			},
			complete: function () {
				$('#modalCommandLayouts').modal({show:true})
				
				$('#tracker_id').val(tracker_id);
				
				var $command_form = $('#command_form');
				
				if( $command_form.length ) {
					$command_form.submit( function() {
						$data = $('#command_form').serialize();
						
						$utils.assignTrackerCommand($data);
					});
				}
				
			}
		});
		
	}
	
	function assignTrackerCommand(data) {
		$.ajax({
			url: "/tracker/assign-command/",
			data: { data: data },
			dataType: 'json',
			success: function (result) {
// 				$('.modal-body').html(result);
			},
			error: function (err) {
				console.log(err);
			},
			complete: function () {
// 				$('#modalCommandLayouts').modal({show:true})
// 				
// 				$('#tracker_id').val(tracker_id);
			}
		});
	}

    return {
		filterVehicle: filterVehicle,
		searchVehicleByPlate: searchVehicleByPlate,
		searchVehicleFleet: searchVehicleFleet,
		getCommandLayouts: getCommandLayouts,
		assignTrackerCommand: assignTrackerCommand
    };
}();

$(document).ready(function () {
	
	var $vehicle_brand = $('#elm_vehicle_brand_id');
	if( $vehicle_brand.length ) {
		$utils.filterVehicle($vehicle_brand.val());
		$vehicle_brand.change(function(){
			$utils.filterVehicle($vehicle_brand.val());
		});
	}
    
	var $vehicles 		= $('#vehicles');
	var $customer 		= $('#elm_customer_id');
	var $business_unity = $('#business_unity_id');
	
	
	if( $vehicles.length ) {
		$('#elm_car_plate').keyup(function () {
			var car_plate = $(this).val().replace(/[-|_]/g, '');
			
			if( car_plate.length == 7 ) {
				
				if( $customer.length && $customer.val() ) {
					$utils.searchVehicleByPlate(car_plate, $customer.val(), 'customer');
				} else if( $business_unity.length && $business_unity.val()) {
					$utils.searchVehicleByPlate(car_plate, $business_unity.val(), 'business_unity');
				} else {
					$utils.searchVehicleByPlate(car_plate);
				}
				
			}
		});
	}
	
	var $customer 		= $('#elm_customer_id');
	var $vehicle_fleets = $('#elm_vehicle_fleet_id');
	
	if( $customer.length && $vehicle_fleets.length ) {
		$utils.searchVehicleFleet( $customer.val() );
		$customer.change(function () {
			$utils.searchVehicleFleet( $customer.val() );
		});
	}
	
	var $real_time_filter = $('#real_time_filter');
	if( $real_time_filter.length ) {
		
		$admin.getRealTimePosition();
		
		$($real_time_filter).submit(function( event ){
			var $data = $real_time_filter.serialize();
			
			$admin.getRealTimePosition($data);
			
			event.preventDefault();
		});
		
	}
	
	var $history_filter = $('#history_filter');
	var $vehicle_id		= $('#vehicle_id');
	if( $history_filter.length ) {
		
		if( $vehicle_id.length ) {
			$admin.getHistoryPositions(0, $vehicle_id.val());
		}
		
		$($history_filter).submit(function( event ){
			
			$('#vehicle_id').attr('disabled', 'disabled');
			
			var $data = $history_filter.serialize();
			$admin.getHistoryPositions($data);
			
			event.preventDefault();
		});
		
	}
	
	var $command = $('.modal_command');
	if( $command.length ) {
		$command.click(function(){
			$utils.getCommandLayouts($(this).attr('data-id'));
		});
		 
	}
	
	var $real_time_filter_sms = $('#real_time_filter_sms');
	if( $real_time_filter_sms.length ) {
		
// 		$admin.getRealTimePositionSms();
		
		$($real_time_filter_sms).submit(function( event ){
			var $data = $real_time_filter_sms.serialize();
			
			$admin.getRealTimePositionSms($data);
			
			event.preventDefault();
		});
		
	}

});