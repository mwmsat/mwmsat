var $admin = function() {

  function getAssociatedLatLnt() {
    $.ajax({
      url: '/admin/associated_routes/get_positions',
      dataType: 'json',
      success: function(result) {
        $maps.buildHeatMap(result);
      },
      error: function(err) {
        console.log(err);
      }
    });
  }

  function getHistoryPositions($data, $vehicle_id, $page) {
    if ($vehicle_id) {
      var $url = '/vehicle_position/get_positions/' + $vehicle_id;
    } else {
      var $url = '/vehicle_position/get_positions';
    }

    $.ajax({
      url: $url,
      data: {
        data: $data,
        page: $page
      },
      dataType: 'json',
      success: function(result) {
        if (result.points.length) {
          // 					Coloca os markers no mapa
          $maps.real_time_position(result.points, 6, 'history');

          // 					Constroi a tabela com informações da posição
          buildPositionTable(result.info);
        } else {
          $maps.real_time_position(0);

          buildPositionTable(0);

          alert('Não foram encontradas posições para esse veículo.');
        }
      },
      error: function(err) {
        console.log(err);
      },
      complete: function(event, xhr) {
        var $next = $('#next');
        var $previous = $('#previous');

        if (xhr == 'success') {
          if (event.responseJSON.previous_page) {
            console.log(event.responseJSON.previous_page);
            $('#previous_button').html('<a href="javascript:;" id="previous" data-page="' + event.responseJSON.previous_page + '">' +
              '<img src="/static/img/previous.png"></img></a>');
          } else if ($previous.length) {
            $previous.remove();
          }

          if (event.responseJSON.next_page) {
            $('#next_button').html('<a href="javascript:;" id="next" data-page="' + event.responseJSON.next_page + '">' +
              '<img src="/static/img/next.png" class="pull-right"></img></a>');
          } else if ($next.length) {
            $next.remove();
          }
        }

        var $page;
        var $data = undefined;

        if ($('#next').length) {
          $next = $('#next');

          $next.off();
          $next.click(function() {
            $page = $(this).attr('data-page');

            if ($('#elm_car_plate').val()) {
              $data = $('#history_filter').serialize();
            }

            $admin.getHistoryPositions($data, $vehicle_id, $page);
          });
        }

        if ($('#previous').length) {
          $previous = $('#previous');

          $previous.off();
          $previous.click(function() {
            $page = $(this).attr('data-page');

            if ($('#elm_car_plate').val()) {
              $data = $('#history_filter').serialize();
            }

            $admin.getHistoryPositions($data, $vehicle_id, $page);
          });
        }
      }
    });
  }

  function getRealTimePosition($data, $page) {
    var $control;

    $.ajax({
      url: '/vehicle_position/get_real_time_position',
      data: {
        data: $data,
        page: $page
      },
      dataType: 'json',
      success: function(result) {
        if (result.points.length) {
          $maps.real_time_position(result.points, 6);
          buildPositionTable(result.info);

          $control = 0;
        } else {
          $maps.real_time_position(0);

          buildPositionTable(0);

          alert('Não foram encontradas posições para esse veículo.');

          $control = 1;
        }
      },
      error: function(err) {
        console.log(err);
      },
      complete: function(event, xhr) {
        var $next = $('#next');
        var $previous = $('#previous');

        if (xhr == 'success') {
          if (event.responseJSON.previous_page) {
            $('#previous_button').html('<a href="javascript:;" id="previous" data-page="' + event.responseJSON.previous_page + '">' +
              '<img src="/static/img/previous.png"></img></a>');
          } else if ($previous.length) {
            $previous.remove();
          }

          if (event.responseJSON.next_page) {
            $('#next_button').html('<a href="javascript:;" id="next" data-page="' + event.responseJSON.next_page + '">' +
              '<img src="/static/img/next.png" class="pull-right"></img></a>');
          } else if ($next.length) {
            $next.remove();
          }
        }

        var $page;

        if ($('#next').length) {
          $next = $('#next');

          $next.off();
          $next.click(function() {
            $page = $(this).attr('data-page');

            var $data = $('#real_time_filter').serialize();

            $admin.getRealTimePosition($data, $page);
          });
        }

        if ($('#previous').length) {
          $previous = $('#previous');

          $previous.off();
          $previous.click(function() {
            $page = $(this).attr('data-page');

            var $data = $('#real_time_filter').serialize();

            $admin.getRealTimePosition($data, $page);
          });
        }

        // 				$("#upload_position_real").button('reset');
        //  				if(!$control) {
        //  					assignReload();
        //  				}
      }
    });

    return $control;
  }

  function buildPositionTable(info, type) {
    var $data = 0;
    if (info) {
      $data = JSON.stringify(info);
    }

    $.ajax({
      url: '/vehicle_position/build_table_info',
      method: 'POST',
      data: {
        info: $data,
        lbs: type == 'lbs' ? 1 : 0
      },
      dataType: 'html',
      success: function(result) {
        $('#table_info').html(result);
      },
      error: function(err) {
        console.log(err);
      },
      complete: function() {
        var $point = $('.point-info');
        if ($point.length) {
          $('.point-info').click(function() {
            $maps.positionZoom($(this).attr('data-lat'), $(this).attr('data-lng'));
          });
        }
      }
    });
  }

  function assignReload() {
    setTimeout(function() {
      $admin.getRealTimePosition();
    }, 180000); //3 minutos
  }


  function getRealTimePositionSms($data) {
    var $control;

    $.ajax({
      url: '/vehicle_position/get_real_time_position_sms',
      data: {
        data: $data
      },
      dataType: 'json',
      success: function(result) {
        if (result.points.length) {
          $maps.real_time_position(result.points, 8, 'lbs');
          console.log(result.info);
          buildPositionTable(result.info, 'lbs');

          $control = 0;
        } else {
          $maps.real_time_position(0, 'lbs');

          buildPositionTable(0);

          alert('Não foram encontradas posições para esse veículo.');

          $control = 1;
        }
      },
      error: function(err) {
        console.log(err);
      },
      /*complete: function(event, xhr) {
      	var $next 		= $('#next') ;
      	var $previous 	= $('#previous');
      	
      	if( xhr == 'success' ) {
      		if( event.responseJSON.previous_page ) {
      			$('#previous_button').html('<a href="javascript:;" id="previous" data-page="'+event.responseJSON.previous_page+'">'+
      			'<img src="/static/img/previous.png"></img></a>');
      		} else if( $previous.length ) {
      			$previous.remove();
      		}
      		
      		if( event.responseJSON.next_page ) {
      			$('#next_button').html('<a href="javascript:;" id="next" data-page="'+event.responseJSON.next_page+'">'+
      			'<img src="/static/img/next.png" class="pull-right"></img></a>');
      		} else if( $next.length ) {
      			$next.remove();
      		}
      	}
      }*/
    });

  }

  return {
    getAssociatedLatLnt: getAssociatedLatLnt,
    getRealTimePosition: getRealTimePosition,
    getHistoryPositions: getHistoryPositions,
    getRealTimePositionSms: getRealTimePositionSms
  };
}();

$(document).ready(function() {
  console.log(1111);
  var $check_all = $('#check_all');
  if ($check_all.length) {
    $check_all.on('click', function() {
      if ($(this).attr('checked') == 'checked') {
        $(this).removeAttr('checked');
        $('.check_driver').attr('checked', false);
      } else {
        $(this).attr('checked', 'checked');
        $('.check_driver').attr('checked', true);
      }
    });
  }

});
