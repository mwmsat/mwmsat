// '#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000'

var parseQueryString = function(queryString) {
  var params = {},
    queries, temp, i, l;
  // Split into key/value pairs
  queries = queryString.split("&");
  // Convert the array of strings into an object
  for (i = 0, l = queries.length; i < l; i++) {
    temp = queries[i].split('=');
    params[temp[0]] = temp[1];
  }
  return params;
};

function setupGrid(data) {

  var $scroll = $('#scrollArea');
  var $content = $('#contentArea');
  var $headers = $("#headersArea");


  /**
   * Makes header columns equal width to content columns
   */
  var fitHeaderColumns = (function() {
    var prevWidth = [];
    return function() {
      var $firstRow = $content.find('tr:not(.clusterize-extra-row):first');
      var columnsWidth = [];
      $firstRow.children().each(function() {
        columnsWidth.push($(this).width());
      });
      if (columnsWidth.toString() == prevWidth.toString()) return;
      $headers.find('tr').children().each(function(i) {
        $(this).width(columnsWidth[i]);
      });
      prevWidth = columnsWidth;
    }
  })();

  /**
   * Keep header equal width to tbody
   */
  var setHeaderWidth = function() {
    $headers.width($content.width());
  }

  /**
   * Set left offset to header to keep equal horizontal scroll position
   */
  var setHeaderLeftMargin = function(scrollLeft) {
    $headers.css('margin-left', -scrollLeft);
  }

  var clusterize = new Clusterize({
    rows: data,
    scrollId: 'scrollArea',
    contentId: 'contentArea',
    callbacks: {
      clusterChanged: function() {
        fitHeaderColumns();
        setHeaderWidth();
      }
    }
  });

  /**
   * Update header columns width on window resize
   */
  $(window).resize(_.debounce(fitHeaderColumns, 150));

  /**
   * Update header left offset on scroll
   */
  $scroll.on('scroll', (function() {
    var prevScrollLeft = 0;
    return function() {
      var scrollLeft = $(this).scrollLeft();
      if (scrollLeft == prevScrollLeft) return;
      prevScrollLeft = scrollLeft;

      setHeaderLeftMargin(scrollLeft);
    }
  }()));
  return clusterize;
}

function formatTrackerData(data) {
  return data.map(function(item) {
    var t = {
      tracker_id: item.tracker_id,
      business_unity_id: item.business_unity_id,
      code: item.code || '(sem número)',
      rf_note: item.rf_note || '--',
      phone_number: item.phone_number,
      address: item.address,
      battery_level: ((item.battery_level !== undefined) && item.battery_level + '%') || '--',
      last_event: item.last_event || '--',
      epoch: item.epoch,
      status_line: item.status_line || '--',
      interval: (item.interval ? item.interval + 'min' : '--'),
      color: item.__color
    }
    t.__searchString = Object.values(t).join('\n');
    return t;
  })
};
$(function() {
  var currentInfoWindow;
  var currentTracker;
  var im_an_admin;
  var map, chart;
  var markersArray = [];

  var qparams = parseQueryString(window.location.search.substring(1));

  $('#multitrack-dropdown').on('change', function() {
    $('#business-unity-dropdown').val('');
    this.form.submit();
  });

  $('#business-unity-dropdown').on('change', function() {
    $('#multitrack-dropdown').val('');
    this.form.submit();
  });


  var timeline_template = Handlebars.compile($("#timeline-template").html());
  var infowindow_template = Handlebars.compile($("#marker-infowindow-template").html());
  var device_row_template = Handlebars.compile($("#device-list-row-template").html());
  var tracker_actions_template = Handlebars.compile($("#tracker-actions-template").html());

  Handlebars.registerHelper('formatDate', function(datetime) {
    return moment(datetime).format('L LTS');
  });

  Handlebars.registerHelper('translateBatteryLevel', function(level) {
    return translate_battery_level(level);
  });

  function string_to_color(str, prc) {
    // https://github.com/brandoncorbin/string_to_color
    'use strict';

    // Check for optional lightness/darkness
    var prc = typeof prc === 'number' ? prc : -10;

    // Generate a Hash for the String
    // var hash = function(word) {
    //   var h = 0;
    //   for (var i = 0; i < word.length; i++) {
    //     h = word.charCodeAt(i) + ((h << 5) - h);
    //   }
    //   return h;
    // };
    var hash = function(s) {
      s = 'hello' + s;
      for (var i = 0, h = 1; i < s.length; i++)
        h = Math.imul(h + s.charCodeAt(i) | 0, 2654435761);
      return (h ^ h >>> 17) >>> 0;
    };

    // Change the darkness or lightness
    var shade = function(color, prc) {
      var num = parseInt(color, 16),
        amt = Math.round(2.55 * prc),
        R = (num >> 16) + amt,
        G = (num >> 8 & 0x00FF) + amt,
        B = (num & 0x0000FF) + amt;
      return (0x1000000 + (R < 255 ? R < 1 ? 0 : R : 255) * 0x10000 +
          (G < 255 ? G < 1 ? 0 : G : 255) * 0x100 +
          (B < 255 ? B < 1 ? 0 : B : 255))
        .toString(16)
        .slice(1);
    };

    // Convert init to an RGBA
    var int_to_rgba = function(i) {
      var color = ((i >> 24) & 0xFF).toString(16) +
        ((i >> 16) & 0xFF).toString(16) +
        ((i >> 8) & 0xFF).toString(16) +
        (i & 0xFF).toString(16);
      return color;
    };

    return shade(int_to_rgba(hash(str)), prc);

  }

  function colorFromId(tracker) {
    var md5_sum = md5(tracker.tracker_id);
    var color = md5_sum.slice(0, 3) + md5_sum.slice(-3);
    return color;
  }


  var COLORS_MARKER_OLDEST = [78, 108, 89];
  var COLORS_MARKER_NEWEST = [78, 210, 90];


  var gradientColors = function(c1, c2, s) {
    var c, cMax, cMin, d, i, j, r;
    if (s == null) {
      s = 1;
    }
    i = void 0;
    j = 1;
    r = [];
    while (j <= s) {
      c = [];
      d = 0;
      i = 0;
      while (i < c1.length) {
        cMax = Math.max(c1[i], c2[i]);
        cMin = Math.min(c1[i], c2[i]);
        c.push(Math.round((cMax - cMin) / (s + 1) * j + cMin));
        i++;
      }
      r.push(c);
      j++;
    }
    return r;
  };
  var reconnect_timeout;

  function connectWs(json) {
// var ws = new ReconnectingWebSocket('ws://' + '127.0.0.1' + ':3000/live');
    var ws = new ReconnectingWebSocket('ws://' +window.location.host + '/live');    
    var msg = {
      type: 'authenticate',
      payload: {
        current_business_unity: qparams['business_unity_id'],
        token: json.token
      }
    };
    ws.onopen = function(event) {
      ws.send(JSON.stringify(msg));
    };

    // ws.onclose = function(event) {
    //   console.log('CLOSE', event);
    //   // if (reconnect_timeout) return;
    //   // reconnect_timeout = setTimeout(function() {
    //   //   console.log('connecting...');
    //   //   var ws = connectWs(json);
    //   //   ws.onopen = function() {
    //   //     console.log('connected!');
    //   //     fetchDevices().then(function(data) {
    //   //       if (window.__grid) {
    //   //         console.log('refreshing');
    //   // 	      window.__grid.attach_ws(ws);
    //   //         window.__grid.refresh(data.trackers);
    //   //       }
    //   //     });
    //   //   };
    //   // });
    // };



    // ws.onerror = function(event) {
    //   console.log('ERROR', event);
    //   ws.close();
    // };

    return ws;
  }

  function initLiveUpdate() {

    return new Promise(function(resolve) {
      $.getJSON('/jwt').done(function(json) {
        im_an_admin = json.im_an_admin;
        resolve({
          ws: connectWs(json),
          auth: json
        });
      });
    });
  }


  function fetchDevices() {
    return new Promise(function(resolve) {
      $.ajax({
        dataType: "json",
        contentType: "application/json",
        accepts: {
          json: "application/json"
        },
        url: window.location.pathname + '/json' + window.location.search,
      }).done(function(data) {
        resolve(data);
      });
    });
  }


  function LiveGrid(kargs) {
    var self = this;

    var _timeout;
    var _prev_input = '';
    var _current_tracker_id;
    self.trackers = {};

    self.allowed_commands = kargs.data.allowed_commands;
    self.is_admin = kargs.auth.im_an_admin;
    im_an_admin = self.is_admin
    self._plotLastPositions = function(_trackers) {
      var trackers = _trackers.filter(function(p) {
	return parseFloat(p.lat) && parseFloat(p.lng);
      });
      var all_at_once = true;
      kargs.data.trackers.map((t) => t.__color = '#' + string_to_color(t.tracker_id));
      self.grid = self._buildGrid(kargs.data.trackers);
      _loadMarkers(trackers, all_at_once);
    };
    self._buildGrid = function(trackers) {
      var tt = self._buildRows(trackers);
      return setupGrid(self.sort(Object.values(self.trackers)).map((i) => i.rendered));
    };

    self._buildRows = function(trackers) {
      return formatTrackerData(trackers).map(function(item) {
        item.im_an_admin = self.is_admin;
        item.allowed_commands = self.allowed_commands;
        item.rendered = device_row_template(item);
        self.trackers[item.tracker_id] = item;
        return item;
      });
    };
    self.addTracker = function(tracker) {
      self._buildRows([].concat(tracker));
      if (($('#device-search').val() || '').length) return self.filter($('#device-search').val());
      return self.update();
    };
    self.removeTracker = function(tracker) {
      [].concat(tracker).map(function(i) {
        delete self.trackers[i.tracker_id];
      });
      if (($('#device-search').val() || '').length) return self.filter($('#device-search').val());
      return self.update();
    };

    self.updateTracker = function(tracker) {
      self.addTracker(tracker);
    };

    self.update = function(items) {
      self.grid.update(self.sort((items || Object.values(self.trackers))).map((i) => i.rendered));
    };

    self.refresh = function(items) {
      self.grid = self._buildGrid(items);
      if (($('#device-search').val() || '').length) return self.filter($('#device-search').val());
      return self.update();
    };

    self.update_delivery = function(delivery) {
      var title = [];
      if (_current_tracker_id && (_current_tracker_id != delivery.tracker_id)) return;
      if (delivery.created_at) {
        $('#mark-delivery-server').addClass('delivered');
        title.push('Entregue no servidor: ' + moment(delivery.created_at).format('DD/MM/YYYY LTS'));
      }
      if (delivery.delivered_at) {
        $('#mark-delivery-tracker').addClass('delivered');
        title.push('Entregue no equipamento: ' + moment(delivery.delivered_at).format('DD/MM/YYYY LTS'));
      }
      $('#mark-delivery').attr('title', title.join('\n'));
    };

    self.sort = function(items, key) {
      if (!key) {
        key = 'epoch';
      }
      return items.slice().sort(function(a, b) {
        a = a[key];
        b = b[key];
        return (a === b ? 0 : a > b ? 1 : -1) * -1;
      });
    };

    self.filter = function(key) {
      var filterKey = key.toLowerCase();
      self.update(Object.values(self.trackers).filter(function(row) {
        return row.__searchString.toLowerCase().indexOf(filterKey) > -1;
      }));
    };
    if (qparams['multitrack_id']) {
      self._plotLastPositions(kargs.data.trackers);
    } else {
      self.grid = self._buildGrid(kargs.data.trackers);
    }

    self.attach_ws = function(ws) {
      var lost_connection = false;
      self.ws = ws;
      self.ws.onclose = function() {
        lost_connection = true;
      };
      self.ws.addEventListener('open',
        function() {
          if (lost_connection) {
            lost_connection = false;
            fetchDevices().then(function(data) {
              self.refresh(data.trackers);
            });
          }
        });
      self.ws.onmessage = function(e) {
        var msg = JSON.parse(e.data);
// console.log(msg);
        if (msg.remove) self.removeTracker(msg.remove);
        if (msg.add) self.addTracker(msg.add);
        if (msg.update) self.updateTracker(msg.update);
        if (msg.delivery_status && _current_tracker_id) self.update_delivery(msg.delivery_status);
      };
    };

    self.attach_ws(kargs.ws);

    $(document).on('click', '.show-tracker-position-btn', function(e) {
      var tracker = self.trackers[$(e.target).attr('data-tracker-id')];
      _current_tracker_id = tracker.tracker_id;
      tracker.allowed_commands = self.allowed_commands;
      $('#tracker-actions-bar').html(tracker_actions_template(tracker));
    });

    $('#device-search').on('input', function(e) {
      if (_timeout) {
        window.clearTimeout(_timeout);
        _timeout = null;
      }
      var input = $('#device-search').val();
      if (_prev_input == input) return;
      if (_prev_input.length > 0 && input.length == 0) return self.update();
      _prev_input = input;
      if (input.length < 3) return;
      _timeout = window.setTimeout(function() {
        self.filter(input)
      }, 600);
    });
  }

  function setupGridAndMap() {

    if (qparams['business_unity_id'] || qparams['multitrack_id'])
      Promise.all([fetchDevices(), initLiveUpdate()]).then(function(values) {
        var data = values[0];
        var live = values[1];
        var trackers = data.trackers;
        // if (trackers && trackers.length) {
        //   var range = function(n) {
        //     return Array(n + 1).join(1).split('').map((x, i) => i)
        //   };
        //   range(12).map(function(i) {
        //     trackers = trackers.slice().concat(trackers);
        //   });
        //   var ii = 0;
        //   data.trackers = trackers.map(function(t) {
        //     t = Object.assign({}, t);
        //     ii += 1;
        //     t.code = ( 'CODE-') + parseInt(Math.random() * 1000000);
        //     t.tracker_id = ii;
        //     return t
        //   });
        // }
        window.__grid = new LiveGrid({
          ws: live.ws,
          auth: live.auth,
          data: data
        });

      });
  }
  google.maps.event.addDomListener(window, 'load', initMap);

  function initMap() {
    var map_dom = document.getElementById("map");
    if (!map_dom) return null;
    var lat = -23.112575;
    var long = -47.212372;

    function buildMap(la, lo) {
      var latlng = new google.maps.LatLng(la, lo);
      var myOptions = {
        zoom: 10,
        center: latlng,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(map_dom, myOptions);

      $('#sidebar').on('hidden.bs.collapse', function() {
        google.maps.event.trigger(map, 'resize')
      });

      $('#sidebar').on('show.bs.collapse', function() {
        google.maps.event.trigger(map, 'resize')
      });

    }

    //	 if (navigator.geolocation) {
    if (false) {
      navigator.geolocation.getCurrentPosition(function(position) {
          buildMap(position.coords.latitude, position.coords.longitude);
        },
        function() {
          buildMap(lat, long);
        });
    } else {
      buildMap(lat, long);
    }
    setupGridAndMap();
    return true;
  }

  function clearOverlays() {
    if (markersArray) {
      for (i in markersArray) {
        markersArray[i].setMap(null);
      }
    }
    markersArray = [];
  }



  function addMarker(location, dialogInfo) {
    var marker;
    clearOverlays();
    if (!dialogInfo) {
      marker = new google.maps.Marker({
        position: location,
        map: map
      });
    } else {
      marker = new google.maps.Marker({
        map: map,
        position: location,
        icon: dialogInfo.markerIcon,
        info: dialogInfo.azimuth == undefined ?
          '<br /> Placa: ' + dialogInfo.car_plate +
          '<br />Data: ' + dialogInfo.date +
          '<br /> Hora: ' + dialogInfo.hour +
          '<br />Velocidade :' + dialogInfo.speed +
          ' Km/h' + '<br />LatLng :' + dialogInfo.latlng + '<br />' : '<br /> Azimuth: ' + dialogInfo.azimuth +
          '<br /> Radius: ' + dialogInfo.radius +
          '<br /> LatLng: ' + dialogInfo.latlng
      });

      google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
          infowindow.setContent(this.info);
          infowindow.open(map, marker);
        }
      })(marker));
    }

    map.panTo(location);
    map.setZoom(13);

    markersArray.push(marker);
  }


  function fetchPositions(id) {
    return $.ajax({
      type: 'GET',
      dataType: 'json',
      url: '/tracker-manager/position',
      data: {
        id: id
      }
    }).fail(function(e) {
      if (e.status == 403) {

      }
    });
  }

  function translate_battery_level($level) {
    if ($level > 4150) return 100;
    if ($level < 3600) return 0;
    return parseInt(0.182621 * $level - 658.005);
  }


  function _attachMessage(marker, messageOptions) {
    var newInfoPanel;
    if (messageOptions == null) {
      messageOptions = null;
    }
    if (messageOptions !== null) {
      newInfoPanel = new google.maps.InfoWindow(messageOptions);
    } else {
      newInfoPanel = null;
    }
    marker.addListener('click', (function() {
      return function() {
        if (currentInfoWindow) {
          currentInfoWindow.close();
        }
        if (newInfoPanel !== null) {
          currentInfoWindow = newInfoPanel;
          newInfoPanel.open(map, marker);
        }
      };
    })());
  };


  function _loadMarkers(positions, all_at_once) {
    var i = 0;
    clearOverlays();

    var colors = gradientColors(COLORS_MARKER_OLDEST, COLORS_MARKER_NEWEST, positions.length);

    var bounds = new google.maps.LatLngBounds;
    $.each(positions, function(i, position) {
      var lat = parseFloat(position.lat);
      var lng = parseFloat(position.lng);
      var icon;
      if (all_at_once && position.__color) {
        icon = {
          fillColor: position.__color,
          path: 'M -2,0 0,-2 2,0 0,2 z',
          strokeColor: 'red',
          fillOpacity: 1,
          scale: 4,
          strokeWeight: 1
        };

      }
      var googleMarker = new google.maps.Marker({
        position: {
          lat: parseFloat(lat),
          lng: parseFloat(lng)
        },
        map: map,
        markerIndex: i,
        icon: icon,
        visible: all_at_once ? true : false,
      });
      bounds.extend(googleMarker.getPosition());
      markersArray.push(googleMarker);
      _attachMessage(googleMarker, {
        position: googleMarker.getPosition(),
        content: infowindow_template({
          position: position,
          tracker: currentTracker
        })
      });
    });
    map.fitBounds(bounds);
    map.panToBounds(bounds);
    if (!all_at_once) {
      var last_index = markersArray.length - 1;
      markersArray[last_index].setVisible(true);
      google.maps.event.trigger(markersArray[last_index], 'click');
      map.setZoom(15);
    }

  }

  function _bindTimeline(slider, bubble, positions) {
    var inputEvtHasNeverFired, rangeValue;
    inputEvtHasNeverFired = true;

    rangeValue = {
      current: void 0,
      mostRecent: void 0
    };
    slider.addEventListener('input', ((function() {
      return function(e) {
        inputEvtHasNeverFired = false;
        rangeValue.current = e.currentTarget.value;
        if (rangeValue.current !== rangeValue.mostRecent) {
          _updateOutputBubble(bubble, slider, positions);
          _toggleMarkers(e.currentTarget.value);
          return rangeValue.mostRecent = rangeValue.current;
        }
      };
    })()), false);

    return slider.addEventListener('change', ((function() {
      return function(e) {
        if (inputEvtHasNeverFired) {
          _updateOutputBubble(bubble, slider, positions);
          return _toggleMarkers(e.currentTarget.value);
        }
      };
    })()), false);
  };


  function _toggleMarkers(i, only) {
    var j;
    if (only == null) {
      only = null;
    }
    j = 0;
    var bounds = new google.maps.LatLngBounds;



    while (j < markersArray.length) {
      if (j >= i) {
        if (!markersArray[j].getVisible()) {

          markersArray[j].setVisible(true);
          bounds.extend(markersArray[j].getPosition());
        }
      } else {
        if (markersArray[j].getVisible()) {
          markersArray[j].setVisible(false);
        }
      }
      j++;
    }

    google.maps.event.trigger(markersArray[i], 'click');
  }



  function _updateOutputBubble(bubble, slider, positions) {
    var newPlace, newPoint, thumbWidth, width;
    newPoint = 0;
    newPlace = 0;
    thumbWidth = 36;
    width = slider.offsetWidth - thumbWidth;
    newPoint = (slider.value - slider.getAttribute('min')) / (slider.getAttribute('max') - slider.getAttribute('min'));
    if (newPoint < 0) {
      newPlace = 0;
    } else if (newPoint > 1) {
      newPlace = width;
    } else {
      newPlace = width * newPoint;
    }
    bubble.style.left = newPlace + 'px';
    bubble.textContent = positions[slider.value].event_date_tz || 0; // moment(positions[slider.value].event_date).format('L LTS') || 0;
  };


  function loadTimeline(_positions) {
    var positions = _positions.filter(function(p) {
      return parseFloat(p.lat) && parseFloat(p.lng);
    });
    
    $('.map-container .slider').html(timeline_template({
      positions: positions,
      size: positions.length - 1,
      current: positions[positions.length - 1]
    }));

    _loadMarkers(positions);
    var slider = document.getElementById('history-slider');
    var bubble = document.getElementById('history-slider-bubble');
    _updateOutputBubble(bubble, slider, positions);
    _bindTimeline(slider, bubble, positions);
  }

  function loadPositions(result, a, b) {
    var positions = result.positions;
    if (window.__grid) {
      window.__grid.update_delivery(result.delivery);
    }
    currentTracker = result.tracker;
    currentTracker.delivery = result.delivery;
    // currentTracker = im_an_admin;
    if (positions.length == 0) {
      return alert('Não há posições');
    }
    positions.reverse();

    loadTimeline(positions);
    return result;
  }

  $(document).on('click', '.show-tracker-position-btn', function(e) {
    fetchPositions($(e.target).attr('data-tracker-id')).then(loadPositions);
  });

  $('#send-message').on('show.bs.modal', function(event) {

    var button = $(event.relatedTarget);
    var tracker_id = button.data('tracker-id');
    var phone_number = button.data('tracker-phone-number');
    var modal = $(this);

    modal.find('.modal-title').text('Nova mensagem para ' + phone_number);
    modal.find('.modal-body input[name="tracker_id"]').val(tracker_id);
    modal.find('.modal-body input[name="message"]').val('');

  });

  $('#send-message-btn').click(function(e) {
    var data = $('#send-message form').serializeArray();
    send_message(data).then(() => $('#send-message').modal('hide'));;
  });

  $(document).on('click', '.send-command', function(e) {

    var anchor = $(e.target);
    send_command(anchor.data('command-id'), anchor.data('tracker-id')).then(function(ok) {
      alert('Mensagem enviada com sucesso');
    }).catch(function(fail) {
      alert('Erro ao enviar mensagem');
    });
  });

  function send_command(command_id, tracker_id) {
    return $.ajax({
      type: 'POST',
      url: '/' + ['admin', 'command', command_id, 'send', tracker_id].join('/')
    }).fail(function(a, b) {
      console.log('fail', a, b);
    });
  }

  function send_message(data) {
    return $.ajax({
      type: 'POST',
      dataType: 'json',
      url: '/tracker-sms/collector',
      data: data
    }).fail(function(a, b) {
      console.log('fail', a, b);
    });
  }

});
