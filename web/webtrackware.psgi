#!/usr/bin/env perl

use strict;
use warnings;

use Plack::Builder;
use WebTrackware;
use IO::Handle;

autoflush STDOUT 1;
autoflush STDERR 1;

my $app = WebTrackware->psgi_app(@_);

builder {
  enable_if { $_[0]->{REMOTE_ADDR} eq '127.0.0.1' }
  "Plack::Middleware::ReverseProxy";
  mount '/' => $app;
};

