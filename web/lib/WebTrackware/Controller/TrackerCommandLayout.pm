package WebTrackware::Controller::TrackerCommandLayout;
use Moose;
use namespace::autoclean;
use URI;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/root') : PathPart('tracker-command-layout') :
  CaptureArgs(0) {
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ( $self, $c, $id ) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    [ 'tracker_command_layouts', $id ],
    stash => 'tracker_command_layout_obj'
  );

  $c->detach( 'TrackerCommandLayout' => 'edit' );
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    'tracker_command_layouts',
    params => {
      business_unity_id => $c->stash->{session_business_unity}{id}
      ? $c->stash->{session_business_unity}{id}
      : undef
    }
  );
}

sub edit : Chained('object') : PathPart('') : Args(0) {
}

sub add : Chained('base') : PathPart('new') : Args(0) {
}

# Action para listar os comandos via xhr
sub list : Chained('base') : PathPart('list') : Args(0) {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    'tracker_command_layouts',
    params => {
      business_unity_id => $c->stash->{session_business_unity}{id}
      ? $c->stash->{session_business_unity}{id}
      : undef
    }
  );

  $c->stash->{select_commands} = [ map { [ $_->{id}, $_->{name} ] }
      @{ $c->stash->{tracker_command_layouts} } ];

  $c->stash->{without_wrapper} = 1;
}

__PACKAGE__->meta->make_immutable;

1;
