package WebTrackware::Controller::VehiclePosition;
use Moose;
use utf8;
use JSON::XS;
use namespace::autoclean;
use DateTime;
use Encode qw(encode_utf8);

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/root') : PathPart('vehicle_position') : CaptureArgs(0) {
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ( $self, $c, $id ) = @_;
}

sub index : Chained('base') : PathPart('') : Args(0) {
}

sub history : Chained('base') : PathPart('history') : Args(0) {
}

sub position_lbs : Chained('base') : PathPart('position_lbs') : Args(0) {
}

sub get_positions : Chained('base') : PathPart('get_positions') {
  my ( $self, $c, $vehicle_id ) = @_;

  my $api = $c->model('API');

  my $car_plate;
  my @final_pos;

  if ( $c->req->params->{data} ) {
    my @par = split '=', $c->req->params->{data};

    $car_plate = lc $par[1];
    $car_plate =~ s/-//g;
  }

  if ( !$vehicle_id || $car_plate ) {
    $api->stash_result(
      $c,
      'vehicles',
      params => {
        car_plate => $car_plate
      }
    );

    $vehicle_id = $c->stash->{vehicles}[0]{id};
  }

  my $limit = 30;
  my $page = $c->req->params->{page} ? $c->req->params->{page} : 1;

  $api->stash_result(
    $c,
    'tracker_positions',
    params => {
      vehicle_id => $vehicle_id,
      limit      => $limit,
      offset     => ( $limit * $page ) - $limit,
      page       => $page,
      positions  => 1
    }
  );

  my $next_page;
  my $previous_page;

  my $count = $c->stash->{position_count};

  if ( $count > $limit && $count != $c->req->params->{page} ) {
    $next_page = $c->req->params->{page} ? $c->req->params->{page} + 1 : 2;
  }

  if ( $c->req->params->{page} >= 2 ) {
    $previous_page = $c->req->params->{page} - 1;
  }

  my @info;
  my @points;

  foreach my $position ( @{ $c->stash->{tracker_positions} } ) {
    my $address;

    if ( $position->{lat} && $position->{lng} ) {
      my $point_id = substr( $position->{lat}, 0, 8 ) . '&'
        . substr( $position->{lng}, 0, 8 );
      my $point_orig_id = substr( $position->{lat_orig}, 0, 8 ) . '&'
        . substr( $position->{lng_orig}, 0, 8 );

      $api->stash_result(
        $c,
        'position_cache',
        params => {
          point_id => $point_id,
        }
      );

      if ($point_id) {
        $address = $c->stash->{position_cache}[0]{address};

        if ( !scalar keys %{ $c->stash->{position_cache}[0] } ) {
          $api->stash_result(
            $c,
            'position_cache/reverse_geolocation',
            params => {
              lat_lng       => $position->{lat} . ',' . $position->{lng},
              point_id      => $point_id,
              point_orig_id => $point_orig_id
            }
          );

          $address = $c->stash->{reverse_geolocation}{formatted_address};
        }
        elsif ( !$c->stash->{position_cache}[0]{point_orig_id} ) {
          $api->stash_result(
            $c,
            [ 'position_cache', $c->stash->{position_cache}[0]{id} ],
            method => 'PUT',
            body   => {
              point_orig_id => $point_orig_id
            }
          );
        }

      }
    }
    else {
      $address = 'Sem dados de localização';
    }

    push(
      @info,
      {
        address          => $address,
        event_date       => $position->{event_date},
        created_at       => $position->{created_at},
        speed            => $position->{speed},
        lat              => $position->{lat},
        lng              => $position->{lng},
        car_plate        => $position->{car_plate},
        reason_generator => $position->{reason_generator},
        firmware_version => $position->{firmware_version},
        position_counter => $position->{position_counter},
        position_type    => $position->{position_type},
      }
    );

    my @time = split ' ', $position->{event_date};
    my $date = $self->format_date_to_human( $time[0] );

    push(
      @points,
      {
        lat       => $position->{lat},
        lng       => $position->{lng},
        speed     => $position->{speed},
        date      => $date,
        car_plate => $position->{car_plate},
        hour      => $time[1],
      }
    );
  }

  my $data = {
    'info'          => \@info,
    'points'        => \@points,
    'next_page'     => $next_page,
    'previous_page' => $previous_page
  };

  $c->res->header( 'content-type', 'application/json;charset=UTF-8' );
  $c->res->body( encode_json( \%$data ) );
}

sub get_real_time_position : Chained('base') :
  PathPart('get_real_time_position') : Args(0) {
  my ( $self, $c ) = @_;

  my $api  = $c->model('API');
  my $form = $c->model('Form');
  my $par;

  if ( $c->req->params->{data} ) {
    my @params = split "&", $c->req->params->{data};
    my $size = @params;

    for ( my $i = 0 ; $i < $size ; $i++ ) {
      my @arr = split '=', $params[$i];

      my $arr_size = @arr;

      next if $arr_size < 2;

      if ( $arr[0] eq 'car_plate' ) {
        $arr[1] = lc $arr[1];
        $arr[1] =~ s/-//g;
      }

      $par->{ $arr[0] } = $arr[1];
    }
  }

  $api->stash_result(
    $c,
    'vehicles',
    params => {
      business_unity_id => $c->stash->{session_business_unity}{id},
      $par ? %$par : undef
    }
  );

  my @ids;
  foreach my $vehicle ( @{ $c->stash->{vehicles} } ) {
    push( @ids, $vehicle->{id} );
  }

  my $limit = 30;

  $api->stash_result(
    $c,
    'tracker_positions/get_last_position',
    params => {
      ids   => encode_json( \@ids ),
      page  => $c->req->params->{page} ? $c->req->params->{page} : 1,
      limit => $limit
    }
  );

  my $next_page;
  my $previous_page;

  my $count = $c->stash->{real_time_position_count};

  if ( $count > $limit && $count != $c->req->params->{page} ) {
    $next_page = $c->req->params->{page} ? $c->req->params->{page} + 1 : 2;
  }

  if ( $c->req->params->{page} >= 2 ) {
    $previous_page = $c->req->params->{page} - 1;
  }

  my @points;
  my @info;

  if ( $c->stash->{real_time_position} ) {

    foreach my $position ( @{ $c->stash->{real_time_position} } ) {
      my $point_id = substr( $position->{lat}, 0, 8 ) . '&'
        . substr( $position->{lng}, 0, 8 );
      my $point_orig_id = substr( $position->{lat_orig}, 0, 8 ) . '&'
        . substr( $position->{lng_orig}, 0, 8 );

      my $address;

      if ($point_id) {
        $api->stash_result(
          $c,
          'position_cache',
          params => {
            point_id => $point_id,
          }
        );

        $address = $c->stash->{position_cache}[0]{address};

        if ( !scalar keys %{ $c->stash->{position_cache}[0] } ) {
          $api->stash_result(
            $c,
            'position_cache/reverse_geolocation',
            params => {
              lat_lng       => $position->{lat} . ',' . $position->{lng},
              point_id      => $point_id,
              point_orig_id => $point_orig_id
            }
          );

          $address = $c->stash->{reverse_geolocation}{formatted_address};
        }
        elsif ( !$c->stash->{position_cache}[0]{point_orig_id} ) {
          $api->stash_result(
            $c,
            [ 'position_cache', $c->stash->{position_cache}[0]{id} ],
            method => 'PUT',
            body   => {
              point_orig_id => $point_orig_id
            }
          );
        }
      }

      push(
        @info,
        {
          address          => $address,
          event_date       => $position->{event_date},
          created_at       => $position->{created_at},
          speed            => $position->{speed},
          car_plate        => $position->{vehicle_car_plate},
          vehicle_id       => $position->{vehicle_id},
          lat              => $position->{lat},
          lng              => $position->{lng},
          reason_generator => $position->{reason_generator},
          firmware_version => $position->{firmware_version},
          position_counter => $position->{position_counter},
          position_type    => $position->{position_type},
          real_time        => 1,
        }
      );

      my @time = split ' ', $position->{event_date};
      my $date = $self->format_date_to_human( $time[0] );

      push(
        @points,
        {
          lat       => $position->{lat},
          lng       => $position->{lng},
          speed     => $position->{speed},
          date      => $date,
          car_plate => $position->{vehicle_car_plate},
          hour      => $time[1],
        }
      );
    }

    my $data = {
      'info'          => \@info,
      'points'        => \@points,
      'next_page'     => $next_page,
      'previous_page' => $previous_page
    };

    $c->res->header( 'content-type', 'application/json;charset=UTF-8' );
    $c->res->body( encode_json( \%$data ) );
  }
  else {
    $c->res->body(0);
  }
}

sub get_lbs_position : Chained('base') : PathPart('get_real_time_position_sms')
  : Args(0) {
  my ( $self, $c ) = @_;

  my $par;
  my $api = $c->model('API');

  if ( $c->req->params->{data} ) {
    my @params = split "&", $c->req->params->{data};
    my $size = @params;

    for ( my $i = 0 ; $i < $size ; $i++ ) {
      my @arr = split '=', $params[$i];

      my $arr_size = @arr;

      next if $arr_size < 2;

      if ( $arr[0] eq 'car_plate' ) {
        $arr[1] = lc $arr[1];
        $arr[1] =~ s/-//g;
      }

      $par->{ $arr[0] } = $arr[1];
    }
  }

  $api->stash_result(
    $c,
    'tracker_sms_positions',
    params => {
      imei     => $par->{imei},
      position => 1
    }
  );

  my @points;
  foreach my $position ( @{ $c->stash->{real_time_position} } ) {
    push(
      @points,
      {
        lat     => $position->{lat},
        lng     => $position->{lng},
        azimuth => $position->{azimuth},
        radius  => $position->{radius},
      }
    );
  }

  $c->stash->{info}{event_date} =
    $self->format_date_to_human( $c->stash->{info}{event_date} )
    if $c->stash->{info}{event_date};
  $c->stash->{info}{created_at} =
    $self->format_date_to_human( $c->stash->{info}{created_at} )
    if $c->stash->{info}{created_at};

  my $data = {
    'info'   => $c->stash->{info},
    'points' => \@points,
  };

  $c->res->header( 'content-type', 'application/json;charset=UTF-8' );
  $c->res->body( encode_json( \%$data ) );
}

sub format_date_to_human {
  my ( $self, $date ) = @_;

  my @data = split ' ', $date;
  my $str;

  my ( $y, $m, $d ) = $data[0] =~ m/^(\d{4})-(\d{1,2})-(\d{1,2})$/;

  $str = $d . '/' . $m . '/' . $y;
  if ( scalar @data > 1 ) {
    my ( $hour, $min, $sec ) = $data[1] =~ m/^(\d{1,2}):(\d{1,2}):(\d{1,2}).*$/;

    $str = $d . '/' . $m . '/' . $y . ' ' . $hour . ':' . $min . ':' . $sec;
  }

  return $str;
}

sub build_table_info : Chained('base') : PathPart('build_table_info') : Args(0)
{
  my ( $self, $c ) = @_;

  my $params = { %{ $c->req->params } };

  $c->stash->{without_wrapper} = 1;

  if ( $params->{lbs} ) {
    $c->stash->{template} = 'vehicleposition/build_table_info_sms.tt',;
  }

  if ( $params->{info} ) {
    my $string = encode_utf8( $params->{info} );

    # 	my $p = decode_json( $string );
    # 	use DDP; p $p; exit;
    $c->stash->{positions} = decode_json($string);
  }
  else {
    $c->stash->{positions} = undef;
  }
}

__PACKAGE__->meta->make_immutable;

1;
