package WebTrackware::Controller::Admin;
use Moose;
use namespace::autoclean;
use URI;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/root') : PathPart('admin') : CaptureArgs(0) {
  my ($self, $c) = @_;

  if (!$c->user || !grep {/^superadmin$/} $c->user->roles) {
    $c->detach('/form/not_found', []);
  }

  $c->stash->{user_name} = $c->user->name;

  $c->stash->{template_wrapper} = 'admin';
  my $api = $c->model('API');

  if ($c->req->method eq 'POST') {
    return;
  }

}

__PACKAGE__->meta->make_immutable;

1;
