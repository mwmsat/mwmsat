package WebTrackware::Controller::User::Vehicle;
use Moose;
use namespace::autoclean;
use DateTime;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/user/base') : PathPart('vehicle') : CaptureArgs(0) {
    my ( $self, $c ) = @_;

    my $api = $c->model('API');
    my $b = $c->stash->{vehicles};
    use DDP; p $b;

    $api->stash_result( $c, 'states' );
    $c->stash->{select_states} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{states} } ];

    $api->stash_result(
        $c, 'cities',
        params => {
            order       => 'name',
            state_id    => $c->stash->{vehicles}[0]{state_id}
        }
    );
    $c->stash->{select_cities} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{cities} } ];

    $api->stash_result(
        $c, 'vehicle_colors',
        params => {
            order => 'name'
        }
    );
    $c->stash->{select_colors} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{vehicle_colors} } ];

    $api->stash_result(
        $c, 'vehicle_brands',
         params => {
            order => 'name'
        }
    );
    $c->stash->{select_brands} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{vehicle_brands} } ];

    $api->stash_result(
        $c, 'vehicle_models',
         params => {
            order               => 'name',
            vehicle_brand_id    => $c->stash->{vehicles}[0]{brand}{id}
        }
    );
    $c->stash->{select_models} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{vehicle_models} } ];

    $api->stash_result(
        $c, 'insurance_companies',
        params => {
            order => 'name'
        }
    );
    $c->stash->{select_insurance_companies} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{insurance_company} } ];

    $self->_build_valid_years($c);
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'vehicles', $id ], stash => 'vehicle_obj' );
    $self->_build_valid_years($c);

    $c->detach( '/form/not_found', [] ) if $c->stash->{vehicle_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;
}

sub edit : Chained('object') : PathPart('') : Args(0) {
}

sub _build_valid_years :Private {
    my ( $self, $c ) = @_;

    my $year       = DateTime->now;
    my $first_year = $year->year - 3;
    my @year_range;
    my %vehicle_years;

    $year_range[0] = $first_year;
    $vehicle_years{ $year_range[0] } = $first_year;

    for ( my $i = 1 ; $i < 6 ; $i++ ) {
        $year_range[$i] = $year_range[ $i - 1 ] + 1;
        $vehicle_years{ $year_range[$i] } = $year_range[$i];
    }

    $c->stash->{vehicle_years} = [ map { [ $_, $vehicle_years{$_} ] } sort keys %vehicle_years ];

    return 1;
}

__PACKAGE__->meta->make_immutable;

1;
