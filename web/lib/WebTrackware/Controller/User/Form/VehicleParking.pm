package WebTrackware::Controller::User::Form::VehicleParking;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/user/form/base') : PathPart('') : CaptureArgs(0) {
    my ( $self, $c ) = @_;

    for my $field (qw /departure_time entry_time/) {
        $c->req->params->{$field} .= ':00';
    }
    $c->req->params->{is_street} ||= 0;

}

sub process : Chained('base') : PathPart('vehicle_parking') : Args(0) {
    my ( $self, $c ) = @_;

    my $api = $c->model('API');

    my $params = { %{ $c->req->params }, user_id => $c->user->id };
    $api->stash_result(
        $c, ['vehicle_parking'],
        method => 'POST',
        body   => $params
    );

    if ( $c->stash->{error} ) {

        $c->detach( '/form/redirect_error', [] );

    }
    else {
        $c->detach(
            '/form/redirect_ok',
            [
                $c->req->params->{redirect_to_dashboard}
                ? '/user/dashboard/index'
                : '/user/parking/index'

                , {}, 'Cadastrado com sucesso!'
            ]
        );
    }
}

sub process_edit : Chained('base') : PathPart('vehicle_parking') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result(
        $c, [ 'vehicle_parking', $id ],
        method => 'PUT',
        body   => $c->req->params
    );

    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    }
    else {
        $c->detach( '/form/redirect_ok', [ '/user/parking/index', {}, 'Alterado com sucesso!' ] );
    }
}

sub process_delete : Chained('base') : PathPart('remove_vehicle_parking') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'vehicle_parking', $id ], method => 'DELETE' );

    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    }
    else {
        $c->detach( '/form/redirect_ok', [ '/user/parking/index', {}, 'Removido com sucesso!' ] );
    }
}

__PACKAGE__->meta->make_immutable;

1;
