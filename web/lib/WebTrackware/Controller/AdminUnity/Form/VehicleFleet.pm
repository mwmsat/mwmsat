package WebTrackware::Controller::AdminUnity::Form::VehicleFleet;
use Moose;
use namespace::autoclean;
use DateTime;
use JSON::XS;
use utf8;
BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/form/base') : PathPart('vehicle_fleet') : CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub process : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;

    my $api		= $c->model('API');
    my $form    = $c->model('Form');

    my $params = { %{ $c->req->params } };
    
    $params->{business_unity_id} = $c->stash->{session_business_unity}{id};
    
    $api->stash_result(
        $c, 'vehicle_fleets',
        method  => 'POST',
        body    => $params
    );
	
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/vehiclefleet/index', {}, 'Cadastrado com sucesso!' ] );
    }
}

sub process_edit : Chained('base') : PathPart('') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');
    
    $api->stash_result(
        $c, [ 'vehicle_fleets', $id ],
        method  => 'PUT',
        body    => $c->req->params
    );

    if ( $c->stash->{error} ) {
    
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/vehiclefleet/index', {}, 'Alterado com sucesso!' ] );
    }
}

sub process_delete : Chained('base') : PathPart('remove') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'vehicle_fleets', $id ], method => 'DELETE' );

    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    }
    else {
        $c->detach( '/form/redirect_ok', [ '/admin/vehiclefleet/index', {}, 'Removido com sucesso!' ] );
    }
}

__PACKAGE__->meta->make_immutable;

1;