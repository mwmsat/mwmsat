package WebTrackware::Controller::AdminUnity::Form::Customer;
use Moose;
use namespace::autoclean;
use DateTime;
use JSON::XS;
use utf8;
BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/form/base') : PathPart('customer') : CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub process : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;

    my $api		= $c->model('API');
    my $form    = $c->model('Form');

    my $params = { %{ $c->req->params } };

    my @fields;

    push(@fields, 'register','phone', 'mobile_phone', 'postal_code');

    $form->only_number( $params, @fields );
    
    $params->{business_unity_id} = $c->stash->{session_business_unity}{id};
    
    $api->stash_result(
        $c, 'customers/complete',
        method  => 'POST',
        body    => $params
    );
	
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/customer/index', {}, 'Cadastrado com sucesso!' ] );
    }
}

sub process_edit : Chained('base') : PathPart('') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api		= $c->model('API');
    my $form    = $c->model('Form');

    my $params = { %{ $c->req->params } };

    my @fields;

    push(@fields, 'register','phone', 'mobile_phone', 'postal_code');

    $form->only_number( $params, @fields );
    
    $params->{business_unity_id} = $c->stash->{session_business_unity}{id};
    
    $api->stash_result(
        $c, ['customers/update_complete', $id],
        method  => 'PUT',
        body    => $params
    );
	
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/customer/index', {}, 'Alterado com sucesso!' ] );
    }
}

sub process_delete : Chained('base') : PathPart('remove') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'customers', $id ], method => 'DELETE' );

    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    }
    else {
        $c->detach( '/form/redirect_ok', [ '/adminunity/customer/index', {}, 'Inativado com sucesso!' ] );
    }
}

__PACKAGE__->meta->make_immutable;

1;