package WebTrackware::Controller::AdminUnity::Form::Vehicle;
use Moose;
use namespace::autoclean;
use DateTime;
use JSON::XS;
use utf8;
BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/form/base') : PathPart('vehicle') : CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub process : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;

    my $api		= $c->model('API');
    my $form	= $c->model('Form');

    my $params = { %{ $c->req->params } };

    $form->format_car_plate( $params, 'car_plate' );
    
    $params->{business_unity_id} = $c->stash->{session_business_unity}{id};
    
    $api->stash_result(
        $c, 'vehicles',
        method  => 'POST',
        body    => $params
    );
	
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/vehicle/index', {}, 'Cadastrado com sucesso!' ] );
    }
}

sub process_edit : Chained('base') : PathPart('') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api		= $c->model('API');
    my $form    = $c->model('Form');

    my $params = { %{ $c->req->params } };

    my @fields;

    push(@fields, 'car_plate');
    
	$form->format_car_plate( $params, 'car_plate' );
    
    $api->stash_result(
		$c, ['vehicles', $id],
		method 	=> 'PUT',
		body 	=> $params
    );

    if ( $c->stash->{error} ) {
    
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/vehicle/index', {}, 'Alterado com sucesso!' ] );
    }
}

sub process_delete : Chained('base') : PathPart('remove') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'vehicles', $id ], method => 'DELETE' );

    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    }
    else {
        $c->detach( '/form/redirect_ok', [ '/adminunity/vehicle/index', {}, 'Removido com sucesso!' ] );
    }
}

__PACKAGE__->meta->make_immutable;

1;