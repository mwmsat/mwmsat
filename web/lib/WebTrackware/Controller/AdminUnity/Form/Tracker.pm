package WebTrackware::Controller::AdminUnity::Form::Tracker;
use Moose;
use namespace::autoclean;
use DateTime;
use JSON::XS;
use utf8;
BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/form/base') : PathPart('tracker') : CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub process : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;

    my $api		= $c->model('API');
    
    $api->stash_result(
        $c, 'trackers',
        method  => 'POST',
        body    => $c->req->params
    );
	
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/tracker/index', {}, 'Cadastrado com sucesso!' ] );
    }
}

sub process_edit : Chained('base') : PathPart('') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api		= $c->model('API');
    
    $api->stash_result(
        $c, ['trackers', $id ],
        method  => 'PUT',
        body    => $c->req->params
    );
	
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } else {
		$c->detach( '/form/redirect_ok', [ '/adminunity/tracker/index', {}, 'Alterado com sucesso!' ] );
    }
}

sub process_delete : Chained('base') : PathPart('remove') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'trackers', $id ], method => 'DELETE' );

    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    }
    else {
        $c->detach( '/form/redirect_ok', [ '/adminunity/tracker/index', {}, 'Inativado com sucesso!' ] );
    }
}

__PACKAGE__->meta->make_immutable;

1;