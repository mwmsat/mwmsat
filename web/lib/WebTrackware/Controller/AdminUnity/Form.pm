package WebTrackware::Controller::AdminUnity::Form;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/base') : PathPart('form') : CaptureArgs(0) {
}

__PACKAGE__->meta->make_immutable;

1;
