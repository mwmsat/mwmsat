package WebTrackware::Controller::AdminUnity::BusinessUnity;
use Moose;
use namespace::autoclean;
use DDP;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/base') : PathPart('client') : CaptureArgs(0) {
  my ($self, $c) = @_;


  my $api = $c->model('API');

  $api->stash_result($c, 'states');
  $c->stash->{select_states}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{states}}];

  $api->stash_result($c, 'cities');
  $c->stash->{select_cities}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{cities}}];
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, 'states');

  $c->stash->{select_states}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{states}}];

  $api->stash_result($c, ['business_units', $id],
    stash => 'business_unity_obj');

  $c->detach('/form/not_found', []) if $c->stash->{business_unity_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');
  $api->stash_result(
    $c, 'business_units/managed_by_me',
    params => {order => 'corporate_name'}
  );
}

__PACKAGE__->meta->make_immutable;

1;
