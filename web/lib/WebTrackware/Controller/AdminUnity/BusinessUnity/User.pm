package WebTrackware::Controller::AdminUnity::BusinessUnity::User;
use Moose;
use namespace::autoclean;
use POSIX;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/businessunity/object') : PathPart('user') :
  CaptureArgs(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, ['users', $id], stash => 'user_obj');

  my %ar = map { $_ => 1 } @{$c->stash->{user_obj}{role_ids}};
  $c->stash->{active_roles} = \%ar;

  $c->detach('/form/not_found', []) if $c->stash->{user_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    ['business_unity_users',],
    params => {business_unity_id => $c->stash->{business_unity_obj}->{id}}
  );
}

sub edit : Chained('object') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');


  $api->stash_result($c, ['trackers'],
    params => {business_unity_id => $c->stash->{business_unity_obj}->{id}});

  use DDP;p($c->stash->{trackers});


# 	use DDP; p $r;exit;
#     $c->stash->{roles} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{roles} } ];
}

sub add : Chained('base') : PathPart('new') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, 'states');
  $c->stash->{select_states}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{states}}];

  $api->stash_result($c, 'cities');
  $c->stash->{select_cities}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{cities}}];


#     trocar isso para sessão
  $c->stash->{business_unity_id} = $c->req->params->{business_unity_id};

}

__PACKAGE__->meta->make_immutable;

1;
