package WebTrackware::Controller::AdminUnity::TrackerCommandLayout;
use Moose;
use namespace::autoclean;
use URI;
use utf8;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/base') : PathPart('__tracker-command-layout') : CaptureArgs(0) {
}

sub index: Chained('base') : PathPart('') : Args(0) {
	my ( $self, $c ) = @_;
	
	$c->stash( template => 'trackercommandlayout/index.tt' );
	
	$c->detach( 'TrackerCommandLayout' => 'index' );
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
	my ( $self, $c, $id ) = @_;
	
	$c->stash->{obj_id} = $id;
}

sub edit : Chained('object') : PathPart('') : Args(0) {
	my ( $self, $c ) = @_;

	$c->stash( template => 'trackercommandlayout/edit.tt' );
	
	$c->detach( 'TrackerCommandLayout' => 'object', [ $c->stash->{obj_id} ] );
}

sub add : Chained('base') : PathPart('new') : Args(0) {
	my ( $self, $c, $id ) = @_;
	
	$c->stash( template => 'trackercommandlayout/add.tt' );
	
	$c->detach( 'TrackerCommandLayout' => 'add' );
}

__PACKAGE__->meta->make_immutable;

1;
