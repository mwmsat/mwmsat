package WebTrackware::Controller::AdminUnity::Vehicle;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/base') : PathPart('__vehicle') : CaptureArgs(0) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, 'states' );
    $c->stash->{select_states} =
      [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{states} } ];

    $api->stash_result( $c, 'cities' );
    $c->stash->{select_cities} =
      [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{cities} } ];

    $api->stash_result( $c, 'vehicle_brands' );
    $c->stash->{select_brands} =
      [ map { [ $_->{id}, $_->{brand} ] } @{ $c->stash->{vehicle_brands} } ];

    $api->stash_result( $c, 'fuel_types' );
    $c->stash->{select_fuel_types} =
      [ map { [ $_->{id}, $_->{type} ] } @{ $c->stash->{fuel_types} } ];

    $api->stash_result( $c, 'vehicle_colors' );
    $c->stash->{select_colors} =
      [ map { [ $_->{id}, $_->{color} ] } @{ $c->stash->{vehicle_colors} } ];

    $api->stash_result(
        $c,
        'customers',
        params => {
            business_unity_id => $c->stash->{session_business_unity}{id},
            order             => 'me.corporate_name'
        }
    );
    $c->stash->{select_customers} = [ map { [ $_->{id}, $_->{corporate_name} ] }
          @{ $c->stash->{customers} } ];

    $api->stash_result( $c, 'insurance_companies' );
    $c->stash->{select_insurance_companies} = [ map { [ $_->{id}, $_->{name} ] }
          @{ $c->stash->{insurance_companies} } ];
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'vehicles', $id ], stash => 'vehicle_obj' );

    $api->stash_result( $c, 'states' );
    $c->stash->{select_states} =
      [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{states} } ];

    $api->stash_result( $c, 'cities' );
    $c->stash->{select_cities} =
      [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{cities} } ];

    $api->stash_result( $c, 'vehicle_brands' );
    $c->stash->{select_brands} =
      [ map { [ $_->{id}, $_->{brand} ] } @{ $c->stash->{vehicle_brands} } ];

    $api->stash_result( $c, 'fuel_types' );
    $c->stash->{select_fuel_types} =
      [ map { [ $_->{id}, $_->{type} ] } @{ $c->stash->{fuel_types} } ];

    $api->stash_result( $c, 'vehicle_colors' );
    $c->stash->{select_colors} =
      [ map { [ $_->{id}, $_->{color} ] } @{ $c->stash->{vehicle_colors} } ];

    $api->stash_result(
        $c,
        'customers',
        params => {
            business_unity_id => $c->stash->{session_business_unity}{id},
            order             => 'me.corporate_name'
        }
    );
    $c->stash->{select_customers} = [ map { [ $_->{id}, $_->{corporate_name} ] }
          @{ $c->stash->{customers} } ];

    $api->stash_result(
        $c,
        'vehicle_fleets',
        params => {
            customer_id => $c->stash->{vehicle_obj}{customer}{id}
            ? $c->stash->{vehicle_obj}{customer}{id}
            : undef,
            business_unity_id => $c->stash->{session_business_unity}{id},
        }
    );
    $c->stash->{select_vehicle_fleets} =
      [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{vehicle_fleets} } ];

    $api->stash_result( $c, 'insurance_companies' );
    $c->stash->{select_insurance_companies} = [ map { [ $_->{id}, $_->{name} ] }
          @{ $c->stash->{insurance_companies} } ];

    $c->detach( '/form/not_found', [] ) if $c->stash->{vehicle_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;

    my $api    = $c->model('API');
    my $model  = $c->req->params->{model} || undef;
    my $plate  = $c->req->params->{plate} || undef;
    my $client = $c->req->params->{client} || undef;
    my $fleet  = $c->req->params->{fleet} || undef;

    $api->stash_result(
        $c,
        'vehicles',
        params => $c->req->params->{vehicle_fleet_id}
        ? {
            business_unity_id => $c->stash->{session_business_unity}{id},
            vehicle_fleet_id  => $c->req->params->{vehicle_fleet_id},
            model             => $model,
            plate             => $plate,
            client            => $client,
            fleet             => $fleet

          }
        : {
            business_unity_id => $c->stash->{session_business_unity}{id},
            model             => $model,
            plate             => $plate,
            client            => $client,
            fleet             => $fleet

        }
    );
}

sub edit : Chained('object') : PathPart('') : Args(0) {
}

sub add : Chained('base') : PathPart('new') : Args(0) {
}

sub position : Chained('base') : PathPart('position') : Args(0) {
    my ( $self, $c ) = @_;

    $c->stash( template => 'vehicleposition/index.tt' );

    $c->detach( 'VehiclePosition' => 'index' );
}

sub history : Chained('base') : PathPart('history') {
    my ( $self, $c, $vehicle_id ) = @_;

    $c->stash(
        template   => 'vehicleposition/history.tt',
        vehicle_id => $vehicle_id
    );

    $c->detach( 'VehiclePosition' => 'history' );
}

sub position_lbs : Chained('base') : PathPart('position_lbs') {
    my ( $self, $c, $imei ) = @_;

    $c->stash(
        template   => 'vehicleposition/position_lbs.tt',
        vehicle_id => $imei
    );

    $c->detach( 'VehiclePosition' => 'position_lbs' );
}

__PACKAGE__->meta->make_immutable;

1;
