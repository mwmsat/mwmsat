package WebTrackware::Controller::AdminUnity::Customer;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/base') : PathPart('__customer') : CaptureArgs(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, 'states');
  $c->stash->{select_states}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{states}}];

  $api->stash_result($c, 'cities');
  $c->stash->{select_cities}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{cities}}];
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, 'states');

  $c->stash->{select_states}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{states}}];

  $api->stash_result($c, ['customers', $id], stash => 'customer_obj');

  $c->detach('/form/not_found', []) if $c->stash->{customer_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    'customers',
    params => {
      business_unity_id => $c->stash->{session_business_unity}{id},
      is_active         => 1
    }
  );
}

sub edit : Chained('object') : PathPart('') : Args(0) {
}

sub add : Chained('base') : PathPart('new') : Args(0) {
}

sub manage_business_unity : Chained('object') : PathPart('manage') : Args(0) {
  my ($self, $c) = @_;

  $c->response->cookies->{'session_business_unity'} = {
    value => [
      $c->stash->{business_unity_obj}{id},
      $c->stash->{business_unity_obj}{corporate_name},
    ],
    expires => '+8h',
  };

  $c->response->redirect($c->uri_for('/admin/dashboard'));
}

__PACKAGE__->meta->make_immutable;

1;
