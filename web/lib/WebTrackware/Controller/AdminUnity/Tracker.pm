package WebTrackware::Controller::AdminUnity::Tracker;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/adminunity/base') : PathPart('tracker') : CaptureArgs(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    'business_units/managed_by_me',
    params => {order => 'corporate_name'}
  );
}


sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, ['trackers', $id], stash => 'tracker_obj');

  $c->detach('/form/not_found', []) if $c->stash->{tracker_obj}{error};
}

sub is_json : Chained('base') : PathPart('json') : Args(0) {
  my ($self, $c) = @_;
  $c->forward('index');
  $c->json(
    {
      trackers => $c->stash->{trackers} || [],
      allowed_commands => $c->stash->{allowed_commands},
    }
  );
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');
  $api->stash_result($c, [qw(command allowed)], stash => 'allowed_commands');

  my %params;

  $params{business_unity_id} = $c->stash->{session_business_unity}{id}
    || $c->req->params->{business_unity_id};


  return unless $params{business_unity_id};

  #somente clientes que o admin gerencia
  return
    unless grep { $_->{id} == $params{business_unity_id} }
    @{$c->stash->{business_units} || {}};

  if (exists $c->req->params->{search}) {
    if ( $c->req->params->{search_type} eq 'vehicle_id'
      && $c->req->params->{search})
    {

      $params{vehicle_id} = $c->req->params->{search};

    }
    elsif ($c->req->params->{search_type} eq 'code'
      && $c->req->params->{search})
    {

      $params{code} = $c->req->params->{search};

    }
  }

  $params{order} = 'imei';

  $api->stash_result($c, ['trackers'], params => {%params});
}

__PACKAGE__->meta->make_immutable;

1;
