package WebTrackware::Controller::TrackerSms;
use Moose;
use utf8;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST' }

sub base : Chained('/root') : PathPart('tracker-sms') : CaptureArgs(0) {
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ( $self, $c, $id ) = @_;
}


sub collector : Chained('base') : PathPart('collector') POST {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  $c->req->params->{created_at} = time();

  my $p = $c->req->params;

  my $message = $p->{message}
    or $self->status_bad_request( $c, message => 'Missing message' ),
    $c->detach;
  my $tracker_id = $p->{tracker_id}
    or $self->status_bad_request( $c, message => 'Missing tracker_id' ),
    $c->detach;

  $api->stash_result(
    $c,
    'sms_trackers/collector',
    method => 'POST',
    params => {
      tracker_id => $tracker_id,
      text       => $message
    }
  );

  if ( $c->stash->{error} ) {
    $self->status_bad_request( $c, message => $c->stash->{error} );
    return;
  }
  $self->status_ok( $c, entity => { 'Response' => 'OK' } );
}

__PACKAGE__->meta->make_immutable;

1;

