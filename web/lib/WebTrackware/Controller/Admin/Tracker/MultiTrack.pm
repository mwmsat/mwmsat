package WebTrackware::Controller::Admin::Tracker::MultiTrack;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/tracker/base') : PathPart(multitrack) :
  CaptureArgs(0) {

}

sub object : Chained(base) : PathPart('') CaptureArgs(1) {
  my ($self, $c, $id) = @_;
  $c->stash->{api} = $c->model('API');
  $c->stash->{api}->stash_result($c, ['multitrack', $id], stash => 'group');
  $c->detach('/form/not_found', []) if $c->stash->{group}{error};

}

sub tracker : Chained(object) : CaptureArgs(1) {
  my ($self, $c, $id) = @_;
  $c->stash->{api}->stash_result(
    $c,
    ['multitrack', $c->stash->{group}->{id}, 'tracker', $id],
    stash => 'tracker'
  );
}

sub remove_tracker : Chained(tracker) PathPart('remove') Args(0) POST {
  my ($self, $c) = @_;

  $c->stash->{api}->stash_result(
    $c,
    [
      'multitrack', $c->stash->{group}->{id},
      'tracker',    $c->stash->{tracker}->{id}
    ],
    method => 'DELETE',
  );

  $c->res->redirect(
    $c->uri_for($self->action_for('view'), [$c->stash->{group}->{id}]));

}

sub view : Chained(object) : PathPart('') : Args(0) GET {
  my ($self, $c) = @_;
  $c->stash->{api}->stash_result(
    $c, ['trackers', 'search'],
    params => $c->req->params,
    stash  => 'search_result',
    method => 'POST'
  ) if $c->req->params->{query};
}

sub remove : Chained(object) : Args(0) POST {
  my ($self, $c) = @_;
  $c->stash->{api}->stash_result(
    $c, ['multitrack', $c->stash->{group}->{id}, 'remove'],
    method => 'DELETE',
    params => $c->req->params
  );

  $c->res->redirect($c->uri_for($self->action_for('root')));

}

sub add : Chained(object) : PathPart('') : Args(0) POST {
  my ($self, $c) = @_;

  $c->stash->{api}->stash_result(
    $c, ['multitrack', $c->stash->{group}->{id}, 'add'],
    method => 'POST',
    params => $c->req->params
  );

  if ($c->stash->{error}) {
    $c->detach('/form/redirect_error', []);
  }


  $c->res->redirect(
    $c->uri_for($c->action, $c->req->captures, @{$c->req->args}));
}


sub root : Chained(base) : PathPart('') Args(0) GET {
  my ($self, $c) = @_;
  my $api = $c->model('API');

  $api->stash_result($c, ['multitrack'], method => 'GET');

}

sub create : Chained(base) PathPart(new) : Args(0) {
  my ($self, $c) = @_;

  return unless $c->req->method eq 'POST';

  my $api = $c->model('API');
  $api->stash_result(
    $c, ['multitrack'],
    stash  => 'group',
    method => 'POST',
    params => $c->req->params
  );
  $c->res->redirect(
    $c->uri_for_action($self->action_for('view'), [$c->stash->{group}->{id}]));

}


1;
