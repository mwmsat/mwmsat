package WebTrackware::Controller::Admin::BusinessUnity;
use Moose;
use namespace::autoclean;
use DDP;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('business_unity') : CaptureArgs(0)
{
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  $api->stash_result( $c, 'states' );
  $c->stash->{select_states} =
    [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{states} } ];

  $api->stash_result( $c, 'cities' );
  $c->stash->{select_cities} =
    [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{cities} } ];
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ( $self, $c, $id ) = @_;

  my $api = $c->model('API');

  $api->stash_result( $c, 'states' );

  $c->stash->{select_states} =
    [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{states} } ];

  $api->stash_result(
    $c,
    [ 'business_units', $id ],
    stash => 'business_unity_obj'
  );

  $c->detach( '/form/not_found', [] ) if $c->stash->{business_unity_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  $api->stash_result( $c, 'business_units' );

  my $p = $c->stash->{business_units};
}

sub edit : Chained('object') : Args(0) {
  my ( $self, $c ) = @_;
  $c->stash->{template} = 'admin/businessunity/edit.tt';
}

sub add : Chained('base') : PathPart('new') : Args(0) {
}

sub pre_registration : Chained('object') : PathPart('pre-registration') Args(0)
{
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  $c->stash->{template} = 'admin/businessunity/pre-register.tt';
  
  $api->stash_result($c, 'network_suppliers', params => {order => 'me.name'});
  
  $api->stash_result(
    $c,
    [
      'business_units', $c->stash->{business_unity_obj}->{id},
      'pre-registration'
    ],
    stash => 'preregs'
  );

  return unless $c->req->method eq 'POST';

  $api->stash_result(
    $c,
    [
      'business_units', $c->stash->{business_unity_obj}->{id},
      'pre-registration'
    ],
    params => $c->req->params,
    stash  => 'preregs',
    method => 'POST',
  );


}

sub manage_business_unity : Chained('object') : PathPart('manage') : Args(0) {
  my ( $self, $c ) = @_;

  $c->response->cookies->{'session_business_unity'} = {
    value => [
      $c->stash->{business_unity_obj}{id},
      $c->stash->{business_unity_obj}{corporate_name},
    ],
    expires => '+8h',
  };

  $c->response->redirect( $c->uri_for('/admin/dashboard') );
}

__PACKAGE__->meta->make_immutable;

1;
