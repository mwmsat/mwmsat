package WebTrackware::Controller::Admin::Tools::PositionReport;

use utf8;
use Moose;
use namespace::autoclean;
use DateTime::Format::CLDR;
use Path::Tiny;
use Spreadsheet::GenerateXLSX qw(generate_xlsx);

BEGIN { extends 'Catalyst::Controller' }
my $fmt = DateTime::Format::CLDR->new(pattern => q{d/MM/y HH:mm});

sub base : Chained('/admin/tools/base') : PathPart('position-report') :
  CaptureArgs(0) {
  pop->stash->{template} = 'admin/tools/position-report.tt';
}

sub root : Chained(base) : PathPart('') Args(0) GET {
  my ($self, $c) = @_;

}

sub list : Chained(base) : PathPart('') : Args(0) POST {
  my ($self, $c) = @_;

  my $api    = $c->model('API');
  my $params = +{%{$c->req->params}};

  $params->{$_} = eval { $fmt->parse_datetime($params->{$_}) . '' }
    for grep { $params->{$_} } qw(tt-start tt-end);

  $api->stash_result($c, [qw(tracker_positions report)], params => $params,);
  $c->stash->{download_link}
    = $c->uri_for_action($self->action_for('download'), $c->req->params)
    ->as_string;
}

sub download : Chained(base) : Args(0) GET {
  my ($self, $c) = @_;
  $c->forward('list');
  $c->forward('generate_file');
}

sub generate_file : Private {
  my ($self, $c) = @_;
  my $tmp = Path::Tiny->tempfile();
  
  generate_xlsx(
    $tmp->stringify,
    [
      [qw( Data Endereço Latitude Longitude Status), 'Bateria(%)'],
      map { [@{$_}{qw(event_date address lat lng status_line battery_level)}] }
        @{$c->stash->{report}->{positions} || []}
    ]
  );

  my $filename = join(
    '-',
    grep { defined && length } (
           $c->stash->{report}->{tracker}->{code}
        || $c->stash->{report}->{tracker}->{phone_number}
    ),
    $c->req->params->{'tt-start'},
    $c->req->params->{'tt-end'}
  ) . '.xlsx';

  $c->res->body($tmp->filehandle);
  $c->res->headers->content_type('application/vnd.ms-excel');
  $c->res->headers->content_length($tmp->stat->size);
  $c->res->headers->last_modified($tmp->stat->mtime);
  $c->res->header('Content-Disposition' => "attachment; filename=$filename");

}

1;
