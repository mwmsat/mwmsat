package WebTrackware::Controller::Admin::Tools::Report;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/tools/base') : PathPart(report) : CaptureArgs(0) {
  pop->stash->{template} = 'admin/tools/report.tt';
}

sub root : Chained(base) : PathPart('') Args(0) GET {
  my ($self, $c) = @_;

}

sub list : Chained(base) : PathPart('') : Args(0) POST {
  my ($self, $c) = @_;
  my $api = $c->model('API');

  $api->stash_result(
    $c, ['trackers/filter'],
    method => 'POST',
    params => {trackers => $c->req->params->{trackers}}
  );

}

1;
