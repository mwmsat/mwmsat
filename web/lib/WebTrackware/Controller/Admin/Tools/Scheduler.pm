package WebTrackware::Controller::Admin::Tools::Scheduler;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/tools/base') : PathPart(scheduler) :
  CaptureArgs(0) { }

sub root : Chained(base) : PathPart('') Args(0) GET {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result($c, ['business_units'], stash => 'units');
}

sub view : Chained(base) : PathPart('') Args(1) GET {
  my ($self, $c, $id) = @_;
  my $api = $c->model('API');
  $api->stash_result($c, ['scheduler', $id], method => 'GET',);
  use DDP;
  p($c->stash);
}

sub create : Chained(base) : PathPart('') Args(0) POST {
  my ($self, $c) = @_;

  my $api = $c->model('API');
  $api->stash_result(
    $c, 'trackers/area-notify',
    method => 'POST',
    params => $c->req->params
  );
}

sub list : Chained(base) : Args(0) GET {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result($c, 'scheduler', method => 'GET',);
  use DDP;
  p($c->stash);
}

1;
