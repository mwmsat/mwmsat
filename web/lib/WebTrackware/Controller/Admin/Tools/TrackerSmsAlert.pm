package WebTrackware::Controller::Admin::Tools::TrackerSmsAlert;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/tools/base') : PathPart('tracker-sms-alert') :
  CaptureArgs(0) {
  my ($self, $c) = @_;
  $c->stash->{template} = 'admin/tools/tracker_sms_alert.tt';
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;
  my $api = $c->model('API');
  $api->stash_result(
    $c,
    ['tracker-sms-alert', $id],
    stash => 'tracker_sms_alert'
  );
  $c->stash->{obj_id} = $id;
}


sub remove : Chained(object) : PathPart('') Args(0) {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result(
    $c,
    ['tracker-sms-alert', $c->stash->{obj_id}],
    method => 'DELETE',
  );
  $c->res->redirect($c->uri_for_action($self->action_for('root')));
}

sub root : Chained(base) : PathPart('') Args(0) GET {
  my ($self, $c) = @_;

  my $api = $c->model('API');
  $api->stash_result($c, 'tracker-sms-alert', method => 'GET',);
 }

sub create : Chained(base) : PathPart('') Args(0) POST {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result(
    $c, 'tracker-sms-alert',
    method => 'POST',
    params => $c->req->params
  );
  $api->stash_result($c, 'tracker-sms-alert', method => 'GET',);
  return if $c->stash->{form_error};
  $c->res->redirect($c->uri_for_action($self->action_for('root')));
}

use JSON qw(encode_json);

sub autocomplete : Chained(base) Args(0) GET {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result(
    $c, 'trackers/search',
    method => 'POST',
    params => $c->req->params
  );
  $c->res->content_type('application/json');
  $c->res->body(encode_json($c->stash->{results}));

}


1;
