package WebTrackware::Controller::Admin::VehiclePosition;
use Moose;
use utf8;
use JSON::XS;
use namespace::autoclean;
use DateTime;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('vehicle_position') :
  CaptureArgs(0) {
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ( $self, $c, $id ) = @_;
}

sub index : Chained('base') : PathPart('') : Args(1) {
  my ( $self, $c, $vehicle_id ) = @_;

  $c->stash->{vehicle_id} = $vehicle_id;

  $self->get_positions( $c, $vehicle_id );
}

sub get_positions : Chained('base') : PathPart('get_positions') {
  my ( $self, $c, $vehicle_id ) = @_;

  my $api = $c->model('API');
  my @final_pos;

  $api->stash_result(
    $c,
    'trackers',
    params => {
      vehicle_id => $vehicle_id,
      status     => 2
    }
  );

  my $date = DateTime->now();

  $api->stash_result(
    $c,
    'tracker_positions',
    params => {
      tracker_id => $c->stash->{trackers}[0]{id},
      vehicle_id => $vehicle_id,

      # 			date		=> $date->format_cldr('y-M-d')
      limit     => 30,
      positions => 1
    }
  );
  my @positions;

  foreach my $position ( @{ $c->stash->{tracker_positions} } ) {
    my $point_id =
      substr( $position->{lat}, 0, 8 ) . '&' . substr( $position->{lng}, 0, 8 );

    $api->stash_result(
      $c,
      'position_cache',
      params => {
        point_id => $point_id,
      }
    );

    push(
      @positions,
      {
        'address'    => $c->stash->{position_cache}[0]{address},
        'event_date' => $position->{event_date},
        'speed'      => $position->{speed},
      }
    );
  }

  $c->stash->{positions} = \@positions;
}

sub search : Chained('base') : PathPart('search') : Args(0) {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');
  my $p   = $c->req->params;

  my $pt = decode_json( $c->req->params->{points} );
  my $gis_polyline = [ map { join ',', @$_ } @$pt ];

  $api->stash_result(
    $c,
    'vehicle_routes',
    params => {
      gis_polyline => $gis_polyline,
      brand => $c->req->params->{brand} ? $c->req->params->{brand} : undef,
      end_age => $c->req->params->{end_age} ? $c->req->params->{end_age}
      : undef,
      start_age => $c->req->params->{start_age} ? $c->req->params->{start_age}
      : undef,
      gender => $c->req->params->{gender} ? $c->req->params->{gender} : undef,
      distance => $c->req->params->{distance}
      ? $c->req->params->{distance} * 1000
      : 500000    #bigger than 500km
    }
  );

  $c->stash->{without_wrapper} = 1;

  if ( $c->stash->{associateds} ) {
    $c->stash->{search_result} = {
      'ids'       => $c->stash->{associateds},
      'count'     => scalar keys %{ $c->stash->{associateds} },
      'positions' => $gis_polyline,
    };
  }
  else {
    $c->stash->{search_result} = 0;
  }

}

sub get_real_time_position : Chained('base') :
  PathPart('get_real_time_position') : Args(0) {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  my $vehicle_id = $c->req->params->{vehicle_id};

  if ( !$vehicle_id ) {
    $c->res->body( { 'error' => 'Wrong parameters' } );
  }

  $api->stash_result(
    $c,
    'tracker_positions/get_last_position',
    params => {
      vehicle_id => $vehicle_id
    }
  );

  if ( $c->stash->{real_time_position} ) {
    my $position = $c->stash->{real_time_position};
    my @time     = split ' ', $position->[0]{event_date};
    my $date     = $self->format_date_to_human( $time[0] );

    my $point = {
      lat   => $position->[0]{lat},
      lng   => $position->[0]{lng},
      speed => $position->[0]{speed},
      date  => $date,
      hour  => $time[1]
    };

    $c->res->header( 'content-type', 'application/json;charset=UTF-8' );
    $c->res->body( encode_json($point) );
  }
  else {
    $c->res->body(0);
  }
}

sub format_date_to_human {
  my ( $self, $date ) = @_;

  my ( $y, $m, $d ) = $date =~ m/^(\d{4})-(\d{1,2})-(\d{1,2})$/;

  return $d . '/' . $m . '/' . $y;
}

__PACKAGE__->meta->make_immutable;

1;
