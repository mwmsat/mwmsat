package WebTrackware::Controller::Admin::User;
use Moose;
use namespace::autoclean;
use POSIX;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('user') : CaptureArgs(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, ['users', $id], stash => 'user_obj');

  my %ar = map { $_ => 1 } @{$c->stash->{user_obj}{role_ids}};
  $c->stash->{active_roles} = \%ar;

  $c->detach('/form/not_found', []) if $c->stash->{user_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c, 'users',
    params => {
      order             => 'me.name',
      business_unity_id => $c->stash->{session_business_unity}
      ? $c->stash->{session_business_unity}{id}
      : undef
    }
  );
}

sub edit : Chained('object') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, 'roles', params => {admin => 1});
  $api->stash_result($c, 'business_units');

  $api->stash_result($c, 'states');
  $c->stash->{select_states}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{states}}];

  $api->stash_result($c, 'cities');
  $c->stash->{select_cities}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{cities}}];


# 	use DDP; p $r;exit;
#     $c->stash->{roles} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{roles} } ];
}

sub add : Chained('base') : PathPart('new') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, 'states');
  $c->stash->{select_states}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{states}}];

  $api->stash_result($c, 'cities');
  $c->stash->{select_cities}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{cities}}];


#     trocar isso para sessão
  $c->stash->{business_unity_id} = $c->req->params->{business_unity_id};

}

sub remove : Chained('object') : Args(0) {
  my ($self, $c) = @_;
  return unless $c->req->method eq 'POST';
  $c->forward('/admin/form/user/process_delete', [$c->stash->{user_obj}->{id}]);
}


__PACKAGE__->meta->make_immutable;

1;
