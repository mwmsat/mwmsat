package WebTrackware::Controller::Admin::Form::User;
use Moose;
use namespace::autoclean;
use DateTime;
use JSON::XS;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/form/base') : PathPart('') : CaptureArgs(0) {
  my ($self, $c) = @_;
}

sub process : Chained('base') : PathPart('user') : Args(0) {
  my ($self, $c) = @_;

  my $api  = $c->model('API');
  my $form = $c->model('Form');

  my $params = {%{$c->req->params}};

  $form->only_number($params, 'mobile_phone');


  $api->stash_result($c, 'users', method => 'POST', body => $params);

  if ($c->stash->{error}) {
    $c->detach('/form/redirect_error', []);
  }
  else {

    if ($c->req->params->{business_unity_id}) {
      $api->stash_result(
        $c,
        'business_unity_users',
        method => 'POST',
        body   => {
          user_id           => $c->stash->{id},
          is_active         => 1,
          business_unity_id => $c->req->params->{business_unity_id}
        }
      );
    }

    if ($c->stash->{error}) {
      $c->detach('/form/redirect_error', []);
    }

    $c->detach('/form/redirect_ok',
      ['/admin/user/index', {}, 'Cadastrado com sucesso!']);
  }
}

sub process_edit : Chained('base') : PathPart('user') : Args(1) {
  my ($self, $c, $id) = @_;

  my $api    = $c->model('API');
  my $params = {%{$c->req->params}};

  my @r;
  if (ref $params->{roles} eq 'ARRAY') {

    foreach my $r (@{$params->{roles}}) {
      push(@r, {user_id => $id, role_id => $r});
    }

  }
  else {
    push(@r, {user_id => $id, role_id => $params->{roles}});
  }

  my $roles = encode_json(\@r);


  $params->{roles}                  = $roles;
  $params->{change_roles}           = 1;
  $params->{managed_business_units} = encode_json(
    [
      grep {defined} (
        ref $params->{managed_business_units} eq 'ARRAY'
        ? @{$params->{managed_business_units}}
        : $params->{managed_business_units}
      )
    ]
  );

  $api->stash_result($c, ['users', $id], method => 'PUT', body => $params);

  if ($c->stash->{error}) {
    $c->detach('/form/redirect_error', []);
  }
  else {
    $c->detach('/form/redirect_ok',
      ['/admin/user/index', {}, 'Alterado com sucesso!']);
  }
}

sub process_delete : Chained('base') : PathPart('remove_user') : Args(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');
  $c->model('API')->stash_result(
    $c, ['users', $id],
    params => {password => $c->req->params->{password}},
    method => 'DELETE'
  );
  
  if ($c->stash->{error}) {
    $c->detach('/form/redirect_error', []);
  }
  else {
    $c->detach('/form/redirect_ok',
      ['/admin/user/index', {}, 'Removido com sucesso!']);
  }
}

__PACKAGE__->meta->make_immutable;

1;
