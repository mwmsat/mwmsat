package WebTrackware::Controller::Admin::Form::BusinessUnity;
use Moose;
use namespace::autoclean;
use DateTime;
use JSON::XS;
use utf8;
BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/form/base') : PathPart('business_unity') :
  CaptureArgs(0) {
  my ( $self, $c ) = @_;
}

sub process : Chained('base') : PathPart('') Args(0) : POST {
  my ( $self, $c ) = @_;

  my $api  = $c->model('API');
  my $form = $c->model('Form');

  my $params = { %{ $c->req->params } };

  my @fields;

  push( @fields, 'register', 'phone', 'phone_2', 'postal_code' );

  $params->{domain} = lc $params->{domain};

  $form->only_number( $params, @fields );

  $api->stash_result(
    $c, 'business_units/complete',
    method => 'POST',
    body   => $params
  );

  if ( $c->stash->{error} ) {
    $c->detach( '/form/redirect_error', [] );
  }
  else {
    $c->detach( '/form/redirect_ok',
      [ '/admin/businessunity/index', {}, 'Cadastrado com sucesso!' ] );
  }
}

sub process_edit : Chained('base') : PathPart('') : Args(1) : POST {
  my ( $self, $c, $id ) = @_;

  my $api  = $c->model('API');
  my $form = $c->model('Form');
  my $params = { %{ $c->req->params } };

  my @fields;

  push( @fields, 'register', 'phone', 'phone_2', 'postal_code' );

  $form->only_number( $params, @fields );

  $api->stash_result(
    $c, [ 'business_units', $id ],
    stash  => 'business_obj',
    method => 'GET'
  );

  $api->stash_result(
    $c, [ 'addresses', $c->stash->{business_obj}{address}{id} ],
    method => 'PUT',
    body   => $params
  );

  if ( $c->stash->{error} ) {

    $c->detach( '/form/redirect_error', [] );
  }
  else {

    $api->stash_result(
      $c, [ 'business_units', $id ],
      method => 'PUT',
      body   => $params
    );

    if ( $c->stash->{error} ) {
      $c->detach( '/form/redirect_error', [] );
    }

    $c->detach( '/form/redirect_ok',
      [ '/admin/businessunity/index', {}, 'Alterado com sucesso!' ] );
  }
}

sub process_delete : Chained('base') : PathPart('remove_business_unity') :
  Args(1) {
  my ( $self, $c, $id ) = @_;

  my $api = $c->model('API');

  $api->stash_result( $c, [ 'business_unitys', $id ], method => 'DELETE' );

  if ( $c->stash->{error} ) {
    $c->detach( '/form/redirect_error', [] );
  }
  else {
    $c->detach( '/form/redirect_ok',
      [ '/admin/business_unity/index', {}, 'Removido com sucesso!' ] );
  }
}

sub process_associated : Chained('base') : PathPart('process_associated') :
  Args(0) {
  my ( $self, $c ) = @_;
  use DDP;
  my $api    = $c->model('API');
  my $params = $c->req->args;
  my @rows;

  if ( exists $params->[0]{vehicles} ) {
    $params->[0]{vehicles} = [ $params->[0]{vehicles} ]
      if ref $params->[0]{vehicles} ne 'ARRAY';
    my $record;

    foreach my $iten ( @{ $params->[0]{vehicles} } ) {

      $record = {
        vehicle_id        => $iten,
        business_unity_id => $params->[0]{business_unity_id},
      };

      push( @rows, $record );
    }
  }

  $api->stash_result(
    $c,
    'business_unity_vehicles',
    method => 'POST',
    params => {
      json => encode_json( \@rows )
    }
  );

  if ( $c->stash->{error} ) {
    $c->detach( '/form/redirect_error', [] );
  }
  else {
    my $uri;

    $api->stash_result(
      $c,
      'invitations',
      params => {
        business_unity_id => $params->[0]{business_unity_id}
      }
    );

    if ( !scalar @{ $c->stash->{invitations} || [] } ) {
      $uri = $c->uri_for_action(
        '/admin/invitation/add',
        {
          'business_unity_id' => $params->[0]{business_unity_id}
        }
      );
    }
    else {
      $api->stash_result(
        $c,
        'invitations/send',
        stash  => 'send_invitation',
        params => {
          associateds       => encode_json( \@{ $params->{vehicles} } ),
          business_unity_id => $params->[0]{business_unity_id}
        }
      );

      if ( $c->stash->{send_invitation}{error} ) {
        $c->stash->{error} = $c->stash->{send_invitation}{error};
        $c->detach( '/form/redirect_error', [] );
      }

      $uri = $c->uri_for_action(
        '/admin/business_unity/index',
        {
          'business_unity_id' => $params->[0]{business_unity_id}
        }
      );
    }

    $c->res->redirect( $uri->as_string );
  }
}

sub process_delete_assoc : Chained('base') : PathPart('remove_associated') :
  Args(0) {
  my ( $self, $c, $association_id ) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    [ 'business_unity_vehicles', $c->req->params->{id} ],
    method => 'DELETE'
  );

  if ( $c->stash->{error} ) {
    $c->detach( '/form/redirect_error', [] );
  }
  else {
    $c->detach(
      '/form/redirect_ok2',
      [
        '/admin/business_unity/list_associated',
        [ $c->req->params->{business_unity_id} ],
        {},
        'Removido com sucesso!'
      ]
    );
  }
}

sub process_activate : Chained('base') : PathPart('activate') : Args(0) {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');

  $api->stash_result(
    $c,
    [ 'business_unity_vehicles', $c->req->params->{business_unity_vehicle} ],
    method => 'PUT',
    body   => {
      status => $c->req->params->{status}
    }
  );

  if ( $c->stash->{error} ) {
    $c->detach( '/form/redirect_error', [] );
  }

  $api->stash_result(
    $c,
    [ 'vehicle_invitations', $c->req->params->{vehicle_invitation} ],
    method => 'PUT',
    body   => {
      status => $c->req->params->{status}
    }
  );

  if ( $c->stash->{error} ) {
    $c->detach( '/form/redirect_error', [] );
  }
  else {
    my $message;

    if ( $c->req->params->{status} == 5 ) {

      $api->stash_result(
        $c,
        'vehicle_tokens',
        method => 'POST',
        body   => {
          vehicle_id => $c->req->params->{vehicle_id},
          user_id    => $c->req->params->{user_id}
        }
      );

      if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
      }

      my $now = DateTime->now();
      $api->stash_result(
        $c,
        'instalation_kits',
        method => 'POST',
        body   => {
          sent_at             => $now,
          name                => $c->req->params->{name},
          email               => $c->req->params->{email},
          vehicle_id          => $c->req->params->{vehicle_id},
          business_unity_id   => $c->req->params->{business_unity_id},
          business_unity_name => $c->req->params->{business_unity_name},
          token               => $c->stash->{vehicle_tokens}{token},
        }
      );

      $message = 'Participação aprovada.';

    }
    elsif ( $c->req->params->{status} == 7 ) {

      $message = 'Participação recusada.';

    }

    if ( $c->stash->{error} ) {
      $c->detach( '/form/redirect_error', [] );
    }

    $c->detach(
      '/form/redirect_ok2',
      [
        '/admin/business_unity/list_associated',
        [ $c->req->params->{business_unity_id} ],
        {}, $message
      ]
    );
  }
}

sub save_perimeter : PrivatePath('save_perimeter') {
  my ( $self, $c, $positions, $business_unity_id ) = @_;

  my $api = $c->model('API');
  my $pos;

  if ( ref $positions ne 'ARRAY' ) {
    $pos->[0] = $positions;
  }
  else {
    $pos = $positions;
  }

  my $record;
  my @rows;

  foreach my $iten ( @{$pos} ) {
    $record = {
      polyline          => $iten,
      business_unity_id => $business_unity_id,
      gis_polyline      => $iten,
    };

    push( @rows, $record );
  }

  $api->stash_result(
    $c,
    'business_unity_perimeters',
    method => 'POST',
    params => {
      json => encode_json( \@rows )
    }
  );

}

__PACKAGE__->meta->make_immutable;

1;
