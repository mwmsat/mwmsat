package WebTrackware::Controller::Admin::Vehicle;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('vehicle') : CaptureArgs(0) {
	my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');
    
    $api->stash_result( $c, 'states' );
    $c->stash->{select_states} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{states} } ];
    
    $api->stash_result( $c, 'cities' );
    $c->stash->{select_cities} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{cities} } ];
    
    $api->stash_result( $c, 'vehicle_brands' );
    $c->stash->{select_brands} = [ map { [ $_->{id}, $_->{brand} ] } @{ $c->stash->{vehicle_brands} } ];
    
    $api->stash_result( $c, 'fuel_types' );
    $c->stash->{select_fuel_types} = [ map { [ $_->{id}, $_->{type} ] } @{ $c->stash->{fuel_types} } ];
    
    $api->stash_result( $c, 'vehicle_colors' );
    $c->stash->{select_colors} = [ map { [ $_->{id}, $_->{color} ] } @{ $c->stash->{vehicle_colors} } ];
    
    $api->stash_result( 
		$c, 'vehicle_fleets',
		params => { 
			business_unity_id =>  $c->stash->{session_business_unity}{id}
		}
	);
    $c->stash->{select_vehicle_fleets} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{vehicle_fleets} } ];
    
    $api->stash_result( $c, 'insurance_companies' );
    $c->stash->{select_insurance_companies} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{insurance_companies} } ];
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');
    
    $api->stash_result( $c, 'states' );
    $c->stash->{select_states} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{states} } ];
    
    $api->stash_result( $c, 'cities' );
    $c->stash->{select_cities} = [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{cities} } ];
    
    $api->stash_result( $c, 'vehicle_brands' );
    $c->stash->{select_brands} = [ map { [ $_->{id}, $_->{brand} ] } @{ $c->stash->{vehicle_brands} } ];
    
    $api->stash_result( $c, 'vehicle_colors' );
    $c->stash->{select_colors} = [ map { [ $_->{id}, $_->{color} ] } @{ $c->stash->{vehicle_colors} } ];
    
    $api->stash_result( $c, [ 'vehicles', $id ], stash => 'vehicle_obj' );

    $c->detach( '/form/not_found', [] ) if $c->stash->{vehicle_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;
    
    my $api = $c->model('API');

    $api->stash_result( 
		$c, 'vehicles',
		params => {
			business_unity_id 	=> $c->stash->{session_business_unity}{id},
			order				=> 'car_plate'
		}
	);
}

sub edit : Chained('object') : PathPart('') : Args(0) {
}

sub add : Chained('base') : PathPart('new') : Args(0) {
}

sub get_vehicle_models : Chained('base') : PathPart('get_vehicle_models') : Args(0) {
    my ( $self, $c ) = @_;

    my $api = $c->model('API');

    $api->stash_result(
        $c,
        'vehicle_models',
        params => {
            vehicle_brand_id => $c->req->params->{vehicle_brand_id},
            order            => 'model'
        }
    );

    $c->stash(
        select_vehicle_models => [ map { [ $_->{id}, $_->{model} ] } @{ $c->stash->{vehicle_models} } ],
        without_wrapper       => 1,
        template => 'admin/vehicle/vehicle_models.tt'
    );
}

sub search_by_plate : Chained('base') : PathPart('search_by_plate') : Args(1) {
	my ( $self, $c, $car_plate ) = @_;

    my $api = $c->model('API');
    
    $api->stash_result(
        $c,
        'vehicles',
        params => {
            car_plate => $car_plate
        }
    );
    
    $c->stash(
        without_wrapper       => 1,
        template => 'auto/search_by_plate.tt'
    );
}

sub history : Chained('base') : PathPart('history') {
	my ( $self, $c, $vehicle_id ) = @_;
	
	$c->stash(
		template 	=> 'vehicleposition/history.tt',
		vehicle_id 	=> $vehicle_id
	);
	
	$c->detach( 'VehiclePosition' => 'history');
}

__PACKAGE__->meta->make_immutable;

1;