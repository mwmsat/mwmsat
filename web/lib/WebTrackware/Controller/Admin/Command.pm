package WebTrackware::Controller::Admin::Command;
use Moose;
use namespace::autoclean;
use URI;
use utf8;
use DDP;
BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('command') : CaptureArgs(0) {
  my ( $self, $c ) = @_;
  my $api = $c->model('API');
  $api->stash_result( $c, 'roles' );

}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ( $self, $c ) = @_;
  my $api = $c->model('API');
  $api->stash_result( $c, 'command', { stash => 'commands' } );
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ( $self, $c, $id ) = @_;

  my $api = $c->model('API');
  $api->stash_result( $c, [ 'command', $id ], stash => 'command' );
  $c->stash->{obj_id} = $id;
}

sub send : Chained('object') : Args(1) POST {
  my ( $self, $c, $tracker_id ) = @_;
  my $api = $c->model('API');
  $api->stash_result(
    $c,
    [ 'command', $c->stash->{obj_id}, 'send', $tracker_id ],
    method => 'POST'
  );
  $c->res->status(200);
  $c->res->body('ok');
}

sub remove : Chained('object') : Args(0) GET {
  my ( $self, $c ) = @_;
  $c->stash->{template} = 'admin/command/remove.tt';
}

sub do_remove : Chained('object') : PathPart('remove') : Args(0) POST {
  my ( $self, $c ) = @_;
  $c->stash->{template} = 'admin/command/remove.tt';
  my $api = $c->model('API');
  $c->model('API')->stash_result(
    $c,
    [ 'command', $c->stash->{obj_id} ],
    params => { password => $c->req->params->{password} },
    stash  => 'removed',
    method => 'DELETE'
  );
  $c->stash->{template} = 'admin/command/removed.tt';
}

sub edit : Chained('object') : Args(0) {
  my ( $self, $c ) = @_;

  $c->stash->{command_roles_map} = { map { $_->{role}{id} => 1 }
      @{ $c->stash->{command}->{command_roles} || [] } };

}

sub do_edit : Chained('object') : PathPart('edit') : Args(0) : POST {
  my ( $self, $c, $id ) = @_;
  $c->stash->{template} = 'admin/command/edit.tt';
  my $api = $c->model('API');
  $api->stash_result(
    $c,
    [ 'command', $c->stash->{obj_id} ],
    params => $c->req->params,
    method => 'PUT'
  );
  $api->stash_result(
    $c,
    [ 'command', $c->stash->{obj_id} ],
    stash => 'command'
  );
}

sub add : Chained('base') : PathPart('') Args(0) POST {
  my ( $self, $c, $id ) = @_;
  $c->stash->{template} = 'admin/command/index.tt';
  my $api = $c->model('API');
  $api->stash_result(
    $c,
    'command',
    params => $c->req->params,
    method => 'POST'
  );

  $api->stash_result( $c, 'command', { stash => 'commands' } );

}

sub render_index : Private {
  my ( $self, $c ) = @_;
  my $api = $c->model('API');
  $api->stash_result( $c, 'command', { stash => 'commands' } );
  $c->stash->{template} = 'admin/command/index.tt';

}

__PACKAGE__->meta->make_immutable;

1;
