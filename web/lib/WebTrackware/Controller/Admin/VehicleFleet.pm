package WebTrackware::Controller::Admin::VehicleFleet;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('vehicle_fleet') : CaptureArgs(0) {
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');
    
    $api->stash_result( $c, [ 'vehicle_fleets', $id ], stash => 'vehicle_fleet_obj' );

    $c->detach( '/form/not_found', [] ) if $c->stash->{vehicle_fleet_obj}{error};
}

sub index : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;
    
    my $api = $c->model('API');

    $api->stash_result( 
		$c, 'vehicle_fleets',
		params => {
			business_unity_id 	=> $c->stash->{session_business_unity}{id},
			order				=> 'name'
		}
	);
}

sub edit : Chained('object') : PathPart('') : Args(0) {
}

sub add : Chained('base') : PathPart('new') : Args(0) {
}

__PACKAGE__->meta->make_immutable;

1;