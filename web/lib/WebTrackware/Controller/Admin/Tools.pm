package WebTrackware::Controller::Admin::Tools;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('tools') : CaptureArgs(0) { }

__PACKAGE__->meta->make_immutable;

sub index : Chained('base') : PathPart('') : Args(0) {
  my ( $self, $c ) = @_;
}

sub multimessage : Chained(base) : Args(0) GET {
  my ( $self, $c ) = @_;
  my $api = $c->model('API');

  $api->stash_result( $c, ['business_units'], stash => 'units' );
}

sub send_multimessage : Chained(base) : PathPart(multimessage) Args(0) POST {
  my ( $self, $c ) = @_;
  use DDP;
  p( $c->req->params );

  my $api = $c->model('API');
  $api->stash_result(
    $c,
    'trackers/multimessage',
    method => 'POST',
    params => $c->req->params
  );

  $c->stash->{template} = 'admin/tools/send_multimessage.tt';
}

# sub area_notify : Chained(base) : PathPart(scheduler) Args(0) GET {
#   my ( $self, $c ) = @_;
#   my $api = $c->model('API');

#   $api->stash_result( $c, ['business_units'], stash => 'units' );
# }


# sub send_area_notify : Chained(base) : PathPart(scheduler) Args(0) POST {
#   my ( $self, $c ) = @_;
#   use DDP;
#   p( $c->req->params );
#   my $api = $c->model('API');
#   $api->stash_result(
#     $c,
#     'trackers/area-notify',
#     method => 'POST',
#     params => $c->req->params
#   );

#   $c->stash->{template} = 'admin/tools/send_area_notify.tt';
# }

=pod

sub multimessage : Chained('base') : POST {
  my ( $self, $c ) = @_;

  my $api = $c->model('API');
  $api->stash_result(
    $c,
    'sms_trackers/multimessage',
    method => 'POST',
    params => $c->req->params
  );

  if ( $c->stash->{error} ) {
    $self->status_bad_request( $c, message => $c->stash->{error} );
    return;
  }
  $self->status_ok( $c, entity => { 'Response' => 'OK' } );

}

sub area_notify : Chained('base') : PathPart(area-notify) POST {
  
  my ( $self, $c ) = @_;

  my $api = $c->model('API');
  $api->stash_result(
    $c,
    'sms_trackers/area-notify',
    method => 'POST',
    params => $c->req->params
  );

  if ( $c->stash->{error} ) {
    $self->status_bad_request( $c, message => $c->stash->{error} );
    return;
  }

  $self->status_ok( $c, entity => { 'Response' => 'OK' } );
}
=cut

1;
