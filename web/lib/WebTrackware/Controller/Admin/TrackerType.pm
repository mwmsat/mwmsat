package WebTrackware::Controller::Admin::TrackerType;
use Moose;
use namespace::autoclean;
use URI;
use utf8;
use DDP;
BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('tracker-type') : CaptureArgs(0) {
  my ( $self, $c ) = @_;
  my $api = $c->model('API');
  $c->stash->{template} = 'admin/trackertype/index.tt';

}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ( $self, $c ) = @_;
  $c->forward('load_items');
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ( $self, $c, $id ) = @_;
  $c->forward('load_items');

  my $api = $c->model('API');
  $api->stash_result( $c, [ 'tracker-type', $id ], stash => 'tracker_type' );
  $c->stash->{obj_id} = $id;
}

sub view : Chained('object') : PathPart('') Args(0) {
  my ( $self, $c ) = @_;
}

sub remove : Chained('object') : Args(0) GET {
  my ( $self, $c ) = @_;
  $c->stash->{template} = 'admin/trackertype/remove.tt';
}

sub do_remove : Chained('object') : PathPart('remove') : Args(0) POST {
  my ( $self, $c ) = @_;
  my $api = $c->model('API');
  $c->model('API')->stash_result(
    $c,
    [ 'tracker-type', $c->stash->{obj_id} ],
    params => { password => $c->req->params->{password} },
    stash  => 'removed',
    method => 'DELETE'
  );
  $c->res->redirect($c->uri_for($self->action_for('index')));
}

sub edit : Chained('object') : Args(0) {
  my ( $self, $c ) = @_;

  $c->forward('load_items');
  $c->stash->{template} = 'admin/trackertype/edit.tt';

  p( $c->stash );
}

sub do_edit : Chained('object') : PathPart('edit') : Args(0) : POST {
  my ( $self, $c, $id ) = @_;
  $c->stash->{template} = 'admin/trackertype/edit.tt';
  my $api = $c->model('API');
  $api->stash_result(
    $c,
    [ 'tracker-type', $c->stash->{obj_id} ],
    params => $c->req->params,
    stash  => 'tracker_type',
    method => 'PUT'
  );
  $c->forward('load_items');
}

sub add : Chained('base') : PathPart('') Args(0) POST {
  my ( $self, $c, $id ) = @_;
  my $api = $c->model('API');

  $api->stash_result(
    $c,
    'tracker-type',
    params => $c->req->params,
    method => 'POST'
  );

  $c->forward('load_items');

}

sub load_items : Private {
  my ( $self, $c ) = @_;
  my $api = $c->model('API');
  $api->stash_result( $c, 'tracker-type', stash => 'tracker_types' );
}

__PACKAGE__->meta->make_immutable;

1;
