package WebTrackware::Controller::Admin::Tracker;
use utf8;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/admin/base') : PathPart('tracker') : CaptureArgs(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, 'business_units',
    params => {order => 'corporate_name'});
  $c->stash->{select_business_units}
    = [map { [$_->{id}, $_->{corporate_name}] } @{$c->stash->{business_units}}];

  $api->stash_result($c, 'tracker_status');
  $c->stash->{select_status}
    = [map { [$_->{id}, $_->{status}] } @{$c->stash->{tracker_status}}];

  $api->stash_result($c, 'network_suppliers', params => {order => 'me.name'});
  $c->stash->{select_network_supplier}
    = [map { [$_->{id}, $_->{name}] } @{$c->stash->{network_suppliers}}];
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');
  $api->stash_result($c, ['trackers', $id], stash => 'tracker_obj');

  $c->detach('/form/not_found', []) if $c->stash->{tracker_obj}{error};
}

sub is_json : Chained('base') : PathPart('json') : Args(0) {
  my ($self, $c) = @_;
  $c->forward('index');
  $c->json(
    {
      trackers => $c->stash->{trackers} || [],
      allowed_commands => $c->stash->{allowed_commands},
    }
  );
}

sub index : Chained('base') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');
  $api->stash_result($c, [qw(command allowed)], stash => 'allowed_commands');
  $api->stash_result($c, 'business_units');
  $api->stash_result($c, ['multitrack'], method => 'GET', stash => 'groups');


  my %params;

  $params{business_unity_id} = $c->stash->{session_business_unity}{id}
    || $c->req->params->{business_unity_id};
  
  $params{multitrack_id} = $c->req->params->{multitrack_id};

  return unless $params{business_unity_id} || $params{multitrack_id};

  if (exists $c->req->params->{search}) {
    if ( $c->req->params->{search_type} eq 'vehicle_id'
      && $c->req->params->{search})
    {

      $params{vehicle_id} = $c->req->params->{search};

    }
    elsif ($c->req->params->{search_type} eq 'code'
      && $c->req->params->{search})
    {

      $params{code} = $c->req->params->{search};

    }
  }

  $params{order} = 'imei';

  $api->stash_result($c, ['trackers'], params => {%params});
  

}

sub messages : Chained(object) Args(0) GET {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result(
    $c, ['tracker_positions',],
    params => {tracker_id => $c->stash->{tracker_obj}->{id}},
    stash  => 'messages'
  );

  $api->stash_result(
    $c,
    ['trackers', $c->stash->{tracker_obj}->{id}, 'sms/last-sms'],
    stash => 'smses'
  );

}

sub remove : Chained('object') : Args(0) GET { }

sub really_remove : Chained('object') : PathPart('remove') Args(0) POST {
  my ($self, $c) = @_;
  $c->model('API')->stash_result(
    $c, ['trackers', $c->stash->{tracker_obj}->{id}],
    params => {password => $c->req->params->{password}},
    stash  => 'removed',
    method => 'DELETE'
  );
}

sub edit : Chained('object') : PathPart('') : Args(0) {
}

sub add : Chained('base') : PathPart('new') : Args(0) {
  my ($self, $c) = @_;
}

sub activate : Chained('base') : PathPart('activate') : Args(0) {
  my ($self, $c) = @_;
}

sub garbage : Chained('base') : Args(0) {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result($c, ['garbage',], stash => 'garbage');
}

sub manage : Chained(base) : PathPart(manage) : Args(0) GET {

}

sub manage_search : Chained(base) : PathPart(manage) : Args(0) POST {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $c->stash->{template} = 'admin/tracker/manage.tt';
  $api->stash_result(
    $c, ['trackers', 'search'],
    params => $c->req->params,
    stash  => 'search_result',
    method => 'POST'
  );
}

sub register : Chained('object') : Args(0) {
  my ($self, $c) = @_;

  $c->detach('/form/redirect_error', [])
    if $c->stash->{tracker_obj}->{business_unity_id};


  return unless $c->req->method eq 'POST';

  my $api = $c->model('API');

  $api->stash_result(
    $c, ['trackers', $c->stash->{tracker_obj}->{id}, 'register'],
    params => $c->req->params,
    stash  => 'result',
    method => 'POST'
  );

  return if $c->stash->{result}{error};
  $c->detach('/form/redirect_ok',
    [$self->action_for('manage'), {}, 'Equipamento cadastrado com sucesso!']);
  $c->stash->{template} = 'admin/tracker/register_done.tt';
}

__PACKAGE__->meta->make_immutable;

1;
