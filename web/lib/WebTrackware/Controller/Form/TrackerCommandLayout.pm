package WebTrackware::Controller::Form::TrackerCommandLayout;
use Moose;
use namespace::autoclean;
use utf8;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/form/root') : PathPart('tracker_command_layout') : CaptureArgs(0) {
}

sub process : Chained('base') : PathPart('') : Args(0) {
    my ( $self, $c ) = @_;

    my $api		= $c->model('API');
    my $form    = $c->model('Form');

    my $params = { %{ $c->req->params } };
    
    $params->{business_unity_id} = $c->stash->{session_business_unity}{id} if !grep { /^admin$/ } $c->user->roles;
    
    $api->stash_result(
        $c, 'tracker_command_layouts',
        method  => 'POST',
        body    => $params
    );
	
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } 
    else {
    
		my $url;
		if ( grep { /^admin$/ } $c->user->roles ) {
			$url = '/admin/trackercommandlayout/index';
		} 
		elsif( grep { /^admin-unity$/ } $c->user->roles ) {
			$url = '/adminunity/trackercommandlayout/index';
		}
    
		$c->detach( '/form/redirect_ok', [ $url, {}, 'Cadastrado com sucesso!' ] );
    }
}

sub process_edit : Chained('base') : PathPart('') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api		= $c->model('API');
    my $form    = $c->model('Form');

    my $params = { %{ $c->req->params } };
    
    $params->{business_unity_id} = $c->stash->{session_business_unity}{id} if !grep { /^admin$/ } $c->user->roles;
    
    $api->stash_result(
        $c, ['tracker_command_layouts', $id],
        method  => 'PUT',
        body    => $params
    );
    
    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } 
    else {
		my $url;
		if ( grep { /^admin$/ } $c->user->roles ) {
			$url = '/admin/trackercommandlayout/index';
		} 
		elsif( grep { /^admin-unity$/ } $c->user->roles ) {
			$url = '/adminunity/trackercommandlayout/index';
		}

		$c->detach( '/form/redirect_ok', [ $url, {}, 'Comando alterado com sucesso!' ] );
	}
}

sub process_delete : Chained('base') : PathPart('remove') : Args(1) {
    my ( $self, $c, $id ) = @_;

    my $api = $c->model('API');

    $api->stash_result( $c, [ 'tracker_command_layouts', $id ], method => 'DELETE' );

    if ( $c->stash->{error} ) {
        $c->detach( '/form/redirect_error', [] );
    } 
    else {
		my $url;
		if ( grep { /^admin$/ } $c->user->roles ) {
			$url = '/admin/trackercommandlayout/index';
		} 
		elsif( grep { /^admin-unity$/ } $c->user->roles ) {
			$url = '/adminunity/trackercommandlayout/index';
		}

		$c->detach( '/form/redirect_ok', [ $url, {}, 'Excluído com sucesso!' ] );
	}
}

__PACKAGE__->meta->make_immutable;

1;
