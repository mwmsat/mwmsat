package WebTrackware::Controller::Agent;
use Moose;
use namespace::autoclean;
use URI;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/root') : PathPart('agent') : CaptureArgs(0) {
  my ($self, $c) = @_;

  if (!$c->user || !grep {/^agent$/} $c->user->roles) {
    $c->detach('/form/redirect_error', []);
  }

  my $api = $c->model('API');

  $c->stash->{template_wrapper} = 'agent';
}

__PACKAGE__->meta->make_immutable;

1;
