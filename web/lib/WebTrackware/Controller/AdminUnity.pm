package WebTrackware::Controller::AdminUnity;
use Moose;
use namespace::autoclean;
use URI;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/root') : PathPart('admin-unity') : CaptureArgs(0) {
  my ($self, $c) = @_;

  if (!$c->user || !grep {/^admin-unity$/} $c->user->roles) {
    $c->detach('/form/not_found', []);
  }

  # my $api = $c->model('API');
  # $api->stash_result($c, 'business_unity_users',
  #   params => {user_id => $c->user->id});

  $c->stash->{user_name}        = $c->user->name;
  $c->stash->{template_wrapper} = 'admin_unity';

  if ($c->req->method eq 'POST') {
    return;
  }

}

__PACKAGE__->meta->make_immutable;

1;
