package WebTrackware::Controller::Resources;
use utf8;
use Moose;
use DateTime;
use JSON::XS;
use DateTime::Format::Pg;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/root') : PathPart('') : CaptureArgs(0) {
}

sub get_address : Chained('base') : PathPart('get_address') {
    my ( $self, $c ) = @_;

    my $api  = $c->model('API');
    my $form = $c->model('Form');

    my $params = { %{ $c->req->params } };

    $form->only_number( $params, 'postal_code' );

    my $result = $api->get_result( $c, ['cep'], params => $params );

    $c->res->header( 'content-type', 'application/json;charset=UTF-8' );
    $c->res->body( encode_json($result) );
}

sub get_cities : Chained('base') : PathPart('get_cities') {
    my ( $self, $c ) = @_;

    my $api = $c->model('API');

    $api->stash_result(
        $c, 'cities',
        params => {
            state_id => $c->req->params->{state_id},
            order    => 'name'
        }
    );

    $c->stash(
        select_cities   => [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{cities} } ],
        without_wrapper => 1,
        template        => 'auto/cities.tt'
    );
}

sub registration_successfully : Chained('base') : PathPart('registration_successfully') : Args(0) {
    my ( $self, $c ) = @_;

    $c->stash( template => 'user/account/success.tt' );
}

sub get_vehicle_models : Chained('base') : PathPart('get_vehicle_models') : Args(0) {
    my ( $self, $c ) = @_;

    my $api = $c->model('API');

    $api->stash_result(
        $c,
        'vehicle_models',
        params => {
            vehicle_brand_id => $c->req->params->{vehicle_brand_id},
            order            => 'model'
        }
    );

    $c->stash(
        select_vehicle_models => [ map { [ $_->{id}, $_->{model} ] } @{ $c->stash->{vehicle_models} } ],
        without_wrapper       => 1,
        template => 'auto/vehicle_models.tt'
    );
}

sub search_by_plate : Chained('base') : PathPart('search_by_plate') : Args(1) {
	my ( $self, $c, $car_plate ) = @_;

    my $api = $c->model('API');
    
    my $filter;
    
    if( $c->req->params->{type} eq 'customer' ) {
		$filter = { customer_id => $c->req->params->{id} };
    } 
    elsif( $c->req->params->{type} eq 'business_unity' ) {
		$filter = { business_unity_id => $c->req->params->{id} };
    }
    
    $api->stash_result(
        $c,
        'vehicles',
        params => {
            car_plate => $car_plate,
			$filter ? %{ $filter } : undef
        }
    );
    
    $c->stash(
        without_wrapper       => 1,
        template => 'auto/search_by_plate.tt'
    );
}

sub search_vehicle_fleet : Chained('base') : PathPart('search_vehicle_fleet') : Args(1) {
    my ( $self, $c, $customer_id ) = @_;

    my $api = $c->model('API');

    $api->stash_result(
        $c, 'vehicle_fleets',
        params => {
            customer_id => $customer_id,
            order    => 'name'
        }
    );

    $c->stash(
        select_vehicle_fleets => [ map { [ $_->{id}, $_->{name} ] } @{ $c->stash->{vehicle_fleets} } ],
        without_wrapper => 1,
        template        => 'auto/vehicle_fleets.tt'
    );
}

__PACKAGE__->meta->make_immutable;

1;
