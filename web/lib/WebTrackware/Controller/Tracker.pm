package WebTrackware::Controller::Tracker;
use Moose;
use namespace::autoclean;
use URI;
use JSON::XS;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/root') : PathPart('tracker') : CaptureArgs(0) {
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, ['trackers', $id], stash => 'tracker_obj');
}


sub assign_command : Chained('base') : PathPart('assign-command') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  my $par;

  if ($c->req->params->{data}) {
    my @params = split "&", $c->req->params->{data};
    my $size = @params;

    for (my $i = 0 ; $i < $size ; $i++) {
      my @arr = split '=', $params[$i];

      my $arr_size = @arr;

      next if $arr_size < 2;

      $par->{$arr[0]} = $arr[1];
    }
  }

  $api->stash_result(
    $c,
    'trackers/assign_commands',
    params => {
      tracker_id => $par->{tracker_id},
      command    => $par->{command_id},
      send_type  => $par->{send_type},
    }
  );

  my $response;

  if ($c->stash->{error}) {
    $response = 'NOK';

    $c->response->status(500);
  }
  else {
    $response = 'OK';
  }

  $c->res->header('content-type', ' application/json');

  $c->res->body($response);

  $c->detach();
}

sub tracker_profile : Chained('object') : PathPart('tracker-profile') : Args(0)
{
  my ($self, $c) = @_;
}
use JSON qw(encode_json);

sub report : Chained(/) PathPart('tracker/report') Args(0) GET {
  my ($self, $c) = @_;
  my $api = $c->model('API');
  $api->stash_result(
    $c,
    ['trackers'],
    params => {
      api_key                => '__from_customer_page',
      business_unity_api_key => $c->req->header('X-Api-Key')
    }
  );

  $c->res->status(200);
  $c->res->content_type('application/json');
  $c->res->body(encode_json({trackers => $c->stash->{trackers}}));
}

__PACKAGE__->meta->make_immutable;

1;
