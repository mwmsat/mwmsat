package WebTrackware::Controller::Root;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=encoding utf-8

=head1 NAME

WebTrackware::Controller::Root - Root Controller for WebTrackware

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub logged_in : Chained('/') : PathPart('') : CaptureArgs(0) {
  my ($self, $c) = @_;
  if (!$c->user) {
    my $uri = URI->new('/login');
    $c->res->redirect($uri->as_string);
    $c->detach;
  }
}

sub index : Path : Args(0) {
  my ($self, $c) = @_;
  $self->root($c);

  if (!$c->user) {
    my $uri = URI->new('/login');

    $c->res->redirect($uri->as_string);
  }
  else {
    $c->detach('Form::Login' => 'after_login');
  }
}

sub root : Chained('/') : PathPart('') : CaptureArgs(0) {
  my ($self, $c) = @_;

  $c->stash->{c_req_path} = $c->req->path;

  $c->session->{foobar}++;

  $c->load_status_msgs;
  my $status_msg = $c->stash->{status_msg};
  my $error_msg  = $c->stash->{error_msg};

  @{$c->stash}{keys %$status_msg} = values %$status_msg
    if ref $status_msg eq 'HASH';
  @{$c->stash}{keys %$error_msg} = values %$error_msg
    if ref $error_msg eq 'HASH';

  my ($class, $action) = ($c->action->class, $c->action->name);
  $class =~ s/^WebTrackware::Controller:://;
  $class =~ s/::/-/g;

  $c->stash->{body_class} = lc "$class $class-$action";

  if ($c->req->cookie('session_business_unity')) {
    my @cookie = $c->req->cookie('session_business_unity')->value();

    $c->stash->{session_business_unity}{id}   = $cookie[0];
    $c->stash->{session_business_unity}{name} = $cookie[1];
  }

#     	Necessário fazer controle de acesso de filiais -
#		Se usuário não é admin geral deve fazer parte do business_unity_users, senão, detach to login

  if ($c->user) {
    if (grep {/^user$/} $c->user->roles) {
      $c->stash->{role_controller} = 'user';
    }
    elsif (grep {/^admin-unity$/} $c->user->roles) {
      $c->stash->{role_controller} = 'adminunity';
    }
    elsif (grep {/^admin$/} $c->user->roles) {
      $c->stash->{role_controller} = 'admin';
    }
  }

}

=head2 default

Standard 404 error page

=cut

sub default : Path {
  my ($self, $c) = @_;

  $self->root($c);
  my $maybe_view = join '/', @{$c->req->arguments};

  if ($c->user && $maybe_view =~ /^(participar)$/) {
    $c->detach('Form::Login' => 'after_login');
  }

  my $output;
  eval {
    $c->stash->{body_class} .= ' ' . $maybe_view;
    $c->stash->{body_class} =~ s/\//-/g;
    $output = $c->view('TT')->render($c, "auto/$maybe_view.tt", $c->stash);
  };
  if ($@ && $@ =~ /not found$/) {
    $c->response->body('Page not found');
    $c->response->status(404);
  }
  elsif (!$@) {
    $c->response->body($output);
  }
  else {
    die $@;
  }
}

sub rest_error : Private {
  my ($self, $c) = @_;

  $c->stash->{template}        = 'rest_error.tt';
  $c->stash->{without_wrapper} = 1;

  # TODO enviar um email se não estiver com $c->debug ?
  $c->res->status(500);

}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {
  my ($self, $c) = @_;

  if ($c->debug && exists $ENV{DUMP_STASH}) {
    my $x = $c->stash;
    use DDP;
    p $x;
  }
}
use JSON qw(encode_json);

sub jwt : Chained('logged_in') Args(0) {
  my ($self, $c) = @_;
  $c->res->content_type('application/json');
  $c->res->headers->header('charset' => 'utf-8');
  $c->res->body(
    encode_json({im_an_admin => $c->am_i_admin(), token => $c->jwt}));
}

=head1 AUTHOR

renato,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
