package WebTrackware::Controller::TrackerManager::Position;
use Moose;
use JSON::XS;
use namespace::autoclean;
use Geo::Coordinates::DecimalDegrees;

BEGIN { extends 'Catalyst::Controller' }

sub base : Chained('/trackermanager/base') : PathPart('position') :
  CaptureArgs(0) {
  my ($self, $c) = @_;

}

sub position : Chained('base') : PathPart('') : Args(0) {
  my ($self, $c) = @_;

  my $api = $c->model('API');

  $api->stash_result($c, ['trackers', $c->req->params->{id}, 'position'],);
  $api->stash_result(
    $c,
    ['trackers', $c->req->params->{id}, 'delivery-status'],
    stash => 'delivery'
  );
  $c->res->header('content-type', 'application/json;charset=UTF-8');
  $c->res->body(
    encode_json(
      {
        tracker   => $c->stash->{tracker},
        delivery  => $c->stash->{delivery},
        positions => [
          map {
            ($_->{address}) = $_->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
            $_;
          } @{$c->stash->{positions}}
        ]
      }
    )
  );
}

__PACKAGE__->meta->make_immutable;

1;
