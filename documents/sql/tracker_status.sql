--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.5
-- Started on 2014-10-13 15:53:33 BRT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 235 (class 1259 OID 39065)
-- Name: tracker_reason_generator; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tracker_reason_generator (
    id integer NOT NULL,
    code integer,
    reason text,
    description text
);


ALTER TABLE public.tracker_reason_generator OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 39071)
-- Name: tracker_generator_reason_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tracker_generator_reason_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tracker_generator_reason_id_seq OWNER TO postgres;

--
-- TOC entry 2174 (class 0 OID 0)
-- Dependencies: 236
-- Name: tracker_generator_reason_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tracker_generator_reason_id_seq OWNED BY tracker_reason_generator.id;


--
-- TOC entry 2056 (class 2604 OID 39073)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracker_reason_generator ALTER COLUMN id SET DEFAULT nextval('tracker_generator_reason_id_seq'::regclass);


--
-- TOC entry 2175 (class 0 OID 0)
-- Dependencies: 236
-- Name: tracker_generator_reason_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tracker_generator_reason_id_seq', 23, true);


--
-- TOC entry 2168 (class 0 OID 39065)
-- Dependencies: 235
-- Data for Name: tracker_reason_generator; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tracker_reason_generator (id, code, reason, description) FROM stdin;
1	0	Hardware reset	1° pacote após equipamento ter sido ligado/alimentado
2	1	Reset	1° pacote após equipamento sofrer reset de firmware (porwatchdog ou após comando de reset)
3	2	Tracking	Mensagem enviada periodicamente quando ignição esta ligada
4	3	Tracking at sleep	Mensagem enviada periodicamente quando ignição esta desligada
5	4	Ultima posição	Mensagem enviada quando equipamento recebe comando de requisitar ultima posição
6	5	Pânico ativo	Mensagem enviada quando equipamento recebe comando de requisitar ultima posição
7	6	Pânico desativa	Mensagem enviada quando entrada de pânico é desacionada
11	10	Entrada 1 desativa	Mensagem enviada quando entrada 1 é desacionada
12	11	Entrada 2 ativa	Mensagem enviada quando entrada 2 é acionada
13	12	Entrada 2 desativa	Mensagem enviada quando entrada 2 é desacionada
14	13	Entrada 3 ativa	Mensagem enviada quando entrada 3 é acionada
15	14	Entrada 3 desativa	Mensagem enviada quando entrada 3 é desacionada
16	15	Falha na alimentação	Mensagem enviada quando bateria externa é removida
17	16	Alimentação externa normalizada	Mensagem enviada quando bateria externa é recolocada
18	17	Falha na bateria backup	Mensagem enviada quando bateria backup é removida
19	18	Bateria backup normalizada	Mensagem enviada quando bateria backup é recolocada
20	19	Inicio detecção de Jammer	Mensagem salva na memória quando equipamento detecta presença de jammer GSM
21	20	Fim detecção de Jammer	Mensagem enviada quando equipamento detecta que não esta mais sobre presença de jammer GSM
22	21	Velocidade máxima excedida	Mensagem enviada quando equipamento passa da velocidade máxima configurada
23	22	Velocidade máxima normalizada	Mensagem enviada quando velocidade do equipamento sai do limite configurado para abaixo deste limite
10	9	Detecção de batida	Mensagem enviada quando entrada 1 é acionada
9	8	Parado	Mensagem enviada quando entrada de ignição é desacionada
8	7	Em movimento	Mensagem enviada quando entrada de ignição é acionada
\.


--
-- TOC entry 2058 (class 2606 OID 39075)
-- Name: tracker_generator_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tracker_reason_generator
    ADD CONSTRAINT tracker_generator_reason_pkey PRIMARY KEY (id);


--
-- TOC entry 2060 (class 2606 OID 39082)
-- Name: tracker_reason_generator_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tracker_reason_generator
    ADD CONSTRAINT tracker_reason_generator_code_key UNIQUE (code);


-- Completed on 2014-10-13 15:53:33 BRT

--
-- PostgreSQL database dump complete
--

