#!/usr/bin/env perl

use strict;
use warnings;
use feature 'state';

use Mojolicious::Lite;
use Mango;
use URI::Escape qw(uri_escape);

my $user = $ENV{UP_USER} or die 'missing UP_USER';
my $pass = $ENV{UP_PASS} or die 'missing UP_PASS';
my $host = $ENV{UP_HOST} or die 'missing UP_HOST';

app->config(hypnotoad =>
    {pid_file => "$ENV{HOME}/upmap-hypnotoad.pid", listen => ['http://*:3000']});

helper mango => sub {
  my $dsn = sprintf(
    'mongodb://%s:%s@%s:27017/admin',
    map { uri_escape($_); $_ } $user,
    $pass, $host
  );
  warn $dsn;
  state $m = Mango->new(

#    'mongodb://localhost:27017/server-development'
    $dsn

  );
};

get '/' => 'index';

use DDP;
websocket '/live' => sub {
  my $c = shift;
  my $last_id;
  $c->inactivity_timeout(0);
  my $db = $c->mango->db('server-development');
  my $last30
    = $db->collection('measurements')->find->sort({_id => -1})->limit(30);
  foreach my $doc (reverse @{$last30->all}) {
    $doc = add_datetime($doc);
    $c->send({json => $doc});
    $last_id = $doc->{_id};
  }

  my $id = Mojo::IOLoop->recurring(
    1 => sub {
      if (!$last_id) {
        my $last30
          = $db->collection('measurements')->find->sort({_id => -1})->limit(30);
	foreach my $doc (reverse @{$last30->all}) {
          $doc = add_datetime($doc);
          $c->send({json => $doc});
          $last_id = $doc->{_id};
        }
      }
      else {
        ($_ = add_datetime($_)), $c->send({json => $_}), ($last_id = $_->{_id})
          for
          @{$db->collection('measurements')->find({_id => {'$gt' => $last_id}})
            ->sort({_id => 1})->all};
      }
    }
  );

  $c->on(finish => sub { Mojo::IOLoop->remove($id) });
};

sub add_datetime {
  my $doc = shift or return;
  $doc->{__ts__}
    = Mango::BSON::Time->new($doc->{_id}->to_epoch * 1000)->to_datetime;
  $doc;
}

app->start;


__DATA__

@@ index.html.ep

<!DOCTYPE html>
<html>
  <head>
    <title>UP</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.2/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.3.2/dist/leaflet.js" integrity="sha512-2fA79E27MOeBgLjmBrtAgM/20clVSV8vJERaW/EcnnWCVGwQRazzKtQS1kIusCZv1PtaQxosDZZ0F1Oastl55w==" crossorigin=""></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/reconnecting-websocket/1.0.0/reconnecting-websocket.min.js"></script>

    <style>
        #container {
        margin: 18px;
	display: inline-grid;
	grid-column-gap: 18px;
	grid-row-gap: 18px;
	grid-template-columns: auto auto auto auto;
      }
        #positions {
  
	min-width: 200px;
        max-height: 400px;
        overflow: scroll;
	overflow-y: scroll;
	grid-column-start: 1;
	grid-column-end: 2;
      }
      #mapid {
	grid-column-start: 2;
	grid-column-end: 4;
      }
      #positions ul {

	list-style-type: none;
	list-style: none;
	padding: 0;
	margin: 0;
      }
      #positions ul li {
	cursor: pointer;
	padding-left: 8px;
	padding-top: 6px;
	padding-bottom: 6px;
	margin-top: 6px;
	margin-bottom: 6px;
	border-bottom: none;
	border-top: 1px solid #f8d9d0;
      }
      #positions ul li:hover {
	background-color: #12a0fe;
      }
    </style>
  </head>
  <body>

    <div id="container">
      <div id="positions" >
	<ul></ul>
      </div>
      <div id="mapid" style="width: 600px; height: 400px;"></div>
    </div>
    <script>
      function fadeIn(el) {
	el.style.opacity = 0;

	var last = +new Date();
	var tick = function() {
	  el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
	  last = +new Date();

	  if (+el.style.opacity < 1) {
	    (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
	  }
	};

	tick();
      }
      var mymap = L.map('mapid').setView([-23.575, -46.634], 11);
      L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	subdomains: ['a','b','c']
      }).addTo( mymap );
      var positions = document.querySelector("#positions ul");
      var connection = new ReconnectingWebSocket('ws://' + window.location.host + '/live');
      connection.onmessage = function (e) {
	var json = JSON.parse(e.data);
	console.log(json);
	var node = document.createElement("li");
	node.style.opacity = 0;
	var ts = json.__ts__.replace(/[A-Za-z]/g, ' ');
	var textnode = document.createTextNode(ts);

	node.appendChild(textnode);     

	positions.insertBefore(node, positions.firstChild);
	fadeIn(node);
	var marker = L.marker(json.location.coordinates)
		      .bindPopup(`<p>${ts}</p><p><em>(${json.location.coordinates.join(',')})</em></p>`)
		      .addTo(mymap).openPopup();
	node.addEventListener('click', function(e) { marker.openPopup() });
      };
    </script>

  </body>
</html>
