package Trackware::TrackerMessageHandler;
use namespace::autoclean;
use Moose;

with 'MooseX::Traits';

# use JSON qw(encode_json);

has '+_trait_namespace' => (default => 'Trackware::TrackerMessageHandler');

sub parse {
  my ($self, $message) = @_;

  my %parsed_msg = $self->prepare();

  return %parsed_msg;
}

1;
