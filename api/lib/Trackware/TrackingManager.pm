package Trackware::TrackingManager;

use Moose;
use Trackware::Redis;
use Trackware::TrackingManager::Cache;
use JSON::XS qw(encode_json);
use Trackware::Log;
use Trackware::Geolocation;
use Geo::Coordinates::DecimalDegrees;
use DateTimeX::Easy qw(parse);
use Geo::Coder::Google::V3;
has schema => (is => 'rw', isa => 'Trackware::Schema',);
use DDP;
my $tracker_cache = Trackware::TrackingManager::Cache->new();
my $logger        = get_logger;
my $redis         = Trackware::Redis->new();

sub add {
  my ($self, %message) = @_;

  my $tracker_data = $tracker_cache->check_status($message{imei});

  if (!$tracker_data) {
    $logger->warn(
      'Tracker is not registered on system. IMEI: ' . $message{imei});

    return 0;
  }

#   Verifica se o contador das mensagens bate com as mensagens recebidas e monta fila de mensagens perdidas, precisa ser revisto
  my $counter = $tracker_cache->check_counter("counter-$message{imei}");

  if (!$counter
    || $counter + 1 == $message{position_counter}
    || ($message{position_counter} == 0 && ($counter == 0 || $counter == 38332))
    )
  {
    $tracker_cache->update_counter($message{imei}, $message{position_counter});
  }
  else {
    my $interval = $message{position_counter} - $counter;

    my @missing_messages;

    for (1 ... $interval - 1) {
      push(@missing_messages, $counter += 1);
    }

    $tracker_cache->push_missing_messages($message{imei}, @missing_messages);
  }

  my $tracker_position = $self->schema->resultset('TrackerPosition');

  eval {
    $tracker_position->create(
      {
        tracker_id => $tracker_data->{tracker_id},
        vehicle_id => $tracker_data->{vehicle_id} ? $tracker_data->{vehicle_id}
        : undef,
        lat                 => $message{latitude},
        lng                 => $message{longitude},
        speed               => $message{speed},
        position_counter    => $message{position_counter},
        event_date          => $message{package_date},
        sat_number          => $message{sat_number},
        hdop                => $message{hdop},
        battery_voltage_bkp => $message{battery_voltage_bkp}
        ? $message{battery_voltage_bkp} / 10
        : undef,
        hourmeter        => $message{hourmeter},
        imei             => $message{imei},
        message_size     => $message{message_size},
        odometer         => $message{odometer},
        temperature      => $message{temperature},
        package_type     => hex($message{package_type}),
        pcb_version      => $message{pcb_version},
        firmware_version => $message{firmware_version}
        ? $message{firmware_version} / 10
        : undef,
        position_amount               => $message{position_amount},
        position_size                 => $message{position_size},
        inputs                        => $message{inputs},
        outputs                       => $message{outputs},
        tracker_reason_generator_code => $message{reason_generator},
      }
    );
  };

  $logger->error('Error saving position on database. Tracker_id: '
      . $tracker_data->{tracker_id}
      . ' Message: '
      . $@)
    if $@;
  use DDP;
  p $@;

  return 1;
}

sub add_error {
  my ($self, $error) = @_;


  eval { $redis->redis->rpush('tracker_error' => $error) };

  die $@ if $@;

  print "Erro adicionado a fila\n";

  return 1;
}

sub add_event {
  my ($self, @message) = @_;
  my $message;

  if (@message == 1) {
    $message = $message[0];
  }
  else {
    $message = Trackware::TrackingManager::Message->new(@message);
  }

  die 'invalid object type'
    if ref $message ne 'Trackware::TrackingManager::Message';

  my $tracker_data = $self->schema->resultset('Tracker')
    ->search({code => $message->tracker_code, status => 1})->next;

  if (!$tracker_data) {
    die "Tracker code not found.\n code: $message->tracker_code\n";
  }

  my $vehicle_tracker_event = $self->schema->resultset('VehicleTrackerEvent');

  $vehicle_tracker_event->create(
    {
      tracker_id        => $tracker_data->id,
      vehicle_id        => $tracker_data->vehicle_id,
      track_event       => $message->track_event,
      event_information => encode_json($message->event_information),
      transaction       => $message->transaction
    }
  );
}

sub build_statistic_queue {
  my ($self, $params) = @_;

  die 'empty data' if !$params;

  my $data = encode_json($params);

  $redis->redis->rpush('vehicle_statistics' => $data);
}

sub new_tracker {
  my ($self, $params) = @_;

  my $tracker    = decode_json($params);
  my $rs_tracker = $self->schema->resultset('Tracker');

  my $tracker_reg;
  my $ret;

  eval {

    if ($rs_tracker->search({imei => $tracker->{imei}})->next) {
      $ret = 2;
    }
    else {
      $tracker_reg = $rs_tracker->create(
        {
          imei              => $tracker->{imei},
          iccid             => $tracker->{iccid},
          tracker_status_id => 1
        }
      );
    }

  };
  $logger->error(
    'Error creating new tracker. IMEI: ' . $tracker->{imei} . ' Message: ' . $@)
    if $@;

  return $ret if $ret == 2;

  if ($tracker_reg) {
    $tracker_cache->update_cache($tracker->{imei}, $tracker_reg->id, undef,
      'post');
  }

  return 0 unless $tracker_reg;

  return 1;
}

sub add_sms_message {
  my ($self, %message) = @_;

  my $tracker_data    = $tracker_cache->check_status($message{imei});
  my $tracker_sms_raw = $self->schema->resultset('TrackerSmsRaw');

  if (!$tracker_data) {
    $logger->warn(
      'Tracker is not registered on system. IMEI: ' . $message{imei});

    $tracker_sms_raw->create(
      {
        imei    => $message{imei},
        iccid   => $message{iccid},
        message => encode_json(\%message)
      }
    );

    return 0;
  }

  my $tracker_position = $self->schema->resultset('TrackerSmsPosition');

  $message{vehicle_id} =
    $tracker_data->{vehicle_id} ? $tracker_data->{vehicle_id} : undef;
  $message{tracker_id} = $tracker_data->{tracker_id};

  delete $message{none};
  delete $message{none_2};
  delete $message{none_3};
  delete $message{none_4};

  eval {
    my $t = $tracker_position->create(\%message);

    $tracker_sms_raw->create(
      {
        imei                    => $message{imei},
        iccid                   => $message{iccid},
        message                 => encode_json(\%message),
        tracker_sms_position_id => $t ? $t->id : undef
      }
    );
  };

  $logger->error('Error saving position on database. Tracker_id: '
      . $tracker_data->{tracker_id}
      . ' Message: '
      . $@)
    if $@;
  use DDP;
  p $@;

  return 1;
}

my $google_geo_api = Geo::Coder::Google::V3->new(
  key =>
    ($ENV{GOOGLE_MAPS_API_KEY} || 'AIzaSyCVRSogQpUwhgvu2GccC7F9rh7B7915qSY'),
  region   => q{br},
  language => q{pt-BR},
);

sub add_gprs_lbs {
  my ($self, $tracker_data) = @_;
  p($tracker_data);
  $tracker_data->{iccid}        .= '';
  $tracker_data->{imei}         .= '';
  $tracker_data->{phone_number} .= '';


  my $tracker_position = $self->schema->resultset('TrackerPosition');
  my $new_position;

  my $lat = $tracker_data->{position}->{lat} || 0;
  my $lng =
    $tracker_data->{position}->{lon} || $tracker_data->{position}->{lng} || 0;

  if (!$tracker_data->{position}->{address} && ($lat && $lng)) {
    eval {
      my $cache_key =
        join('_', 'google', map { sprintf '%.3f', $_ } ($lat, $lng));
      $tracker_data->{position}->{address} = $redis->redis->get($cache_key) || qq{$lat,$lng};
        # or $redis->redis->set(
        # $cache_key => (
        #   $tracker_data->{position}->{address} =
        #     $google_geo_api->reverse_geocode(latlng => qq{$lat,$lng})
        #     ->{formatted_address}
        # )
        # );
    };
    warn $@ if $@;
  }

  eval {
    $self->schema->txn_do(
      sub {

        my $supplier = $self->schema->resultset('NetworkSupplier')
          ->find_or_create({name => $tracker_data->{supplier}});

        my $tracker = $self->schema->resultset('Tracker')->search_rs(
          {
            active              => 1,
            phone_number        => $tracker_data->{phone_number},
            tracker_status_id   => 1,
            network_supplier_id => $supplier->id,

          },
          {rows => 1}

          )->next
          || $self->schema->resultset('Tracker')->create(
          {
            active            => 1,
            phone_number      => $tracker_data->{phone_number},
            tracker_status_id => 1,
            network_supplier  => $supplier
          }

          );

        $tracker->update({imei => $tracker_data->{imei}})
          if !$tracker->imei || ($tracker->imei ne $tracker_data->{imei});

        # nao tem iccid ainda ou só os ultimos 4 digitos

        my $current_iccid = $tracker->iccid;

        if (
          !$current_iccid
          || ( length($tracker->iccid) == 4
            && length($tracker_data->{iccid}) >= 20
            && ($tracker_data->{iccid} =~ /\Q$current_iccid\E$/))
          )
        {
          $tracker->iccid($tracker_data->{iccid});
          $tracker->update;
        }
        elsif ($tracker->iccid ne $tracker_data->{iccid}
          && length($tracker_data->{iccid}) >= 20
          && substr($tracker->iccid, -4) ne substr($tracker_data->{iccid}, -4))
        {

          # inativa antigo
          $tracker->update({active => 0}) if $tracker;

          # cria novo equipamento
          $tracker = $self->schema->resultset('Tracker')->create(
            {
              active            => 1,
              phone_number      => $tracker_data->{phone_number},
              tracker_status_id => 1,
              network_supplier  => $supplier,
              iccid             => $tracker_data->{iccid},
              imei              => $tracker_data->{imei},
            }
          );
        }

        $new_position = $tracker_position->create(
          {
            tracker    => $tracker,
            vehicle_id => $tracker_data->{vehicle_id}
            ? $tracker_data->{vehicle_id}
            : undef,
            event_date      => (eval{parse($tracker_data->{datetime})} || \q{now()}),
            lat             => $lat,
            lng             => $lng,
            imei            => $tracker_data->{imei},
            position_type   => 'gprs_lbs',
            address         => $tracker_data->{position}->{address},
            precision       => $tracker_data->{position}->{accuracy},
            battery_voltage => $tracker_data->{battery_voltage} * 10,
            lbs_raw         => $tracker_data->{text},

            (
              map { ($_ => $tracker_data->{$_}) }
                qw(tx cycle gprs gps r1 interval)
            )
          }
        );

        eval {
          $tracker->confirm_pending_configuration($new_position);
          $tracker->check_pending_notifications;
        };
        warn $@ if $@;

        $redis->redis->publish($tracker->queue_key,
          encode_json({add => $tracker->notification_payload}));

        $_->send for $tracker->tracker_sms_alerts->all;

      }
    );
  };

  $logger->error('Error saving position on database. Tracker_id: '
      . $tracker_data->{tracker_id}
      . ' Message: '
      . $@), return
    if $@;

  return $new_position;
}

sub parse_position {
  my ($self, $position) = @_;

  my ($degrees, $minutes) = decimal2dm($position);

  return $degrees . $minutes;
}

1;
