
package Trackware::Controller::API::TrackerCommandQueue;

use Moose;
use utf8;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::TrackerCommandQueue',
  object_key => 'tracker_command_queue',

  update_roles => [qw/superadmin admin admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],
  search_ok    => {business_unity_id => 'Int', order => 'Str'},
  result_attr  => {prefetch => ['tracker_command_layout', 'tracker']}
);

with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('tracker_command_queues') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $tracker_command_queue = $c->stash->{tracker_command_queue};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $tracker_command_queue->$_, }
          qw/
          id
          response

          /
      ),
      (
        map {
              $_ => $tracker_command_queue->$_
            ? $tracker_command_queue->$_->datetime
            : undef
        } qw/created_at updated_at/
      ),
      tracker_command_layout => {
        (
          map { $_ => $tracker_command_queue->tracker_command_layout->$_, }
            qw/
            id
            command
            /
        )
      },
      tracker => {
        (
          map { $_ => $tracker_command_queue->tracker->$_, }
            qw/
            id
            imei
            iccid
            /
        )
      },
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;

  my $tracker_command_queue = $c->stash->{tracker_command_queue};

  $tracker_command_queue->delete;

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $tracker_command_queue = $c->stash->{tracker_command_queue};

  $tracker_command_queue->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_command_queue->id])
      ->as_string,
    entity => {id => $tracker_command_queue->id}
    ),
    $c->detach
    if $tracker_command_queue;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  my $rs = $c->stash->{collection};

  $self->status_ok(
    $c,
    entity => {
      tracker_command_queues => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                response
                created_at
                updated_at
                /
            ),
            tracker_command_layout => {
              (
                map { $_ => $r->{tracker_command_layout}{$_} }
                  qw/
                  id
                  command
                  /
              )
            },
            tracker => {
              (
                map { $_ => $r->{tracker}{$_} }
                  qw/
                  id
                  imei
                  iccid
                  /
              )
            }
            }
        } $rs->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $tracker_command_queue = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_command_queue->id])
      ->as_string,
    entity => {id => $tracker_command_queue->id}
  );

}

1;
