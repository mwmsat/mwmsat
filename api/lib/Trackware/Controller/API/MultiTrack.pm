## Please see file perltidy.ERR
package Trackware::Controller::API::MultiTrack;
use Moose;
use namespace::autoclean;
use JSON qw(decode_json);
use Trackware::Util qw(rule_listing);

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(default => 'application/json');

sub base : Chained('/api/base') : PathPart(multitrack) : CaptureArgs(0) {
  my ($self, $c) = @_;
  $c->stash->{collection} = $c->model('DB::MultiTrack');
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;
  $c->stash->{object} = $c->stash->{group} = $c->stash->{collection}->find($id)
    or $c->detach('/error_404');
}


sub remove : Chained(object) : Args(0) DELETE {
  my ($self, $c) = @_;
  $c->stash->{group}->set_trackers([]);
  $c->stash->{group}->delete;
  $self->status_no_content($c);
}

sub add : Chained(object) : PathPart(add) : Args(0) POST {
  my ($self, $c) = @_;

  $self->status_bad_request($c, message => "Missing list"), $c->detach
    unless $c->req->params->{list};
  my @trackers = $c->model('DB::Tracker')->from_text_list($c->req->params->{list})->all;
  
  $self->status_bad_request($c, message => "Limite de iscas excedido: 10"), $c->detach
    if @trackers > 10;
  
  $c->stash->{group}->set_trackers(@trackers);
  $self->status_no_content($c);
}

sub view : Chained(object) : PathPart('') : Args(0) GET {
  my ($self, $c) = @_;
  $self->status_ok($c, entity => $c->stash->{object});
}

sub create : Chained(base) PathPart('') : Args(0) POST {
  my ($self, $c) = @_;
  my $group = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);
  $self->status_created(
    $c,
    location => $c->uri_for($self->action_for('view'), [$group->id])->as_string,
    entity   => {id => $group->id}
  );
}

sub list : Chained(base) PathPart('') : Args(0) GET {
  my ($self, $c) = @_;

  $self->status_ok($c,
    entity => {results => [$c->stash->{collection}->summary->as_hashref->all]});
}

1;
