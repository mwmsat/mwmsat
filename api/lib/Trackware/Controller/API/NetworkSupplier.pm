package Trackware::Controller::API::NetworkSupplier;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::NetworkSupplier',
  object_key => 'network_supplier',
  search_ok  => {'name' => 'Str', 'order' => 'Str'},

  update_roles => [qw/superadmin admin admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],

);

with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('network_suppliers') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $network_supplier = $c->stash->{network_supplier};
  my %attrs            = $network_supplier->get_inflated_columns;

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $attrs{$_}, }
          qw/
          id
          name
          /
      ),
      (
        map { $_ => ($attrs{$_} ? $attrs{$_}->datetime : undef) }
          qw/created_at/
      ),
    }
  );
}

sub result_PUT {
  my ($self, $c) = @_;

  my $network_supplier = $c->stash->{network_supplier};

  $network_supplier->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$network_supplier->id])
      ->as_string,
    entity => {id => $network_supplier->id}
    ),
    $c->detach
    if $network_supplier;
}

sub result_DELETE {
  my ($self, $c) = @_;

  my $network_supplier = $c->stash->{network_supplier};

  $network_supplier->delete;
  $self->status_no_content($c);
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      network_suppliers => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                name
                created_at
                /
            ),
            url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string
            }
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $network_supplier = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    entity => {id => $network_supplier->id},
    location =>
      $c->uri_for($self->action_for('result'), [$network_supplier->id])
      ->as_string,
  );

}

1;
