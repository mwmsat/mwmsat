package Trackware::Controller::API::MultiTrack::Tracker;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(default => 'application/json');

sub base : Chained('/api/multitrack/object') : PathPart(tracker) :
  CaptureArgs(1) {
  my ($self, $c, $id) = @_;
  $c->stash->{tracker} = $c->stash->{object} =
    $c->stash->{group}->multitrack_trackers->related_resultset('tracker')
    ->find($id)
    or $c->detach('/error_404');
}

sub remove : Chained(base) : PathPart('') Args(0) DELETE {
  my ($self, $c) = @_;
  $c->stash->{group}->remove_from_trackers($c->stash->{tracker})
    if $c->stash->{group}
    ->multitrack_trackers->find({tracker_id => $c->stash->{tracker}->id});
  $self->status_no_content($c);
}

sub view : Chained(base) : PathPart('') Args(0) GET {
  my ($self, $c) = @_;
  $self->status_ok($c, entity => $c->stash->{tracker});
}

1;
