package Trackware::Controller::API::VehicleDocument;

use Moose;
use Trackware::S3Client;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::VehicleDocument',
  object_key => 'document',

  update_roles => [qw/superadmin user admin/],
  create_roles => [qw/superadmin user/],
  delete_roles => [qw/superadmin user/],
  result_attr  => {prefetch => 'document_type'},
  search_ok    => {vehicle_id => 'Int',},

);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('vehicle_documents') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
}

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $document = $c->stash->{vehicle_document};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => ($document->$_ ? $document->$_->datetime : undef) }
          qw/created_at valid_date/
      ),
      (
        map { $_ => $document->$_, }
          qw/
          id
          link
          is_valid
          vehicle_id
          /
      ),
      document_type => {
        (
          map { $_ => $document->document_type->$_, }
            qw/
            id
            type
            description
            /
        )
      },
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;
  my $document = $c->stash->{vehicle_document};

  $document->delete;

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $document = $c->stash->{vehicle_document};

  my $params = $c->req->params;

  $document->execute($c, for => 'update', with => $params);

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$document->id])->as_string,
    entity => {id => $document->id}
    ),
    $c->detach
    if $document;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  my $rs = $c->stash->{collection};

  $self->status_ok(
    $c,
    entity => {
      vehicle_documents => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                link
                is_valid
                vehicle_id
                /
            ),
            document_type => {
              (
                map { $_ => $r->{document_type}{$_}, }
                  qw/
                  id
                  type
                  description
                  /
              )
            },
            url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string
            }
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  # TODO verificar tipo do arquivo

  my $document = $c->stash->{collection}->execute(
    $c,
    for  => 'create',
    with => {%{$c->req->params}, user_id => $c->user->id}
  );

  if ($c->req->upload('file')) {
    $self->_upload_file($c, $document);
  }

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$document->id])->as_string,
    entity => {id => $document->id}
  );
}

sub _upload_file {
  my ($self, $c, $document) = @_;

  my $client      = Trackware::S3Client->new();
  my $bucket_name = 'Trackware-BKT01';
  my $bucket      = $client->s3->bucket($bucket_name);

  my $upload     = $c->req->upload('file');
  my $user_id    = $c->user->id;
  my $class_name = $document->class_name;

  my $filename = sprintf('%i_%s', $document->id, $class_name);
  my $private_path = "vehicle_documents/$user_id/$filename";

  eval {
    $bucket->add_key_filename($private_path, $upload->tempname,
      {content_type => $upload->type},
    ) or die $client->s3->err . ": " . $$client->s3->errstr;
  };

  unless (!$@) {
    $self->status_bad_request($c, message => "Copy failed: $!"), $c->detach;
  }

  $document->update({link => $private_path});

  return 1;
}

1;
