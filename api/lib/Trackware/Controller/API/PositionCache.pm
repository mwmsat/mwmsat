package Trackware::Controller::API::PositionCache;

use Moose;
use utf8;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::PositionCache',
  object_key => 'position_cache',

  update_roles => [qw/superadmin webapi admin-unity/],
  create_roles => [qw/superadmin webapi/],
  delete_roles => [qw/superadmin webapi/],

  search_ok => {order => 'Str', point_id => 'Str'}

);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('position_cache') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $position_cache = $c->stash->{position_cache};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $position_cache->$_ }
          qw/
          id
          point_id
          address
          lat_lng
          postal_code
          /
      ),
      (
        map {
          $_ => ($position_cache->$_ ? $position_cache->$_->datetime : undef)
        } qw/created_at/
      ),
    }
  );
}

sub result_PUT {
  my ($self, $c) = @_;

  my $position_cache = $c->stash->{position_cache};

  $position_cache->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted(
    $c,
    location => $c->uri_for($self->action_for('result'), [$position_cache->id])
      ->as_string,
    entity => {id => $position_cache->id, point_id => $position_cache->point_id}
    ),

    $c->detach if $position_cache;
}

sub result_DELETE {
  my ($self, $c) = @_;

  my $position_cache = $c->stash->{position_cache};

  $position_cache->delete;
  $self->status_no_content($c);
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      position_cache => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                point_id
                address
                lat_lng
                created_at
                postal_code
                /
            ),
            url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string
            },
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $position_cache = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location => $c->uri_for($self->action_for('result'), [$position_cache->id])
      ->as_string,
    entity => {id => $position_cache->id, point_id => $position_cache->point_id}
  );

}

sub reverse_geolocation : Chained('base') : PathPart('reverse_geolocation') :
  Args(0) {
  my ($self, $c) = @_;

  my $geolocation = $c->model('Geolocation');

  my $address = $geolocation->reverse_geo_code($c->req->params->{lat_lng});

  my $cep = $address->{cep};

  $cep =~ s/\D//g;

  if ($address) {
    my $position_cache = $c->model('DB::PositionCache')->create(
      {
        lat_lng       => $c->req->params->{lat_lng},
        point_id      => $c->req->params->{point_id},
        point_orig_id => $c->req->params->{point_orig_id},
        address       => $address->{formatted_address},
        postal_code   => $cep
      }
    );
  }

  $self->status_ok($c, entity => {reverse_geolocation => $address});
}

1;
