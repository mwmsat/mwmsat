package Trackware::Controller::API::Garbage;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result => 'DB::SmsGarbage',
);
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::Search';

sub base : Chained('/api/base') : PathPart('garbage') : CaptureArgs(0) { }

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      items => [
        $c->stash->{collection}->search_rs(
          undef,
          {
            columns => [
              qw(chip_number message),
              {
                ts => \
q{to_char(me.ts at time zone 'utc' at time zone 'America/Sao_Paulo', 'DD/MM/YYYY HH24:MI:SS') as last_event}
              }

            ],
            order_by => {-desc => 'ts'}
          }
        )->as_hashref->all
      ]
    }
  );

}

1;
