package Trackware::Controller::API::TrackerPosition;

use Moose;
use utf8;
use JSON::XS;
use Geo::Coordinates::DecimalDegrees;
use DateTimeX::Easy qw(parse);
use DateTime;

use Trackware::Util qw(status_line);

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::TrackerPosition',
  object_key => 'tracker_position',

  update_roles => [qw/superadmin user admin-tracker/],
  create_roles => [qw/superadmin user admin-tracker/],
  delete_roles => [qw/superadmin user admin-tracker/],

  search_ok => {vehicle_id => 'Int', tracker_id => 'Int'}
);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('tracker_positions') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $tracker_position = $c->stash->{tracker_position};
  my %attrs            = $tracker_position->get_inflated_columns;
  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $attrs{$_}, }
          qw/
          id
          tracker_id
          lat
          lng
          vehicle_id
          speed
          sat_number
          hdop
          transaction
          tracker_reason_generator_code
          precision
          position_type
          /
      ),
      (map { $_ => $tracker_position->$_->datetime } qw/created_at event_date/)
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;
  my $tracker_position = $c->stash->{tracker_position};

  $tracker_position->delete;

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $tracker_position = $c->stash->{tracker_position};

  $tracker_position->execute($c, for => 'update', with => $c->req->params);
  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_position->id])
      ->as_string,
    entity => {
      vehicle_id => $tracker_position->vehicle_id,
      tracker_id => $tracker_position->tracker_id,
      id         => $tracker_position->id
    }
    ),
    $c->detach
    if $tracker_position;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') { }

sub list_GET {
  my ($self, $c) = @_;

  my $rs          = $c->stash->{collection};
  my %extra_attrs = ();

  $rs = $rs->search_rs(
    undef,
    {
      order_by => {-desc => 'me.event_date'},
      columns  => [
        qw(created_at lbs_raw tx cycle gprs gps interval),
        {
          event_date => \
q{to_char(me.event_date at time zone 'utc' at time zone 'America/Sao_Paulo', 'DD/MM/YYYY HH24:MI:SS') as last_event}
        }
      ]
    }
  );

  $self->status_ok(
    $c,
    entity => {
      tracker_positions => [
        map {
          $_->{status_line} = status_line($_);
          ($_->{address}) = $_->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
          $_
        } $rs->as_hashref->all
      ]
    }
  );

}

sub list_POST {
  my ($self, $c) = @_;

  my $tracker_position = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_position->id])
      ->as_string,
    entity => {id => $tracker_position->id}
  );
}

sub get_last_position : Chained('base') : PathPart('get_last_position') :
  Args(0) {
  my ($self, $c) = @_;

  my $rs = $c->model('DB::TrackerPosition');

  my @ids = decode_json($c->req->params->{ids});

  $rs = $rs->search_rs(
    {id => $c->req->params->{id}},
    {
      order_by => {-desc => 'event_date'},
      columns  => [
        qw(
          speed
          event_date
          vehicle_car_plate
          vehicle_id
          reason_generator
          created_at
          firmware_version
          position_counter
          precision
          position_type
          lat
          lng)
      ]
    }
  );

  $self->status_ok($c, entity => {position => $rs->as_hashref->next});
}

sub report : Chained('base') : Args(0) : GET {
  my ($self, $c) = @_;

  return $self->status_bad_request($c, message => 'invalid query')
    if !defined $c->req->params->{'q'} || length($c->req->params->{'q'}) < 4;

  my $tracker = $c->model('DB::Tracker')->search_rs(
    {
      -or => [
        map {
          +{
            $_ => {
              -ilike => \[q{ '%' || ? || '%' }, [_q => $c->req->params->{'q'}]]
            }
          }
        } qw(iccid phone_number code)
      ]
    },
    {rows => 1}
    )->next
    or return $self->status_not_found($c, message => 'Tracker not found');

  my $rs          = $tracker->tracker_positions_rs;
  my %extra_attrs = ();

  my ($tt_start, $tt_end);
  $tt_start = $c->req->params->{'tt-start'}
    if $c->req->params->{'tt-start'} && parse($c->req->params->{'tt-start'});

  $tt_end = $c->req->params->{'tt-end'}
    if $c->req->params->{'tt-end'} && parse($c->req->params->{'tt-end'});
  warn "$tt_start $tt_end";

  $rs = $rs->search_rs(
    {
      -and => [
        {
          'me.event_date' => {
            '>' => $tt_start
              || \q{(now() - interval '30 days') at time zone 'UTC'}
          }
        },
        {'me.event_date' => {'<=' => $tt_end || \q|now() at time zone 'UTC'|}}
      ]
    },
    {
      order_by => {-desc => 'me.event_date'},
      columns  => [
        qw(lat lng address tx cycle gprs gps interval),
        {
          event_date => \
q{to_char(me.event_date at time zone 'utc' at time zone 'America/Sao_Paulo', 'DD/MM/YYYY HH24:MI:SS') as last_event}
        }
      ]
    }
  );
  $rs = $rs->with_battery_level;

  $self->status_ok(
    $c,
    entity => {

      report => {
        tracker   => +{$tracker->get_columns},
        positions => [
          map {
            $_->{status_line} = status_line($_);
            ($_->{address}) = $_->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
            $_
          } $rs->as_hashref->all
        ]
      }
    }
  );

}

sub parse_position : Private() {
  my ($self, $position) = @_;

  my $g = substr $position, 0, 3;
  my $s = substr $position, 3, -1;

  return dm2decimal($g, $s);
}

1;
