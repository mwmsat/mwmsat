package Trackware::Controller::API::TrackerSmsPosition;

use Moose;
use utf8;
use JSON::XS;
use Geo::Coordinates::DecimalDegrees;
use DateTime;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::TrackerSmsPosition',
  object_key => 'tracker_sms_position',

  update_roles => [qw/superadmin user admin-tracker/],
  create_roles => [qw/superadmin user admin-tracker/],
  delete_roles => [qw/superadmin user admin-tracker/],

  search_ok => {vehicle_id => 'Int', tracker_id => 'Int'}
);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('tracker_sms_positions') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $tracker_sms_position = $c->stash->{tracker_sms_position};
  my %attrs                = $tracker_sms_position->get_inflated_columns;

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $attrs{$_}, }
          qw/
          tracker_id
          iccid
          header
          imei
          batery_voltage
          mcc
          mnc
          lac_1
          cellid_1
          rxlevel_1
          lac_2
          cellid_2
          rxlevel_2
          lac_3
          cellid_3
          rxlevel_3
          lac_4
          cellid_4
          rxlevel_4
          lac_5
          cellid_5
          rxlevel_5
          lac_6
          cellid_6
          rxlevel_6
          time_advance
          cycle
          tx_jammer
          mode_save
          ala_6
          emergency_mode
          ta_mode
          rf
          firmware_version
          sender
          receiver
          vehicle_id
          /
      ),
      (
        map { $_ => $tracker_sms_position->$_->datetime }
          qw/created_at event_date/
      )
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;
  my $tracker_sms_position = $c->stash->{tracker_sms_position};

  $tracker_sms_position->delete;

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $tracker_sms_position = $c->stash->{tracker_sms_position};

  $tracker_sms_position->execute($c, for => 'update', with => $c->req->params);
  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_sms_position->id])
      ->as_string,
    entity => {
      vehicle_id => $tracker_sms_position->vehicle_id,
      tracker_id => $tracker_sms_position->tracker_id,
      id         => $tracker_sms_position->id
    }
    ),
    $c->detach
    if $tracker_sms_position;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') { }

sub list_GET {
  my ($self, $c) = @_;

  my $rs = $c->stash->{collection};

  if ($c->req->params->{position}) {

    $rs = $c->model('DB::ViewVehicleLastPositionSms');

    my $data = $rs->search_rs(undef, {bind => [$c->req->params->{imei}]});

    my @params = map {
      my $r = $_;
      +{
        (
          map { $_ => $r->get_column($_) }
            qw/
            mcc
            mnc
            lac_1
            cellid_1
            lac_2
            cellid_2
            lac_3
            cellid_3
            lac_4
            cellid_4
            lac_5
            cellid_5
            lac_6
            cellid_6
            sender
            receiver
            event_date
            created_at
            time_advance
            firmware_version
            /
        ),
        },
    } $data->all;

    $self->calculate_perimeter($c, $params[0]);

  }
  else {
    $self->status_ok(
      $c,
      entity => {
        tracker_sms_positions => [
          map {
            my $r = $_;
            +{
              (
                map { $_ => $r->{$_} }
                  qw/
                  tracker_id
                  iccid
                  header
                  imei
                  batery_voltage
                  mcc
                  mnc
                  lac_1
                  cellid_1
                  rxlevel_1
                  lac_2
                  cellid_2
                  rxlevel_2
                  lac_3
                  cellid_3
                  rxlevel_3
                  lac_4
                  cellid_4
                  rxlevel_4
                  lac_5
                  cellid_5
                  rxlevel_5
                  lac_6
                  cellid_6
                  rxlevel_6
                  time_advance
                  cycle
                  tx_jammer
                  mode_save
                  ala_6
                  emergency_mode
                  ta_mode
                  rf
                  firmware_version
                  sender
                  receiver
                  vehicle_id
                  created_at
                  /
              ),
              url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
                ->as_string
              },
          } $rs->as_hashref->all
        ]
      }
    );
  }
}

sub list_POST {
  my ($self, $c) = @_;

  my $tracker_sms_position = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_sms_position->id])
      ->as_string,
    entity => {id => $tracker_sms_position->id}
  );
}

sub calculate_perimeter {
  my ($self, $c, $message) = @_;

  my $rs = $c->model('DB::ViewTrackerMessagePerimeter');

  my $data = $rs->search_rs(
    undef,
    {
      bind => [
        $message->{mcc},      $message->{mnc},      $message->{lac_1},
        $message->{lac_2},    $message->{lac_3},    $message->{lac_4},
        $message->{lac_5},    $message->{lac_6},    $message->{cellid_1},
        $message->{cellid_2}, $message->{cellid_3}, $message->{cellid_4},
        $message->{cellid_5}, $message->{cellid_6},
      ]
    }
  );

  $self->status_ok(
    $c,
    entity => {
      real_time_position => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->get_column($_) }
                qw/
                lat
                lng
                azimuth
                radius
                /
            ),
            },
        } $data->all
      ],
      info => {
        event_date       => $message->{event_date},
        receiver         => $message->{receiver},
        sender           => $message->{sender},
        created_at       => $message->{created_at},
        firmware_version => $message->{firmware_version},
        time_advance     => $message->{time_advance}
      }
    }
  );
}

1;
