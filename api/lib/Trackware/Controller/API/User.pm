package Trackware::Controller::API::User;

use Moose;
use JSON::XS;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::User',
  object_key => 'user',

  update_roles => [qw/superadmin webapi admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],
  search_ok    => {email => 'Str', order => 'Str', reset_password_key => 'Str',}

);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('users') : CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

my %_role_label_map = (
  operator      => 'Operador',
  agent         => 'Agente',
  superadmin    => 'Super Admin',
  'admin-unity' => 'Admin'
);

sub result_GET {
  my ($self, $c) = @_;


  my $user  = $c->stash->{user};
  my %attrs = $user->get_inflated_columns;

  $self->status_ok(
    $c,
    entity => {
      roles       => [map { $_->name } $user->roles],
      role_labels => [map { $_role_label_map{$_->name} || '--' } $user->roles],
      role_ids    => [map { $_->id } $user->roles],
      managed_business_units =>
        {map { ($_->id => 1) } $user->managed_business_units},

      managed_trackers => {map { ($_->id => 1) } $user->managed_trackers},

      map { $_ => $attrs{$_}, } qw/
        id
        name
        email
        active
        reset_password_key
        /
    }
  );
}

sub result_PUT {
  my ($self, $c) = @_;

  my $user   = $c->stash->{user};
  my $params = $c->req->params;

# if ($params->{change_roles}) {
#   my $rel = $c->model('DB::UserRole')->search({user_id => $user->id})->delete;
#   my @roles = decode_json($params->{roles});

  #   eval { $c->model('DB::UserRole')->populate($roles[0]); };

  #   delete $params->{roles};
  # }
  $params->{managed_business_units} =
    decode_json($params->{managed_business_units})
    if $params->{managed_business_units};

  $params->{managed_trackers} = decode_json($params->{managed_trackers})
    if $params->{managed_trackers};

  use DDP;
  p($params);
  $user->execute($c, for => 'update', with => $params);

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$user->id])->as_string,
    entity => {name => $user->name, id => $user->id, email => $user->email}
    ),
    $c->detach
    if $user;
}

sub result_DELETE {
  my ($self, $c) = @_;
  $self->status_bad_request($c, message => 'NO!'), $c->detach
    if $c->user->id == $c->stash->{user}->id;

  $self->status_bad_request($c, message => 'Senha incorreta'), $c->detach
    unless $c->user->obj->check_password($c->req->params->{password});

  my $user = $c->stash->{user};

  # $self->status_gone($c, message => 'deleted'), $c->detach
  #   unless $user->is_active;
  $user->$_->delete
    for
    qw(user_roles business_unity_users business_user_trackers customer_users user_addresses user_sessions);
  $user->delete;    #update({is_active => 0});

  $self->status_no_content($c);
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  my $rs = $c->stash->{collection};

  my $conditions;

  if ($c->req->params->{business_unity_id}) {
    my @registers =
      $rs->search_related_rs('business_unity_users',
      {business_unity_id => $c->req->params->{business_unity_id}},
    )->all;

    my @ids = map { $_->user_id } @registers;

    $conditions = {'me.id' => {'in' => \@ids}};
  }

  $rs = $c->stash->{collection}->search(
    $conditions ? {%{$conditions}} : undef,
    {
      '+columns' => [
        {
          'managed_business_units' => {
            ''  => \q{string_agg(business_unity.fancy_name, ', ')},
            -as => 'managed_business_units'
          }
        }
      ],

      distinct => 1,
      join     => {business_unity_users => 'business_unity'},
    }
  )->as_subselect_rs->search(
    undef,
    {
      '+columns' => [qw(managed_business_units)],
      prefetch   => [{user_roles => 'role'}]
    }
  );

  $self->status_ok(
    $c,
    entity => {
      users => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/managed_business_units id name email active reset_password_key created_at/
            ),
            roles => [map { $_->{role}{name} } @{$r->{user_roles}}],
            url   => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string
            }
        } $rs->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $params = {%{$c->req->params}};

  $c->model('DB')->txn_do(
    sub {
      my $user =
        $c->stash->{collection}->execute($c, for => 'create', with => $params);

      if ($c->req->params->{business_unity_id}) {
        $c->model('DB::BusinessUnityUser')->create(
          {
            business_unity_id => $c->req->params->{business_unity_id},
            user_id           => $user->id,
            is_active         => 1
          }
        );
      }

      $self->status_created(
        $c,
        location =>
          $c->uri_for($self->action_for('result'), [$user->id])->as_string,
        entity => {id => $user->id}
      );
    }
  );

}

sub complete : Chained('base') : PathPart('complete') : Args(0) {
  my ($self, $c) = @_;

  my $user;

  $c->model('DB')->txn_do(
    sub {
      my $address = $c->model('DB::Address')
        ->execute($c, for => 'create', with => $c->req->params);

      $c->req->params->{address_id} = $address->id;

      $user = $c->stash->{collection}
        ->execute($c, for => 'create', with => $c->req->params);
    }
  );

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$user->id])->as_string,
    entity => {id => $user->id}
  );

}

1;
