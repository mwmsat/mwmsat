package Trackware::Controller::API::VehicleModel;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::VehicleModel',
  object_key => 'vehicle_model',

  update_roles => [qw/superadmin admin admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],

  search_ok => {vehicle_brand_id => 'Int', order => 'Str'}

);

with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('vehicle_models') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $vehicle_model = $c->stash->{vehicle_model};
  my %attrs         = $vehicle_model->get_inflated_columns;
  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $attrs{$_}, }
          qw(
          id
          model
          vehicle_brand_id
          )
      ),
      (
        map { $_ => ($attrs{$_} ? $attrs{$_}->datetime : undef) }
          qw/created_at/
      ),

    }
  );
}

sub result_PUT {
  my ($self, $c) = @_;

  my $vehicle_model = $c->stash->{vehicle_model};

  $vehicle_model->execute($c, for => 'update', with => $c->req->params);
  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$vehicle_model->id])->as_string,
    entity => {model => $vehicle_model->model, id => $vehicle_model->id}
    ),
    $c->detach
    if $vehicle_model;
}

sub result_DELETE {
  my ($self, $c) = @_;

  my $vehicle_model = $c->stash->{vehicle_model};

  $vehicle_model->delete;
  $self->status_no_content($c);
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      vehicle_models => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                model
                vehicle_brand_id
                created_at
                /
            ),
            url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string
            }
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $vehicle_model = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$vehicle_model->id])->as_string,
    entity => {model => $vehicle_model->model, id => $vehicle_model->id}
  );

}

1;
