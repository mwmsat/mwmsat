package Trackware::Controller::API::Scheduler;

use Moose;
use namespace::autoclean;
use JSON qw(decode_json);
use Trackware::Util qw(rule_listing);

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(default => 'application/json');

sub base : Chained('/api/base') : PathPart(scheduler) : CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) {
  my ($self, $c, $id) = @_;
  $c->stash->{notification_id} = $id;
}

sub view : Chained(object) : PathPart('') : Args(0) GET {
  my ($self, $c) = @_;
  $self->status_ok(
    $c,
    entity => {
      notification => (
        map {
          $_->{rule_list} =
            [rule_listing($_->{rule} = decode_json($_->{rule}))];
          $_
        } $c->model('DB::Notification')
          ->search_rs({id => $c->stash->{notification_id}})->as_hashref->next
      ),
      notifications => [
        $c->model('DB::TrackerNotification')->search_rs(
          {'me.notification_id' => $c->stash->{notification_id}},
          {
            join       => 'tracker',
            '+columns' => [
              {iccid        => 'tracker.iccid'},
              {phone_number => 'tracker.phone_number'},
              {code         => 'tracker.code'}
            ],
            prefetch => [
              'notification',
              {
                tracker_notification_sms =>
                  ['ack_config_position', 'sms_tracker']
              }
            ],
            result_class => 'DBIx::Class::ResultClass::HashRefInflator'
          }
        )->all
      ]
    }
  );
}


sub list : Chained(base) PathPart('') : Args(0) GET {
  my ($self, $c) = @_;
  $self->status_ok(
    $c,
    entity => {
      results => [
        map {
          $_->{rule_list} =
            [rule_listing($_->{rule} = decode_json($_->{rule}))];
          $_
        } $c->model('DB::NotificationReport')->as_hashref->all
      ]
    }
  );
}

1;
