package Trackware::Controller::API::TrackerType;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default    => 'application/json',
  result     => 'DB::TrackerType',
  object_key => 'tracker_type',
);
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::Search';
with 'Trackware::TraitFor::Controller::AutoObject';

sub base : Chained('/api/base') : PathPart('tracker-type') : CaptureArgs(0) { }

sub object : Chained(base) PathPart('') : CaptureArgs(1) { }

sub view : Chained(object) : PathPart('') : Args(0) GET {
  my ($self, $c) = @_;
  $c->stash->{object}->reset;

  $self->status_ok($c, entity => $c->stash->{object}->as_hashref->next);
}

sub edit : Chained(object) : PathPart('') : Args(0) PUT {
  my ($self, $c) = @_;

  my $tracker_type = $c->stash->{tracker_type}
    ->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted($c, entity => $tracker_type,);

}

sub remove : Chained(object) : PathPart('') Args(0) DELETE {
  my ($self, $c) = @_;

  $self->status_bad_request($c, message => 'Senha incorreta'), $c->detach
    unless $c->user->obj->check_password($c->req->params->{password});

  $c->stash->{object}->delete;
  $self->status_accepted($c, entity => {ok => 1});
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {

}

sub list_POST {
  my ($self, $c) = @_;

  my $tracker_type = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('view'), [$tracker_type->id])->as_string,
    entity => {id => $tracker_type->id}
  );

}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      items => [$c->stash->{collection}->search_rs(undef, {})->as_hashref->all]
    }
  );

}

1;
