package Trackware::Controller::API::FuelType;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result    => 'DB::FuelType',
  search_ok => {order => 'Str'}
);
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::Search';

sub base : Chained('/api/base') : PathPart('fuel_types') : CaptureArgs(0) { }

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      fuel_types => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                type
                /
            ),
            }
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );

}

1;
