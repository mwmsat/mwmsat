package Trackware::Controller::API::SmsStatus;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result => 'DB::TrackerStatus',
);
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::Search';

sub base : Chained('/api/base') : PathPart('sms_status') : CaptureArgs(0) { }

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      sms_status => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                status
                /
            ),
            }
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );

}

1;
