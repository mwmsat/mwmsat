package Trackware::Controller::API::BusinessUnityUsers;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result      => 'DB::BusinessUnityUser',
  object_key  => 'business_unity_user',
  search_ok   => {user_id => 'Int', business_unity_id => 'Int'},
  result_attr => {
    distinct => 1,
    prefetch => ['business_unity' => {'user' => {user_roles => 'role'}}]
  },

  update_roles => [qw/superadmin user admin admin-unity/],
  create_roles => [qw/superadmin user webapi admin admin-unity/],
  delete_roles => [qw/superadmin user admin admin-unity/],
);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('business_unity_users') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $business_unity_users = $c->stash->{business_unity_user};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $business_unity_users->$_ }
          qw/
          id
          user_id
          is_active
          /
      ),
      business_unity => {
        (
          map { $_ => $business_unity_users->business_unity->$_ }
            qw/
            id
            corporate_name
            /
        ),
      }
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;

  my $business_unity_users = $c->stash->{business_unity_user};

  $business_unity_users->delete;

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $business_unity_users = $c->stash->{business_unity_user};

  $business_unity_users->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$business_unity_users->id])
      ->as_string,
    entity => {
      user_id => $business_unity_users->user_id,
      id      => $business_unity_users->id
    }
    ),
    $c->detach
    if $business_unity_users;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      business_unity_users => [
        $c->stash->{collection}
          ->search_rs({'role.name' => {-in => [qw(operator agent)]}})
          ->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $business_unity_user = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$business_unity_user->id])
      ->as_string,
    entity => {id => $business_unity_user->id}
  );

}

1;
