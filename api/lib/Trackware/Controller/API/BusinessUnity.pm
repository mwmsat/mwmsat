package Trackware::Controller::API::BusinessUnity;

use utf8;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result      => 'DB::BusinessUnity',
  object_key  => 'business_unity',
  result_attr => {prefetch => [{'address' => {'city' => 'state'}}]},
  search_ok   => {order => 'Str'},

  update_roles => [qw/superadmin user admin/],
  create_roles => [qw/superadmin user webapi admin/],
  delete_roles => [qw/superadmin user admin/],
);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('business_units') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $business_unity = $c->stash->{business_unity};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $business_unity->$_ }
          qw/
          id
          corporate_name
          code
          fancy_name
          register
          website
          email
          phone
          phone_2
          parent_id
          domain
          /
      ),
      address => {
        (
          map { $_ => $business_unity->address->$_ }
            qw/
            id
            address
            number
            neighborhood
            postal_code
            complement
            lat_lng
            /
        ),
        city => {
          (
            map { $_ => $business_unity->address->city->$_ }
              qw/
              id
              name
              /
          ),
          state => {
            (
              map { $_ => $business_unity->address->city->state->$_ }
                qw/
                id
                name
                country_id
                /
            )
          }
        }
      },
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;

  my $business_unity = $c->stash->{business_unity};

  $business_unity->update({is_active => 0});

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $business_unity = $c->stash->{business_unity};
  $business_unity->execute($c, for => 'update', with => $c->req->params);
  $self->status_accepted(
    $c,
    location => $c->uri_for($self->action_for('result'), [$business_unity->id])
      ->as_string,
    entity => {id => $business_unity->id}
    ),
    $c->detach
    if $business_unity;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET : Private {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      business_units => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                corporate_name
                fancy_name
                register
                website
                code
                email
                phone
                phone_2
                parent_id
                domain
                /
            ),
            address => {
              (
                map { $_ => $r->{address}{$_} }
                  qw/
                  id
                  address
                  number
                  neighborhood
                  postal_code
                  complement
                  lat_lng
                  /
              ),
              city => {
                (
                  map { $_ => $r->{address}{city}{$_} }
                    qw/
                    id
                    name
                    /
                ),
                state => {
                  (
                    map { $_ => $r->{address}{city}{state}{$_} }
                      qw/
                      id
                      name
                      country_id
                      /
                  )
                }
              }
            },
            },
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $business_unity = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location => $c->uri_for($self->action_for('result'), [$business_unity->id])
      ->as_string,
    entity => {id => $business_unity->id}
  );

}

sub pre_registration_create : Chained(object) : PathPart('pre-registration') :
  Args(0) POST {
  my ($self, $c) = @_;

  $self->status_bad_request($c, message => 'Senha incorreta'), $c->detach
    unless $c->user->obj->check_password($c->req->params->{password});

  my $n                   = $c->req->params->{count};
  my $network_supplier_id = $c->req->params->{network_supplier_id};

  $self->status_bad_request($c, message => 'Quantidade inválida'), $c->detach
    if !$n || $n < 1 || $n > 1_000;

  $self->status_bad_request($c, message => 'Operadora não existe'), $c->detach
    if $network_supplier_id
    && !$c->model('DB::Networksupplier')->find($network_supplier_id);

  $c->stash->{business_unity}
    ->pre_registrations->allocate($n, $network_supplier_id);

  $c->forward('pre_registration_list');

}

sub pre_registration_list : Chained(object) : PathPart('pre-registration') :
  Args(0) GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      results => [
        $c->stash->{business_unity}
          ->pre_registrations->with_trackers->with_network_supplier->as_hashref
          ->all
      ]
    }
  );
}

sub managed_by_me : Chained(base) : Args(0) GET {
  my ($self, $c) = @_;
  $c->stash->{collection} =
    $c->user->business_unity_users->related_resultset('business_unity');
  $c->forward('list_GET');
}

sub complete : Chained('base') : PathPart('complete') : Args(0) {
  my ($self, $c) = @_;

  my $business_unity;

  $c->model('DB')->txn_do(
    sub {
      my $address = $c->model('DB::Address')
        ->execute($c, for => 'create', with => $c->req->params);

      $c->req->params->{address_id} = $address->id;

      $business_unity = $c->stash->{collection}
        ->execute($c, for => 'create', with => $c->req->params);
    }
  );

  $self->status_created(
    $c,
    location => $c->uri_for($self->action_for('result'), [$business_unity->id])
      ->as_string,
    entity => {id => $business_unity->id}
  );

}

1;
