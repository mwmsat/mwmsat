package Trackware::Controller::API::TrackerCommandLayout;

use Moose;
use utf8;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::TrackerCommandLayout',
  object_key => 'tracker_command_layout',

  update_roles => [qw/superadmin admin admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],
  search_ok    => {business_unity_id => 'Int', order => 'Str'}

);

with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('tracker_command_layouts') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $tracker_command_layout = $c->stash->{tracker_command_layout};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $tracker_command_layout->$_, }
          qw/
          id
          name
          command
          is_active
          business_unity_id
          /
      ),
      (
        map {
              $_ => $tracker_command_layout->$_
            ? $tracker_command_layout->$_->datetime
            : undef
        } qw/created_at/
      ),
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;

  my $tracker_command_layout = $c->stash->{tracker_command_layout};

  $tracker_command_layout->delete;

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $tracker_command_layout = $c->stash->{tracker_command_layout};

  $tracker_command_layout->execute(
    $c,
    for  => 'update',
    with => $c->req->params
  );

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_command_layout->id])
      ->as_string,
    entity => {id => $tracker_command_layout->id}
    ),
    $c->detach
    if $tracker_command_layout;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  my $rs = $c->stash->{collection};

  if ($c->req->params->{business_unity_id}) {
    $rs = $rs->search(
      {
        'me.business_unity_id' =>
          ['-or' => [undef, $c->req->params->{business_unity_id}]]
      }
    );

  }

  $self->status_ok(
    $c,
    entity => {
      tracker_command_layouts => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                name
                command
                is_active
                created_at
                business_unity_id
                /
            ),
            }
        } $rs->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $tracker_command_layout = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker_command_layout->id])
      ->as_string,
    entity => {id => $tracker_command_layout->id}
  );

}

1;
