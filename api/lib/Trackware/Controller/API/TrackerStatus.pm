package Trackware::Controller::API::TrackerStatus;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result => 'DB::TrackerStatus',
);
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::Search';

sub base : Chained('/api/base') : PathPart('tracker_status') :
  CaptureArgs(0) { }

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      tracker_status => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                status
                description
                /
            ),
            }
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );

}

1;
