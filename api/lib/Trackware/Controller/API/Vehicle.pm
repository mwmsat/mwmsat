package Trackware::Controller::API::Vehicle;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result      => 'DB::Vehicle',
  object_key  => 'vehicle',
  result_attr => {
    prefetch => [
      {'vehicle_model' => 'vehicle_brand'}, {'city' => 'state'},
      'vehicle_color',  'fuel_type',
      'business_unity', 'customer',
      'vehicle_fleet'
    ]
  },

  update_roles => [qw/superadmin admin-unity admin/],
  create_roles => [qw/superadmin admin-unity admin/],
  delete_roles => [qw/superadmin admin-unity admin/],

  search_ok => {
    vehicle_fleet_id  => 'Int',
    business_unity_id => 'Int',
    customer_id       => 'Int',
    car_plate         => 'Str',
    }

);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('vehicles') : CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $vehicle = $c->stash->{vehicle};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $vehicle->$_ }
          qw/
          id
          car_plate
          doors_number
          manufacture_year
          model_year
          km
          insurance_company_id

          /
      ),
      (
        map { $_ => ($vehicle->$_ ? $vehicle->$_->datetime : undef) }
          qw/created_at/
      ),
      fuel_type => $vehicle->fuel_type
      ? {(map { $_ => $vehicle->fuel_type->$_ } qw/id type/),}
      : undef,
      business_unity => $vehicle->business_unity
      ? {(map { $_ => $vehicle->business_unity->$_ } qw/id fancy_name/),}
      : undef,
      vehicle_color => $vehicle->vehicle_color
      ? {(map { $_ => $vehicle->vehicle_color->$_ } qw/id color/),}
      : undef,
      vehicle_model => $vehicle->vehicle_model
      ? {
        (map { $_ => $vehicle->vehicle_model->$_ } qw/id model/),
        vehicle_brand => {
          (
            map { $_ => $vehicle->vehicle_model->vehicle_brand->$_ }
              qw/id brand/
          ),
        },
        }
      : undef,
      customer => $vehicle->customer
      ? {(map { $_ => $vehicle->customer->$_ } qw/id corporate_name/),}
      : undef,
      vehicle_fleet => $vehicle->vehicle_fleet
      ? {(map { $_ => $vehicle->vehicle_fleet->$_ } qw/id name/),}
      : undef,
      city => {
        (map { $_ => $vehicle->city->$_ } qw/id name/),
        state => {(map { $_ => $vehicle->city->state->$_ } qw/id uf/),}
      }
    }
  );
}

sub result_PUT {
  my ($self, $c) = @_;

  my $vehicle = $c->stash->{vehicle};

  $vehicle->execute(
    $c,
    for  => 'update',
    with => {%{$c->req->params}, created_by => $c->user->id}
  );
  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$vehicle->id])->as_string,
    entity => {id => $vehicle->id}
    ),
    $c->detach
    if $vehicle;
}

sub result_DELETE {
  my ($self, $c) = @_;

  my $vehicle = $c->stash->{vehicle};

  $vehicle->delete;
  $self->status_no_content($c);
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;
  my %args = %{$c->req->params};

  my $rs = $c->stash->{collection};
  if ($c->req->params->{model}) {
    $rs = $rs->search(
      {
        'vehicle_model.model' =>
          {-ilike => '%' . $c->req->params->{model} . '%'}
      }
    );
  }
  if ($c->req->params->{plate}) {
    $rs = $rs->search(
      {car_plate => {-ilike => '%' . $c->req->params->{plate} . '%'}});
  }
  if ($c->req->params->{client}) {
    $rs = $rs->search(
      {
        'customer.corporate_name' =>
          {-ilike => '%' . $c->req->params->{client} . '%'}
      }
    );
  }
  if ($c->req->params->{fleet}) {
    $rs = $rs->search(
      {
        'vehicle_fleet.name' => {-ilike => '%' . $c->req->params->{fleet} . '%'}
      }
    );
  }

  $rs = $rs->search({}, {order_by => 'me.car_plate'});
  $self->status_ok(
    $c,
    entity => {
      vehicles => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                car_plate
                doors_number
                manufacture_year
                model_year
                km
                insurance_company_id
                created_at
                customer_id
                /
            ),
            business_unity => $r->{business_unity}
            ? {(map { $_ => $r->{business_unity}{$_} } qw/id fancy_name/),}
            : undef,
            fuel_type => $r->{fuel_type}
            ? {(map { $_ => $r->{fuel_type}{$_} } qw/id type/),}
            : undef,
            vehicle_color => $r->{vehicle_color}
            ? {(map { $_ => $r->{vehicle_color}{$_} } qw/id color/),}
            : undef,
            vehicle_model => {
              (map { $_ => $r->{vehicle_model}{$_} } qw/id model/),
              vehicle_brand => {
                (
                  map { $_ => $r->{vehicle_model}{vehicle_brand}{$_} }
                    qw/id brand/
                )
              },
            },
            customer => $r->{customer}
            ? {(map { $_ => $r->{customer}{$_} } qw/id corporate_name/),}
            : undef,
            vehicle_fleet => $r->{vehicle_fleet}
            ? {(map { $_ => $r->{vehicle_fleet}{$_} } qw/id name/),}
            : undef,
            city => {
              (map { $_ => $r->{city}{$_} } qw/id name/),
              state => {(map { $_ => $r->{city}{state}{$_} } qw/id uf/),}
            },
            url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string,
            }
        } $rs->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $vehicle = $c->stash->{collection}->execute(
    $c,
    for  => 'create',
    with => {%{$c->req->params}, created_by => $c->user->id}
  );

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$vehicle->id])->as_string,
    entity => {id => $vehicle->id, car_plate => $vehicle->car_plate}
  );

}

1;
