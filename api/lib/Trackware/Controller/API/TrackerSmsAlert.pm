package Trackware::Controller::API::TrackerSmsAlert;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default    => 'application/json',
  result     => 'DB::TrackerSmsAlert',
  object_key => 'tracker_sms_alert',
);
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::Search';
with 'Trackware::TraitFor::Controller::AutoObject';

sub base : Chained('/api/base') : PathPart('tracker-sms-alert') :
  CaptureArgs(0) { }

sub object : Chained(base) PathPart('') : CaptureArgs(1) { }

sub view : Chained(object) : PathPart('') : Args(0) GET {
  my ($self, $c) = @_;
  $c->stash->{object}->reset;
  $self->status_ok($c, entity => $c->stash->{object}->as_hashref->next);
}

sub edit : Chained(object) : PathPart('') : Args(0) PUT {
  my ($self, $c) = @_;

  my $tacker_sms_alert = $c->stash->{tacker_sms_alert}
    ->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted($c, entity => $tacker_sms_alert,);

}

sub remove : Chained(object) : PathPart('') Args(0) DELETE {
  my ($self, $c) = @_;

  # $self->status_bad_request( $c, message => 'Senha incorreta' ), $c->detach
  #   unless $c->user->obj->check_password( $c->req->params->{password} );

  $c->stash->{object}->delete;
  $self->status_accepted($c, entity => {ok => 1});
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {

}

sub list_POST {
  my ($self, $c) = @_;

  my $tacker_sms_alert = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location => $c->uri_for($self->action_for('view'), [$tacker_sms_alert->id])
      ->as_string,
    entity => {id => $tacker_sms_alert->id}
  );

}

my %destination_phone_numbers_name_map =
  ("11975086326" => 'Teste', "11988612664" => 'Passarinho');


sub _destination_phone_numbers_list {
  [
    map { s/^\s+|\s+$//g; $destination_phone_numbers_name_map{"$_"} || $_ }
      split /;/,
    shift
  ]
}


sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      items => [
        map {
          +{
            %$_,
            destination_phone_numbers_list =>
              _destination_phone_numbers_list($_->{destination_phone_numbers})
            }
        } $c->stash->{collection}->search_rs(undef, {})
          ->with_tracker->as_hashref->all
      ]
    }
  );

}

1;
