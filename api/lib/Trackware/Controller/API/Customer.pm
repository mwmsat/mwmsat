package Trackware::Controller::API::Customer;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result      => 'DB::Customer',
  object_key  => 'customer',
  result_attr => {prefetch => [{'address' => {'city' => 'state'}}]},
  search_ok => {order => 'Str', business_unity_id => 'Int', is_active => 'Int'},

  update_roles => [qw/superadmin user admin/],
  create_roles => [qw/superadmin user webapi admin/],
  delete_roles => [qw/superadmin user admin/],
);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('customers') : CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $customer = $c->stash->{customer};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $customer->$_ }
          qw/
          id
          corporate_name
          fancy_name
          parent_id
          business_unity_id
          is_active
          phone
          mobile_phone
          email
          website
          register
          /
      ),
      address => {
        (
          map { $_ => $customer->address->$_ }
            qw/
            id
            address
            number
            neighborhood
            postal_code
            complement
            lat_lng
            /
        ),
        city => {
          (
            map { $_ => $customer->address->city->$_ }
              qw/
              id
              name
              /
          ),
          state => {
            (
              map { $_ => $customer->address->city->state->$_ }
                qw/
                id
                name
                country_id
                /
            )
          }
        }
      },
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;

  my $customer = $c->stash->{customer};

  $customer->update({is_active => 0});

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $customer = $c->stash->{customer};

  $customer->execute($c, for => 'update', with => $c->req->params);
  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$customer->id])->as_string,
    entity => {id => $customer->id}
    ),
    $c->detach
    if $customer;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      customers => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                corporate_name
                fancy_name
                parent_id
                business_unity_id
                is_active
                phone
                mobile_phone
                email
                website
                register
                /
            ),
            address => {
              (
                map { $_ => $r->{address}{$_} }
                  qw/
                  id
                  address
                  number
                  neighborhood
                  postal_code
                  complement
                  lat_lng
                  /
              ),
              city => {
                (
                  map { $_ => $r->{address}{city}{$_} }
                    qw/
                    id
                    name
                    /
                ),
                state => {
                  (
                    map { $_ => $r->{address}{city}{state}{$_} }
                      qw/
                      id
                      name
                      country_id
                      /
                  )
                }
              }
            },
            },
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $customer = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$customer->id])->as_string,
    entity => {id => $customer->id}
  );

}

sub complete : Chained('base') : PathPart('complete') : Args(0) {
  my ($self, $c) = @_;

  my $customer;

  $c->model('DB')->txn_do(
    sub {
      my $address = $c->model('DB::Address')
        ->execute($c, for => 'create', with => $c->req->params);

      $c->req->params->{address_id} = $address->id;

      $customer = $c->stash->{collection}
        ->execute($c, for => 'create', with => $c->req->params);
    }
  );

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$customer->id])->as_string,
    entity => {id => $customer->id}
  );

}

sub update_complete : Chained('base') : PathPart('update_complete') : Args(1) {
  my ($self, $c, $id) = @_;

  my $customer = $c->model('DB::Customer')->search({id => $id})->next;
  my $address =
    $c->model('DB::Address')->search({id => $customer->address_id})->next;

  $c->model('DB')->txn_do(
    sub {
      $address =
        $address->execute($c, for => 'update', with => $c->req->params);
      $customer =
        $customer->execute($c, for => 'update', with => $c->req->params);
    }
  );

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$customer->id])->as_string,
    entity => {id => $customer->id}
    ),
    $c->detach
    if $customer;
}

1;
