package Trackware::Controller::API::VehicleFleet;

use Moose;
use Geo::Coordinates::DecimalDegrees;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::VehicleFleet',
  object_key => 'vehicle_fleet',

  update_roles => [qw/superadmin admin admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],
  result_attr  => {prefetch => ['business_unity', 'customer']},

  search_ok =>
    {business_unity_id => 'Int', customer_id => 'Int', order => 'Str'}
);
with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('vehicle_fleets') :
  CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $vehicle_fleet = $c->stash->{vehicle_fleet};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $vehicle_fleet->$_, }
          qw/
          id
          name
          customer_id
          /
      ),
      (map { $_ => $vehicle_fleet->$_->datetime } qw/created_at/),
      business_unity => {
        (
          map { $_ => $vehicle_fleet->business_unity->$_, }
            qw/
            id
            fancy_name
            corporate_name
            /
        ),
      },
      customer => {
        (
          map { $_ => $vehicle_fleet->customer->$_, }
            qw/
            id
            fancy_name
            corporate_name
            /
        ),
      }
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;

  my $vehicle_fleet = $c->stash->{vehicle_fleet};

  $c->model('DB')->txn_do(
    sub {
      $c->model('DB::Vehicle')
        ->search({vehicle_fleet_id => $vehicle_fleet->id})
        ->update_all(vehicle_fleet_id => undef);

      $vehicle_fleet->delete;
    }
  );

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $vehicle_fleet = $c->stash->{vehicle_fleet};

  $vehicle_fleet->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$vehicle_fleet->id])->as_string,
    entity => {id => $vehicle_fleet->id}
    ),
    $c->detach
    if $vehicle_fleet;
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') { }

sub list_GET {
  my ($self, $c) = @_;
  my $rs = $c->stash->{collection};
  if ($c->req->params->{client}) {
    $rs = $rs->search(
      {
        'customer.corporate_name' =>
          {-ilike => '%' . $c->req->params->{client} . '%'}
      }
    );
  }
  if ($c->req->params->{name}) {
    $rs =
      $rs->search({name => {-ilike => '%' . $c->req->params->{name} . '%'}});
  }

  $rs = $rs->search({}, {order_by => 'customer.corporate_name'});
  $self->status_ok(
    $c,
    entity => {
      vehicle_fleets => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                name
                created_at
                customer_id
                /
            ),
            business_unity => {
              (
                map { $_ => $r->{business_unity}{$_} }
                  qw/
                  id
                  fancy_name
                  corporate_name
                  /
              )
            },
            customer => {
              (
                map { $_ => $r->{customer}{$_} }
                  qw/
                  id
                  fancy_name
                  corporate_name
                  /
              )
            },
            url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string
            },
        } $rs->as_hashref->all
      ]
    }
  );

}

sub list_POST {
  my ($self, $c) = @_;

  my $vehicle_fleet = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$vehicle_fleet->id])->as_string,
    entity => {id => $vehicle_fleet->id}
  );
}

1;
