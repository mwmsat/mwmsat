package Trackware::Controller::API::Tracker;

use utf8;
use Moose;
use JSON::XS qw(encode_json);
use Trackware::Supplier;
use Trackware::Util qw(status_line);
BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::Tracker',
  object_key => 'tracker',

  update_roles => [qw/superadmin admin admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],
  result_cond  => {active => 1},
  result_attr  => {

# '+columns' => [
#   {
#     last_event => {
#       '' => \
#         q{to_char( MAX(tracker_positions.event_date), 'DD/MM/YYYY HH24:MI:SS')},
#       -as => 'last_event'
#     }
#   }
# ],
# distinct => 1,
# join     => 'tracker_positions',

#    prefetch => [ 'business_unity', 'network_supplier',]
  },
  search_ok => {
    vehicle_id => 'Int',
    imei       => 'Int',

#    business_unity_id => 'Any',
    tracker_status_id => 'Int',
    iccid             => 'Str',
    code              => 'Str',
    order             => 'Str'
  },
);

with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('trackers') : CaptureArgs(0) {
  my ($self, $c) = @_;

  $c->stash->{collection} = $c->model('DB::Tracker')
    if $c->check_user_roles('superadmin');

  $c->stash->{collection} =
    $c->user->obj->business_unity_users->related_resultset('business_unity')
    ->related_resultset('trackers')
    if $c->check_any_user_role(qw(admin-unity operator));

  $c->stash->{collection} =
    $c->user->obj->business_user_trackers->related_resultset('tracker')
    if $c->check_user_roles('agent');

  $c->stash->{collection} = $c->stash->{collection}
    ->search_rs($self->config->{result_cond}, $self->config->{result_attr});
}

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub remove : Chained('object') : PathPart('') : Args(0) : DELETE {
  my ($self, $c) = @_;

  $self->status_bad_request($c, message => 'Senha incorreta'), $c->detach
    unless $c->user->obj->check_password($c->req->params->{password});

  $c->stash->{object}->update({active => 0});
  $self->status_no_content($c);
}

sub result_GET {
  my ($self, $c) = @_;

  my $tracker = $c->stash->{tracker};

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $tracker->$_, }
          qw/
          id
          imei
          vehicle_id
          iccid
          phone_number
          code
          rf_note
          /
      ),
      (map { $_ => $tracker->$_->datetime } qw/created_at/),
      tracker_status => {
        (
          map { $_ => $tracker->tracker_status->$_, }
            qw/
            id
            status
            description
            /
        ),
      },
      business_unity => $tracker->business_unity
      ? {
        (
          map { $_ => $tracker->business_unity->$_, }
            qw/
            id
            corporate_name
            fancy_name
            /
        ),
        }
      : undef,
      customer => $tracker->customer
      ? {
        (
          map { $_ => $tracker->customer->$_, }
            qw/
            id
            corporate_name
            fancy_name
            /
        ),
        }
      : undef,
      vehicle => $tracker->vehicle
      ? {
        (
          map { $_ => $tracker->vehicle->$_, }
            qw/
            id
            car_plate
            /
        ),
        }
      : undef,
      network_supplier => $tracker->network_supplier
      ? {
        (
          map { $_ => $tracker->network_supplier->$_, }
            qw/
            id
            name
            /
        )
        }
      : undef,
      tracker_model => $tracker->tracker_model
      ? {
        (
          map { $_ => $tracker->tracker_model->$_, }
            qw/
            id
            name
            /
        )
        }
      : undef,
    }
  );

}

sub result_DELETE {
  my ($self, $c) = @_;

  my $tracker = $c->stash->{tracker};

  $tracker->update({tracker_status_id => 3});

  $self->status_no_content($c);
}

sub result_PUT {
  my ($self, $c) = @_;

  my $tracker = $c->stash->{tracker};

  die 'Cannot edit' unless $tracker->business_unity_id;

  $tracker->execute($c, for => 'update', with => $c->req->params);

  #	processo para atualizar o cache no redis
  if ($tracker) {
    my $tracker_cache = $c->model('TrackingCache');
    my $verb          = 'delete';
    my $vehicle_id    = undef;

    if ($c->req->params->{vehicle_id}) {
      $verb       = 'post';
      $vehicle_id = $c->req->params->{vehicle_id};
    }

    $tracker_cache->update_cache($tracker->imei, $tracker->id, $vehicle_id,
      $verb);
  }

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker->id])->as_string,
    entity => {vehicle_id => $tracker->vehicle_id, id => $tracker->id}
    ),
    $c->detach
    if $tracker;
}

sub filter : Chained('base') : PathPart('filter') : Args(0) : POST {
  my ($self, $c) = @_;
  use DDP;
  my @trackers      = $c->req->params->{trackers} =~ /(\d+)/g;
  my @phone_numbers = grep { /\d{12,14}/ } @trackers;
  my @iccids        = grep { /\d{19,22}/ } @trackers;

  return $self->status_not_found($c, message => 'Not found')
    unless @phone_numbers || @iccids;

  $self->status_ok(
    $c,
    entity => {
      trackers => [
        map {
          $_->{status_line} = status_line($_);
          ($_->{address}) = $_->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
          $_
        } $c->stash->{collection}->search_rs(
          {
            (('me.phone_number' => {-in => \@phone_numbers}) x
                !!@phone_numbers),
            (('me.iccid' => {-in => \@iccids}) x !!@iccids)
          }
        )->related_resultset('last_position')
          ->search_rs({}, {order_by => {-desc => 'last_position.event_date'}})
          ->as_hashref->all
      ]
    }
  );

}


sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  if (my $api_key = $c->req->params->{business_unity_api_key}) {
    if (my $bu = $c->model('DB::BusinessUnity')->find({api_key => $api_key})) {
      $c->req->params->{business_unity_id} = $bu->id;
    }
  }

  my ($rs, $args);
  if (my $business_unity_id = $c->req->params->{business_unity_id}) {
    $args = {
      (
        ('last_position.business_unity_id' => $business_unity_id) x
          !!$business_unity_id
      ),
      (
        ('last_position.business_unity_id' => undef) x
          !!($business_unity_id && $business_unity_id eq 'any')
      ),
    };
    $rs = $c->stash->{collection};

  }

  if (my $multitrack_id = $c->req->params->{multitrack_id}) {
    $rs = $c->model('DB::MultiTrack')->search_rs('me.id' => $multitrack_id)
      ->related_resultset('multitrack_trackers')->related_resultset('tracker');
  }


  $self->status_ok(
    $c,
    entity => {
      trackers => [
        map {
          $_->{status_line} = status_line($_);
          ($_->{address}) = $_->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
          $_
        } $rs->related_resultset('last_position')
          ->search_rs($args,
          {order_by => {-desc => 'last_position.event_date'}})->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $tracker = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  # disponibiliza rastreador mesmo se nao vinculado a nenhum veiculo
  if ($tracker) {
    my $tracker_cache = $c->model('TrackingCache');

    $tracker_cache->update_cache($tracker->imei, $tracker->id,
      $c->req->params->{vehicle_id} ? $c->req->params->{vehicle_id} : undef,
      'post');
  }

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$tracker->id])->as_string,
    entity => {id => $tracker->id}
  );

}

sub assign_command : Chained('base') : PathPart('assign_commands') : Args(0) {
  my ($self, $c) = @_;

  my $tracker =
    $c->model('DB::Tracker')->search({id => $c->req->params->{tracker_id}})
    ->next;
  my $command = $c->model('DB::TrackerCommandLayout')
    ->search({id => $c->req->params->{command}})->next;

  my $message = $c->model('DB::SmsTracker')->create(
    {
      tracker_id          => $tracker->id,
      message             => $command->command,
      network_supplier_id => $tracker->network_supplier_id,
      send_type           => $c->req->params->{send_type} == 0 ? 'tcp' : 'sms',
    }
  );

  my $assign_command;

  if ($c->req->params->{send_type} == 0) {
    my $tracker_cache = $c->model('TrackingCache');

    $assign_command =
      $tracker_cache->assign_command($tracker->imei, $command->command);
  }
  else {
    $assign_command = $self->sender($c, $tracker, $command, $message->id);
  }

  $self->status_accepted(
    $c, entity => {tracker_id => $tracker->id, command_id => $command->id}
    ),
    $c->detach
    if $assign_command;
}

sub sms : Chained('object') : CaptureArgs(0) {
  my ($self, $c) = @_;

  $c->stash->{collection} = $c->stash->{tracker}
    ->smses_rs->search_rs({}, {order_by => {-desc => 'created_at'},});
}

sub last_sms : Chained('sms') : PathPart('last-sms') : Args(0) GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      results => [
        $c->stash->{collection}->search_rs(
          undef,
          {
            columns => [
              'message',
              {
                created_at => \
                  q{to_char(me.created_at, 'DD/MM/YYYY HH24:MI:SS')}
              }

            ],
            rows => 300
          }
        )->as_hashref->all
      ]
    }
  );

}

sub delivery : Chained('object') : PathPart('delivery-status') : Args(0) {
  my ($self, $c) = @_;

  if (my $last_sms = $c->stash->{tracker}->sms_trackers->last->next) {
    $self->status_ok($c, entity => +{$last_sms->get_columns});
  }
  else {
    $self->status_ok($c, entity => +{});
  }
}

sub position : Chained('object') : CaptureArgs(0) {
  my ($self, $c) = @_;

  $c->stash->{collection} =
    $c->stash->{tracker}->tracker_positions_rs->search_rs(
    undef,    #    {'me.event_date' => {'>' => \q{now() - interval '45 days'}}},
    {
      '+columns' => [
        {
          event_date_tz => \
q{to_char(me.event_date at time zone 'utc' at time zone 'America/Sao_Paulo', 'DD/MM/YYYY HH24:MI:SS') as last_event}
        }
      ],
      order_by => {-desc => 'event_date'},
    }
    );

}

sub last_position : Chained('position') : PathPart('last') : Args(0) GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      position => map {
        $_->{status_line} = status_line($_);
        ($_->{address}) = $_->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
        $_
      } $c->stash->{collection}->as_hashref->next
    }
  );
}

sub position_GET : Chained('position') : PathPart('') Args(0) {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      tracker   => {$c->stash->{tracker}->get_columns},
      positions => [
        map {
          $_->{status_line} = status_line($_);
          ($_->{address}) = $_->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
          $_
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub sender : Private {
  my ($self, $c, $tracker, $command, $message_id) = @_;

  my $sender = Trackware::Supplier->with_traits('Infobip')->new(
    mobile_phone => $tracker->phone_number,
    message      => $command->command,
    message_id   => $message_id
  )->send;

  return $sender;
}

sub multimessage : Chained('base') : Args(0) {
  my ($self, $c) = @_;

  my $p = $c->req->params;

  my $text = $p->{text}
    or $self->status_bad_request($c, message => 'Missing text'), $c->detach;

  $self->status_bad_request(
    $c, message => 'Falta empresa ou uma lista de ICCID'
    ),
    $c->detach
    unless $p->{iccid} || $p->{'business-unity'};

  my $trackers = $c->model('DB::Tracker')->search_rs;

  $trackers =
    $trackers->search_rs({business_unity_id => $p->{'business-unity'}})
    if $p->{'business-unity'};

  $trackers = $trackers->({iccid => {-in => $p->{iccid}}}) if $p->{iccid};

  while (my $tracker = $trackers->next) {
    $tracker->send_sms(text => $tracker->render_message($text));
  }

  $self->status_ok($c, entity => {ok => 1,});

}

sub area_notify : Chained('base') : PathPart(area-notify) POST {
  my ($self, $c) = @_;

  my $p = $c->req->params;

  my $message = delete $p->{text}
    or $self->status_bad_request(
    $c, message => 'Falta mensagem a ser enviada ao equipamento'
    ),
    $c->detach;


  my $iccids = [];
  $iccids =
    [grep { !!$_ } map { s/^\s+|\s+$//gr } split /\W+/, delete $p->{iccid}]
    if $p->{iccid};

  $self->status_bad_request(
    $c,
    message =>
      'É necessário informar uma lista de equipamentos ou uma empresa'
    ),
    $c->detach
    unless $p->{'business-unity'} || scalar @$iccids;

  my %args;

  $args{business_unity_id} = $p->{'business-unity'} if $p->{'business-unity'};

  $args{business_unity_id} = undef
    if $p->{'business-unity'} && $p->{'business-unity'} eq 'any';

  $args{'-or'} =
    [{phone_number => {-in => $iccids}}, {code => {-in => $iccids}},]
    if $iccids;

  $c->model('DB')->txn_do(
    sub {
      my $trackers = $c->model('DB::Tracker')->search_rs(\%args);
      $c->model('DB::Notification')->add($p, $message, $trackers);
    }
  );
  $self->status_ok($c, entity => {ok => 1,});
}

sub search : Chained('base') : Args(0) POST {
  my ($self, $c) = @_;

  my $trackers = $c->stash->{collection}
    ->execute($c, for => 'search', with => $c->req->params);

  $self->status_ok($c, entity => {results => [$trackers->as_hashref->all]});

}
my $code_regex = qr/^([A-F0-9]{6})([A-Z0-9]{2})(\d{7})$/;

sub register : Chained('object') : Args(0) POST {
  my ($self, $c) = @_;

  my $code    = uc $c->req->params->{code};
  my $rf_note = $c->req->params->{rf_note};

  $c->model('DB')->schema->txn_do(
    sub {

      $self->status_bad_request($c, message => 'Falta o código'), $c->detach
        unless $code;

      $self->status_bad_request($c, message => 'Formato do RF inválido'),
        $c->detach
        if $rf_note && $rf_note !~ /^\d{3}$/;

      $self->status_bad_request(
        $c, message => 'Formato do código é inválido'
        ),
        $c->detach
        unless $code =~ $code_regex;

      my ($business_unity_code, $model, $serial) = $code =~ $code_regex;

      $self->status_bad_request($c, message => 'Modelo inexistente'), $c->detach
        unless $c->model('DB::TrackerType')->find({code => $model});

      $serial += 0;

      my $business_unity =
        $c->model('DB::BusinessUnity')->find({code => $business_unity_code})
        or $self->status_bad_request(
        $c,
        message =>
"Não existe empresa com este código de cadastro: $business_unity_code"
        ),
        $c->detach;

      # my $registration = $business_unity->pre_registrations->available->next
      #   or $self->status_bad_request( $c,
      #   message => "Não há mais códigos disponíveis para essa empresa" ),
      #   $c->detach;

      my $registration =
        $business_unity->pre_registrations->available->search_rs(
        {seq => $serial})->next
        or $self->status_bad_request($c,
        message => "Este código está indisponível ou já foi utilizado"),
        $c->detach;


      my $tracker = $c->stash->{tracker};

      $self->status_bad_request(
        $c, message => 'Operadora do equipamento difere do pré-cadastro'
        ),
        $c->detach
        if $tracker->network_supplier_id
        && $registration->network_supplier_id
        && (
        $tracker->network_supplier_id != $registration->network_supplier_id);

      my $old_business_unity_id = $tracker->business_unity_id;
      my $old_queue_key         = $tracker->queue_key;
      $tracker->update({rf_note => '433.' . $rf_note . '0'}) if $rf_note;
      $tracker->adopt($model => $registration);
      $tracker->discard_changes;
      my $new_queue_key         = $tracker->queue_key;
      my $new_business_unity_id = $tracker->business_unity_id;
      eval {
        $c->model('Redis')
          ->publish($old_queue_key =>
            encode_json({remove => {tracker_id => $tracker->id}}));
        $c->model('Redis')
          ->publish($new_queue_key =>
            encode_json({add => $tracker->notification_payload}));
      };
      $self->status_ok($c, entity => {ok => 1});
    }
  );
}
1;

