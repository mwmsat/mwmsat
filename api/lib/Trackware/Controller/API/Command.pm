package Trackware::Controller::API::Command;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result     => 'DB::Command',
  object_key => 'command',
);
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::Search';
with 'Trackware::TraitFor::Controller::AutoObject';

sub base : Chained('/api/base') : PathPart('command') : CaptureArgs(0) { }

sub object : Chained(base) PathPart('') : CaptureArgs(1) { }

sub view : Chained(object) : PathPart('') : Args(0) GET {
  my ($self, $c) = @_;
  $c->stash->{object}->reset;

  $self->status_ok($c,
    entity => $c->stash->{object}
      ->search_rs(undef, {prefetch => {command_roles => 'role'}})
      ->as_hashref->next);
}

sub edit : Chained(object) : PathPart('') : Args(0) PUT {
  my ($self, $c) = @_;

  my $command =
    $c->stash->{command}->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted($c, entity => {id => $command->id});

}

sub remove : Chained(object) : PathPart('') Args(0) DELETE {
  my ($self, $c) = @_;

  $self->status_bad_request($c, message => 'Senha incorreta'), $c->detach
    unless $c->user->obj->check_password($c->req->params->{password});

  $c->stash->{object}->delete;
  $self->status_accepted($c, entity => {ok => 1});
}

sub send : Chained(object) : Args(1) POST {
  my ($self, $c, $tracker_id) = @_;

  my $tracker = $c->model('DB::Tracker')->find({id => $tracker_id})
    or $self->status_not_found(
    $c, message => 'Equipamento não encontrado ou inativo'
    ),
    $c->detach;

  $tracker->send_sms(
    text => $tracker->render_message($c->stash->{command}->message_template));

  $self->status_ok($c, entity => {sent => 1});
}

sub allowed : Chained('base') : Args(0) : GET {
  my ($self, $c) = @_;
  $self->status_ok(
    $c,
    entity => {
      commands => [
        $c->user->user_roles->related_resultset('role')
          ->related_resultset('command_roles')->related_resultset('command')
          ->valid->as_hashref->all
      ]
    }
  );

}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {

}

sub list_POST {
  my ($self, $c) = @_;

  my $command = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    location =>
      $c->uri_for($self->action_for('view'), [$command->id])->as_string,
    entity => {id => $command->id}
  );

}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      items => [
        $c->stash->{collection}
          ->search_rs(undef, {order_by => {-desc => 'create_ts'}})
          ->as_hashref->all
      ]
    }
  );

}

1;
