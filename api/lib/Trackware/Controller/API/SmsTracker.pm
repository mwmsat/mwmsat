package Trackware::Controller::API::SmsTracker;

use Moose;
use Trackware::Supplier;
use Trackware::SqsManager;
use Trackware::Log;
use JSON::XS;

BEGIN { extends 'Catalyst::Controller::REST' }

__PACKAGE__->config(
  default => 'application/json',

  result      => 'DB::SmsTracker',
  object_key  => 'sms_tracker',
  result_attr => {prefetch => ['tracker', 'network_supplier', 'sms_statuses'],},
  search_ok   => {'tracker_id' => 'Int', 'order' => 'Str'},

  update_roles => [qw/superadmin admin admin-unity/],
  create_roles => [qw/superadmin admin admin-unity/],
  delete_roles => [qw/superadmin admin admin-unity/],

);

with 'Trackware::TraitFor::Controller::DefaultCRUD';

sub base : Chained('/api/base') : PathPart('sms_trackers') : CaptureArgs(0) { }

sub object : Chained('base') : PathPart('') : CaptureArgs(1) { }

sub result : Chained('object') : PathPart('') : Args(0) :
  ActionClass('REST') { }

sub result_GET {
  my ($self, $c) = @_;

  my $sms_tracker = $c->stash->{sms_tracker};
  my %attrs       = $sms_tracker->get_inflated_columns;

  $self->status_ok(
    $c,
    entity => {
      (
        map { $_ => $attrs{$_}, }
          qw/
          id
          message
          /
      ),
      (
        map { $_ => ($attrs{$_} ? $attrs{$_}->datetime : undef) }
          qw/created_at updated_at/
      ),
      tracker => {
        (
          map { $_ => $sms_tracker->tracker->$_, }
            qw/
            id
            imei
            iccid
            phone_number
            /
        ),
      },
      sms_statuses => {
        (
          map { $_ => $sms_tracker->sms_statuses->$_, }
            qw/
            id
            name
            /
        ),
      },
      network_supplier => {
        (
          map { $_ => $sms_tracker->network_supplier->$_, }
            qw/
            id
            name
            /
        ),
      },
    }
  );
}

sub result_PUT {
  my ($self, $c) = @_;

  my $sms_tracker = $c->stash->{sms_tracker};

  $sms_tracker->execute($c, for => 'update', with => $c->req->params);

  $self->status_accepted(
    $c,
    location =>
      $c->uri_for($self->action_for('result'), [$sms_tracker->id])->as_string,
    entity => {id => $sms_tracker->id}
    ),
    $c->detach
    if $sms_tracker;
}

sub result_DELETE {
  my ($self, $c) = @_;

  my $sms_tracker = $c->stash->{sms_tracker};

  $sms_tracker->delete;
  $self->status_no_content($c);
}

sub list : Chained('base') : PathPart('') : Args(0) : ActionClass('REST') {
}

sub list_GET {
  my ($self, $c) = @_;

  $self->status_ok(
    $c,
    entity => {
      sms_trackers => [
        map {
          my $r = $_;
          +{
            (
              map { $_ => $r->{$_} }
                qw/
                id
                message
                created_at
                updated_at
                /
            ),
            tracker => {
              (
                map { $_ => $r->{tracker}{$_}, }
                  qw/
                  id
                  imei
                  iccid
                  phone_number
                  /
              ),
            },
            sms_statuses => {
              (
                map { $_ => $r->{sms_statuses}{$_}, }
                  qw/
                  id
                  name
                  /
              ),
            },
            network_supplier => {
              (
                map { $_ => $r->{network_supplier}{$_}, }
                  qw/
                  id
                  name
                  /
              ),
            },
            url => $c->uri_for_action($self->action_for('result'), [$r->{id}])
              ->as_string
            }
        } $c->stash->{collection}->as_hashref->all
      ]
    }
  );
}

sub list_POST {
  my ($self, $c) = @_;

  my $sms_tracker = $c->stash->{collection}
    ->execute($c, for => 'create', with => $c->req->params);

  $self->status_created(
    $c,
    entity => {id => $sms_tracker->id},
    location =>
      $c->uri_for($self->action_for('result'), [$sms_tracker->id])->as_string,
  );

}


sub collector : Chained('base') : PathPart('collector') : Args(0) {
  my ($self, $c) = @_;

  my $logger = get_logger;

  my $p = $c->req->params;

  my $message = $p->{text}
    or $self->status_bad_request($c, message => 'Missing text'), $c->detach;

  my $tracker = $c->model('DB::Tracker')->find(
    $p->{tracker_id}
      || (
      $self->status_bad_request($c, message => 'Missing tracker_id'),
      $c->detach
      )
    )
    or $self->status_not_found($c, message => 'Tracker not found'), $c->detach;

  $self->status_bad_request($c, message => 'Tracker has no phone_number'),
    $c->detach
    unless $tracker->phone_number;

  my $msg;

  $logger->warn('Mensagem recebida :' . encode_json($p));

  eval {
    my $sqs =
      Trackware::SqsManager->new({queue_name => 'trackware-outbound-sms'});

    $msg = $sqs->sqs->SendMessage(encode_json($p));
  };

  if ($@) {
    $self->status_bad_request($c,
      message => 'Probleme saving message. Error: ' . $@);
  }

  $self->status_ok($c, entity => {sqs_message_id => $msg->MessageId});
}

1;
