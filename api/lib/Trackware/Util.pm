package Trackware::Util;

use utf8;
use strict;
use warnings;
use Exporter qw(import);

our @EXPORT_OK = qw(extract_message_data rule_listing status_line IS_TEST
  extract_tracker_attributes_from_text
);

sub extract_message_data {
  my $event   = shift;
  my $message = $event->{message}->{body};

  return +{
    imei            => $message->{imei},
    iccid           => $message->{iccid},
    phone_number    => $event->{phone},
    text            => $event->{text},
    battery_voltage => $message->{battery},
    position        => $event->{position},
    supplier        => $event->{supplier},
    datetime        => $event->{datetime},
    (map { ($_ => int($message->{$_} || 0)) } qw(tx cycle gps gprs interval))
  };
}

my %rule_area_mapping = (
  leaving  => 'Saindo',
  entering => 'Entrando',
  inside   => 'Dentro',
  outside  => 'Fora'

);

sub rule_listing {
  my $rule  = shift;
  my @rules = ();
  if (my $type = $rule->{'area-type'}) {
    push @rules, $rule_area_mapping{$type};
  }
  push @rules,
    sprintf(
    'Nível de bateria: [%s, %s]',
    $rule->{'battery-min'} || '-Inf',
    $rule->{'battery-max'} || 'Inf'
    ) if $rule->{'battery-min'} || $rule->{'battery-max'};

  push @rules,
    sprintf(
    'Temperatura: [%s, %s]',
    $rule->{'temperature-min'} || '-Inf',
    $rule->{'temperature-max'} || 'Inf'
    ) if $rule->{'temperature-min'} || $rule->{'temperature-max'};

  return @rules;
}

sub status_line {
  my $info     = shift;
  my @statuses = ();
  my $tx       = $info->{tx};
  my $cycle    = $info->{cycle};
  if ($tx) {
    push @statuses, 'TX'          if $tx == 1;
    push @statuses, 'Emergência' if $tx == 2;
  }
  if ($cycle) {
    push @statuses, 'Ciclo'  if $cycle == 1;
    push @statuses, 'Wakeup' if $cycle == 2;
  }
  my $size = scalar @statuses;
  return unless $size;
  return $statuses[0] if $size == 1;
  return _abbrev_status(@statuses) if scalar $size > 1;
}
my %status_abbrev_map = ('Emergência' => 'E', Ciclo => 'C', Wakeup => 'W');

sub _abbrev_status {
  join(', ', map { $status_abbrev_map{$_} || $_ } @_);
}

sub IS_TEST {
  $ENV{HARNESS_ACTIVE} || $0 =~ /forkprove/ ? 1 : 0;
}

my $code_regex = qr/^([A-F0-9]{6})([A-Z0-9]{2})(\d{7})$/;

sub extract_tracker_attributes_from_text {
  my ($text) = @_;
  my @numbers = grep { !!$_ } map { s/^\s+|\s+$//gr } split /\W+/, $text;
  my @iccids        = grep { /^\d+$/ && length($_) >= 20 } @numbers;
  my @codes         = grep { $_ =~ $code_regex } @numbers;
  my @phone_numbers = grep { /^\d+$/ && length($_) < 20 } @numbers;
  return {iccid => \@iccids, code => \@codes, phone_number => \@phone_numbers};
}

1;
