package Trackware::TraitFor::Controller::DefaultCRUD;

use Moose::Role;

with 'Trackware::TraitFor::Controller::Search';
with 'Trackware::TraitFor::Controller::AutoBase';
with 'Trackware::TraitFor::Controller::AutoObject';
with 'Trackware::TraitFor::Controller::CheckRoleForPUT';
with 'Trackware::TraitFor::Controller::CheckRoleForPOST';

1;

