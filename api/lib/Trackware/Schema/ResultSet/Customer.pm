package Trackware::Schema::ResultSet::Customer;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::ResultsetFind';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        fancy_name        => {required => 1, type => 'Str',},
        corporate_name    => {required => 1, type => 'Str',},
        address_id        => {required => 1, type => 'Int',},
        business_unity_id => {required => 0, type => 'Int',},
        parent_id         => {required => 0, type => 'Int',},
        phone             => {required => 0, type => 'Str',},
        mobile_phone      => {required => 0, type => 'Str',},
        email             => {required => 1, type => 'Str',},
        website           => {required => 0, type => 'Str',},
        register          => {required => 0, type => 'Num',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $customer = $self->create(\%values);

      return $customer;
    }
  };
}

1;
