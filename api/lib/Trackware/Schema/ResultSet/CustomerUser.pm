package Trackware::Schema::ResultSet::CustomerUser;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::ResultsetFind';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        customer_id => {required => 1, type => 'Int',},
        user_id     => {required => 1, type => 'Int',},
        is_active   => {required => 0, type => 'Bool'}
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $customer_user = $self->create(\%values);

      return $customer_user;
    }
  };
}

1;
