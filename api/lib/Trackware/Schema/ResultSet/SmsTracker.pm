package Trackware::Schema::ResultSet::SmsTracker;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        message             => {required => 1, type => 'Str',},
        sms_status_id       => {required => 1, type => 'Int',},
        updated_at          => {required => 0, type => DataStr,},
        tracker_id          => {required => 1, type => 'Int',},
        network_supplier_id => {required => 0, type => 'Int',},
        send_type           => {required => 0, type => 'Str',}
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $sms_tracker = $self->create(\%values);

      return $sms_tracker;
    }
  };
}

sub last {
  shift->search_rs(undef, {order_by => {-desc => 'created_at'}});
}

1;
