package Trackware::Schema::ResultSet::TrackerNotificationSms;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';

__PACKAGE__->load_components(qw(Helper::ResultSet::CorrelateRelationship));


1;
