package Trackware::Schema::ResultSet::BusinessUnity;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr CNPJ/;

extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::ResultsetFind';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;
use Digest::SHA qw(sha256_hex);

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        corporate_name => {required => 1, type => 'Str',},
        fancy_name     => {required => 1, type => 'Str',},

        # code           => {
        #   required   => 1,
        #   type       => 'Str',
        #   min_length => 6,
        #   max_length => 6,
        #   filters    => [qw(upper flatten)],
        #   post_check => sub {
        #     my $r = shift;
        #     !$self->find({code => $r->get_value('code')});
        #   }
        # },
        register => {
          required => 1,
          filters => [qw(upper flatten), sub { my ($v) = @_; $v =~ s/\D+//gr }],
          post_check => sub {
            my $r = shift;
            !$self->find({register => $r->get_value('register')});
          },
          type => CNPJ,
        },
        address_id => {required => 0, type => 'Int',},
        is_active  => {required => 0, type => 'Bool',},
        website    => {required => 0, type => 'Str',},
        email      => {required => 0, type => 'Str',},
        phone      => {required => 1, type => 'Str',},
        phone_2    => {required => 0, type => 'Str',},
        parent_id  => {required => 0, type => 'Int',},
        domain     => {
          required   => 1,
          type       => 'Str',
          post_check => sub {
            my $r = shift;

            my $match = $r->get_value('domain') =~ /^[a-z0-9-]+$/;

            return 0 unless $match;

            return 0
              if $self->resultset_find({domain => lc($r->get_value('domain'))});

            return 1;
          }
        },
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $business_unity = $self->create(
        {%values, code => uc substr(sha256_hex($values{register}), 0, 6)});

      return $business_unity;
    }
  };
}

1;
