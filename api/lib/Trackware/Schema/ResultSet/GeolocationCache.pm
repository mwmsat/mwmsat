package Trackware::Schema::ResultSet::GeolocationCache;
use namespace::autoclean;

use utf8;
use Moose;
use Trackware::Types qw /DataStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        cache_key => {required => 1, type => 'Str'},
        request   => {required => 1, type => 'Str'},
        response  => {required => 1, type => 'Str'},
      },
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $geolocation_cache = $self->create(\%values);

      return $geolocation_cache;
    }

  };
}

1;
