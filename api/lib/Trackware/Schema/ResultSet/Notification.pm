package Trackware::Schema::ResultSet::Notification;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::ResultsetFind';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';
use List::MoreUtils qw(pairwise);
use Data::Verifier;

__PACKAGE__->load_components(qw(Helper::ResultSet::CorrelateRelationship));

sub verifiers_specs {
  my $self = shift;
  return {create => Data::Verifier->new()};
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
    }
  };
}

sub add {
  my ($self, $rule, $message, $trackers) = @_;
  my $notification = $self->create({rule => $rule, message => $message});

  pairwise {
    $notification->add_to_areas(
      {
        center => \(
          sprintf(
            q{'SRID=4326;POINT( %s %s )'::geometry},
            reverse split /,/, $a
          )
        ),
        radius => $b
      }
      )
      if $a
      && $b
  }
  @{ref $rule->{center} eq 'ARRAY' ? $rule->{center} : [$rule->{center}] || []},
    @{ref $rule->{radius} eq 'ARRAY' ? $rule->{radius} : [$rule->{radius}]
      || []};

  while (my $tracker = $trackers->next) {
    $notification->add_to_trackers($tracker);
  }
}

sub summary {
  my $self = shift;
  return $self;
}

1;
