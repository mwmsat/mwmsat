package Trackware::Schema::ResultSet::NotificationArea;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';

sub check {
  my ($self, $notification, $before, $after) = @_;
  my $rules = $notification->rule;
  my $way = $rules->{'area-type'} or return;

  my ($before_lat, $before_lon, $after_lat, $after_lon) =
    ($before->lat, $before->lng, $after->lat, $after->lng);

  if ($way eq 'inside') {
    return $self->search_rs(
      \[
        q{ST_DWithin(me.center::geography, ?::geography, me.radius)},
        [_coords => qq{SRID=4326;POINT($after_lon $after_lat )}]
      ],
    )->count > 0;
  }

  if ($way eq 'outside') {
    return $self->search_rs(
      {
        -not => \[
          q{ST_DWithin(me.center::geography, ?::geography, me.radius)},
          [_coords => qq{SRID=4326;POINT($after_lon $after_lat )}]
        ],
      }
    )->count > 0;
  }

  if ($way eq 'entering') {
    return $self->search_rs(
      {
        -and => [

          \[
            q{ST_DWithin(me.center::geography, ?::geography, me.radius)},
            [_coords => qq{SRID=4326;POINT($after_lon $after_lat )}]
          ],
          \[
            q{NOT ST_DWithin(me.center::geography, ?::geography, me.radius)},
            [_coords => qq{SRID=4326;POINT($before_lon $before_lat )}]
            ]

          ]

      }
    )->count > 0;
  }

  if ($way eq 'exiting') {
    return $self->search_rs(
      {
        -and => [
          \[
            q{NOT ST_DWithin(me.center::geography, ?::geography, me.radius)},
            [_coords => qq{SRID=4326;POINT($after_lon $after_lat )}]
          ],
          \[
            q{ST_DWithin(me.center::geography, ?::geography, me.radius)},
            [_coords => qq{SRID=4326;POINT($before_lon $before_lat )}]
            ]

          ]

      }
    )->count > 0;
  }

}
1;
