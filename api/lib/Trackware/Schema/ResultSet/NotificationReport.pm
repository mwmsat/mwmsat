package Trackware::Schema::ResultSet::NotificationReport;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::InflateAsHashRef';

sub summary {
  my $self = shift;
  return $self;
}

1;
