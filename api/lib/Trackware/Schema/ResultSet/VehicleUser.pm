package Trackware::Schema::ResultSet::VehicleUser;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        vehicle_id => {required => 1, type => 'Int',},
        user_id    => {required => 1, type => 'Int',},
        is_driver  => {required => 0, type => 'Bool',},
        is_owner   => {required => 0, type => 'Bool',},
      },
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $vehicle = $self->create(\%values);

      return $vehicle;
    }

  };
}

1;

