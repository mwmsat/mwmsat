package Trackware::Schema::ResultSet::TrackerSmsAlert;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';
use Trackware::Types qw(PositiveInt);
use MooseX::Types::Common::String qw/NonEmptySimpleStr/;
use Data::Verifier;
use DDP;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        note              => {required => 0, type => NonEmptySimpleStr,},
        alert_type        => {required => 0, type => NonEmptySimpleStr,},
        has_status        => {required => 0, type => 'Bool',},
        has_gps_location  => {required => 0, type => 'Bool',},
        has_address       => {required => 0, type => 'Bool',},
        has_battery_level => {required => 0, type => 'Bool',},
        destination_phone_numbers => {
          required => 0,
          type     => 'ArrayRef',
          filters  => [qw(collapse trim)],
          coercion => Data::Verifier::coercion(
            from => 'Str',
            via  => sub {
              ["$_"];
            }
          ),

          post_check => sub {
            my $r = shift;
            p($r->get_value('destination_phone_numbers'));
            scalar grep { /^\d+$/ }
              @{$r->get_value('destination_phone_numbers') || []};
          }
        },

        tracker_id => {
          required   => 1,
          type       => PositiveInt,
          post_check => sub {
            my $r = shift;
            $self->result_source->schema->resultset('Tracker')
              ->find($r->get_value('tracker_id'));
          }
        },
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;
      $values{destination_phone_numbers} =
        join(';', @{$values{destination_phone_numbers}});

      $self->create(\%values);
    },
  };
}

sub with_tracker {
  shift->search_rs(undef, {prefetch => 'tracker'});
}

1;


