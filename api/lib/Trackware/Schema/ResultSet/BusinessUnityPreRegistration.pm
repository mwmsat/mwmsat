package Trackware::Schema::ResultSet::BusinessUnityPreRegistration;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::InflateAsHashRef';

sub allocate {
  my ($self, $n, $network_supplier_id) = @_;
  die 'more than 10k' if $n > 10_000;
  $self->result_source->schema->txn_do(
    sub {
      $self->create({network_supplier_id => $network_supplier_id}) for 1 .. $n;
    }
  );
  return $n;
}

sub with_trackers {
  shift->search_rs(undef, {prefetch => 'used_by'});
}

sub with_network_supplier {
  shift->search_rs(undef, {prefetch => 'network_supplier'});
}


sub available {
  my $self = shift;
  my $me   = $self->current_source_alias;
  $self->search_rs({'used_by.id' => undef},
    {join => 'used_by', order_by => {-asc => 'seq'}});
}

1;

