package Trackware::Schema::ResultSet::Command;
use namespace::autoclean;

use utf8;
use Moose;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name => {
          required   => 1,
          type       => 'Str',
          post_check => sub {
            my $r = shift;
            return !$self->search({valid => 1, name => $r->get_value('name')},
              {rows => 1})->next;
          }
        },
        message_template => {required => 1, type => 'Str',},
        roles            => {required => 1, type => 'ArrayRef[Int]',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values  = shift->valid_values;
      my $roles   = delete $values{roles};
      my $command = $self->create(\%values);
      $command->add_to_command_roles({role_id => $_}) for @$roles;
      return $command;

    }
  };
}

sub valid {
  my $self = shift;
  my $me   = $self->current_source_alias;
  $self->search_rs({"$me.valid" => 1});
}

1;
