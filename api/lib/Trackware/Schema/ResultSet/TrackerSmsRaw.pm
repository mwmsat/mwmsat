package Trackware::Schema::ResultSet::TrackerSmsRaw;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        imei                    => {required => 1, type => 'Num',},
        iccid                   => {required => 0, type => 'Num',},
        message                 => {required => 0, type => 'Str',},
        tracker_sms_position_id => {required => 0, type => 'Int'},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $tracker_sms_raw = $self->create(\%values);

      return $tracker_sms_raw;
    }
  };
}

1;
