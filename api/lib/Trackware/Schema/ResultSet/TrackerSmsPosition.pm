package Trackware::Schema::ResultSet::TrackerSmsPosition;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        tracker_id       => {required => 1, type => 'Int',},
        iccid            => {required => 0, type => 'Num',},
        header           => {required => 0, type => 'Str',},
        imei             => {required => 0, type => 'Num',},
        batery_voltage   => {required => 0, type => 'Num',},
        mcc              => {required => 0, type => 'Int',},
        mnc              => {required => 0, type => 'Int',},
        lac_1            => {required => 0, type => 'Int',},
        cellid_1         => {required => 0, type => 'Int',},
        rxlevel_1        => {required => 0, type => 'Int',},
        lac_2            => {required => 0, type => 'Int',},
        cellid_2         => {required => 0, type => 'Int',},
        rxlevel_2        => {required => 0, type => 'Int',},
        lac_3            => {required => 0, type => 'Int',},
        cellid_3         => {required => 0, type => 'Int',},
        rxlevel_3        => {required => 0, type => 'Int',},
        lac_4            => {required => 0, type => 'Int',},
        cellid_4         => {required => 0, type => 'Int',},
        rxlevel_4        => {required => 0, type => 'Int',},
        lac_5            => {required => 0, type => 'Int',},
        cellid_5         => {required => 0, type => 'Int',},
        rxlevel_5        => {required => 0, type => 'Int',},
        lac_6            => {required => 0, type => 'Int',},
        cellid_6         => {required => 0, type => 'Int',},
        rxlevel_6        => {required => 0, type => 'Int',},
        time_advance     => {required => 0, type => 'Boolean',},
        cycle            => {required => 0, type => 'Boolean',},
        tx_jammer        => {required => 0, type => 'Boolean',},
        mode_save        => {required => 0, type => 'Boolean',},
        ala_6            => {required => 0, type => 'Boolean',},
        emergency_mode   => {required => 0, type => 'Boolean',},
        ta_mode          => {required => 0, type => 'Boolean',},
        rf               => {required => 0, type => 'Boolean',},
        firmware_version => {required => 0, type => 'Str',},
        event_date       => {required => 0, type => DataStr,},
        sender           => {required => 0, type => 'Str',},
        receiver         => {required => 0, type => 'Str',},
        vehicle_id       => {required => 0, type => 'Int',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $tracker_sms_position = $self->create(\%values);

      return $tracker_sms_position;
    }
  };
}

1;
