package Trackware::Schema::ResultSet::TrackerEvent;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        tracker_id    => {required => 1, type => 'Int',},
        event_date    => {required => 1, type => DataStr,},
        information   => {required => 0, type => 'Str',},
        event_type_id => {required => 1, type => 'Int',},
        vehicle_id    => {required => 0, type => 'Int',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $tracker_event = $self->create(\%values);

      return $tracker_event;
    }
  };
}

1;
