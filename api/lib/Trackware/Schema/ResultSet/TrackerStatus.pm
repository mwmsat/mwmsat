package Trackware::Schema::ResultSet::TrackerStatus;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        status      => {required => 1, type => 'Str',},
        description => {required => 0, type => 'Str',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $tracker_status = $self->create(\%values);

      return $tracker_status;
    }
  };
}

1;
