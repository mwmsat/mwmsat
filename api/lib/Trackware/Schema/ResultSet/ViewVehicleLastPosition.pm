package Trackware::Schema::ResultSet::ViewVehicleLastPosition;
use namespace::autoclean;

use utf8;
use Moose;

extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::InflateAsHashRef';

1;
