package Trackware::Schema::ResultSet::User;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::ResultsetFind';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name         => {required => 1, type => 'Str',},
        birthdate    => {required => 0, type => DataStr,},
        cpf          => {required => 0, type => 'Num',},
        gender       => {required => 0, type => 'Str',},
        phone        => {required => 0, type => 'Str',},
        mobile_phone => {required => 0, type => 'Str',},
        role         => {required => 0, type => 'Str',},
        email        => {
          required   => 1,
          type       => EmailAddress,
          post_check => sub {
            my $r = shift;
            my $s = 'bla';

            # 						use DDP; p $s; exit;
            # 						return 1 if $self->email eq $r->get_value('email');

            return 0
              if $self->resultset_find({email => lc($r->get_value('email'))});

            return 1;
          }
        },
        password => {
          required   => 1,
          type       => 'Str',
          dependent  => {password_confirm => {required => 1, type => 'Str',},},
          post_check => sub {
            my $r = shift;
            return 1
              if ($r->get_value('password') eq $r->get_value('password_confirm')
              && length $r->get_value('password') >= 5);
          },
        },
        reset_password_key => {required => 0, type => 'Str',},
        is_active          => {required => 0, type => 'Bool',},
        has_profile        => {required => 0, type => 'Bool',},
      },
    ),
    login => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        email    => {required => 1, type => EmailAddress},
        password => {required => 1, type => 'Str',},
      }
    ),
    check_email => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {email => {required => 1, type => EmailAddress}}
    ),
    reset_password_key => {required => 0, type => 'Str'}
  };
}

sub action_specs {
  my $self = shift;
  return {
    login => sub { 1 },

    create => sub {
      my %values = shift->valid_values;

      delete $values{password_confirm};

      $values{email} = lc $values{email};
      my $role       = delete $values{role};
      my $address_id = delete $values{address_id};

      my $user = $self->create(\%values);

      if ($role) {
        $user->set_roles({id => $role});
      }

      use DDP;
      p $address_id;
      if ($address_id) {
        $user->set_addresses({address_id => $address_id});
      }

      return $user;
    }

  };
}

1;

