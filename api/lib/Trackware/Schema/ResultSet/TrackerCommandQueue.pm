package Trackware::Schema::ResultSet::TrackerCommandQueue;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        tracker_command_layout_id => {required => 1, type => 'Int',},
        tracker_id                => {required => 1, type => 'Int',},
        response                  => {required => 0, type => 'Str',},
        updated_at                => {required => 0, type => DataStr,},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $tracker_command_queue = $self->create(\%values);

      return $tracker_command_queue;
    }
  };
}

1;
