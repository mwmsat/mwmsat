package Trackware::Schema::ResultSet::Multitrack;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::ResultsetFind';
with 'Trackware::Schema::Role::InflateAsHashRef';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name => {
          required   => 1,
          type       => 'Str',
          post_check => sub {
            my $r = shift;
            return !$self->search({name => $r->get_value('name')}, {rows => 1})
              ->next;
          }
        },
        description => {required => 0, type => 'Str',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;
  return {
    create => sub {
      $self->create({shift->valid_values});
    }
  };
}


sub summary {
  shift->search_rs(
    undef,
    {
      distinct => 1,
      '+columns' =>
        [{tracker_count => {count => 'tracker.id', -as => 'tracker_count'}}],
      join => {multitrack_trackers => 'tracker'},
    }
  );
}

1;
