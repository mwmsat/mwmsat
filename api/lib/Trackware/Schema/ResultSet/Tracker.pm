package Trackware::Schema::ResultSet::Tracker;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr PositiveInt/;
use Trackware::Util qw(extract_tracker_attributes_from_text);
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    search => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        query => {
          required   => 1,
          type       => 'Str',
          min_length => 3,
          filters    => [sub { my ($val) = @_; $val =~ s/\W+//g; $val }]
        },
      }
    ),
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        code => {required => 0, type => 'Int',},

        imei => {required => 1, type => 'Int',},

        iccid        => {required => 1, type => 'Str',},
        phone_number => {
          required => 0,
          type     => 'Str',
          filters  => [sub { my ($val) = @_; $val =~ s/\D+//g; $val }]
        },
        vehicle_id          => {required => 0, type => 'Int',},
        business_unity_id   => {required => 0, type => 'Int',},
        tracker_status_id   => {required => 0, type => 'Int',},
        customer_id         => {required => 0, type => 'Int',},
        network_supplier_id => {required => 0, type => 'Int',},
        tracker_model_id    => {required => 0, type => 'Int',}
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    search => sub {
      my %values = shift->valid_values;
      not defined $values{$_} and delete $values{$_} for keys %values;

      return $self->search_rs(\'1=0') unless keys %values;
      my $me = $self->current_source_alias;

      my $query = delete $values{query};

      $self->search_rs(
        {
          -or => [
            map {
              +{"$me.$_" => {-ilike => \[q{ '%' || ? || '%' }, [_q => $query]]}}
            } qw(imei iccid phone_number code)
          ]
        },
        {
          join => 'business_unity',

          '+columns' => [
            {business_unity_name => 'business_unity.corporate_name'},
            map {
              +{
                $_
                  . '_highlight' => \[
qq{regexp_replace( $me.$_, '('|| ? || ')' , '<b style="color: #330040">\\1</b>')},
                  [_q => $query]
                  ]
                }
            } qw(imei iccid phone_number code)
          ],
        }
      );
    },
    create => sub {
      my %values = shift->valid_values;

      my $tracker = $self->create(\%values);

      return $tracker;
    }
  };
}

sub from_text_list {
  my ($self, $text) = @_;
  my $me  = $self->current_source_alias;
  my $map = extract_tracker_attributes_from_text($text);
  return $self->search_rs(
    {-or => [map { +{$_ => {-in => $map->{$_}}} } keys %$map]});
}

1;
