package Trackware::Schema::ResultSet::TrackerPosition;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        tracker_id                    => {required => 1, type => 'Int',},
        lat                           => {required => 1, type => 'Num',},
        lng                           => {required => 1, type => 'Num',},
        event_date                    => {required => 1, type => DataStr,},
        speed                         => {required => 1, type => 'Num',},
        sat_number                    => {required => 0, type => 'Int',},
        hdop                          => {required => 0, type => 'Int',},
        vehicle_id                    => {required => 0, type => 'Int',},
        transaction                   => {required => 0, type => 'Int',},
        tracker_reason_generator_code => {required => 0, type => 'Int',},
        imei                          => {required => 0, type => 'Int',},
        battery_voltage               => {required => 0, type => 'Num',},
        battery_voltage_bkp           => {required => 0, type => 'Num',},
        hourmeter                     => {required => 0, type => 'Num',},
        message_size                  => {required => 0, type => 'Num',},
        odometer                      => {required => 0, type => 'Num',},
        position_counter              => {required => 0, type => 'Num',},
        temperature                   => {required => 0, type => 'Num',},
        package_type                  => {required => 0, type => 'Int',},
        pcb_version                   => {required => 0, type => 'Num',},
        firmware_version              => {required => 0, type => 'Float',},
        position_amount               => {required => 0, type => 'Int',},
        position_size                 => {required => 0, type => 'Int',},
        inputs                        => {required => 0, type => 'Num',},
        outputs                       => {required => 0, type => 'Num',},
        precision                     => {required => 0, type => 'Str',},
        position_type                 => {required => 0, type => 'Str',},
        lbs_raw                       => {required => 0, type => 'Str',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $tracker_position = $self->create(\%values);

      return $tracker_position;
    }
  };
}

sub with_battery_level {
  shift->search_rs(
    undef,
    {
      '+columns' => [
        {
          battery_level => \q{CASE WHEN me.battery_voltage > 4150 THEN 100
                             WHEN me.battery_voltage  < 3600 THEN 0
                        ELSE (0.182621 * me.battery_voltage - 658.005)::int END}
        }
      ]
    }
  );
}

sub last_two {
  shift->search_rs(undef, {rows => 2, order_by => {-desc => 'event_date'}})
    ->all;
}
1;
