package Trackware::Schema::ResultSet::TrackerType;
use namespace::autoclean;

use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name => {
          required   => 0,
          type       => 'Str',
          post_check => sub {
            my $r = shift;
            !$self->search_rs({name => $r->get_value('name')})->count;
          }
        },
        code => {
          required   => 0,
          type       => 'Str',
          max_length => 2,
          min_length => 2,
          filters    => [qw(upper flatten)],
          post_check => sub {
            my $r = shift;
            !$self->search_rs({name => $r->get_value('code')})->count;
          }
        },
        days => {required => 0, type => 'Int',},

        battery_level => {
          required   => 0,
          type       => 'Int',
          post_check => sub {
            my $r     = shift;
            my $level = $r->get_value('battery_level');
            return $level >= 100 && $level <= 100_000;
          }
        },


        low_battery_level_alert => {required => 0, type => 'Int',},
        has_temperature_sensor  => {required => 0, type => 'Bool',},

        has_movement_sensor => {required => 0, type => 'Bool',},
        has_gps_sensor      => {required => 0, type => 'Bool',},
        has_rf_sensor       => {required => 0, type => 'Bool',}
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $user = $self->create(\%values);

      return $user;
    },
  };
}

1;
