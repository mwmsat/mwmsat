package Trackware::Schema::ResultSet::Address;
use namespace::autoclean;

use utf8;
use Moose;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        address      => {required => 1, type => 'Str',},
        number       => {required => 1, type => 'Str',},
        neighborhood => {required => 1, type => 'Str',},
        postal_code  => {required => 1, type => 'Str',},
        lat_lng      => {required => 0, type => 'Str',},
        city_id      => {required => 1, type => 'Int',},
        complement   => {required => 0, type => 'Str',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $address = $self->create(\%values);

      return $address;
    }
  };
}

1;
