package Trackware::Schema::ResultSet::PositionCache;
use namespace::autoclean;

use utf8;
use Moose;
use Trackware::Types qw /DataStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        point_id => {
          required => 0,
          type     => 'Str'
        },
        lat_lng => {
          required => 0,
          type     => 'Str'
        },
        address => {
          required => 0,
          type     => 'Str'
        },
        point_orig_id => {
          required => 0,
          type     => 'Str'
        },
        postal_code => {
          required => 0,
          type     => 'Str'
        }
      },
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $position_cache = $self->create( \%values );

      return $position_cache;
    }

  };
}

use Geo::Coordinates::DecimalDegrees qw(decimal2dm);

sub normalize_cache {
  my ($self) = @_;

  # use Trackware::Geolocation;
  # my $geo  = Trackware::Geolocation->new;

  my $denormalized_cache = $self->search(
    {
      -or => [ { point_orig_id => undef }, { postal_code => undef } ]
    },
    {
      #      result_class => 'DBIx::Class::ResultClass::HashRefInflator'
    }
  );

  while ( my $row = $denormalized_cache->next ) {
    if ( !$row->postal_code ) {
      my $address = $row->address;
      warn $address;
      my $postal_code;
      ($postal_code) = $address =~ /\b(\d{5}-\d{3})\b/;

      $postal_code =~ s/\D+//g if $postal_code;

      # $postal_code = eval {
      #   my $res = $geo->reverse_geo_code( $self->lat_lng_as_google );
      #   $res->{cep};
      # };

      $row->update( { postal_code => $postal_code } ) if $postal_code;
    }
    if ( !$row->point_orig_id ) {
      no warnings 'numeric';
      $row->update(
        {
          point_orig_id => join( q/&/,
            ,
            map { sprintf "%.2f", ( join( q//, decimal2dm($_) ) + 0 ) }
              split /,/,
            $row->lat_lng_as_google )
        }
      );
    }
  }

}
1;
