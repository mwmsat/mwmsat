package Trackware::Schema::ResultSet::VehicleDocument;
use namespace::autoclean;

use utf8;
use Moose;
use Trackware::Types qw /DataStr/;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::InflateAsHashRef';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    create => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        link             => {required => 0, type => 'Str',},
        valid_date       => {required => 1, type => DataStr,},
        document_type_id => {required => 1, type => 'Int',},
        is_valid         => {required => 0, type => 'Bool',},
        vehicle_id       => {required => 1, type => 'Int',},
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    create => sub {
      my %values = shift->valid_values;

      my $vehicle_document = $self->create(\%values);

      return $vehicle_document;
    },
  };
}

1;
