package Trackware::Schema::ResultSet::SmsGarbage;

use namespace::autoclean;
use utf8;
use Moose;
extends 'DBIx::Class::ResultSet';
with 'Trackware::Schema::Role::InflateAsHashRef';

1;
