#<<<
use utf8;
package Trackware::Schema::Result::Erb;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("erb");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "erb_id_seq",
  },
  "mcc",
  { data_type => "integer", is_nullable => 0 },
  "mnc",
  { data_type => "integer", is_nullable => 0 },
  "lac",
  { data_type => "integer", is_nullable => 0 },
  "cid",
  { data_type => "integer", is_nullable => 0 },
  "rxl",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("erb_unique_key", ["mcc", "mnc", "lac", "cid"]);
__PACKAGE__->has_many(
  "erb_positions",
  "Trackware::Schema::Result::ErbPosition",
  { "foreign.erb_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jQiiratL6Haa9ZhGCmAH5A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
