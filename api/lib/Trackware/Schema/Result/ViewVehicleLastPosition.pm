use utf8;

package Trackware::Schema::Result::ViewVehicleLastPosition;
use strict;
use warnings;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');

# For the time being this is necessary even for virtual views
__PACKAGE__->table('ViewVehicleLastPosition');

__PACKAGE__->add_columns(
  qw/
    tracker_id
    business_unity_id
    code
    rf_note
    phone_number
    last_event
    event_date
    tx cycle gprs gps interval
    epoch
    address
    lat
    lng
    battery_voltage
    battery_level
    /
);

# do not attempt to deploy() this view
__PACKAGE__->result_source_instance->is_virtual(1);

__PACKAGE__->result_source_instance->view_definition(
  q[
		WITH last_position AS (
			SELECT DISTINCT ON (tracker_id) *
			FROM
				tracker_position 
			WHERE 
				lat <> 0 AND lng <> 0
			ORDER BY tracker_id, event_date DESC

		)
     
                       SELECT
                        t.id AS tracker_id,
                        t.business_unity_id,
                        t.code AS code,
                        t.rf_note,
                        t.phone_number as phone_number,
                        to_char(me.event_date at time zone 'utc' at time zone 'America/Sao_Paulo', 'DD/MM/YYYY HH24:MI:SS') as last_event,
                        me.event_date,
                        extract('epoch' from me.event_date) as epoch,
                        me.tx, me.cycle,  me.gprs, me.gps, me.interval,
                        me.address,
			me.lat, 
			me.lng,
                        me.battery_voltage,
                        CASE WHEN me.battery_voltage > 4150 THEN 100
                             WHEN me.battery_voltage  < 3600 THEN 0
                        ELSE (0.182621 * me.battery_voltage - 658.005)::int END as battery_level
            
		FROM 
			last_position lp
		JOIN 
                  tracker_position me ON lp.id = me.id
                JOIN
                  tracker t on me.tracker_id = t.id
		ORDER BY
			event_date DESC
     ]

);

__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  {id            => "tracker_id"},
  {is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION"},
);


1;
