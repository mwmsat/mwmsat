#<<<
use utf8;
package Trackware::Schema::Result::SmsTracker;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("sms_tracker");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "sms_tracker_id_seq",
  },
  "message",
  { data_type => "text", is_nullable => 0 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "updated_at",
  { data_type => "timestamp", is_nullable => 1 },
  "tracker_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "network_supplier_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "receipt_check_url",
  { data_type => "text", is_nullable => 1 },
  "message_uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "direction",
  { data_type => "text", default_value => "from-tracker", is_nullable => 1 },
  "delivered_at",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("sms_tracker_message_uuid_key", ["message_uuid"]);
__PACKAGE__->belongs_to(
  "network_supplier",
  "Trackware::Schema::Result::NetworkSupplier",
  { id => "network_supplier_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "sms_statuses",
  "Trackware::Schema::Result::SmsStatus",
  { "foreign.sms_tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  { id => "tracker_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->has_many(
  "tracker_notification_sms",
  "Trackware::Schema::Result::TrackerNotificationSms",
  { "foreign.sms_tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:frqW38cHc3W9jkl3er9gRA

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        message             => {required => 0, type => 'Str',},
        sms_status_id       => {required => 0, type => 'Int',},
        updated_at          => {required => 0, type => DataStr,},
        tracker_id          => {required => 0, type => 'Int',},
        network_supplier_id => {required => 0, type => 'Int',},
        send_type           => {required => 0, type => 'Str',}
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $sms_tracker = $self->update(\%values);

      return $sms_tracker;
    },
  };
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
