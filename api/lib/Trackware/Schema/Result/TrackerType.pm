#<<<
use utf8;
package Trackware::Schema::Result::TrackerType;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_type");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tracker_type_id_seq",
  },
  "code",
  { data_type => "text", is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 0 },
  "days",
  { data_type => "integer", is_nullable => 1 },
  "low_battery_level_alert",
  { data_type => "integer", is_nullable => 1 },
  "has_temperature_sensor",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "has_movement_sensor",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "has_gps_sensor",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "has_rf_sensor",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "battery_level",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("tracker_type_code_key", ["code"]);
__PACKAGE__->add_unique_constraint("tracker_type_name_key", ["name"]);
__PACKAGE__->has_many(
  "trackers",
  "Trackware::Schema::Result::Tracker",
  { "foreign.type_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:pA3Z1+KMp6s34re9AXew+g

# You can replace this text with custom code or comments, and it will be preserved on regeneration

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name => {
          required   => 0,
          type       => 'Str',
          post_check => sub {
            my $r = shift;
            !$self->result_source->resultset->search_rs(
              {id => {'!=' => $self->id}, name => $r->get_value('name')})
              ->count;
          }
        },
        code => {
          required   => 0,
          type       => 'Str',
          max_length => 2,
          min_length => 2,

          post_check => sub {
            my $r = shift;
            !$self->result_source->resultset->search_rs(
              {id => {'!=' => $self->id}, name => $r->get_value('code')})
              ->count;
          }
        },
        days          => {required => 0, type => 'Int',},
        battery_level => {
          required   => 0,
          type       => 'Int',
          post_check => sub {
            my $r     = shift;
            my $level = $r->get_value('battery_level');
            return $level >= 100 && $level <= 100_000;
          }
        },

        low_battery_level_alert => {required => 0, type => 'Int',},
        has_temperature_sensor  => {required => 0, type => 'Bool',},

        has_movement_sensor => {required => 0, type => 'Bool',},
        has_gps_sensor      => {required => 0, type => 'Bool',},
        has_rf_sensor       => {required => 0, type => 'Bool',}
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $tracker_type = $self->update(\%values);

      return $tracker_type;
    },
  };
}

sub TO_JSON {
  +{shift->get_columns};
}

__PACKAGE__->meta->make_immutable;

1;
