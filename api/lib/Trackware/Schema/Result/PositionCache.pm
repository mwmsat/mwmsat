#<<<
use utf8;
package Trackware::Schema::Result::PositionCache;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("position_cache");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "position_cache_id_seq",
  },
  "point_id",
  { data_type => "text", is_nullable => 0 },
  "address",
  { data_type => "text", is_nullable => 0 },
  "lat_lng",
  { data_type => "point", is_nullable => 0 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "point_orig_id",
  { data_type => "text", is_nullable => 1 },
  "postal_code",
  { data_type => "varchar", is_nullable => 1, size => 8 },
);
__PACKAGE__->set_primary_key("id");
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:DrQUqgRQ0bzt0hzGqIEN+Q
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        point_id      => {required => 0, type => 'Str'},
        lat_lng       => {required => 0, type => 'Str'},
        address       => {required => 0, type => 'Str'},
        point_orig_id => {required => 0, type => 'Str'},
        postal_code   => {required => 0, type => 'Str'}
      }
    ),
  };
}

sub action_specs {
  my $self = shift;
  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $position_cache = $self->update(\%values);

      return $position_cache;
    },

  };
}

sub lat_lng_as_google {
  my $lat_lng = shift->lat_lng;
  $lat_lng =~ s/\(|\)//g;
  return $lat_lng;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;

1;
