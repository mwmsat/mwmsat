#<<<
use utf8;
package Trackware::Schema::Result::ErbPosition;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("erb_position");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "erb_position_id_seq",
  },
  "source_id",
  { data_type => "integer", is_nullable => 0 },
  "erb_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "latitude",
  {
    data_type     => "double precision",
    default_value => "0.0",
    is_nullable   => 0,
  },
  "longitude",
  {
    data_type     => "double precision",
    default_value => "0.0",
    is_nullable   => 0,
  },
  "azimuth",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "radius",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "last_check",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint(
  "erb_position_source_id_key",
  [
    "source_id",
    "erb_id",
    "latitude",
    "longitude",
    "azimuth",
    "radius",
  ],
);
__PACKAGE__->belongs_to(
  "erb",
  "Trackware::Schema::Result::Erb",
  { id => "erb_id" },
  { is_deferrable => 0, on_delete => "CASCADE", on_update => "NO ACTION" },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:P9gqWTQsHZc2PmBrPsRfCw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
