#<<<
use utf8;
package Trackware::Schema::Result::Command;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("command");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "command_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "message_template",
  { data_type => "text", is_nullable => 0 },
  "create_ts",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "valid",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("command_name_key", ["name"]);
__PACKAGE__->has_many(
  "command_roles",
  "Trackware::Schema::Result::CommandRole",
  { "foreign.command_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:oFhWbtpxmIi40Sq/HjsQTQ

# You can replace this text with custom code or comments, and it will be preserved on regeneration

__PACKAGE__->many_to_many(roles => 'command_roles' => 'role');

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name => {
          required   => 0,
          type       => 'Str',
          post_check => sub {
            my $r = shift;
            return !$self->result_source->resultset->search(
              {
                id    => {'!=' => $self->id},
                valid => 1,
                name  => $r->get_value('name')
              },
              {rows => 1}
            )->next;
          }
        },
        message_template => {required => 0, type => 'Str',},
        roles            => {required => 0, type => 'ArrayRef[Int]',},
      }
    )
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $roles = delete $values{roles};

      my $tracker = $self->update(\%values);

      $tracker->set_roles($self->result_source->schema->resultset('Role')
          ->search({id => {-in => $roles}},)->all);

      return $tracker;
    },
  };
}

sub TO_JSON {
  +{shift->get_columns};
}

__PACKAGE__->meta->make_immutable;

1;

