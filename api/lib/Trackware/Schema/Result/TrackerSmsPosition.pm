#<<<
use utf8;
package Trackware::Schema::Result::TrackerSmsPosition;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_sms_position");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tracker_sms_position_id_seq",
  },
  "tracker_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "iccid",
  { data_type => "numeric", is_nullable => 1 },
  "header",
  { data_type => "text", is_nullable => 1 },
  "imei",
  { data_type => "numeric", is_nullable => 1 },
  "batery_voltage",
  { data_type => "numeric", is_nullable => 1 },
  "mcc",
  { data_type => "integer", is_nullable => 1 },
  "mnc",
  { data_type => "integer", is_nullable => 1 },
  "lac_1",
  { data_type => "integer", is_nullable => 1 },
  "cellid_1",
  { data_type => "integer", is_nullable => 1 },
  "rxlevel_1",
  { data_type => "integer", is_nullable => 1 },
  "lac_2",
  { data_type => "integer", is_nullable => 1 },
  "cellid_2",
  { data_type => "integer", is_nullable => 1 },
  "rxlevel_2",
  { data_type => "integer", is_nullable => 1 },
  "lac_3",
  { data_type => "integer", is_nullable => 1 },
  "cellid_3",
  { data_type => "integer", is_nullable => 1 },
  "rxlevel_3",
  { data_type => "integer", is_nullable => 1 },
  "lac_4",
  { data_type => "integer", is_nullable => 1 },
  "cellid_4",
  { data_type => "integer", is_nullable => 1 },
  "rxlevel_4",
  { data_type => "integer", is_nullable => 1 },
  "lac_5",
  { data_type => "integer", is_nullable => 1 },
  "cellid_5",
  { data_type => "integer", is_nullable => 1 },
  "rxlevel_5",
  { data_type => "integer", is_nullable => 1 },
  "lac_6",
  { data_type => "integer", is_nullable => 1 },
  "cellid_6",
  { data_type => "integer", is_nullable => 1 },
  "rxlevel_6",
  { data_type => "integer", is_nullable => 1 },
  "time_advance",
  { data_type => "integer", is_nullable => 1 },
  "cycle",
  { data_type => "boolean", is_nullable => 1 },
  "tx_jammer",
  { data_type => "boolean", is_nullable => 1 },
  "mode_save",
  { data_type => "boolean", is_nullable => 1 },
  "ala_6",
  { data_type => "boolean", is_nullable => 1 },
  "emergency_mode",
  { data_type => "boolean", is_nullable => 1 },
  "ta_mode",
  { data_type => "boolean", is_nullable => 1 },
  "rf",
  { data_type => "boolean", is_nullable => 1 },
  "firmware_version",
  { data_type => "text", is_nullable => 1 },
  "event_date",
  { data_type => "timestamp", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "sender",
  { data_type => "text", is_nullable => 1 },
  "receiver",
  { data_type => "text", is_nullable => 1 },
  "vehicle_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  { id => "tracker_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->has_many(
  "tracker_sms_raws",
  "Trackware::Schema::Result::TrackerSmsRaw",
  { "foreign.tracker_sms_position_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "vehicle",
  "Trackware::Schema::Result::Vehicle",
  { id => "vehicle_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:f4IetOvrNbJKgdVqaJshcw

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        tracker_id       => {required => 0, type => 'Int',},
        iccid            => {required => 0, type => 'Num',},
        header           => {required => 0, type => 'Str',},
        imei             => {required => 0, type => 'Num',},
        batery_voltage   => {required => 0, type => 'Num',},
        mcc              => {required => 0, type => 'Int',},
        mnc              => {required => 0, type => 'Int',},
        lac_1            => {required => 0, type => 'Int',},
        cellid_1         => {required => 0, type => 'Int',},
        rxlevel_1        => {required => 0, type => 'Int',},
        lac_2            => {required => 0, type => 'Int',},
        cellid_2         => {required => 0, type => 'Int',},
        rxlevel_2        => {required => 0, type => 'Int',},
        lac_3            => {required => 0, type => 'Int',},
        cellid_3         => {required => 0, type => 'Int',},
        rxlevel_3        => {required => 0, type => 'Int',},
        lac_4            => {required => 0, type => 'Int',},
        cellid_4         => {required => 0, type => 'Int',},
        rxlevel_4        => {required => 0, type => 'Int',},
        lac_5            => {required => 0, type => 'Int',},
        cellid_5         => {required => 0, type => 'Int',},
        rxlevel_5        => {required => 0, type => 'Int',},
        lac_6            => {required => 0, type => 'Int',},
        cellid_6         => {required => 0, type => 'Int',},
        rxlevel_6        => {required => 0, type => 'Int',},
        time_advance     => {required => 0, type => 'Boolean',},
        cycle            => {required => 0, type => 'Boolean',},
        tx_jammer        => {required => 0, type => 'Boolean',},
        mode_save        => {required => 0, type => 'Boolean',},
        ala_6            => {required => 0, type => 'Boolean',},
        emergency_mode   => {required => 0, type => 'Boolean',},
        ta_mode          => {required => 0, type => 'Boolean',},
        rf               => {required => 0, type => 'Boolean',},
        firmware_version => {required => 0, type => 'Str',},
        event_date       => {required => 0, type => DataStr,},
        sender           => {required => 0, type => 'Str',},
        receiver         => {required => 0, type => 'Str',},
        vehicle_id       => {required => 0, type => 'Int',},
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $tracker_sms_position = $self->update(\%values);

      return $tracker_sms_position;
    },
  };
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
