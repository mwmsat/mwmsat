#<<<
use utf8;
package Trackware::Schema::Result::TrackerSmsAlert;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_sms_alert");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tracker_sms_alert_id_seq",
  },
  "created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "has_gps_location",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "has_address",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "has_status",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "has_battery_level",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "destination_phone_numbers",
  { data_type => "text", is_nullable => 0 },
  "tracker_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "note",
  { data_type => "text", is_nullable => 1 },
  "alert_type",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  { id => "tracker_id" },
  { is_deferrable => 0, on_delete => "CASCADE", on_update => "NO ACTION" },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:pSYr30E0wiQru70f6K3tdA


# You can replace this text with custom code or comments, and it will be preserved on regeneration

use Trackware::Supplier;

my $supplier = Trackware::Supplier->with_traits('Vivo')->new;

sub send {
  my ($self) = @_;
  my $message = $self->_render($self->tracker);
  $supplier->send($_, $message) for $self->destination_phone_numbers_list;
}

my %destination_phone_numbers_name_map =
  ("11975086326" => 'Teste', "11988612664" => 'Passarinho');

sub destination_phone_numbers_list {
  [
    map { s/^\s+|\s+$//g; $destination_phone_numbers_name_map{"$_"} || $_ }
      split /;/,
    shift->destination_phone_numbers
  ]
}

sub _render {
  my ($self, $tracker) = @_;
  my ($current, undef) = $tracker->tracker_positions->last_two;
  my @msg_components = ();
  push @msg_components,
    sprintf('LAT:%s,LNG:%s',
    map { sprintf(q{%.4f}, $_) } $current->lat,
    $current->lng)
    if $self->has_gps_location && $current->lat && $current->lng;

  push @msg_components, $current->battery_level . '%'
    if $self->has_battery_level && $current->battery_voltage;

  push @msg_components, join(q{,}, (split /\s?,\s+/, $current->address)[0 .. 3])
    if $self->has_address && $current->address;


  return ($tracker->code || $tracker->phone_number) . ': '
    . join(q{;}, @msg_components);
}

__PACKAGE__->meta->make_immutable;
1;
