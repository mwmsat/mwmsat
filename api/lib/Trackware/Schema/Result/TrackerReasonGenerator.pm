#<<<
use utf8;
package Trackware::Schema::Result::TrackerReasonGenerator;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_reason_generator");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tracker_generator_reason_id_seq",
  },
  "code",
  { data_type => "integer", is_nullable => 1 },
  "reason",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("tracker_reason_generator_code_key", ["code"]);
__PACKAGE__->has_many(
  "tracker_positions",
  "Trackware::Schema::Result::TrackerPosition",
  { "foreign.tracker_reason_generator_code" => "self.code" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1Z6hmYcagujPx+p8VG2nEw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
