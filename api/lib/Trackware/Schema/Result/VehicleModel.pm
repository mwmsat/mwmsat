#<<<
use utf8;
package Trackware::Schema::Result::VehicleModel;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("vehicle_model");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "vehicle_model_id_seq",
  },
  "model",
  { data_type => "text", is_nullable => 0 },
  "vehicle_brand_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "fipe_id",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "vehicle_brand",
  "Trackware::Schema::Result::VehicleBrand",
  { id => "vehicle_brand_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->has_many(
  "vehicles",
  "Trackware::Schema::Result::Vehicle",
  { "foreign.vehicle_model_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jua8IspaEzr51MTq+R/1RA

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        model            => {required => 0, type => 'Str',},
        vehicle_brand_id => {required => 0, type => 'Int',},
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $vehicle_model = $self->update(\%values);

      return $vehicle_model;
    },
  };
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
