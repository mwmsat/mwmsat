#<<<
use utf8;
package Trackware::Schema::Result::BusinessUnityPreRegistration;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("business_unity_pre_registration");
__PACKAGE__->add_columns(
  "business_unity_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "seq",
  { data_type => "integer", is_nullable => 0 },
  "used_by",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "network_supplier_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("business_unity_id", "seq");
__PACKAGE__->belongs_to(
  "business_unity",
  "Trackware::Schema::Result::BusinessUnity",
  { id => "business_unity_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "network_supplier",
  "Trackware::Schema::Result::NetworkSupplier",
  { id => "network_supplier_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "used_by",
  "Trackware::Schema::Result::Tracker",
  { id => "used_by" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fjSaGLHtH2RSh5/Pp5ByUw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
