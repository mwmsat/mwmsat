#<<<
use utf8;
package Trackware::Schema::Result::TrackerNotificationSms;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_notification_sms");
__PACKAGE__->add_columns(
  "notification_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "tracker_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "sms_tracker_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "ack_config_position_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("notification_id", "tracker_id", "sms_tracker_id");
__PACKAGE__->belongs_to(
  "ack_config_position",
  "Trackware::Schema::Result::TrackerPosition",
  { id => "ack_config_position_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "notification",
  "Trackware::Schema::Result::Notification",
  { id => "notification_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "sms_tracker",
  "Trackware::Schema::Result::SmsTracker",
  { id => "sms_tracker_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  { id => "tracker_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "tracker_notification",
  "Trackware::Schema::Result::TrackerNotification",
  { notification_id => "notification_id", tracker_id => "tracker_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bDLV0NEdfBcZXD/aY2O4XQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;

use Trackware::Parser::GPS qw(parse);
use Trackware::Util qw(IS_TEST);
use List::Util qw(all);

my %check_map = (
  PS => sub {
    my ($req, $res) = @_;
    all { $res->{body}->{$_} eq $req->{body}->{$_} } keys %{$req->{body}};
  }
);

sub confirm_config {
  my ($self, $tracker, $position) = @_;
  my $request  = parse($self->sms_tracker->message);
  my $response = parse($position->lbs_raw);

  return $self->update({ack_config_position => $position})
    if exists $check_map{$request->{type}}
    && $check_map{$request->{type}}->($request, $response);

  $self->notification->deliver($tracker)
    unless IS_TEST;    # envia config novamente
  return 0;
}


1;
