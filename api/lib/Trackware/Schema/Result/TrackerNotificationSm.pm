use utf8;

package Trackware::Schema::Result::TrackerNotificationSm;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Trackware::Schema::Result::TrackerNotificationSm

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp",
  "PassphraseColumn");

=head1 TABLE: C<tracker_notification_sms>

=cut

__PACKAGE__->table("tracker_notification_sms");

=head1 ACCESSORS

=head2 notification_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 tracker_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 sms_tracker_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 ack_config_position_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "notification_id",
  {data_type => "integer", is_foreign_key => 1, is_nullable => 0},
  "tracker_id",
  {data_type => "integer", is_foreign_key => 1, is_nullable => 0},
  "sms_tracker_id",
  {data_type => "integer", is_foreign_key => 1, is_nullable => 0},
  "ack_config_position_id",
  {data_type => "integer", is_foreign_key => 1, is_nullable => 1},
);

=head1 RELATIONS

=head2 ack_config_position

Type: belongs_to

Related object: L<Trackware::Schema::Result::TrackerPosition>

=cut

__PACKAGE__->belongs_to(
  "ack_config_position",
  "Trackware::Schema::Result::TrackerPosition",
  {id => "ack_config_position_id"},
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);

=head2 notification

Type: belongs_to

Related object: L<Trackware::Schema::Result::Notification>

=cut

__PACKAGE__->belongs_to(
  "notification",
  "Trackware::Schema::Result::Notification",
  {id            => "notification_id"},
  {is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION"},
);

=head2 sms_tracker

Type: belongs_to

Related object: L<Trackware::Schema::Result::SmsTracker>

=cut

__PACKAGE__->belongs_to(
  "sms_tracker",
  "Trackware::Schema::Result::SmsTracker",
  {id            => "sms_tracker_id"},
  {is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION"},
);

=head2 tracker

Type: belongs_to

Related object: L<Trackware::Schema::Result::Tracker>

=cut

__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  {id            => "tracker_id"},
  {is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION"},
);

=head2 tracker_notification

Type: belongs_to

Related object: L<Trackware::Schema::Result::TrackerNotification>

=cut

__PACKAGE__->belongs_to(
  "tracker_notification",
  "Trackware::Schema::Result::TrackerNotification",
  {notification_id => "notification_id", tracker_id => "tracker_id"},
  {is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION"},
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-06-05 11:35:31
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:IJjisC18+3MRO+gDWmz/xA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
