package Trackware::Schema::Result::NotificationReport;

use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');

__PACKAGE__->table('notification_report');
__PACKAGE__->result_source_instance->is_virtual(1);
__PACKAGE__->result_source_instance->view_definition(
  q{
SELECT "me"."id", "me"."message", "me"."create_ts", "me"."rule",
affected_trackers.affected_trackers AS affected_trackers,
deliveries.deliveries AS deliveries
FROM notification me
LEFT JOIN LATERAL(
    SELECT  count(1) AS affected_trackers
    FROM "tracker_notification" "tracker_notifications_alias"
    WHERE "tracker_notifications_alias"."notification_id" = me.id
) affected_trackers ON TRUE
LEFT JOIN LATERAL (
  SELECT count(1) AS deliveries
  FROM tracker_notification_sms tns
  WHERE  tns.notification_id = me.id AND tns.ack_config_position_id IS NOT NULL
) AS deliveries ON TRUE
}
);

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "notification_id_seq",
  },
  "message",
  {data_type => "text", is_nullable => 0},
  "rule",
  {data_type => "json", is_nullable => 0},
  "create_ts",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => {default_value => \"now()"},
  },
  "affected_trackers",
  {data_type => "integer", is_auto_increment => 1, is_nullable => 0,},
  "deliveries",
  {data_type => "integer", is_auto_increment => 1, is_nullable => 0,},

);
1;
