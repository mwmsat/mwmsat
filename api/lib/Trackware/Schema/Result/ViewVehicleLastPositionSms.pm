use utf8;

package Trackware::Schema::Result::ViewVehicleLastPositionSms;
use strict;
use warnings;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');

# For the time being this is necessary even for virtual views
__PACKAGE__->table('ViewVehicleLastPositionSms');

__PACKAGE__->add_columns(
  qw/
    mcc
    mnc
    lac_1
    cellid_1
    rxlevel_1
    lac_2
    cellid_2
    rxlevel_2
    lac_3
    cellid_3
    rxlevel_3
    lac_4
    cellid_4
    rxlevel_4
    lac_5
    cellid_5
    rxlevel_5
    lac_6
    cellid_6
    rxlevel_6
    time_advance
    event_date
    receiver
    sender
    created_at
    firmware_version
    /
);

# do not attempt to deploy() this view
__PACKAGE__->result_source_instance->is_virtual(1);

__PACKAGE__->result_source_instance->view_definition(
  q[
		WITH last_position AS (
			SELECT
				max(id) as max_id 
			FROM
				tracker_sms_position 
			WHERE 
				imei = ?
			GROUP BY
				imei
		)
     
		SELECT
			me.mcc,
			me.mnc,
			me.lac_1 ,
			me.cellid_1 ,
			me.rxlevel_1 ,
			me.lac_2 ,
			me.cellid_2 ,
			me.rxlevel_2 ,
			me.lac_3 ,
			me.cellid_3 ,
			me.rxlevel_3 ,
			me.lac_4 ,
			me.cellid_4 ,
			me.rxlevel_4 ,
			me.lac_5 ,
			me.cellid_5 ,
			me.rxlevel_5 ,
			me.lac_6 ,
			me.cellid_6 ,
			me.rxlevel_6 ,
			me.time_advance,
			me.event_date,
			me.receiver,
			me.sender,
			me.created_at,
			me.firmware_version
		FROM 
			last_position lp
		JOIN 
			tracker_sms_position me ON lp.max_id = me.id
		ORDER BY
			me.event_date DESC
     ]

);

1;
