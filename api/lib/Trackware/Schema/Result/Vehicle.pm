#<<<
use utf8;
package Trackware::Schema::Result::Vehicle;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("vehicle");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "vehicle_id_seq",
  },
  "vehicle_model_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "vehicle_color_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "km",
  { data_type => "double precision", is_nullable => 1 },
  "manufacture_year",
  { data_type => "integer", is_nullable => 1 },
  "model_year",
  { data_type => "integer", is_nullable => 1 },
  "fuel_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "car_plate",
  { data_type => "text", is_nullable => 0 },
  "doors_number",
  { data_type => "integer", is_nullable => 0 },
  "city_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "insurance_company_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "vehicle_fleet_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "business_unity_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "customer_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "business_unity",
  "Trackware::Schema::Result::BusinessUnity",
  { id => "business_unity_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "city",
  "Trackware::Schema::Result::City",
  { id => "city_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "customer",
  "Trackware::Schema::Result::Customer",
  { id => "customer_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "fuel_type",
  "Trackware::Schema::Result::FuelType",
  { id => "fuel_type_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "insurance_company",
  "Trackware::Schema::Result::InsuranceCompany",
  { id => "insurance_company_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "tracker_events",
  "Trackware::Schema::Result::TrackerEvent",
  { "foreign.vehicle_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_positions",
  "Trackware::Schema::Result::TrackerPosition",
  { "foreign.vehicle_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_sms_positions",
  "Trackware::Schema::Result::TrackerSmsPosition",
  { "foreign.vehicle_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "trackers",
  "Trackware::Schema::Result::Tracker",
  { "foreign.vehicle_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "vehicle_color",
  "Trackware::Schema::Result::VehicleColor",
  { id => "vehicle_color_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "vehicle_documents",
  "Trackware::Schema::Result::VehicleDocument",
  { "foreign.vehicle_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "vehicle_fleet",
  "Trackware::Schema::Result::VehicleFleet",
  { id => "vehicle_fleet_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "vehicle_model",
  "Trackware::Schema::Result::VehicleModel",
  { id => "vehicle_model_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->has_many(
  "vehicle_users",
  "Trackware::Schema::Result::VehicleUser",
  { "foreign.vehicle_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nJp2L2TLxnLRtskNd30a3A

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr/;
use Data::Dumper;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        car_plate            => {required => 0, type => 'Str',},
        doors_number         => {required => 0, type => 'Int',},
        vehicle_model_id     => {required => 0, type => 'Int',},
        manufacture_year     => {required => 0, type => 'Int',},
        model_year           => {required => 0, type => 'Int',},
        km                   => {required => 0, type => 'Int',},
        vehicle_color_id     => {required => 0, type => 'Int',},
        fuel_type_id         => {required => 0, type => 'Int',},
        city_id              => {required => 0, type => 'Int',},
        insurance_company_id => {required => 0, type => 'Int',},
        business_unity_id    => {required => 0, type => 'Int',},
        customer_id          => {required => 0, type => 'Int',},
        vehicle_fleet_id     => {required => 0, type => 'Int',}
      },
    ),
  };
}

sub action_specs {
  my $self = shift;
  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $vehicle = $self->update(\%values);

      return $vehicle;
    },

  };
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
