#<<<
use utf8;
package Trackware::Schema::Result::Tracker;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tracker_id_seq",
  },
  "imei",
  { data_type => "text", is_nullable => 1 },
  "iccid",
  { data_type => "text", is_nullable => 1 },
  "phone_number",
  { data_type => "text", is_nullable => 0 },
  "vehicle_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "business_unity_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "tracker_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "customer_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "network_supplier_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "tracker_model_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "code",
  { data_type => "text", is_nullable => 1 },
  "active",
  { data_type => "boolean", default_value => \"true", is_nullable => 1 },
  "profile_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "rf_note",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "business_unity",
  "Trackware::Schema::Result::BusinessUnity",
  { id => "business_unity_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "business_unity_pre_registrations",
  "Trackware::Schema::Result::BusinessUnityPreRegistration",
  { "foreign.used_by" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "business_user_trackers",
  "Trackware::Schema::Result::BusinessUserTracker",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "customer",
  "Trackware::Schema::Result::Customer",
  { id => "customer_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "multitrack_trackers",
  "Trackware::Schema::Result::MultitrackTracker",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "network_supplier",
  "Trackware::Schema::Result::NetworkSupplier",
  { id => "network_supplier_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "profile",
  "Trackware::Schema::Result::TrackerProfile",
  { id => "profile_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "sms_trackers",
  "Trackware::Schema::Result::SmsTracker",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_command_queues",
  "Trackware::Schema::Result::TrackerCommandQueue",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_events",
  "Trackware::Schema::Result::TrackerEvent",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "tracker_model",
  "Trackware::Schema::Result::TrackerModel",
  { id => "tracker_model_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "tracker_notification_sms",
  "Trackware::Schema::Result::TrackerNotificationSms",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_notifications",
  "Trackware::Schema::Result::TrackerNotification",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_positions",
  "Trackware::Schema::Result::TrackerPosition",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_sms_alerts",
  "Trackware::Schema::Result::TrackerSmsAlert",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_sms_positions",
  "Trackware::Schema::Result::TrackerSmsPosition",
  { "foreign.tracker_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "tracker_status",
  "Trackware::Schema::Result::TrackerStatus",
  { id => "tracker_status_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "type",
  "Trackware::Schema::Result::TrackerType",
  { id => "type_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "SET NULL",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "vehicle",
  "Trackware::Schema::Result::Vehicle",
  { id => "vehicle_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:sqLXg6jxHIptoa0f+NM2zA

__PACKAGE__->has_many(
  "smses",
  "Trackware::Schema::Result::SmsTracker",
  {"foreign.tracker_id" => "self.id"},
  {cascade_copy         => 0, cascade_delete => 0},
);


__PACKAGE__->has_many(
  "last_position",
  "Trackware::Schema::Result::ViewVehicleLastPosition",
  {"foreign.tracker_id" => "self.id"},
  {cascade_copy         => 0, cascade_delete => 0},
);


with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;
use Trackware::Util qw(status_line);

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        code => {required => 0, type => 'Str',},

        imei         => {required => 0, type => 'Int',},
        iccid        => {required => 0, type => 'Str',},
        rf_note      => {required => 0, type => 'Str',},
        phone_number => {
          required => 0,
          type     => 'Str',
          filters  => [sub { my ($val) = @_; $val =~ s/\D+//g; $val }]
        },
        vehicle_id          => {required => 0, type => 'Int',},
        business_unity_id   => {required => 0, type => 'Int',},
        tracker_status_id   => {required => 0, type => 'Int',},
        customer_id         => {required => 0, type => 'Int',},
        network_supplier_id => {required => 0, type => 'Int',},
        tracker_model_id    => {required => 0, type => 'Int',},
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $tracker = $self->update(\%values);

      return $tracker;
    },
  };
}

sub render_message {
  my ($self, $message) = @_;
  my %columns = $self->get_columns;
  $message =~ s/\$\Q$_\E/$columns{$_}/g for qw(iccid imei phone_number);
  $message;
}

use JSON qw(encode_json);
use Trackware::SqsManager;
my $sqs = Trackware::SqsManager->new(
  {
    queue_name => 'trackware-outbound-sms'

  }
);

sub send_sms {
  my ($self, %payload) = @_;
  return $sqs->sqs->SendMessage(
    encode_json({tracker_id => $self->id, %payload}));
}

sub check_pending_notifications {
  my $self    = shift;
  my $pending = $self->tracker_notifications_rs->search_rs(
    {'tracker_notification_sms.tracker_id' => undef},
    {join                                  => 'tracker_notification_sms',}
  )->related_resultset('notification');

  $_->check($self) && $_->deliver($self) for $pending->all;

  # while (my $notification = $pending->next) {
  #   $self->send_sms(
  #     text            => $self->render_message($notification->message),
  #     notification_id => $notification->id
  #   ) if $notification->check($self);
  # }
}

sub adopt {
  my ($self, $model, $pre_registration) = @_;
  $self->update(
    {
      business_unity => $pre_registration->business_unity,
      code           => sprintf('%s%s%07d',
        $pre_registration->business_unity->code, $model,
        $pre_registration->seq),
      type => $self->result_source->schema->resultset('TrackerType')
        ->find({code => $model})
    }
  );
  $pre_registration->update({used_by => $self});
}

sub queue_key {
  my $self = shift;
  ($self->business_unity_id || 0) . '.' . $self->id;
}

sub _last_pending_notification {
  my ($self) = @_;
  return $self->tracker_notification_sms->search_rs(
    {ack_config_position_id => undef},
    {
      prefetch => 'sms_tracker',
      order_by => {-desc => 'sms_tracker.created_at'},
      rows     => 1
    }
    )->next
    || ();
}

sub confirm_pending_configuration {
  my ($self, $position) = @_;
  $_->confirm_config($self, $position) for $self->_last_pending_notification;
}

sub notification_payload {
  my $self    = shift;
  my $payload = $self->last_position_rs->as_hashref->next;
  ($payload->{address}) = $payload->{address} =~ /^((?:[^,]+,){3}(?:[^,]+))/;
  $payload->{status_line} = status_line($payload);
  $payload;
}

sub TO_JSON {
  +{shift->get_columns};
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
