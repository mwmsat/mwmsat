use utf8;

package Trackware::Schema::Result::ViewTrackerMessagePerimeter;
use strict;
use warnings;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');

# For the time being this is necessary even for virtual views
__PACKAGE__->table('ViewTrackerMessagePerimeter');

__PACKAGE__->add_columns(
  qw/
    lat
    lng
    azimuth
    radius
    /
);

# do not attempt to deploy() this view
__PACKAGE__->result_source_instance->is_virtual(1);

__PACKAGE__->result_source_instance->view_definition(
  q[
		SELECT
			ep.latitude as lat, 
			ep.longitude as lng,
			ep.azimuth as azimuth,
			ep.radius as radius,
			e.rxl
		FROM
			erb_position ep
		JOIN
			erb e
		ON
			ep.erb_id = e.id
		WHERE
			e.mcc = ?
		AND
			e.mnc = ?
		AND
			( lac = ? or lac = ? or lac = ? or lac = ? or lac = ? or lac = ? )
		AND
			( cid = ? or cid = ? or cid = ? or cid = ? or cid = ? or cid = ? )
	]
);

1;
