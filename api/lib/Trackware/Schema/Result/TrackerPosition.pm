#<<<
use utf8;
package Trackware::Schema::Result::TrackerPosition;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_position");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tracker_position_id_seq",
  },
  "lat",
  { data_type => "double precision", is_nullable => 0 },
  "lng",
  { data_type => "double precision", is_nullable => 0 },
  "event_date",
  { data_type => "timestamp", is_nullable => 1 },
  "tracker_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "vehicle_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "battery_voltage",
  { data_type => "numeric", is_nullable => 1 },
  "imei",
  { data_type => "numeric", is_nullable => 1 },
  "position_counter",
  { data_type => "integer", is_nullable => 1 },
  "temperature",
  { data_type => "numeric", is_nullable => 1 },
  "firmware_version",
  { data_type => "double precision", is_nullable => 1 },
  "tracker_reason_generator_code",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "precision",
  { data_type => "text", is_nullable => 1 },
  "position_type",
  { data_type => "text", is_nullable => 1 },
  "lbs_raw",
  { data_type => "text", is_nullable => 1 },
  "address",
  { data_type => "text", is_nullable => 1 },
  "tx",
  { data_type => "integer", is_nullable => 1 },
  "cycle",
  { data_type => "integer", is_nullable => 1 },
  "gprs",
  { data_type => "integer", is_nullable => 1 },
  "gps",
  { data_type => "integer", is_nullable => 1 },
  "r1",
  { data_type => "integer", is_nullable => 1 },
  "interval",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  { id => "tracker_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->has_many(
  "tracker_notification_sms",
  "Trackware::Schema::Result::TrackerNotificationSms",
  { "foreign.ack_config_position_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "tracker_reason_generator_code",
  "Trackware::Schema::Result::TrackerReasonGenerator",
  { code => "tracker_reason_generator_code" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->belongs_to(
  "vehicle",
  "Trackware::Schema::Result::Vehicle",
  { id => "vehicle_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lUOzjy/2xNFimfJOJnVhKg

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        tracker_id => {
          required => 0,
          type     => 'Int',
        },
        lat => {
          required => 0,
          type     => 'Num',
        },
        lng => {
          required => 0,
          type     => 'Num',
        },
        event_date => {
          required => 0,
          type     => DataStr,
        },
        speed => {
          required => 0,
          type     => 'Num',
        },
        sat_number => {
          required => 0,
          type     => 'Int',
        },
        hdop => {
          required => 0,
          type     => 'Int',
        },
        vehicle_id => {
          required => 0,
          type     => 'Int',
        },
        transaction => {
          required => 0,
          type     => 'Int',
        },
        tracker_reason_generator_code => {
          required => 0,
          type     => 'Int',
        },
        imei => => {
          required => 0,
          type     => 'Int',
        },
        battery_voltage => {
          required => 0,
          type     => 'Num',
        },
        battery_voltage_bkp => {
          required => 0,
          type     => 'Num',
        },
        hourmeter => {
          required => 0,
          type     => 'Num',
        },
        message_size => {
          required => 0,
          type     => 'Num',
        },
        odometer => {
          required => 0,
          type     => 'Num',
        },
        position_counter => {
          required => 0,
          type     => 'Num',
        },
        temperature => {
          required => 0,
          type     => 'Num',
        },
        package_type => {
          required => 0,
          type     => 'Int',
        },
        pcb_version => {
          required => 0,
          type     => 'Num',
        },
        firmware_version => {
          required => 0,
          type     => 'Float',
        },
        position_amount => {
          required => 0,
          type     => 'Int',
        },
        position_size => {
          required => 0,
          type     => 'Int',
        },
        inputs => {
          required => 0,
          type     => 'Num',
        },
        outputs => {
          required => 0,
          type     => 'Num',
        },
        precision => {
          required => 0,
          type     => 'Str',
        },
        position_type => {
          required => 0,
          type     => 'Str',
        },
        lbs_raw => {
          required => 0,
          type     => 'Str',
        },
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $tracker_position = $self->update( \%values );

      return $tracker_position;
    },
  };
}

sub battery_level {
  my $level = shift->battery_voltage;
  if ( $level > 4150 ) { return 100 }
  if ( $level < 3600 ) { return 0 }
  return int( 0.182621 * $level - 658.005 );
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
