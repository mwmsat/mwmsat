#<<<
use utf8;
package Trackware::Schema::Result::BusinessUnity;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("business_unity");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "business_unity_id_seq",
  },
  "corporate_name",
  { data_type => "text", is_nullable => 0 },
  "fancy_name",
  { data_type => "text", is_nullable => 1 },
  "register",
  { data_type => "numeric", is_nullable => 0 },
  "address_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "is_active",
  { data_type => "boolean", default_value => \"true", is_nullable => 1 },
  "website",
  { data_type => "text", is_nullable => 1 },
  "email",
  { data_type => "text", is_nullable => 1 },
  "parent_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "phone",
  { data_type => "text", is_nullable => 0 },
  "phone_2",
  { data_type => "text", is_nullable => 1 },
  "domain",
  { data_type => "text", is_nullable => 0 },
  "code",
  { data_type => "text", is_nullable => 1 },
  "api_key",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("business_unity_api_key_key", ["api_key"]);
__PACKAGE__->add_unique_constraint("business_unity_code_key", ["code"]);
__PACKAGE__->belongs_to(
  "address",
  "Trackware::Schema::Result::Address",
  { id => "address_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "business_unities",
  "Trackware::Schema::Result::BusinessUnity",
  { "foreign.parent_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "business_unity_pre_registrations",
  "Trackware::Schema::Result::BusinessUnityPreRegistration",
  { "foreign.business_unity_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "business_unity_users",
  "Trackware::Schema::Result::BusinessUnityUser",
  { "foreign.business_unity_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "customers",
  "Trackware::Schema::Result::Customer",
  { "foreign.business_unity_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "parent",
  "Trackware::Schema::Result::BusinessUnity",
  { id => "parent_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "tracker_command_layouts",
  "Trackware::Schema::Result::TrackerCommandLayout",
  { "foreign.business_unity_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "trackers",
  "Trackware::Schema::Result::Tracker",
  { "foreign.business_unity_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "vehicle_fleets",
  "Trackware::Schema::Result::VehicleFleet",
  { "foreign.business_unity_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "vehicles",
  "Trackware::Schema::Result::Vehicle",
  { "foreign.business_unity_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-12-03 17:01:15
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:sldkQhGMJDMSIN6ZUIg3Nw

__PACKAGE__->has_many(
  "pre_registrations",
  "Trackware::Schema::Result::BusinessUnityPreRegistration",
  {"foreign.business_unity_id" => "self.id"},
  {cascade_copy                => 0, cascade_delete => 0},
);

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr CNPJ/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        corporate_name => {required => 0, type => 'Str',},
        fancy_name     => {required => 0, type => 'Str',},

        # code => {
        #   required   => 1,
        #   type       => 'Str',
        #   min_length => 6,
        #   max_length => 6,
        #   filters  => [qw(upper flatten)],
        #   post_check => sub {
        #     my $r = shift;
        #     warn $r->get_value('code')	    ;
        #     !$self->result_source->resultset->search_rs(
        #       { id => { '!=' => $self->id }, code => $r->get_value('code') },
        #       { rows => 1 } )->count;
        #   }
        # },
        register => {
          required   => 0,
          filters    => [sub { my ($v) = @_; $v =~ s/\D+//gr }],
          type       => CNPJ,
          post_check => sub {
            my $r = shift;
            !$self->result_source->resultset->search_rs(
              {
                id       => {'!=' => $self->id},
                register => $r->get_value('register')
              },
              {rows => 1}
            )->count;
          }
        },
        address_id => {required => 0, type => 'Int',},
        is_active  => {required => 0, type => 'Bool',},
        website    => {required => 0, type => 'Str',},
        email      => {required => 0, type => 'Str',},
        phone      => {required => 0, type => 'Str',},
        phone_2    => {required => 0, type => 'Str',},
        parent_id  => {required => 0, type => 'Int',},
        domain     => {required => 0, type => 'Str',},
      }
    ),
  };
}
use Digest::SHA qw(sha256_hex);

sub action_specs {
  my $self = shift;
  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $business_unity = $self->update(
        {
          %values, code => uc
            substr(sha256_hex($values{register} || $self->register), 0, 6)

        }
      );

      return $business_unity;
    },

  };
}


__PACKAGE__->meta->make_immutable;
1;
