#<<<
use utf8;
package Trackware::Schema::Result::Notification;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("notification");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "notification_id_seq",
  },
  "message",
  { data_type => "text", is_nullable => 0 },
  "rule",
  { data_type => "json", is_nullable => 0 },
  "create_ts",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->has_many(
  "notification_areas",
  "Trackware::Schema::Result::NotificationArea",
  { "foreign.notification_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_notification_sms",
  "Trackware::Schema::Result::TrackerNotificationSms",
  { "foreign.notification_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "tracker_notifications",
  "Trackware::Schema::Result::TrackerNotification",
  { "foreign.notification_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:LzqgEvijrsh04z6e1M//AQ

# You can replace this text with custom code or comments, and it will be preserved on regeneration

__PACKAGE__->load_components('InflateColumn::Serializer');

__PACKAGE__->remove_column('rule');

__PACKAGE__->add_columns(
  rule => {
    data_type            => "json",
    is_nullable          => 1,
    'serializer_class'   => 'JSON',
    'serializer_options' => {allow_blessed => 1, convert_blessed => 1},
  }
);

__PACKAGE__->many_to_many(trackers => tracker_notifications => tracker =>);

__PACKAGE__->has_many(
  "areas",
  "Trackware::Schema::Result::NotificationArea",
  {"foreign.notification_id" => "self.id"},
  {cascade_copy              => 0, cascade_delete => 0},
);

use DDP;

use DateTime;
use DateTimeX::Easy qw(parse);
use Scalar::Util qw(looks_like_number);

sub check {
  my ($self,    $tracker)  = @_;
  my ($current, $previous) = $tracker->tracker_positions->last_two;

  my $current_event_date;

  if ($current) {
    $current_event_date = $current->event_date;
    $current_event_date->set_time_zone('UTC');
    $current_event_date->set_time_zone('America/Sao_Paulo');
  }

  my $rule = $self->rule;
  p($rule);
  return 0
    if $self->areas->count && !$self->areas->check($self, $previous, $current);

  return 0
    if $current
    && (defined $rule->{'tt-start'} || defined $rule->{'tt-end'})
    && (
    (
      $rule->{'tt-start'} && ($current_event_date <= parse($rule->{'tt-start'}))
    )
    || ($rule->{'tt-end'} && ($current_event_date >= parse($rule->{'tt-end'})))
    );

  return 0
    if $current
    && defined $current->temperature
    && (defined $rule->{'temperature-max'}
    || defined $rule->{'temperature-min'})
    && (($current->temperature <= $rule->{'temperature-max'})
    || ($current->temperature >= $rule->{'temperature-min'}));

  return 0
    if $current
    && defined $current->battery_level
    && (defined $rule->{'battery-max'} || defined $rule->{'battery-min'})
    && (($current->battery_level > $rule->{'battery-max'})
    || ($current->battery_level < $rule->{'battery-min'}));
  return 1;
}

sub deliver {
  my ($self, $tracker) = @_;
  $tracker->send_sms(
    text            => $tracker->render_message($self->message),
    notification_id => $self->id
  );
}

__PACKAGE__->meta->make_immutable;
1;
