#<<<
use utf8;
package Trackware::Schema::Result::TrackerCommandLayout;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_command_layout");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "tracker_command_layout_id_seq",
  },
  "command",
  { data_type => "text", is_nullable => 0 },
  "is_active",
  { data_type => "boolean", default_value => \"true", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "business_unity_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "business_unity",
  "Trackware::Schema::Result::BusinessUnity",
  { id => "business_unity_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "tracker_command_queues",
  "Trackware::Schema::Result::TrackerCommandQueue",
  { "foreign.tracker_command_layout_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:+xU1XoHFt9i5oPs96n08bg
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name              => {required => 0, type => 'Str',},
        command           => {required => 0, type => 'Str',},
        business_unity_id => {required => 0, type => 'Int'}
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $tracker_command_layout = $self->update(\%values);

      return $tracker_command_layout;
    },
  };
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
