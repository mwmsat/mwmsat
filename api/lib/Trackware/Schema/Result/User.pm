#<<<
use utf8;
package Trackware::Schema::Result::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("user");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "user_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "email",
  { data_type => "text", is_nullable => 0 },
  "birthdate",
  { data_type => "date", is_nullable => 1 },
  "cpf",
  { data_type => "numeric", is_nullable => 1 },
  "gender",
  {
    data_type => "enum",
    extra => { custom_type_name => "gender", list => ["m", "f"] },
    is_nullable => 1,
  },
  "phone",
  { data_type => "text", is_nullable => 1 },
  "mobile_phone",
  { data_type => "text", is_nullable => 1 },
  "password",
  { data_type => "text", is_nullable => 0 },
  "reset_password_key",
  { data_type => "text", is_nullable => 1 },
  "is_active",
  { data_type => "boolean", is_nullable => 1 },
  "has_profile",
  { data_type => "boolean", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->has_many(
  "actions_logs",
  "Trackware::Schema::Result::ActionsLog",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "business_unity_users",
  "Trackware::Schema::Result::BusinessUnityUser",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "business_user_trackers",
  "Trackware::Schema::Result::BusinessUserTracker",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "customer_users",
  "Trackware::Schema::Result::CustomerUser",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "user_addresses",
  "Trackware::Schema::Result::UserAddress",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "user_roles",
  "Trackware::Schema::Result::UserRole",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "user_sessions",
  "Trackware::Schema::Result::UserSession",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "vehicle_users",
  "Trackware::Schema::Result::VehicleUser",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bskJxJ27MpzgEg5hINkXyQ

__PACKAGE__->many_to_many(roles => user_roles => 'role');

__PACKAGE__->many_to_many(addresses => user_addresses => 'address');
__PACKAGE__->many_to_many(
  managed_business_units => business_unity_users => 'business_unity');

__PACKAGE__->many_to_many(
  managed_trackers => "business_user_trackers" => 'tracker');

__PACKAGE__->remove_column('password');
__PACKAGE__->add_column(
  password => {
    data_type               => "text",
    passphrase              => 'crypt',
    passphrase_class        => 'BlowfishCrypt',
    passphrase_args         => {cost => 8, salt_random => 1,},
    passphrase_check_method => 'check_password',
    is_nullable             => 0
  },
);

__PACKAGE__->has_many(
  "sessions", "Trackware::Schema::Result::UserSession",
  {"foreign.user_id" => "self.id"}, {cascade_copy => 0, cascade_delete => 0},
);

with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr TimeStr/;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        name         => {required => 0, type => 'Str',},
        birthdate    => {required => 0, type => DataStr,},
        cpf          => {required => 0, type => 'Num',},
        gender       => {required => 0, type => 'Str',},
        phone        => {required => 0, type => 'Str',},
        mobile_phone => {required => 0, type => 'Str',},
        role         => {required => 0, type => 'Str',},
        email        => {
          required   => 0,
          type       => EmailAddress,
          post_check => sub {
            my $r = shift;

            return 1 if $self->email eq $r->get_value('email');

            return 0
              if $self->resultset_find({email => lc($r->get_value('email'))});

            return 1;
          }
        },
        password => {
          required   => 0,
          type       => 'Str',
          dependent  => {password_confirm => {required => 1, type => 'Str',},},
          post_check => sub {
            my $r = shift;
            return 1
              if ($r->get_value('password') eq $r->get_value('password_confirm')
              && length $r->get_value('password') >= 5);
          },
        },
        managed_business_units => {
          required   => 0,
          type       => 'Maybe[ArrayRef[Int]]',
          post_check => sub {
            my $r = shift;
            my @ids = @{$r->get_value('managed_business_units') || []};
            return 1 unless scalar @ids;
            $self->result_source->schema->resultset('BusinessUnity')
              ->search_rs({id => {-in => \@ids}})->count == scalar @ids;
          }
        },

        managed_trackers => {
          required   => 0,
          type       => 'Maybe[ArrayRef[Int]]',
          post_check => sub {
            my $r = shift;
            my @ids = @{$r->get_value('managed_trackers') || []};
            return 1 unless scalar @ids;
            $self->business_unity_users_rs->related_resultset('business_unity')
              ->related_resultset('trackers')
              ->search_rs({'trackers.id' => {-in => \@ids}})->count ==
              scalar @ids;
          }
        },

        reset_password_key => {required => 0, type => 'Str',},
        is_active          => {required => 0, type => 'Bool',},
        has_profile        => {required => 0, type => 'Bool',},
      }
    ),
  };
}

sub action_specs {
  my $self = shift;

  return {
    update => sub {
      my %values = shift->valid_values;
      delete $values{password_confirm};
      not defined $values{$_} and delete $values{$_} for keys %values;
      $self->set_managed_business_units(
        [
          $self->result_source->schema->resultset('BusinessUnity')->search_rs(
            {id => {-in => delete $values{managed_business_units}}}
          )->all
        ]
      ) if $values{managed_business_units};

      $self->set_managed_trackers(
        [
          $self->result_source->schema->resultset('Tracker')
            ->search_rs({id => {-in => delete $values{managed_trackers}}})->all
        ]
      ) if $values{managed_trackers};

      return $self->update(\%values);
    },
  };
}

sub allowed_commands_rs {
  shift->user_roles->related_resultset('role')
    ->related_resultset('command_roles')->related_resultset('command');
}

sub queues {
  my $self = shift;
  {
    # trackers
    t => [map { $_->id } $self->managed_trackers],

    # business unities
    bu => [map { $_->id } $self->managed_business_units],
  };
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
