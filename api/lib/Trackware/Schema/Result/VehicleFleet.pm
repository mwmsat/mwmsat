#<<<
use utf8;
package Trackware::Schema::Result::VehicleFleet;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("vehicle_fleet");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "vehicle_fleet_id_seq",
  },
  "business_unity_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "customer_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "business_unity",
  "Trackware::Schema::Result::BusinessUnity",
  { id => "business_unity_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "customer",
  "Trackware::Schema::Result::Customer",
  { id => "customer_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);
__PACKAGE__->has_many(
  "vehicles",
  "Trackware::Schema::Result::Vehicle",
  { "foreign.vehicle_fleet_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/PdsrRtqdM0+d9F6pVSQug
with 'Trackware::Role::Verification';
with 'Trackware::Role::Verification::TransactionalActions::DBIC';
with 'Trackware::Schema::Role::ResultsetFind';

use Data::Verifier;
use MooseX::Types::Email qw/EmailAddress/;
use Trackware::Types qw /DataStr/;
use Data::Dumper;

sub verifiers_specs {
  my $self = shift;
  return {
    update => Data::Verifier->new(
      filters => [qw(trim)],
      profile => {
        business_unity_id => {required => 0, type => 'Int',},
        customer_id       => {required => 0, type => 'Int',},
        name              => {required => 0, type => 'Str',}
      },
    ),
  };
}

sub action_specs {
  my $self = shift;
  return {
    update => sub {
      my %values = shift->valid_values;

      not defined $values{$_} and delete $values{$_} for keys %values;

      my $vehicle_fleet = $self->update(\%values);

      return $vehicle_fleet;
    },

  };
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
