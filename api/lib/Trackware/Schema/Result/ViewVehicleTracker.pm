use utf8;

package Trackware::Schema::Result::ViewVehicleTracker;
use strict;
use warnings;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');

# For the time being this is necessary even for virtual views
__PACKAGE__->table('ViewVehicleTracker');

__PACKAGE__->add_columns(
  qw/
    lat
    lng
    speed
    event_date
    reason_generator
    created_at
    firmware_version
    car_plate
    position_counter
    precision
    position_type
    /
);

# do not attempt to deploy() this view
__PACKAGE__->result_source_instance->is_virtual(1);

__PACKAGE__->result_source_instance->view_definition(
  q[
		SELECT
			me.lat,
			me.lng,
			me.event_date + ?::interval as event_date,
			me.created_at + ?::interval as created_at,
			me.speed as speed,
			me.firmware_version as firmware_version,
			me.position_counter as position_counter,
			me.precision as precision,
			me.position_type as position_type,
			tracker_reason_generator.reason as reason_generator,
			vehicle.car_plate as car_plate
		FROM
			tracker_position me
		LEFT JOIN
			tracker_reason_generator tracker_reason_generator ON tracker_reason_generator.code = me.tracker_reason_generator_code
		LEFT JOIN
			vehicle vehicle ON vehicle.id = me.vehicle_id
		WHERE 
			me.id
		IN
		(
			SELECT
				id
			FROM
				tracker_position
			WHERE
				vehicle_id = ?
		)
		ORDER BY
			me.created_at DESC
		LIMIT ?
		OFFSET ?
    ]

);

1;
