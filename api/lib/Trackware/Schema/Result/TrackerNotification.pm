#<<<
use utf8;
package Trackware::Schema::Result::TrackerNotification;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("tracker_notification");
__PACKAGE__->add_columns(
  "notification_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "tracker_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "create_ts",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
);
__PACKAGE__->add_unique_constraint(
  "tracker_notification_notification_id_tracker_id_key",
  ["notification_id", "tracker_id"],
);
__PACKAGE__->belongs_to(
  "notification",
  "Trackware::Schema::Result::Notification",
  { id => "notification_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "tracker",
  "Trackware::Schema::Result::Tracker",
  { id => "tracker_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->has_many(
  "tracker_notification_sms",
  "Trackware::Schema::Result::TrackerNotificationSms",
  {
    "foreign.notification_id" => "self.notification_id",
    "foreign.tracker_id"      => "self.tracker_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:0BWFDrs/z10TtpTrTI/HmQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
