#<<<
use utf8;
package Trackware::Schema::Result::NotificationArea;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("notification_area");
__PACKAGE__->add_columns(
  "notification_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "center",
  { data_type => "geography", is_nullable => 0, size => "58880,16" },
  "radius",
  { data_type => "integer", is_nullable => 0 },
);
__PACKAGE__->belongs_to(
  "notification",
  "Trackware::Schema::Result::Notification",
  { id => "notification_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:a458uTXybmEqVe+geFLJ4A

# You can replace this text with custom code or comments, and it will be preserved on regeneration


__PACKAGE__->meta->make_immutable;
1;
