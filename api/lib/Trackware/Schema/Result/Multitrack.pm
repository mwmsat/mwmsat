#<<<
use utf8;
package Trackware::Schema::Result::Multitrack;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("multitrack");
__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "multitrack_id_seq",
  },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "description",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("multitrack_name_key", ["name"]);
__PACKAGE__->has_many(
  "multitrack_trackers",
  "Trackware::Schema::Result::MultitrackTracker",
  { "foreign.multitrack_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
#>>>

# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-10-23 10:56:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ZvGPS37dlaEZc2CoLjNjKg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->many_to_many(trackers => multitrack_trackers => 'tracker');

sub TO_JSON {
  my $self = shift;
  $self->result_source->resultset->search_rs(
    {'me.id' => $self->id},
    {
      join       => {multitrack_trackers => 'tracker'},
      collapse   => 1,
      '+columns' => [
        {
          "multitrack_trackers.multitrack_id",
          "multitrack_trackers.tracker_id",
          map { ("multitrack_trackers.tracker.$_" => "tracker.$_") }
            qw(phone_number iccid code id)
        },
      ]
    }
  )->as_hashref->next;
}

__PACKAGE__->meta->make_immutable;
1;
