package Trackware::Geolocation;

use Moose;
use Furl;
use JSON::XS;
use URI;
use Geo::Coordinates::DecimalDegrees;
use Digest::MD5 qw(md5_hex);
use HTTP::Request;
use Trackware::Log;

has uri => (
  is      => 'ro',
  isa     => 'Str',
  default => 'http://maps.googleapis.com/maps/api/geocode/json'
);

has uri_direction => (
  is      => 'ro',
  isa     => 'Str',
  default => 'http://maps.googleapis.com/maps/api/directions/json'
);

has schema => (is => 'rw', isa => 'Trackware::Schema', required => 0);

has '_geolocation_cache' =>
  (is => 'rw', lazy => 1, builder => '_build_geolocationcache');

my $logger = get_logger;

sub geo_by_address {
  my ($self, $address) = @_;
  my $res;

  eval {
    my $uri = URI->new($self->uri . "?address=$address&sensor=false&region=br");
    my $req = &access_uri($uri);
    $res = decode_json($req->content);
  };

  if ($@) {
    return $@;
  }

  return $res->{results}[0]{geometry}{location};
}

sub geo_by_point {
  my ($self, $points) = @_;
  my $res;
  my $data;

  return 0 unless exists $points->{origin} && exists $points->{destination};

  eval {
    my $uri = $self->uri_direction
      . "?origin=$points->{origin}&destination=$points->{destination}&sensor=false&region=br";
    my $req = &access_uri($uri);
    my $con = $req->content;
    $res = decode_json($req->content);

    if (exists $res->{'routes'}[0]{'legs'}[0]{'steps'}) {
      my $distance = $res->{'routes'}[0]{'legs'}[0]{'distance'}{'value'};
      my @polyline;
      foreach my $point (@{$res->{'routes'}[0]{'legs'}[0]{'steps'}}) {
        push(@polyline,
          $point->{start_location}{lat} . ',' . $point->{start_location}{lng},
          $point->{end_location}{lat} . ',' . $point->{end_location}{lng});
      }

      $data = {'distance' => $distance, 'polyline' => encode_json(\@polyline)};

    }
    else {
      die 'Error.zero_results';
    }
  };

  if ($@) {
    return $@;
  }

  return $data;
}

sub reverse_geo_code {
  my ($self, $lat_lng) = @_;

  my $data;

  return 0 unless $lat_lng;

  $lat_lng =~ s/''//g;

  eval {
    my $uri = $self->uri . '?latlng=' . $lat_lng . '&language=pt-br';

    my $req = &access_uri($uri);

    my $con = $req->content;

    my $res = decode_json($req->content);

    if (exists $res->{results}[0]{formatted_address}) {
      my $cep;

      foreach my $component (@{$res->{results}[0]{address_components}}) {
        $cep =
            $component->{'types'}[0] eq 'postal_code'
          ? $component->{'long_name'}
          : undef;
      }

      $data = {
        'formatted_address' => $res->{results}[0]{formatted_address},
        'cep'               => $cep
      };
    }
    else {
      die 'Error.zero_results';
    }
  };

  if ($@) {
    return undef;
  }

  return $data;
}

sub find_postal_code {
  my ($self, $address) = @_;
  my $res;

  eval {
    my $uri = URI->new($self->uri . "?address=$address&sensor=false&region=br");
    my $req = &access_uri($uri);
    $res = decode_json($req->content);
  };

  if ($@) {
    return $@;
  }

  return $res->{results}[0]{geometry}{location};
}

sub access_uri {
  my ($uri, $req_type) = @_;
  my $req;

  die 'URI inválida' if !$uri;

  my $furl = Furl->new(
    agent    => 'MyGreatUA/2.0',
    timeout  => 10,
    ssl_opts => {SSL_verify_mode => 'SSL_VERIFY_NONE',},
  );

  eval { $req = $req_type ? $furl->request($uri) : $furl->get($uri); };

  die $@ if $@;

  return $req;
}

sub do_google_request {
  my ($self, $body) = @_;

  my $data;

  my $gen_key_for_body = md5_hex(
    join ':',
    (
      map { $body->{$_} }
        qw/carrier homeMobileCountryCode homeMobileNetworkCode radioType/
    ),
    map { $_->{cellId} . ':' . $_->{locationAreaCode} } @{$body->{cellTowers}}
  );

  my $google_cached = $self->get_from_google_cache($gen_key_for_body);

  if ($google_cached) {
    $logger->info("Google request saved: $gen_key_for_body on cache!");

    $data->{calc_latitude}  = $google_cached->{location}{lat};
    $data->{calc_longitude} = $google_cached->{location}{lng};
    $data->{calc_accuracy}  = $google_cached->{accuracy};

    $data->{calc_source} = 'cache-google';
  }
  else {
    eval {
      my $request = HTTP::Request->new(
        POST => 'https://www.googleapis.com/geolocation/v1/geolocate?key='
          . $ENV{GOOGLE_GEOLOCATION_API},
        ['Content-Type' => 'application/json'], encode_json($body)
      );

      my $res = &access_uri($request, 1);

      if ($res->code == 200) {
        my $obj = decode_json $res->body;

        $self->save_to_google_cache($gen_key_for_body, $body, $obj);

        $data->{calc_latitude}  = $obj->{location}{lat};
        $data->{calc_longitude} = $obj->{location}{lng};
        $data->{calc_accuracy}  = $obj->{accuracy};

        $data->{calc_source} = 'google';
      }
      else {
        $data->{other}{google_error} = $res->body;
        $logger->error("Google Error:" . $res->body);
      }
    };
  }

  if ($@) {
    $self->logger->error(
      "Error while trying to resolve google.geolocation: $@");
  }

  return $data;
}

sub _build_geolocationcache {
  my $self = shift;

  $self->schema->resultset('GeolocationCache');
}

sub get_from_google_cache {
  my ($self, $key) = @_;

  my $found = $self->_geolocation_cache()->search({cache_key => ($key)},
    {result_class => 'DBIx::Class::ResultClass::HashRefInflator'})->next;

  return decode_json($found->{response}) if $found;

  return undef;
}

sub save_to_google_cache {
  my ($self, $key, $req, $res) = @_;

  $self->_geolocation_cache()->create(
    {
      cache_key => ($key),
      request   => encode_json $req,
      response  => encode_json $res,
    }
  );
}

1;
