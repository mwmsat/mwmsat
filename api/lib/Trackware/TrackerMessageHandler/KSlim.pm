package Trackware::TrackerMessageHandler::KSlim;
use namespace::autoclean;
use Moose::Role;

has message => (is => 'ro', isa => 'HashRef', required => 1);

sub prepare {
  my $self = shift;

  my @message = split ',', $self->message->{text};

  my %result = &build_array(@message);

  $result{event_date} = $self->message->{'when'};
  $result{sender}     = $self->message->{'sender'};
  $result{receiver}   = $self->message->{'receiver'};

  return %result;
}

sub build_array {
  my @values = @_;

  my $size = @values;

  my @indexes = qw/
    header
    imei
    iccid
    batery_voltage
    none
    mcc
    mnc
    lac_1
    cellid_1
    rxlevel_1
    lac_2
    cellid_2
    rxlevel_2
    lac_3
    cellid_3
    rxlevel_3
    lac_4
    cellid_4
    rxlevel_4
    lac_5
    cellid_5
    rxlevel_5
    lac_6
    cellid_6
    rxlevel_6
    time_advance
    cycle
    tx_jammer
    none_2
    mode_save
    ala_6
    none_3
    none_4
    emergency_mode
    ta_mode
    rf
    firmware_version
    /;

  my %result = (map { $indexes[$_] => $values[$_] } 0 .. scalar @indexes - 1);

  return %result;
}

1;
