package Trackware::TrackerMessageHandler::GPRS_LBS;
use namespace::autoclean;
use Moose::Role;

use utf8;
use strict;
use warnings;
use Time::Piece;
use Math::BigInt;
use Geo::Coordinates::DecimalDegrees;

has message => (is => 'ro', isa => 'HashRef', required => 1);

sub prepare {
  my $self = shift;

  my @pack = unpack("A6A16A4A4A4A4A4A4", $self->message->{text});
  my %result = &build_array(@pack);

  return %result;
}

sub build_array {
  my @values = @_;

  my $size = @values;

  my @indexes = qw/
    header
    imei
    position_counter
    mcc
    mnc
    lac
    cellid
    rxl
    /;

#my %result = ( map { $indexes[$_] => $indexes[$_] eq 'lac' || $indexes[$_] eq 'cellid' ? $values[$_] :  eval( "0x" .  $values[$_] ) } 0 .. scalar @indexes - 1 );

  my %result =
    (map { $indexes[$_] => eval("0x" . $values[$_]) } 0 .. scalar @indexes - 1);

  return %result;
}

1;
