package Trackware::SqsManager;

use utf8;
use strict;
use warnings;

use Moose;
use Config::General;
use Amazon::SQS::Simple;

use Trackware::Util qw(IS_TEST);

has queue_name => (
  is      => 'rw',
  isa     => 'Str',
  default => 'trackware',

  #   default => 'trackware_sms',   #test queue
);

has sqs => (is => 'ro', lazy => 1, builder => '_build_sqs');

my $app_dir = $ENV{TRACKWARE_APP_DIR} || '/www/aware/trackware';

my $conf   = Config::General->new("$app_dir/api/trackware_local.conf");
my %config = $conf->getall;

if (!$config{aws}) {
  $conf   = Config::General->new("$app_dir/api/trackware.conf");
  %config = $conf->getall;
}

die "Can't find aws configuration" unless $config{aws};

sub _build_sqs {
  my $self = shift;

  my $access_key = $config{aws}{access_key_id};
  my $secret_key = $config{aws}{secret_access_key};
  warn "$access_key => $secret_key";
  my $sqs = Amazon::SQS::Simple->new($access_key, $secret_key, 
    Endpoint => 'http://sqs.sa-east-1.amazonaws.com',
    Region   => 'sa-east-1'
  );
  my $queue = $sqs->CreateQueue($self->queue_name . (IS_TEST ? '-test' : ''));
  warn 55;
  return $queue;
}

1;
