package Trackware::Types;
our $ONLY_DIGITY = sub { my ($val) = @_; $val =~ s/[^0-9]//g; $val };

use utf8;
use MooseX::Types -declare => [
  qw(
    DataStr
    TimeStr
    RenavamStr
    PositiveInt
    CNPJ
    )
];
use MooseX::Types::Moose qw(ArrayRef HashRef CodeRef Str ScalarRef Int);
use Moose::Util::TypeConstraints;
use Business::BR::CNPJ qw(test_cnpj);
use DateTime::Format::Pg;
use Trackware::Data::Renavam;

subtype DataStr, as Str, where {
  eval { DateTime::Format::Pg->parse_datetime($_)->datetime };
  return $@ eq '';
}, message { "$_ data invalida" };

coerce DataStr, from Str, via {
  DateTime::Format::Pg->parse_datetime($_)->datetime;
};

subtype TimeStr, as Str, where {
  eval { DateTime::Format::Pg->parse_time($_)->hms };
  return $@ eq '';
}, message { "$_ data invalida" };

coerce TimeStr, from Str, via {
  DateTime::Format::Pg->parse_time($_)->hms;
};

subtype RenavamStr, as Str, where {
  eval { Trackware::Data::Renavam::new $_ };
  return $@ eq '';
}, message { "$_ documento inválido   " };

coerce RenavamStr, from Str, via {
  Trackware::Data::Renavam->new($_);
  return $_;
};

subtype PositiveInt, as Int, where { $_ > 0 },
  message { "$_ número inválido" };

subtype CNPJ, as Str, where { test_cnpj($_) };
1;
