package Trackware::S3Client;
use Moose;
use Config::General;
use Net::Amazon::S3;

has s3 => (is => 'ro', lazy => 1, builder => '_connect_s3');

my $app_dir = $ENV{TRACKWARE_APP_DIR} || '/www/aware/trackware';

my $conf   = Config::General->new("$app_dir/api/trackware_local.conf");
my %config = $conf->getall;

if (!$config{aws}) {
  $conf   = Config::General->new("$app_dir/api/trackware.conf");
  %config = $conf->getall;
}

die "Can't find aws configuration" unless $config{aws};

sub _connect_s3 {
  my $self = shift;

  my @credentials = (
    aws_access_key_id     => $config{aws}{access_key_id},
    aws_secret_access_key => $config{aws}{secret_access_key},
    retry                 => 1,
    secure                => 0,
  );

  return Net::Amazon::S3->new(@credentials);
}

1;
