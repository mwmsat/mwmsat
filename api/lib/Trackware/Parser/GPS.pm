package Trackware::Parser::GPS;

use utf8;
use strict;
use warnings;

use Exporter qw(import);
our @EXPORT_OK = qw(parse);

use Pegex::Grammar;
use Pegex::Tree::Wrap;
use Pegex::Parser;
use Pegex::Input;
use Geo::Coordinates::Transform;

my $geo = Geo::Coordinates::Transform->new;

my $grammar = Pegex::Grammar->new(text => join('', <DATA>));

{

  package Lambda::Parser::GPS::Receiver;
  use base 'Pegex::Tree::Wrap';

  sub got_single_cell {
    my ($self, $cell) = @_;
    {cells => [$cell]};
  }

  sub got_cell {
    my ($self, $cell) = @_;
    return +{@$cell};
  }

  sub got_lac {
    return (lac => hex $_[1]);
  }

  sub got_cid {
    return (cid => hex $_[1]);
  }

  sub got_signal {
    return (signal => $_[1]);
  }

  sub got_body {
    my ($self, $rest) = @_;
    return +{map { %$_ } @$rest};

  }

  sub got_message {
    my ($self, $rest) = @_;
    my ($type, $body) = %$rest;
    return {type => $type, body => +{map { %$_ } @$body}};
  }

  sub got_location {
    my ($self, $rest) = @_;
    my %location = map { %$_ } @$rest;
    my $lat      = $location{lat} =~ s/^(\d{2})/$1 /r;
    my $lng      = $location{lng} =~ s/^(\d{3})/$1 /r;

    ($lat, $lng) = @{(eval { $geo->cnv_to_dd([$lat, $lng]) }) || []};
    $lat *= -1 if $location{n_s} && $location{n_s} eq 'S';
    $lng *= -1 if $location{w_e} && $location{w_e} eq 'W';
    return {location => {lat => $lat + 0, lng => $lng + 0}};
  }

}
my $receiver = Lambda::Parser::GPS::Receiver->new;
my $parser =
  Pegex::Parser->new(grammar => $grammar, receiver => $receiver, debug => 0,);

sub parse {
  my ($input) = @_;
  $input =~ s/[^\w\.,\?]+//g;
  return $parser->parse(Pegex::Input->new(string => $input));
}

__DATA__

%grammar Silentrack
%version 0.0.2

message: A | S | E | O | W | J | PS | RPS | RGPRS 


body:  <imei> COMMA
   <iccid> COMMA
   <battery> COMMA
   <fw_version> COMMA
   <cycle> COMMA
   <tx> COMMA
   <gprs> COMMA
   <gps> COMMA
   <r1> COMMA
   (<cells> | <location> | <single_cell>) COMMA
   <timing_advance> COMMA
   <mcc>? COMMA
   <mnc>? COMMA
   <network> COMMA
   <battery_cycle_count> COMMA
   <message_count>

A: 'A' interval COMMA body
S: 'S' interval COMMA body
E: 'E' interval COMMA body 
O: 'O' interval COMMA body
W: 'W' interval COMMA body
J: 'J' interval COMMA body
  
PS: 'PS' COMMA
  imei COMMA
  interval COMMA
  cycle_on COMMA
  cycle_off COMMA
  emergency_time COMMA
  cycle COMMA
  tx COMMA
  gprs COMMA
  gps COMMA
  r1
  
RPS: 'RPS' COMMA
  imei COMMA
  iccid COMMA
  interval COMMA
  cycle_on COMMA
  cycle_off COMMA
  emergency_time COMMA
  cycle COMMA
  tx COMMA
  gprs COMMA
  gps COMMA
  r1

RGPRS:
  'RGPRS' COMMA
  imei COMMA
  iccid COMMA
  gprs_url COMMA
  port COMMA
  ota_url COMMA
  shortcode
  
imei: /( DIGIT{4} )/

iccid: /( DIGIT{20} | DIGIT{4} ) /

battery:/( DIGIT+ )/

fw_version: /( [^,]+)/

cycle: /( '0'|'1'|'2' )/

cycle_on: /( DIGIT+ )/

cycle_off: /( DIGIT+ )/  

emergency_time: /( DIGIT+ )/

tx : /( '0'|'1'|'2' )/

gprs : /( '0'|'1')/

gps : /( '0'|'1' )/

r1: /( DIGIT+ )/

interval: /( DIGIT+ )/

mcc : /( DIGIT+ )/

mnc: /(  DIGIT+ )/

timing_advance: /( DIGIT+ | '?' | '')/

network: /( '2'|'3' )/

temperature: /( DASH? DIGIT+ DOT DIGIT+ )/

message_count: /( DIGIT+ )/

battery_cycle_count: /( DIGIT+ )/

cell:  cid? COMMA lac? COMMA signal?

single_cell: cell
  
cid: /(
   HEX+ | DIGIT+
)/

lac:  /(
   HEX+ | DIGIT+
)/

signal: /(
   HEX+ | DIGIT+
)/

lat: /( DASH? DIGIT+ DOT DIGIT+ )/
lng: /( DASH? DIGIT+ DOT DIGIT+ )/
speed: /( DIGIT+ DOT DIGIT+ )/

n_s: /('N'|'S'| '')/
w_e: /('W'|'E'| '')/ 

cells: cell COMMA cell COMMA cell COMMA cell COMMA cell COMMA cell

url: /( [^,]+)/
port: /( DIGIT+ )/

gprs_url: url  
ota_url: url 
shortcode:   /( \+? DIGIT+ )/
location: lat COMMA n_s? COMMA lng COMMA w_e? COMMA speed
