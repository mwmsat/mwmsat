package Trackware::Model::EmailQueue;

use base 'Catalyst::Model';
use Trackware::EmailQueue;
use Moose;

our $AUTOLOAD;

has 'emailinstance' => (is => 'rw', isa => 'Trackware::EmailQueue',);

sub initialize_after_setup {
  my ($self, $app) = @_;

  $self->emailinstance(
    Trackware::EmailQueue->new(schema => $app->model('DB')->schema));
}

sub AUTOLOAD {
  my $self = shift;
  my $name = $AUTOLOAD;
  $name =~ s/.*://;
  $self->emailinstance->$name(@_);
}

1;
