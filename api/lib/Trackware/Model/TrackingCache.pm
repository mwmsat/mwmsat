package Trackware::Model::TrackingCache;

use base 'Catalyst::Model';
use Trackware::TrackingManager::Cache;
use Moose;

our $AUTOLOAD;

has 'cacheinstance' =>
  (is => 'rw', isa => 'Trackware::TrackingManager::Cache',);

sub initialize_after_setup {
  my ($self, $app) = @_;

  $self->cacheinstance(Trackware::TrackingManager::Cache->new());
}

sub AUTOLOAD {
  my $self = shift;
  my $name = $AUTOLOAD;
  $name =~ s/.*://;
  $self->cacheinstance->$name(@_);
}

1;
