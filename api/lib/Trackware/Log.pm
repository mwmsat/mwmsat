package Trackware::Log;

require Exporter;
@ISA    = qw(Exporter);
@EXPORT = qw(get_logger);

use Log::Log4perl qw(:easy);

Log::Log4perl->easy_init(
  {level => $DEBUG, layout => '[%P] %d %p> %F{1}:%L %M %m%n'});

# sub get_logger { Log::Log4perl->get_logger(@_) }

1
