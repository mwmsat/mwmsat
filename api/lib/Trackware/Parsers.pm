package Trackware::Parsers;

use strict;
use warnings;

use Exporter 'import';
our @EXPORT_OK = qw($_lbs $_gprs);
use Data::ParseBinary;
use Geo::Coordinates::DecimalDegrees;

our $_lbs = Struct(
  'LBS' => (
    Magic('LBS'),
    String('__imei', 8),
    Value(
      'imei',
      sub {
        join '', map { hex } unpack q{H*}, $_->ctx->{__imei};
      }
    ),
    UBInt16('counter'),
    UBInt16('mcc'),
    UBInt16('mnc'),
    UBInt16('lac'),
    UBInt16('cid'),
    UBInt16('rxl'),
  )
);

our $_gprs = Struct(
  'GPRS' => (
    Magic('SC'),
    Byte("__reserved1"),
    String('__imei', 8),
    Value(
      'imei',
      sub {
        join '', map { hex } unpack q{H*}, $_->ctx->{__imei};
      }
    ),
    String('__pcb_version', 1),
    Value(
      'pcb_version',
      sub {
        my $v = unpack q{H*}, $_->ctx->{__pcb_version};
        $v =~ s/(\w)(\w)/$1.$2/;
        "v$v";
      }
    ),
    String('__firmware_version', 1),
    Value(
      'firmware_version',
      sub {
        my $v = unpack q{H*}, $_->ctx->{__firmware_version};
        $v =~ s/(\w)(\w)/$1.$2/;
        "v$v";
      }
    ),
    Bytes('__reserved2', 2),
    Bytes('__reserved3', 1),
    UBInt16('counter'),
    Bytes('__reserved4', 2),
    UBInt8('reason_generator'),

    # Enum(
    #   UBInt8('reason_generator'),
    #   "Software reset"                    => 0,
    #   "Reset"                             => 1,
    #   "Tracking"                          => 2,
    #   "Tracking at sleep"                 => 3,
    #   "Ultima posição"                  => 4,
    #   "Pânico ativo"                     => 5,
    #   "Pânico desativa"                  => 6,
    #   "Em movimento"                      => 7,
    #   "Parou"                             => 8,
    #   "Detecção de batida"              => 9,
    #   "Entrada 1 desativa"                => 10,
    #   "Entrada 2 ativa"                   => 11,
    #   "Entrada 2 desativa"                => 12,
    #   "Entrada 3 ativa"                   => 13,
    #   "Entrada 3 desativa"                => 14,
    #   "Falha na alimentação"            => 15,
    #   "Alimentação externa normalizada" => 16,
    #   "Falha na bateria backup"           => 17,
    #   "Bateria backup normalizada"        => 18,
    #   "Inicio detecção de Jammer"       => 19,
    #   "Fim detecção de Jammer"          => 20,
    #   "Velocidade máxima excedida"       => 21,
    #   "Velocidade máxima normalizada"    => 22,
    # ),
    UBInt32('__timestamp'),
    Value(timestamp => sub { scalar localtime $_->ctx->{__timestamp} }),
    BFloat32('__latitude'),
    BFloat32('__longitude'),
    String('satellite', 1),

  )
);
1;

