package Trackware::Supplier::Telit;

use Moose::Role;
with 'Trackware::Role::Supplier';

use namespace::autoclean;

use DDP;
use FindBin qw($Bin);
use Data::Printer;
use JSON qw(encode_json decode_json);
use Furl;
use IO::Socket::SSL;
use Try::Tiny::Retry ':all';

my $ua = Furl->new;

has recipient => (is => 'ro', isa => 'Str');
has message   => (is => 'ro', isa => 'Str');
has username =>
  (is => 'ro', isa => 'Str', lazy => 1, builder => '_build_username');
has password =>
  (is => 'ro', isa => 'Str', lazy => 1, builder => '_build_password');

has session_key => (
  is      => 'ro',
  isa     => 'Str',
  lazy    => 1,
  builder => '_build_session_key',
  clearer => '_clear_session_key'
);

sub api_url { }

sub _build_session_key {
  my $self = shift;
  my $res  = $ua->post(
    'https://api.devicewise.com/api',
    [Content_Type => 'application/json'],
    encode_json(
      {
        "auth" => {
          "command" => "api.authenticate",
          "params" =>
            {"username" => $self->username, "password" => $self->password}
        }
      }
    )
  );
  die 'Error fetching Telit sessionId: '
    . $res->status_line . ' => '
    . $res->content
    unless $res->is_success;
  my $json = decode_json($res->decoded_content);
  p($json);
  return $json->{auth}->{params}->{sessionId};
}

sub _build_username { return ($ENV{TELIT_USER} || die 'missing TELIT_USER') }
sub _build_password { return ($ENV{TELIT_PASS} || die 'missing TELIT_PASS') }

sub send_sms {
  my ($self, $chip_number, $message, $uuid) = @_;
  my $res = retry {
    my $res = $ua->post(
      'https://api.devicewise.com/api',
      [Content_Type => 'application/json', sessionId => $self->session_key],
      encode_json(
        {
          "1" => {
            "command" => "sms.send",
            "params"  => {
              "msisdn"  => $chip_number,
              "message" => $message,
              "coding"  => "SEVEN_BIT",
            }
          }
        }
      )
    );
    die $res unless $res->is_success;
    $res;
  }
  retry_if { $_->code == 401 && shift() < 3; }
  on_retry { $self->_clear_session_key }
  catch {
    die 'Error sending command via Telit: '
      . $_->status_line . ' => '
      . $_->content
      if ref $_ && $_->can('content');
    die $_;
  };

  my $json = decode_json($res->decoded_content);
  p($json);
  die 'Telit: ' . join('; ', @{$json->{1}->{errorMessages}})
    if !$json->{1}->{success} && $json->{1}->{errorMessages};

  return 1;
}

1;
