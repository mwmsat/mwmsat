package Trackware::Supplier::Vivo;

use 5.014;
use Moose::Role;
with 'Trackware::Role::Supplier';

use Furl;
use URI;
use HTTP::Request;

use URI::Escape qw(uri_escape);
use JSON qw(encode_json decode_json);
use Mozilla::CA;
use Try::Tiny::Retry ':all';
use IO::Socket::SSL;

my $ssl_cache = IO::Socket::SSL::Session_Cache->new(10);
my $context   = IO::Socket::SSL::SSL_Context->new(
  SSL_verify_mode   => 0,
  SSL_session_cache => $ssl_cache
);
IO::Socket::SSL::set_default_context($context);
IO::Socket::SSL::set_default_session_cache($ssl_cache);

my $ua = Furl->new(

  #  proxy => 'socks://127.0.0.1:8123',
  ssl_opts => {SSL_verify_mode => 0, SSL_version => q{tlsv1}},
);

my $sms_api_endpoint =
'https://oneapi.torpedoempresas.vivo.com.br:443/oneapi/1/smsmessaging/outbound/tel:53011/requests';

sub send_sms {
  my ($self, $chip_number, $message, $uuid) = @_;

  my $response;
  retry {
    $response =
      $ua->request($self->_build_request($chip_number, $message, $uuid));
    die unless $response->code == 201;
  }
  retry_if { shift() < 4 };


  if ($response->code == 201) {
    my $receipt = decode_json($response->content);
    return $receipt->{resourceReference}{resourceURL};
  }

  return $response;

}

sub _build_request {
  my ($self, $phone_number, $message, $uuid) = @_;
  my $r = HTTP::Request->new(
    POST => $sms_api_endpoint,
    [
      Content_Type => 'application/json;charset=UTF-8',

    ],

    encode_json(
      {
        "outboundRequest" => {
          "address"                => ["tel:$phone_number"],
          "senderAddress"          => "tel:53011",
          "outboundSMSTextMessage" => {"message" => $message},

          receiptRequest => {
            notifyURL    => 'http://bmbpd6vvge.underdc.com.br/vivo/status',
            callbackData => $uuid
          }
        }
      }
    )
  );

  $r->headers->authorization_basic($ENV{TRACKWARE_VIVO_USER},
    $ENV{TRACKWARE_VIVO_PASS});

  use DDP;
  p($r);
  warn $r->as_string;
  return $r;
}

1;
