package Trackware::Supplier::Infobip;
use namespace::autoclean;
use Moose::Role;
use HTTP::Request;
use URI;

has api_url => (
  is      => 'ro',
  lazy    => 1,
  default => 'http://api.infobip.com/api/v3/sendsms/plain'
);

has api_url_secondary => (
  is      => 'ro',
  lazy    => 1,
  default => 'http://api2.infobip.com/api/v3/sendsms/json'
);

has mobile_phone => (is => 'ro', isa => 'Str');

has message => (is => 'ro', isa => 'Str');

has message_id => (is => 'ro', isa => 'Int');

has user => (is => 'ro', isa => 'Str', default => 'nsms0000');

has password => (is => 'ro', isa => 'Str', default => 'Nsms0001');

has sender => (
  is      => 'ro',
  isa     => 'Str',
  default => '+56964590003',

  #     default => '27185'     #short code brasileiro, apenas para testes
);

sub prepare {
  my $self = shift;

  my $uri = URI->new($self->api_url);

  $uri->query_form(
    SMSText  => $self->message,
    GSM      => $self->mobile_phone,
    user     => $self->user,
    password => $self->password,
    sender   => $self->sender
  );

  my $req = HTTP::Request->new('GET', $uri);

  return $req;
}

1;
