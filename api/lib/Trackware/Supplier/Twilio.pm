package Trackware::Supplier::Twilio;

use 5.014;
use DDP;
use Moose::Role;
with 'Trackware::Role::Supplier';
use WWW::Twilio::API;
use Mozilla::CA;
use XML::Simple qw(XMLin);

use IO::Socket::SSL;

my $ssl_cache = IO::Socket::SSL::Session_Cache->new(10);
my $context   = IO::Socket::SSL::SSL_Context->new(
  SSL_verify_mode   => 0,
  SSL_session_cache => $ssl_cache
);
IO::Socket::SSL::set_default_context($context);
IO::Socket::SSL::set_default_session_cache($ssl_cache);

my $twilio = WWW::Twilio::API->new(
  AccountSid => 'ACe42753bd75f1f4d12ee510bc3b1c7936',
  AuthToken  => '446c31d0ecc91af0cc226f33dcf973ab'
);

sub send_sms {
  my ($self, $chip_number, $message, $uuid) = @_;

  my $response = $twilio->POST(
    'SMS/Messages',
    From           => '+13342923635',
    To             => $chip_number,
    Body           => $message,
#    StatusCallback => "https://e5c083f1.ngrok.io?uuid=$uuid"
  );

  if ($response->{code} == 201) {
    my $receipt = XMLin($response->{content});
    return $receipt->{SMSMessage}->{Uri};
  }

  return $response;

}

1;
