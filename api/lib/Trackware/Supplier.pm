package Trackware::Supplier;
use namespace::autoclean;
use Moose;
use Furl;

with 'MooseX::Traits';

use JSON qw(encode_json);

has network_supplier => (is => 'ro', isa => 'Str',);

has '+_trait_namespace' => (default => 'Trackware::Supplier');

sub send {
  my $self = shift;

  return $self->send_sms(@_);
}

1;
