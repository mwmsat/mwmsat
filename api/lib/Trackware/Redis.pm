package Trackware::Redis;
use Moose;
use Redis;
use Trackware::ConfigFindLocal;
use Config::General;

has queue_key => (is => 'rw', isa => 'Str', default => 'email');

has redis => (is => 'ro', isa => 'Redis', lazy => 1, builder => '_build_redis');

my $app_dir = $ENV{TRACKWARE_APP_DIR} || '/www/aware/trackware';

my $conf   = Config::General->new("$app_dir/api/trackware_local.conf");
my %config = $conf->getall;

if (!$config{redis}{host}) {
  $conf   = Config::General->new("$app_dir/api/trackware.conf");
  %config = $conf->getall;
}

die "Can't find redis configuration" unless $config{redis}{host};

sub _build_redis {
  my $self = shift;
  use DDP;
  my $q = $self->queue_key;

  my @a = (
    reconnect => 60,
    every     => 5000,
    server    => $config{redis}{host},
    name      => $self->queue_key ? $self->queue_key : $config{redis}{queue_key}
  );

  return Redis->new(@a);
}

1;
