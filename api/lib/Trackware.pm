package Trackware;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;
use open qw(:std :utf8);
use Trackware::Log;

# Set flags and add plugins for the application.
#
# Note that ORDERING IS IMPORTANT here as plugins are initialized in order,
# therefore you almost certainly want to keep ConfigLoader at the head of the
# list if you're using it.
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
  ConfigLoader
  Static::Simple

  Authentication
  Authorization::Roles

  +CatalystX::Plugin::Logx

  /;

extends 'Catalyst';

our $VERSION = '0.01';

# Configure the application.
#
# Note that settings in pi_pcs.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
  name         => 'Trackware',
  encoding     => 'UTF-8',
  private_path => '../etc/uploads',

  # Disable deprecated behavior needed by old applications
  disable_component_resolution_regex_fallback => 1,
  enable_catalyst_header                      => 1,    # Send X-Catalyst header

);

after 'setup_components' => sub {
  my $app = shift;
  for (keys %{$app->components}) {
    if ($app->components->{$_}->can('initialize_after_setup')) {
      $app->components->{$_}->initialize_after_setup($app);
    }
  }

};

after setup_finalize => sub {
  my $app = shift;

  for ($app->registered_plugins) {
    if ($_->can('initialize_after_setup')) {
      $_->initialize_after_setup($app);
    }
  }
};

__PACKAGE__->log(get_logger());

# Start the application
__PACKAGE__->setup();

=head1 NAME

Trackware - Catalyst based application

=head1 SYNOPSIS

    script/pi_pcs_server.pl

=head1 SEE ALSO

L<Trackware::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Renato CRON

=head1 LICENSE

This library is private software.

=cut


1;
