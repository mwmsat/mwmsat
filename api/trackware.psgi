use strict;
use warnings;
use lib 'lib';
use Trackware;

my $app = Trackware->apply_default_middlewares(Trackware->psgi_app);
$app;

