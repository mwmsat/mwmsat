use utf8;
use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {
	
	#criar novo endereço
	rest_post '/addresses',
		name  => 'criar novo endereço',
		list  => 1,
		stash => 'address',
		[
			address      => 'Av. Paulista',
			number       => '568',
			neighborhood => 'Bela Vista',
			postal_code  => '01310000',
			complement   => 'bloco c',
			city_id      => 1
		];
	
	rest_post '/business_units',
		name  => 'criar nova filial',
		list  => 1,
		stash => 'business_unity',
		[
			corporate_name 	=> 'Teste ltda.',
			fancy_name     	=> 'Teste',
			register       	=> '75789517000100',
			address_id     	=> stash 'address.id',
			is_active      	=> 1,
			website			=> 'teste-123456.com.br',
			domain			=> 'teste-123456',
			email          	=> 'teste@teste.com.br',
			phone          	=> '11111111111',
			phone_2        	=> '11111111112'
		];
	
    #criar nova marca
    rest_post '/vehicle_brands',
      name  => 'criar marca de veículo',
      list  => 1,
      stash => 'vehicle_brand',
      [ brand => 'renault', ];

    #criar novo modelo de veiculo
    rest_post '/vehicle_models',
      name  => 'criar modelo de veículo',
      list  => 1,
      stash => 'vehicle_model',
      [
        model            => 'Clio',
        vehicle_brand_id => stash 'vehicle_brand.id'
      ];

    #criar nova cor de veiculo
    rest_post '/vehicle_colors',
      name  => 'criar cor de veículo',
      list  => 1,
      stash => 'vehicle_color',
      [ color => 'Preto', ];

    #criar novo veiculo
    rest_post '/vehicles',
      name  => 'criar veículos',
      list  => 1,
      stash => 'vehicle',
      [
        car_plate             	=> 'lpi2609',
        doors_number          	=> '5',
        manufacture_year      	=> '2009',
        vehicle_model_id      	=> stash 'vehicle_model.id',
        model_year            	=> '2009',
        km                    	=> 41000,
        vehicle_color_id      	=> stash 'vehicle_color.id',
        fuel_type_id          	=> 1,
        observations          	=> 'teste',
        driver_id             	=> stash 'driver.id',
        vehicle_owner_id      	=> stash 'vehicle_owner.id',
        state_id              	=> 1,
        city_id               	=> 1,
        business_unity_id		=> stash 'business_unity.id'
      ];

    stash_test 'vehicle.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'vehicle.id', 'get has the same id!' );
    };

    stash_test 'vehicle.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{vehicles}, 'vehicle list exists' );

        is( @$me, 1, '1 vehicle' );

        $me = [ sort { $a->{id} cmp $b->{id} } @$me ];

        is( $me->[0]{vehicle_model}{id}, stash 'vehicle_model.id', 'listing ok' );
    };
    
    rest_put stash 'vehicle.url',
      name => 'atualizar veiculo',
      [
        car_plate             => 'BUA2609',
        doors_number          => '5',
        manufacture_year      => '1995',
        vehicle_model_id      => stash 'vehicle_model.id',
        model_year            => '1996',
        vehicle_brand_id      => stash 'vehicle_brand.id',
        vehicle_body_style_id => stash 'vehicle_body_style_id.id',
        km                    => 70000,
        vehicle_color_id      => stash 'vehicle_color.id',
        fuel_type_id          => 1,
        city_id               => 1
      ];

    rest_reload 'vehicle';

    stash_test 'vehicle.get', sub {
        my ($me) = @_;

        is( $me->{km}, 70000, 'car km updated!' );
    };

    rest_delete stash 'vehicle.url';

    rest_reload 'vehicle', 404;

    rest_reload_list 'vehicle';

    stash_test 'vehicle.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{vehicles}, 'vehicle list exists' );

        is( @$me, 0, '0 vehicles' );
    };

};

done_testing;