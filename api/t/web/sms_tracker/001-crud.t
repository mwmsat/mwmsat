use strict;
use warnings;
use utf8;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {
	
	#criar novo rastreador
	rest_post '/trackers',
		name  => 'criar rastreador de veiculos',
		list  => 1,
		stash => 'tracker',
		[
			imei				=> '123456789123456',
			iccid				=> '1234567891478523698',
			phone_number		=> '11969258780',
			tracker_status_id	=> 1
		];
	
	rest_post '/network_suppliers',
		name  => 'criar nova operadora de rede',
		list  => 1,
		stash => 'network_supplier',
		[ name => 'Vivo' ];
	
	rest_post '/sms_trackers',
		name  => 'criar nova mensagem',
		list  => 1,
		stash => 'sms_tracker',
		[
			message 			=> 'Mensagem de teste',
			tracker_id			=> stash 'tracker.id',
			network_supplier_id	=> stash 'network_supplier.id',
			sms_status_id 		=> 1,
		];

    stash_test 'sms_tracker.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'sms_tracker.id', 'get has the same id!' );
    };

    stash_test 'sms_tracker.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{sms_trackers}, 'sms_tracker list exists' );
		
        is( @$me, 1, '1 sms_tracker' );

		is( $me->[0]{message}, 'Mensagem de teste', 'listing ok' );
    };

    rest_put stash 'sms_tracker.url',
		name => 'atualizar mensagem',
		[ sms_status_id => 2 ];

    rest_reload 'sms_tracker';

    stash_test 'sms_tracker.get', sub {
        my ($me) = @_;

        is( $me->{sms_status}{id}, 2, 'status updated!' );
    };

    rest_delete stash 'sms_tracker.url';

    rest_reload 'sms_tracker',404;

    rest_reload_list 'sms_tracker';

    stash_test 'sms_tracker.list', sub {
        my ($me) = @_;
		
        ok( $me = delete $me->{sms_trackers}, 'sms_tracker list exists' );
		
		is( @$me, 0, '0 sms_trackers' );
    };

};

done_testing;