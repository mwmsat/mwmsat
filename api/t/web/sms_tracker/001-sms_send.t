use utf8;
use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {
    #criar novo rastreador
	rest_post '/trackers',
		name  => 'criar rastreador de veiculos',
		list  => 1,
		stash => 'tracker',
		[
			imei				=> '123456789123456',
			iccid				=> '1234567891478523698',
			phone_number		=> '11969258780',
			tracker_status_id	=> 1
		];
	
	rest_post '/network_suppliers',
		name  => 'criar nova operadora de rede',
		list  => 1,
		stash => 'network_supplier',
		[ name => 'Vivo' ];
	
	rest_post '/sms_trackers',
		name  => 'criar nova mensagem',
		list  => 1,
		stash => 'sms_tracker',
		[
			message 			=> 'Mensagem de teste',
			tracker_id			=> stash 'tracker.id',
			network_supplier_id	=> stash 'network_supplier.id',
			sms_status_id 		=> 1,
		];
		
	rest_post '/sms_trackers/sender',
		name  => 'enviar mensagem',
		list  => 1,
		stash => 'sms_tracker_sender',
		[
			message 			=> 'Mensagem de teste',
			tracker_id			=> stash 'tracker.id',
			network_supplier_id	=> stash 'network_supplier.id',
			sms_status_id 		=> 1,
		];

# 	stash_test 'sms_tracker_sender.get', sub {
#         my ($me) = @_;
# 
#         is( $me->{id}, stash 'tracker.id', 'get has the same id!' );
#     };
   
};

done_testing;