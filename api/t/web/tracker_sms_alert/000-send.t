use strict;
use warnings;
use utf8;
use DateTime;
use FindBin qw($Bin);
use lib "$Bin/../../lib";
use Config::General;
use Trackware::Schema;
use Trackware::Test::Further;
use_ok 'Trackware::TrackingManager';

my $schema       = Trackware->model('DB')->schema;
my $tm           = new_ok 'Trackware::TrackingManager', [schema => $schema];
my $tracker_info = {
  'type' => 'W',
  phone  => '1111111111111',
  text =>
    'W60,8301,9947,375,2.F,2,0,0,0,0,01ED,24C3,85,0BBB,24C3,82,2AA1,2397,83,0BBA,24C3,85,0BE2,2397,91,26EA,2397,90,4,724,10,2,0,2895',
  position => {
    'lat' => '-23.622827',
    'address' =>
      "Rua Doutor Luiz Migliano, Jardim Parque Morumbi, Vila Andrade, S\x{e3}o Paulo, Microrregi\x{e3}o de S\x{e3}o Paulo, RMSP, Mesorregi\x{e3}o Metropolitana de S\x{e3}o Paulo, S\x{e3}o Paulo, Southeast Region, 05749260, Brazil",
    'accuracy'       => 1036,
    'balance'        => 10820,
    'address_detail' => {
      'country_code' => 'BR',
      'district'     => "Mesorregi\x{e3}o Metropolitana de S\x{e3}o Paulo",
      'country'      => 'Brazil',
      'county'       => "Microrregi\x{e3}o de S\x{e3}o Paulo",
      'postal_code'  => '05749260',
      'city'         => "S\x{e3}o Paulo",
      'state'        => "S\x{e3}o Paulo",
      'road'         => 'Rua Doutor Luiz Migliano'
    },
    'lon'    => '-46.742178',
    'status' => 'ok'
  },
  supplier => 'Vivo',
  datetime => DateTime->now->iso8601,
  message  => {
    'body' => {
      'mcc'            => '724',
      'imei'           => '8301',
      'gps'            => '0',
      'timing_advance' => '4',
      'network'        => '2',
      'iccid'          => '9947',
      'mnc'            => '10',
      'cells'          => [
        {'lac' => 9411, 'signal' => '85',  'cid'    => 493},
        {'lac' => 9411, 'cid'    => 3003,  'signal' => '82'},
        {'lac' => 9111, 'cid'    => 10913, 'signal' => '83'},
        {'lac' => 9411, 'cid'    => 3002,  'signal' => '85'},
        {'lac' => 9111, 'cid'    => 3042,  'signal' => '91'},
        {'lac' => 9111, 'signal' => '90',  'cid'    => 9962}
      ],
      'gprs'                => '0',
      'interval'            => '60',
      'cycle'               => '2',
      'battery_cycle_count' => '0',
      'r1'                  => '0',
      'tx'                  => '0',
      'battery'             => '375',
      'message_count'       => '2895',
      'fw_version'          => '2.F',
    }
  }
};

db_transaction {

  use Trackware::Util qw(extract_message_data);
  ok(my $position = $tm->add_gprs_lbs(extract_message_data($tracker_info)),
    'position ok');

  my $tracker = $position->tracker;
  $tracker->add_to_tracker_sms_alerts(
    {
      has_address               => 1,
      has_gps_location          => 1,
      has_battery_level         => 1,
      destination_phone_numbers => '9988886666666;998888444444444'
    }
  );

  ok($tm->add_gprs_lbs(extract_message_data($tracker_info)), 'position ok');
};

done_testing;
