use utf8;
use FindBin qw($Bin);
use lib "$Bin/../../lib";
use DateTime;

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {
#     criar novo rastreador
    rest_post '/trackers',
	name  => 'criar rastreador de veiculos',
	list  => 1,
	stash => 'tracker',
	[
		imei				=> '123456789123456',
		iccid				=> '1234567891478523698',
		phone_number		=> '11969258780',
		tracker_status_id	=> 1
	];
	  
# 	criar novo layout de comando
	rest_post '/tracker_command_layouts',
	name  => 'criar layout de comando para rastreador',
	list  => 1,
	stash => 'tracker_command_layout',
	[
		name		=> 'Posição avulsa',
		command		=> 'C00',
		is_active	=> 1,
	];
	
# 	criar novo layout de comando
	rest_post '/tracker_command_queues',
	name  => 'criar layout de comando para rastreador',
	list  => 1,
	stash => 'tracker_command_queue',
	[
		tracker_command_layout_id 	=> stash 'tracker_command_layout.id',
		tracker_id					=> stash 'tracker.id',
	];

    stash_test 'tracker_command_queue.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'tracker_command_queue.id', 'get has the same id!' );
    };

    stash_test 'tracker_command_queue.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{tracker_command_queues}, 'tracker_command_queues list exists' );

        is( @$me, 1, '1 tracker_command_queue' );

        $me = [ sort { $a->{id} cmp $b->{id} } @$me ];

		is( $me->[0]{tracker_command_layout}{id}, stash 'tracker_command_layout.id', 'listing ok' );
    };
	
    rest_put stash 'tracker_command_queue.url',
		name => 'atualizar fila de comandos',
		[ 
			response 	=> 'accepted',
			updated_at 	=> DateTime->now()
		];

    rest_reload 'tracker_command_queue';

    stash_test 'tracker_command_queue.get', sub {
        my ($me) = @_;

        is( $me->{id},   stash 'tracker_command_queue.id', 'get has the same id!' );
		is( $me->{response}, 'accepted', 'queue updated' );
    };

    rest_delete stash 'tracker_command_queue.url';

    rest_reload 'tracker_command_queue', 404;

    rest_reload_list 'tracker_command_queue';

    stash_test 'tracker_command_queue.list', sub {
        my ($me) = @_;
        ok( $me = delete $me->{tracker_command_queues}, 'tracker_command_queue list exists' );

        is( @$me, 0, '0 tracker_command_queues' );
    };
};

done_testing;