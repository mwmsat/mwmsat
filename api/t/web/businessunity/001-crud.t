use strict;
use warnings;
use utf8;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {

    #criar novo endereço
    rest_post '/addresses',
      name  => 'criar novo endereço',
      list  => 1,
      stash => 'address',
      [
        address      => 'Av. Paulista',
        number       => '568',
        neighborhood => 'Bela Vista',
        postal_code  => '01310000',
        complement   => 'bloco c',
        city_id      => 1
      ];

    rest_post '/business_units',
      name  => 'criar nova filial',
      list  => 1,
      stash => 'business_unity',
      [
        corporate_name 	=> 'Teste ltda.',
        fancy_name     	=> 'Teste',
        register       	=> '75789517000100',
        address_id     	=> stash 'address.id',
        is_active      	=> 1,
		website			=> 'teste-123456.com.br',
		domain			=> 'teste-123456',
        email          	=> 'teste@teste.com.br',
        phone          	=> '11111111111',
        phone_2        	=> '11111111112'
      ];

    stash_test 'business_unity.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'business_unity.id', 'get has the same id!' );
    };

    stash_test 'business_unity.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{business_units}, 'business_unity list exists' );
        is( @$me, 1, '1 business_unity' );

        is( $me->[0]{corporate_name}, 'Teste ltda.', 'listing ok' );
    };

    rest_put stash 'business_unity.url',
      name => 'atualizar filial',
      [ corporate_name => 'Teste S/A' ];

    rest_reload 'business_unity';

    stash_test 'business_unity.get', sub {
        my ($me) = @_;

        is( $me->{corporate_name}, 'Teste S/A', 'corporate name updated!' );
    };

    rest_delete stash 'business_unity.url';

    rest_reload 'business_unity', 404;

    rest_reload_list 'business_unity';

    stash_test 'business_unity.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{business_units}, 'business_unity list exists' );

        is( @$me, 0, '0 business_unity' );
    };

};

done_testing;
