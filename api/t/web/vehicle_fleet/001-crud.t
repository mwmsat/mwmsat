use strict;
use warnings;
use utf8;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {

    #criar novo endereço
    rest_post '/addresses',
      name  => 'criar novo endereço',
      list  => 1,
      stash => 'address',
      [
        address      => 'Av. Paulista',
        number       => '568',
        neighborhood => 'Bela Vista',
        postal_code  => '01310000',
        complement   => 'bloco c',
        city_id      => 1
      ];

    rest_post '/business_units',
      name  => 'criar nova filial',
      list  => 1,
      stash => 'business_unity',
      [
        corporate_name 	=> 'Teste ltda.',
        fancy_name     	=> 'Teste',
        register       	=> '75789517000100',
        address_id     	=> stash 'address.id',
        is_active      	=> 1,
        email          	=> 'teste@teste.com.br',
        phone          	=> '11111111111',
        phone_2        	=> '11111111112',
		website			=> 'teste-123456.com.br',
		domain			=> 'teste-123456'
      ];
	  
	  rest_post '/vehicle_fleets',
		name  => 'criar nova frota',
		list  => 1,
		stash => 'vehicle_fleet',
		[
			name => 'Frota teste',
			business_unity_id => stash 'business_unity.id'
		];

		stash_test 'vehicle_fleet.get', sub {
        my ($me) = @_;

		is( $me->{id}, stash 'vehicle_fleet.id', 'get has the same id!' );
    };

	stash_test 'vehicle_fleet.list', sub {
        my ($me) = @_;

		ok( $me = delete $me->{vehicle_fleets}, 'vehicle_fleet list exists' );
		is( @$me, 1, '1 vehicle_fleet' );

        is( $me->[0]{name}, 'Frota teste', 'listing ok' );
    };

	rest_put stash 'vehicle_fleet.url',
      name => 'atualizar frota',
      [ name => 'Frota teste 2' ];

	rest_reload 'vehicle_fleet';

	stash_test 'vehicle_fleet.get', sub {
        my ($me) = @_;

		is( $me->{name}, 'Frota teste 2', 'name updated!' );
    };

	rest_delete stash 'vehicle_fleet.url';

	rest_reload 'vehicle_fleet', 404;

	rest_reload_list 'vehicle_fleet';

	stash_test 'vehicle_fleet.list', sub {
        my ($me) = @_;

		ok( $me = delete $me->{vehicle_fleets}, 'vehicle_fleet list exists' );

		is( @$me, 0, '0 vehicle_fleet' );
    };

};

done_testing;