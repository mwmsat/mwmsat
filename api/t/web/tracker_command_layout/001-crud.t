use utf8;
use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {
    #criar novo layout de comando
    rest_post '/tracker_command_layouts',
      name  => 'criar layout de comando para rastreador',
      list  => 1,
      stash => 'tracker_command_layout',
      [
		name		=> 'Posição avulsa',
        command		=> 'C00',
		is_active	=> 1,
      ];

	stash_test 'tracker_command_layout.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'tracker_command_layout.id', 'get has the same id!' );
    };

    stash_test 'tracker_command_layout.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{tracker_command_layouts}, 'tracker_command_layouts list exists' );

        is( @$me, 1, '1 tracker_command_layout' );

        $me = [ sort { $a->{id} cmp $b->{id} } @$me ];

		is( $me->[0]{command}, 'C00', 'listing ok' );
    };

    rest_put stash 'tracker_command_layout.url',
		name => 'atualizar rastreador',
		[ 
			name 	=> 'Posição única avulsa',
			command => 'C01'
		];

    rest_reload 'tracker_command_layout';

    stash_test 'tracker_command_layout.get', sub {
        my ($me) = @_;

        is( $me->{id},   stash 'tracker_command_layout.id', 'get has the same id!' );
		is( $me->{command}, 'C01', 'command updated!' );
    };

    rest_delete stash 'tracker_command_layout.url';

    rest_reload 'tracker_command_layout', 404;

    rest_reload_list 'tracker_command_layout';

    stash_test 'tracker_command_layout.list', sub {
        my ($me) = @_;
        ok( $me = delete $me->{tracker_command_layouts}, 'tracker_command_layout list exists' );

        is( @$me, 0, '0 tracker_command_layouts' );
    };
};

done_testing;