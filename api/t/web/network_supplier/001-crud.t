use strict;
use warnings;
use utf8;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {
	
	rest_post '/network_suppliers',
	  name  => 'criar nova operadora de rede',
	  list  => 1,
	  stash => 'network_supplier',
	  [
		name => 'Vivo',
	  ];

    stash_test 'network_supplier.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'network_supplier.id', 'get has the same id!' );
    };

    stash_test 'network_supplier.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{network_suppliers}, 'network_supplier list exists' );
        is( @$me, 1, '1 network_supplier' );

        is( $me->[0]{name}, 'Vivo', 'listing ok' );
    };

    rest_put stash 'network_supplier.url',
	 name => 'atualizar operadora de rede',
	 [ name => 'Claro' ];

    rest_reload 'network_supplier';

    stash_test 'network_supplier.get', sub {
        my ($me) = @_;

        is( $me->{name}, 'Claro', 'name updated!' );
    };

    rest_delete stash 'network_supplier.url';

    rest_reload 'network_supplier',404;

    rest_reload_list 'network_supplier';

    stash_test 'network_supplier.list', sub {
        my ($me) = @_;
		
        ok( $me = delete $me->{network_suppliers}, 'network_supplier list exists' );
		
		is( @$me, 0, '0 network_suppliers' );
    };

};

done_testing;