use strict;
use warnings;
use utf8;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

db_transaction {
  my $schema = shift;

  ok(
    my $tracker
      = $schema->resultset('Tracker')
      ->create({phone_number => '111111111111', tracker_status_id => 1,}),
    'tracker ok'
  );
  ok(
    my $position = $tracker->create_related(
      'tracker_positions',
      {lat => 111, lng => 111, lbs_raw => 'RPS,5637,9863,5,10,50,60,0,0,0,1,0'}
    ),
    'position ok'
  );

  ok(my $notification = $schema->resultset('Notification')
      ->create({message => 'null', rule => '{}'}));
  ok(
    my $tracker_notification = $tracker->create_related(
      'tracker_notifications', {notification => $notification}
    ),
    'notification ok'
  );

  ok(
    my $sms_tracker = $tracker->create_related(
      'sms_trackers', {message => 'PS,5637,5,10,50,60,0,0,0,1,0'}
    ),
    'sms ok'
  );
  ok(
    my $tracker_notification_sms = $tracker_notification->create_related(
      tracker_notification_sms => {
        notification_id => $notification->id,
        tracker         => $tracker,
        sms_tracker     => $sms_tracker
      }
    ),
    'tracker_notification_sms ok'
  );

  ok($tracker_notification_sms->confirm_config($tracker, $position),
    'config confirmed');

  db_transaction {
    $sms_tracker->update({message => 'PS,5637,5,10,40,80,1,1,1,1,1'});
    $sms_tracker->discard_changes;
    $tracker_notification_sms->discard_changes;
    ok(!$tracker_notification_sms->confirm_config($tracker, $position),
      'config not confirmed');
  };

};

done_testing;
