use utf8;
use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {

#	criar nova operadora de telefonia
	rest_post '/network_suppliers',
		name  => 'criar nova operadora de rede',
		list  => 1,
		stash => 'network_supplier',
		[ name => 'Vivo' ];
	
#	criar novo rastreador
    rest_post '/trackers',
		name  => 'criar rastreador de veiculos',
		list  => 1,
		stash => 'tracker',
		[
			imei				=> '123456789123456',
			iccid				=> '1234567891478523698',
			phone_number		=> '11969258780',
			tracker_status_id	=> 1,
			network_supplier_id	=> stash 'network_supplier.id'
		];

    stash_test 'tracker.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'tracker.id', 'get has the same id!' );
    };

    stash_test 'tracker.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{trackers}, 'trackers list exists' );

        is( @$me, 1, '1 tracker' );

        $me = [ sort { $a->{id} cmp $b->{id} } @$me ];

		is( $me->[0]{iccid}, '1234567891478523698', 'listing ok' );
    };

    rest_put stash 'tracker.url',
		name => 'atualizar rastreador',
		[ imei => '123456789456123', ];

    rest_reload 'tracker';

    stash_test 'tracker.get', sub {
        my ($me) = @_;

        is( $me->{id},   stash 'tracker.id', 'get has the same id!' );
		is( $me->{imei}, '123456789456123', 'imei updated!' );
    };

    rest_delete stash 'tracker.url';

    rest_reload 'tracker', 404;

    rest_reload_list 'tracker';

    stash_test 'tracker.list', sub {
        my ($me) = @_;
        ok( $me = delete $me->{trackers}, 'tracker list exists' );

        is( @$me, 0, '0 trackers' );
    };
};

done_testing;