use strict;
use warnings;
use utf8;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {

    #criar novo endereço
    rest_post '/addresses',
      name  => 'criar novo endereço',
      list  => 1,
      stash => 'address',
      [
        address      => 'Av. Paulista',
        number       => '568',
        neighborhood => 'Bela Vista',
        postal_code  => '01310000',
        complement   => 'bloco c',
        city_id      => 1
      ];

    stash_test 'address.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'address.id', 'get has the same id!' );
    };

    stash_test 'address.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{addresses}, 'address list exists' );

        is( @$me, 1, '1 address' );

        is( $me->[0]{address}, 'Av. Paulista', 'listing ok' );
    };

    rest_put stash 'address.url',
      name => 'atualizar endereço',
      [
        address      => 'Av. Paulista',
        number       => '765',
        neighborhood => 'Bela Vista',
        postal_code  => '01310000',
        complement   => 'bloco c',
        city_id      => 1
      ];

    rest_reload 'address';

    stash_test 'address.get', sub {
        my ($me) = @_;

        is( $me->{number}, '765', 'number updated!' );
    };

    rest_delete stash 'address.url';

    rest_reload 'address', 404;

    rest_reload_list 'address';

    stash_test 'address.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{addresses}, 'address list exists' );

        is( @$me, 0, '0 address' );
    };

};

done_testing;
