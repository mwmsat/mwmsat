use strict;
use warnings;
use utf8;
use Digest::SHA1 qw(sha1 sha1_hex sha1_base64);

use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {

    #criar novo endereço
    rest_post '/addresses',
      name  => 'criar novo endereço',
      list  => 1,
      stash => 'address',
      [
        address      => 'Av. Paulista',
        number       => '568',
        neighborhood => 'Bela Vista',
        postal_code  => '01310000',
        complement   => 'bloco c',
        city_id      => 1
      ];

    rest_post '/business_units',
      name  => 'criar nova filial',
      list  => 1,
	  stash => 'business_unity',
      [
        corporate_name 	=> 'Teste ltda.',
        fancy_name     	=> 'Teste',
        register       	=> '75789517000100',
        address_id     	=> stash 'address.id',
        is_active      	=> 1,
		website			=> 'teste-123456.com.br',
		domain			=> 'teste-123456',
        email          	=> 'teste@teste.com.br',
        phone          	=> '11111111111',
        phone_2        	=> '11111111112'
      ];
	  
	rest_post '/customers',
	  name  => 'criar novo cliente',
	  list  => 1,
	  stash => 'customer',
	  [
		corporate_name 		=> 'Cliente ltda.',
		fancy_name     		=> 'Cliente',
		address_id     		=> stash 'address.id',
		business_unity_id	=> stash 'business_unity.id',
		is_active      		=> 1,
	  ];

    stash_test 'customer.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'customer.id', 'get has the same id!' );
    };

    stash_test 'customer.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{customers}, 'customer list exists' );
        is( @$me, 1, '1 customer' );

        is( $me->[0]{corporate_name}, 'Cliente ltda.', 'listing ok' );
    };

    rest_put stash 'customer.url',
      name => 'atualizar filial',
      [ corporate_name => 'Cliente S/A' ];

    rest_reload 'customer';

    stash_test 'customer.get', sub {
        my ($me) = @_;

        is( $me->{corporate_name}, 'Cliente S/A', 'corporate name updated!' );
    };

    rest_delete stash 'customer.url';

    rest_reload 'customer';

    rest_reload_list 'customer';

    stash_test 'customer.list', sub {
        my ($me) = @_;
		
        ok( $me = delete $me->{customers}, 'customer list exists' );
        is( $me->[0]{is_active}, 0, 'customer inactivated' );
    };

};

done_testing;