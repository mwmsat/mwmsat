use utf8;
use FindBin qw($Bin);
use lib "$Bin/../../lib";

use Trackware::Test::Further;

api_auth_as user_id => 1, roles => ['superadmin'];

db_transaction {
    #criar novo rastreador
    rest_post '/position_cache',
      name  => 'criar cache de posições',
      list  => 1,
      stash => 'position_cache',
      [
		point_id	=> '-2339.04&-4627.22',
        address		=> 'Estrada Adutora Rio Claro, Mauá, SP',
        lat_lng		=> '-2339.04150390625,-4627.22216796875'
      ];

    stash_test 'position_cache.get', sub {
        my ($me) = @_;

        is( $me->{id}, stash 'position_cache.id', 'get has the same id!' );
    };

    stash_test 'position_cache.list', sub {
        my ($me) = @_;

        ok( $me = delete $me->{position_cache}, 'position_cache list exists' );

        is( @$me, 1, '1 position_cache' );

        $me = [ sort { $a->{id} cmp $b->{id} } @$me ];

		is( $me->[0]{point_id}, '-2339.0&-4627.2', 'listing ok' );
    };
    
    rest_delete stash 'position_cache.url';

    rest_reload 'position_cache', 404;

    rest_reload_list 'position_cache';

    stash_test 'position_cache.list', sub {
        my ($me) = @_;
        ok( $me = delete $me->{position_cache}, 'position_cache list exists' );

        is( @$me, 0, '0 position_caches' );
    };
};

done_testing;