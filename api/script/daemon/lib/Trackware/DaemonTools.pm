package Trackware::DaemonTools;
$|++;
use v5.16;
use strict;
use warnings;
use utf8;

# importa o projeto ja
use FindBin::libs qw( base=api subdir=lib );

use Trackware::Log;

my $logger = get_logger();
our $BAIL_OUT = 0;

# importa as funcoes para o script.
no strict 'refs';
*{"main::$_"} = *$_ for grep { defined &{$_} } keys %Trackware::DaemonTools::;
use strict 'refs';

# coloca use strict/warnings sozinho
sub import {
  strict->import;
  warnings->import;
  Trackware::Log->import;
}

# logs
sub log_info {
  my (@texts) = @_;
  $logger->info( join ' ', @texts );
}

sub log_error {
  my (@texts) = @_;
  $logger->error( join ' ', @texts );
}

sub log_fatal {
  my (@texts) = @_;
  $logger->fatal( join ' ', @texts );
}

# daemon functions
sub log_and_exit {
  log_info "Graceful exit.";
  exit(0);
}

sub log_and_wait {
  log_info "SIG [TERM|INT] RECV. Waiting job...";
  $BAIL_OUT = 1;
}

# atalhos do daemon
sub ASKED_TO_EXIT {
  $BAIL_OUT;
}

sub EXIT_IF_ASKED {
  &log_and_exit() if $BAIL_OUT;
}

sub ON_TERM_EXIT {
  $SIG{TERM} = \&log_and_exit;
  $SIG{INT}  = \&log_and_exit;
}

sub ON_TERM_WAIT {
  $SIG{TERM} = \&log_and_wait;
  $SIG{INT}  = \&log_and_wait;
}

sub GET_SCHEMA {

  log_info "require Trackware::Schema...";
  require Trackware::Schema;

  my $app_dir = $ENV{TRACKWARE_APP_DIR} || '/www/aware/trackware';

  my $conf   = Config::General->new("$app_dir/api/trackware_local.conf");
  my %config = $conf->getall;

  if ( !$config{redis}{host} ) {
    $conf   = Config::General->new("$app_dir/api/trackware.conf");
    %config = $conf->getall;
  }

  %config = %{ $config{model}{DB}{connect_info} || {} };

  die "Can't find schema configuration" unless $config{dsn};

  # database
  my $db_dsn  = $config{dsn};
  my $db_pass = $config{password};
  my $db_user = $config{user};

  Trackware::Schema->connect(
    $db_dsn, $db_user, $db_pass,
    {
      "AutoCommit"     => 1,
      "quote_char"     => "\"",
      "name_sep"       => ".",
      "pg_enable_utf8" => 1,
      "on_connect_do"  => "SET client_encoding=UTF8"
    }
  );

}

1
