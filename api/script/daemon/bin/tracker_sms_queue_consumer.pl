#!/usr/bin/env perl
use utf8;

use FindBin::libs qw( base=daemon subdir=lib );

use Trackware::DaemonTools;

use Furl;
use JSON qw(from_json to_json);
use JSON::XS;
use Trackware::SqsManager;
use Trackware::TrackingManager;
use Trackware::TrackerMessageParser;
use Trackware::Util qw(extract_message_data);
use Amazon::SQS::Simple;
my $schema = GET_SCHEMA;
my $coder  = JSON::XS->new;

my $tracking_manager = Trackware::TrackingManager->new({schema => $schema});
my $sqs = Trackware::SqsManager->new(queue_name => 'processed-messages');
my $logger = get_logger();

=pod
my $dev_sqs = Amazon::SQS::Simple->new(
  'AKIAJEEKIMTZOAXCSQVA', 'FheLixx3MhkSwEJemPMwWRflopZB8n1EqkN21Lup',
  Endpoint => 'http://sqs.sa-east-1.amazonaws.com',
  Region   => 'sa-east-1'
);

my $dev_queue = $dev_sqs->CreateQueue('processed-messages');
warn $dev_queue->Endpoint;
=cut

&process;

sub process {
  $logger->info("Consumer initialized successfully, waiting for data.......");


  my $i = 0;

  while (1) {
    
    my $message = eval { $sqs->sqs->ReceiveMessage(WaitTimeSeconds => 5) };
    warn $@ if $@;
    eval {
      if ($message) {
        warn $message->MessageBody;
#        eval { warn 'TRY' ; my $ret = $dev_queue->SendMessage($message->MessageBody); warn "RET: $ret"; };
        warn $@ if $@;
        my $msg_trans = from_json($message->MessageBody);

        if (!$tracking_manager->add_gprs_lbs(extract_message_data($msg_trans)))
        {
          $logger->error(
            "Error saving data. Message: ${\($message->MessageBody)}");
        }

        $sqs->sqs->DeleteMessage($message->ReceiptHandle);
      }
    };

    $logger->error($@) if $@;
    next;
  }
}

