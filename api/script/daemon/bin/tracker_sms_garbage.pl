#!/usr/bin/env perl
use utf8;

use FindBin::libs qw( subdir=lib );

use JSON::XS qw(decode_json);
use Trackware::SqsManager;
use Config::General;
use Trackware::Schema;
use DateTimeX::Easy qw(parse);
use Time::HiRes;
use DDP;

my $conf = +{
  Config::General->new(
    -ConfigFile =>
      ($ENV{TRACKWARE_APP_FILE} or die 'Please, set TRACKWARE_APP_FILE')
  )->getall
};

my $schema
  = Trackware::Schema->connect(
  delete @{$conf->{model}->{DB}->{connect_info}}{qw(dsn user password)},
  $conf->{model}->{DB}->{connect_info});

warn 1;
my $sqs = Trackware::SqsManager->new(queue_name => 'trackware-sms-garbage');
warn 2;
while (1) {
warn 3;
  my $message = eval { $sqs->sqs->ReceiveMessage(WaitTimeSeconds => 10) };
warn $@ if $@;
#    or sleep(5), warn($@), next;
  warn 4;
warn $message if $message;
  if ($message) {
    eval {
      $schema->txn_do(
        sub {
          my $data = decode_json($message->MessageBody);
          return unless $data->{phone} && $data->{text};
          $data->{phone} =~ s/^tel://;
          $schema->resultset('SmsGarbage')->create(
            {
              message     => $data->{text},
              chip_number => $data->{phone},
              ts => (parse($data->{datetime}) || parse('now'))->datetime,
            }
          );
          $sqs->sqs->DeleteMessage($message->ReceiptHandle);
        }
      );
    };

    warn $@ if $@;
  }
}

