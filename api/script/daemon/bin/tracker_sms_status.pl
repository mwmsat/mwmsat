#!/usr/bin/env perl
use strict;
use warnings;
use feature 'say';
use FindBin::libs qw(  subdir=lib );

use DDP;

$|++;

use Paws;
use Furl;
use URI;
use Trackware::Schema;
use URI::Escape qw(uri_escape);
use JSON qw(encode_json decode_json);
use Parallel::Prefork::SpareWorkers qw(:status);
use IO::Socket::SSL;
use Data::UUID;

use Config::General;
use Trackware::Schema;
use Trackware::Redis;

my $redis = Trackware::Redis->new();


my $conf = +{
  Config::General->new(
    -ConfigFile =>
      ($ENV{TRACKWARE_APP_FILE} or die 'Please, set TRACKWARE_APP_FILE')
  )->getall
};

my $schema
  = Trackware::Schema->connect(
  delete @{$conf->{model}->{DB}->{connect_info}}{qw(dsn user password)},
  $conf->{model}->{DB}->{connect_info});

IO::Socket::SSL::set_defaults(SSL_verify_mode => 0x00);

my $aws = Paws->new();
my $sqs = $aws->service('SQS', region => 'sa-east-1');

my $queue_url = eval {
  $sqs->GetQueueUrl(QueueName => 'trackware-sms-delivery-status')->QueueUrl;
} or die 'could not find queue url: ' . $@;

# my $pm = Parallel::Prefork::SpareWorkers->new(
#   {
#     max_workers       => 6,
#     min_spare_workers => 2,
#     max_spare_workers => 5,
#     trap_signals      => {
#       TERM => 'TERM',
#       HUP  => 'TERM',
#       USR1 => undef,
#     },
#   }
# );

# while ( $pm->signal_received ne 'TERM' ) {

#   $pm->start and next;

#   while (1) {

#     $pm->set_status(STATUS_IDLE);

#     my $r =
#       $sqs->ReceiveMessage( QueueUrl => $queue_url, MaxNumberOfMessages => 5 );

#     process_message($_) for @{ $r->Messages || [] };

#     $pm->set_status('A');

#   }

#   $pm->finish;
# }
# $pm->wait_all_children;

while (1) {
  my $r = eval {
    $sqs->ReceiveMessage(
      QueueUrl            => $queue_url,
      MaxNumberOfMessages => 5,
      WaitTimeSeconds     => 5
    );
  } or sleep(10), warn($@), next;
  process_message($_) for @{$r->Messages || []};
}

sub delete_message {
  my ($msg) = @_;
  eval {
    $sqs->DeleteMessage(
      QueueUrl      => $queue_url,
      ReceiptHandle => $msg->ReceiptHandle
    );
  };
}

sub process_message {
  my ($payload) = @_;
  say '>>> ' . $payload->Body;
  my $err;
  my $message = eval { decode_json($payload->Body) }
    or ($err = $@), delete_message($payload), die 'error: ' . $err;

  print
    "$message->{datetime}: $message->{phone} [$message->{delivery_status}]\n";

  my $sms_tracker
    = $schema->resultset('SmsTracker')
    ->search({'message_uuid' => $message->{message_uuid}}, {rows => 1})->next
    or delete_message($payload), return;


  my $sms
    = $sms_tracker->add_to_sms_statuses({name => $message->{delivery_status}});

  $sms_tracker->update({delivered_at => \q{now()}})
    if $message->{delivery_status} =~ /DeliveredToTerminal/i;
  my $tracker = $sms_tracker->tracker;

  $sms_tracker->discard_changes;
  
  $redis->redis->publish($tracker->queue_key,
    encode_json({delivery_status => +{$sms_tracker->get_columns}}));


  print
    "$message->{datetime}: $message->{phone} [$message->{delivery_status}]\n";

  return delete_message($payload);
}

