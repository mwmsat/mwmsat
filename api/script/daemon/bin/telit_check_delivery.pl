#!/usr/bin/env perl
use strict;
use warnings;
use feature 'say';
use FindBin::libs qw( subdir=lib );
use FindBin qw($Bin);
use Carp qw(carp);
use JSON qw(decode_json);
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::SOAP11;
use XML::Compile::WSDL11;
use MIME::Base64 'encode_base64';
use DDP;
use Paws;

my $user     = 'INVISI_GA';
my $password = 'x7819023';

my $soap =
  XML::Compile::WSDL11->new(
  "$ENV{TRACKWARE_APP_DIR}/api/data/XbSmsServices06.wsdl");

my $aws = Paws->new();
my $sqs = $aws->service( 'SQS', region => 'us-east-1' );
my $queue_url =
  eval { $sqs->GetQueueUrl( QueueName => 'telit-check-delivery' )->QueueUrl }
  or die 'could not find queue url: ' . $@;

sub basic_auth ($$) {
  my ( $request, $trace ) = @_;

  # Encode userid and password
  my $authorization = 'Basic ' . encode_base64 "$user:$password";

  # Modify http header to include basic authorisation
  $request->header( Authorization => $authorization );

  my $ua = $trace->{user_agent};
  $ua->request($request);
}

while (1) {
  my $got_term;
  local $SIG{TERM} = sub { warn "$$ got TERM"; $got_term++ };

  my $call =
    $soap->compileClient( q{getSmsDeliveryStatus},
    transport_hook => \&basic_auth );
  say 'waiting...';

  my $result =
    $sqs->ReceiveMessage( QueueUrl => $queue_url, MaxNumberOfMessages => 5 );

  foreach my $msg ( @{ $result->Messages } ) {

    my $json = eval { decode_json( $msg->Body ) }
      or carp( 'Invalid json: ' . $msg->Body ), next;

    p($json);
    my ( $res, $trace ) = $call->(
      deviceIdType => 'MSISDN',
      msgRef       => $json->{msgref}
    );
    p($res);

    if ($got_term) {
      say "$$ terminating";
      exit(0);
    }

  }
  sleep(5);
}

