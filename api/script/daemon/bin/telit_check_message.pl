#!/usr/bin/env perl
use strict;
use warnings;
use feature 'say';
use FindBin::libs qw( subdir=lib );

use JSON qw(encode_json);

use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::SOAP11;
use XML::Compile::WSDL11;
use MIME::Base64 'encode_base64';
use DDP;
use Paws;
use DateTime;

my $user     = 'INVISI_GA';
my $password = 'x7819023';

my $soap =
  XML::Compile::WSDL11->new(
  "$ENV{TRACKWARE_APP_DIR}/api/data/XbSmsServices06.wsdl");

sub basic_auth ($$) {
  my ( $request, $trace ) = @_;

  # Encode userid and password
  my $authorization = 'Basic ' . encode_base64 "$user:$password";

  # Modify http header to include basic authorisation
  $request->header( Authorization => $authorization );

  my $ua = $trace->{user_agent};
  $ua->request($request);
}
my $aws = Paws->new();
my $sqs = $aws->service( 'SQS', region => 'us-east-1' );
my $queue_url =
  eval { $sqs->GetQueueUrl( QueueName => 'trackware-inbound-sms' )->QueueUrl }
  or die 'could not find queue url: ' . $@;
warn $queue_url;
while (1) {
  my $got_term;
  local $SIG{TERM} = sub { warn "$$ got TERM"; $got_term++ };
  my $call =
    $soap->compileClient( q{getReceivedSms}, transport_hook => \&basic_auth );

  my ( $res, $trace ) = $call->(
    deviceIdType => 'MSISDN',
    network      => q{AT}
  );

  if ( $res
    && ( ref $res eq 'HASH' )
    && ( defined $res->{response}->{status}->{statusText} )
    && ( $res->{response}->{status}->{statusText} eq 'OK' ) )
  {
    if ( defined $res->{response}->{smsMoMessages}->{smsMoMessages}
      && ref $res->{response}->{smsMoMessages}->{smsMoMessages} eq 'ARRAY'
      && @{ $res->{response}->{smsMoMessages}->{smsMoMessages} } )
    {
      say "$_->{receivedAt}: $_->{deviceId} - $_->{message}"
        and $sqs->SendMessage(
        QueueUrl    => $queue_url,
        MessageBody => encode_json(
          {
            text      => $_->{message},
            phone     => $_->{deviceId},
            smscenter => 1111,
            datetime  => $_->{receivedAt},
	    supplier => 'Telit'
          }
          )
        ) for @{ $res->{response}->{smsMoMessages}->{smsMoMessages} };
    }
  }

  if ($got_term) {
    say "$$ terminating";
    exit(0);
  }
  sleep(5);
}

