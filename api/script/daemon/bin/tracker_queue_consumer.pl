#!/usr/bin/env perl
use utf8;

use FindBin::libs qw( base=daemon subdir=lib );

use Trackware::DaemonTools;

use Furl;
use JSON::XS;
use Trackware::SqsManager;
use Trackware::TrackingManager;

# use Trackware::TrackerMessageParser;
use Trackware::TrackerMessageHandler;

my $schema = GET_SCHEMA;
my $coder  = JSON::XS->new;

my $tracking_manager =
  Trackware::TrackingManager->new( { schema => $schema } );
my $sqs    = Trackware::SqsManager->new();
my $logger = get_logger();

&process;

sub process {
  $logger->info("Consumer initialized successfully, waiting for data.......");

  eval {
    my $i = 0;

    while (1) {
      my $message_obj = $sqs->sqs->ReceiveMessage();

      if ($message_obj) {
        my $message;

        eval { $message = decode_json( $message_obj->MessageBody ); };

        $message->{text} = $message_obj->MessageBody if $@;

        my $model = &check_header( $message->{text} );

        my %msg_trans =
          Trackware::TrackerMessageHandler->with_traits("$model")
          ->new( message => $message )->parse;

# 				my $save_result = $model eq 'GPRS' ? $tracking_manager->add(%msg_trans) : $tracking_manager->add_sms_message(%msg_trans);

        my $save_result;

        if ( $model eq 'GPRS' ) {
          $save_result = $tracking_manager->add(%msg_trans);
        }
        elsif ( $model eq 'GPRS_LBS' ) {
          $save_result = $tracking_manager->add_gprs_lbs(%msg_trans);
        }
        else {
          $save_result = $tracking_manager->add_sms_message(%msg_trans);
        }

        if ( !$save_result ) {
          $logger->error("Error saving data. Message: $message->{text}");
        }

        $sqs->sqs->DeleteMessage( $message_obj->ReceiptHandle );
      }

      next;
    }
  };

  $logger->error($@) if $@;
}

sub check_header {
  my $message = shift;

  my @headers_sms = qw/
    POA
    REED
    EVT
    JAM
    ALA6
    RESP
    RST
    XXX
    ALA12
    ALA13
    /;

  my @r = split ',', $message;

  my $model = 'GPRS';

  if ( scalar @r <= 1 && unpack( 'A4', $message ) eq '5343' ) {
    $model = 'GPRS';
  }
  elsif ( scalar @r <= 1 && unpack( 'A6', $message ) eq '4c4253' ) {
    $model = 'GPRS_LBS';
  }
  else {
    my %items = map { $_ => 1 } @headers_sms;
    $model = 'KSlim' if exists $items{ $r[0] };
  }

  return $model;
}
