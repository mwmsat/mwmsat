#!/usr/bin/env perl
use utf8;
use strict;
use warnings;
use FindBin::libs qw( subdir=lib );
use DDP;

$|++;
use feature 'say';
binmode STDOUT, ":encoding(UTF-8)";
binmode STDERR, ":encoding(UTF-8)";
binmode STDIN,  ":encoding(UTF-8)";

use Paws;

use Trackware::Schema;
use Trackware::Supplier;
use Trackware::Schema;
use JSON qw(encode_json decode_json);
use Parallel::Prefork::SpareWorkers qw(:status);

use Data::UUID;
use Config::General;

use IO::Socket::SSL;

use Trackware::Redis;

my $redis = Trackware::Redis->new();


my $conf = +{
  Config::General->new(
    -ConfigFile =>
      ($ENV{TRACKWARE_APP_FILE} or die 'Please, set TRACKWARE_APP_FILE')
  )->getall
};

my $schema
  = Trackware::Schema->connect(
  delete @{$conf->{model}->{DB}->{connect_info}}{qw(dsn user password)},
  $conf->{model}->{DB}->{connect_info});

IO::Socket::SSL::set_defaults(SSL_verify_mode => 0x00);

#my $aws = Paws->new();
#my $sqs = $aws->service('SQS', region => 'us-east-1');
use Trackware::SqsManager;
my $sqs = Trackware::SqsManager->new(queue_name => 'trackware-outbound-sms')->sqs;
#my $queue_url
#  = eval { $sqs->GetQueueUrl(QueueName => 'trackware-outbound-sms')->QueueUrl; }
#  or die 'could not find queue url: ' . $@;

my $sms_api_endpoint = 'https://oneapi.torpedoempresas.vivo.com.br';

# my $pm = Parallel::Prefork::SpareWorkers->new(
#   {
#     max_workers       => 6,
#     min_spare_workers => 2,
#     max_spare_workers => 5,
#     trap_signals      => {
#       TERM => 'TERM',
#       HUP  => 'TERM',
#       USR1 => undef,
#     },
#   }
# );

# while ( $pm->signal_received ne 'TERM' ) {

#   $pm->start and next;

#   while ( sleep(1) ) {

#     $pm->set_status(STATUS_IDLE);
#     my $r =
#       $sqs->ReceiveMessage( QueueUrl => $queue_url, MaxNumberOfMessages => 5 );

#     send_message($_) for @{ $r->Messages || [] };

#     $pm->set_status('A');

#   }

#   $pm->finish;
# }
# $pm->wait_all_children;
use Try::Tiny::Retry;

while (1) {
  my $r;
  try {
    $r = $sqs->ReceiveMessage(
#      QueueUrl            => $queue_url,
# 	     MaxNumberOfMessages => 5,
      WaitTimeSeconds     => 5
    );
  }
  catch { warn $_ };
#  warn $r;
  send_message($r) if $r;
}

sub delete_message {
  my ($msg) = @_;
  eval {
    $sqs->DeleteMessage($msg);
#      QueueUrl      => $queue_url,
#      ReceiptHandle => $msg->ReceiptHandle
#    );
  };
warn $@ if $@;
}

my %suppliers = ();

sub send_message {
  my ($payload) = @_;
  say '>>> ' . $payload->MessageBody;
  my $message = eval { decode_json($payload->MessageBody) }
    or delete_message($payload), die 'error: ' . $@;

  my $tracker = $schema->resultset('Tracker')->search_rs(
    {'me.id' => $message->{tracker_id}},
    {
      join      => 'network_supplier',
      '+select' => qw(network_supplier.name),
      '+as'     => qw(supplier_name),
    }
    )->next
    or print("Equipamento não existe: ${\($message->{tracker_id})}\n"),
    delete_message($payload), return;

  my $supplier_name = $tracker->get_column('supplier_name');
  my $supplier      = (
    $supplier_name
      && (
      $suppliers{$supplier_name}
      || ($suppliers{$supplier_name}
        = Trackware::Supplier->with_traits(
          $tracker->get_column('supplier_name'))->new)
      )
    )
    or print(
    "Equipamento não possui fornecedor de SMS: ${\($tracker->phone_number)}\n"
    ), delete_message($payload), return;

  my $sms = $tracker->add_to_smses(
    {direction => 'to-tracker', message => $message->{text}});
  $sms->discard_changes;

  $schema->resultset('TrackerNotificationSms')->create(
    {
      tracker         => $tracker,
      sms_tracker     => $sms,
      notification_id => $message->{notification_id}
    }
  ) if $message->{notification_id};

  my $receipt = $supplier->send($tracker->phone_number, $sms->message,
    $sms->message_uuid);

  if ($receipt && !ref $receipt) {
    $sms->update({receipt_check_url => $receipt});
  }
  say join(' ',
    scalar localtime, $tracker->phone_number,
    $sms->message,    $sms->message_uuid);

  $sms->discard_changes;
  $redis->redis->publish($tracker->queue_key,
    encode_json({delivery_status => +{$sms->get_columns}}));

  return delete_message($payload);
}

exit(0);
