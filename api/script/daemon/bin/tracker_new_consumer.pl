#!/usr/bin/env perl
use utf8;

use FindBin::libs qw( base=daemon subdir=lib );

use Trackware::DaemonTools;

use JSON::XS;
use Trackware::Redis;
use Trackware::TrackingManager;

my $coder  = JSON::XS->new;
my $redis  = Trackware::Redis->new();
my $schema = GET_SCHEMA;
my $tracking_manager =
  Trackware::TrackingManager->new( { schema => $schema } );
my $logger = get_logger();

&process;

sub process {
  $logger->info("Consumer initialized successfully, waiting for data.......");

  eval {
    while (1) {
      my ( $list, $trackers ) = $redis->redis->blpop( 'new_trackers', 0 );

      if ($trackers) {
        my $ret = $tracking_manager->new_tracker($trackers);

        if ( !$ret ) {
          $logger->error("Error adding new trackers: $trackers");
        }
        elsif ( $ret == 2 ) {
          $logger->warn("Tracker already exist: $trackers");
        }
        else {
          $logger->info("Tracker added successfully: $trackers");
        }
      }

      next;
    }
  };

  $logger->error($@) if $@;
}
