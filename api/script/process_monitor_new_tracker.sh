PROCESSO="tracker_new_consumer.pl"
# LOCAL="/www/aware/lambda/"
LOCAL="/home/ubuntu/trackware/api/script/daemon"
INTERVALO=60

cd $LOCAL
while true; do
	OCORRENCIAS=`ps ax | grep $PROCESSO | grep -v grep| wc -l`
	if [ $OCORRENCIAS -eq 0 ]; then
		`./tracker_new_consumer start`
		
 	#	echo "O processo foi reiniciado em: "  `date` > $LOCAL/script/log/new_tracker.log
	fi

	sleep $INTERVALO
done
