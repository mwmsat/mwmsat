#!/usr/bin/env perl

use strict;
use warnings;

use Config::JFDI;
use Trackware::Schema;

my $conf =
  Config::JFDI->new( file =>
    ( $ENV{TRACKWARE_APP_FILE} or die 'Please, set TRACKWARE_APP_FILE' ) )
  ->get;

my $schema = Trackware::Schema->connect(
  delete @{ $conf->{model}->{DB}->{connect_info} }{qw(dsn user password)},
  $conf->{model}->{DB}->{connect_info} );

$schema->resultset('PositionCache')->normalize_cache;
exit(0);
