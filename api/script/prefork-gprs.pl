#!perl
use strict;
use warnings;
$|++;

package MWMSat::GPRS;

use FindBin qw($Bin $Script);
use lib "$Bin/../lib";
use DateTime;
use feature 'say';
use JSON qw(encode_json);
use Trackware::Parser::GPS qw(parse);
use base qw(Net::Server);
use DDP;
my %imei_ccid_map =
  ('4879,9947' => '5511975169360', '5645,8713' => '5511941094835',);

use Trackware::SqsManager;
my $sqs = Trackware::SqsManager->new(
  {
    queue_name => 'trackware-inbound-sms'

  }
);


sub process_request {
  my $self = shift;
  warn scalar localtime;
  while (<STDIN>) {
    my $time = scalar localtime;
    warn "$time : $_";
    s/[\r\n]+$//;
#    print "You said '$_'\015\012";    # basic echo
    if (my $msg = eval { parse($_) }) {
      if (
        my $chip_number = eval {
          $imei_ccid_map{
            join(',', $msg->{body}->{imei}, $msg->{body}->{iccid},)
          };
        }
        )
      {
        eval {
          $sqs->sqs->SendMessage(
            encode_json(
              {
                phone     => $chip_number,
                smscenter => 53011,
                text      => $_,
                datetime  => DateTime->now->iso8601(),
                supplier  => 'Vivo'
              }
            )
          );
        };
        warn "Error sending message: $@" if $@;
      }
    }
  }
}

MWMSat::GPRS->run(port => 2389, ipv => '4', log_file => '/tmp/netcat.log', log_level  => 3);
