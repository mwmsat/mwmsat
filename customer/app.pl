use strict;
use warnings;
use Mojolicious::Lite;
use Mojo::URL;
use DDP;
app->secrets(
  [
    app->config->{secret}
      || '9027a1d3c9f359fd3abadb8613372c856b44459e5a3743118554ad97'
  ]
);

plugin Config => {file => 'app.conf'};

get '/' => sub {
  my $c = shift;
  warn $c->session('logged_in');
  return $c->redirect_to('login') unless $c->session('logged_in');
} => 'index';

get '/login' => 'login';

post '/login' => sub {
  my $c = shift;
  p(app->config);
  $c->session(logged_in => 1);
  return $c->redirect_to('index');
};

get '/logout' => sub {
  my $c = shift;
  $c->session(expires => 1);
  return $c->redirect_to('login');
};

post '/new-tracker' => 'new-tracker';

post '/register' => sub {
  my $c = shift;
  $c->ua->post(
    Mojo::URL->new($c->config->{api_url})->path('/users')
      ->query(api_key => '__from_customer_page'),
    form => $c->req->params->to_hash
  );
} => 'register';

push @{app->static->paths}, 'static';

app->start;


