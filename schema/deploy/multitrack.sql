-- Deploy trackware:multitrack to pg
-- requires: tracker-notification-sms-programming-confirmation

BEGIN;

CREATE TABLE multitrack (
  id SERIAL PRIMARY KEY NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  name TEXT UNIQUE NOT NULL,
  description TEXT
);

CREATE TABLE multitrack_tracker (
  multitrack_id INTEGER REFERENCES multitrack(id) NOT NULL,
  tracker_id INTEGER REFERENCES tracker(id) NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  UNIQUE(multitrack_id, tracker_id)
);

COMMIT;
