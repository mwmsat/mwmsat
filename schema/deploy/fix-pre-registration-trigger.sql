-- Deploy trackware:fix-pre-registration-trigger to pg

BEGIN;

drop trigger IF EXISTS make_pre_registration_seq_f ON business_unity_pre_registration ;
drop trigger IF EXISTS fill_in_stuff_seq_f on business_unity_pre_registration;

drop trigger IF EXISTS aa__make_pre_registration_seq_f ON business_unity_pre_registration;
drop trigger IF EXISTS ab__fill_in_stuff_seq_f ON business_unity_pre_registration;

CREATE OR REPLACE FUNCTION make_pre_registration_seq() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  execute format('CREATE SEQUENCE IF NOT EXISTS pre_registration_seq_%s', NEW.business_unity_id);
  return NEW;
end
$$;

CREATE TRIGGER aa__make_pre_registration_seq_f
  BEFORE INSERT ON business_unity_pre_registration
  FOR EACH ROW EXECUTE PROCEDURE make_pre_registration_seq();


CREATE OR REPLACE FUNCTION fill_in_stuff_seq() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  NEW.seq := nextval('pre_registration_seq_' || NEW.business_unity_id);
  RETURN NEW;
end
$$;

CREATE TRIGGER ab__fill_in_stuff_seq_f
  BEFORE INSERT ON business_unity_pre_registration
  FOR EACH ROW EXECUTE PROCEDURE fill_in_stuff_seq();


COMMIT;
