-- Deploy trackware:sms_tracker_delivered_at to pg

BEGIN;

ALTER TABLE sms_tracker ADD COLUMN delivered_at TIMESTAMP WITHOUT TIME ZONE;

COMMIT;
