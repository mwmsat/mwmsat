-- Deploy trackware:tracker-type-battery to pg

BEGIN;

ALTER TABLE tracker_type ADD COLUMN battery_level INTEGER;

COMMIT;
