-- Deploy trackware:tracker-types to pg

BEGIN;

CREATE TABLE tracker_type (
  id SERIAL PRIMARY KEY,
  code TEXT NOT NULL UNIQUE,
  name TEXT NOT NULL UNIQUE,
  days INTEGER,
  low_battery_level_alert INTEGER,
  has_temperature_sensor BOOLEAN NOT NULL DEFAULT FALSE,
  has_movement_sensor BOOLEAN NOT NULL DEFAULT FALSE,
  has_gps_sensor BOOLEAN NOT NULL DEFAULT FALSE,
  has_rf_sensor BOOLEAN NOT NULL DEFAULT FALSE
);

ALTER TABLE tracker ADD COLUMN type_id INTEGER REFERENCES tracker_type(id) ON DELETE SET NULL;

COMMIT;
