-- Deploy trackware:command_and_roles to pg

BEGIN;

CREATE TABLE command (
  id SERIAL PRIMARY KEY,
  name TEXT NOT NULL UNIQUE,
  message_template TEXT NOT NULL,
  create_ts TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  valid BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE command_role (
  command_id INTEGER REFERENCES command(id) ON DELETE CASCADE NOT NULL ,
  role_id INTEGER REFERENCES "role"(id) ON DELETE CASCADE NOT NULL,
  UNIQUE(command_id, role_id)
);

COMMIT;
