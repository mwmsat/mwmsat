-- Deploy sms_garbage

BEGIN;

-- XXX Add DDLs here.

CREATE TABLE sms_garbage(
  message TEXT,
  ts TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  chip_number TEXT NOT NULL
);
  

COMMIT;
