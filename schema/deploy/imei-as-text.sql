-- Deploy trackware:imei-as-text to pg

BEGIN;

ALTER TABLE tracker
  ALTER COLUMN imei TYPE TEXT;

COMMIT;
