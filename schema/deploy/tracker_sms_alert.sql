-- Deploy trackware:tracker_sms_alert to pg

BEGIN;

CREATE TABLE tracker_sms_alert (
  id SERIAL PRIMARY KEY,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  has_gps_location BOOLEAN DEFAULT FALSE,
  has_address BOOLEAN DEFAULT FALSE,	
  has_status BOOLEAN DEFAULT FALSE,
  has_battery_level BOOLEAN DEFAULT FALSE,  
  destination_phone_numbers TEXT NOT NULL,
  tracker_id INTEGER REFERENCES tracker(id) ON DELETE CASCADE NOT NULL 
);

COMMIT;
