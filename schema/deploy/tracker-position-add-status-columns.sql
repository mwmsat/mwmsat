-- Deploy trackware:tracker-position-add-status-columns to pg

BEGIN;

ALTER TABLE tracker_position
  ADD COLUMN tx INTEGER,
  ADD COLUMN cycle INTEGER,
  ADD COLUMN rf INTEGER,
  ADD COLUMN gprs INTEGER,
  ADD COLUMN gps INTEGER,
  ADD COLUMN r1 INTEGER,  
  
  DROP COLUMN battery_voltage_bkp,
  DROP COLUMN hourmeter,
  DROP COLUMN message_size,
  DROP COLUMN odometer,
  DROP COLUMN package_type,
  DROP COLUMN pcb_version,
  DROP COLUMN inputs,
  DROP COLUMN outputs,
  DROP COLUMN position_size,
  DROP COLUMN position_amount,
  DROP COLUMN original_message,
  DROP COLUMN speed,
  DROP COLUMN sat_number,
  DROP COLUMN hdop;
COMMIT;
