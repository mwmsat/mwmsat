-- Deploy appschema

BEGIN;
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: gender; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE gender AS ENUM (
    'm',
    'f'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actions_log; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE actions_log (
    id integer NOT NULL,
    dt_when timestamp without time zone DEFAULT now() NOT NULL,
    url text,
    user_id integer,
    message text,
    ip text
);


--
-- Name: actions_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE actions_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: actions_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE actions_log_id_seq OWNED BY actions_log.id;


--
-- Name: address; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE address (
    id integer NOT NULL,
    address text NOT NULL,
    number text NOT NULL,
    neighborhood text NOT NULL,
    complement text,
    city_id integer NOT NULL,
    lat_lng point,
    postal_code text NOT NULL
);


--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- Name: business_unity; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE business_unity (
    id integer NOT NULL,
    corporate_name text NOT NULL,
    fancy_name text,
    register numeric NOT NULL,
    address_id integer,
    is_active boolean DEFAULT true,
    website text,
    email text,
    parent_id integer,
    phone text NOT NULL,
    phone_2 text,
    domain text NOT NULL
);


--
-- Name: business_unity_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE business_unity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: business_unity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE business_unity_id_seq OWNED BY business_unity.id;


--
-- Name: business_unity_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE business_unity_users (
    user_id integer NOT NULL,
    business_unity_id integer NOT NULL,
    is_active boolean,
    id integer NOT NULL
);


--
-- Name: business_unity_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE business_unity_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: business_unity_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE business_unity_users_id_seq OWNED BY business_unity_users.id;


--
-- Name: cep_cache; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cep_cache (
    id integer NOT NULL,
    address text NOT NULL,
    postal_code integer NOT NULL,
    neighborhood text,
    city_id integer NOT NULL,
    state_id integer NOT NULL,
    location text
);


--
-- Name: cep_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cep_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cep_cache_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cep_cache_id_seq OWNED BY cep_cache.id;


--
-- Name: city; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE city (
    id integer NOT NULL,
    state_id integer NOT NULL,
    name text NOT NULL,
    name_url text,
    created_at timestamp without time zone DEFAULT now(),
    country_id integer
);


--
-- Name: city_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE city_id_seq OWNED BY city.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE country (
    id integer NOT NULL,
    acronym text NOT NULL,
    name text NOT NULL,
    name_url text
);


--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE country_id_seq OWNED BY country.id;


--
-- Name: customer; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE customer (
    id integer NOT NULL,
    fancy_name text NOT NULL,
    corporate_name text NOT NULL,
    address_id integer NOT NULL,
    business_unity_id integer,
    parent_id integer,
    created_at timestamp without time zone DEFAULT now(),
    is_active boolean DEFAULT true,
    phone text NOT NULL,
    mobile_phone text,
    email text NOT NULL,
    website text,
    register numeric
);


--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_id_seq OWNED BY customer.id;


--
-- Name: customer_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE customer_users (
    id integer NOT NULL,
    user_id integer NOT NULL,
    customer_id integer NOT NULL
);


--
-- Name: customer_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE customer_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE customer_users_id_seq OWNED BY customer_users.id;


--
-- Name: document_type; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE document_type (
    id integer NOT NULL,
    type text NOT NULL,
    description text
);


--
-- Name: document_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE document_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: document_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE document_type_id_seq OWNED BY document_type.id;


--
-- Name: erb; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE erb (
    id integer NOT NULL,
    mcc integer NOT NULL,
    mnc integer NOT NULL,
    lac integer NOT NULL,
    cid integer NOT NULL,
    rxl integer
);


--
-- Name: erb_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE erb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: erb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE erb_id_seq OWNED BY erb.id;


--
-- Name: erb_position; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE erb_position (
    id integer NOT NULL,
    source_id integer NOT NULL,
    erb_id integer NOT NULL,
    latitude double precision DEFAULT 0.0 NOT NULL,
    longitude double precision DEFAULT 0.0 NOT NULL,
    azimuth integer DEFAULT 0 NOT NULL,
    radius integer DEFAULT 0 NOT NULL,
    last_check timestamp without time zone DEFAULT now()
);


--
-- Name: erb_position_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE erb_position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: erb_position_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE erb_position_id_seq OWNED BY erb_position.id;


--
-- Name: event_type; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE event_type (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: event_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE event_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE event_type_id_seq OWNED BY event_type.id;


--
-- Name: fuel_type; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fuel_type (
    id integer NOT NULL,
    type text NOT NULL
);


--
-- Name: fuel_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fuel_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fuel_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fuel_type_id_seq OWNED BY fuel_type.id;


--
-- Name: geolocation_cache; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE geolocation_cache (
    cache_key character varying(32) NOT NULL,
    request character varying NOT NULL,
    response character varying NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: insurance_company; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE insurance_company (
    id integer NOT NULL,
    name text
);


--
-- Name: insurance_company_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE insurance_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: insurance_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE insurance_company_id_seq OWNED BY insurance_company.id;


--
-- Name: network_supplier; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE network_supplier (
    id integer NOT NULL,
    name text NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


--
-- Name: network_supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE network_supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: network_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE network_supplier_id_seq OWNED BY network_supplier.id;


--
-- Name: position_cache; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE position_cache (
    id integer NOT NULL,
    point_id text NOT NULL,
    address text NOT NULL,
    lat_lng point NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    point_orig_id text,
    postal_code character varying(8)
);


--
-- Name: position_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE position_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: position_cache_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE position_cache_id_seq OWNED BY position_cache.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- Name: sms_message_tmp; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sms_message_tmp (
    id integer NOT NULL,
    data text,
    created_at timestamp without time zone DEFAULT now()
);


--
-- Name: sms_message_tmp_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sms_message_tmp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_message_tmp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sms_message_tmp_id_seq OWNED BY sms_message_tmp.id;


--
-- Name: sms_status; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sms_status (
    id integer NOT NULL,
    name text NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    sms_tracker_id integer NOT NULL
);


--
-- Name: sms_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sms_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sms_status_id_seq OWNED BY sms_status.id;


--
-- Name: sms_tracker; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sms_tracker (
    id integer NOT NULL,
    message text NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    tracker_id integer NOT NULL,
    network_supplier_id integer,
    send_type text
);


--
-- Name: sms_tracker_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sms_tracker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_tracker_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sms_tracker_id_seq OWNED BY sms_tracker.id;


--
-- Name: state; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE state (
    id integer NOT NULL,
    uf text NOT NULL,
    name text NOT NULL,
    country_id integer NOT NULL
);


--
-- Name: state_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE state_id_seq OWNED BY state.id;


--
-- Name: tracker; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker (
    id integer NOT NULL,
    imei bigint NOT NULL,
    iccid text NOT NULL,
    phone_number text,
    vehicle_id integer,
    business_unity_id integer,
    tracker_status_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    customer_id integer,
    network_supplier_id integer,
    tracker_model_id integer
);


--
-- Name: tracker_command_layout; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_command_layout (
    id integer NOT NULL,
    command text NOT NULL,
    is_active boolean DEFAULT true,
    created_at timestamp without time zone DEFAULT now(),
    name text NOT NULL,
    business_unity_id integer
);


--
-- Name: tracker_command_layout_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_command_layout_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_command_layout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_command_layout_id_seq OWNED BY tracker_command_layout.id;


--
-- Name: tracker_command_queue; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_command_queue (
    id integer NOT NULL,
    tracker_command_layout_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    tracker_id integer NOT NULL,
    response text,
    updated_at timestamp without time zone
);


--
-- Name: tracker_command_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_command_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_command_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_command_queue_id_seq OWNED BY tracker_command_queue.id;


--
-- Name: tracker_event; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_event (
    id integer NOT NULL,
    tracker_id integer NOT NULL,
    event_date timestamp without time zone NOT NULL,
    information text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    event_type_id integer NOT NULL,
    vehicle_id integer
);


--
-- Name: tracker_event_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_event_id_seq OWNED BY tracker_event.id;


--
-- Name: tracker_firmware_information; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_firmware_information (
    id integer NOT NULL,
    version numeric NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    is_active boolean,
    private_path text,
    version_hash text NOT NULL
);


--
-- Name: tracker_firmware_information_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_firmware_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_firmware_information_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_firmware_information_id_seq OWNED BY tracker_firmware_information.id;


--
-- Name: tracker_reason_generator; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_reason_generator (
    id integer NOT NULL,
    code integer,
    reason text,
    description text
);


--
-- Name: tracker_generator_reason_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_generator_reason_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_generator_reason_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_generator_reason_id_seq OWNED BY tracker_reason_generator.id;


--
-- Name: tracker_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_id_seq OWNED BY tracker.id;


--
-- Name: tracker_model; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_model (
    id integer NOT NULL,
    name text NOT NULL,
    description text
);


--
-- Name: tracker_model_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_model_id_seq OWNED BY tracker_model.id;


--
-- Name: tracker_position; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_position (
    id integer NOT NULL,
    lat double precision NOT NULL,
    lng double precision NOT NULL,
    event_date timestamp without time zone,
    tracker_id integer NOT NULL,
    speed double precision,
    sat_number integer,
    hdop integer,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    vehicle_id integer,
    battery_voltage numeric,
    battery_voltage_bkp numeric,
    hourmeter numeric,
    imei numeric,
    message_size bigint,
    odometer numeric,
    position_counter integer,
    temperature numeric,
    package_type integer,
    pcb_version numeric,
    firmware_version double precision,
    position_amount integer,
    position_size bigint,
    inputs bit varying,
    outputs bit varying,
    tracker_reason_generator_code integer,
    "precision" text,
    position_type text,
    lbs_raw text,
    original_message text
);


--
-- Name: tracker_position_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_position_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_position_id_seq OWNED BY tracker_position.id;


--
-- Name: tracker_sms_position; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_sms_position (
    id integer NOT NULL,
    tracker_id integer NOT NULL,
    iccid numeric,
    header text,
    imei numeric,
    batery_voltage numeric,
    mcc integer,
    mnc integer,
    lac_1 integer,
    cellid_1 integer,
    rxlevel_1 integer,
    lac_2 integer,
    cellid_2 integer,
    rxlevel_2 integer,
    lac_3 integer,
    cellid_3 integer,
    rxlevel_3 integer,
    lac_4 integer,
    cellid_4 integer,
    rxlevel_4 integer,
    lac_5 integer,
    cellid_5 integer,
    rxlevel_5 integer,
    lac_6 integer,
    cellid_6 integer,
    rxlevel_6 integer,
    time_advance integer,
    cycle boolean,
    tx_jammer boolean,
    mode_save boolean,
    ala_6 boolean,
    emergency_mode boolean,
    ta_mode boolean,
    rf boolean,
    firmware_version text,
    event_date timestamp without time zone,
    created_at timestamp without time zone DEFAULT now(),
    sender text,
    receiver text,
    vehicle_id integer
);


--
-- Name: tracker_sms_position_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_sms_position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_sms_position_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_sms_position_id_seq OWNED BY tracker_sms_position.id;


--
-- Name: tracker_sms_raw; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_sms_raw (
    id integer NOT NULL,
    imei numeric NOT NULL,
    iccid numeric,
    message text,
    created_at timestamp without time zone DEFAULT now(),
    tracker_sms_position_id integer
);


--
-- Name: tracker_sms_raw_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_sms_raw_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_sms_raw_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_sms_raw_id_seq OWNED BY tracker_sms_raw.id;


--
-- Name: tracker_status; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tracker_status (
    id integer NOT NULL,
    status text NOT NULL,
    description text
);


--
-- Name: tracker_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tracker_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracker_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tracker_status_id_seq OWNED BY tracker_status.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    birthdate date,
    cpf numeric,
    gender gender,
    phone text,
    mobile_phone text,
    password text NOT NULL,
    reset_password_key text,
    is_active boolean,
    has_profile boolean,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: user_address; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_address (
    user_id integer NOT NULL,
    address_id integer NOT NULL
);


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_role; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_role (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


--
-- Name: user_session; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_session (
    id integer NOT NULL,
    user_id integer NOT NULL,
    api_key text,
    valid_for_ip text,
    valid_until timestamp without time zone DEFAULT (now() + '1 day'::interval) NOT NULL,
    ts_created timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: user_session_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_session_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE user_session_id_seq OWNED BY user_session.id;


--
-- Name: vehicle; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE vehicle (
    id integer NOT NULL,
    vehicle_model_id integer NOT NULL,
    vehicle_color_id integer,
    km double precision,
    manufacture_year integer,
    model_year integer,
    fuel_type_id integer,
    car_plate text NOT NULL,
    doors_number integer NOT NULL,
    city_id integer NOT NULL,
    insurance_company_id integer,
    vehicle_fleet_id integer,
    created_at timestamp without time zone DEFAULT now(),
    business_unity_id integer NOT NULL,
    customer_id integer
);


--
-- Name: vehicle_brand; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE vehicle_brand (
    id integer NOT NULL,
    brand text NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    fipe_id integer
);


--
-- Name: vehicle_brand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE vehicle_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vehicle_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE vehicle_brand_id_seq OWNED BY vehicle_brand.id;


--
-- Name: vehicle_color; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE vehicle_color (
    id integer NOT NULL,
    color text,
    created_at timestamp without time zone DEFAULT now()
);


--
-- Name: vehicle_color_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE vehicle_color_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vehicle_color_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE vehicle_color_id_seq OWNED BY vehicle_color.id;


--
-- Name: vehicle_document; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE vehicle_document (
    id integer NOT NULL,
    link text,
    valid_date date NOT NULL,
    document_type_id integer NOT NULL,
    is_valid boolean,
    vehicle_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


--
-- Name: vehicle_document_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE vehicle_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vehicle_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE vehicle_document_id_seq OWNED BY vehicle_document.id;


--
-- Name: vehicle_fleet; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE vehicle_fleet (
    id integer NOT NULL,
    business_unity_id integer NOT NULL,
    name text,
    created_at timestamp without time zone DEFAULT now(),
    customer_id integer
);


--
-- Name: vehicle_fleet_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE vehicle_fleet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vehicle_fleet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE vehicle_fleet_id_seq OWNED BY vehicle_fleet.id;


--
-- Name: vehicle_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE vehicle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vehicle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE vehicle_id_seq OWNED BY vehicle.id;


--
-- Name: vehicle_model; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE vehicle_model (
    id integer NOT NULL,
    model text NOT NULL,
    vehicle_brand_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    fipe_id text
);


--
-- Name: vehicle_model_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE vehicle_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vehicle_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE vehicle_model_id_seq OWNED BY vehicle_model.id;


--
-- Name: vehicle_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE vehicle_user (
    vehicle_id integer NOT NULL,
    user_id integer NOT NULL,
    is_driver boolean,
    is_owner boolean
);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY actions_log ALTER COLUMN id SET DEFAULT nextval('actions_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY business_unity ALTER COLUMN id SET DEFAULT nextval('business_unity_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY business_unity_users ALTER COLUMN id SET DEFAULT nextval('business_unity_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cep_cache ALTER COLUMN id SET DEFAULT nextval('cep_cache_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY city ALTER COLUMN id SET DEFAULT nextval('city_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY country ALTER COLUMN id SET DEFAULT nextval('country_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer ALTER COLUMN id SET DEFAULT nextval('customer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_users ALTER COLUMN id SET DEFAULT nextval('customer_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY document_type ALTER COLUMN id SET DEFAULT nextval('document_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY erb ALTER COLUMN id SET DEFAULT nextval('erb_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY erb_position ALTER COLUMN id SET DEFAULT nextval('erb_position_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY event_type ALTER COLUMN id SET DEFAULT nextval('event_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fuel_type ALTER COLUMN id SET DEFAULT nextval('fuel_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY insurance_company ALTER COLUMN id SET DEFAULT nextval('insurance_company_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY network_supplier ALTER COLUMN id SET DEFAULT nextval('network_supplier_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY position_cache ALTER COLUMN id SET DEFAULT nextval('position_cache_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sms_message_tmp ALTER COLUMN id SET DEFAULT nextval('sms_message_tmp_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sms_status ALTER COLUMN id SET DEFAULT nextval('sms_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sms_tracker ALTER COLUMN id SET DEFAULT nextval('sms_tracker_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY state ALTER COLUMN id SET DEFAULT nextval('state_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker ALTER COLUMN id SET DEFAULT nextval('tracker_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_command_layout ALTER COLUMN id SET DEFAULT nextval('tracker_command_layout_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_command_queue ALTER COLUMN id SET DEFAULT nextval('tracker_command_queue_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_event ALTER COLUMN id SET DEFAULT nextval('tracker_event_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_firmware_information ALTER COLUMN id SET DEFAULT nextval('tracker_firmware_information_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_model ALTER COLUMN id SET DEFAULT nextval('tracker_model_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_position ALTER COLUMN id SET DEFAULT nextval('tracker_position_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_reason_generator ALTER COLUMN id SET DEFAULT nextval('tracker_generator_reason_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_sms_position ALTER COLUMN id SET DEFAULT nextval('tracker_sms_position_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_sms_raw ALTER COLUMN id SET DEFAULT nextval('tracker_sms_raw_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_status ALTER COLUMN id SET DEFAULT nextval('tracker_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_session ALTER COLUMN id SET DEFAULT nextval('user_session_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle ALTER COLUMN id SET DEFAULT nextval('vehicle_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_brand ALTER COLUMN id SET DEFAULT nextval('vehicle_brand_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_color ALTER COLUMN id SET DEFAULT nextval('vehicle_color_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_document ALTER COLUMN id SET DEFAULT nextval('vehicle_document_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_fleet ALTER COLUMN id SET DEFAULT nextval('vehicle_fleet_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_model ALTER COLUMN id SET DEFAULT nextval('vehicle_model_id_seq'::regclass);


--
-- Name: actions_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY actions_log
    ADD CONSTRAINT actions_log_pkey PRIMARY KEY (id);


--
-- Name: address_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: business_unity_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY business_unity
    ADD CONSTRAINT business_unity_pkey PRIMARY KEY (id);


--
-- Name: business_unity_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY business_unity_users
    ADD CONSTRAINT business_unity_users_pkey PRIMARY KEY (id);


--
-- Name: cep_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cep_cache
    ADD CONSTRAINT cep_cache_pkey PRIMARY KEY (id);


--
-- Name: city_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- Name: coutry_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT coutry_pkey PRIMARY KEY (id);


--
-- Name: customer_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: customer_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY customer_users
    ADD CONSTRAINT customer_users_pkey PRIMARY KEY (id);


--
-- Name: document_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY document_type
    ADD CONSTRAINT document_type_pkey PRIMARY KEY (id);


--
-- Name: erb_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY erb
    ADD CONSTRAINT erb_pkey PRIMARY KEY (id);


--
-- Name: erb_position_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY erb_position
    ADD CONSTRAINT erb_position_pkey PRIMARY KEY (id);


--
-- Name: erb_position_source_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY erb_position
    ADD CONSTRAINT erb_position_source_id_key UNIQUE (source_id, erb_id, latitude, longitude, azimuth, radius);


--
-- Name: erb_unique_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY erb
    ADD CONSTRAINT erb_unique_key UNIQUE (mcc, mnc, lac, cid);


--
-- Name: event_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY event_type
    ADD CONSTRAINT event_type_pkey PRIMARY KEY (id);


--
-- Name: fuel_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fuel_type
    ADD CONSTRAINT fuel_type_pkey PRIMARY KEY (id);


--
-- Name: geolocation_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY geolocation_cache
    ADD CONSTRAINT geolocation_cache_pkey PRIMARY KEY (cache_key);


--
-- Name: insurance_company_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY insurance_company
    ADD CONSTRAINT insurance_company_pkey PRIMARY KEY (id);


--
-- Name: network_supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY network_supplier
    ADD CONSTRAINT network_supplier_pkey PRIMARY KEY (id);


--
-- Name: position_cache_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY position_cache
    ADD CONSTRAINT position_cache_pkey PRIMARY KEY (id);


--
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: sms_message_tmp_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sms_message_tmp
    ADD CONSTRAINT sms_message_tmp_pkey PRIMARY KEY (id);


--
-- Name: sms_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sms_status
    ADD CONSTRAINT sms_status_pkey PRIMARY KEY (id);


--
-- Name: sms_tracker_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sms_tracker
    ADD CONSTRAINT sms_tracker_pkey PRIMARY KEY (id);


--
-- Name: state_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_pkey PRIMARY KEY (id);


--
-- Name: tracker_command_layout_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_command_layout
    ADD CONSTRAINT tracker_command_layout_pkey PRIMARY KEY (id);


--
-- Name: tracker_command_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_command_queue
    ADD CONSTRAINT tracker_command_queue_pkey PRIMARY KEY (id);


--
-- Name: tracker_event_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_event
    ADD CONSTRAINT tracker_event_pkey PRIMARY KEY (id);


--
-- Name: tracker_firmware_information_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_firmware_information
    ADD CONSTRAINT tracker_firmware_information_pkey PRIMARY KEY (id);


--
-- Name: tracker_generator_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_reason_generator
    ADD CONSTRAINT tracker_generator_reason_pkey PRIMARY KEY (id);


--
-- Name: tracker_model_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_model
    ADD CONSTRAINT tracker_model_pkey PRIMARY KEY (id);


--
-- Name: tracker_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_pkey PRIMARY KEY (id);


--
-- Name: tracker_position_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_position
    ADD CONSTRAINT tracker_position_pkey PRIMARY KEY (id);


--
-- Name: tracker_reason_generator_code_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_reason_generator
    ADD CONSTRAINT tracker_reason_generator_code_key UNIQUE (code);


--
-- Name: tracker_sms_position_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_sms_position
    ADD CONSTRAINT tracker_sms_position_pkey PRIMARY KEY (id);


--
-- Name: tracker_sms_raw_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_sms_raw
    ADD CONSTRAINT tracker_sms_raw_pkey PRIMARY KEY (id);


--
-- Name: tracker_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tracker_status
    ADD CONSTRAINT tracker_status_pkey PRIMARY KEY (id);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_session_api_key_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--
 
ALTER TABLE ONLY user_session
    ADD CONSTRAINT user_session_api_key_key UNIQUE (api_key);


--
-- Name: user_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_session
    ADD CONSTRAINT user_session_pkey PRIMARY KEY (id);


--
-- Name: vehicle_brand_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY vehicle_brand
    ADD CONSTRAINT vehicle_brand_pkey PRIMARY KEY (id);


--
-- Name: vehicle_color_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY vehicle_color
    ADD CONSTRAINT vehicle_color_pkey PRIMARY KEY (id);


--
-- Name: vehicle_document_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY vehicle_document
    ADD CONSTRAINT vehicle_document_pkey PRIMARY KEY (id);


--
-- Name: vehicle_fleet_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY vehicle_fleet
    ADD CONSTRAINT vehicle_fleet_pkey PRIMARY KEY (id);


--
-- Name: vehicle_model_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY vehicle_model
    ADD CONSTRAINT vehicle_model_pkey PRIMARY KEY (id);


--
-- Name: vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_pkey PRIMARY KEY (id);


--
-- Name: idx_erb_position_last_check_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_erb_position_last_check_idx ON erb_position USING btree (last_check);


--
-- Name: tracker_positions_tracker_id__idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX tracker_positions_tracker_id__idx ON tracker_position USING btree (tracker_id);


--
-- Name: actions_log_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY actions_log
    ADD CONSTRAINT actions_log_fk_user_id FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: address_city_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_city_id_fkey FOREIGN KEY (city_id) REFERENCES city(id);


--
-- Name: business_unity_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY business_unity
    ADD CONSTRAINT business_unity_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: business_unity_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY business_unity
    ADD CONSTRAINT business_unity_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES business_unity(id);


--
-- Name: business_unity_users_business_unity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY business_unity_users
    ADD CONSTRAINT business_unity_users_business_unity_id_fkey FOREIGN KEY (business_unity_id) REFERENCES business_unity(id);


--
-- Name: business_unity_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY business_unity_users
    ADD CONSTRAINT business_unity_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: cep_cache_fk_city_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cep_cache
    ADD CONSTRAINT cep_cache_fk_city_id FOREIGN KEY (city_id) REFERENCES city(id);


--
-- Name: cep_cache_fk_state_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cep_cache
    ADD CONSTRAINT cep_cache_fk_state_id FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: city_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: city_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY city
    ADD CONSTRAINT city_state_id_fkey FOREIGN KEY (state_id) REFERENCES state(id);


--
-- Name: customer_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: customer_business_unity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_business_unity_id_fkey FOREIGN KEY (business_unity_id) REFERENCES business_unity(id);


--
-- Name: customer_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES customer(id);


--
-- Name: customer_users_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_users
    ADD CONSTRAINT customer_users_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: customer_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY customer_users
    ADD CONSTRAINT customer_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: erb_position_erb_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY erb_position
    ADD CONSTRAINT erb_position_erb_id_fkey FOREIGN KEY (erb_id) REFERENCES erb(id) ON DELETE CASCADE;


--
-- Name: sms_status_sms_tracker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sms_status
    ADD CONSTRAINT sms_status_sms_tracker_id_fkey FOREIGN KEY (sms_tracker_id) REFERENCES sms_tracker(id);


--
-- Name: sms_tracker_network_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sms_tracker
    ADD CONSTRAINT sms_tracker_network_supplier_id_fkey FOREIGN KEY (network_supplier_id) REFERENCES network_supplier(id);


--
-- Name: sms_tracker_tracker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sms_tracker
    ADD CONSTRAINT sms_tracker_tracker_id_fkey FOREIGN KEY (tracker_id) REFERENCES tracker(id);


--
-- Name: state_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(id);


--
-- Name: tracker_business_unity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_business_unity_id_fkey FOREIGN KEY (business_unity_id) REFERENCES business_unity(id);


--
-- Name: tracker_command_layout_business_unity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_command_layout
    ADD CONSTRAINT tracker_command_layout_business_unity_id_fkey FOREIGN KEY (business_unity_id) REFERENCES business_unity(id);


--
-- Name: tracker_command_queue_tracker_command_layout_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_command_queue
    ADD CONSTRAINT tracker_command_queue_tracker_command_layout_id_fkey FOREIGN KEY (tracker_command_layout_id) REFERENCES tracker_command_layout(id);


--
-- Name: tracker_command_queue_tracker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_command_queue
    ADD CONSTRAINT tracker_command_queue_tracker_id_fkey FOREIGN KEY (tracker_id) REFERENCES tracker(id);


--
-- Name: tracker_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: tracker_event_event_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_event
    ADD CONSTRAINT tracker_event_event_type_id_fkey FOREIGN KEY (event_type_id) REFERENCES event_type(id);


--
-- Name: tracker_event_tracker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_event
    ADD CONSTRAINT tracker_event_tracker_id_fkey FOREIGN KEY (tracker_id) REFERENCES tracker(id);


--
-- Name: tracker_event_vehicle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_event
    ADD CONSTRAINT tracker_event_vehicle_id_fkey FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);


--
-- Name: tracker_network_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_network_supplier_id_fkey FOREIGN KEY (network_supplier_id) REFERENCES network_supplier(id);


--
-- Name: tracker_position_tracker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_position
    ADD CONSTRAINT tracker_position_tracker_id_fkey FOREIGN KEY (tracker_id) REFERENCES tracker(id);


--
-- Name: tracker_position_tracker_reason_generator_code_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_position
    ADD CONSTRAINT tracker_position_tracker_reason_generator_code_fkey FOREIGN KEY (tracker_reason_generator_code) REFERENCES tracker_reason_generator(code);


--
-- Name: tracker_position_vehicle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_position
    ADD CONSTRAINT tracker_position_vehicle_id_fkey FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);


--
-- Name: tracker_sms_position_tracker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_sms_position
    ADD CONSTRAINT tracker_sms_position_tracker_id_fkey FOREIGN KEY (tracker_id) REFERENCES tracker(id);


--
-- Name: tracker_sms_position_vehicle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_sms_position
    ADD CONSTRAINT tracker_sms_position_vehicle_id_fkey FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);


--
-- Name: tracker_sms_raw_tracker_sms_position_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker_sms_raw
    ADD CONSTRAINT tracker_sms_raw_tracker_sms_position_id_fkey FOREIGN KEY (tracker_sms_position_id) REFERENCES tracker_sms_position(id);


--
-- Name: tracker_tracker_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_tracker_model_id_fkey FOREIGN KEY (tracker_model_id) REFERENCES tracker_model(id);


--
-- Name: tracker_tracker_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_tracker_status_id_fkey FOREIGN KEY (tracker_status_id) REFERENCES tracker_status(id);


--
-- Name: tracker_vehicle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_vehicle_id_fkey FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);


--
-- Name: user_address_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_address
    ADD CONSTRAINT user_address_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: user_address_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_address
    ADD CONSTRAINT user_address_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: user_roles_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES role(id);


--
-- Name: user_roles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: user_session_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_session
    ADD CONSTRAINT user_session_fk_user_id FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: vehicle_business_unity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_business_unity_id_fkey FOREIGN KEY (business_unity_id) REFERENCES business_unity(id);


--
-- Name: vehicle_city_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_city_id_fkey FOREIGN KEY (city_id) REFERENCES city(id);


--
-- Name: vehicle_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: vehicle_document_document_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_document
    ADD CONSTRAINT vehicle_document_document_type_id_fkey FOREIGN KEY (document_type_id) REFERENCES document_type(id);


--
-- Name: vehicle_document_vehicle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_document
    ADD CONSTRAINT vehicle_document_vehicle_id_fkey FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);


--
-- Name: vehicle_fleet_business_unity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_fleet
    ADD CONSTRAINT vehicle_fleet_business_unity_id_fkey FOREIGN KEY (business_unity_id) REFERENCES business_unity(id);


--
-- Name: vehicle_fleet_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_fleet
    ADD CONSTRAINT vehicle_fleet_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: vehicle_fuel_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_fuel_type_id_fkey FOREIGN KEY (fuel_type_id) REFERENCES fuel_type(id);


--
-- Name: vehicle_insurance_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_insurance_company_id_fkey FOREIGN KEY (insurance_company_id) REFERENCES insurance_company(id);


--
-- Name: vehicle_model_brand_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_model
    ADD CONSTRAINT vehicle_model_brand_id_fkey FOREIGN KEY (vehicle_brand_id) REFERENCES vehicle_brand(id);


--
-- Name: vehicle_user_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_user
    ADD CONSTRAINT vehicle_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: vehicle_user_vehicle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle_user
    ADD CONSTRAINT vehicle_user_vehicle_id_fkey FOREIGN KEY (vehicle_id) REFERENCES vehicle(id);


--
-- Name: vehicle_vehicle_color_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_vehicle_color_id_fkey FOREIGN KEY (vehicle_color_id) REFERENCES vehicle_color(id);


--
-- Name: vehicle_vehicle_fleet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_vehicle_fleet_id_fkey FOREIGN KEY (vehicle_fleet_id) REFERENCES vehicle_fleet(id);


--
-- Name: vehicle_vehicle_model_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY vehicle
    ADD CONSTRAINT vehicle_vehicle_model_id_fkey FOREIGN KEY (vehicle_model_id) REFERENCES vehicle_model(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

COMMIT;
