-- Deploy trackware:business-unity-api-key to pg
-- requires: multitrack

BEGIN;


CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

ALTER TABLE business_unity ADD COLUMN api_key uuid UNIQUE NOT NULL DEFAULT uuid_generate_v4();


COMMIT;
