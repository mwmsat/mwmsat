-- Deploy tracker_position_address

BEGIN;

ALTER TABLE tracker_position ADD COLUMN address TEXT;

COMMIT;
