-- Deploy trackware:tracker_rf_note to pg

BEGIN;

ALTER TABLE tracker ADD COLUMN rf_note TEXT;

COMMIT;
