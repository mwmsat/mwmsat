-- Deploy delivery_status_url
-- requires: appschema

BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

ALTER TABLE sms_tracker ADD COLUMN receipt_check_url TEXT;
ALTER TABLE sms_tracker ADD COLUMN message_uuid uuid UNIQUE NOT NULL  DEFAULT uuid_generate_v4();

COMMIT;
