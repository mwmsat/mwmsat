-- Deploy trackware:tracker-notification-sms-programming-confirmation to pg

BEGIN;

ALTER TABLE tracker_notification_sms
  ADD COLUMN ack_config_position_id INTEGER REFERENCES tracker_position(id);
  
ALTER TABLE ONLY tracker_notification_sms
  ADD CONSTRAINT tracker_notification_sms_pk
  PRIMARY KEY (notification_id, tracker_id, sms_tracker_id);  

COMMIT;
