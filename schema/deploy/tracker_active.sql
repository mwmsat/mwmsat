-- Deploy trackware:tracker_active to pg

BEGIN;

ALTER TABLE tracker ADD COLUMN active BOOLEAN DEFAULT TRUE;

COMMIT;
