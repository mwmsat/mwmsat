-- Deploy addr_data

BEGIN;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET client_encoding = 'UTF8';


--
-- Data for Name: city; Type: TABLE DATA; Schema: public; Owner: -
--

COPY city (id, state_id, name, name_url, created_at, country_id) FROM stdin;
9278	25	São Paulo	sao-paulo	2013-09-03 16:04:39.338617	1
1	1	Acrelândia	acrelandia	2013-09-03 15:30:12.389704	1
2	1	Assis Brasil	assis-brasil	2013-09-03 15:30:12.389704	1
844	6	Ipu	ipu	2013-09-03 15:30:12.389704	1
3	1	Brasiléia	brasileia	2013-09-03 15:30:12.389704	1
4	1	Bujari	bujari	2013-09-03 15:30:12.389704	1
5	1	Capixaba	capixaba	2013-09-03 15:30:12.389704	1
6	1	Cruzeiro do Sul	cruzeiro-do-sul	2013-09-03 15:30:12.389704	1
7	1	Epitaciolândia	epitaciolandia	2013-09-03 15:30:12.389704	1
8	1	Feijó	feijo	2013-09-03 15:30:12.389704	1
9	1	Jordão	jordao	2013-09-03 15:30:12.389704	1
10	1	Mâncio Lima	mancio-lima	2013-09-03 15:30:12.389704	1
11	1	Manoel Urbano	manoel-urbano	2013-09-03 15:30:12.389704	1
12	1	Marechal Thaumaturgo	marechal-thaumaturgo	2013-09-03 15:30:12.389704	1
13	1	Plácido de Castro	placido-de-castro	2013-09-03 15:30:12.389704	1
14	1	Porto Acre	porto-acre	2013-09-03 15:30:12.389704	1
15	1	Porto Walter	porto-walter	2013-09-03 15:30:12.389704	1
16	1	Rio Branco	rio-branco	2013-09-03 15:30:12.389704	1
17	1	Rodrigues Alves	rodrigues-alves	2013-09-03 15:30:12.389704	1
18	1	Santa Rosa do Purus	santa-rosa-do-purus	2013-09-03 15:30:12.389704	1
19	1	Sena Madureira	sena-madureira	2013-09-03 15:30:12.389704	1
20	1	Senador Guiomard	senador-guiomard	2013-09-03 15:30:12.389704	1
21	1	Tarauacá	tarauaca	2013-09-03 15:30:12.389704	1
22	1	Xapuri	xapuri	2013-09-03 15:30:12.389704	1
32	2	Água Branca	agua-branca	2013-09-03 15:30:12.389704	1
33	2	Anadia	anadia	2013-09-03 15:30:12.389704	1
34	2	Arapiraca	arapiraca	2013-09-03 15:30:12.389704	1
35	2	Atalaia	atalaia	2013-09-03 15:30:12.389704	1
36	2	Barra de Santo Antônio	barra-de-santo-antonio	2013-09-03 15:30:12.389704	1
37	2	Barra de São Miguel	barra-de-sao-miguel	2013-09-03 15:30:12.389704	1
38	2	Batalha	batalha	2013-09-03 15:30:12.389704	1
39	2	Belém	belem	2013-09-03 15:30:12.389704	1
40	2	Belo Monte	belo-monte	2013-09-03 15:30:12.389704	1
41	2	Boca da Mata	boca-da-mata	2013-09-03 15:30:12.389704	1
42	2	Branquinha	branquinha	2013-09-03 15:30:12.389704	1
43	2	Cacimbinhas	cacimbinhas	2013-09-03 15:30:12.389704	1
44	2	Cajueiro	cajueiro	2013-09-03 15:30:12.389704	1
45	2	Campestre	campestre	2013-09-03 15:30:12.389704	1
46	2	Campo Alegre	campo-alegre	2013-09-03 15:30:12.389704	1
47	2	Campo Grande	campo-grande	2013-09-03 15:30:12.389704	1
48	2	Canapi	canapi	2013-09-03 15:30:12.389704	1
49	2	Capela	capela	2013-09-03 15:30:12.389704	1
50	2	Carneiros	carneiros	2013-09-03 15:30:12.389704	1
51	2	Chã Preta	cha-preta	2013-09-03 15:30:12.389704	1
52	2	Coité do Nóia	coite-do-noia	2013-09-03 15:30:12.389704	1
53	2	Colônia Leopoldina	colonia-leopoldina	2013-09-03 15:30:12.389704	1
54	2	Coqueiro Seco	coqueiro-seco	2013-09-03 15:30:12.389704	1
55	2	Coruripe	coruripe	2013-09-03 15:30:12.389704	1
56	2	Craíbas	craibas	2013-09-03 15:30:12.389704	1
57	2	Delmiro Gouveia	delmiro-gouveia	2013-09-03 15:30:12.389704	1
58	2	Dois Riachos	dois-riachos	2013-09-03 15:30:12.389704	1
59	2	Estrela de Alagoas	estrela-de-alagoas	2013-09-03 15:30:12.389704	1
60	2	Feira Grande	feira-grande	2013-09-03 15:30:12.389704	1
61	2	Feliz Deserto	feliz-deserto	2013-09-03 15:30:12.389704	1
62	2	Flexeiras	flexeiras	2013-09-03 15:30:12.389704	1
63	2	Girau do Ponciano	girau-do-ponciano	2013-09-03 15:30:12.389704	1
64	2	Ibateguara	ibateguara	2013-09-03 15:30:12.389704	1
65	2	Igaci	igaci	2013-09-03 15:30:12.389704	1
66	2	Igreja Nova	igreja-nova	2013-09-03 15:30:12.389704	1
67	2	Inhapi	inhapi	2013-09-03 15:30:12.389704	1
68	2	Jacaré dos Homens	jacare-dos-homens	2013-09-03 15:30:12.389704	1
69	2	Jacuípe	jacuipe	2013-09-03 15:30:12.389704	1
70	2	Japaratinga	japaratinga	2013-09-03 15:30:12.389704	1
71	2	Jaramataia	jaramataia	2013-09-03 15:30:12.389704	1
72	2	Jequiá da Praia	jequia-da-praia	2013-09-03 15:30:12.389704	1
73	2	Joaquim Gomes	joaquim-gomes	2013-09-03 15:30:12.389704	1
74	2	Jundiá	jundia	2013-09-03 15:30:12.389704	1
75	2	Junqueiro	junqueiro	2013-09-03 15:30:12.389704	1
76	2	Lagoa da Canoa	lagoa-da-canoa	2013-09-03 15:30:12.389704	1
77	2	Limoeiro de Anadia	limoeiro-de-anadia	2013-09-03 15:30:12.389704	1
78	2	Maceió	maceio	2013-09-03 15:30:12.389704	1
79	2	Major Isidoro	major-isidoro	2013-09-03 15:30:12.389704	1
80	2	Mar Vermelho	mar-vermelho	2013-09-03 15:30:12.389704	1
81	2	Maragogi	maragogi	2013-09-03 15:30:12.389704	1
82	2	Maravilha	maravilha	2013-09-03 15:30:12.389704	1
83	2	Marechal Deodoro	marechal-deodoro	2013-09-03 15:30:12.389704	1
84	2	Maribondo	maribondo	2013-09-03 15:30:12.389704	1
85	2	Mata Grande	mata-grande	2013-09-03 15:30:12.389704	1
86	2	Matriz de Camaragibe	matriz-de-camaragibe	2013-09-03 15:30:12.389704	1
87	2	Messias	messias	2013-09-03 15:30:12.389704	1
88	2	Minador do Negrão	minador-do-negrao	2013-09-03 15:30:12.389704	1
89	2	Monteirópolis	monteiropolis	2013-09-03 15:30:12.389704	1
90	2	Murici	murici	2013-09-03 15:30:12.389704	1
91	2	Novo Lino	novo-lino	2013-09-03 15:30:12.389704	1
92	2	Olho d`Água das Flores	olho-d-agua-das-flores	2013-09-03 15:30:12.389704	1
93	2	Olho d`Água do Casado	olho-d-agua-do-casado	2013-09-03 15:30:12.389704	1
94	2	Olho d`Água Grande	olho-d-agua-grande	2013-09-03 15:30:12.389704	1
95	2	Olivença	olivenca	2013-09-03 15:30:12.389704	1
96	2	Ouro Branco	ouro-branco	2013-09-03 15:30:12.389704	1
97	2	Palestina	palestina	2013-09-03 15:30:12.389704	1
98	2	Palmeira dos Índios	palmeira-dos-indios	2013-09-03 15:30:12.389704	1
99	2	Pão de Açúcar	pao-de-acucar	2013-09-03 15:30:12.389704	1
100	2	Pariconha	pariconha	2013-09-03 15:30:12.389704	1
101	2	Paripueira	paripueira	2013-09-03 15:30:12.389704	1
102	2	Passo de Camaragibe	passo-de-camaragibe	2013-09-03 15:30:12.389704	1
103	2	Paulo Jacinto	paulo-jacinto	2013-09-03 15:30:12.389704	1
104	2	Penedo	penedo	2013-09-03 15:30:12.389704	1
105	2	Piaçabuçu	piacabucu	2013-09-03 15:30:12.389704	1
106	2	Pilar	pilar	2013-09-03 15:30:12.389704	1
107	2	Pindoba	pindoba	2013-09-03 15:30:12.389704	1
108	2	Piranhas	piranhas	2013-09-03 15:30:12.389704	1
2362	11	Nobres	nobres	2013-09-03 15:30:12.389704	1
109	2	Poço das Trincheiras	poco-das-trincheiras	2013-09-03 15:30:12.389704	1
110	2	Porto Calvo	porto-calvo	2013-09-03 15:30:12.389704	1
111	2	Porto de Pedras	porto-de-pedras	2013-09-03 15:30:12.389704	1
112	2	Porto Real do Colégio	porto-real-do-colegio	2013-09-03 15:30:12.389704	1
113	2	Quebrangulo	quebrangulo	2013-09-03 15:30:12.389704	1
114	2	Rio Largo	rio-largo	2013-09-03 15:30:12.389704	1
115	2	Roteiro	roteiro	2013-09-03 15:30:12.389704	1
116	2	Santa Luzia do Norte	santa-luzia-do-norte	2013-09-03 15:30:12.389704	1
117	2	Santana do Ipanema	santana-do-ipanema	2013-09-03 15:30:12.389704	1
118	2	Santana do Mundaú	santana-do-mundau	2013-09-03 15:30:12.389704	1
119	2	São Brás	sao-bras	2013-09-03 15:30:12.389704	1
120	2	São José da Laje	sao-jose-da-laje	2013-09-03 15:30:12.389704	1
121	2	São José da Tapera	sao-jose-da-tapera	2013-09-03 15:30:12.389704	1
122	2	São Luís do Quitunde	sao-luis-do-quitunde	2013-09-03 15:30:12.389704	1
123	2	São Miguel dos Campos	sao-miguel-dos-campos	2013-09-03 15:30:12.389704	1
124	2	São Miguel dos Milagres	sao-miguel-dos-milagres	2013-09-03 15:30:12.389704	1
125	2	São Sebastião	sao-sebastiao	2013-09-03 15:30:12.389704	1
126	2	Satuba	satuba	2013-09-03 15:30:12.389704	1
127	2	Senador Rui Palmeira	senador-rui-palmeira	2013-09-03 15:30:12.389704	1
128	2	Tanque d`Arca	tanque-d-arca	2013-09-03 15:30:12.389704	1
129	2	Taquarana	taquarana	2013-09-03 15:30:12.389704	1
130	2	Teotônio Vilela	teotonio-vilela	2013-09-03 15:30:12.389704	1
131	2	Traipu	traipu	2013-09-03 15:30:12.389704	1
132	2	União dos Palmares	uniao-dos-palmares	2013-09-03 15:30:12.389704	1
133	2	Viçosa	vicosa	2013-09-03 15:30:12.389704	1
159	4	Alvarães	alvaraes	2013-09-03 15:30:12.389704	1
160	4	Amaturá	amatura	2013-09-03 15:30:12.389704	1
161	4	Anamã	anama	2013-09-03 15:30:12.389704	1
162	4	Anori	anori	2013-09-03 15:30:12.389704	1
163	4	Apuí	apui	2013-09-03 15:30:12.389704	1
164	4	Atalaia do Norte	atalaia-do-norte	2013-09-03 15:30:12.389704	1
165	4	Autazes	autazes	2013-09-03 15:30:12.389704	1
166	4	Barcelos	barcelos	2013-09-03 15:30:12.389704	1
167	4	Barreirinha	barreirinha	2013-09-03 15:30:12.389704	1
168	4	Benjamin Constant	benjamin-constant	2013-09-03 15:30:12.389704	1
169	4	Beruri	beruri	2013-09-03 15:30:12.389704	1
170	4	Boa Vista do Ramos	boa-vista-do-ramos	2013-09-03 15:30:12.389704	1
171	4	Boca do Acre	boca-do-acre	2013-09-03 15:30:12.389704	1
172	4	Borba	borba	2013-09-03 15:30:12.389704	1
173	4	Caapiranga	caapiranga	2013-09-03 15:30:12.389704	1
174	4	Canutama	canutama	2013-09-03 15:30:12.389704	1
175	4	Carauari	carauari	2013-09-03 15:30:12.389704	1
176	4	Careiro	careiro	2013-09-03 15:30:12.389704	1
177	4	Careiro da Várzea	careiro-da-varzea	2013-09-03 15:30:12.389704	1
178	4	Coari	coari	2013-09-03 15:30:12.389704	1
179	4	Codajás	codajas	2013-09-03 15:30:12.389704	1
180	4	Eirunepé	eirunepe	2013-09-03 15:30:12.389704	1
181	4	Envira	envira	2013-09-03 15:30:12.389704	1
182	4	Fonte Boa	fonte-boa	2013-09-03 15:30:12.389704	1
183	4	Guajará	guajara	2013-09-03 15:30:12.389704	1
184	4	Humaitá	humaita	2013-09-03 15:30:12.389704	1
185	4	Ipixuna	ipixuna	2013-09-03 15:30:12.389704	1
186	4	Iranduba	iranduba	2013-09-03 15:30:12.389704	1
187	4	Itacoatiara	itacoatiara	2013-09-03 15:30:12.389704	1
188	4	Itamarati	itamarati	2013-09-03 15:30:12.389704	1
189	4	Itapiranga	itapiranga	2013-09-03 15:30:12.389704	1
190	4	Japurá	japura	2013-09-03 15:30:12.389704	1
191	4	Juruá	jurua	2013-09-03 15:30:12.389704	1
192	4	Jutaí	jutai	2013-09-03 15:30:12.389704	1
193	4	Lábrea	labrea	2013-09-03 15:30:12.389704	1
194	4	Manacapuru	manacapuru	2013-09-03 15:30:12.389704	1
195	4	Manaquiri	manaquiri	2013-09-03 15:30:12.389704	1
196	4	Manaus	manaus	2013-09-03 15:30:12.389704	1
197	4	Manicoré	manicore	2013-09-03 15:30:12.389704	1
198	4	Maraã	maraa	2013-09-03 15:30:12.389704	1
199	4	Maués	maues	2013-09-03 15:30:12.389704	1
200	4	Nhamundá	nhamunda	2013-09-03 15:30:12.389704	1
201	4	Nova Olinda do Norte	nova-olinda-do-norte	2013-09-03 15:30:12.389704	1
202	4	Novo Airão	novo-airao	2013-09-03 15:30:12.389704	1
203	4	Novo Aripuanã	novo-aripuana	2013-09-03 15:30:12.389704	1
204	4	Parintins	parintins	2013-09-03 15:30:12.389704	1
205	4	Pauini	pauini	2013-09-03 15:30:12.389704	1
206	4	Presidente Figueiredo	presidente-figueiredo	2013-09-03 15:30:12.389704	1
207	4	Rio Preto da Eva	rio-preto-da-eva	2013-09-03 15:30:12.389704	1
208	4	Santa Isabel do Rio Negro	santa-isabel-do-rio-negro	2013-09-03 15:30:12.389704	1
209	4	Santo Antônio do Içá	santo-antonio-do-ica	2013-09-03 15:30:12.389704	1
210	4	São Gabriel da Cachoeira	sao-gabriel-da-cachoeira	2013-09-03 15:30:12.389704	1
211	4	São Paulo de Olivença	sao-paulo-de-olivenca	2013-09-03 15:30:12.389704	1
212	4	São Sebastião do Uatumã	sao-sebastiao-do-uatuma	2013-09-03 15:30:12.389704	1
213	4	Silves	silves	2013-09-03 15:30:12.389704	1
214	4	Tabatinga	tabatinga	2013-09-03 15:30:12.389704	1
215	4	Tapauá	tapaua	2013-09-03 15:30:12.389704	1
216	4	Tefé	tefe	2013-09-03 15:30:12.389704	1
217	4	Tonantins	tonantins	2013-09-03 15:30:12.389704	1
218	4	Uarini	uarini	2013-09-03 15:30:12.389704	1
219	4	Urucará	urucara	2013-09-03 15:30:12.389704	1
220	4	Urucurituba	urucurituba	2013-09-03 15:30:12.389704	1
222	3	Amapá	amapa	2013-09-03 15:30:12.389704	1
653	5	Una	una	2013-09-03 15:30:12.389704	1
223	3	Calçoene	calcoene	2013-09-03 15:30:12.389704	1
224	3	Cutias	cutias	2013-09-03 15:30:12.389704	1
225	3	Ferreira Gomes	ferreira-gomes	2013-09-03 15:30:12.389704	1
226	3	Itaubal	itaubal	2013-09-03 15:30:12.389704	1
227	3	Laranjal do Jari	laranjal-do-jari	2013-09-03 15:30:12.389704	1
228	3	Macapá	macapa	2013-09-03 15:30:12.389704	1
229	3	Mazagão	mazagao	2013-09-03 15:30:12.389704	1
230	3	Oiapoque	oiapoque	2013-09-03 15:30:12.389704	1
231	3	Pedra Branca do Amaparí	pedra-branca-do-amapari	2013-09-03 15:30:12.389704	1
232	3	Porto Grande	porto-grande	2013-09-03 15:30:12.389704	1
233	3	Pracuúba	pracuuba	2013-09-03 15:30:12.389704	1
234	3	Santana	santana	2013-09-03 15:30:12.389704	1
235	3	Serra do Navio	serra-do-navio	2013-09-03 15:30:12.389704	1
236	3	Tartarugalzinho	tartarugalzinho	2013-09-03 15:30:12.389704	1
237	3	Vitória do Jari	vitoria-do-jari	2013-09-03 15:30:12.389704	1
253	5	Abaíra	abaira	2013-09-03 15:30:12.389704	1
254	5	Abaré	abare	2013-09-03 15:30:12.389704	1
255	5	Acajutiba	acajutiba	2013-09-03 15:30:12.389704	1
256	5	Adustina	adustina	2013-09-03 15:30:12.389704	1
257	5	Água Fria	agua-fria	2013-09-03 15:30:12.389704	1
258	5	Aiquara	aiquara	2013-09-03 15:30:12.389704	1
259	5	Alagoinhas	alagoinhas	2013-09-03 15:30:12.389704	1
260	5	Alcobaça	alcobaca	2013-09-03 15:30:12.389704	1
261	5	Almadina	almadina	2013-09-03 15:30:12.389704	1
262	5	Amargosa	amargosa	2013-09-03 15:30:12.389704	1
263	5	Amélia Rodrigues	amelia-rodrigues	2013-09-03 15:30:12.389704	1
264	5	América Dourada	america-dourada	2013-09-03 15:30:12.389704	1
265	5	Anagé	anage	2013-09-03 15:30:12.389704	1
266	5	Andaraí	andarai	2013-09-03 15:30:12.389704	1
267	5	Andorinha	andorinha	2013-09-03 15:30:12.389704	1
268	5	Angical	angical	2013-09-03 15:30:12.389704	1
269	5	Anguera	anguera	2013-09-03 15:30:12.389704	1
270	5	Antas	antas	2013-09-03 15:30:12.389704	1
271	5	Antônio Cardoso	antonio-cardoso	2013-09-03 15:30:12.389704	1
272	5	Antônio Gonçalves	antonio-goncalves	2013-09-03 15:30:12.389704	1
273	5	Aporá	apora	2013-09-03 15:30:12.389704	1
274	5	Apuarema	apuarema	2013-09-03 15:30:12.389704	1
275	5	Araças	aracas	2013-09-03 15:30:12.389704	1
276	5	Aracatu	aracatu	2013-09-03 15:30:12.389704	1
277	5	Araci	araci	2013-09-03 15:30:12.389704	1
278	5	Aramari	aramari	2013-09-03 15:30:12.389704	1
279	5	Arataca	arataca	2013-09-03 15:30:12.389704	1
280	5	Aratuípe	aratuipe	2013-09-03 15:30:12.389704	1
281	5	Aurelino Leal	aurelino-leal	2013-09-03 15:30:12.389704	1
282	5	Baianópolis	baianopolis	2013-09-03 15:30:12.389704	1
283	5	Baixa Grande	baixa-grande	2013-09-03 15:30:12.389704	1
284	5	Banzaê	banzae	2013-09-03 15:30:12.389704	1
285	5	Barra	barra	2013-09-03 15:30:12.389704	1
286	5	Barra da Estiva	barra-da-estiva	2013-09-03 15:30:12.389704	1
287	5	Barra do Choça	barra-do-choca	2013-09-03 15:30:12.389704	1
288	5	Barra do Mendes	barra-do-mendes	2013-09-03 15:30:12.389704	1
289	5	Barra do Rocha	barra-do-rocha	2013-09-03 15:30:12.389704	1
290	5	Barreiras	barreiras	2013-09-03 15:30:12.389704	1
291	5	Barro Alto	barro-alto	2013-09-03 15:30:12.389704	1
292	5	Barro Preto (antigo Gov. Lomanto Jr.)	barro-preto-antigo-gov.-lomanto-jr.	2013-09-03 15:30:12.389704	1
293	5	Barrocas	barrocas	2013-09-03 15:30:12.389704	1
294	5	Belmonte	belmonte	2013-09-03 15:30:12.389704	1
295	5	Belo Campo	belo-campo	2013-09-03 15:30:12.389704	1
296	5	Biritinga	biritinga	2013-09-03 15:30:12.389704	1
297	5	Boa Nova	boa-nova	2013-09-03 15:30:12.389704	1
298	5	Boa Vista do Tupim	boa-vista-do-tupim	2013-09-03 15:30:12.389704	1
299	5	Bom Jesus da Lapa	bom-jesus-da-lapa	2013-09-03 15:30:12.389704	1
300	5	Bom Jesus da Serra	bom-jesus-da-serra	2013-09-03 15:30:12.389704	1
301	5	Boninal	boninal	2013-09-03 15:30:12.389704	1
302	5	Bonito	bonito	2013-09-03 15:30:12.389704	1
303	5	Boquira	boquira	2013-09-03 15:30:12.389704	1
304	5	Botuporã	botupora	2013-09-03 15:30:12.389704	1
305	5	Brejões	brejoes	2013-09-03 15:30:12.389704	1
306	5	Brejolândia	brejolandia	2013-09-03 15:30:12.389704	1
307	5	Brotas de Macaúbas	brotas-de-macaubas	2013-09-03 15:30:12.389704	1
308	5	Brumado	brumado	2013-09-03 15:30:12.389704	1
309	5	Buerarema	buerarema	2013-09-03 15:30:12.389704	1
310	5	Buritirama	buritirama	2013-09-03 15:30:12.389704	1
311	5	Caatiba	caatiba	2013-09-03 15:30:12.389704	1
312	5	Cabaceiras do Paraguaçu	cabaceiras-do-paraguacu	2013-09-03 15:30:12.389704	1
313	5	Cachoeira	cachoeira	2013-09-03 15:30:12.389704	1
314	5	Caculé	cacule	2013-09-03 15:30:12.389704	1
315	5	Caém	caem	2013-09-03 15:30:12.389704	1
316	5	Caetanos	caetanos	2013-09-03 15:30:12.389704	1
317	5	Caetité	caetite	2013-09-03 15:30:12.389704	1
318	5	Cafarnaum	cafarnaum	2013-09-03 15:30:12.389704	1
319	5	Cairu	cairu	2013-09-03 15:30:12.389704	1
320	5	Caldeirão Grande	caldeirao-grande	2013-09-03 15:30:12.389704	1
321	5	Camacan	camacan	2013-09-03 15:30:12.389704	1
322	5	Camaçari	camacari	2013-09-03 15:30:12.389704	1
323	5	Camamu	camamu	2013-09-03 15:30:12.389704	1
324	5	Campo Alegre de Lourdes	campo-alegre-de-lourdes	2013-09-03 15:30:12.389704	1
325	5	Campo Formoso	campo-formoso	2013-09-03 15:30:12.389704	1
326	5	Canápolis	canapolis	2013-09-03 15:30:12.389704	1
327	5	Canarana	canarana	2013-09-03 15:30:12.389704	1
328	5	Canavieiras	canavieiras	2013-09-03 15:30:12.389704	1
329	5	Candeal	candeal	2013-09-03 15:30:12.389704	1
330	5	Candeias	candeias	2013-09-03 15:30:12.389704	1
331	5	Candiba	candiba	2013-09-03 15:30:12.389704	1
332	5	Cândido Sales	candido-sales	2013-09-03 15:30:12.389704	1
333	5	Cansanção	cansancao	2013-09-03 15:30:12.389704	1
334	5	Canudos	canudos	2013-09-03 15:30:12.389704	1
335	5	Capela do Alto Alegre	capela-do-alto-alegre	2013-09-03 15:30:12.389704	1
336	5	Capim Grosso	capim-grosso	2013-09-03 15:30:12.389704	1
337	5	Caraíbas	caraibas	2013-09-03 15:30:12.389704	1
338	5	Caravelas	caravelas	2013-09-03 15:30:12.389704	1
339	5	Cardeal da Silva	cardeal-da-silva	2013-09-03 15:30:12.389704	1
340	5	Carinhanha	carinhanha	2013-09-03 15:30:12.389704	1
341	5	Casa Nova	casa-nova	2013-09-03 15:30:12.389704	1
342	5	Castro Alves	castro-alves	2013-09-03 15:30:12.389704	1
343	5	Catolândia	catolandia	2013-09-03 15:30:12.389704	1
344	5	Catu	catu	2013-09-03 15:30:12.389704	1
345	5	Caturama	caturama	2013-09-03 15:30:12.389704	1
346	5	Central	central	2013-09-03 15:30:12.389704	1
347	5	Chorrochó	chorrocho	2013-09-03 15:30:12.389704	1
348	5	Cícero Dantas	cicero-dantas	2013-09-03 15:30:12.389704	1
349	5	Cipó	cipo	2013-09-03 15:30:12.389704	1
350	5	Coaraci	coaraci	2013-09-03 15:30:12.389704	1
351	5	Cocos	cocos	2013-09-03 15:30:12.389704	1
352	5	Conceição da Feira	conceicao-da-feira	2013-09-03 15:30:12.389704	1
353	5	Conceição do Almeida	conceicao-do-almeida	2013-09-03 15:30:12.389704	1
354	5	Conceição do Coité	conceicao-do-coite	2013-09-03 15:30:12.389704	1
355	5	Conceição do Jacuípe	conceicao-do-jacuipe	2013-09-03 15:30:12.389704	1
356	5	Conde	conde	2013-09-03 15:30:12.389704	1
357	5	Condeúba	condeuba	2013-09-03 15:30:12.389704	1
358	5	Contendas do Sincorá	contendas-do-sincora	2013-09-03 15:30:12.389704	1
359	5	Coração de Maria	coracao-de-maria	2013-09-03 15:30:12.389704	1
360	5	Cordeiros	cordeiros	2013-09-03 15:30:12.389704	1
361	5	Coribe	coribe	2013-09-03 15:30:12.389704	1
362	5	Coronel João Sá	coronel-joao-sa	2013-09-03 15:30:12.389704	1
363	5	Correntina	correntina	2013-09-03 15:30:12.389704	1
364	5	Cotegipe	cotegipe	2013-09-03 15:30:12.389704	1
365	5	Cravolândia	cravolandia	2013-09-03 15:30:12.389704	1
366	5	Crisópolis	crisopolis	2013-09-03 15:30:12.389704	1
367	5	Cristópolis	cristopolis	2013-09-03 15:30:12.389704	1
368	5	Cruz das Almas	cruz-das-almas	2013-09-03 15:30:12.389704	1
369	5	Curaçá	curaca	2013-09-03 15:30:12.389704	1
370	5	Dário Meira	dario-meira	2013-09-03 15:30:12.389704	1
371	5	Dias d`Ávila	dias-d-avila	2013-09-03 15:30:12.389704	1
372	5	Dom Basílio	dom-basilio	2013-09-03 15:30:12.389704	1
373	5	Dom Macedo Costa	dom-macedo-costa	2013-09-03 15:30:12.389704	1
374	5	Elísio Medrado	elisio-medrado	2013-09-03 15:30:12.389704	1
375	5	Encruzilhada	encruzilhada	2013-09-03 15:30:12.389704	1
376	5	Entre Rios	entre-rios	2013-09-03 15:30:12.389704	1
377	5	Érico Cardoso	erico-cardoso	2013-09-03 15:30:12.389704	1
378	5	Esplanada	esplanada	2013-09-03 15:30:12.389704	1
379	5	Euclides da Cunha	euclides-da-cunha	2013-09-03 15:30:12.389704	1
380	5	Eunápolis	eunapolis	2013-09-03 15:30:12.389704	1
381	5	Fátima	fatima	2013-09-03 15:30:12.389704	1
382	5	Feira da Mata	feira-da-mata	2013-09-03 15:30:12.389704	1
383	5	Feira de Santana	feira-de-santana	2013-09-03 15:30:12.389704	1
384	5	Filadélfia	filadelfia	2013-09-03 15:30:12.389704	1
385	5	Firmino Alves	firmino-alves	2013-09-03 15:30:12.389704	1
386	5	Floresta Azul	floresta-azul	2013-09-03 15:30:12.389704	1
387	5	Formosa do Rio Preto	formosa-do-rio-preto	2013-09-03 15:30:12.389704	1
388	5	Gandu	gandu	2013-09-03 15:30:12.389704	1
389	5	Gavião	gaviao	2013-09-03 15:30:12.389704	1
390	5	Gentio do Ouro	gentio-do-ouro	2013-09-03 15:30:12.389704	1
391	5	Glória	gloria	2013-09-03 15:30:12.389704	1
392	5	Gongogi	gongogi	2013-09-03 15:30:12.389704	1
393	5	Governador Mangabeira	governador-mangabeira	2013-09-03 15:30:12.389704	1
394	5	Guajeru	guajeru	2013-09-03 15:30:12.389704	1
395	5	Guanambi	guanambi	2013-09-03 15:30:12.389704	1
396	5	Guaratinga	guaratinga	2013-09-03 15:30:12.389704	1
397	5	Heliópolis	heliopolis	2013-09-03 15:30:12.389704	1
398	5	Iaçu	iacu	2013-09-03 15:30:12.389704	1
399	5	Ibiassucê	ibiassuce	2013-09-03 15:30:12.389704	1
400	5	Ibicaraí	ibicarai	2013-09-03 15:30:12.389704	1
401	5	Ibicoara	ibicoara	2013-09-03 15:30:12.389704	1
402	5	Ibicuí	ibicui	2013-09-03 15:30:12.389704	1
403	5	Ibipeba	ibipeba	2013-09-03 15:30:12.389704	1
404	5	Ibipitanga	ibipitanga	2013-09-03 15:30:12.389704	1
405	5	Ibiquera	ibiquera	2013-09-03 15:30:12.389704	1
406	5	Ibirapitanga	ibirapitanga	2013-09-03 15:30:12.389704	1
407	5	Ibirapuã	ibirapua	2013-09-03 15:30:12.389704	1
408	5	Ibirataia	ibirataia	2013-09-03 15:30:12.389704	1
409	5	Ibitiara	ibitiara	2013-09-03 15:30:12.389704	1
410	5	Ibititá	ibitita	2013-09-03 15:30:12.389704	1
411	5	Ibotirama	ibotirama	2013-09-03 15:30:12.389704	1
412	5	Ichu	ichu	2013-09-03 15:30:12.389704	1
413	5	Igaporã	igapora	2013-09-03 15:30:12.389704	1
414	5	Igrapiúna	igrapiuna	2013-09-03 15:30:12.389704	1
415	5	Iguaí	iguai	2013-09-03 15:30:12.389704	1
416	5	Ilhéus	ilheus	2013-09-03 15:30:12.389704	1
417	5	Inhambupe	inhambupe	2013-09-03 15:30:12.389704	1
418	5	Ipecaetá	ipecaeta	2013-09-03 15:30:12.389704	1
419	5	Ipiaú	ipiau	2013-09-03 15:30:12.389704	1
420	5	Ipirá	ipira	2013-09-03 15:30:12.389704	1
421	5	Ipupiara	ipupiara	2013-09-03 15:30:12.389704	1
422	5	Irajuba	irajuba	2013-09-03 15:30:12.389704	1
423	5	Iramaia	iramaia	2013-09-03 15:30:12.389704	1
424	5	Iraquara	iraquara	2013-09-03 15:30:12.389704	1
425	5	Irará	irara	2013-09-03 15:30:12.389704	1
426	5	Irecê	irece	2013-09-03 15:30:12.389704	1
427	5	Itabela	itabela	2013-09-03 15:30:12.389704	1
428	5	Itaberaba	itaberaba	2013-09-03 15:30:12.389704	1
429	5	Itabuna	itabuna	2013-09-03 15:30:12.389704	1
430	5	Itacaré	itacare	2013-09-03 15:30:12.389704	1
431	5	Itaeté	itaete	2013-09-03 15:30:12.389704	1
432	5	Itagi	itagi	2013-09-03 15:30:12.389704	1
433	5	Itagibá	itagiba	2013-09-03 15:30:12.389704	1
434	5	Itagimirim	itagimirim	2013-09-03 15:30:12.389704	1
435	5	Itaguaçu da Bahia	itaguacu-da-bahia	2013-09-03 15:30:12.389704	1
436	5	Itaju do Colônia	itaju-do-colonia	2013-09-03 15:30:12.389704	1
437	5	Itajuípe	itajuipe	2013-09-03 15:30:12.389704	1
438	5	Itamaraju	itamaraju	2013-09-03 15:30:12.389704	1
439	5	Itamari	itamari	2013-09-03 15:30:12.389704	1
440	5	Itambé	itambe	2013-09-03 15:30:12.389704	1
441	5	Itanagra	itanagra	2013-09-03 15:30:12.389704	1
442	5	Itanhém	itanhem	2013-09-03 15:30:12.389704	1
443	5	Itaparica	itaparica	2013-09-03 15:30:12.389704	1
444	5	Itapé	itape	2013-09-03 15:30:12.389704	1
445	5	Itapebi	itapebi	2013-09-03 15:30:12.389704	1
446	5	Itapetinga	itapetinga	2013-09-03 15:30:12.389704	1
447	5	Itapicuru	itapicuru	2013-09-03 15:30:12.389704	1
448	5	Itapitanga	itapitanga	2013-09-03 15:30:12.389704	1
449	5	Itaquara	itaquara	2013-09-03 15:30:12.389704	1
450	5	Itarantim	itarantim	2013-09-03 15:30:12.389704	1
451	5	Itatim	itatim	2013-09-03 15:30:12.389704	1
452	5	Itiruçu	itirucu	2013-09-03 15:30:12.389704	1
453	5	Itiúba	itiuba	2013-09-03 15:30:12.389704	1
454	5	Itororó	itororo	2013-09-03 15:30:12.389704	1
455	5	Ituaçu	ituacu	2013-09-03 15:30:12.389704	1
456	5	Ituberá	itubera	2013-09-03 15:30:12.389704	1
457	5	Iuiú	iuiu	2013-09-03 15:30:12.389704	1
458	5	Jaborandi	jaborandi	2013-09-03 15:30:12.389704	1
459	5	Jacaraci	jacaraci	2013-09-03 15:30:12.389704	1
460	5	Jacobina	jacobina	2013-09-03 15:30:12.389704	1
461	5	Jaguaquara	jaguaquara	2013-09-03 15:30:12.389704	1
462	5	Jaguarari	jaguarari	2013-09-03 15:30:12.389704	1
463	5	Jaguaripe	jaguaripe	2013-09-03 15:30:12.389704	1
464	5	Jandaíra	jandaira	2013-09-03 15:30:12.389704	1
465	5	Jequié	jequie	2013-09-03 15:30:12.389704	1
466	5	Jeremoabo	jeremoabo	2013-09-03 15:30:12.389704	1
467	5	Jiquiriçá	jiquirica	2013-09-03 15:30:12.389704	1
468	5	Jitaúna	jitauna	2013-09-03 15:30:12.389704	1
469	5	João Dourado	joao-dourado	2013-09-03 15:30:12.389704	1
470	5	Juazeiro	juazeiro	2013-09-03 15:30:12.389704	1
471	5	Jucuruçu	jucurucu	2013-09-03 15:30:12.389704	1
472	5	Jussara	jussara	2013-09-03 15:30:12.389704	1
473	5	Jussari	jussari	2013-09-03 15:30:12.389704	1
474	5	Jussiape	jussiape	2013-09-03 15:30:12.389704	1
475	5	Lafaiete Coutinho	lafaiete-coutinho	2013-09-03 15:30:12.389704	1
476	5	Lagoa Real	lagoa-real	2013-09-03 15:30:12.389704	1
477	5	Laje	laje	2013-09-03 15:30:12.389704	1
478	5	Lajedão	lajedao	2013-09-03 15:30:12.389704	1
479	5	Lajedinho	lajedinho	2013-09-03 15:30:12.389704	1
480	5	Lajedo do Tabocal	lajedo-do-tabocal	2013-09-03 15:30:12.389704	1
481	5	Lamarão	lamarao	2013-09-03 15:30:12.389704	1
482	5	Lapão	lapao	2013-09-03 15:30:12.389704	1
483	5	Lauro de Freitas	lauro-de-freitas	2013-09-03 15:30:12.389704	1
484	5	Lençóis	lencois	2013-09-03 15:30:12.389704	1
485	5	Licínio de Almeida	licinio-de-almeida	2013-09-03 15:30:12.389704	1
486	5	\nLivramento de Nossa Senhora	livramento-de-nossa-senhora	2013-09-03 15:30:12.389704	1
487	5	Luís Eduardo Magalhães	luis-eduardo-magalhaes	2013-09-03 15:30:12.389704	1
488	5	Macajuba	macajuba	2013-09-03 15:30:12.389704	1
489	5	Macarani	macarani	2013-09-03 15:30:12.389704	1
490	5	Macaúbas	macaubas	2013-09-03 15:30:12.389704	1
491	5	Macururé	macurure	2013-09-03 15:30:12.389704	1
492	5	Madre de Deus	madre-de-deus	2013-09-03 15:30:12.389704	1
493	5	Maetinga	maetinga	2013-09-03 15:30:12.389704	1
494	5	Maiquinique	maiquinique	2013-09-03 15:30:12.389704	1
495	5	Mairi	mairi	2013-09-03 15:30:12.389704	1
496	5	Malhada	malhada	2013-09-03 15:30:12.389704	1
497	5	Malhada de Pedras	malhada-de-pedras	2013-09-03 15:30:12.389704	1
498	5	Manoel Vitorino	manoel-vitorino	2013-09-03 15:30:12.389704	1
499	5	Mansidão	mansidao	2013-09-03 15:30:12.389704	1
500	5	Maracás	maracas	2013-09-03 15:30:12.389704	1
501	5	Maragogipe	maragogipe	2013-09-03 15:30:12.389704	1
502	5	Maraú	marau	2013-09-03 15:30:12.389704	1
503	5	Marcionílio Souza	marcionilio-souza	2013-09-03 15:30:12.389704	1
504	5	Mascote	mascote	2013-09-03 15:30:12.389704	1
505	5	Mata de São João	mata-de-sao-joao	2013-09-03 15:30:12.389704	1
506	5	Matina	matina	2013-09-03 15:30:12.389704	1
507	5	Medeiros Neto	medeiros-neto	2013-09-03 15:30:12.389704	1
508	5	Miguel Calmon	miguel-calmon	2013-09-03 15:30:12.389704	1
509	5	Milagres	milagres	2013-09-03 15:30:12.389704	1
510	5	Mirangaba	mirangaba	2013-09-03 15:30:12.389704	1
511	5	Mirante	mirante	2013-09-03 15:30:12.389704	1
512	5	Monte Santo	monte-santo	2013-09-03 15:30:12.389704	1
513	5	Morpará	morpara	2013-09-03 15:30:12.389704	1
514	5	Morro do Chapéu	morro-do-chapeu	2013-09-03 15:30:12.389704	1
515	5	Mortugaba	mortugaba	2013-09-03 15:30:12.389704	1
516	5	Mucugê	mucuge	2013-09-03 15:30:12.389704	1
517	5	Mucuri	mucuri	2013-09-03 15:30:12.389704	1
518	5	Mulungu do Morro	mulungu-do-morro	2013-09-03 15:30:12.389704	1
519	5	Mundo Novo	mundo-novo	2013-09-03 15:30:12.389704	1
520	5	Muniz Ferreira	muniz-ferreira	2013-09-03 15:30:12.389704	1
521	5	Muquém de São Francisco	muquem-de-sao-francisco	2013-09-03 15:30:12.389704	1
522	5	Muritiba	muritiba	2013-09-03 15:30:12.389704	1
523	5	Mutuípe	mutuipe	2013-09-03 15:30:12.389704	1
524	5	Nazaré	nazare	2013-09-03 15:30:12.389704	1
525	5	Nilo Peçanha	nilo-pecanha	2013-09-03 15:30:12.389704	1
526	5	Nordestina	nordestina	2013-09-03 15:30:12.389704	1
527	5	Nova Canaã	nova-canaa	2013-09-03 15:30:12.389704	1
528	5	Nova Fátima	nova-fatima	2013-09-03 15:30:12.389704	1
529	5	Nova Ibiá	nova-ibia	2013-09-03 15:30:12.389704	1
530	5	Nova Itarana	nova-itarana	2013-09-03 15:30:12.389704	1
531	5	Nova Redenção	nova-redencao	2013-09-03 15:30:12.389704	1
532	5	Nova Soure	nova-soure	2013-09-03 15:30:12.389704	1
533	5	Nova Viçosa	nova-vicosa	2013-09-03 15:30:12.389704	1
534	5	Novo Horizonte	novo-horizonte	2013-09-03 15:30:12.389704	1
535	5	Novo Triunfo	novo-triunfo	2013-09-03 15:30:12.389704	1
536	5	Olindina	olindina	2013-09-03 15:30:12.389704	1
537	5	Oliveira dos Brejinhos	oliveira-dos-brejinhos	2013-09-03 15:30:12.389704	1
538	5	Ouriçangas	ouricangas	2013-09-03 15:30:12.389704	1
539	5	Ourolândia	ourolandia	2013-09-03 15:30:12.389704	1
540	5	Palmas de Monte Alto	palmas-de-monte-alto	2013-09-03 15:30:12.389704	1
541	5	Palmeiras	palmeiras	2013-09-03 15:30:12.389704	1
542	5	Paramirim	paramirim	2013-09-03 15:30:12.389704	1
543	5	Paratinga	paratinga	2013-09-03 15:30:12.389704	1
544	5	Paripiranga	paripiranga	2013-09-03 15:30:12.389704	1
545	5	Pau Brasil	pau-brasil	2013-09-03 15:30:12.389704	1
546	5	Paulo Afonso	paulo-afonso	2013-09-03 15:30:12.389704	1
547	5	Pé de Serra	pe-de-serra	2013-09-03 15:30:12.389704	1
548	5	Pedrão	pedrao	2013-09-03 15:30:12.389704	1
549	5	Pedro Alexandre	pedro-alexandre	2013-09-03 15:30:12.389704	1
550	5	Piatã	piata	2013-09-03 15:30:12.389704	1
551	5	Pilão Arcado	pilao-arcado	2013-09-03 15:30:12.389704	1
552	5	Pindaí	pindai	2013-09-03 15:30:12.389704	1
553	5	Pindobaçu	pindobacu	2013-09-03 15:30:12.389704	1
554	5	Pintadas	pintadas	2013-09-03 15:30:12.389704	1
555	5	Piraí do Norte	pirai-do-norte	2013-09-03 15:30:12.389704	1
556	5	Piripá	piripa	2013-09-03 15:30:12.389704	1
557	5	Piritiba	piritiba	2013-09-03 15:30:12.389704	1
558	5	Planaltino	planaltino	2013-09-03 15:30:12.389704	1
559	5	Planalto	planalto	2013-09-03 15:30:12.389704	1
560	5	Poções	pocoes	2013-09-03 15:30:12.389704	1
561	5	Pojuca	pojuca	2013-09-03 15:30:12.389704	1
562	5	Ponto Novo	ponto-novo	2013-09-03 15:30:12.389704	1
563	5	Porto Seguro	porto-seguro	2013-09-03 15:30:12.389704	1
564	5	Potiraguá	potiragua	2013-09-03 15:30:12.389704	1
565	5	Prado	prado	2013-09-03 15:30:12.389704	1
566	5	Presidente Dutra	presidente-dutra	2013-09-03 15:30:12.389704	1
567	5	Presidente Jânio Quadros	presidente-janio-quadros	2013-09-03 15:30:12.389704	1
568	5	Presidente Tancredo Neves	presidente-tancredo-neves	2013-09-03 15:30:12.389704	1
569	5	Queimadas	queimadas	2013-09-03 15:30:12.389704	1
570	5	Quijingue	quijingue	2013-09-03 15:30:12.389704	1
571	5	Quixabeira	quixabeira	2013-09-03 15:30:12.389704	1
572	5	Rafael Jambeiro	rafael-jambeiro	2013-09-03 15:30:12.389704	1
573	5	Remanso	remanso	2013-09-03 15:30:12.389704	1
574	5	Retirolândia	retirolandia	2013-09-03 15:30:12.389704	1
575	5	Riachão das Neves	riachao-das-neves	2013-09-03 15:30:12.389704	1
576	5	Riachão do Jacuípe	riachao-do-jacuipe	2013-09-03 15:30:12.389704	1
577	5	Riacho de Santana	riacho-de-santana	2013-09-03 15:30:12.389704	1
578	5	Ribeira do Amparo	ribeira-do-amparo	2013-09-03 15:30:12.389704	1
579	5	Ribeira do Pombal	ribeira-do-pombal	2013-09-03 15:30:12.389704	1
580	5	Ribeirão do Largo	ribeirao-do-largo	2013-09-03 15:30:12.389704	1
581	5	Rio de Contas	rio-de-contas	2013-09-03 15:30:12.389704	1
582	5	Rio do Antônio	rio-do-antonio	2013-09-03 15:30:12.389704	1
583	5	Rio do Pires	rio-do-pires	2013-09-03 15:30:12.389704	1
584	5	Rio Real	rio-real	2013-09-03 15:30:12.389704	1
585	5	Rodelas	rodelas	2013-09-03 15:30:12.389704	1
586	5	Ruy Barbosa	ruy-barbosa	2013-09-03 15:30:12.389704	1
587	5	Salinas da Margarida	salinas-da-margarida	2013-09-03 15:30:12.389704	1
588	5	Salvador	salvador	2013-09-03 15:30:12.389704	1
589	5	Santa Bárbara	santa-barbara	2013-09-03 15:30:12.389704	1
590	5	Santa Brígida	santa-brigida	2013-09-03 15:30:12.389704	1
591	5	Santa Cruz Cabrália	santa-cruz-cabralia	2013-09-03 15:30:12.389704	1
592	5	Santa Cruz da Vitória	santa-cruz-da-vitoria	2013-09-03 15:30:12.389704	1
593	5	Santa Inês	santa-ines	2013-09-03 15:30:12.389704	1
594	5	Santa Luzia	santa-luzia	2013-09-03 15:30:12.389704	1
595	5	Santa Maria da Vitória	santa-maria-da-vitoria	2013-09-03 15:30:12.389704	1
596	5	Santa Rita de Cássia	santa-rita-de-cassia	2013-09-03 15:30:12.389704	1
597	5	Santa Teresinha	santa-teresinha	2013-09-03 15:30:12.389704	1
598	5	Santaluz	santaluz	2013-09-03 15:30:12.389704	1
599	5	Santana	santana	2013-09-03 15:30:12.389704	1
600	5	Santanópolis	santanopolis	2013-09-03 15:30:12.389704	1
601	5	Santo Amaro	santo-amaro	2013-09-03 15:30:12.389704	1
602	5	Santo Antônio de Jesus	santo-antonio-de-jesus	2013-09-03 15:30:12.389704	1
603	5	Santo Estêvão	santo-estevao	2013-09-03 15:30:12.389704	1
604	5	São Desidério	sao-desiderio	2013-09-03 15:30:12.389704	1
605	5	São Domingos	sao-domingos	2013-09-03 15:30:12.389704	1
606	5	São Felipe	sao-felipe	2013-09-03 15:30:12.389704	1
607	5	São Félix	sao-felix	2013-09-03 15:30:12.389704	1
608	5	São Félix do Coribe	sao-felix-do-coribe	2013-09-03 15:30:12.389704	1
609	5	São Francisco do Conde	sao-francisco-do-conde	2013-09-03 15:30:12.389704	1
610	5	São Gabriel	sao-gabriel	2013-09-03 15:30:12.389704	1
611	5	São Gonçalo dos Campos	sao-goncalo-dos-campos	2013-09-03 15:30:12.389704	1
612	5	São José da Vitória	sao-jose-da-vitoria	2013-09-03 15:30:12.389704	1
613	5	São José do Jacuípe	sao-jose-do-jacuipe	2013-09-03 15:30:12.389704	1
614	5	São Miguel das Matas	sao-miguel-das-matas	2013-09-03 15:30:12.389704	1
615	5	São Sebastião do Passé	sao-sebastiao-do-passe	2013-09-03 15:30:12.389704	1
616	5	Sapeaçu	sapeacu	2013-09-03 15:30:12.389704	1
617	5	Sátiro Dias	satiro-dias	2013-09-03 15:30:12.389704	1
618	5	Saubara	saubara	2013-09-03 15:30:12.389704	1
619	5	Saúde	saude	2013-09-03 15:30:12.389704	1
620	5	Seabra	seabra	2013-09-03 15:30:12.389704	1
621	5	Sebastião Laranjeiras	sebastiao-laranjeiras	2013-09-03 15:30:12.389704	1
622	5	Senhor do Bonfim	senhor-do-bonfim	2013-09-03 15:30:12.389704	1
623	5	Sento Sé	sento-se	2013-09-03 15:30:12.389704	1
624	5	Serra do Ramalho	serra-do-ramalho	2013-09-03 15:30:12.389704	1
625	5	Serra Dourada	serra-dourada	2013-09-03 15:30:12.389704	1
626	5	Serra Preta	serra-preta	2013-09-03 15:30:12.389704	1
627	5	Serrinha	serrinha	2013-09-03 15:30:12.389704	1
628	5	Serrolândia	serrolandia	2013-09-03 15:30:12.389704	1
629	5	Simões Filho	simoes-filho	2013-09-03 15:30:12.389704	1
630	5	Sítio do Mato	sitio-do-mato	2013-09-03 15:30:12.389704	1
631	5	Sítio do Quinto	sitio-do-quinto	2013-09-03 15:30:12.389704	1
632	5	Sobradinho	sobradinho	2013-09-03 15:30:12.389704	1
633	5	Souto Soares	souto-soares	2013-09-03 15:30:12.389704	1
634	5	Tabocas do Brejo Velho	tabocas-do-brejo-velho	2013-09-03 15:30:12.389704	1
635	5	Tanhaçu	tanhacu	2013-09-03 15:30:12.389704	1
636	5	Tanque Novo	tanque-novo	2013-09-03 15:30:12.389704	1
637	5	Tanquinho	tanquinho	2013-09-03 15:30:12.389704	1
638	5	Taperoá	taperoa	2013-09-03 15:30:12.389704	1
639	5	Tapiramutá	tapiramuta	2013-09-03 15:30:12.389704	1
640	5	Teixeira de Freitas	teixeira-de-freitas	2013-09-03 15:30:12.389704	1
641	5	Teodoro Sampaio	teodoro-sampaio	2013-09-03 15:30:12.389704	1
642	5	Teofilândia	teofilandia	2013-09-03 15:30:12.389704	1
643	5	Teolândia	teolandia	2013-09-03 15:30:12.389704	1
644	5	Terra Nova	terra-nova	2013-09-03 15:30:12.389704	1
645	5	Tremedal	tremedal	2013-09-03 15:30:12.389704	1
646	5	Tucano	tucano	2013-09-03 15:30:12.389704	1
647	5	Uauá	uaua	2013-09-03 15:30:12.389704	1
648	5	Ubaíra	ubaira	2013-09-03 15:30:12.389704	1
649	5	Ubaitaba	ubaitaba	2013-09-03 15:30:12.389704	1
650	5	Ubatã	ubata	2013-09-03 15:30:12.389704	1
651	5	Uibaí	uibai	2013-09-03 15:30:12.389704	1
652	5	Umburanas	umburanas	2013-09-03 15:30:12.389704	1
654	5	Urandi	urandi	2013-09-03 15:30:12.389704	1
655	5	Uruçuca	urucuca	2013-09-03 15:30:12.389704	1
656	5	Utinga	utinga	2013-09-03 15:30:12.389704	1
657	5	Valença	valenca	2013-09-03 15:30:12.389704	1
658	5	Valente	valente	2013-09-03 15:30:12.389704	1
659	5	Várzea da Roça	varzea-da-roca	2013-09-03 15:30:12.389704	1
660	5	Várzea do Poço	varzea-do-poco	2013-09-03 15:30:12.389704	1
661	5	Várzea Nova	varzea-nova	2013-09-03 15:30:12.389704	1
662	5	Varzedo	varzedo	2013-09-03 15:30:12.389704	1
663	5	Vera Cruz	vera-cruz	2013-09-03 15:30:12.389704	1
664	5	Vereda	vereda	2013-09-03 15:30:12.389704	1
665	5	Vitória da Conquista	vitoria-da-conquista	2013-09-03 15:30:12.389704	1
666	5	Wagner	wagner	2013-09-03 15:30:12.389704	1
667	5	Wanderley	wanderley	2013-09-03 15:30:12.389704	1
668	5	Wenceslau Guimarães	wenceslau-guimaraes	2013-09-03 15:30:12.389704	1
669	5	Xique-Xique	xique-xique	2013-09-03 15:30:12.389704	1
764	6	Abaiara	abaiara	2013-09-03 15:30:12.389704	1
765	6	Acarape	acarape	2013-09-03 15:30:12.389704	1
766	6	Acaraú	acarau	2013-09-03 15:30:12.389704	1
767	6	Acopiara	acopiara	2013-09-03 15:30:12.389704	1
768	6	Aiuaba	aiuaba	2013-09-03 15:30:12.389704	1
769	6	Alcântaras	alcantaras	2013-09-03 15:30:12.389704	1
770	6	Altaneira	altaneira	2013-09-03 15:30:12.389704	1
771	6	Alto Santo	alto-santo	2013-09-03 15:30:12.389704	1
772	6	Amontada	amontada	2013-09-03 15:30:12.389704	1
773	6	Antonina do Norte	antonina-do-norte	2013-09-03 15:30:12.389704	1
774	6	Apuiarés	apuiares	2013-09-03 15:30:12.389704	1
775	6	Aquiraz	aquiraz	2013-09-03 15:30:12.389704	1
776	6	Aracati	aracati	2013-09-03 15:30:12.389704	1
777	6	Aracoiaba	aracoiaba	2013-09-03 15:30:12.389704	1
778	6	Ararendá	ararenda	2013-09-03 15:30:12.389704	1
779	6	Araripe	araripe	2013-09-03 15:30:12.389704	1
780	6	Aratuba	aratuba	2013-09-03 15:30:12.389704	1
781	6	Arneiroz	arneiroz	2013-09-03 15:30:12.389704	1
782	6	Assaré	assare	2013-09-03 15:30:12.389704	1
783	6	Aurora	aurora	2013-09-03 15:30:12.389704	1
784	6	Baixio	baixio	2013-09-03 15:30:12.389704	1
785	6	Banabuiú	banabuiu	2013-09-03 15:30:12.389704	1
786	6	Barbalha	barbalha	2013-09-03 15:30:12.389704	1
787	6	Barreira	barreira	2013-09-03 15:30:12.389704	1
788	6	Barro	barro	2013-09-03 15:30:12.389704	1
789	6	Barroquinha	barroquinha	2013-09-03 15:30:12.389704	1
790	6	Baturité	baturite	2013-09-03 15:30:12.389704	1
791	6	Beberibe	beberibe	2013-09-03 15:30:12.389704	1
792	6	Bela Cruz	bela-cruz	2013-09-03 15:30:12.389704	1
793	6	Boa Viagem	boa-viagem	2013-09-03 15:30:12.389704	1
794	6	Brejo Santo	brejo-santo	2013-09-03 15:30:12.389704	1
795	6	Camocim	camocim	2013-09-03 15:30:12.389704	1
796	6	Campos Sales	campos-sales	2013-09-03 15:30:12.389704	1
797	6	Canindé	caninde	2013-09-03 15:30:12.389704	1
798	6	Capistrano	capistrano	2013-09-03 15:30:12.389704	1
799	6	Caridade	caridade	2013-09-03 15:30:12.389704	1
800	6	Cariré	carire	2013-09-03 15:30:12.389704	1
801	6	Caririaçu	caririacu	2013-09-03 15:30:12.389704	1
802	6	Cariús	carius	2013-09-03 15:30:12.389704	1
803	6	Carnaubal	carnaubal	2013-09-03 15:30:12.389704	1
804	6	Cascavel	cascavel	2013-09-03 15:30:12.389704	1
805	6	Catarina	catarina	2013-09-03 15:30:12.389704	1
806	6	Catunda	catunda	2013-09-03 15:30:12.389704	1
807	6	Caucaia	caucaia	2013-09-03 15:30:12.389704	1
808	6	Cedro	cedro	2013-09-03 15:30:12.389704	1
809	6	Chaval	chaval	2013-09-03 15:30:12.389704	1
810	6	Choró	choro	2013-09-03 15:30:12.389704	1
811	6	Chorozinho	chorozinho	2013-09-03 15:30:12.389704	1
812	6	Coreaú	coreau	2013-09-03 15:30:12.389704	1
813	6	Crateús	crateus	2013-09-03 15:30:12.389704	1
814	6	Crato	crato	2013-09-03 15:30:12.389704	1
815	6	Croatá	croata	2013-09-03 15:30:12.389704	1
816	6	Cruz	cruz	2013-09-03 15:30:12.389704	1
817	6	Deputado Irapuan Pinheiro	deputado-irapuan-pinheiro	2013-09-03 15:30:12.389704	1
818	6	Ererê	erere	2013-09-03 15:30:12.389704	1
819	6	Eusébio	eusebio	2013-09-03 15:30:12.389704	1
820	6	Farias Brito	farias-brito	2013-09-03 15:30:12.389704	1
821	6	Forquilha	forquilha	2013-09-03 15:30:12.389704	1
822	6	Fortaleza	fortaleza	2013-09-03 15:30:12.389704	1
823	6	Fortim	fortim	2013-09-03 15:30:12.389704	1
824	6	Frecheirinha	frecheirinha	2013-09-03 15:30:12.389704	1
825	6	General Sampaio	general-sampaio	2013-09-03 15:30:12.389704	1
826	6	Graça	graca	2013-09-03 15:30:12.389704	1
827	6	Granja	granja	2013-09-03 15:30:12.389704	1
828	6	Granjeiro	granjeiro	2013-09-03 15:30:12.389704	1
829	6	Groaíras	groairas	2013-09-03 15:30:12.389704	1
830	6	Guaiúba	guaiuba	2013-09-03 15:30:12.389704	1
831	6	Guaraciaba do Norte	guaraciaba-do-norte	2013-09-03 15:30:12.389704	1
832	6	Guaramiranga	guaramiranga	2013-09-03 15:30:12.389704	1
833	6	Hidrolândia	hidrolandia	2013-09-03 15:30:12.389704	1
834	6	Horizonte	horizonte	2013-09-03 15:30:12.389704	1
835	6	Ibaretama	ibaretama	2013-09-03 15:30:12.389704	1
836	6	Ibiapina	ibiapina	2013-09-03 15:30:12.389704	1
837	6	Ibicuitinga	ibicuitinga	2013-09-03 15:30:12.389704	1
838	6	Icapuí	icapui	2013-09-03 15:30:12.389704	1
839	6	Icó	ico	2013-09-03 15:30:12.389704	1
840	6	Iguatu	iguatu	2013-09-03 15:30:12.389704	1
841	6	Independência	independencia	2013-09-03 15:30:12.389704	1
842	6	Ipaporanga	ipaporanga	2013-09-03 15:30:12.389704	1
843	6	Ipaumirim	ipaumirim	2013-09-03 15:30:12.389704	1
845	6	Ipueiras	ipueiras	2013-09-03 15:30:12.389704	1
846	6	Iracema	iracema	2013-09-03 15:30:12.389704	1
847	6	Irauçuba	iraucuba	2013-09-03 15:30:12.389704	1
848	6	Itaiçaba	itaicaba	2013-09-03 15:30:12.389704	1
849	6	Itaitinga	itaitinga	2013-09-03 15:30:12.389704	1
850	6	Itapagé	itapage	2013-09-03 15:30:12.389704	1
851	6	Itapipoca	itapipoca	2013-09-03 15:30:12.389704	1
852	6	Itapiúna	itapiuna	2013-09-03 15:30:12.389704	1
853	6	Itarema	itarema	2013-09-03 15:30:12.389704	1
854	6	Itatira	itatira	2013-09-03 15:30:12.389704	1
855	6	Jaguaretama	jaguaretama	2013-09-03 15:30:12.389704	1
856	6	Jaguaribara	jaguaribara	2013-09-03 15:30:12.389704	1
857	6	Jaguaribe	jaguaribe	2013-09-03 15:30:12.389704	1
1511	9	Uruaçu	uruacu	2013-09-03 15:30:12.389704	1
858	6	Jaguaruana	jaguaruana	2013-09-03 15:30:12.389704	1
859	6	Jardim	jardim	2013-09-03 15:30:12.389704	1
860	6	Jati	jati	2013-09-03 15:30:12.389704	1
861	6	Jijoca de Jericoacoara	jijoca-de-jericoacoara	2013-09-03 15:30:12.389704	1
862	6	Juazeiro do Norte	juazeiro-do-norte	2013-09-03 15:30:12.389704	1
863	6	Jucás	jucas	2013-09-03 15:30:12.389704	1
864	6	Lavras da Mangabeira	lavras-da-mangabeira	2013-09-03 15:30:12.389704	1
865	6	Limoeiro do Norte	limoeiro-do-norte	2013-09-03 15:30:12.389704	1
866	6	Madalena	madalena	2013-09-03 15:30:12.389704	1
867	6	Maracanaú	maracanau	2013-09-03 15:30:12.389704	1
868	6	Maranguape	maranguape	2013-09-03 15:30:12.389704	1
869	6	Marco	marco	2013-09-03 15:30:12.389704	1
870	6	Martinópole	martinopole	2013-09-03 15:30:12.389704	1
871	6	Massapê	massape	2013-09-03 15:30:12.389704	1
872	6	Mauriti	mauriti	2013-09-03 15:30:12.389704	1
873	6	Meruoca	meruoca	2013-09-03 15:30:12.389704	1
874	6	Milagres	milagres	2013-09-03 15:30:12.389704	1
875	6	Milhã	milha	2013-09-03 15:30:12.389704	1
876	6	Miraíma	miraima	2013-09-03 15:30:12.389704	1
877	6	Missão Velha	missao-velha	2013-09-03 15:30:12.389704	1
878	6	Mombaça	mombaca	2013-09-03 15:30:12.389704	1
879	6	Monsenhor Tabosa	monsenhor-tabosa	2013-09-03 15:30:12.389704	1
880	6	Morada Nova	morada-nova	2013-09-03 15:30:12.389704	1
881	6	Moraújo	moraujo	2013-09-03 15:30:12.389704	1
882	6	Morrinhos	morrinhos	2013-09-03 15:30:12.389704	1
883	6	Mucambo	mucambo	2013-09-03 15:30:12.389704	1
884	6	Mulungu	mulungu	2013-09-03 15:30:12.389704	1
885	6	Nova Olinda	nova-olinda	2013-09-03 15:30:12.389704	1
886	6	Nova Russas	nova-russas	2013-09-03 15:30:12.389704	1
887	6	Novo Oriente	novo-oriente	2013-09-03 15:30:12.389704	1
888	6	Ocara	ocara	2013-09-03 15:30:12.389704	1
889	6	Orós	oros	2013-09-03 15:30:12.389704	1
890	6	Pacajus	pacajus	2013-09-03 15:30:12.389704	1
891	6	Pacatuba	pacatuba	2013-09-03 15:30:12.389704	1
892	6	Pacoti	pacoti	2013-09-03 15:30:12.389704	1
893	6	Pacujá	pacuja	2013-09-03 15:30:12.389704	1
894	6	Palhano	palhano	2013-09-03 15:30:12.389704	1
895	6	Palmácia	palmacia	2013-09-03 15:30:12.389704	1
896	6	Paracuru	paracuru	2013-09-03 15:30:12.389704	1
897	6	Paraipaba	paraipaba	2013-09-03 15:30:12.389704	1
898	6	Parambu	parambu	2013-09-03 15:30:12.389704	1
899	6	Paramoti	paramoti	2013-09-03 15:30:12.389704	1
900	6	Pedra Branca	pedra-branca	2013-09-03 15:30:12.389704	1
901	6	Penaforte	penaforte	2013-09-03 15:30:12.389704	1
902	6	Pentecoste	pentecoste	2013-09-03 15:30:12.389704	1
903	6	Pereiro	pereiro	2013-09-03 15:30:12.389704	1
904	6	Pindoretama	pindoretama	2013-09-03 15:30:12.389704	1
905	6	Piquet Carneiro	piquet-carneiro	2013-09-03 15:30:12.389704	1
906	6	Pires Ferreira	pires-ferreira	2013-09-03 15:30:12.389704	1
907	6	Poranga	poranga	2013-09-03 15:30:12.389704	1
908	6	Porteiras	porteiras	2013-09-03 15:30:12.389704	1
909	6	Potengi	potengi	2013-09-03 15:30:12.389704	1
910	6	Potiretama	potiretama	2013-09-03 15:30:12.389704	1
911	6	Quiterianópolis	quiterianopolis	2013-09-03 15:30:12.389704	1
912	6	Quixadá	quixada	2013-09-03 15:30:12.389704	1
913	6	Quixelô	quixelo	2013-09-03 15:30:12.389704	1
914	6	Quixeramobim	quixeramobim	2013-09-03 15:30:12.389704	1
915	6	Quixeré	quixere	2013-09-03 15:30:12.389704	1
916	6	Redenção	redencao	2013-09-03 15:30:12.389704	1
917	6	Reriutaba	reriutaba	2013-09-03 15:30:12.389704	1
918	6	Russas	russas	2013-09-03 15:30:12.389704	1
919	6	Saboeiro	saboeiro	2013-09-03 15:30:12.389704	1
920	6	Salitre	salitre	2013-09-03 15:30:12.389704	1
921	6	Santa Quitéria	santa-quiteria	2013-09-03 15:30:12.389704	1
922	6	Santana do Acaraú	santana-do-acarau	2013-09-03 15:30:12.389704	1
923	6	Santana do Cariri	santana-do-cariri	2013-09-03 15:30:12.389704	1
924	6	São Benedito	sao-benedito	2013-09-03 15:30:12.389704	1
925	6	São Gonçalo do Amarante	sao-goncalo-do-amarante	2013-09-03 15:30:12.389704	1
926	6	São João do Jaguaribe	sao-joao-do-jaguaribe	2013-09-03 15:30:12.389704	1
927	6	São Luís do Curu	sao-luis-do-curu	2013-09-03 15:30:12.389704	1
928	6	Senador Pompeu	senador-pompeu	2013-09-03 15:30:12.389704	1
929	6	Senador Sá	senador-sa	2013-09-03 15:30:12.389704	1
930	6	Sobral	sobral	2013-09-03 15:30:12.389704	1
931	6	Solonópole	solonopole	2013-09-03 15:30:12.389704	1
932	6	Tabuleiro do Norte	tabuleiro-do-norte	2013-09-03 15:30:12.389704	1
933	6	Tamboril	tamboril	2013-09-03 15:30:12.389704	1
934	6	Tarrafas	tarrafas	2013-09-03 15:30:12.389704	1
935	6	Tauá	taua	2013-09-03 15:30:12.389704	1
936	6	Tejuçuoca	tejucuoca	2013-09-03 15:30:12.389704	1
937	6	Tianguá	tiangua	2013-09-03 15:30:12.389704	1
938	6	Trairi	trairi	2013-09-03 15:30:12.389704	1
939	6	Tururu	tururu	2013-09-03 15:30:12.389704	1
940	6	Ubajara	ubajara	2013-09-03 15:30:12.389704	1
941	6	Umari	umari	2013-09-03 15:30:12.389704	1
942	6	Umirim	umirim	2013-09-03 15:30:12.389704	1
943	6	Uruburetama	uruburetama	2013-09-03 15:30:12.389704	1
944	6	Uruoca	uruoca	2013-09-03 15:30:12.389704	1
945	6	Varjota	varjota	2013-09-03 15:30:12.389704	1
946	6	Várzea Alegre	varzea-alegre	2013-09-03 15:30:12.389704	1
947	6	Viçosa do Ceará	vicosa-do-ceara	2013-09-03 15:30:12.389704	1
1019	7	Brasília	brasilia	2013-09-03 15:30:12.389704	1
1020	8	Afonso Cláudio	afonso-claudio	2013-09-03 15:30:12.389704	1
1021	8	Água Doce do Norte	agua-doce-do-norte	2013-09-03 15:30:12.389704	1
1022	8	Águia Branca	aguia-branca	2013-09-03 15:30:12.389704	1
1023	8	Alegre	alegre	2013-09-03 15:30:12.389704	1
1024	8	Alfredo Chaves	alfredo-chaves	2013-09-03 15:30:12.389704	1
1025	8	Alto Rio Novo	alto-rio-novo	2013-09-03 15:30:12.389704	1
1026	8	Anchieta	anchieta	2013-09-03 15:30:12.389704	1
1027	8	Apiacá	apiaca	2013-09-03 15:30:12.389704	1
1028	8	Aracruz	aracruz	2013-09-03 15:30:12.389704	1
1029	8	Atilio Vivacqua	atilio-vivacqua	2013-09-03 15:30:12.389704	1
1030	8	Baixo Guandu	baixo-guandu	2013-09-03 15:30:12.389704	1
1031	8	Barra de São Francisco	barra-de-sao-francisco	2013-09-03 15:30:12.389704	1
1032	8	Boa Esperança	boa-esperanca	2013-09-03 15:30:12.389704	1
1033	8	Bom Jesus do Norte	bom-jesus-do-norte	2013-09-03 15:30:12.389704	1
1034	8	Brejetuba	brejetuba	2013-09-03 15:30:12.389704	1
1035	8	Cachoeiro de Itapemirim	cachoeiro-de-itapemirim	2013-09-03 15:30:12.389704	1
1036	8	Cariacica	cariacica	2013-09-03 15:30:12.389704	1
1037	8	Castelo	castelo	2013-09-03 15:30:12.389704	1
1038	8	Colatina	colatina	2013-09-03 15:30:12.389704	1
1039	8	Conceição da Barra	conceicao-da-barra	2013-09-03 15:30:12.389704	1
3136	13	Marilac	marilac	2013-09-03 15:30:12.389704	1
1040	8	Conceição do Castelo	conceicao-do-castelo	2013-09-03 15:30:12.389704	1
1041	8	Divino de São Lourenço	divino-de-sao-lourenco	2013-09-03 15:30:12.389704	1
1042	8	Domingos Martins	domingos-martins	2013-09-03 15:30:12.389704	1
1043	8	Dores do Rio Preto	dores-do-rio-preto	2013-09-03 15:30:12.389704	1
1044	8	Ecoporanga	ecoporanga	2013-09-03 15:30:12.389704	1
1045	8	Fundão	fundao	2013-09-03 15:30:12.389704	1
1046	8	Governador Lindenberg	governador-lindenberg	2013-09-03 15:30:12.389704	1
1047	8	Guaçuí	guacui	2013-09-03 15:30:12.389704	1
1048	8	Guarapari	guarapari	2013-09-03 15:30:12.389704	1
1049	8	Ibatiba	ibatiba	2013-09-03 15:30:12.389704	1
1050	8	Ibiraçu	ibiracu	2013-09-03 15:30:12.389704	1
1051	8	Ibitirama	ibitirama	2013-09-03 15:30:12.389704	1
1052	8	Iconha	iconha	2013-09-03 15:30:12.389704	1
1053	8	Irupi	irupi	2013-09-03 15:30:12.389704	1
1054	8	Itaguaçu	itaguacu	2013-09-03 15:30:12.389704	1
1055	8	Itapemirim	itapemirim	2013-09-03 15:30:12.389704	1
1056	8	Itarana	itarana	2013-09-03 15:30:12.389704	1
1057	8	Iúna	iuna	2013-09-03 15:30:12.389704	1
1058	8	Jaguaré	jaguare	2013-09-03 15:30:12.389704	1
1059	8	Jerônimo Monteiro	jeronimo-monteiro	2013-09-03 15:30:12.389704	1
1060	8	João Neiva	joao-neiva	2013-09-03 15:30:12.389704	1
1061	8	Laranja da Terra	laranja-da-terra	2013-09-03 15:30:12.389704	1
1062	8	Linhares	linhares	2013-09-03 15:30:12.389704	1
1063	8	Mantenópolis	mantenopolis	2013-09-03 15:30:12.389704	1
1064	8	Marataízes	marataizes	2013-09-03 15:30:12.389704	1
1065	8	Marechal Floriano	marechal-floriano	2013-09-03 15:30:12.389704	1
1066	8	Marilândia	marilandia	2013-09-03 15:30:12.389704	1
1067	8	Mimoso do Sul	mimoso-do-sul	2013-09-03 15:30:12.389704	1
1068	8	Montanha	montanha	2013-09-03 15:30:12.389704	1
1069	8	Mucurici	mucurici	2013-09-03 15:30:12.389704	1
1070	8	Muniz Freire	muniz-freire	2013-09-03 15:30:12.389704	1
1071	8	Muqui	muqui	2013-09-03 15:30:12.389704	1
1072	8	Nova Venécia	nova-venecia	2013-09-03 15:30:12.389704	1
1073	8	Pancas	pancas	2013-09-03 15:30:12.389704	1
1074	8	Pedro Canário	pedro-canario	2013-09-03 15:30:12.389704	1
1075	8	Pinheiros	pinheiros	2013-09-03 15:30:12.389704	1
1076	8	Piúma	piuma	2013-09-03 15:30:12.389704	1
1077	8	Ponto Belo	ponto-belo	2013-09-03 15:30:12.389704	1
1078	8	Presidente Kennedy	presidente-kennedy	2013-09-03 15:30:12.389704	1
1079	8	Rio Bananal	rio-bananal	2013-09-03 15:30:12.389704	1
1080	8	Rio Novo do Sul	rio-novo-do-sul	2013-09-03 15:30:12.389704	1
1081	8	Santa Leopoldina	santa-leopoldina	2013-09-03 15:30:12.389704	1
1082	8	Santa Maria de Jetibá	santa-maria-de-jetiba	2013-09-03 15:30:12.389704	1
1083	8	Santa Teresa	santa-teresa	2013-09-03 15:30:12.389704	1
1084	8	São Domingos do Norte	sao-domingos-do-norte	2013-09-03 15:30:12.389704	1
1085	8	São Gabriel da Palha	sao-gabriel-da-palha	2013-09-03 15:30:12.389704	1
1086	8	São José do Calçado	sao-jose-do-calcado	2013-09-03 15:30:12.389704	1
1087	8	São Mateus	sao-mateus	2013-09-03 15:30:12.389704	1
1088	8	São Roque do Canaã	sao-roque-do-canaa	2013-09-03 15:30:12.389704	1
1089	8	Serra	serra	2013-09-03 15:30:12.389704	1
1090	8	Sooretama	sooretama	2013-09-03 15:30:12.389704	1
1091	8	Vargem Alta	vargem-alta	2013-09-03 15:30:12.389704	1
1092	8	Venda Nova do Imigrante	venda-nova-do-imigrante	2013-09-03 15:30:12.389704	1
1093	8	Viana	viana	2013-09-03 15:30:12.389704	1
1094	8	Vila Pavão	vila-pavao	2013-09-03 15:30:12.389704	1
1095	8	Vila Valério	vila-valerio	2013-09-03 15:30:12.389704	1
1096	8	Vila Velha	vila-velha	2013-09-03 15:30:12.389704	1
1097	8	Vitória	vitoria	2013-09-03 15:30:12.389704	1
1274	9	Abadia de Goiás	abadia-de-goias	2013-09-03 15:30:12.389704	1
1275	9	Abadiânia	abadiania	2013-09-03 15:30:12.389704	1
1276	9	Acreúna	acreuna	2013-09-03 15:30:12.389704	1
1277	9	Adelândia	adelandia	2013-09-03 15:30:12.389704	1
1278	9	Água Fria de Goiás	agua-fria-de-goias	2013-09-03 15:30:12.389704	1
1279	9	Água Limpa	agua-limpa	2013-09-03 15:30:12.389704	1
1280	9	Águas Lindas de Goiás	aguas-lindas-de-goias	2013-09-03 15:30:12.389704	1
1281	9	Alexânia	alexania	2013-09-03 15:30:12.389704	1
1282	9	Aloândia	aloandia	2013-09-03 15:30:12.389704	1
1283	9	Alto Horizonte	alto-horizonte	2013-09-03 15:30:12.389704	1
1284	9	Alto Paraíso de Goiás	alto-paraiso-de-goias	2013-09-03 15:30:12.389704	1
1285	9	Alvorada do Norte	alvorada-do-norte	2013-09-03 15:30:12.389704	1
1286	9	Amaralina	amaralina	2013-09-03 15:30:12.389704	1
1287	9	Americano do Brasil	americano-do-brasil	2013-09-03 15:30:12.389704	1
1288	9	Amorinópolis	amorinopolis	2013-09-03 15:30:12.389704	1
1289	9	Anápolis	anapolis	2013-09-03 15:30:12.389704	1
1290	9	Anhanguera	anhanguera	2013-09-03 15:30:12.389704	1
1291	9	Anicuns	anicuns	2013-09-03 15:30:12.389704	1
1292	9	Aparecida de Goiânia	aparecida-de-goiania	2013-09-03 15:30:12.389704	1
1293	9	Aparecida do Rio Doce	aparecida-do-rio-doce	2013-09-03 15:30:12.389704	1
1294	9	Aporé	apore	2013-09-03 15:30:12.389704	1
1295	9	Araçu	aracu	2013-09-03 15:30:12.389704	1
1296	9	Aragarças	aragarcas	2013-09-03 15:30:12.389704	1
1297	9	Aragoiânia	aragoiania	2013-09-03 15:30:12.389704	1
1298	9	Araguapaz	araguapaz	2013-09-03 15:30:12.389704	1
1299	9	Arenópolis	arenopolis	2013-09-03 15:30:12.389704	1
1300	9	Aruanã	aruana	2013-09-03 15:30:12.389704	1
1301	9	Aurilândia	aurilandia	2013-09-03 15:30:12.389704	1
1302	9	Avelinópolis	avelinopolis	2013-09-03 15:30:12.389704	1
1303	9	Baliza	baliza	2013-09-03 15:30:12.389704	1
1304	9	Barro Alto	barro-alto	2013-09-03 15:30:12.389704	1
1305	9	Bela Vista de Goiás	bela-vista-de-goias	2013-09-03 15:30:12.389704	1
1306	9	Bom Jardim de Goiás	bom-jardim-de-goias	2013-09-03 15:30:12.389704	1
1307	9	Bom Jesus de Goiás	bom-jesus-de-goias	2013-09-03 15:30:12.389704	1
1308	9	Bonfinópolis	bonfinopolis	2013-09-03 15:30:12.389704	1
1309	9	Bonópolis	bonopolis	2013-09-03 15:30:12.389704	1
1310	9	Brazabrantes	brazabrantes	2013-09-03 15:30:12.389704	1
1311	9	Britânia	britania	2013-09-03 15:30:12.389704	1
1312	9	Buriti Alegre	buriti-alegre	2013-09-03 15:30:12.389704	1
1313	9	Buriti de Goiás	buriti-de-goias	2013-09-03 15:30:12.389704	1
1314	9	Buritinópolis	buritinopolis	2013-09-03 15:30:12.389704	1
1315	9	Cabeceiras	cabeceiras	2013-09-03 15:30:12.389704	1
1316	9	Cachoeira Alta	cachoeira-alta	2013-09-03 15:30:12.389704	1
1317	9	Cachoeira de Goiás	cachoeira-de-goias	2013-09-03 15:30:12.389704	1
1318	9	Cachoeira Dourada	cachoeira-dourada	2013-09-03 15:30:12.389704	1
1319	9	Caçu	cacu	2013-09-03 15:30:12.389704	1
1320	9	Caiapônia	caiaponia	2013-09-03 15:30:12.389704	1
1321	9	Caldas Novas	caldas-novas	2013-09-03 15:30:12.389704	1
1322	9	Caldazinha	caldazinha	2013-09-03 15:30:12.389704	1
1323	9	Campestre de Goiás	campestre-de-goias	2013-09-03 15:30:12.389704	1
1324	9	Campinaçu	campinacu	2013-09-03 15:30:12.389704	1
1325	9	Campinorte	campinorte	2013-09-03 15:30:12.389704	1
1326	9	Campo Alegre de Goiás	campo-alegre-de-goias	2013-09-03 15:30:12.389704	1
1327	9	Campo Limpo de Goiás	campo-limpo-de-goias	2013-09-03 15:30:12.389704	1
1328	9	Campos Belos	campos-belos	2013-09-03 15:30:12.389704	1
1329	9	Campos Verdes	campos-verdes	2013-09-03 15:30:12.389704	1
1330	9	Carmo do Rio Verde	carmo-do-rio-verde	2013-09-03 15:30:12.389704	1
1331	9	Castelândia	castelandia	2013-09-03 15:30:12.389704	1
1332	9	Catalão	catalao	2013-09-03 15:30:12.389704	1
1333	9	Caturaí	caturai	2013-09-03 15:30:12.389704	1
1334	9	Cavalcante	cavalcante	2013-09-03 15:30:12.389704	1
1335	9	Ceres	ceres	2013-09-03 15:30:12.389704	1
1336	9	Cezarina	cezarina	2013-09-03 15:30:12.389704	1
1337	9	Chapadão do Céu	chapadao-do-ceu	2013-09-03 15:30:12.389704	1
1338	9	Cidade Ocidental	cidade-ocidental	2013-09-03 15:30:12.389704	1
1339	9	Cocalzinho de Goiás	cocalzinho-de-goias	2013-09-03 15:30:12.389704	1
1340	9	Colinas do Sul	colinas-do-sul	2013-09-03 15:30:12.389704	1
1341	9	Córrego do Ouro	corrego-do-ouro	2013-09-03 15:30:12.389704	1
1342	9	Corumbá de Goiás	corumba-de-goias	2013-09-03 15:30:12.389704	1
1343	9	Corumbaíba	corumbaiba	2013-09-03 15:30:12.389704	1
1344	9	Cristalina	cristalina	2013-09-03 15:30:12.389704	1
1345	9	Cristianópolis	cristianopolis	2013-09-03 15:30:12.389704	1
1346	9	Crixás	crixas	2013-09-03 15:30:12.389704	1
1347	9	Cromínia	crominia	2013-09-03 15:30:12.389704	1
1348	9	Cumari	cumari	2013-09-03 15:30:12.389704	1
1349	9	Damianópolis	damianopolis	2013-09-03 15:30:12.389704	1
1350	9	Damolândia	damolandia	2013-09-03 15:30:12.389704	1
1351	9	Davinópolis	davinopolis	2013-09-03 15:30:12.389704	1
1352	9	Diorama	diorama	2013-09-03 15:30:12.389704	1
1353	9	Divinópolis de Goiás	divinopolis-de-goias	2013-09-03 15:30:12.389704	1
1354	9	Doverlândia	doverlandia	2013-09-03 15:30:12.389704	1
1355	9	Edealina	edealina	2013-09-03 15:30:12.389704	1
1356	9	Edéia	edeia	2013-09-03 15:30:12.389704	1
1357	9	Estrela do Norte	estrela-do-norte	2013-09-03 15:30:12.389704	1
1358	9	Faina	faina	2013-09-03 15:30:12.389704	1
1359	9	Fazenda Nova	fazenda-nova	2013-09-03 15:30:12.389704	1
1360	9	Firminópolis	firminopolis	2013-09-03 15:30:12.389704	1
1361	9	Flores de Goiás	flores-de-goias	2013-09-03 15:30:12.389704	1
1362	9	Formosa	formosa	2013-09-03 15:30:12.389704	1
1363	9	Formoso	formoso	2013-09-03 15:30:12.389704	1
1364	9	Gameleira de Goiás	gameleira-de-goias	2013-09-03 15:30:12.389704	1
1365	9	Goianápolis	goianapolis	2013-09-03 15:30:12.389704	1
1366	9	Goiandira	goiandira	2013-09-03 15:30:12.389704	1
1367	9	Goianésia	goianesia	2013-09-03 15:30:12.389704	1
1368	9	Goiânia	goiania	2013-09-03 15:30:12.389704	1
1369	9	Goianira	goianira	2013-09-03 15:30:12.389704	1
1370	9	Goiás	goias	2013-09-03 15:30:12.389704	1
1371	9	Goiatuba	goiatuba	2013-09-03 15:30:12.389704	1
1372	9	Gouvelândia	gouvelandia	2013-09-03 15:30:12.389704	1
1373	9	Guapó	guapo	2013-09-03 15:30:12.389704	1
1374	9	Guaraíta	guaraita	2013-09-03 15:30:12.389704	1
1375	9	Guarani de Goiás	guarani-de-goias	2013-09-03 15:30:12.389704	1
1376	9	Guarinos	guarinos	2013-09-03 15:30:12.389704	1
1377	9	Heitoraí	heitorai	2013-09-03 15:30:12.389704	1
1378	9	Hidrolândia	hidrolandia	2013-09-03 15:30:12.389704	1
1379	9	Hidrolina	hidrolina	2013-09-03 15:30:12.389704	1
1380	9	Iaciara	iaciara	2013-09-03 15:30:12.389704	1
1381	9	Inaciolândia	inaciolandia	2013-09-03 15:30:12.389704	1
1382	9	Indiara	indiara	2013-09-03 15:30:12.389704	1
1383	9	Inhumas	inhumas	2013-09-03 15:30:12.389704	1
1384	9	Ipameri	ipameri	2013-09-03 15:30:12.389704	1
1385	9	Ipiranga de Goiás	ipiranga-de-goias	2013-09-03 15:30:12.389704	1
1386	9	Iporá	ipora	2013-09-03 15:30:12.389704	1
1387	9	Israelândia	israelandia	2013-09-03 15:30:12.389704	1
1388	9	Itaberaí	itaberai	2013-09-03 15:30:12.389704	1
1389	9	Itaguari	itaguari	2013-09-03 15:30:12.389704	1
1390	9	Itaguaru	itaguaru	2013-09-03 15:30:12.389704	1
1391	9	Itajá	itaja	2013-09-03 15:30:12.389704	1
1392	9	Itapaci	itapaci	2013-09-03 15:30:12.389704	1
1393	9	Itapirapuã	itapirapua	2013-09-03 15:30:12.389704	1
1394	9	Itapuranga	itapuranga	2013-09-03 15:30:12.389704	1
1395	9	Itarumã	itaruma	2013-09-03 15:30:12.389704	1
1396	9	Itauçu	itaucu	2013-09-03 15:30:12.389704	1
1397	9	Itumbiara	itumbiara	2013-09-03 15:30:12.389704	1
1398	9	Ivolândia	ivolandia	2013-09-03 15:30:12.389704	1
1399	9	Jandaia	jandaia	2013-09-03 15:30:12.389704	1
1400	9	Jaraguá	jaragua	2013-09-03 15:30:12.389704	1
1401	9	Jataí	jatai	2013-09-03 15:30:12.389704	1
1402	9	Jaupaci	jaupaci	2013-09-03 15:30:12.389704	1
1403	9	Jesúpolis	jesupolis	2013-09-03 15:30:12.389704	1
1404	9	Joviânia	joviania	2013-09-03 15:30:12.389704	1
1405	9	Jussara	jussara	2013-09-03 15:30:12.389704	1
1406	9	Lagoa Santa	lagoa-santa	2013-09-03 15:30:12.389704	1
1407	9	Leopoldo de Bulhões	leopoldo-de-bulhoes	2013-09-03 15:30:12.389704	1
1408	9	Luziânia	luziania	2013-09-03 15:30:12.389704	1
1409	9	Mairipotaba	mairipotaba	2013-09-03 15:30:12.389704	1
1410	9	Mambaí	mambai	2013-09-03 15:30:12.389704	1
1411	9	Mara Rosa	mara-rosa	2013-09-03 15:30:12.389704	1
1412	9	Marzagão	marzagao	2013-09-03 15:30:12.389704	1
1413	9	Matrinchã	matrincha	2013-09-03 15:30:12.389704	1
1414	9	Maurilândia	maurilandia	2013-09-03 15:30:12.389704	1
1415	9	Mimoso de Goiás	mimoso-de-goias	2013-09-03 15:30:12.389704	1
1416	9	Minaçu	minacu	2013-09-03 15:30:12.389704	1
1417	9	Mineiros	mineiros	2013-09-03 15:30:12.389704	1
1418	9	Moiporá	moipora	2013-09-03 15:30:12.389704	1
1419	9	Monte Alegre de Goiás	monte-alegre-de-goias	2013-09-03 15:30:12.389704	1
1420	9	Montes Claros de Goiás	montes-claros-de-goias	2013-09-03 15:30:12.389704	1
1421	9	Montividiu	montividiu	2013-09-03 15:30:12.389704	1
1422	9	Montividiu do Norte	montividiu-do-norte	2013-09-03 15:30:12.389704	1
1423	9	Morrinhos	morrinhos	2013-09-03 15:30:12.389704	1
1424	9	Morro Agudo de Goiás	morro-agudo-de-goias	2013-09-03 15:30:12.389704	1
1425	9	Mossâmedes	mossamedes	2013-09-03 15:30:12.389704	1
1426	9	Mozarlândia	mozarlandia	2013-09-03 15:30:12.389704	1
1427	9	Mundo Novo	mundo-novo	2013-09-03 15:30:12.389704	1
1428	9	Mutunópolis	mutunopolis	2013-09-03 15:30:12.389704	1
1429	9	Nazário	nazario	2013-09-03 15:30:12.389704	1
1430	9	Nerópolis	neropolis	2013-09-03 15:30:12.389704	1
1431	9	Niquelândia	niquelandia	2013-09-03 15:30:12.389704	1
1432	9	Nova América	nova-america	2013-09-03 15:30:12.389704	1
1433	9	Nova Aurora	nova-aurora	2013-09-03 15:30:12.389704	1
1434	9	Nova Crixás	nova-crixas	2013-09-03 15:30:12.389704	1
1435	9	Nova Glória	nova-gloria	2013-09-03 15:30:12.389704	1
1436	9	Nova Iguaçu de Goiás	nova-iguacu-de-goias	2013-09-03 15:30:12.389704	1
1437	9	Nova Roma	nova-roma	2013-09-03 15:30:12.389704	1
1438	9	Nova Veneza	nova-veneza	2013-09-03 15:30:12.389704	1
1439	9	Novo Brasil	novo-brasil	2013-09-03 15:30:12.389704	1
1440	9	Novo Gama	novo-gama	2013-09-03 15:30:12.389704	1
1441	9	Novo Planalto	novo-planalto	2013-09-03 15:30:12.389704	1
1442	9	Orizona	orizona	2013-09-03 15:30:12.389704	1
1443	9	Ouro Verde de Goiás	ouro-verde-de-goias	2013-09-03 15:30:12.389704	1
1444	9	Ouvidor	ouvidor	2013-09-03 15:30:12.389704	1
1445	9	Padre Bernardo	padre-bernardo	2013-09-03 15:30:12.389704	1
1446	9	Palestina de Goiás	palestina-de-goias	2013-09-03 15:30:12.389704	1
1447	9	Palmeiras de Goiás	palmeiras-de-goias	2013-09-03 15:30:12.389704	1
1448	9	Palmelo	palmelo	2013-09-03 15:30:12.389704	1
1449	9	Palminópolis	palminopolis	2013-09-03 15:30:12.389704	1
1450	9	Panamá	panama	2013-09-03 15:30:12.389704	1
1451	9	Paranaiguara	paranaiguara	2013-09-03 15:30:12.389704	1
1452	9	Paraúna	parauna	2013-09-03 15:30:12.389704	1
1453	9	Perolândia	perolandia	2013-09-03 15:30:12.389704	1
1454	9	Petrolina de Goiás	petrolina-de-goias	2013-09-03 15:30:12.389704	1
1455	9	Pilar de Goiás	pilar-de-goias	2013-09-03 15:30:12.389704	1
1456	9	Piracanjuba	piracanjuba	2013-09-03 15:30:12.389704	1
1457	9	Piranhas	piranhas	2013-09-03 15:30:12.389704	1
1458	9	Pirenópolis	pirenopolis	2013-09-03 15:30:12.389704	1
1459	9	Pires do Rio	pires-do-rio	2013-09-03 15:30:12.389704	1
1460	9	Planaltina	planaltina	2013-09-03 15:30:12.389704	1
1461	9	Pontalina	pontalina	2013-09-03 15:30:12.389704	1
1462	9	Porangatu	porangatu	2013-09-03 15:30:12.389704	1
1463	9	Porteirão	porteirao	2013-09-03 15:30:12.389704	1
1464	9	Portelândia	portelandia	2013-09-03 15:30:12.389704	1
1465	9	Posse	posse	2013-09-03 15:30:12.389704	1
1466	9	Professor Jamil	professor-jamil	2013-09-03 15:30:12.389704	1
1467	9	Quirinópolis	quirinopolis	2013-09-03 15:30:12.389704	1
1468	9	Rialma	rialma	2013-09-03 15:30:12.389704	1
1469	9	Rianápolis	rianapolis	2013-09-03 15:30:12.389704	1
1470	9	Rio Quente	rio-quente	2013-09-03 15:30:12.389704	1
1471	9	Rio Verde	rio-verde	2013-09-03 15:30:12.389704	1
1472	9	Rubiataba	rubiataba	2013-09-03 15:30:12.389704	1
1473	9	Sanclerlândia	sanclerlandia	2013-09-03 15:30:12.389704	1
1474	9	Santa Bárbara de Goiás	santa-barbara-de-goias	2013-09-03 15:30:12.389704	1
1475	9	Santa Cruz de Goiás	santa-cruz-de-goias	2013-09-03 15:30:12.389704	1
1476	9	Santa Fé de Goiás	santa-fe-de-goias	2013-09-03 15:30:12.389704	1
1477	9	Santa Helena de Goiás	santa-helena-de-goias	2013-09-03 15:30:12.389704	1
1478	9	Santa Isabel	santa-isabel	2013-09-03 15:30:12.389704	1
1479	9	Santa Rita do Araguaia	santa-rita-do-araguaia	2013-09-03 15:30:12.389704	1
1480	9	Santa Rita do Novo Destino	santa-rita-do-novo-destino	2013-09-03 15:30:12.389704	1
1481	9	Santa Rosa de Goiás	santa-rosa-de-goias	2013-09-03 15:30:12.389704	1
1482	9	Santa Tereza de Goiás	santa-tereza-de-goias	2013-09-03 15:30:12.389704	1
1483	9	Santa Terezinha de Goiás	santa-terezinha-de-goias	2013-09-03 15:30:12.389704	1
1484	9	Santo Antônio da Barra	santo-antonio-da-barra	2013-09-03 15:30:12.389704	1
1485	9	Santo Antônio de Goiás	santo-antonio-de-goias	2013-09-03 15:30:12.389704	1
1486	9	Santo Antônio do Descoberto	santo-antonio-do-descoberto	2013-09-03 15:30:12.389704	1
1487	9	São Domingos	sao-domingos	2013-09-03 15:30:12.389704	1
1488	9	São Francisco de Goiás	sao-francisco-de-goias	2013-09-03 15:30:12.389704	1
1489	9	São João d`Aliança	sao-joao-d-alianca	2013-09-03 15:30:12.389704	1
1490	9	São João da Paraúna	sao-joao-da-parauna	2013-09-03 15:30:12.389704	1
1491	9	São Luís de Montes Belos	sao-luis-de-montes-belos	2013-09-03 15:30:12.389704	1
1492	9	São Luíz do Norte	sao-luiz-do-norte	2013-09-03 15:30:12.389704	1
1493	9	São Miguel do Araguaia	sao-miguel-do-araguaia	2013-09-03 15:30:12.389704	1
1494	9	São Miguel do Passa Quatro	sao-miguel-do-passa-quatro	2013-09-03 15:30:12.389704	1
1495	9	São Patrício	sao-patricio	2013-09-03 15:30:12.389704	1
1496	9	São Simão	sao-simao	2013-09-03 15:30:12.389704	1
1497	9	Senador Canedo	senador-canedo	2013-09-03 15:30:12.389704	1
1498	9	Serranópolis	serranopolis	2013-09-03 15:30:12.389704	1
1499	9	Silvânia	silvania	2013-09-03 15:30:12.389704	1
1500	9	Simolândia	simolandia	2013-09-03 15:30:12.389704	1
1501	9	Sítio d`Abadia	sitio-d-abadia	2013-09-03 15:30:12.389704	1
1502	9	Taquaral de Goiás	taquaral-de-goias	2013-09-03 15:30:12.389704	1
1503	9	Teresina de Goiás	teresina-de-goias	2013-09-03 15:30:12.389704	1
1504	9	Terezópolis de Goiás	terezopolis-de-goias	2013-09-03 15:30:12.389704	1
1505	9	Três Ranchos	tres-ranchos	2013-09-03 15:30:12.389704	1
1506	9	Trindade	trindade	2013-09-03 15:30:12.389704	1
1507	9	Trombas	trombas	2013-09-03 15:30:12.389704	1
1508	9	Turvânia	turvania	2013-09-03 15:30:12.389704	1
1509	9	Turvelândia	turvelandia	2013-09-03 15:30:12.389704	1
1510	9	Uirapuru	uirapuru	2013-09-03 15:30:12.389704	1
1512	9	Uruana	uruana	2013-09-03 15:30:12.389704	1
1513	9	Urutaí	urutai	2013-09-03 15:30:12.389704	1
1514	9	Valparaíso de Goiás	valparaiso-de-goias	2013-09-03 15:30:12.389704	1
1515	9	Varjão	varjao	2013-09-03 15:30:12.389704	1
1516	9	Vianópolis	vianopolis	2013-09-03 15:30:12.389704	1
1517	9	Vicentinópolis	vicentinopolis	2013-09-03 15:30:12.389704	1
1518	9	Vila Boa	vila-boa	2013-09-03 15:30:12.389704	1
1519	9	Vila Propício	vila-propicio	2013-09-03 15:30:12.389704	1
1529	10	Açailândia	acailandia	2013-09-03 15:30:12.389704	1
1530	10	Afonso Cunha	afonso-cunha	2013-09-03 15:30:12.389704	1
1531	10	Água Doce do Maranhão	agua-doce-do-maranhao	2013-09-03 15:30:12.389704	1
1532	10	Alcântara	alcantara	2013-09-03 15:30:12.389704	1
1533	10	Aldeias Altas	aldeias-altas	2013-09-03 15:30:12.389704	1
1534	10	Altamira do Maranhão	altamira-do-maranhao	2013-09-03 15:30:12.389704	1
1535	10	Alto Alegre do Maranhão	alto-alegre-do-maranhao	2013-09-03 15:30:12.389704	1
1536	10	Alto Alegre do Pindaré	alto-alegre-do-pindare	2013-09-03 15:30:12.389704	1
1537	10	Alto Parnaíba	alto-parnaiba	2013-09-03 15:30:12.389704	1
1538	10	Amapá do Maranhão	amapa-do-maranhao	2013-09-03 15:30:12.389704	1
1539	10	Amarante do Maranhão	amarante-do-maranhao	2013-09-03 15:30:12.389704	1
1540	10	Anajatuba	anajatuba	2013-09-03 15:30:12.389704	1
1541	10	Anapurus	anapurus	2013-09-03 15:30:12.389704	1
1542	10	Apicum-Açu	apicum-acu	2013-09-03 15:30:12.389704	1
1543	10	Araguanã	araguana	2013-09-03 15:30:12.389704	1
1544	10	Araioses	araioses	2013-09-03 15:30:12.389704	1
1545	10	Arame	arame	2013-09-03 15:30:12.389704	1
1546	10	Arari	arari	2013-09-03 15:30:12.389704	1
1547	10	Axixá	axixa	2013-09-03 15:30:12.389704	1
1548	10	Bacabal	bacabal	2013-09-03 15:30:12.389704	1
1549	10	Bacabeira	bacabeira	2013-09-03 15:30:12.389704	1
1550	10	Bacuri	bacuri	2013-09-03 15:30:12.389704	1
1551	10	Bacurituba	bacurituba	2013-09-03 15:30:12.389704	1
1552	10	Balsas	balsas	2013-09-03 15:30:12.389704	1
1553	10	Barão de Grajaú	barao-de-grajau	2013-09-03 15:30:12.389704	1
1554	10	Barra do Corda	barra-do-corda	2013-09-03 15:30:12.389704	1
1555	10	Barreirinhas	barreirinhas	2013-09-03 15:30:12.389704	1
1556	10	Bela Vista do Maranhão	bela-vista-do-maranhao	2013-09-03 15:30:12.389704	1
1557	10	Belágua	belagua	2013-09-03 15:30:12.389704	1
1558	10	Benedito Leite	benedito-leite	2013-09-03 15:30:12.389704	1
1559	10	Bequimão	bequimao	2013-09-03 15:30:12.389704	1
1560	10	Bernardo do Mearim	bernardo-do-mearim	2013-09-03 15:30:12.389704	1
1561	10	Boa Vista do Gurupi	boa-vista-do-gurupi	2013-09-03 15:30:12.389704	1
1562	10	Bom Jardim	bom-jardim	2013-09-03 15:30:12.389704	1
1563	10	Bom Jesus das Selvas	bom-jesus-das-selvas	2013-09-03 15:30:12.389704	1
1564	10	Bom Lugar	bom-lugar	2013-09-03 15:30:12.389704	1
1565	10	Brejo	brejo	2013-09-03 15:30:12.389704	1
1566	10	Brejo de Areia	brejo-de-areia	2013-09-03 15:30:12.389704	1
1567	10	Buriti	buriti	2013-09-03 15:30:12.389704	1
1568	10	Buriti Bravo	buriti-bravo	2013-09-03 15:30:12.389704	1
1569	10	Buriticupu	buriticupu	2013-09-03 15:30:12.389704	1
1570	10	Buritirana	buritirana	2013-09-03 15:30:12.389704	1
1571	10	Cachoeira Grande	cachoeira-grande	2013-09-03 15:30:12.389704	1
1572	10	Cajapió	cajapio	2013-09-03 15:30:12.389704	1
1573	10	Cajari	cajari	2013-09-03 15:30:12.389704	1
1574	10	Campestre do Maranhão	campestre-do-maranhao	2013-09-03 15:30:12.389704	1
1575	10	Cândido Mendes	candido-mendes	2013-09-03 15:30:12.389704	1
1576	10	Cantanhede	cantanhede	2013-09-03 15:30:12.389704	1
1577	10	Capinzal do Norte	capinzal-do-norte	2013-09-03 15:30:12.389704	1
1578	10	Carolina	carolina	2013-09-03 15:30:12.389704	1
1579	10	Carutapera	carutapera	2013-09-03 15:30:12.389704	1
1580	10	Caxias	caxias	2013-09-03 15:30:12.389704	1
1581	10	Cedral	cedral	2013-09-03 15:30:12.389704	1
1582	10	Central do Maranhão	central-do-maranhao	2013-09-03 15:30:12.389704	1
1583	10	Centro do Guilherme	centro-do-guilherme	2013-09-03 15:30:12.389704	1
1584	10	Centro Novo do Maranhão	centro-novo-do-maranhao	2013-09-03 15:30:12.389704	1
1585	10	Chapadinha	chapadinha	2013-09-03 15:30:12.389704	1
1586	10	Cidelândia	cidelandia	2013-09-03 15:30:12.389704	1
1587	10	Codó	codo	2013-09-03 15:30:12.389704	1
1588	10	Coelho Neto	coelho-neto	2013-09-03 15:30:12.389704	1
1589	10	Colinas	colinas	2013-09-03 15:30:12.389704	1
1590	10	Conceição do Lago-Açu	conceicao-do-lago-acu	2013-09-03 15:30:12.389704	1
1591	10	Coroatá	coroata	2013-09-03 15:30:12.389704	1
1592	10	Cururupu	cururupu	2013-09-03 15:30:12.389704	1
1593	10	Davinópolis	davinopolis	2013-09-03 15:30:12.389704	1
1594	10	Dom Pedro	dom-pedro	2013-09-03 15:30:12.389704	1
1595	10	Duque Bacelar	duque-bacelar	2013-09-03 15:30:12.389704	1
1596	10	Esperantinópolis	esperantinopolis	2013-09-03 15:30:12.389704	1
1597	10	Estreito	estreito	2013-09-03 15:30:12.389704	1
1598	10	Feira Nova do Maranhão	feira-nova-do-maranhao	2013-09-03 15:30:12.389704	1
1599	10	Fernando Falcão	fernando-falcao	2013-09-03 15:30:12.389704	1
1600	10	Formosa da Serra Negra	formosa-da-serra-negra	2013-09-03 15:30:12.389704	1
1601	10	Fortaleza dos Nogueiras	fortaleza-dos-nogueiras	2013-09-03 15:30:12.389704	1
1602	10	Fortuna	fortuna	2013-09-03 15:30:12.389704	1
1603	10	Godofredo Viana	godofredo-viana	2013-09-03 15:30:12.389704	1
1604	10	Gonçalves Dias	goncalves-dias	2013-09-03 15:30:12.389704	1
1605	10	Governador Archer	governador-archer	2013-09-03 15:30:12.389704	1
1606	10	Governador Edison Lobão	governador-edison-lobao	2013-09-03 15:30:12.389704	1
1607	10	Governador Eugênio Barros	governador-eugenio-barros	2013-09-03 15:30:12.389704	1
1608	10	Governador Luiz Rocha	governador-luiz-rocha	2013-09-03 15:30:12.389704	1
1609	10	Governador Newton Bello	governador-newton-bello	2013-09-03 15:30:12.389704	1
1610	10	Governador Nunes Freire	governador-nunes-freire	2013-09-03 15:30:12.389704	1
1611	10	Graça Aranha	graca-aranha	2013-09-03 15:30:12.389704	1
1612	10	Grajaú	grajau	2013-09-03 15:30:12.389704	1
1613	10	Guimarães	guimaraes	2013-09-03 15:30:12.389704	1
1614	10	Humberto de Campos	humberto-de-campos	2013-09-03 15:30:12.389704	1
1615	10	Icatu	icatu	2013-09-03 15:30:12.389704	1
1616	10	Igarapé do Meio	igarape-do-meio	2013-09-03 15:30:12.389704	1
1617	10	Igarapé Grande	igarape-grande	2013-09-03 15:30:12.389704	1
1618	10	Imperatriz	imperatriz	2013-09-03 15:30:12.389704	1
1619	10	Itaipava do Grajaú	itaipava-do-grajau	2013-09-03 15:30:12.389704	1
1620	10	Itapecuru Mirim	itapecuru-mirim	2013-09-03 15:30:12.389704	1
1621	10	Itinga do Maranhão	itinga-do-maranhao	2013-09-03 15:30:12.389704	1
1622	10	Jatobá	jatoba	2013-09-03 15:30:12.389704	1
1623	10	Jenipapo dos Vieiras	jenipapo-dos-vieiras	2013-09-03 15:30:12.389704	1
1624	10	João Lisboa	joao-lisboa	2013-09-03 15:30:12.389704	1
1625	10	Joselândia	joselandia	2013-09-03 15:30:12.389704	1
1626	10	Junco do Maranhão	junco-do-maranhao	2013-09-03 15:30:12.389704	1
1627	10	Lago da Pedra	lago-da-pedra	2013-09-03 15:30:12.389704	1
1628	10	Lago do Junco	lago-do-junco	2013-09-03 15:30:12.389704	1
1629	10	Lago dos Rodrigues	lago-dos-rodrigues	2013-09-03 15:30:12.389704	1
1630	10	Lago Verde	lago-verde	2013-09-03 15:30:12.389704	1
1631	10	Lagoa do Mato	lagoa-do-mato	2013-09-03 15:30:12.389704	1
1632	10	Lagoa Grande do Maranhão	lagoa-grande-do-maranhao	2013-09-03 15:30:12.389704	1
1633	10	Lajeado Novo	lajeado-novo	2013-09-03 15:30:12.389704	1
1634	10	Lima Campos	lima-campos	2013-09-03 15:30:12.389704	1
1635	10	Loreto	loreto	2013-09-03 15:30:12.389704	1
1636	10	Luís Domingues	luis-domingues	2013-09-03 15:30:12.389704	1
1637	10	Magalhães de Almeida	magalhaes-de-almeida	2013-09-03 15:30:12.389704	1
1638	10	Maracaçumé	maracacume	2013-09-03 15:30:12.389704	1
1639	10	Marajá do Sena	maraja-do-sena	2013-09-03 15:30:12.389704	1
1640	10	Maranhãozinho	maranhaozinho	2013-09-03 15:30:12.389704	1
1641	10	Mata Roma	mata-roma	2013-09-03 15:30:12.389704	1
1642	10	Matinha	matinha	2013-09-03 15:30:12.389704	1
1643	10	Matões	matoes	2013-09-03 15:30:12.389704	1
1644	10	Matões do Norte	matoes-do-norte	2013-09-03 15:30:12.389704	1
1645	10	Milagres do Maranhão	milagres-do-maranhao	2013-09-03 15:30:12.389704	1
1646	10	Mirador	mirador	2013-09-03 15:30:12.389704	1
1647	10	Miranda do Norte	miranda-do-norte	2013-09-03 15:30:12.389704	1
1648	10	Mirinzal	mirinzal	2013-09-03 15:30:12.389704	1
1649	10	Monção	moncao	2013-09-03 15:30:12.389704	1
1650	10	Montes Altos	montes-altos	2013-09-03 15:30:12.389704	1
1651	10	Morros	morros	2013-09-03 15:30:12.389704	1
1652	10	Nina Rodrigues	nina-rodrigues	2013-09-03 15:30:12.389704	1
1653	10	Nova Colinas	nova-colinas	2013-09-03 15:30:12.389704	1
1654	10	Nova Iorque	nova-iorque	2013-09-03 15:30:12.389704	1
1655	10	Nova Olinda do Maranhão	nova-olinda-do-maranhao	2013-09-03 15:30:12.389704	1
1656	10	Olho d`Água das Cunhãs	olho-d-agua-das-cunhas	2013-09-03 15:30:12.389704	1
1657	10	Olinda Nova do Maranhão	olinda-nova-do-maranhao	2013-09-03 15:30:12.389704	1
1658	10	Paço do Lumiar	paco-do-lumiar	2013-09-03 15:30:12.389704	1
1659	10	Palmeirândia	palmeirandia	2013-09-03 15:30:12.389704	1
1660	10	Paraibano	paraibano	2013-09-03 15:30:12.389704	1
1661	10	Parnarama	parnarama	2013-09-03 15:30:12.389704	1
1662	10	Passagem Franca	passagem-franca	2013-09-03 15:30:12.389704	1
1663	10	Pastos Bons	pastos-bons	2013-09-03 15:30:12.389704	1
1664	10	Paulino Neves	paulino-neves	2013-09-03 15:30:12.389704	1
1665	10	Paulo Ramos	paulo-ramos	2013-09-03 15:30:12.389704	1
1666	10	Pedreiras	pedreiras	2013-09-03 15:30:12.389704	1
1667	10	Pedro do Rosário	pedro-do-rosario	2013-09-03 15:30:12.389704	1
1668	10	Penalva	penalva	2013-09-03 15:30:12.389704	1
1669	10	Peri Mirim	peri-mirim	2013-09-03 15:30:12.389704	1
1670	10	Peritoró	peritoro	2013-09-03 15:30:12.389704	1
1671	10	Pindaré-Mirim	pindare-mirim	2013-09-03 15:30:12.389704	1
1672	10	Pinheiro	pinheiro	2013-09-03 15:30:12.389704	1
1673	10	Pio XII	pio-xii	2013-09-03 15:30:12.389704	1
1674	10	Pirapemas	pirapemas	2013-09-03 15:30:12.389704	1
1675	10	Poção de Pedras	pocao-de-pedras	2013-09-03 15:30:12.389704	1
1676	10	Porto Franco	porto-franco	2013-09-03 15:30:12.389704	1
1677	10	Porto Rico do Maranhão	porto-rico-do-maranhao	2013-09-03 15:30:12.389704	1
1678	10	Presidente Dutra	presidente-dutra	2013-09-03 15:30:12.389704	1
1679	10	Presidente Juscelino	presidente-juscelino	2013-09-03 15:30:12.389704	1
1680	10	Presidente Médici	presidente-medici	2013-09-03 15:30:12.389704	1
1681	10	Presidente Sarney	presidente-sarney	2013-09-03 15:30:12.389704	1
1682	10	Presidente Vargas	presidente-vargas	2013-09-03 15:30:12.389704	1
1683	10	Primeira Cruz	primeira-cruz	2013-09-03 15:30:12.389704	1
1684	10	Raposa	raposa	2013-09-03 15:30:12.389704	1
1685	10	Riachão	riachao	2013-09-03 15:30:12.389704	1
1686	10	Ribamar Fiquene	ribamar-fiquene	2013-09-03 15:30:12.389704	1
1687	10	Rosário	rosario	2013-09-03 15:30:12.389704	1
1688	10	Sambaíba	sambaiba	2013-09-03 15:30:12.389704	1
1689	10	Santa Filomena do Maranhão	santa-filomena-do-maranhao	2013-09-03 15:30:12.389704	1
1690	10	Santa Helena	santa-helena	2013-09-03 15:30:12.389704	1
1691	10	Santa Inês	santa-ines	2013-09-03 15:30:12.389704	1
1692	10	Santa Luzia	santa-luzia	2013-09-03 15:30:12.389704	1
1693	10	Santa Luzia do Paruá	santa-luzia-do-parua	2013-09-03 15:30:12.389704	1
1694	10	Santa Quitéria do Maranhão	santa-quiteria-do-maranhao	2013-09-03 15:30:12.389704	1
1695	10	Santa Rita	santa-rita	2013-09-03 15:30:12.389704	1
1696	10	Santana do Maranhão	santana-do-maranhao	2013-09-03 15:30:12.389704	1
1697	10	Santo Amaro do Maranhão	santo-amaro-do-maranhao	2013-09-03 15:30:12.389704	1
1698	10	Santo Antônio dos Lopes	santo-antonio-dos-lopes	2013-09-03 15:30:12.389704	1
1699	10	São Benedito do Rio Preto	sao-benedito-do-rio-preto	2013-09-03 15:30:12.389704	1
1700	10	São Bento	sao-bento	2013-09-03 15:30:12.389704	1
1701	10	São Bernardo	sao-bernardo	2013-09-03 15:30:12.389704	1
1702	10	São Domingos do Azeitão	sao-domingos-do-azeitao	2013-09-03 15:30:12.389704	1
1703	10	São Domingos do Maranhão	sao-domingos-do-maranhao	2013-09-03 15:30:12.389704	1
1704	10	São Félix de Balsas	sao-felix-de-balsas	2013-09-03 15:30:12.389704	1
1705	10	São Francisco do Brejão	sao-francisco-do-brejao	2013-09-03 15:30:12.389704	1
1706	10	São Francisco do Maranhão	sao-francisco-do-maranhao	2013-09-03 15:30:12.389704	1
1707	10	São João Batista	sao-joao-batista	2013-09-03 15:30:12.389704	1
1708	10	São João do Carú	sao-joao-do-caru	2013-09-03 15:30:12.389704	1
1709	10	São João do Paraíso	sao-joao-do-paraiso	2013-09-03 15:30:12.389704	1
1710	10	São João do Soter	sao-joao-do-soter	2013-09-03 15:30:12.389704	1
1711	10	São João dos Patos	sao-joao-dos-patos	2013-09-03 15:30:12.389704	1
1712	10	São José de Ribamar	sao-jose-de-ribamar	2013-09-03 15:30:12.389704	1
1713	10	São José dos Basílios	sao-jose-dos-basilios	2013-09-03 15:30:12.389704	1
3027	13	Inhapim	inhapim	2013-09-03 15:30:12.389704	1
1714	10	São Luís	sao-luis	2013-09-03 15:30:12.389704	1
1715	10	São Luís Gonzaga do Maranhão	sao-luis-gonzaga-do-maranhao	2013-09-03 15:30:12.389704	1
1716	10	São Mateus do Maranhão	sao-mateus-do-maranhao	2013-09-03 15:30:12.389704	1
1717	10	São Pedro da Água Branca	sao-pedro-da-agua-branca	2013-09-03 15:30:12.389704	1
1718	10	São Pedro dos Crentes	sao-pedro-dos-crentes	2013-09-03 15:30:12.389704	1
1719	10	São Raimundo das Mangabeiras	sao-raimundo-das-mangabeiras	2013-09-03 15:30:12.389704	1
1720	10	São Raimundo do Doca Bezerra	sao-raimundo-do-doca-bezerra	2013-09-03 15:30:12.389704	1
1721	10	São Roberto	sao-roberto	2013-09-03 15:30:12.389704	1
1722	10	São Vicente Ferrer	sao-vicente-ferrer	2013-09-03 15:30:12.389704	1
1723	10	Satubinha	satubinha	2013-09-03 15:30:12.389704	1
1724	10	Senador Alexandre Costa	senador-alexandre-costa	2013-09-03 15:30:12.389704	1
1725	10	Senador La Rocque	senador-la-rocque	2013-09-03 15:30:12.389704	1
1726	10	Serrano do Maranhão	serrano-do-maranhao	2013-09-03 15:30:12.389704	1
1727	10	Sítio Novo	sitio-novo	2013-09-03 15:30:12.389704	1
1728	10	Sucupira do Norte	sucupira-do-norte	2013-09-03 15:30:12.389704	1
1729	10	Sucupira do Riachão	sucupira-do-riachao	2013-09-03 15:30:12.389704	1
1730	10	Tasso Fragoso	tasso-fragoso	2013-09-03 15:30:12.389704	1
1731	10	Timbiras	timbiras	2013-09-03 15:30:12.389704	1
1732	10	Timon	timon	2013-09-03 15:30:12.389704	1
1733	10	Trizidela do Vale	trizidela-do-vale	2013-09-03 15:30:12.389704	1
1734	10	Tufilândia	tufilandia	2013-09-03 15:30:12.389704	1
1735	10	Tuntum	tuntum	2013-09-03 15:30:12.389704	1
1736	10	Turiaçu	turiacu	2013-09-03 15:30:12.389704	1
1737	10	Turilândia	turilandia	2013-09-03 15:30:12.389704	1
1738	10	Tutóia	tutoia	2013-09-03 15:30:12.389704	1
1739	10	Urbano Santos	urbano-santos	2013-09-03 15:30:12.389704	1
1740	10	Vargem Grande	vargem-grande	2013-09-03 15:30:12.389704	1
1741	10	Viana	viana	2013-09-03 15:30:12.389704	1
1742	10	Vila Nova dos Martírios	vila-nova-dos-martirios	2013-09-03 15:30:12.389704	1
1743	10	Vitória do Mearim	vitoria-do-mearim	2013-09-03 15:30:12.389704	1
1744	10	Vitorino Freire	vitorino-freire	2013-09-03 15:30:12.389704	1
1745	10	Zé Doca	ze-doca	2013-09-03 15:30:12.389704	1
2294	11	Acorizal	acorizal	2013-09-03 15:30:12.389704	1
2295	11	Água Boa	agua-boa	2013-09-03 15:30:12.389704	1
2296	11	Alta Floresta	alta-floresta	2013-09-03 15:30:12.389704	1
2297	11	Alto Araguaia	alto-araguaia	2013-09-03 15:30:12.389704	1
2298	11	Alto Boa Vista	alto-boa-vista	2013-09-03 15:30:12.389704	1
2299	11	Alto Garças	alto-garcas	2013-09-03 15:30:12.389704	1
2300	11	Alto Paraguai	alto-paraguai	2013-09-03 15:30:12.389704	1
2301	11	Alto Taquari	alto-taquari	2013-09-03 15:30:12.389704	1
2302	11	Apiacás	apiacas	2013-09-03 15:30:12.389704	1
2303	11	Araguaiana	araguaiana	2013-09-03 15:30:12.389704	1
2304	11	Araguainha	araguainha	2013-09-03 15:30:12.389704	1
2305	11	Araputanga	araputanga	2013-09-03 15:30:12.389704	1
2306	11	Arenápolis	arenapolis	2013-09-03 15:30:12.389704	1
2307	11	Aripuanã	aripuana	2013-09-03 15:30:12.389704	1
2308	11	Barão de Melgaço	barao-de-melgaco	2013-09-03 15:30:12.389704	1
2309	11	Barra do Bugres	barra-do-bugres	2013-09-03 15:30:12.389704	1
2310	11	Barra do Garças	barra-do-garcas	2013-09-03 15:30:12.389704	1
2311	11	Bom Jesus do Araguaia	bom-jesus-do-araguaia	2013-09-03 15:30:12.389704	1
2312	11	Brasnorte	brasnorte	2013-09-03 15:30:12.389704	1
2313	11	Cáceres	caceres	2013-09-03 15:30:12.389704	1
2314	11	Campinápolis	campinapolis	2013-09-03 15:30:12.389704	1
2315	11	Campo Novo do Parecis	campo-novo-do-parecis	2013-09-03 15:30:12.389704	1
2316	11	Campo Verde	campo-verde	2013-09-03 15:30:12.389704	1
2317	11	Campos de Júlio	campos-de-julio	2013-09-03 15:30:12.389704	1
2318	11	Canabrava do Norte	canabrava-do-norte	2013-09-03 15:30:12.389704	1
2319	11	Canarana	canarana	2013-09-03 15:30:12.389704	1
2320	11	Carlinda	carlinda	2013-09-03 15:30:12.389704	1
2321	11	Castanheira	castanheira	2013-09-03 15:30:12.389704	1
2322	11	Chapada dos Guimarães	chapada-dos-guimaraes	2013-09-03 15:30:12.389704	1
2323	11	Cláudia	claudia	2013-09-03 15:30:12.389704	1
2324	11	Cocalinho	cocalinho	2013-09-03 15:30:12.389704	1
2325	11	Colíder	colider	2013-09-03 15:30:12.389704	1
2326	11	Colniza	colniza	2013-09-03 15:30:12.389704	1
2327	11	Comodoro	comodoro	2013-09-03 15:30:12.389704	1
2328	11	Confresa	confresa	2013-09-03 15:30:12.389704	1
2329	11	Conquista d`Oeste	conquista-d-oeste	2013-09-03 15:30:12.389704	1
2330	11	Cotriguaçu	cotriguacu	2013-09-03 15:30:12.389704	1
2331	11	Cuiabá	cuiaba	2013-09-03 15:30:12.389704	1
2332	11	Curvelândia	curvelandia	2013-09-03 15:30:12.389704	1
2333	11	Curvelândia	curvelandia	2013-09-03 15:30:12.389704	1
2334	11	Denise	denise	2013-09-03 15:30:12.389704	1
2335	11	Diamantino	diamantino	2013-09-03 15:30:12.389704	1
2336	11	Dom Aquino	dom-aquino	2013-09-03 15:30:12.389704	1
2337	11	Feliz Natal	feliz-natal	2013-09-03 15:30:12.389704	1
2338	11	Figueirópolis d`Oeste	figueiropolis-d-oeste	2013-09-03 15:30:12.389704	1
2339	11	Gaúcha do Norte	gaucha-do-norte	2013-09-03 15:30:12.389704	1
2340	11	General Carneiro	general-carneiro	2013-09-03 15:30:12.389704	1
2341	11	Glória d`Oeste	gloria-d-oeste	2013-09-03 15:30:12.389704	1
2342	11	Guarantã do Norte	guaranta-do-norte	2013-09-03 15:30:12.389704	1
2343	11	Guiratinga	guiratinga	2013-09-03 15:30:12.389704	1
2344	11	Indiavaí	indiavai	2013-09-03 15:30:12.389704	1
2345	11	Ipiranga do Norte	ipiranga-do-norte	2013-09-03 15:30:12.389704	1
2346	11	Itanhangá	itanhanga	2013-09-03 15:30:12.389704	1
2347	11	Itaúba	itauba	2013-09-03 15:30:12.389704	1
2348	11	Itiquira	itiquira	2013-09-03 15:30:12.389704	1
2349	11	Jaciara	jaciara	2013-09-03 15:30:12.389704	1
2350	11	Jangada	jangada	2013-09-03 15:30:12.389704	1
2351	11	Jauru	jauru	2013-09-03 15:30:12.389704	1
2352	11	Juara	juara	2013-09-03 15:30:12.389704	1
2353	11	Juína	juina	2013-09-03 15:30:12.389704	1
2354	11	Juruena	juruena	2013-09-03 15:30:12.389704	1
2355	11	Juscimeira	juscimeira	2013-09-03 15:30:12.389704	1
2356	11	Lambari d`Oeste	lambari-d-oeste	2013-09-03 15:30:12.389704	1
2357	11	Lucas do Rio Verde	lucas-do-rio-verde	2013-09-03 15:30:12.389704	1
2358	11	Luciára	luciara	2013-09-03 15:30:12.389704	1
2359	11	Marcelândia	marcelandia	2013-09-03 15:30:12.389704	1
2360	11	Matupá	matupa	2013-09-03 15:30:12.389704	1
3028	13	Inhaúma	inhauma	2013-09-03 15:30:12.389704	1
2361	11	Mirassol d`Oeste	mirassol-d-oeste	2013-09-03 15:30:12.389704	1
2363	11	Nortelândia	nortelandia	2013-09-03 15:30:12.389704	1
2364	11	Nossa Senhora do Livramento	nossa-senhora-do-livramento	2013-09-03 15:30:12.389704	1
2365	11	Nova Bandeirantes	nova-bandeirantes	2013-09-03 15:30:12.389704	1
2366	11	Nova Brasilândia	nova-brasilandia	2013-09-03 15:30:12.389704	1
2367	11	Nova Canaã do Norte	nova-canaa-do-norte	2013-09-03 15:30:12.389704	1
2368	11	Nova Guarita	nova-guarita	2013-09-03 15:30:12.389704	1
2369	11	Nova Lacerda	nova-lacerda	2013-09-03 15:30:12.389704	1
2370	11	Nova Marilândia	nova-marilandia	2013-09-03 15:30:12.389704	1
2371	11	Nova Maringá	nova-maringa	2013-09-03 15:30:12.389704	1
2372	11	Nova Monte verde	nova-monte-verde	2013-09-03 15:30:12.389704	1
2373	11	Nova Mutum	nova-mutum	2013-09-03 15:30:12.389704	1
2374	11	Nova Olímpia	nova-olimpia	2013-09-03 15:30:12.389704	1
2375	11	Nova Santa Helena	nova-santa-helena	2013-09-03 15:30:12.389704	1
2376	11	Nova Ubiratã	nova-ubirata	2013-09-03 15:30:12.389704	1
2377	11	Nova Xavantina	nova-xavantina	2013-09-03 15:30:12.389704	1
2378	11	Novo Horizonte do Norte	novo-horizonte-do-norte	2013-09-03 15:30:12.389704	1
2379	11	Novo Mundo	novo-mundo	2013-09-03 15:30:12.389704	1
2380	11	Novo Santo Antônio	novo-santo-antonio	2013-09-03 15:30:12.389704	1
2381	11	Novo São Joaquim	novo-sao-joaquim	2013-09-03 15:30:12.389704	1
2382	11	Paranaíta	paranaita	2013-09-03 15:30:12.389704	1
2383	11	Paranatinga	paranatinga	2013-09-03 15:30:12.389704	1
2384	11	Pedra Preta	pedra-preta	2013-09-03 15:30:12.389704	1
2385	11	Peixoto de Azevedo	peixoto-de-azevedo	2013-09-03 15:30:12.389704	1
2386	11	Planalto da Serra	planalto-da-serra	2013-09-03 15:30:12.389704	1
2387	11	Poconé	pocone	2013-09-03 15:30:12.389704	1
2388	11	Pontal do Araguaia	pontal-do-araguaia	2013-09-03 15:30:12.389704	1
2389	11	Ponte Branca	ponte-branca	2013-09-03 15:30:12.389704	1
2390	11	Pontes e Lacerda	pontes-e-lacerda	2013-09-03 15:30:12.389704	1
2391	11	Porto Alegre do Norte	porto-alegre-do-norte	2013-09-03 15:30:12.389704	1
2392	11	Porto dos Gaúchos	porto-dos-gauchos	2013-09-03 15:30:12.389704	1
2393	11	Porto Esperidião	porto-esperidiao	2013-09-03 15:30:12.389704	1
2394	11	Porto Estrela	porto-estrela	2013-09-03 15:30:12.389704	1
2395	11	Poxoréo	poxoreo	2013-09-03 15:30:12.389704	1
2396	11	Primavera do Leste	primavera-do-leste	2013-09-03 15:30:12.389704	1
2397	11	Querência	querencia	2013-09-03 15:30:12.389704	1
2398	11	Reserva do Cabaçal	reserva-do-cabacal	2013-09-03 15:30:12.389704	1
2399	11	Ribeirão Cascalheira	ribeirao-cascalheira	2013-09-03 15:30:12.389704	1
2400	11	Ribeirãozinho	ribeiraozinho	2013-09-03 15:30:12.389704	1
2401	11	Rio Branco	rio-branco	2013-09-03 15:30:12.389704	1
2402	11	Rondolândia	rondolandia	2013-09-03 15:30:12.389704	1
2403	11	Rondonópolis	rondonopolis	2013-09-03 15:30:12.389704	1
2404	11	Rosário Oeste	rosario-oeste	2013-09-03 15:30:12.389704	1
2405	11	Salto do Céu	salto-do-ceu	2013-09-03 15:30:12.389704	1
2406	11	Santa Carmem	santa-carmem	2013-09-03 15:30:12.389704	1
2407	11	Santa Cruz do Xingu	santa-cruz-do-xingu	2013-09-03 15:30:12.389704	1
2408	11	Santa Rita do Trivelato	santa-rita-do-trivelato	2013-09-03 15:30:12.389704	1
2409	11	Santa Terezinha	santa-terezinha	2013-09-03 15:30:12.389704	1
2410	11	Santo Afonso	santo-afonso	2013-09-03 15:30:12.389704	1
2411	11	Santo Antônio do Leste	santo-antonio-do-leste	2013-09-03 15:30:12.389704	1
2412	11	Santo Antônio do Leverger	santo-antonio-do-leverger	2013-09-03 15:30:12.389704	1
2413	11	São Félix do Araguaia	sao-felix-do-araguaia	2013-09-03 15:30:12.389704	1
2414	11	São José do Povo	sao-jose-do-povo	2013-09-03 15:30:12.389704	1
2415	11	São José do Rio Claro	sao-jose-do-rio-claro	2013-09-03 15:30:12.389704	1
2416	11	São José do Xingu	sao-jose-do-xingu	2013-09-03 15:30:12.389704	1
2417	11	São José dos Quatro Marcos	sao-jose-dos-quatro-marcos	2013-09-03 15:30:12.389704	1
2418	11	São Pedro da Cipa	sao-pedro-da-cipa	2013-09-03 15:30:12.389704	1
2419	11	Sapezal	sapezal	2013-09-03 15:30:12.389704	1
2420	11	Serra Nova Dourada	serra-nova-dourada	2013-09-03 15:30:12.389704	1
2421	11	Sinop	sinop	2013-09-03 15:30:12.389704	1
2422	11	Sorriso	sorriso	2013-09-03 15:30:12.389704	1
2423	11	Tabaporã	tabapora	2013-09-03 15:30:12.389704	1
2424	11	Tangará da Serra	tangara-da-serra	2013-09-03 15:30:12.389704	1
2425	11	Tapurah	tapurah	2013-09-03 15:30:12.389704	1
2426	11	Terra Nova do Norte	terra-nova-do-norte	2013-09-03 15:30:12.389704	1
2427	11	Tesouro	tesouro	2013-09-03 15:30:12.389704	1
2428	11	Torixoréu	torixoreu	2013-09-03 15:30:12.389704	1
2429	11	União do Sul	uniao-do-sul	2013-09-03 15:30:12.389704	1
2430	11	Vale de São Domingos	vale-de-sao-domingos	2013-09-03 15:30:12.389704	1
2431	11	Várzea Grande	varzea-grande	2013-09-03 15:30:12.389704	1
2432	11	Vera	vera	2013-09-03 15:30:12.389704	1
2433	11	Vila Bela da Santíssima Trindade	vila-bela-da-santissima-trindade	2013-09-03 15:30:12.389704	1
2434	11	Vila Rica	vila-rica	2013-09-03 15:30:12.389704	1
2549	12	Água Clara	agua-clara	2013-09-03 15:30:12.389704	1
2550	12	Alcinópolis	alcinopolis	2013-09-03 15:30:12.389704	1
2551	12	Amambaí	amambai	2013-09-03 15:30:12.389704	1
2552	12	Anastácio	anastacio	2013-09-03 15:30:12.389704	1
2553	12	Anaurilândia	anaurilandia	2013-09-03 15:30:12.389704	1
2554	12	Angélica	angelica	2013-09-03 15:30:12.389704	1
2555	12	Antônio João	antonio-joao	2013-09-03 15:30:12.389704	1
2556	12	Aparecida do Taboado	aparecida-do-taboado	2013-09-03 15:30:12.389704	1
2557	12	Aquidauana	aquidauana	2013-09-03 15:30:12.389704	1
2558	12	Aral Moreira	aral-moreira	2013-09-03 15:30:12.389704	1
2559	12	Bandeirantes	bandeirantes	2013-09-03 15:30:12.389704	1
2560	12	Bataguassu	bataguassu	2013-09-03 15:30:12.389704	1
2561	12	Bataiporã	bataipora	2013-09-03 15:30:12.389704	1
2562	12	Bela Vista	bela-vista	2013-09-03 15:30:12.389704	1
2563	12	Bodoquena	bodoquena	2013-09-03 15:30:12.389704	1
2564	12	Bonito	bonito	2013-09-03 15:30:12.389704	1
2565	12	Brasilândia	brasilandia	2013-09-03 15:30:12.389704	1
2566	12	Caarapó	caarapo	2013-09-03 15:30:12.389704	1
2567	12	Camapuã	camapua	2013-09-03 15:30:12.389704	1
2568	12	Campo Grande	campo-grande	2013-09-03 15:30:12.389704	1
2569	12	Caracol	caracol	2013-09-03 15:30:12.389704	1
2570	12	Cassilândia	cassilandia	2013-09-03 15:30:12.389704	1
2571	12	Chapadão do Sul	chapadao-do-sul	2013-09-03 15:30:12.389704	1
2572	12	Corguinho	corguinho	2013-09-03 15:30:12.389704	1
2573	12	Coronel Sapucaia	coronel-sapucaia	2013-09-03 15:30:12.389704	1
2574	12	Corumbá	corumba	2013-09-03 15:30:12.389704	1
2575	12	Costa Rica	costa-rica	2013-09-03 15:30:12.389704	1
2576	12	Coxim	coxim	2013-09-03 15:30:12.389704	1
2577	12	Deodápolis	deodapolis	2013-09-03 15:30:12.389704	1
2578	12	Dois Irmãos do Buriti	dois-irmaos-do-buriti	2013-09-03 15:30:12.389704	1
2579	12	Douradina	douradina	2013-09-03 15:30:12.389704	1
2580	12	Dourados	dourados	2013-09-03 15:30:12.389704	1
2581	12	Eldorado	eldorado	2013-09-03 15:30:12.389704	1
2582	12	Fátima do Sul	fatima-do-sul	2013-09-03 15:30:12.389704	1
2583	12	Figueirão	figueirao	2013-09-03 15:30:12.389704	1
2584	12	Glória de Dourados	gloria-de-dourados	2013-09-03 15:30:12.389704	1
2585	12	Guia Lopes da Laguna	guia-lopes-da-laguna	2013-09-03 15:30:12.389704	1
2586	12	Iguatemi	iguatemi	2013-09-03 15:30:12.389704	1
2587	12	Inocência	inocencia	2013-09-03 15:30:12.389704	1
2588	12	Itaporã	itapora	2013-09-03 15:30:12.389704	1
2589	12	Itaquiraí	itaquirai	2013-09-03 15:30:12.389704	1
2590	12	Ivinhema	ivinhema	2013-09-03 15:30:12.389704	1
2591	12	Japorã	japora	2013-09-03 15:30:12.389704	1
2592	12	Jaraguari	jaraguari	2013-09-03 15:30:12.389704	1
2593	12	Jardim	jardim	2013-09-03 15:30:12.389704	1
2594	12	Jateí	jatei	2013-09-03 15:30:12.389704	1
2595	12	Juti	juti	2013-09-03 15:30:12.389704	1
2596	12	Ladário	ladario	2013-09-03 15:30:12.389704	1
2597	12	Laguna Carapã	laguna-carapa	2013-09-03 15:30:12.389704	1
2598	12	Maracaju	maracaju	2013-09-03 15:30:12.389704	1
2599	12	Miranda	miranda	2013-09-03 15:30:12.389704	1
2600	12	Mundo Novo	mundo-novo	2013-09-03 15:30:12.389704	1
2601	12	Naviraí	navirai	2013-09-03 15:30:12.389704	1
2602	12	Nioaque	nioaque	2013-09-03 15:30:12.389704	1
2603	12	Nova Alvorada do Sul	nova-alvorada-do-sul	2013-09-03 15:30:12.389704	1
2604	12	Nova Andradina	nova-andradina	2013-09-03 15:30:12.389704	1
2605	12	Novo Horizonte do Sul	novo-horizonte-do-sul	2013-09-03 15:30:12.389704	1
2606	12	Paranaíba	paranaiba	2013-09-03 15:30:12.389704	1
2607	12	Paranhos	paranhos	2013-09-03 15:30:12.389704	1
2608	12	Pedro Gomes	pedro-gomes	2013-09-03 15:30:12.389704	1
2609	12	Ponta Porã	ponta-pora	2013-09-03 15:30:12.389704	1
2610	12	Porto Murtinho	porto-murtinho	2013-09-03 15:30:12.389704	1
2611	12	Ribas do Rio Pardo	ribas-do-rio-pardo	2013-09-03 15:30:12.389704	1
2612	12	Rio Brilhante	rio-brilhante	2013-09-03 15:30:12.389704	1
2613	12	Rio Negro	rio-negro	2013-09-03 15:30:12.389704	1
2614	12	Rio Verde de Mato Grosso	rio-verde-de-mato-grosso	2013-09-03 15:30:12.389704	1
2615	12	Rochedo	rochedo	2013-09-03 15:30:12.389704	1
2616	12	Santa Rita do Pardo	santa-rita-do-pardo	2013-09-03 15:30:12.389704	1
2617	12	São Gabriel do Oeste	sao-gabriel-do-oeste	2013-09-03 15:30:12.389704	1
2618	12	Selvíria	selviria	2013-09-03 15:30:12.389704	1
2619	12	Sete Quedas	sete-quedas	2013-09-03 15:30:12.389704	1
2620	12	Sidrolândia	sidrolandia	2013-09-03 15:30:12.389704	1
2621	12	Sonora	sonora	2013-09-03 15:30:12.389704	1
2622	12	Tacuru	tacuru	2013-09-03 15:30:12.389704	1
2623	12	Taquarussu	taquarussu	2013-09-03 15:30:12.389704	1
2624	12	Terenos	terenos	2013-09-03 15:30:12.389704	1
2625	12	Três Lagoas	tres-lagoas	2013-09-03 15:30:12.389704	1
2626	12	Vicentina	vicentina	2013-09-03 15:30:12.389704	1
2676	13	Abadia dos Dourados	abadia-dos-dourados	2013-09-03 15:30:12.389704	1
2677	13	Abaeté	abaete	2013-09-03 15:30:12.389704	1
2678	13	Abre Campo	abre-campo	2013-09-03 15:30:12.389704	1
2679	13	Acaiaca	acaiaca	2013-09-03 15:30:12.389704	1
2680	13	Açucena	acucena	2013-09-03 15:30:12.389704	1
2681	13	Água Boa	agua-boa	2013-09-03 15:30:12.389704	1
2682	13	Água Comprida	agua-comprida	2013-09-03 15:30:12.389704	1
2683	13	Aguanil	aguanil	2013-09-03 15:30:12.389704	1
2684	13	Águas Formosas	aguas-formosas	2013-09-03 15:30:12.389704	1
2685	13	Águas Vermelhas	aguas-vermelhas	2013-09-03 15:30:12.389704	1
2686	13	Aimorés	aimores	2013-09-03 15:30:12.389704	1
2687	13	Aiuruoca	aiuruoca	2013-09-03 15:30:12.389704	1
2688	13	Alagoa	alagoa	2013-09-03 15:30:12.389704	1
2689	13	Albertina	albertina	2013-09-03 15:30:12.389704	1
2690	13	Além Paraíba	alem-paraiba	2013-09-03 15:30:12.389704	1
2691	13	Alfenas	alfenas	2013-09-03 15:30:12.389704	1
2692	13	Alfredo Vasconcelos	alfredo-vasconcelos	2013-09-03 15:30:12.389704	1
2693	13	Almenara	almenara	2013-09-03 15:30:12.389704	1
2694	13	Alpercata	alpercata	2013-09-03 15:30:12.389704	1
2695	13	Alpinópolis	alpinopolis	2013-09-03 15:30:12.389704	1
2696	13	Alterosa	alterosa	2013-09-03 15:30:12.389704	1
2697	13	Alto Caparaó	alto-caparao	2013-09-03 15:30:12.389704	1
2698	13	Alto Jequitibá	alto-jequitiba	2013-09-03 15:30:12.389704	1
2699	13	Alto Rio Doce	alto-rio-doce	2013-09-03 15:30:12.389704	1
2700	13	Alvarenga	alvarenga	2013-09-03 15:30:12.389704	1
2701	13	Alvinópolis	alvinopolis	2013-09-03 15:30:12.389704	1
2702	13	Alvorada de Minas	alvorada-de-minas	2013-09-03 15:30:12.389704	1
2703	13	Amparo do Serra	amparo-do-serra	2013-09-03 15:30:12.389704	1
2704	13	Andradas	andradas	2013-09-03 15:30:12.389704	1
2705	13	Andrelândia	andrelandia	2013-09-03 15:30:12.389704	1
2706	13	Angelândia	angelandia	2013-09-03 15:30:12.389704	1
2707	13	Antônio Carlos	antonio-carlos	2013-09-03 15:30:12.389704	1
2708	13	Antônio Dias	antonio-dias	2013-09-03 15:30:12.389704	1
2709	13	Antônio Prado de Minas	antonio-prado-de-minas	2013-09-03 15:30:12.389704	1
2710	13	Araçaí	aracai	2013-09-03 15:30:12.389704	1
2711	13	Aracitaba	aracitaba	2013-09-03 15:30:12.389704	1
2712	13	Araçuaí	aracuai	2013-09-03 15:30:12.389704	1
2713	13	Araguari	araguari	2013-09-03 15:30:12.389704	1
2714	13	Arantina	arantina	2013-09-03 15:30:12.389704	1
2715	13	Araponga	araponga	2013-09-03 15:30:12.389704	1
2716	13	Araporã	arapora	2013-09-03 15:30:12.389704	1
2717	13	Arapuá	arapua	2013-09-03 15:30:12.389704	1
2718	13	Araújos	araujos	2013-09-03 15:30:12.389704	1
2719	13	Araxá	araxa	2013-09-03 15:30:12.389704	1
2720	13	Arceburgo	arceburgo	2013-09-03 15:30:12.389704	1
2721	13	Arcos	arcos	2013-09-03 15:30:12.389704	1
2722	13	Areado	areado	2013-09-03 15:30:12.389704	1
2723	13	Argirita	argirita	2013-09-03 15:30:12.389704	1
2724	13	Aricanduva	aricanduva	2013-09-03 15:30:12.389704	1
2725	13	Arinos	arinos	2013-09-03 15:30:12.389704	1
2726	13	Astolfo Dutra	astolfo-dutra	2013-09-03 15:30:12.389704	1
2727	13	Ataléia	ataleia	2013-09-03 15:30:12.389704	1
2728	13	Augusto de Lima	augusto-de-lima	2013-09-03 15:30:12.389704	1
2729	13	Baependi	baependi	2013-09-03 15:30:12.389704	1
2730	13	\nBaldim	baldim	2013-09-03 15:30:12.389704	1
2731	13	Bambuí	bambui	2013-09-03 15:30:12.389704	1
2732	13	Bandeira	bandeira	2013-09-03 15:30:12.389704	1
2733	13	Bandeira do Sul	bandeira-do-sul	2013-09-03 15:30:12.389704	1
2734	13	Barão de Cocais	barao-de-cocais	2013-09-03 15:30:12.389704	1
2735	13	Barão de Monte Alto	barao-de-monte-alto	2013-09-03 15:30:12.389704	1
2736	13	Barbacena	barbacena	2013-09-03 15:30:12.389704	1
2737	13	Barra Longa	barra-longa	2013-09-03 15:30:12.389704	1
2738	13	Barroso	barroso	2013-09-03 15:30:12.389704	1
2739	13	Bela Vista de Minas	bela-vista-de-minas	2013-09-03 15:30:12.389704	1
2740	13	Belmiro Braga	belmiro-braga	2013-09-03 15:30:12.389704	1
2741	13	Belo Horizonte	belo-horizonte	2013-09-03 15:30:12.389704	1
2742	13	Belo Oriente	belo-oriente	2013-09-03 15:30:12.389704	1
2743	13	Belo Vale	belo-vale	2013-09-03 15:30:12.389704	1
2744	13	Berilo	berilo	2013-09-03 15:30:12.389704	1
2745	13	Berizal	berizal	2013-09-03 15:30:12.389704	1
2746	13	Bertópolis	bertopolis	2013-09-03 15:30:12.389704	1
2747	13	Betim	betim	2013-09-03 15:30:12.389704	1
2748	13	Bias Fortes	bias-fortes	2013-09-03 15:30:12.389704	1
2749	13	Bicas	bicas	2013-09-03 15:30:12.389704	1
2750	13	Biquinhas	biquinhas	2013-09-03 15:30:12.389704	1
2751	13	Boa Esperança	boa-esperanca	2013-09-03 15:30:12.389704	1
2752	13	Bocaina de Minas	bocaina-de-minas	2013-09-03 15:30:12.389704	1
2753	13	Bocaiúva	bocaiuva	2013-09-03 15:30:12.389704	1
2754	13	Bom Despacho	bom-despacho	2013-09-03 15:30:12.389704	1
2755	13	Bom Jardim de Minas	bom-jardim-de-minas	2013-09-03 15:30:12.389704	1
2756	13	Bom Jesus da Penha	bom-jesus-da-penha	2013-09-03 15:30:12.389704	1
2757	13	Bom Jesus do Amparo	bom-jesus-do-amparo	2013-09-03 15:30:12.389704	1
2758	13	Bom Jesus do Galho	bom-jesus-do-galho	2013-09-03 15:30:12.389704	1
2759	13	Bom Repouso	bom-repouso	2013-09-03 15:30:12.389704	1
2760	13	Bom Sucesso	bom-sucesso	2013-09-03 15:30:12.389704	1
2761	13	Bonfim	bonfim	2013-09-03 15:30:12.389704	1
2762	13	Bonfinópolis de Minas	bonfinopolis-de-minas	2013-09-03 15:30:12.389704	1
2763	13	Bonito de Minas	bonito-de-minas	2013-09-03 15:30:12.389704	1
2764	13	Borda da Mata	borda-da-mata	2013-09-03 15:30:12.389704	1
2765	13	Botelhos	botelhos	2013-09-03 15:30:12.389704	1
2766	13	Botumirim	botumirim	2013-09-03 15:30:12.389704	1
2767	13	Brás Pires	bras-pires	2013-09-03 15:30:12.389704	1
2768	13	Brasilândia de Minas	brasilandia-de-minas	2013-09-03 15:30:12.389704	1
2769	13	Brasília de Minas	brasilia-de-minas	2013-09-03 15:30:12.389704	1
2770	13	Brasópolis	brasopolis	2013-09-03 15:30:12.389704	1
2771	13	Braúnas	braunas	2013-09-03 15:30:12.389704	1
2772	13	Brumadinho	brumadinho	2013-09-03 15:30:12.389704	1
2773	13	Bueno Brandão	bueno-brandao	2013-09-03 15:30:12.389704	1
2774	13	Buenópolis	buenopolis	2013-09-03 15:30:12.389704	1
2775	13	Bugre	bugre	2013-09-03 15:30:12.389704	1
2776	13	Buritis	buritis	2013-09-03 15:30:12.389704	1
2777	13	Buritizeiro	buritizeiro	2013-09-03 15:30:12.389704	1
2778	13	Cabeceira Grande	cabeceira-grande	2013-09-03 15:30:12.389704	1
2779	13	Cabo Verde	cabo-verde	2013-09-03 15:30:12.389704	1
2780	13	Cachoeira da Prata	cachoeira-da-prata	2013-09-03 15:30:12.389704	1
2781	13	Cachoeira de Minas	cachoeira-de-minas	2013-09-03 15:30:12.389704	1
2782	13	Cachoeira de Pajeú	cachoeira-de-pajeu	2013-09-03 15:30:12.389704	1
2783	13	Cachoeira Dourada	cachoeira-dourada	2013-09-03 15:30:12.389704	1
2784	13	Caetanópolis	caetanopolis	2013-09-03 15:30:12.389704	1
2785	13	Caeté	caete	2013-09-03 15:30:12.389704	1
2786	13	Caiana	caiana	2013-09-03 15:30:12.389704	1
2787	13	Cajuri	cajuri	2013-09-03 15:30:12.389704	1
2788	13	Caldas	caldas	2013-09-03 15:30:12.389704	1
2789	13	Camacho	camacho	2013-09-03 15:30:12.389704	1
2790	13	Camanducaia	camanducaia	2013-09-03 15:30:12.389704	1
2791	13	Cambuí	cambui	2013-09-03 15:30:12.389704	1
2792	13	Cambuquira	cambuquira	2013-09-03 15:30:12.389704	1
2793	13	Campanário	campanario	2013-09-03 15:30:12.389704	1
2794	13	Campanha	campanha	2013-09-03 15:30:12.389704	1
2795	13	Campestre	campestre	2013-09-03 15:30:12.389704	1
2796	13	Campina Verde	campina-verde	2013-09-03 15:30:12.389704	1
2797	13	Campo Azul	campo-azul	2013-09-03 15:30:12.389704	1
2798	13	Campo Belo	campo-belo	2013-09-03 15:30:12.389704	1
2799	13	Campo do Meio	campo-do-meio	2013-09-03 15:30:12.389704	1
2800	13	Campo Florido	campo-florido	2013-09-03 15:30:12.389704	1
2801	13	Campos Altos	campos-altos	2013-09-03 15:30:12.389704	1
2802	13	Campos Gerais	campos-gerais	2013-09-03 15:30:12.389704	1
2803	13	Cana Verde	cana-verde	2013-09-03 15:30:12.389704	1
2804	13	Canaã	canaa	2013-09-03 15:30:12.389704	1
2805	13	Canápolis	canapolis	2013-09-03 15:30:12.389704	1
2806	13	Candeias	candeias	2013-09-03 15:30:12.389704	1
2807	13	Cantagalo	cantagalo	2013-09-03 15:30:12.389704	1
2808	13	Caparaó	caparao	2013-09-03 15:30:12.389704	1
2809	13	Capela Nova	capela-nova	2013-09-03 15:30:12.389704	1
2810	13	Capelinha	capelinha	2013-09-03 15:30:12.389704	1
2811	13	Capetinga	capetinga	2013-09-03 15:30:12.389704	1
2812	13	Capim Branco	capim-branco	2013-09-03 15:30:12.389704	1
2813	13	Capinópolis	capinopolis	2013-09-03 15:30:12.389704	1
2814	13	Capitão Andrade	capitao-andrade	2013-09-03 15:30:12.389704	1
2815	13	Capitão Enéas	capitao-eneas	2013-09-03 15:30:12.389704	1
2816	13	Capitólio	capitolio	2013-09-03 15:30:12.389704	1
2817	13	Caputira	caputira	2013-09-03 15:30:12.389704	1
2818	13	Caraí	carai	2013-09-03 15:30:12.389704	1
2819	13	Caranaíba	caranaiba	2013-09-03 15:30:12.389704	1
2820	13	Carandaí	carandai	2013-09-03 15:30:12.389704	1
2821	13	Carangola	carangola	2013-09-03 15:30:12.389704	1
2822	13	Caratinga	caratinga	2013-09-03 15:30:12.389704	1
2823	13	Carbonita	carbonita	2013-09-03 15:30:12.389704	1
2824	13	Careaçu	careacu	2013-09-03 15:30:12.389704	1
2825	13	Carlos Chagas	carlos-chagas	2013-09-03 15:30:12.389704	1
2826	13	Carmésia	carmesia	2013-09-03 15:30:12.389704	1
2827	13	Carmo da Cachoeira	carmo-da-cachoeira	2013-09-03 15:30:12.389704	1
2828	13	Carmo da Mata	carmo-da-mata	2013-09-03 15:30:12.389704	1
2829	13	Carmo de Minas	carmo-de-minas	2013-09-03 15:30:12.389704	1
2830	13	Carmo do Cajuru	carmo-do-cajuru	2013-09-03 15:30:12.389704	1
2831	13	Carmo do Paranaíba	carmo-do-paranaiba	2013-09-03 15:30:12.389704	1
2832	13	Carmo do Rio Claro	carmo-do-rio-claro	2013-09-03 15:30:12.389704	1
2833	13	Carmópolis de Minas	carmopolis-de-minas	2013-09-03 15:30:12.389704	1
2834	13	Carneirinho	carneirinho	2013-09-03 15:30:12.389704	1
2835	13	Carrancas	carrancas	2013-09-03 15:30:12.389704	1
2836	13	Carvalhópolis	carvalhopolis	2013-09-03 15:30:12.389704	1
2837	13	Carvalhos	carvalhos	2013-09-03 15:30:12.389704	1
2838	13	Casa Grande	casa-grande	2013-09-03 15:30:12.389704	1
2839	13	Cascalho Rico	cascalho-rico	2013-09-03 15:30:12.389704	1
2840	13	Cássia	cassia	2013-09-03 15:30:12.389704	1
2841	13	Cataguases	cataguases	2013-09-03 15:30:12.389704	1
2842	13	Catas Altas	catas-altas	2013-09-03 15:30:12.389704	1
2843	13	Catas Altas da Noruega	catas-altas-da-noruega	2013-09-03 15:30:12.389704	1
2844	13	Catuji	catuji	2013-09-03 15:30:12.389704	1
2845	13	Catuti	catuti	2013-09-03 15:30:12.389704	1
2846	13	Caxambu	caxambu	2013-09-03 15:30:12.389704	1
2847	13	Cedro do Abaeté	cedro-do-abaete	2013-09-03 15:30:12.389704	1
2848	13	Central de Minas	central-de-minas	2013-09-03 15:30:12.389704	1
2849	13	Centralina	centralina	2013-09-03 15:30:12.389704	1
2850	13	Chácara	chacara	2013-09-03 15:30:12.389704	1
2851	13	Chalé	chale	2013-09-03 15:30:12.389704	1
2852	13	Chapada do Norte	chapada-do-norte	2013-09-03 15:30:12.389704	1
2853	13	Chapada Gaúcha	chapada-gaucha	2013-09-03 15:30:12.389704	1
2854	13	Chiador	chiador	2013-09-03 15:30:12.389704	1
2855	13	Cipotânea	cipotanea	2013-09-03 15:30:12.389704	1
2856	13	Claraval	claraval	2013-09-03 15:30:12.389704	1
2857	13	Claro dos Poções	claro-dos-pocoes	2013-09-03 15:30:12.389704	1
2858	13	Cláudio	claudio	2013-09-03 15:30:12.389704	1
2859	13	Coimbra	coimbra	2013-09-03 15:30:12.389704	1
2860	13	Coluna	coluna	2013-09-03 15:30:12.389704	1
2861	13	Comendador Gomes	comendador-gomes	2013-09-03 15:30:12.389704	1
2862	13	Comercinho	comercinho	2013-09-03 15:30:12.389704	1
2863	13	Conceição da Aparecida	conceicao-da-aparecida	2013-09-03 15:30:12.389704	1
2864	13	Conceição da Barra de Minas	conceicao-da-barra-de-minas	2013-09-03 15:30:12.389704	1
2865	13	Conceição das Alagoas	conceicao-das-alagoas	2013-09-03 15:30:12.389704	1
2866	13	Conceição das Pedras	conceicao-das-pedras	2013-09-03 15:30:12.389704	1
2867	13	Conceição de Ipanema	conceicao-de-ipanema	2013-09-03 15:30:12.389704	1
2868	13	Conceição do Mato Dentro	conceicao-do-mato-dentro	2013-09-03 15:30:12.389704	1
2869	13	Conceição do Pará	conceicao-do-para	2013-09-03 15:30:12.389704	1
2870	13	Conceição do Rio Verde	conceicao-do-rio-verde	2013-09-03 15:30:12.389704	1
2871	13	Conceição dos Ouros	conceicao-dos-ouros	2013-09-03 15:30:12.389704	1
2872	13	Cônego Marinho	conego-marinho	2013-09-03 15:30:12.389704	1
2873	13	Confins	confins	2013-09-03 15:30:12.389704	1
2874	13	Congonhal	congonhal	2013-09-03 15:30:12.389704	1
2875	13	Congonhas	congonhas	2013-09-03 15:30:12.389704	1
2876	13	Congonhas do Norte	congonhas-do-norte	2013-09-03 15:30:12.389704	1
2877	13	Conquista	conquista	2013-09-03 15:30:12.389704	1
2878	13	Conselheiro Lafaiete	conselheiro-lafaiete	2013-09-03 15:30:12.389704	1
2879	13	Conselheiro Pena	conselheiro-pena	2013-09-03 15:30:12.389704	1
2880	13	Consolação	consolacao	2013-09-03 15:30:12.389704	1
2881	13	Contagem	contagem	2013-09-03 15:30:12.389704	1
2882	13	Coqueiral	coqueiral	2013-09-03 15:30:12.389704	1
2883	13	Coração de Jesus	coracao-de-jesus	2013-09-03 15:30:12.389704	1
2884	13	Cordisburgo	cordisburgo	2013-09-03 15:30:12.389704	1
2885	13	Cordislândia	cordislandia	2013-09-03 15:30:12.389704	1
2886	13	Corinto	corinto	2013-09-03 15:30:12.389704	1
2887	13	Coroaci	coroaci	2013-09-03 15:30:12.389704	1
2888	13	Coromandel	coromandel	2013-09-03 15:30:12.389704	1
2889	13	Coronel Fabriciano	coronel-fabriciano	2013-09-03 15:30:12.389704	1
2890	13	Coronel Murta	coronel-murta	2013-09-03 15:30:12.389704	1
2891	13	Coronel Pacheco	coronel-pacheco	2013-09-03 15:30:12.389704	1
2892	13	Coronel Xavier Chaves	coronel-xavier-chaves	2013-09-03 15:30:12.389704	1
2893	13	Córrego Danta	corrego-danta	2013-09-03 15:30:12.389704	1
2894	13	Córrego do Bom Jesus	corrego-do-bom-jesus	2013-09-03 15:30:12.389704	1
2895	13	Córrego Fundo	corrego-fundo	2013-09-03 15:30:12.389704	1
2896	13	Córrego Novo	corrego-novo	2013-09-03 15:30:12.389704	1
2897	13	Couto de Magalhães de Minas	couto-de-magalhaes-de-minas	2013-09-03 15:30:12.389704	1
2898	13	Crisólita	crisolita	2013-09-03 15:30:12.389704	1
2899	13	Cristais	cristais	2013-09-03 15:30:12.389704	1
2900	13	Cristália	cristalia	2013-09-03 15:30:12.389704	1
2901	13	Cristiano Otoni	cristiano-otoni	2013-09-03 15:30:12.389704	1
2902	13	Cristina	cristina	2013-09-03 15:30:12.389704	1
2903	13	Crucilândia	crucilandia	2013-09-03 15:30:12.389704	1
2904	13	Cruzeiro da Fortaleza	cruzeiro-da-fortaleza	2013-09-03 15:30:12.389704	1
2905	13	Cruzília	cruzilia	2013-09-03 15:30:12.389704	1
2906	13	Cuparaque	cuparaque	2013-09-03 15:30:12.389704	1
2907	13	Curral de Dentro	curral-de-dentro	2013-09-03 15:30:12.389704	1
2908	13	Curvelo	curvelo	2013-09-03 15:30:12.389704	1
2909	13	Datas	datas	2013-09-03 15:30:12.389704	1
2910	13	Delfim Moreira	delfim-moreira	2013-09-03 15:30:12.389704	1
2911	13	Delfinópolis	delfinopolis	2013-09-03 15:30:12.389704	1
2912	13	Delta	delta	2013-09-03 15:30:12.389704	1
2913	13	Descoberto	descoberto	2013-09-03 15:30:12.389704	1
2914	13	Desterro de Entre Rios	desterro-de-entre-rios	2013-09-03 15:30:12.389704	1
2915	13	Desterro do Melo	desterro-do-melo	2013-09-03 15:30:12.389704	1
2916	13	Diamantina	diamantina	2013-09-03 15:30:12.389704	1
2917	13	Diogo de Vasconcelos	diogo-de-vasconcelos	2013-09-03 15:30:12.389704	1
2918	13	Dionísio	dionisio	2013-09-03 15:30:12.389704	1
2919	13	Divinésia	divinesia	2013-09-03 15:30:12.389704	1
2920	13	Divino	divino	2013-09-03 15:30:12.389704	1
2921	13	Divino das Laranjeiras	divino-das-laranjeiras	2013-09-03 15:30:12.389704	1
2922	13	Divinolândia de Minas	divinolandia-de-minas	2013-09-03 15:30:12.389704	1
2923	13	Divinópolis	divinopolis	2013-09-03 15:30:12.389704	1
2924	13	Divisa Alegre	divisa-alegre	2013-09-03 15:30:12.389704	1
2925	13	Divisa Nova	divisa-nova	2013-09-03 15:30:12.389704	1
2926	13	Divisópolis	divisopolis	2013-09-03 15:30:12.389704	1
2927	13	Dom Bosco	dom-bosco	2013-09-03 15:30:12.389704	1
2928	13	Dom Cavati	dom-cavati	2013-09-03 15:30:12.389704	1
2929	13	Dom Joaquim	dom-joaquim	2013-09-03 15:30:12.389704	1
2930	13	Dom Silvério	dom-silverio	2013-09-03 15:30:12.389704	1
2931	13	Dom Viçoso	dom-vicoso	2013-09-03 15:30:12.389704	1
2932	13	Dona Eusébia	dona-eusebia	2013-09-03 15:30:12.389704	1
2933	13	Dores de Campos	dores-de-campos	2013-09-03 15:30:12.389704	1
2934	13	Dores de Guanhães	dores-de-guanhaes	2013-09-03 15:30:12.389704	1
2935	13	Dores do Indaiá	dores-do-indaia	2013-09-03 15:30:12.389704	1
2936	13	Dores do Turvo	dores-do-turvo	2013-09-03 15:30:12.389704	1
2937	13	Doresópolis	doresopolis	2013-09-03 15:30:12.389704	1
2938	13	Douradoquara	douradoquara	2013-09-03 15:30:12.389704	1
2939	13	Durandé	durande	2013-09-03 15:30:12.389704	1
2940	13	Elói Mendes	eloi-mendes	2013-09-03 15:30:12.389704	1
2941	13	Engenheiro Caldas	engenheiro-caldas	2013-09-03 15:30:12.389704	1
2942	13	Engenheiro Navarro	engenheiro-navarro	2013-09-03 15:30:12.389704	1
2943	13	Entre Folhas	entre-folhas	2013-09-03 15:30:12.389704	1
2944	13	Entre Rios de Minas	entre-rios-de-minas	2013-09-03 15:30:12.389704	1
2945	13	Ervália	ervalia	2013-09-03 15:30:12.389704	1
2946	13	Esmeraldas	esmeraldas	2013-09-03 15:30:12.389704	1
2947	13	Espera Feliz	espera-feliz	2013-09-03 15:30:12.389704	1
2948	13	Espinosa	espinosa	2013-09-03 15:30:12.389704	1
2949	13	Espírito Santo do Dourado	espirito-santo-do-dourado	2013-09-03 15:30:12.389704	1
2950	13	Estiva	estiva	2013-09-03 15:30:12.389704	1
2951	13	Estrela Dalva	estrela-dalva	2013-09-03 15:30:12.389704	1
2952	13	Estrela do Indaiá	estrela-do-indaia	2013-09-03 15:30:12.389704	1
2953	13	Estrela do Sul	estrela-do-sul	2013-09-03 15:30:12.389704	1
2954	13	Eugenópolis	eugenopolis	2013-09-03 15:30:12.389704	1
2955	13	Ewbank da Câmara	ewbank-da-camara	2013-09-03 15:30:12.389704	1
2956	13	Extrema	extrema	2013-09-03 15:30:12.389704	1
2957	13	Fama	fama	2013-09-03 15:30:12.389704	1
2958	13	Faria Lemos	faria-lemos	2013-09-03 15:30:12.389704	1
2959	13	Felício dos Santos	felicio-dos-santos	2013-09-03 15:30:12.389704	1
2960	13	Felisburgo	felisburgo	2013-09-03 15:30:12.389704	1
2961	13	Felixlândia	felixlandia	2013-09-03 15:30:12.389704	1
2962	13	Fernandes Tourinho	fernandes-tourinho	2013-09-03 15:30:12.389704	1
2963	13	Ferros	ferros	2013-09-03 15:30:12.389704	1
2964	13	Fervedouro	fervedouro	2013-09-03 15:30:12.389704	1
2965	13	Florestal	florestal	2013-09-03 15:30:12.389704	1
2966	13	Formiga	formiga	2013-09-03 15:30:12.389704	1
2967	13	Formoso	formoso	2013-09-03 15:30:12.389704	1
2968	13	Fortaleza de Minas	fortaleza-de-minas	2013-09-03 15:30:12.389704	1
2969	13	Fortuna de Minas	fortuna-de-minas	2013-09-03 15:30:12.389704	1
2970	13	Francisco Badaró	francisco-badaro	2013-09-03 15:30:12.389704	1
2971	13	Francisco Dumont	francisco-dumont	2013-09-03 15:30:12.389704	1
2972	13	Francisco Sá	francisco-sa	2013-09-03 15:30:12.389704	1
2973	13	Franciscópolis	franciscopolis	2013-09-03 15:30:12.389704	1
2974	13	Frei Gaspar	frei-gaspar	2013-09-03 15:30:12.389704	1
2975	13	Frei Inocêncio	frei-inocencio	2013-09-03 15:30:12.389704	1
2976	13	Frei Lagonegro	frei-lagonegro	2013-09-03 15:30:12.389704	1
2977	13	Fronteira	fronteira	2013-09-03 15:30:12.389704	1
2978	13	Fronteira dos Vales	fronteira-dos-vales	2013-09-03 15:30:12.389704	1
2979	13	Fruta de Leite	fruta-de-leite	2013-09-03 15:30:12.389704	1
2980	13	Frutal	frutal	2013-09-03 15:30:12.389704	1
2981	13	Funilândia	funilandia	2013-09-03 15:30:12.389704	1
2982	13	Galiléia	galileia	2013-09-03 15:30:12.389704	1
2983	13	Gameleiras	gameleiras	2013-09-03 15:30:12.389704	1
2984	13	Glaucilândia	glaucilandia	2013-09-03 15:30:12.389704	1
2985	13	Goiabeira	goiabeira	2013-09-03 15:30:12.389704	1
2986	13	Goianá	goiana	2013-09-03 15:30:12.389704	1
2987	13	Gonçalves	goncalves	2013-09-03 15:30:12.389704	1
2988	13	Gonzaga	gonzaga	2013-09-03 15:30:12.389704	1
2989	13	Gouveia	gouveia	2013-09-03 15:30:12.389704	1
2990	13	Governador Valadares	governador-valadares	2013-09-03 15:30:12.389704	1
2991	13	Grão Mogol	grao-mogol	2013-09-03 15:30:12.389704	1
2992	13	Grupiara	grupiara	2013-09-03 15:30:12.389704	1
2993	13	Guanhães	guanhaes	2013-09-03 15:30:12.389704	1
2994	13	Guapé	guape	2013-09-03 15:30:12.389704	1
2995	13	Guaraciaba	guaraciaba	2013-09-03 15:30:12.389704	1
2996	13	Guaraciama	guaraciama	2013-09-03 15:30:12.389704	1
2997	13	Guaranésia	guaranesia	2013-09-03 15:30:12.389704	1
2998	13	Guarani	guarani	2013-09-03 15:30:12.389704	1
2999	13	Guarará	guarara	2013-09-03 15:30:12.389704	1
3000	13	Guarda-Mor	guarda-mor	2013-09-03 15:30:12.389704	1
3001	13	Guaxupé	guaxupe	2013-09-03 15:30:12.389704	1
3002	13	Guidoval	guidoval	2013-09-03 15:30:12.389704	1
3003	13	Guimarânia	guimarania	2013-09-03 15:30:12.389704	1
3004	13	Guiricema	guiricema	2013-09-03 15:30:12.389704	1
3005	13	Gurinhatã	gurinhata	2013-09-03 15:30:12.389704	1
3006	13	Heliodora	heliodora	2013-09-03 15:30:12.389704	1
3007	13	Iapu	iapu	2013-09-03 15:30:12.389704	1
3008	13	Ibertioga	ibertioga	2013-09-03 15:30:12.389704	1
3009	13	Ibiá	ibia	2013-09-03 15:30:12.389704	1
3010	13	Ibiaí	ibiai	2013-09-03 15:30:12.389704	1
3011	13	Ibiracatu	ibiracatu	2013-09-03 15:30:12.389704	1
3012	13	Ibiraci	ibiraci	2013-09-03 15:30:12.389704	1
3013	13	Ibirité	ibirite	2013-09-03 15:30:12.389704	1
3014	13	Ibitiúra de Minas	ibitiura-de-minas	2013-09-03 15:30:12.389704	1
3015	13	Ibituruna	ibituruna	2013-09-03 15:30:12.389704	1
3016	13	Icaraí de Minas	icarai-de-minas	2013-09-03 15:30:12.389704	1
3017	13	Igarapé	igarape	2013-09-03 15:30:12.389704	1
3018	13	Igaratinga	igaratinga	2013-09-03 15:30:12.389704	1
3019	13	Iguatama	iguatama	2013-09-03 15:30:12.389704	1
3020	13	Ijaci	ijaci	2013-09-03 15:30:12.389704	1
3021	13	Ilicínea	ilicinea	2013-09-03 15:30:12.389704	1
3022	13	Imbé de Minas	imbe-de-minas	2013-09-03 15:30:12.389704	1
3023	13	Inconfidentes	inconfidentes	2013-09-03 15:30:12.389704	1
3024	13	Indaiabira	indaiabira	2013-09-03 15:30:12.389704	1
3025	13	Indianópolis	indianopolis	2013-09-03 15:30:12.389704	1
3026	13	Ingaí	ingai	2013-09-03 15:30:12.389704	1
3029	13	Inimutaba	inimutaba	2013-09-03 15:30:12.389704	1
3030	13	Ipaba	ipaba	2013-09-03 15:30:12.389704	1
3031	13	Ipanema	ipanema	2013-09-03 15:30:12.389704	1
3032	13	Ipatinga	ipatinga	2013-09-03 15:30:12.389704	1
3033	13	Ipiaçu	ipiacu	2013-09-03 15:30:12.389704	1
3034	13	Ipuiúna	ipuiuna	2013-09-03 15:30:12.389704	1
3035	13	Iraí de Minas	irai-de-minas	2013-09-03 15:30:12.389704	1
3036	13	Itabira	itabira	2013-09-03 15:30:12.389704	1
3037	13	Itabirinha de Mantena	itabirinha-de-mantena	2013-09-03 15:30:12.389704	1
3038	13	Itabirito	itabirito	2013-09-03 15:30:12.389704	1
3039	13	Itacambira	itacambira	2013-09-03 15:30:12.389704	1
3040	13	Itacarambi	itacarambi	2013-09-03 15:30:12.389704	1
3041	13	Itaguara	itaguara	2013-09-03 15:30:12.389704	1
3042	13	Itaipé	itaipe	2013-09-03 15:30:12.389704	1
3043	13	Itajubá	itajuba	2013-09-03 15:30:12.389704	1
3044	13	Itamarandiba	itamarandiba	2013-09-03 15:30:12.389704	1
3045	13	Itamarati de Minas	itamarati-de-minas	2013-09-03 15:30:12.389704	1
3046	13	Itambacuri	itambacuri	2013-09-03 15:30:12.389704	1
3047	13	Itambé do Mato Dentro	itambe-do-mato-dentro	2013-09-03 15:30:12.389704	1
3048	13	Itamogi	itamogi	2013-09-03 15:30:12.389704	1
3049	13	Itamonte	itamonte	2013-09-03 15:30:12.389704	1
3050	13	Itanhandu	itanhandu	2013-09-03 15:30:12.389704	1
3051	13	Itanhomi	itanhomi	2013-09-03 15:30:12.389704	1
3052	13	Itaobim	itaobim	2013-09-03 15:30:12.389704	1
3053	13	Itapagipe	itapagipe	2013-09-03 15:30:12.389704	1
3054	13	Itapecerica	itapecerica	2013-09-03 15:30:12.389704	1
3055	13	Itapeva	itapeva	2013-09-03 15:30:12.389704	1
3056	13	Itatiaiuçu	itatiaiucu	2013-09-03 15:30:12.389704	1
3057	13	Itaú de Minas	itau-de-minas	2013-09-03 15:30:12.389704	1
3058	13	Itaúna	itauna	2013-09-03 15:30:12.389704	1
3059	13	Itaverava	itaverava	2013-09-03 15:30:12.389704	1
3060	13	Itinga	itinga	2013-09-03 15:30:12.389704	1
3061	13	Itueta	itueta	2013-09-03 15:30:12.389704	1
3062	13	Ituiutaba	ituiutaba	2013-09-03 15:30:12.389704	1
3063	13	Itumirim	itumirim	2013-09-03 15:30:12.389704	1
3064	13	Iturama	iturama	2013-09-03 15:30:12.389704	1
3065	13	Itutinga	itutinga	2013-09-03 15:30:12.389704	1
3066	13	Jaboticatubas	jaboticatubas	2013-09-03 15:30:12.389704	1
3067	13	Jacinto	jacinto	2013-09-03 15:30:12.389704	1
3068	13	Jacuí	jacui	2013-09-03 15:30:12.389704	1
3069	13	Jacutinga	jacutinga	2013-09-03 15:30:12.389704	1
3070	13	Jaguaraçu	jaguaracu	2013-09-03 15:30:12.389704	1
3071	13	Jaíba	jaiba	2013-09-03 15:30:12.389704	1
3072	13	Jampruca	jampruca	2013-09-03 15:30:12.389704	1
3073	13	Janaúba	janauba	2013-09-03 15:30:12.389704	1
3074	13	Januária	januaria	2013-09-03 15:30:12.389704	1
3075	13	Japaraíba	japaraiba	2013-09-03 15:30:12.389704	1
3076	13	Japonvar	japonvar	2013-09-03 15:30:12.389704	1
3077	13	Jeceaba	jeceaba	2013-09-03 15:30:12.389704	1
3078	13	Jenipapo de Minas	jenipapo-de-minas	2013-09-03 15:30:12.389704	1
3079	13	Jequeri	jequeri	2013-09-03 15:30:12.389704	1
3080	13	Jequitaí	jequitai	2013-09-03 15:30:12.389704	1
3081	13	Jequitibá	jequitiba	2013-09-03 15:30:12.389704	1
3082	13	Jequitinhonha	jequitinhonha	2013-09-03 15:30:12.389704	1
3083	13	Jesuânia	jesuania	2013-09-03 15:30:12.389704	1
3084	13	Joaíma	joaima	2013-09-03 15:30:12.389704	1
3085	13	Joanésia	joanesia	2013-09-03 15:30:12.389704	1
3086	13	João Monlevade	joao-monlevade	2013-09-03 15:30:12.389704	1
3087	13	João Pinheiro	joao-pinheiro	2013-09-03 15:30:12.389704	1
3088	13	Joaquim Felício	joaquim-felicio	2013-09-03 15:30:12.389704	1
3089	13	Jordânia	jordania	2013-09-03 15:30:12.389704	1
3090	13	José Gonçalves de Minas	jose-goncalves-de-minas	2013-09-03 15:30:12.389704	1
3091	13	José Raydan	jose-raydan	2013-09-03 15:30:12.389704	1
3092	13	Josenópolis	josenopolis	2013-09-03 15:30:12.389704	1
3093	13	Juatuba	juatuba	2013-09-03 15:30:12.389704	1
3094	13	Juiz de Fora	juiz-de-fora	2013-09-03 15:30:12.389704	1
3095	13	Juramento	juramento	2013-09-03 15:30:12.389704	1
3096	13	Juruaia	juruaia	2013-09-03 15:30:12.389704	1
3097	13	Juvenília	juvenilia	2013-09-03 15:30:12.389704	1
3098	13	Ladainha	ladainha	2013-09-03 15:30:12.389704	1
3099	13	Lagamar	lagamar	2013-09-03 15:30:12.389704	1
3100	13	Lagoa da Prata	lagoa-da-prata	2013-09-03 15:30:12.389704	1
3101	13	Lagoa dos Patos	lagoa-dos-patos	2013-09-03 15:30:12.389704	1
3102	13	Lagoa Dourada	lagoa-dourada	2013-09-03 15:30:12.389704	1
3103	13	Lagoa Formosa	lagoa-formosa	2013-09-03 15:30:12.389704	1
3104	13	Lagoa Grande	lagoa-grande	2013-09-03 15:30:12.389704	1
3105	13	Lagoa Santa	lagoa-santa	2013-09-03 15:30:12.389704	1
3106	13	Lajinha	lajinha	2013-09-03 15:30:12.389704	1
3107	13	Lambari	lambari	2013-09-03 15:30:12.389704	1
3108	13	Lamim	lamim	2013-09-03 15:30:12.389704	1
3109	13	Laranjal	laranjal	2013-09-03 15:30:12.389704	1
3110	13	Lassance	lassance	2013-09-03 15:30:12.389704	1
3111	13	Lavras	lavras	2013-09-03 15:30:12.389704	1
3112	13	Leandro Ferreira	leandro-ferreira	2013-09-03 15:30:12.389704	1
3113	13	Leme do Prado	leme-do-prado	2013-09-03 15:30:12.389704	1
3114	13	Leopoldina	leopoldina	2013-09-03 15:30:12.389704	1
3115	13	Liberdade	liberdade	2013-09-03 15:30:12.389704	1
3116	13	Lima Duarte	lima-duarte	2013-09-03 15:30:12.389704	1
3117	13	Limeira do Oeste	limeira-do-oeste	2013-09-03 15:30:12.389704	1
3118	13	Lontra	lontra	2013-09-03 15:30:12.389704	1
3119	13	Luisburgo	luisburgo	2013-09-03 15:30:12.389704	1
3120	13	Luislândia	luislandia	2013-09-03 15:30:12.389704	1
3121	13	Luminárias	luminarias	2013-09-03 15:30:12.389704	1
3122	13	Luz	luz	2013-09-03 15:30:12.389704	1
3123	13	Machacalis	machacalis	2013-09-03 15:30:12.389704	1
3124	13	Machado	machado	2013-09-03 15:30:12.389704	1
3125	13	Madre de Deus de Minas	madre-de-deus-de-minas	2013-09-03 15:30:12.389704	1
3126	13	Malacacheta	malacacheta	2013-09-03 15:30:12.389704	1
3127	13	Mamonas	mamonas	2013-09-03 15:30:12.389704	1
3128	13	Manga	manga	2013-09-03 15:30:12.389704	1
3129	13	Manhuaçu	manhuacu	2013-09-03 15:30:12.389704	1
3130	13	Manhumirim	manhumirim	2013-09-03 15:30:12.389704	1
3131	13	Mantena	mantena	2013-09-03 15:30:12.389704	1
3132	13	Mar de Espanha	mar-de-espanha	2013-09-03 15:30:12.389704	1
3133	13	Maravilhas	maravilhas	2013-09-03 15:30:12.389704	1
3134	13	Maria da Fé	maria-da-fe	2013-09-03 15:30:12.389704	1
3135	13	Mariana	mariana	2013-09-03 15:30:12.389704	1
3137	13	Mário Campos	mario-campos	2013-09-03 15:30:12.389704	1
3138	13	Maripá de Minas	maripa-de-minas	2013-09-03 15:30:12.389704	1
3139	13	Marliéria	marlieria	2013-09-03 15:30:12.389704	1
3140	13	Marmelópolis	marmelopolis	2013-09-03 15:30:12.389704	1
3141	13	Martinho Campos	martinho-campos	2013-09-03 15:30:12.389704	1
3142	13	Martins Soares	martins-soares	2013-09-03 15:30:12.389704	1
3143	13	Mata Verde	mata-verde	2013-09-03 15:30:12.389704	1
3144	13	Materlândia	materlandia	2013-09-03 15:30:12.389704	1
3145	13	Mateus Leme	mateus-leme	2013-09-03 15:30:12.389704	1
3146	13	Mathias Lobato	mathias-lobato	2013-09-03 15:30:12.389704	1
3147	13	Matias Barbosa	matias-barbosa	2013-09-03 15:30:12.389704	1
3148	13	Matias Cardoso	matias-cardoso	2013-09-03 15:30:12.389704	1
3149	13	Matipó	matipo	2013-09-03 15:30:12.389704	1
3150	13	Mato Verde	mato-verde	2013-09-03 15:30:12.389704	1
3151	13	Matozinhos	matozinhos	2013-09-03 15:30:12.389704	1
3152	13	Matutina	matutina	2013-09-03 15:30:12.389704	1
3153	13	Medeiros	medeiros	2013-09-03 15:30:12.389704	1
3154	13	Medina	medina	2013-09-03 15:30:12.389704	1
3155	13	Mendes Pimentel	mendes-pimentel	2013-09-03 15:30:12.389704	1
3156	13	Mercês	merces	2013-09-03 15:30:12.389704	1
3157	13	Mesquita	mesquita	2013-09-03 15:30:12.389704	1
3158	13	Minas Novas	minas-novas	2013-09-03 15:30:12.389704	1
3159	13	Minduri	minduri	2013-09-03 15:30:12.389704	1
3160	13	Mirabela	mirabela	2013-09-03 15:30:12.389704	1
3161	13	Miradouro	miradouro	2013-09-03 15:30:12.389704	1
3162	13	Miraí	mirai	2013-09-03 15:30:12.389704	1
3163	13	Miravânia	miravania	2013-09-03 15:30:12.389704	1
3164	13	Moeda	moeda	2013-09-03 15:30:12.389704	1
3165	13	Moema	moema	2013-09-03 15:30:12.389704	1
3166	13	Monjolos	monjolos	2013-09-03 15:30:12.389704	1
3167	13	Monsenhor Paulo	monsenhor-paulo	2013-09-03 15:30:12.389704	1
3168	13	Montalvânia	montalvania	2013-09-03 15:30:12.389704	1
3169	13	Monte Alegre de Minas	monte-alegre-de-minas	2013-09-03 15:30:12.389704	1
3170	13	Monte Azul	monte-azul	2013-09-03 15:30:12.389704	1
3171	13	Monte Belo	monte-belo	2013-09-03 15:30:12.389704	1
3172	13	Monte Carmelo	monte-carmelo	2013-09-03 15:30:12.389704	1
3173	13	Monte Formoso	monte-formoso	2013-09-03 15:30:12.389704	1
3174	13	Monte Santo de Minas	monte-santo-de-minas	2013-09-03 15:30:12.389704	1
3175	13	Monte Sião	monte-siao	2013-09-03 15:30:12.389704	1
3176	13	Montes Claros	montes-claros	2013-09-03 15:30:12.389704	1
3177	13	Montezuma	montezuma	2013-09-03 15:30:12.389704	1
3178	13	Morada Nova de Minas	morada-nova-de-minas	2013-09-03 15:30:12.389704	1
3179	13	Morro da Garça	morro-da-garca	2013-09-03 15:30:12.389704	1
3180	13	Morro do Pilar	morro-do-pilar	2013-09-03 15:30:12.389704	1
3181	13	Munhoz	munhoz	2013-09-03 15:30:12.389704	1
3182	13	Muriaé	muriae	2013-09-03 15:30:12.389704	1
3183	13	Mutum	mutum	2013-09-03 15:30:12.389704	1
3184	13	Muzambinho	muzambinho	2013-09-03 15:30:12.389704	1
3185	13	Nacip Raydan	nacip-raydan	2013-09-03 15:30:12.389704	1
3186	13	Nanuque	nanuque	2013-09-03 15:30:12.389704	1
3187	13	Naque	naque	2013-09-03 15:30:12.389704	1
3188	13	Natalândia	natalandia	2013-09-03 15:30:12.389704	1
3189	13	Natércia	natercia	2013-09-03 15:30:12.389704	1
3190	13	Nazareno	nazareno	2013-09-03 15:30:12.389704	1
3191	13	Nepomuceno	nepomuceno	2013-09-03 15:30:12.389704	1
3192	13	Ninheira	ninheira	2013-09-03 15:30:12.389704	1
3193	13	Nova Belém	nova-belem	2013-09-03 15:30:12.389704	1
3194	13	Nova Era	nova-era	2013-09-03 15:30:12.389704	1
3195	13	Nova Lima	nova-lima	2013-09-03 15:30:12.389704	1
3196	13	Nova Módica	nova-modica	2013-09-03 15:30:12.389704	1
3197	13	Nova Ponte	nova-ponte	2013-09-03 15:30:12.389704	1
3198	13	Nova Porteirinha	nova-porteirinha	2013-09-03 15:30:12.389704	1
3199	13	Nova Resende	nova-resende	2013-09-03 15:30:12.389704	1
3200	13	Nova Serrana	nova-serrana	2013-09-03 15:30:12.389704	1
3201	13	Nova União	nova-uniao	2013-09-03 15:30:12.389704	1
3202	13	Novo Cruzeiro	novo-cruzeiro	2013-09-03 15:30:12.389704	1
3203	13	Novo Oriente de Minas	novo-oriente-de-minas	2013-09-03 15:30:12.389704	1
3204	13	Novorizonte	novorizonte	2013-09-03 15:30:12.389704	1
3205	13	Olaria	olaria	2013-09-03 15:30:12.389704	1
3206	13	Olhos-d`Água	olhos-d-agua	2013-09-03 15:30:12.389704	1
3207	13	Olímpio Noronha	olimpio-noronha	2013-09-03 15:30:12.389704	1
3208	13	Oliveira	oliveira	2013-09-03 15:30:12.389704	1
3209	13	Oliveira Fortes	oliveira-fortes	2013-09-03 15:30:12.389704	1
3210	13	Onça de Pitangui	onca-de-pitangui	2013-09-03 15:30:12.389704	1
3211	13	Oratórios	oratorios	2013-09-03 15:30:12.389704	1
3212	13	Orizânia	orizania	2013-09-03 15:30:12.389704	1
3213	13	Ouro Branco	ouro-branco	2013-09-03 15:30:12.389704	1
3214	13	Ouro Fino	ouro-fino	2013-09-03 15:30:12.389704	1
3215	13	Ouro Preto	ouro-preto	2013-09-03 15:30:12.389704	1
3216	13	Ouro Verde de Minas	ouro-verde-de-minas	2013-09-03 15:30:12.389704	1
3217	13	Padre Carvalho	padre-carvalho	2013-09-03 15:30:12.389704	1
3218	13	Padre Paraíso	padre-paraiso	2013-09-03 15:30:12.389704	1
3219	13	Pai Pedro	pai-pedro	2013-09-03 15:30:12.389704	1
3220	13	Paineiras	paineiras	2013-09-03 15:30:12.389704	1
3221	13	Pains	pains	2013-09-03 15:30:12.389704	1
3222	13	Paiva	paiva	2013-09-03 15:30:12.389704	1
3223	13	Palma	palma	2013-09-03 15:30:12.389704	1
3224	13	Palmópolis	palmopolis	2013-09-03 15:30:12.389704	1
3225	13	Papagaios	papagaios	2013-09-03 15:30:12.389704	1
3226	13	Pará de Minas	para-de-minas	2013-09-03 15:30:12.389704	1
3227	13	Paracatu	paracatu	2013-09-03 15:30:12.389704	1
3228	13	Paraguaçu	paraguacu	2013-09-03 15:30:12.389704	1
3229	13	Paraisópolis	paraisopolis	2013-09-03 15:30:12.389704	1
3230	13	Paraopeba	paraopeba	2013-09-03 15:30:12.389704	1
3231	13	Passa Quatro	passa-quatro	2013-09-03 15:30:12.389704	1
3232	13	Passa Tempo	passa-tempo	2013-09-03 15:30:12.389704	1
3233	13	Passabém	passabem	2013-09-03 15:30:12.389704	1
3234	13	Passa-Vinte	passa-vinte	2013-09-03 15:30:12.389704	1
3235	13	Passos	passos	2013-09-03 15:30:12.389704	1
3236	13	Patis	patis	2013-09-03 15:30:12.389704	1
3237	13	Patos de Minas	patos-de-minas	2013-09-03 15:30:12.389704	1
3238	13	Patrocínio	patrocinio	2013-09-03 15:30:12.389704	1
3239	13	Patrocínio do Muriaé	patrocinio-do-muriae	2013-09-03 15:30:12.389704	1
4755	17	Calumbi	calumbi	2013-09-03 15:30:12.389704	1
3240	13	Paula Cândido	paula-candido	2013-09-03 15:30:12.389704	1
3241	13	Paulistas	paulistas	2013-09-03 15:30:12.389704	1
3242	13	Pavão	pavao	2013-09-03 15:30:12.389704	1
3243	13	Peçanha	pecanha	2013-09-03 15:30:12.389704	1
3244	13	Pedra Azul	pedra-azul	2013-09-03 15:30:12.389704	1
3245	13	Pedra Bonita	pedra-bonita	2013-09-03 15:30:12.389704	1
3246	13	Pedra do Anta	pedra-do-anta	2013-09-03 15:30:12.389704	1
3247	13	Pedra do Indaiá	pedra-do-indaia	2013-09-03 15:30:12.389704	1
3248	13	Pedra Dourada	pedra-dourada	2013-09-03 15:30:12.389704	1
3249	13	Pedralva	pedralva	2013-09-03 15:30:12.389704	1
3250	13	Pedras de Maria da Cruz	pedras-de-maria-da-cruz	2013-09-03 15:30:12.389704	1
3251	13	Pedrinópolis	pedrinopolis	2013-09-03 15:30:12.389704	1
3252	13	Pedro Leopoldo	pedro-leopoldo	2013-09-03 15:30:12.389704	1
3253	13	Pedro Teixeira	pedro-teixeira	2013-09-03 15:30:12.389704	1
3254	13	Pequeri	pequeri	2013-09-03 15:30:12.389704	1
3255	13	Pequi	pequi	2013-09-03 15:30:12.389704	1
3256	13	Perdigão	perdigao	2013-09-03 15:30:12.389704	1
3257	13	Perdizes	perdizes	2013-09-03 15:30:12.389704	1
3258	13	Perdões	perdoes	2013-09-03 15:30:12.389704	1
3259	13	Periquito	periquito	2013-09-03 15:30:12.389704	1
3260	13	Pescador	pescador	2013-09-03 15:30:12.389704	1
3261	13	Piau	piau	2013-09-03 15:30:12.389704	1
3262	13	Piedade de Caratinga	piedade-de-caratinga	2013-09-03 15:30:12.389704	1
3263	13	Piedade de Ponte Nova	piedade-de-ponte-nova	2013-09-03 15:30:12.389704	1
3264	13	Piedade do Rio Grande	piedade-do-rio-grande	2013-09-03 15:30:12.389704	1
3265	13	Piedade dos Gerais	piedade-dos-gerais	2013-09-03 15:30:12.389704	1
3266	13	Pimenta	pimenta	2013-09-03 15:30:12.389704	1
3267	13	Pingo-d`Água	pingo-d-agua	2013-09-03 15:30:12.389704	1
3268	13	Pintópolis	pintopolis	2013-09-03 15:30:12.389704	1
3269	13	Piracema	piracema	2013-09-03 15:30:12.389704	1
3270	13	Pirajuba	pirajuba	2013-09-03 15:30:12.389704	1
3271	13	Piranga	piranga	2013-09-03 15:30:12.389704	1
3272	13	Piranguçu	pirangucu	2013-09-03 15:30:12.389704	1
3273	13	Piranguinho	piranguinho	2013-09-03 15:30:12.389704	1
3274	13	Pirapetinga	pirapetinga	2013-09-03 15:30:12.389704	1
3275	13	Pirapora	pirapora	2013-09-03 15:30:12.389704	1
3276	13	Piraúba	pirauba	2013-09-03 15:30:12.389704	1
3277	13	Pitangui	pitangui	2013-09-03 15:30:12.389704	1
3278	13	Piumhi	piumhi	2013-09-03 15:30:12.389704	1
3279	13	Planura	planura	2013-09-03 15:30:12.389704	1
3280	13	Poço Fundo	poco-fundo	2013-09-03 15:30:12.389704	1
3281	13	Poços de Caldas	pocos-de-caldas	2013-09-03 15:30:12.389704	1
3282	13	Pocrane	pocrane	2013-09-03 15:30:12.389704	1
3283	13	Pompéu	pompeu	2013-09-03 15:30:12.389704	1
3284	13	Ponte Nova	ponte-nova	2013-09-03 15:30:12.389704	1
3285	13	Ponto Chique	ponto-chique	2013-09-03 15:30:12.389704	1
3286	13	Ponto dos Volantes	ponto-dos-volantes	2013-09-03 15:30:12.389704	1
3287	13	Porteirinha	porteirinha	2013-09-03 15:30:12.389704	1
3288	13	Porto Firme	porto-firme	2013-09-03 15:30:12.389704	1
3289	13	Poté	pote	2013-09-03 15:30:12.389704	1
3290	13	Pouso Alegre	pouso-alegre	2013-09-03 15:30:12.389704	1
3291	13	Pouso Alto	pouso-alto	2013-09-03 15:30:12.389704	1
3292	13	Prados	prados	2013-09-03 15:30:12.389704	1
3293	13	Prata	prata	2013-09-03 15:30:12.389704	1
3294	13	Pratápolis	pratapolis	2013-09-03 15:30:12.389704	1
3295	13	Pratinha	pratinha	2013-09-03 15:30:12.389704	1
3296	13	Presidente Bernardes	presidente-bernardes	2013-09-03 15:30:12.389704	1
3297	13	Presidente Juscelino	presidente-juscelino	2013-09-03 15:30:12.389704	1
3298	13	Presidente Kubitschek	presidente-kubitschek	2013-09-03 15:30:12.389704	1
3299	13	Presidente Olegário	presidente-olegario	2013-09-03 15:30:12.389704	1
3300	13	Prudente de Morais	prudente-de-morais	2013-09-03 15:30:12.389704	1
3301	13	Quartel Geral	quartel-geral	2013-09-03 15:30:12.389704	1
3302	13	Queluzito	queluzito	2013-09-03 15:30:12.389704	1
3303	13	Raposos	raposos	2013-09-03 15:30:12.389704	1
3304	13	Raul Soares	raul-soares	2013-09-03 15:30:12.389704	1
3305	13	Recreio	recreio	2013-09-03 15:30:12.389704	1
3306	13	Reduto	reduto	2013-09-03 15:30:12.389704	1
3307	13	Resende Costa	resende-costa	2013-09-03 15:30:12.389704	1
3308	13	Resplendor	resplendor	2013-09-03 15:30:12.389704	1
3309	13	Ressaquinha	ressaquinha	2013-09-03 15:30:12.389704	1
3310	13	Riachinho	riachinho	2013-09-03 15:30:12.389704	1
3311	13	Riacho dos Machados	riacho-dos-machados	2013-09-03 15:30:12.389704	1
3312	13	Ribeirão das Neves	ribeirao-das-neves	2013-09-03 15:30:12.389704	1
3313	13	Ribeirão Vermelho	ribeirao-vermelho	2013-09-03 15:30:12.389704	1
3314	13	Rio Acima	rio-acima	2013-09-03 15:30:12.389704	1
3315	13	Rio Casca	rio-casca	2013-09-03 15:30:12.389704	1
3316	13	Rio do Prado	rio-do-prado	2013-09-03 15:30:12.389704	1
3317	13	Rio Doce	rio-doce	2013-09-03 15:30:12.389704	1
3318	13	Rio Espera	rio-espera	2013-09-03 15:30:12.389704	1
3319	13	Rio Manso	rio-manso	2013-09-03 15:30:12.389704	1
3320	13	Rio Novo	rio-novo	2013-09-03 15:30:12.389704	1
3321	13	Rio Paranaíba	rio-paranaiba	2013-09-03 15:30:12.389704	1
3322	13	Rio Pardo de Minas	rio-pardo-de-minas	2013-09-03 15:30:12.389704	1
3323	13	Rio Piracicaba	rio-piracicaba	2013-09-03 15:30:12.389704	1
3324	13	Rio Pomba	rio-pomba	2013-09-03 15:30:12.389704	1
3325	13	Rio Preto	rio-preto	2013-09-03 15:30:12.389704	1
3326	13	Rio Vermelho	rio-vermelho	2013-09-03 15:30:12.389704	1
3327	13	Ritápolis	ritapolis	2013-09-03 15:30:12.389704	1
3328	13	Rochedo de Minas	rochedo-de-minas	2013-09-03 15:30:12.389704	1
3329	13	Rodeiro	rodeiro	2013-09-03 15:30:12.389704	1
3330	13	Romaria	romaria	2013-09-03 15:30:12.389704	1
3331	13	Rosário da Limeira	rosario-da-limeira	2013-09-03 15:30:12.389704	1
3332	13	Rubelita	rubelita	2013-09-03 15:30:12.389704	1
3333	13	Rubim	rubim	2013-09-03 15:30:12.389704	1
3334	13	Sabará	sabara	2013-09-03 15:30:12.389704	1
3335	13	Sabinópolis	sabinopolis	2013-09-03 15:30:12.389704	1
3336	13	Sacramento	sacramento	2013-09-03 15:30:12.389704	1
3337	13	Salinas	salinas	2013-09-03 15:30:12.389704	1
3338	13	Salto da Divisa	salto-da-divisa	2013-09-03 15:30:12.389704	1
3339	13	Santa Bárbara	santa-barbara	2013-09-03 15:30:12.389704	1
3340	13	Santa Bárbara do Leste	santa-barbara-do-leste	2013-09-03 15:30:12.389704	1
3341	13	Santa Bárbara do Monte Verde	santa-barbara-do-monte-verde	2013-09-03 15:30:12.389704	1
3342	13	Santa Bárbara do Tugúrio	santa-barbara-do-tugurio	2013-09-03 15:30:12.389704	1
3343	13	Santa Cruz de Minas	santa-cruz-de-minas	2013-09-03 15:30:12.389704	1
3344	13	Santa Cruz de Salinas	santa-cruz-de-salinas	2013-09-03 15:30:12.389704	1
3345	13	Santa Cruz do Escalvado	santa-cruz-do-escalvado	2013-09-03 15:30:12.389704	1
3346	13	Santa Efigênia de Minas	santa-efigenia-de-minas	2013-09-03 15:30:12.389704	1
3347	13	Santa Fé de Minas	santa-fe-de-minas	2013-09-03 15:30:12.389704	1
3348	13	Santa Helena de Minas	santa-helena-de-minas	2013-09-03 15:30:12.389704	1
3349	13	Santa Juliana	santa-juliana	2013-09-03 15:30:12.389704	1
3350	13	Santa Luzia	santa-luzia	2013-09-03 15:30:12.389704	1
3351	13	Santa Margarida	santa-margarida	2013-09-03 15:30:12.389704	1
3352	13	Santa Maria de Itabira	santa-maria-de-itabira	2013-09-03 15:30:12.389704	1
3353	13	Santa Maria do Salto	santa-maria-do-salto	2013-09-03 15:30:12.389704	1
3354	13	Santa Maria do Suaçuí	santa-maria-do-suacui	2013-09-03 15:30:12.389704	1
3355	13	Santa Rita de Caldas	santa-rita-de-caldas	2013-09-03 15:30:12.389704	1
3356	13	Santa Rita de Ibitipoca	santa-rita-de-ibitipoca	2013-09-03 15:30:12.389704	1
3357	13	Santa Rita de Jacutinga	santa-rita-de-jacutinga	2013-09-03 15:30:12.389704	1
3358	13	Santa Rita de Minas	santa-rita-de-minas	2013-09-03 15:30:12.389704	1
3359	13	Santa Rita do Itueto	santa-rita-do-itueto	2013-09-03 15:30:12.389704	1
3360	13	Santa Rita do Sapucaí	santa-rita-do-sapucai	2013-09-03 15:30:12.389704	1
3361	13	Santa Rosa da Serra	santa-rosa-da-serra	2013-09-03 15:30:12.389704	1
3362	13	Santa Vitória	santa-vitoria	2013-09-03 15:30:12.389704	1
3363	13	Santana da Vargem	santana-da-vargem	2013-09-03 15:30:12.389704	1
3364	13	Santana de Cataguases	santana-de-cataguases	2013-09-03 15:30:12.389704	1
3365	13	Santana de Pirapama	santana-de-pirapama	2013-09-03 15:30:12.389704	1
3366	13	Santana do Deserto	santana-do-deserto	2013-09-03 15:30:12.389704	1
3367	13	Santana do Garambéu	santana-do-garambeu	2013-09-03 15:30:12.389704	1
3368	13	Santana do Jacaré	santana-do-jacare	2013-09-03 15:30:12.389704	1
3369	13	Santana do Manhuaçu	santana-do-manhuacu	2013-09-03 15:30:12.389704	1
3370	13	Santana do Paraíso	santana-do-paraiso	2013-09-03 15:30:12.389704	1
3371	13	Santana do Riacho	santana-do-riacho	2013-09-03 15:30:12.389704	1
3372	13	Santana dos Montes	santana-dos-montes	2013-09-03 15:30:12.389704	1
3373	13	Santo Antônio do Amparo	santo-antonio-do-amparo	2013-09-03 15:30:12.389704	1
3374	13	Santo Antônio do Aventureiro	santo-antonio-do-aventureiro	2013-09-03 15:30:12.389704	1
3375	13	Santo Antônio do Grama	santo-antonio-do-grama	2013-09-03 15:30:12.389704	1
3376	13	Santo Antônio do Itambé	santo-antonio-do-itambe	2013-09-03 15:30:12.389704	1
3377	13	Santo Antônio do Jacinto	santo-antonio-do-jacinto	2013-09-03 15:30:12.389704	1
3378	13	Santo Antônio do Monte	santo-antonio-do-monte	2013-09-03 15:30:12.389704	1
3379	13	Santo Antônio do Retiro	santo-antonio-do-retiro	2013-09-03 15:30:12.389704	1
3380	13	Santo Antônio do Rio Abaixo	santo-antonio-do-rio-abaixo	2013-09-03 15:30:12.389704	1
3381	13	Santo Hipólito	santo-hipolito	2013-09-03 15:30:12.389704	1
3382	13	Santos Dumont	santos-dumont	2013-09-03 15:30:12.389704	1
3383	13	São Bento Abade	sao-bento-abade	2013-09-03 15:30:12.389704	1
3384	13	São Brás do Suaçuí	sao-bras-do-suacui	2013-09-03 15:30:12.389704	1
3385	13	São Domingos das Dores	sao-domingos-das-dores	2013-09-03 15:30:12.389704	1
3386	13	São Domingos do Prata	sao-domingos-do-prata	2013-09-03 15:30:12.389704	1
3387	13	São Félix de Minas	sao-felix-de-minas	2013-09-03 15:30:12.389704	1
3388	13	São Francisco	sao-francisco	2013-09-03 15:30:12.389704	1
3389	13	São Francisco de Paula	sao-francisco-de-paula	2013-09-03 15:30:12.389704	1
3390	13	São Francisco de Sales	sao-francisco-de-sales	2013-09-03 15:30:12.389704	1
3391	13	São Francisco do Glória	sao-francisco-do-gloria	2013-09-03 15:30:12.389704	1
3392	13	São Geraldo	sao-geraldo	2013-09-03 15:30:12.389704	1
3393	13	São Geraldo da Piedade	sao-geraldo-da-piedade	2013-09-03 15:30:12.389704	1
3394	13	São Geraldo do Baixio	sao-geraldo-do-baixio	2013-09-03 15:30:12.389704	1
3395	13	São Gonçalo do Abaeté	sao-goncalo-do-abaete	2013-09-03 15:30:12.389704	1
3396	13	São Gonçalo do Pará	sao-goncalo-do-para	2013-09-03 15:30:12.389704	1
3397	13	São Gonçalo do Rio Abaixo	sao-goncalo-do-rio-abaixo	2013-09-03 15:30:12.389704	1
3398	13	São Gonçalo do Rio Preto	sao-goncalo-do-rio-preto	2013-09-03 15:30:12.389704	1
3399	13	São Gonçalo do Sapucaí	sao-goncalo-do-sapucai	2013-09-03 15:30:12.389704	1
3400	13	São Gotardo	sao-gotardo	2013-09-03 15:30:12.389704	1
3401	13	São João Batista do Glória	sao-joao-batista-do-gloria	2013-09-03 15:30:12.389704	1
3402	13	São João da Lagoa	sao-joao-da-lagoa	2013-09-03 15:30:12.389704	1
3403	13	São João da Mata	sao-joao-da-mata	2013-09-03 15:30:12.389704	1
3404	13	São João da Ponte	sao-joao-da-ponte	2013-09-03 15:30:12.389704	1
3405	13	São João das Missões	sao-joao-das-missoes	2013-09-03 15:30:12.389704	1
3406	13	São João del Rei	sao-joao-del-rei	2013-09-03 15:30:12.389704	1
3407	13	São João do Manhuaçu	sao-joao-do-manhuacu	2013-09-03 15:30:12.389704	1
3408	13	São João do Manteninha	sao-joao-do-manteninha	2013-09-03 15:30:12.389704	1
3409	13	São João do Oriente	sao-joao-do-oriente	2013-09-03 15:30:12.389704	1
3410	13	São João do Pacuí	sao-joao-do-pacui	2013-09-03 15:30:12.389704	1
3411	13	São João do Paraíso	sao-joao-do-paraiso	2013-09-03 15:30:12.389704	1
3412	13	São João Evangelista	sao-joao-evangelista	2013-09-03 15:30:12.389704	1
3413	13	São João Nepomuceno	sao-joao-nepomuceno	2013-09-03 15:30:12.389704	1
3414	13	São Joaquim de Bicas	sao-joaquim-de-bicas	2013-09-03 15:30:12.389704	1
3415	13	São José da Barra	sao-jose-da-barra	2013-09-03 15:30:12.389704	1
3416	13	São José da Lapa	sao-jose-da-lapa	2013-09-03 15:30:12.389704	1
3417	13	São José da Safira	sao-jose-da-safira	2013-09-03 15:30:12.389704	1
3418	13	São José da Varginha	sao-jose-da-varginha	2013-09-03 15:30:12.389704	1
3419	13	São José do Alegre	sao-jose-do-alegre	2013-09-03 15:30:12.389704	1
3420	13	São José do Divino	sao-jose-do-divino	2013-09-03 15:30:12.389704	1
3421	13	São José do Goiabal	sao-jose-do-goiabal	2013-09-03 15:30:12.389704	1
3422	13	São José do Jacuri	sao-jose-do-jacuri	2013-09-03 15:30:12.389704	1
4336	16	Goioxim	goioxim	2013-09-03 15:30:12.389704	1
3423	13	São José do Mantimento	sao-jose-do-mantimento	2013-09-03 15:30:12.389704	1
3424	13	São Lourenço	sao-lourenco	2013-09-03 15:30:12.389704	1
3425	13	São Miguel do Anta	sao-miguel-do-anta	2013-09-03 15:30:12.389704	1
3426	13	São Pedro da União	sao-pedro-da-uniao	2013-09-03 15:30:12.389704	1
3427	13	São Pedro do Suaçuí	sao-pedro-do-suacui	2013-09-03 15:30:12.389704	1
3428	13	São Pedro dos Ferros	sao-pedro-dos-ferros	2013-09-03 15:30:12.389704	1
3429	13	São Romão	sao-romao	2013-09-03 15:30:12.389704	1
3430	13	São Roque de Minas	sao-roque-de-minas	2013-09-03 15:30:12.389704	1
3431	13	São Sebastião da Bela Vista	sao-sebastiao-da-bela-vista	2013-09-03 15:30:12.389704	1
3432	13	São Sebastião da Vargem Alegre	sao-sebastiao-da-vargem-alegre	2013-09-03 15:30:12.389704	1
3433	13	São Sebastião do Anta	sao-sebastiao-do-anta	2013-09-03 15:30:12.389704	1
3434	13	São Sebastião do Maranhão	sao-sebastiao-do-maranhao	2013-09-03 15:30:12.389704	1
3435	13	São Sebastião do Oeste	sao-sebastiao-do-oeste	2013-09-03 15:30:12.389704	1
3436	13	São Sebastião do Paraíso	sao-sebastiao-do-paraiso	2013-09-03 15:30:12.389704	1
3437	13	São Sebastião do Rio Preto	sao-sebastiao-do-rio-preto	2013-09-03 15:30:12.389704	1
3438	13	São Sebastião do Rio Verde	sao-sebastiao-do-rio-verde	2013-09-03 15:30:12.389704	1
3439	13	São Thomé das Letras	sao-thome-das-letras	2013-09-03 15:30:12.389704	1
3440	13	São Tiago	sao-tiago	2013-09-03 15:30:12.389704	1
3441	13	São Tomás de Aquino	sao-tomas-de-aquino	2013-09-03 15:30:12.389704	1
3442	13	São Vicente de Minas	sao-vicente-de-minas	2013-09-03 15:30:12.389704	1
3443	13	Sapucaí-Mirim	sapucai-mirim	2013-09-03 15:30:12.389704	1
3444	13	Sardoá	sardoa	2013-09-03 15:30:12.389704	1
3445	13	Sarzedo	sarzedo	2013-09-03 15:30:12.389704	1
3446	13	Sem-Peixe	sem-peixe	2013-09-03 15:30:12.389704	1
3447	13	Senador Amaral	senador-amaral	2013-09-03 15:30:12.389704	1
3448	13	Senador Cortes	senador-cortes	2013-09-03 15:30:12.389704	1
3449	13	Senador Firmino	senador-firmino	2013-09-03 15:30:12.389704	1
3450	13	Senador José Bento	senador-jose-bento	2013-09-03 15:30:12.389704	1
3451	13	Senador Modestino Gonçalves	senador-modestino-goncalves	2013-09-03 15:30:12.389704	1
3452	13	Senhora de Oliveira	senhora-de-oliveira	2013-09-03 15:30:12.389704	1
3453	13	Senhora do Porto	senhora-do-porto	2013-09-03 15:30:12.389704	1
3454	13	Senhora dos Remédios	senhora-dos-remedios	2013-09-03 15:30:12.389704	1
3455	13	Sericita	sericita	2013-09-03 15:30:12.389704	1
3456	13	Seritinga	seritinga	2013-09-03 15:30:12.389704	1
3457	13	Serra Azul de Minas	serra-azul-de-minas	2013-09-03 15:30:12.389704	1
3458	13	Serra da Saudade	serra-da-saudade	2013-09-03 15:30:12.389704	1
3459	13	Serra do Salitre	serra-do-salitre	2013-09-03 15:30:12.389704	1
3460	13	Serra dos Aimorés	serra-dos-aimores	2013-09-03 15:30:12.389704	1
3461	13	Serrania	serrania	2013-09-03 15:30:12.389704	1
3462	13	Serranópolis de Minas	serranopolis-de-minas	2013-09-03 15:30:12.389704	1
3463	13	Serranos	serranos	2013-09-03 15:30:12.389704	1
3464	13	Serro	serro	2013-09-03 15:30:12.389704	1
3465	13	Sete Lagoas	sete-lagoas	2013-09-03 15:30:12.389704	1
3466	13	Setubinha	setubinha	2013-09-03 15:30:12.389704	1
3467	13	Silveirânia	silveirania	2013-09-03 15:30:12.389704	1
3468	13	Silvianópolis	silvianopolis	2013-09-03 15:30:12.389704	1
3469	13	Simão Pereira	simao-pereira	2013-09-03 15:30:12.389704	1
3470	13	Simonésia	simonesia	2013-09-03 15:30:12.389704	1
3471	13	Sobrália	sobralia	2013-09-03 15:30:12.389704	1
3472	13	Soledade de Minas	soledade-de-minas	2013-09-03 15:30:12.389704	1
3473	13	Tabuleiro	tabuleiro	2013-09-03 15:30:12.389704	1
3474	13	Taiobeiras	taiobeiras	2013-09-03 15:30:12.389704	1
3475	13	Taparuba	taparuba	2013-09-03 15:30:12.389704	1
3476	13	Tapira	tapira	2013-09-03 15:30:12.389704	1
3477	13	Tapiraí	tapirai	2013-09-03 15:30:12.389704	1
3478	13	Taquaraçu de Minas	taquaracu-de-minas	2013-09-03 15:30:12.389704	1
3479	13	Tarumirim	tarumirim	2013-09-03 15:30:12.389704	1
3480	13	Teixeiras	teixeiras	2013-09-03 15:30:12.389704	1
3481	13	Teófilo Otoni	teofilo-otoni	2013-09-03 15:30:12.389704	1
3482	13	Timóteo	timoteo	2013-09-03 15:30:12.389704	1
3483	13	Tiradentes	tiradentes	2013-09-03 15:30:12.389704	1
3484	13	Tiros	tiros	2013-09-03 15:30:12.389704	1
3485	13	Tocantins	tocantins	2013-09-03 15:30:12.389704	1
3486	13	Tocos do Moji	tocos-do-moji	2013-09-03 15:30:12.389704	1
3487	13	Toledo	toledo	2013-09-03 15:30:12.389704	1
3488	13	Tombos	tombos	2013-09-03 15:30:12.389704	1
3489	13	Três Corações	tres-coracoes	2013-09-03 15:30:12.389704	1
3490	13	Três Marias	tres-marias	2013-09-03 15:30:12.389704	1
3491	13	Três Pontas	tres-pontas	2013-09-03 15:30:12.389704	1
3492	13	Tumiritinga	tumiritinga	2013-09-03 15:30:12.389704	1
3493	13	Tupaciguara	tupaciguara	2013-09-03 15:30:12.389704	1
3494	13	Turmalina	turmalina	2013-09-03 15:30:12.389704	1
3495	13	Turvolândia	turvolandia	2013-09-03 15:30:12.389704	1
3496	13	Ubá	uba	2013-09-03 15:30:12.389704	1
3497	13	Ubaí	ubai	2013-09-03 15:30:12.389704	1
3498	13	Ubaporanga	ubaporanga	2013-09-03 15:30:12.389704	1
3499	13	Uberaba	uberaba	2013-09-03 15:30:12.389704	1
3500	13	Uberlândia	uberlandia	2013-09-03 15:30:12.389704	1
3501	13	Umburatiba	umburatiba	2013-09-03 15:30:12.389704	1
3502	13	Unaí	unai	2013-09-03 15:30:12.389704	1
3503	13	União de Minas	uniao-de-minas	2013-09-03 15:30:12.389704	1
3504	13	Uruana de Minas	uruana-de-minas	2013-09-03 15:30:12.389704	1
3505	13	Urucânia	urucania	2013-09-03 15:30:12.389704	1
3506	13	Urucuia	urucuia	2013-09-03 15:30:12.389704	1
3507	13	Vargem Alegre	vargem-alegre	2013-09-03 15:30:12.389704	1
3508	13	Vargem Bonita	vargem-bonita	2013-09-03 15:30:12.389704	1
3509	13	Vargem Grande do Rio Pardo	vargem-grande-do-rio-pardo	2013-09-03 15:30:12.389704	1
3510	13	Varginha	varginha	2013-09-03 15:30:12.389704	1
3511	13	Varjão de Minas	varjao-de-minas	2013-09-03 15:30:12.389704	1
3512	13	Várzea da Palma	varzea-da-palma	2013-09-03 15:30:12.389704	1
3513	13	Varzelândia	varzelandia	2013-09-03 15:30:12.389704	1
3514	13	Vazante	vazante	2013-09-03 15:30:12.389704	1
3515	13	Verdelândia	verdelandia	2013-09-03 15:30:12.389704	1
3516	13	Veredinha	veredinha	2013-09-03 15:30:12.389704	1
3517	13	Veríssimo	verissimo	2013-09-03 15:30:12.389704	1
3518	13	Vermelho Novo	vermelho-novo	2013-09-03 15:30:12.389704	1
3519	13	Vespasiano	vespasiano	2013-09-03 15:30:12.389704	1
3520	13	Viçosa	vicosa	2013-09-03 15:30:12.389704	1
3521	13	Vieiras	vieiras	2013-09-03 15:30:12.389704	1
3522	13	Virgem da Lapa	virgem-da-lapa	2013-09-03 15:30:12.389704	1
3523	13	Virgínia	virginia	2013-09-03 15:30:12.389704	1
3524	13	Virginópolis	virginopolis	2013-09-03 15:30:12.389704	1
3525	13	Virgolândia	virgolandia	2013-09-03 15:30:12.389704	1
3526	13	Visconde do Rio Branco	visconde-do-rio-branco	2013-09-03 15:30:12.389704	1
3527	13	Volta Grande	volta-grande	2013-09-03 15:30:12.389704	1
3528	13	Wenceslau Braz	wenceslau-braz	2013-09-03 15:30:12.389704	1
3699	14	Abaetetuba	abaetetuba	2013-09-03 15:30:12.389704	1
3700	14	Abel Figueiredo	abel-figueiredo	2013-09-03 15:30:12.389704	1
3701	14	Acará	acara	2013-09-03 15:30:12.389704	1
3702	14	Afuá	afua	2013-09-03 15:30:12.389704	1
3703	14	Água Azul do Norte	agua-azul-do-norte	2013-09-03 15:30:12.389704	1
3704	14	Alenquer	alenquer	2013-09-03 15:30:12.389704	1
3705	14	Almeirim	almeirim	2013-09-03 15:30:12.389704	1
3706	14	Altamira	altamira	2013-09-03 15:30:12.389704	1
3707	14	Anajás	anajas	2013-09-03 15:30:12.389704	1
3708	14	Ananindeua	ananindeua	2013-09-03 15:30:12.389704	1
3709	14	Anapu	anapu	2013-09-03 15:30:12.389704	1
3710	14	Augusto Corrêa	augusto-correa	2013-09-03 15:30:12.389704	1
3711	14	Aurora do Pará	aurora-do-para	2013-09-03 15:30:12.389704	1
3712	14	Aveiro	aveiro	2013-09-03 15:30:12.389704	1
3713	14	Bagre	bagre	2013-09-03 15:30:12.389704	1
3714	14	Baião	baiao	2013-09-03 15:30:12.389704	1
3715	14	Bannach	bannach	2013-09-03 15:30:12.389704	1
3716	14	Barcarena	barcarena	2013-09-03 15:30:12.389704	1
3717	14	Belém	belem	2013-09-03 15:30:12.389704	1
3718	14	Belterra	belterra	2013-09-03 15:30:12.389704	1
3719	14	Benevides	benevides	2013-09-03 15:30:12.389704	1
3720	14	Bom Jesus do Tocantins	bom-jesus-do-tocantins	2013-09-03 15:30:12.389704	1
3721	14	Bonito	bonito	2013-09-03 15:30:12.389704	1
3722	14	Bragança	braganca	2013-09-03 15:30:12.389704	1
3723	14	Brasil Novo	brasil-novo	2013-09-03 15:30:12.389704	1
3724	14	Brejo Grande do Araguaia	brejo-grande-do-araguaia	2013-09-03 15:30:12.389704	1
3725	14	Breu Branco	breu-branco	2013-09-03 15:30:12.389704	1
3726	14	Breves	breves	2013-09-03 15:30:12.389704	1
3727	14	Bujaru	bujaru	2013-09-03 15:30:12.389704	1
3728	14	Cachoeira do Arari	cachoeira-do-arari	2013-09-03 15:30:12.389704	1
3729	14	Cachoeira do Piriá	cachoeira-do-piria	2013-09-03 15:30:12.389704	1
3730	14	Cametá	cameta	2013-09-03 15:30:12.389704	1
3731	14	Canaã dos Carajás	canaa-dos-carajas	2013-09-03 15:30:12.389704	1
3732	14	Capanema	capanema	2013-09-03 15:30:12.389704	1
3733	14	Capitão Poço	capitao-poco	2013-09-03 15:30:12.389704	1
3734	14	Castanhal	castanhal	2013-09-03 15:30:12.389704	1
3735	14	Chaves	chaves	2013-09-03 15:30:12.389704	1
3736	14	Colares	colares	2013-09-03 15:30:12.389704	1
3737	14	Conceição do Araguaia	conceicao-do-araguaia	2013-09-03 15:30:12.389704	1
3738	14	Concórdia do Pará	concordia-do-para	2013-09-03 15:30:12.389704	1
3739	14	Cumaru do Norte	cumaru-do-norte	2013-09-03 15:30:12.389704	1
3740	14	Curionópolis	curionopolis	2013-09-03 15:30:12.389704	1
3741	14	Curralinho	curralinho	2013-09-03 15:30:12.389704	1
3742	14	Curuá	curua	2013-09-03 15:30:12.389704	1
3743	14	Curuçá	curuca	2013-09-03 15:30:12.389704	1
3744	14	Dom Eliseu	dom-eliseu	2013-09-03 15:30:12.389704	1
3745	14	Eldorado dos Carajás	eldorado-dos-carajas	2013-09-03 15:30:12.389704	1
3746	14	Faro	faro	2013-09-03 15:30:12.389704	1
3747	14	Floresta do Araguaia	floresta-do-araguaia	2013-09-03 15:30:12.389704	1
3748	14	Garrafão do Norte	garrafao-do-norte	2013-09-03 15:30:12.389704	1
3749	14	Goianésia do Pará	goianesia-do-para	2013-09-03 15:30:12.389704	1
3750	14	Gurupá	gurupa	2013-09-03 15:30:12.389704	1
3751	14	Igarapé-Açu	igarape-acu	2013-09-03 15:30:12.389704	1
3752	14	Igarapé-Miri	igarape-miri	2013-09-03 15:30:12.389704	1
3753	14	Inhangapi	inhangapi	2013-09-03 15:30:12.389704	1
3754	14	Ipixuna do Pará	ipixuna-do-para	2013-09-03 15:30:12.389704	1
3755	14	Irituia	irituia	2013-09-03 15:30:12.389704	1
3756	14	Itaituba	itaituba	2013-09-03 15:30:12.389704	1
3757	14	Itupiranga	itupiranga	2013-09-03 15:30:12.389704	1
3758	14	Jacareacanga	jacareacanga	2013-09-03 15:30:12.389704	1
3759	14	Jacundá	jacunda	2013-09-03 15:30:12.389704	1
3760	14	Juruti	juruti	2013-09-03 15:30:12.389704	1
3761	14	Limoeiro do Ajuru	limoeiro-do-ajuru	2013-09-03 15:30:12.389704	1
3762	14	Mãe do Rio	mae-do-rio	2013-09-03 15:30:12.389704	1
3763	14	Magalhães Barata	magalhaes-barata	2013-09-03 15:30:12.389704	1
3764	14	Marabá	maraba	2013-09-03 15:30:12.389704	1
3765	14	Maracanã	maracana	2013-09-03 15:30:12.389704	1
3766	14	Marapanim	marapanim	2013-09-03 15:30:12.389704	1
3767	14	Marituba	marituba	2013-09-03 15:30:12.389704	1
3768	14	Medicilândia	medicilandia	2013-09-03 15:30:12.389704	1
3769	14	Melgaço	melgaco	2013-09-03 15:30:12.389704	1
3770	14	Mocajuba	mocajuba	2013-09-03 15:30:12.389704	1
3771	14	Moju	moju	2013-09-03 15:30:12.389704	1
3772	14	Monte Alegre	monte-alegre	2013-09-03 15:30:12.389704	1
3773	14	Muaná	muana	2013-09-03 15:30:12.389704	1
3774	14	Nova Esperança do Piriá	nova-esperanca-do-piria	2013-09-03 15:30:12.389704	1
3775	14	Nova Ipixuna	nova-ipixuna	2013-09-03 15:30:12.389704	1
3776	14	Nova Timboteua	nova-timboteua	2013-09-03 15:30:12.389704	1
3777	14	Novo Progresso	novo-progresso	2013-09-03 15:30:12.389704	1
3778	14	Novo Repartimento	novo-repartimento	2013-09-03 15:30:12.389704	1
3779	14	Óbidos	obidos	2013-09-03 15:30:12.389704	1
3780	14	Oeiras do Pará	oeiras-do-para	2013-09-03 15:30:12.389704	1
3781	14	Oriximiná	oriximina	2013-09-03 15:30:12.389704	1
3782	14	Ourém	ourem	2013-09-03 15:30:12.389704	1
3783	14	Ourilândia do Norte	ourilandia-do-norte	2013-09-03 15:30:12.389704	1
3784	14	Pacajá	pacaja	2013-09-03 15:30:12.389704	1
3785	14	Palestina do Pará	palestina-do-para	2013-09-03 15:30:12.389704	1
3786	14	Paragominas	paragominas	2013-09-03 15:30:12.389704	1
3787	14	Parauapebas	parauapebas	2013-09-03 15:30:12.389704	1
3788	14	Pau d`Arco	pau-d-arco	2013-09-03 15:30:12.389704	1
3789	14	Peixe-Boi	peixe-boi	2013-09-03 15:30:12.389704	1
3790	14	Piçarra	picarra	2013-09-03 15:30:12.389704	1
3791	14	Placas	placas	2013-09-03 15:30:12.389704	1
3792	14	Ponta de Pedras	ponta-de-pedras	2013-09-03 15:30:12.389704	1
3793	14	Portel	portel	2013-09-03 15:30:12.389704	1
3794	14	Porto de Moz	porto-de-moz	2013-09-03 15:30:12.389704	1
3795	14	Prainha	prainha	2013-09-03 15:30:12.389704	1
3796	14	Primavera	primavera	2013-09-03 15:30:12.389704	1
3797	14	Quatipuru	quatipuru	2013-09-03 15:30:12.389704	1
3798	14	Redenção	redencao	2013-09-03 15:30:12.389704	1
3799	14	Rio Maria	rio-maria	2013-09-03 15:30:12.389704	1
3800	14	Rondon do Pará	rondon-do-para	2013-09-03 15:30:12.389704	1
3801	14	Rurópolis	ruropolis	2013-09-03 15:30:12.389704	1
3802	14	Salinópolis	salinopolis	2013-09-03 15:30:12.389704	1
3803	14	Salvaterra	salvaterra	2013-09-03 15:30:12.389704	1
3804	14	Santa Bárbara do Pará	santa-barbara-do-para	2013-09-03 15:30:12.389704	1
3805	14	Santa Cruz do Arari	santa-cruz-do-arari	2013-09-03 15:30:12.389704	1
3806	14	Santa Isabel do Pará	santa-isabel-do-para	2013-09-03 15:30:12.389704	1
3807	14	Santa Luzia do Pará	santa-luzia-do-para	2013-09-03 15:30:12.389704	1
3808	14	Santa Maria das Barreiras	santa-maria-das-barreiras	2013-09-03 15:30:12.389704	1
3809	14	Santa Maria do Pará	santa-maria-do-para	2013-09-03 15:30:12.389704	1
3810	14	Santana do Araguaia	santana-do-araguaia	2013-09-03 15:30:12.389704	1
3811	14	Santarém	santarem	2013-09-03 15:30:12.389704	1
3812	14	Santarém Novo	santarem-novo	2013-09-03 15:30:12.389704	1
3813	14	Santo Antônio do Tauá	santo-antonio-do-taua	2013-09-03 15:30:12.389704	1
3814	14	São Caetano de Odivelas	sao-caetano-de-odivelas	2013-09-03 15:30:12.389704	1
3815	14	São Domingos do Araguaia	sao-domingos-do-araguaia	2013-09-03 15:30:12.389704	1
3816	14	São Domingos do Capim	sao-domingos-do-capim	2013-09-03 15:30:12.389704	1
3817	14	São Félix do Xingu	sao-felix-do-xingu	2013-09-03 15:30:12.389704	1
3818	14	São Francisco do Pará	sao-francisco-do-para	2013-09-03 15:30:12.389704	1
3819	14	São Geraldo do Araguaia	sao-geraldo-do-araguaia	2013-09-03 15:30:12.389704	1
3820	14	São João da Ponta	sao-joao-da-ponta	2013-09-03 15:30:12.389704	1
3821	14	São João de Pirabas	sao-joao-de-pirabas	2013-09-03 15:30:12.389704	1
3822	14	São João do Araguaia	sao-joao-do-araguaia	2013-09-03 15:30:12.389704	1
3823	14	São Miguel do Guamá	sao-miguel-do-guama	2013-09-03 15:30:12.389704	1
3824	14	São Sebastião da Boa Vista	sao-sebastiao-da-boa-vista	2013-09-03 15:30:12.389704	1
3825	14	Sapucaia	sapucaia	2013-09-03 15:30:12.389704	1
3826	14	Senador José Porfírio	senador-jose-porfirio	2013-09-03 15:30:12.389704	1
3827	14	Soure	soure	2013-09-03 15:30:12.389704	1
3828	14	Tailândia	tailandia	2013-09-03 15:30:12.389704	1
3829	14	Terra Alta	terra-alta	2013-09-03 15:30:12.389704	1
3830	14	Terra Santa	terra-santa	2013-09-03 15:30:12.389704	1
3831	14	Tomé-Açu	tome-acu	2013-09-03 15:30:12.389704	1
3832	14	Tracuateua	tracuateua	2013-09-03 15:30:12.389704	1
3833	14	Trairão	trairao	2013-09-03 15:30:12.389704	1
3834	14	Tucumã	tucuma	2013-09-03 15:30:12.389704	1
3835	14	Tucuruí	tucurui	2013-09-03 15:30:12.389704	1
3836	14	Ulianópolis	ulianopolis	2013-09-03 15:30:12.389704	1
3837	14	Uruará	uruara	2013-09-03 15:30:12.389704	1
3838	14	Vigia	vigia	2013-09-03 15:30:12.389704	1
3839	14	Viseu	viseu	2013-09-03 15:30:12.389704	1
3840	14	Vitória do Xingu	vitoria-do-xingu	2013-09-03 15:30:12.389704	1
3841	14	Xinguara	xinguara	2013-09-03 15:30:12.389704	1
3954	15	Água Branca	agua-branca	2013-09-03 15:30:12.389704	1
3955	15	Aguiar	aguiar	2013-09-03 15:30:12.389704	1
3956	15	Alagoa Grande	alagoa-grande	2013-09-03 15:30:12.389704	1
3957	15	Alagoa Nova	alagoa-nova	2013-09-03 15:30:12.389704	1
3958	15	Alagoinha	alagoinha	2013-09-03 15:30:12.389704	1
3959	15	Alcantil	alcantil	2013-09-03 15:30:12.389704	1
3960	15	Algodão de Jandaíra	algodao-de-jandaira	2013-09-03 15:30:12.389704	1
3961	15	Alhandra	alhandra	2013-09-03 15:30:12.389704	1
3962	15	Amparo	amparo	2013-09-03 15:30:12.389704	1
3963	15	Aparecida	aparecida	2013-09-03 15:30:12.389704	1
3964	15	Araçagi	aracagi	2013-09-03 15:30:12.389704	1
3965	15	Arara	arara	2013-09-03 15:30:12.389704	1
3966	15	Araruna	araruna	2013-09-03 15:30:12.389704	1
3967	15	Areia	areia	2013-09-03 15:30:12.389704	1
3968	15	Areia de Baraúnas	areia-de-baraunas	2013-09-03 15:30:12.389704	1
3969	15	Areial	areial	2013-09-03 15:30:12.389704	1
3970	15	Aroeiras	aroeiras	2013-09-03 15:30:12.389704	1
3971	15	Assunção	assuncao	2013-09-03 15:30:12.389704	1
3972	15	Baía da Traição	baia-da-traicao	2013-09-03 15:30:12.389704	1
3973	15	Bananeiras	bananeiras	2013-09-03 15:30:12.389704	1
3974	15	Baraúna	barauna	2013-09-03 15:30:12.389704	1
3975	15	Barra de Santa Rosa	barra-de-santa-rosa	2013-09-03 15:30:12.389704	1
3976	15	Barra de Santana	barra-de-santana	2013-09-03 15:30:12.389704	1
3977	15	Barra de São Miguel	barra-de-sao-miguel	2013-09-03 15:30:12.389704	1
3978	15	Bayeux	bayeux	2013-09-03 15:30:12.389704	1
3979	15	Belém	belem	2013-09-03 15:30:12.389704	1
3980	15	Belém do Brejo do Cruz	belem-do-brejo-do-cruz	2013-09-03 15:30:12.389704	1
3981	15	Bernardino Batista	bernardino-batista	2013-09-03 15:30:12.389704	1
3982	15	Boa Ventura	boa-ventura	2013-09-03 15:30:12.389704	1
3983	15	Boa Vista	boa-vista	2013-09-03 15:30:12.389704	1
3984	15	Bom Jesus	bom-jesus	2013-09-03 15:30:12.389704	1
3985	15	Bom Sucesso	bom-sucesso	2013-09-03 15:30:12.389704	1
3986	15	Bonito de Santa Fé	bonito-de-santa-fe	2013-09-03 15:30:12.389704	1
3987	15	Boqueirão	boqueirao	2013-09-03 15:30:12.389704	1
3988	15	Borborema	borborema	2013-09-03 15:30:12.389704	1
3989	15	Brejo do Cruz	brejo-do-cruz	2013-09-03 15:30:12.389704	1
3990	15	Brejo dos Santos	brejo-dos-santos	2013-09-03 15:30:12.389704	1
3991	15	Caaporã	caapora	2013-09-03 15:30:12.389704	1
3992	15	Cabaceiras	cabaceiras	2013-09-03 15:30:12.389704	1
3993	15	Cabedelo	cabedelo	2013-09-03 15:30:12.389704	1
3994	15	Cachoeira dos Índios	cachoeira-dos-indios	2013-09-03 15:30:12.389704	1
3995	15	Cacimba de Areia	cacimba-de-areia	2013-09-03 15:30:12.389704	1
3996	15	Cacimba de Dentro	cacimba-de-dentro	2013-09-03 15:30:12.389704	1
3997	15	Cacimbas	cacimbas	2013-09-03 15:30:12.389704	1
3998	15	Caiçara	caicara	2013-09-03 15:30:12.389704	1
3999	15	Cajazeiras	cajazeiras	2013-09-03 15:30:12.389704	1
4000	15	Cajazeirinhas	cajazeirinhas	2013-09-03 15:30:12.389704	1
4001	15	Caldas Brandão	caldas-brandao	2013-09-03 15:30:12.389704	1
4002	15	Camalaú	camalau	2013-09-03 15:30:12.389704	1
4003	15	Campina Grande	campina-grande	2013-09-03 15:30:12.389704	1
4004	15	Campo de Santana	campo-de-santana	2013-09-03 15:30:12.389704	1
4005	15	Capim	capim	2013-09-03 15:30:12.389704	1
4006	15	Caraúbas	caraubas	2013-09-03 15:30:12.389704	1
4007	15	Carrapateira	carrapateira	2013-09-03 15:30:12.389704	1
4008	15	Casserengue	casserengue	2013-09-03 15:30:12.389704	1
4009	15	Catingueira	catingueira	2013-09-03 15:30:12.389704	1
4010	15	Catolé do Rocha	catole-do-rocha	2013-09-03 15:30:12.389704	1
4011	15	Caturité	caturite	2013-09-03 15:30:12.389704	1
4012	15	Conceição	conceicao	2013-09-03 15:30:12.389704	1
4013	15	Condado	condado	2013-09-03 15:30:12.389704	1
4014	15	Conde	conde	2013-09-03 15:30:12.389704	1
4015	15	Congo	congo	2013-09-03 15:30:12.389704	1
4016	15	Coremas	coremas	2013-09-03 15:30:12.389704	1
4017	15	Coxixola	coxixola	2013-09-03 15:30:12.389704	1
4018	15	Cruz do Espírito Santo	cruz-do-espirito-santo	2013-09-03 15:30:12.389704	1
4019	15	Cubati	cubati	2013-09-03 15:30:12.389704	1
4020	15	Cuité	cuite	2013-09-03 15:30:12.389704	1
4021	15	Cuité de Mamanguape	cuite-de-mamanguape	2013-09-03 15:30:12.389704	1
4022	15	Cuitegi	cuitegi	2013-09-03 15:30:12.389704	1
4023	15	Curral de Cima	curral-de-cima	2013-09-03 15:30:12.389704	1
4024	15	Curral Velho	curral-velho	2013-09-03 15:30:12.389704	1
4025	15	Damião	damiao	2013-09-03 15:30:12.389704	1
4026	15	Desterro	desterro	2013-09-03 15:30:12.389704	1
4027	15	Diamante	diamante	2013-09-03 15:30:12.389704	1
4028	15	Dona Inês	dona-ines	2013-09-03 15:30:12.389704	1
4029	15	Duas Estradas	duas-estradas	2013-09-03 15:30:12.389704	1
4030	15	Emas	emas	2013-09-03 15:30:12.389704	1
4031	15	Esperança	esperanca	2013-09-03 15:30:12.389704	1
4032	15	Fagundes	fagundes	2013-09-03 15:30:12.389704	1
4033	15	Frei Martinho	frei-martinho	2013-09-03 15:30:12.389704	1
4034	15	Gado Bravo	gado-bravo	2013-09-03 15:30:12.389704	1
4035	15	Guarabira	guarabira	2013-09-03 15:30:12.389704	1
4036	15	Gurinhém	gurinhem	2013-09-03 15:30:12.389704	1
4037	15	Gurjão	gurjao	2013-09-03 15:30:12.389704	1
4038	15	Ibiara	ibiara	2013-09-03 15:30:12.389704	1
4039	15	Igaracy	igaracy	2013-09-03 15:30:12.389704	1
4040	15	Imaculada	imaculada	2013-09-03 15:30:12.389704	1
4041	15	Ingá	inga	2013-09-03 15:30:12.389704	1
4042	15	Itabaiana	itabaiana	2013-09-03 15:30:12.389704	1
4043	15	Itaporanga	itaporanga	2013-09-03 15:30:12.389704	1
4044	15	Itapororoca	itapororoca	2013-09-03 15:30:12.389704	1
4045	15	Itatuba	itatuba	2013-09-03 15:30:12.389704	1
4046	15	Jacaraú	jacarau	2013-09-03 15:30:12.389704	1
4047	15	Jericó	jerico	2013-09-03 15:30:12.389704	1
4048	15	João Pessoa	joao-pessoa	2013-09-03 15:30:12.389704	1
4049	15	Juarez Távora	juarez-tavora	2013-09-03 15:30:12.389704	1
4050	15	Juazeirinho	juazeirinho	2013-09-03 15:30:12.389704	1
4051	15	Junco do Seridó	junco-do-serido	2013-09-03 15:30:12.389704	1
4052	15	Juripiranga	juripiranga	2013-09-03 15:30:12.389704	1
4053	15	Juru	juru	2013-09-03 15:30:12.389704	1
4054	15	Lagoa	lagoa	2013-09-03 15:30:12.389704	1
4055	15	Lagoa de Dentro	lagoa-de-dentro	2013-09-03 15:30:12.389704	1
4056	15	Lagoa Seca	lagoa-seca	2013-09-03 15:30:12.389704	1
4057	15	Lastro	lastro	2013-09-03 15:30:12.389704	1
4058	15	Livramento	livramento	2013-09-03 15:30:12.389704	1
4059	15	Logradouro	logradouro	2013-09-03 15:30:12.389704	1
4060	15	Lucena	lucena	2013-09-03 15:30:12.389704	1
4061	15	Mãe d`Água	mae-d-agua	2013-09-03 15:30:12.389704	1
4062	15	Malta	malta	2013-09-03 15:30:12.389704	1
4063	15	Mamanguape	mamanguape	2013-09-03 15:30:12.389704	1
4064	15	Manaíra	manaira	2013-09-03 15:30:12.389704	1
4065	15	Marcação	marcacao	2013-09-03 15:30:12.389704	1
4066	15	Mari	mari	2013-09-03 15:30:12.389704	1
4067	15	Marizópolis	marizopolis	2013-09-03 15:30:12.389704	1
4068	15	Massaranduba	massaranduba	2013-09-03 15:30:12.389704	1
4069	15	Mataraca	mataraca	2013-09-03 15:30:12.389704	1
4070	15	Matinhas	matinhas	2013-09-03 15:30:12.389704	1
4071	15	Mato Grosso	mato-grosso	2013-09-03 15:30:12.389704	1
4072	15	Maturéia	matureia	2013-09-03 15:30:12.389704	1
4073	15	Mogeiro	mogeiro	2013-09-03 15:30:12.389704	1
4074	15	Montadas	montadas	2013-09-03 15:30:12.389704	1
4075	15	Monte Horebe	monte-horebe	2013-09-03 15:30:12.389704	1
4076	15	Monteiro	monteiro	2013-09-03 15:30:12.389704	1
4077	15	Mulungu	mulungu	2013-09-03 15:30:12.389704	1
4078	15	Natuba	natuba	2013-09-03 15:30:12.389704	1
4079	15	Nazarezinho	nazarezinho	2013-09-03 15:30:12.389704	1
4080	15	Nova Floresta	nova-floresta	2013-09-03 15:30:12.389704	1
4081	15	Nova Olinda	nova-olinda	2013-09-03 15:30:12.389704	1
4082	15	Nova Palmeira	nova-palmeira	2013-09-03 15:30:12.389704	1
4083	15	Olho d`Água	olho-d-agua	2013-09-03 15:30:12.389704	1
4084	15	Olivedos	olivedos	2013-09-03 15:30:12.389704	1
4085	15	Ouro Velho	ouro-velho	2013-09-03 15:30:12.389704	1
4086	15	Parari	parari	2013-09-03 15:30:12.389704	1
4087	15	Passagem	passagem	2013-09-03 15:30:12.389704	1
4088	15	Patos	patos	2013-09-03 15:30:12.389704	1
4089	15	Paulista	paulista	2013-09-03 15:30:12.389704	1
4090	15	Pedra Branca	pedra-branca	2013-09-03 15:30:12.389704	1
4091	15	Pedra Lavrada	pedra-lavrada	2013-09-03 15:30:12.389704	1
4092	15	Pedras de Fogo	pedras-de-fogo	2013-09-03 15:30:12.389704	1
4093	15	Pedro Régis	pedro-regis	2013-09-03 15:30:12.389704	1
4094	15	Piancó	pianco	2013-09-03 15:30:12.389704	1
4095	15	Picuí	picui	2013-09-03 15:30:12.389704	1
4096	15	Pilar	pilar	2013-09-03 15:30:12.389704	1
4097	15	Pilões	piloes	2013-09-03 15:30:12.389704	1
4098	15	Pilõezinhos	piloezinhos	2013-09-03 15:30:12.389704	1
4099	15	Pirpirituba	pirpirituba	2013-09-03 15:30:12.389704	1
4100	15	Pitimbu	pitimbu	2013-09-03 15:30:12.389704	1
4101	15	Pocinhos	pocinhos	2013-09-03 15:30:12.389704	1
4102	15	Poço Dantas	poco-dantas	2013-09-03 15:30:12.389704	1
4103	15	Poço de José de Moura	poco-de-jose-de-moura	2013-09-03 15:30:12.389704	1
4104	15	Pombal	pombal	2013-09-03 15:30:12.389704	1
4105	15	Prata	prata	2013-09-03 15:30:12.389704	1
4106	15	Princesa Isabel	princesa-isabel	2013-09-03 15:30:12.389704	1
4107	15	Puxinanã	puxinana	2013-09-03 15:30:12.389704	1
4108	15	Queimadas	queimadas	2013-09-03 15:30:12.389704	1
4109	15	Quixabá	quixaba	2013-09-03 15:30:12.389704	1
4110	15	Remígio	remigio	2013-09-03 15:30:12.389704	1
4111	15	Riachão	riachao	2013-09-03 15:30:12.389704	1
4112	15	Riachão do Bacamarte	riachao-do-bacamarte	2013-09-03 15:30:12.389704	1
4113	15	Riachão do Poço	riachao-do-poco	2013-09-03 15:30:12.389704	1
4114	15	Riacho de Santo Antônio	riacho-de-santo-antonio	2013-09-03 15:30:12.389704	1
4115	15	Riacho dos Cavalos	riacho-dos-cavalos	2013-09-03 15:30:12.389704	1
4116	15	Rio Tinto	rio-tinto	2013-09-03 15:30:12.389704	1
4117	15	Salgadinho	salgadinho	2013-09-03 15:30:12.389704	1
4118	15	Salgado de São Félix	salgado-de-sao-felix	2013-09-03 15:30:12.389704	1
4119	15	Santa Cecília	santa-cecilia	2013-09-03 15:30:12.389704	1
4120	15	Santa Cruz	santa-cruz	2013-09-03 15:30:12.389704	1
4121	15	Santa Helena	santa-helena	2013-09-03 15:30:12.389704	1
4122	15	Santa Inês	santa-ines	2013-09-03 15:30:12.389704	1
4123	15	Santa Luzia	santa-luzia	2013-09-03 15:30:12.389704	1
4124	15	Santa Rita	santa-rita	2013-09-03 15:30:12.389704	1
4125	15	Santa Teresinha	santa-teresinha	2013-09-03 15:30:12.389704	1
4126	15	Santana de Mangueira	santana-de-mangueira	2013-09-03 15:30:12.389704	1
4127	15	Santana dos Garrotes	santana-dos-garrotes	2013-09-03 15:30:12.389704	1
4128	15	Santarém	santarem	2013-09-03 15:30:12.389704	1
4129	15	Santo André	santo-andre	2013-09-03 15:30:12.389704	1
4130	15	São Bentinho	sao-bentinho	2013-09-03 15:30:12.389704	1
4131	15	São Bento	sao-bento	2013-09-03 15:30:12.389704	1
4132	15	São Domingos de Pombal	sao-domingos-de-pombal	2013-09-03 15:30:12.389704	1
4133	15	São Domingos do Cariri	sao-domingos-do-cariri	2013-09-03 15:30:12.389704	1
4134	15	São Francisco	sao-francisco	2013-09-03 15:30:12.389704	1
4135	15	São João do Cariri	sao-joao-do-cariri	2013-09-03 15:30:12.389704	1
4136	15	São João do Rio do Peixe	sao-joao-do-rio-do-peixe	2013-09-03 15:30:12.389704	1
4137	15	São João do Tigre	sao-joao-do-tigre	2013-09-03 15:30:12.389704	1
4138	15	São José da Lagoa Tapada	sao-jose-da-lagoa-tapada	2013-09-03 15:30:12.389704	1
4139	15	São José de Caiana	sao-jose-de-caiana	2013-09-03 15:30:12.389704	1
4140	15	São José de Espinharas	sao-jose-de-espinharas	2013-09-03 15:30:12.389704	1
4141	15	São José de Piranhas	sao-jose-de-piranhas	2013-09-03 15:30:12.389704	1
4142	15	São José de Princesa	sao-jose-de-princesa	2013-09-03 15:30:12.389704	1
4143	15	São José do Bonfim	sao-jose-do-bonfim	2013-09-03 15:30:12.389704	1
4144	15	São José do Brejo do Cruz	sao-jose-do-brejo-do-cruz	2013-09-03 15:30:12.389704	1
4145	15	São José do Sabugi	sao-jose-do-sabugi	2013-09-03 15:30:12.389704	1
4146	15	São José dos Cordeiros	sao-jose-dos-cordeiros	2013-09-03 15:30:12.389704	1
4147	15	São José dos Ramos	sao-jose-dos-ramos	2013-09-03 15:30:12.389704	1
4148	15	São Mamede	sao-mamede	2013-09-03 15:30:12.389704	1
4149	15	São Miguel de Taipu	sao-miguel-de-taipu	2013-09-03 15:30:12.389704	1
4150	15	São Sebastião de Lagoa de Roça	sao-sebastiao-de-lagoa-de-roca	2013-09-03 15:30:12.389704	1
4151	15	São Sebastião do Umbuzeiro	sao-sebastiao-do-umbuzeiro	2013-09-03 15:30:12.389704	1
4152	15	Sapé	sape	2013-09-03 15:30:12.389704	1
4153	15	Seridó	serido	2013-09-03 15:30:12.389704	1
4154	15	Serra Branca	serra-branca	2013-09-03 15:30:12.389704	1
4155	15	Serra da Raiz	serra-da-raiz	2013-09-03 15:30:12.389704	1
4156	15	Serra Grande	serra-grande	2013-09-03 15:30:12.389704	1
4157	15	Serra Redonda	serra-redonda	2013-09-03 15:30:12.389704	1
4158	15	Serraria	serraria	2013-09-03 15:30:12.389704	1
4159	15	Sertãozinho	sertaozinho	2013-09-03 15:30:12.389704	1
4160	15	Sobrado	sobrado	2013-09-03 15:30:12.389704	1
4161	15	Solânea	solanea	2013-09-03 15:30:12.389704	1
4162	15	Soledade	soledade	2013-09-03 15:30:12.389704	1
4163	15	Sossêgo	sossego	2013-09-03 15:30:12.389704	1
4164	15	Sousa	sousa	2013-09-03 15:30:12.389704	1
4165	15	Sumé	sume	2013-09-03 15:30:12.389704	1
4166	15	Taperoá	taperoa	2013-09-03 15:30:12.389704	1
4167	15	Tavares	tavares	2013-09-03 15:30:12.389704	1
4168	15	Teixeira	teixeira	2013-09-03 15:30:12.389704	1
4169	15	Tenório	tenorio	2013-09-03 15:30:12.389704	1
4170	15	Triunfo	triunfo	2013-09-03 15:30:12.389704	1
4171	15	Uiraúna	uirauna	2013-09-03 15:30:12.389704	1
4172	15	Umbuzeiro	umbuzeiro	2013-09-03 15:30:12.389704	1
4173	15	Várzea	varzea	2013-09-03 15:30:12.389704	1
4174	15	Vieirópolis	vieiropolis	2013-09-03 15:30:12.389704	1
4175	15	Vista Serrana	vista-serrana	2013-09-03 15:30:12.389704	1
4176	15	Zabelê	zabele	2013-09-03 15:30:12.389704	1
4209	16	Abatiá	abatia	2013-09-03 15:30:12.389704	1
4210	16	Adrianópolis	adrianopolis	2013-09-03 15:30:12.389704	1
4211	16	Agudos do Sul	agudos-do-sul	2013-09-03 15:30:12.389704	1
4212	16	Almirante Tamandaré	almirante-tamandare	2013-09-03 15:30:12.389704	1
4213	16	Altamira do Paraná	altamira-do-parana	2013-09-03 15:30:12.389704	1
4214	16	Alto Paraíso	alto-paraiso	2013-09-03 15:30:12.389704	1
4215	16	Alto Paraná	alto-parana	2013-09-03 15:30:12.389704	1
4216	16	Alto Piquiri	alto-piquiri	2013-09-03 15:30:12.389704	1
4217	16	Altônia	altonia	2013-09-03 15:30:12.389704	1
4218	16	Alvorada do Sul	alvorada-do-sul	2013-09-03 15:30:12.389704	1
4219	16	Amaporã	amapora	2013-09-03 15:30:12.389704	1
4220	16	Ampére	ampere	2013-09-03 15:30:12.389704	1
4221	16	Anahy	anahy	2013-09-03 15:30:12.389704	1
4222	16	Andirá	andira	2013-09-03 15:30:12.389704	1
4223	16	Ângulo	angulo	2013-09-03 15:30:12.389704	1
4224	16	Antonina	antonina	2013-09-03 15:30:12.389704	1
4225	16	Antônio Olinto	antonio-olinto	2013-09-03 15:30:12.389704	1
4226	16	Apucarana	apucarana	2013-09-03 15:30:12.389704	1
4227	16	Arapongas	arapongas	2013-09-03 15:30:12.389704	1
4228	16	Arapoti	arapoti	2013-09-03 15:30:12.389704	1
4229	16	Arapuã	arapua	2013-09-03 15:30:12.389704	1
4230	16	Araruna	araruna	2013-09-03 15:30:12.389704	1
4231	16	Araucária	araucaria	2013-09-03 15:30:12.389704	1
4232	16	Ariranha do Ivaí	ariranha-do-ivai	2013-09-03 15:30:12.389704	1
4233	16	Assaí	assai	2013-09-03 15:30:12.389704	1
4234	16	Assis Chateaubriand	assis-chateaubriand	2013-09-03 15:30:12.389704	1
4235	16	Astorga	astorga	2013-09-03 15:30:12.389704	1
4236	16	Atalaia	atalaia	2013-09-03 15:30:12.389704	1
4237	16	Balsa Nova	balsa-nova	2013-09-03 15:30:12.389704	1
4238	16	Bandeirantes	bandeirantes	2013-09-03 15:30:12.389704	1
4239	16	Barbosa Ferraz	barbosa-ferraz	2013-09-03 15:30:12.389704	1
4240	16	Barra do Jacaré	barra-do-jacare	2013-09-03 15:30:12.389704	1
4241	16	Barracão	barracao	2013-09-03 15:30:12.389704	1
4242	16	Bela Vista da Caroba	bela-vista-da-caroba	2013-09-03 15:30:12.389704	1
4243	16	Bela Vista do Paraíso	bela-vista-do-paraiso	2013-09-03 15:30:12.389704	1
4244	16	Bituruna	bituruna	2013-09-03 15:30:12.389704	1
4245	16	Boa Esperança	boa-esperanca	2013-09-03 15:30:12.389704	1
4246	16	Boa Esperança do Iguaçu	boa-esperanca-do-iguacu	2013-09-03 15:30:12.389704	1
4247	16	Boa Ventura de São Roque	boa-ventura-de-sao-roque	2013-09-03 15:30:12.389704	1
4248	16	Boa Vista da Aparecida	boa-vista-da-aparecida	2013-09-03 15:30:12.389704	1
4249	16	Bocaiúva do Sul	bocaiuva-do-sul	2013-09-03 15:30:12.389704	1
4250	16	Bom Jesus do Sul	bom-jesus-do-sul	2013-09-03 15:30:12.389704	1
4251	16	Bom Sucesso	bom-sucesso	2013-09-03 15:30:12.389704	1
4252	16	Bom Sucesso do Sul	bom-sucesso-do-sul	2013-09-03 15:30:12.389704	1
4253	16	Borrazópolis	borrazopolis	2013-09-03 15:30:12.389704	1
4254	16	Braganey	braganey	2013-09-03 15:30:12.389704	1
4255	16	Brasilândia do Sul	brasilandia-do-sul	2013-09-03 15:30:12.389704	1
4256	16	Cafeara	cafeara	2013-09-03 15:30:12.389704	1
4257	16	Cafelândia	cafelandia	2013-09-03 15:30:12.389704	1
4258	16	Cafezal do Sul	cafezal-do-sul	2013-09-03 15:30:12.389704	1
4259	16	Califórnia	california	2013-09-03 15:30:12.389704	1
4260	16	Cambará	cambara	2013-09-03 15:30:12.389704	1
4261	16	Cambé	cambe	2013-09-03 15:30:12.389704	1
4262	16	Cambira	cambira	2013-09-03 15:30:12.389704	1
4263	16	Campina da Lagoa	campina-da-lagoa	2013-09-03 15:30:12.389704	1
4264	16	Campina do Simão	campina-do-simao	2013-09-03 15:30:12.389704	1
4265	16	Campina Grande do Sul	campina-grande-do-sul	2013-09-03 15:30:12.389704	1
4266	16	Campo Bonito	campo-bonito	2013-09-03 15:30:12.389704	1
4267	16	Campo do Tenente	campo-do-tenente	2013-09-03 15:30:12.389704	1
4268	16	Campo Largo	campo-largo	2013-09-03 15:30:12.389704	1
4269	16	Campo Magro	campo-magro	2013-09-03 15:30:12.389704	1
4270	16	Campo Mourão	campo-mourao	2013-09-03 15:30:12.389704	1
4271	16	Cândido de Abreu	candido-de-abreu	2013-09-03 15:30:12.389704	1
4272	16	Candói	candoi	2013-09-03 15:30:12.389704	1
4273	16	Cantagalo	cantagalo	2013-09-03 15:30:12.389704	1
4274	16	Capanema	capanema	2013-09-03 15:30:12.389704	1
4275	16	Capitão Leônidas Marques	capitao-leonidas-marques	2013-09-03 15:30:12.389704	1
4276	16	Carambeí	carambei	2013-09-03 15:30:12.389704	1
4277	16	Carlópolis	carlopolis	2013-09-03 15:30:12.389704	1
4278	16	Cascavel	cascavel	2013-09-03 15:30:12.389704	1
4279	16	Castro	castro	2013-09-03 15:30:12.389704	1
4280	16	Catanduvas	catanduvas	2013-09-03 15:30:12.389704	1
4281	16	Centenário do Sul	centenario-do-sul	2013-09-03 15:30:12.389704	1
4282	16	Cerro Azul	cerro-azul	2013-09-03 15:30:12.389704	1
4283	16	Céu Azul	ceu-azul	2013-09-03 15:30:12.389704	1
4284	16	Chopinzinho	chopinzinho	2013-09-03 15:30:12.389704	1
4285	16	Cianorte	cianorte	2013-09-03 15:30:12.389704	1
4286	16	Cidade Gaúcha	cidade-gaucha	2013-09-03 15:30:12.389704	1
4287	16	Clevelândia	clevelandia	2013-09-03 15:30:12.389704	1
4288	16	Colombo	colombo	2013-09-03 15:30:12.389704	1
4289	16	Colorado	colorado	2013-09-03 15:30:12.389704	1
4290	16	Congonhinhas	congonhinhas	2013-09-03 15:30:12.389704	1
4291	16	Conselheiro Mairinck	conselheiro-mairinck	2013-09-03 15:30:12.389704	1
4292	16	Contenda	contenda	2013-09-03 15:30:12.389704	1
4293	16	Corbélia	corbelia	2013-09-03 15:30:12.389704	1
4294	16	Cornélio Procópio	cornelio-procopio	2013-09-03 15:30:12.389704	1
4295	16	Coronel Domingos Soares	coronel-domingos-soares	2013-09-03 15:30:12.389704	1
4296	16	Coronel Vivida	coronel-vivida	2013-09-03 15:30:12.389704	1
4297	16	Corumbataí do Sul	corumbatai-do-sul	2013-09-03 15:30:12.389704	1
4298	16	Cruz Machado	cruz-machado	2013-09-03 15:30:12.389704	1
4299	16	Cruzeiro do Iguaçu	cruzeiro-do-iguacu	2013-09-03 15:30:12.389704	1
4300	16	Cruzeiro do Oeste	cruzeiro-do-oeste	2013-09-03 15:30:12.389704	1
4301	16	Cruzeiro do Sul	cruzeiro-do-sul	2013-09-03 15:30:12.389704	1
4302	16	Cruzmaltina	cruzmaltina	2013-09-03 15:30:12.389704	1
4303	16	Curitiba	curitiba	2013-09-03 15:30:12.389704	1
4304	16	Curiúva	curiuva	2013-09-03 15:30:12.389704	1
4305	16	Diamante d`Oeste	diamante-d-oeste	2013-09-03 15:30:12.389704	1
4306	16	Diamante do Norte	diamante-do-norte	2013-09-03 15:30:12.389704	1
4307	16	Diamante do Sul	diamante-do-sul	2013-09-03 15:30:12.389704	1
4308	16	Dois Vizinhos	dois-vizinhos	2013-09-03 15:30:12.389704	1
4309	16	Douradina	douradina	2013-09-03 15:30:12.389704	1
4310	16	Doutor Camargo	doutor-camargo	2013-09-03 15:30:12.389704	1
4311	16	Doutor Ulysses	doutor-ulysses	2013-09-03 15:30:12.389704	1
4312	16	Enéas Marques	eneas-marques	2013-09-03 15:30:12.389704	1
4313	16	Engenheiro Beltrão	engenheiro-beltrao	2013-09-03 15:30:12.389704	1
4314	16	Entre Rios do Oeste	entre-rios-do-oeste	2013-09-03 15:30:12.389704	1
4315	16	Esperança Nova	esperanca-nova	2013-09-03 15:30:12.389704	1
4316	16	Espigão Alto do Iguaçu	espigao-alto-do-iguacu	2013-09-03 15:30:12.389704	1
4317	16	Farol	farol	2013-09-03 15:30:12.389704	1
4318	16	Faxinal	faxinal	2013-09-03 15:30:12.389704	1
4319	16	Fazenda Rio Grande	fazenda-rio-grande	2013-09-03 15:30:12.389704	1
4320	16	Fênix	fenix	2013-09-03 15:30:12.389704	1
4321	16	Fernandes Pinheiro	fernandes-pinheiro	2013-09-03 15:30:12.389704	1
4322	16	Figueira	figueira	2013-09-03 15:30:12.389704	1
4323	16	Flor da Serra do Sul	flor-da-serra-do-sul	2013-09-03 15:30:12.389704	1
4324	16	Floraí	florai	2013-09-03 15:30:12.389704	1
4325	16	Floresta	floresta	2013-09-03 15:30:12.389704	1
4326	16	Florestópolis	florestopolis	2013-09-03 15:30:12.389704	1
4327	16	Flórida	florida	2013-09-03 15:30:12.389704	1
4328	16	Formosa do Oeste	formosa-do-oeste	2013-09-03 15:30:12.389704	1
4329	16	Foz do Iguaçu	foz-do-iguacu	2013-09-03 15:30:12.389704	1
4330	16	Foz do Jordão	foz-do-jordao	2013-09-03 15:30:12.389704	1
4331	16	Francisco Alves	francisco-alves	2013-09-03 15:30:12.389704	1
4332	16	Francisco Beltrão	francisco-beltrao	2013-09-03 15:30:12.389704	1
4333	16	General Carneiro	general-carneiro	2013-09-03 15:30:12.389704	1
4334	16	Godoy Moreira	godoy-moreira	2013-09-03 15:30:12.389704	1
4335	16	Goioerê	goioere	2013-09-03 15:30:12.389704	1
4337	16	Grandes Rios	grandes-rios	2013-09-03 15:30:12.389704	1
4338	16	Guaíra	guaira	2013-09-03 15:30:12.389704	1
4339	16	Guairaçá	guairaca	2013-09-03 15:30:12.389704	1
4340	16	Guamiranga	guamiranga	2013-09-03 15:30:12.389704	1
4341	16	Guapirama	guapirama	2013-09-03 15:30:12.389704	1
4342	16	Guaporema	guaporema	2013-09-03 15:30:12.389704	1
4343	16	Guaraci	guaraci	2013-09-03 15:30:12.389704	1
4344	16	Guaraniaçu	guaraniacu	2013-09-03 15:30:12.389704	1
4345	16	Guarapuava	guarapuava	2013-09-03 15:30:12.389704	1
4346	16	Guaraqueçaba	guaraquecaba	2013-09-03 15:30:12.389704	1
4347	16	Guaratuba	guaratuba	2013-09-03 15:30:12.389704	1
4348	16	Honório Serpa	honorio-serpa	2013-09-03 15:30:12.389704	1
4349	16	Ibaiti	ibaiti	2013-09-03 15:30:12.389704	1
4350	16	Ibema	ibema	2013-09-03 15:30:12.389704	1
4351	16	Ibiporã	ibipora	2013-09-03 15:30:12.389704	1
4352	16	Icaraíma	icaraima	2013-09-03 15:30:12.389704	1
4353	16	Iguaraçu	iguaracu	2013-09-03 15:30:12.389704	1
4354	16	Iguatu	iguatu	2013-09-03 15:30:12.389704	1
4355	16	Imbaú	imbau	2013-09-03 15:30:12.389704	1
4356	16	Imbituva	imbituva	2013-09-03 15:30:12.389704	1
4357	16	Inácio Martins	inacio-martins	2013-09-03 15:30:12.389704	1
4358	16	Inajá	inaja	2013-09-03 15:30:12.389704	1
4359	16	Indianópolis	indianopolis	2013-09-03 15:30:12.389704	1
4360	16	Ipiranga	ipiranga	2013-09-03 15:30:12.389704	1
4361	16	Iporã	ipora	2013-09-03 15:30:12.389704	1
4362	16	Iracema do Oeste	iracema-do-oeste	2013-09-03 15:30:12.389704	1
4363	16	Irati	irati	2013-09-03 15:30:12.389704	1
4364	16	Iretama	iretama	2013-09-03 15:30:12.389704	1
4365	16	Itaguajé	itaguaje	2013-09-03 15:30:12.389704	1
4366	16	Itaipulândia	itaipulandia	2013-09-03 15:30:12.389704	1
4367	16	Itambaracá	itambaraca	2013-09-03 15:30:12.389704	1
4368	16	Itambé	itambe	2013-09-03 15:30:12.389704	1
4369	16	Itapejara d`Oeste	itapejara-d-oeste	2013-09-03 15:30:12.389704	1
4370	16	Itaperuçu	itaperucu	2013-09-03 15:30:12.389704	1
4371	16	Itaúna do Sul	itauna-do-sul	2013-09-03 15:30:12.389704	1
4372	16	Ivaí	ivai	2013-09-03 15:30:12.389704	1
4373	16	Ivaiporã	ivaipora	2013-09-03 15:30:12.389704	1
4374	16	Ivaté	ivate	2013-09-03 15:30:12.389704	1
4375	16	Ivatuba	ivatuba	2013-09-03 15:30:12.389704	1
4376	16	Jaboti	jaboti	2013-09-03 15:30:12.389704	1
4377	16	Jacarezinho	jacarezinho	2013-09-03 15:30:12.389704	1
4378	16	Jaguapitã	jaguapita	2013-09-03 15:30:12.389704	1
4379	16	Jaguariaíva	jaguariaiva	2013-09-03 15:30:12.389704	1
4380	16	Jandaia do Sul	jandaia-do-sul	2013-09-03 15:30:12.389704	1
4381	16	Janiópolis	janiopolis	2013-09-03 15:30:12.389704	1
4382	16	Japira	japira	2013-09-03 15:30:12.389704	1
4383	16	Japurá	japura	2013-09-03 15:30:12.389704	1
4384	16	Jardim Alegre	jardim-alegre	2013-09-03 15:30:12.389704	1
4385	16	Jardim Olinda	jardim-olinda	2013-09-03 15:30:12.389704	1
4386	16	Jataizinho	jataizinho	2013-09-03 15:30:12.389704	1
4387	16	Jesuítas	jesuitas	2013-09-03 15:30:12.389704	1
4388	16	Joaquim Távora	joaquim-tavora	2013-09-03 15:30:12.389704	1
4389	16	Jundiaí do Sul	jundiai-do-sul	2013-09-03 15:30:12.389704	1
4390	16	Juranda	juranda	2013-09-03 15:30:12.389704	1
4391	16	Jussara	jussara	2013-09-03 15:30:12.389704	1
4392	16	Kaloré	kalore	2013-09-03 15:30:12.389704	1
4393	16	Lapa	lapa	2013-09-03 15:30:12.389704	1
4394	16	Laranjal	laranjal	2013-09-03 15:30:12.389704	1
4395	16	Laranjeiras do Sul	laranjeiras-do-sul	2013-09-03 15:30:12.389704	1
4396	16	Leópolis	leopolis	2013-09-03 15:30:12.389704	1
4397	16	Lidianópolis	lidianopolis	2013-09-03 15:30:12.389704	1
4398	16	Lindoeste	lindoeste	2013-09-03 15:30:12.389704	1
4399	16	Loanda	loanda	2013-09-03 15:30:12.389704	1
4400	16	Lobato	lobato	2013-09-03 15:30:12.389704	1
4401	16	Londrina	londrina	2013-09-03 15:30:12.389704	1
4402	16	Luiziana	luiziana	2013-09-03 15:30:12.389704	1
4403	16	Lunardelli	lunardelli	2013-09-03 15:30:12.389704	1
4404	16	Lupionópolis	lupionopolis	2013-09-03 15:30:12.389704	1
4405	16	Mallet	mallet	2013-09-03 15:30:12.389704	1
4406	16	Mamborê	mambore	2013-09-03 15:30:12.389704	1
4407	16	Mandaguaçu	mandaguacu	2013-09-03 15:30:12.389704	1
4408	16	Mandaguari	mandaguari	2013-09-03 15:30:12.389704	1
4409	16	Mandirituba	mandirituba	2013-09-03 15:30:12.389704	1
4410	16	Manfrinópolis	manfrinopolis	2013-09-03 15:30:12.389704	1
4411	16	Mangueirinha	mangueirinha	2013-09-03 15:30:12.389704	1
4412	16	Manoel Ribas	manoel-ribas	2013-09-03 15:30:12.389704	1
4413	16	Marechal Cândido Rondon	marechal-candido-rondon	2013-09-03 15:30:12.389704	1
4414	16	Maria Helena	maria-helena	2013-09-03 15:30:12.389704	1
4415	16	Marialva	marialva	2013-09-03 15:30:12.389704	1
4416	16	Marilândia do Sul	marilandia-do-sul	2013-09-03 15:30:12.389704	1
4417	16	Marilena	marilena	2013-09-03 15:30:12.389704	1
4418	16	Mariluz	mariluz	2013-09-03 15:30:12.389704	1
4419	16	Maringá	maringa	2013-09-03 15:30:12.389704	1
4420	16	Mariópolis	mariopolis	2013-09-03 15:30:12.389704	1
4421	16	Maripá	maripa	2013-09-03 15:30:12.389704	1
4422	16	Marmeleiro	marmeleiro	2013-09-03 15:30:12.389704	1
4423	16	Marquinho	marquinho	2013-09-03 15:30:12.389704	1
4424	16	Marumbi	marumbi	2013-09-03 15:30:12.389704	1
4425	16	Matelândia	matelandia	2013-09-03 15:30:12.389704	1
4426	16	Matinhos	matinhos	2013-09-03 15:30:12.389704	1
4427	16	Mato Rico	mato-rico	2013-09-03 15:30:12.389704	1
4428	16	Mauá da Serra	maua-da-serra	2013-09-03 15:30:12.389704	1
4429	16	Medianeira	medianeira	2013-09-03 15:30:12.389704	1
4430	16	Mercedes	mercedes	2013-09-03 15:30:12.389704	1
4431	16	Mirador	mirador	2013-09-03 15:30:12.389704	1
4432	16	Miraselva	miraselva	2013-09-03 15:30:12.389704	1
4433	16	Missal	missal	2013-09-03 15:30:12.389704	1
4434	16	Moreira Sales	moreira-sales	2013-09-03 15:30:12.389704	1
4435	16	Morretes	morretes	2013-09-03 15:30:12.389704	1
4436	16	Munhoz de Melo	munhoz-de-melo	2013-09-03 15:30:12.389704	1
4437	16	Nossa Senhora das Graças	nossa-senhora-das-gracas	2013-09-03 15:30:12.389704	1
4438	16	Nova Aliança do Ivaí	nova-alianca-do-ivai	2013-09-03 15:30:12.389704	1
4439	16	Nova América da Colina	nova-america-da-colina	2013-09-03 15:30:12.389704	1
4440	16	Nova Aurora	nova-aurora	2013-09-03 15:30:12.389704	1
4441	16	Nova Cantu	nova-cantu	2013-09-03 15:30:12.389704	1
4749	17	Buíque	buique	2013-09-03 15:30:12.389704	1
4442	16	Nova Esperança	nova-esperanca	2013-09-03 15:30:12.389704	1
4443	16	Nova Esperança do Sudoeste	nova-esperanca-do-sudoeste	2013-09-03 15:30:12.389704	1
4444	16	Nova Fátima	nova-fatima	2013-09-03 15:30:12.389704	1
4445	16	Nova Laranjeiras	nova-laranjeiras	2013-09-03 15:30:12.389704	1
4446	16	Nova Londrina	nova-londrina	2013-09-03 15:30:12.389704	1
4447	16	Nova Olímpia	nova-olimpia	2013-09-03 15:30:12.389704	1
4448	16	Nova Prata do Iguaçu	nova-prata-do-iguacu	2013-09-03 15:30:12.389704	1
4449	16	Nova Santa Bárbara	nova-santa-barbara	2013-09-03 15:30:12.389704	1
4450	16	Nova Santa Rosa	nova-santa-rosa	2013-09-03 15:30:12.389704	1
4451	16	Nova Tebas	nova-tebas	2013-09-03 15:30:12.389704	1
4452	16	Novo Itacolomi	novo-itacolomi	2013-09-03 15:30:12.389704	1
4453	16	Ortigueira	ortigueira	2013-09-03 15:30:12.389704	1
4454	16	Ourizona	ourizona	2013-09-03 15:30:12.389704	1
4455	16	Ouro Verde do Oeste	ouro-verde-do-oeste	2013-09-03 15:30:12.389704	1
4456	16	Paiçandu	paicandu	2013-09-03 15:30:12.389704	1
4457	16	Palmas	palmas	2013-09-03 15:30:12.389704	1
4458	16	Palmeira	palmeira	2013-09-03 15:30:12.389704	1
4459	16	Palmital	palmital	2013-09-03 15:30:12.389704	1
4460	16	Palotina	palotina	2013-09-03 15:30:12.389704	1
4461	16	Paraíso do Norte	paraiso-do-norte	2013-09-03 15:30:12.389704	1
4462	16	Paranacity	paranacity	2013-09-03 15:30:12.389704	1
4463	16	Paranaguá	paranagua	2013-09-03 15:30:12.389704	1
4464	16	Paranapoema	paranapoema	2013-09-03 15:30:12.389704	1
4465	16	Paranavaí	paranavai	2013-09-03 15:30:12.389704	1
4466	16	Pato Bragado	pato-bragado	2013-09-03 15:30:12.389704	1
4467	16	Pato Branco	pato-branco	2013-09-03 15:30:12.389704	1
4468	16	Paula Freitas	paula-freitas	2013-09-03 15:30:12.389704	1
4469	16	Paulo Frontin	paulo-frontin	2013-09-03 15:30:12.389704	1
4470	16	Peabiru	peabiru	2013-09-03 15:30:12.389704	1
4471	16	Perobal	perobal	2013-09-03 15:30:12.389704	1
4472	16	Pérola	perola	2013-09-03 15:30:12.389704	1
4473	16	Pérola d`Oeste	perola-d-oeste	2013-09-03 15:30:12.389704	1
4474	16	Piên	pien	2013-09-03 15:30:12.389704	1
4475	16	Pinhais	pinhais	2013-09-03 15:30:12.389704	1
4476	16	Pinhal de São Bento	pinhal-de-sao-bento	2013-09-03 15:30:12.389704	1
4477	16	Pinhalão	pinhalao	2013-09-03 15:30:12.389704	1
4478	16	Pinhão	pinhao	2013-09-03 15:30:12.389704	1
4479	16	Piraí do Sul	pirai-do-sul	2013-09-03 15:30:12.389704	1
4480	16	Piraquara	piraquara	2013-09-03 15:30:12.389704	1
4481	16	Pitanga	pitanga	2013-09-03 15:30:12.389704	1
4482	16	Pitangueiras	pitangueiras	2013-09-03 15:30:12.389704	1
4483	16	Planaltina do Paraná	planaltina-do-parana	2013-09-03 15:30:12.389704	1
4484	16	Planalto	planalto	2013-09-03 15:30:12.389704	1
4485	16	Ponta Grossa	ponta-grossa	2013-09-03 15:30:12.389704	1
4486	16	Pontal do Paraná	pontal-do-parana	2013-09-03 15:30:12.389704	1
4487	16	Porecatu	porecatu	2013-09-03 15:30:12.389704	1
4488	16	Porto Amazonas	porto-amazonas	2013-09-03 15:30:12.389704	1
4489	16	Porto Barreiro	porto-barreiro	2013-09-03 15:30:12.389704	1
4490	16	Porto Rico	porto-rico	2013-09-03 15:30:12.389704	1
4491	16	Porto Vitória	porto-vitoria	2013-09-03 15:30:12.389704	1
4492	16	Prado Ferreira	prado-ferreira	2013-09-03 15:30:12.389704	1
4493	16	Pranchita	pranchita	2013-09-03 15:30:12.389704	1
4494	16	Presidente Castelo Branco	presidente-castelo-branco	2013-09-03 15:30:12.389704	1
4495	16	Primeiro de Maio	primeiro-de-maio	2013-09-03 15:30:12.389704	1
4496	16	Prudentópolis	prudentopolis	2013-09-03 15:30:12.389704	1
4497	16	Quarto Centenário	quarto-centenario	2013-09-03 15:30:12.389704	1
4498	16	Quatiguá	quatigua	2013-09-03 15:30:12.389704	1
4499	16	Quatro Barras	quatro-barras	2013-09-03 15:30:12.389704	1
4500	16	Quatro Pontes	quatro-pontes	2013-09-03 15:30:12.389704	1
4501	16	Quedas do Iguaçu	quedas-do-iguacu	2013-09-03 15:30:12.389704	1
4502	16	Querência do Norte	querencia-do-norte	2013-09-03 15:30:12.389704	1
4503	16	Quinta do Sol	quinta-do-sol	2013-09-03 15:30:12.389704	1
4504	16	Quitandinha	quitandinha	2013-09-03 15:30:12.389704	1
4505	16	Ramilândia	ramilandia	2013-09-03 15:30:12.389704	1
4506	16	Rancho Alegre	rancho-alegre	2013-09-03 15:30:12.389704	1
4507	16	Rancho Alegre d`Oeste	rancho-alegre-d-oeste	2013-09-03 15:30:12.389704	1
4508	16	Realeza	realeza	2013-09-03 15:30:12.389704	1
4509	16	Rebouças	reboucas	2013-09-03 15:30:12.389704	1
4510	16	Renascença	renascenca	2013-09-03 15:30:12.389704	1
4511	16	Reserva	reserva	2013-09-03 15:30:12.389704	1
4512	16	Reserva do Iguaçu	reserva-do-iguacu	2013-09-03 15:30:12.389704	1
4513	16	Ribeirão Claro	ribeirao-claro	2013-09-03 15:30:12.389704	1
4514	16	Ribeirão do Pinhal	ribeirao-do-pinhal	2013-09-03 15:30:12.389704	1
4515	16	Rio Azul	rio-azul	2013-09-03 15:30:12.389704	1
4516	16	Rio Bom	rio-bom	2013-09-03 15:30:12.389704	1
4517	16	Rio Bonito do Iguaçu	rio-bonito-do-iguacu	2013-09-03 15:30:12.389704	1
4518	16	Rio Branco do Ivaí	rio-branco-do-ivai	2013-09-03 15:30:12.389704	1
4519	16	Rio Branco do Sul	rio-branco-do-sul	2013-09-03 15:30:12.389704	1
4520	16	Rio Negro	rio-negro	2013-09-03 15:30:12.389704	1
4521	16	Rolândia	rolandia	2013-09-03 15:30:12.389704	1
4522	16	Roncador	roncador	2013-09-03 15:30:12.389704	1
4523	16	Rondon	rondon	2013-09-03 15:30:12.389704	1
4524	16	Rosário do Ivaí	rosario-do-ivai	2013-09-03 15:30:12.389704	1
4525	16	Sabáudia	sabaudia	2013-09-03 15:30:12.389704	1
4526	16	Salgado Filho	salgado-filho	2013-09-03 15:30:12.389704	1
4527	16	Salto do Itararé	salto-do-itarare	2013-09-03 15:30:12.389704	1
4528	16	Salto do Lontra	salto-do-lontra	2013-09-03 15:30:12.389704	1
4529	16	Santa Amélia	santa-amelia	2013-09-03 15:30:12.389704	1
4530	16	Santa Cecília do Pavão	santa-cecilia-do-pavao	2013-09-03 15:30:12.389704	1
4531	16	Santa Cruz de Monte Castelo	santa-cruz-de-monte-castelo	2013-09-03 15:30:12.389704	1
4532	16	Santa Fé	santa-fe	2013-09-03 15:30:12.389704	1
4533	16	Santa Helena	santa-helena	2013-09-03 15:30:12.389704	1
4534	16	Santa Inês	santa-ines	2013-09-03 15:30:12.389704	1
4535	16	Santa Isabel do Ivaí	santa-isabel-do-ivai	2013-09-03 15:30:12.389704	1
4536	16	Santa Izabel do Oeste	santa-izabel-do-oeste	2013-09-03 15:30:12.389704	1
4537	16	Santa Lúcia	santa-lucia	2013-09-03 15:30:12.389704	1
4538	16	Santa Maria do Oeste	santa-maria-do-oeste	2013-09-03 15:30:12.389704	1
4539	16	Santa Mariana	santa-mariana	2013-09-03 15:30:12.389704	1
4540	16	Santa Mônica	santa-monica	2013-09-03 15:30:12.389704	1
4541	16	Santa Tereza do Oeste	santa-tereza-do-oeste	2013-09-03 15:30:12.389704	1
4542	16	Santa Terezinha de Itaipu	santa-terezinha-de-itaipu	2013-09-03 15:30:12.389704	1
4543	16	Santana do Itararé	santana-do-itarare	2013-09-03 15:30:12.389704	1
4544	16	Santo Antônio da Platina	santo-antonio-da-platina	2013-09-03 15:30:12.389704	1
4545	16	Santo Antônio do Caiuá	santo-antonio-do-caiua	2013-09-03 15:30:12.389704	1
4546	16	Santo Antônio do Paraíso	santo-antonio-do-paraiso	2013-09-03 15:30:12.389704	1
4547	16	Santo Antônio do Sudoeste	santo-antonio-do-sudoeste	2013-09-03 15:30:12.389704	1
4548	16	Santo Inácio	santo-inacio	2013-09-03 15:30:12.389704	1
4549	16	São Carlos do Ivaí	sao-carlos-do-ivai	2013-09-03 15:30:12.389704	1
4550	16	São Jerônimo da Serra	sao-jeronimo-da-serra	2013-09-03 15:30:12.389704	1
4551	16	São João	sao-joao	2013-09-03 15:30:12.389704	1
4552	16	São João do Caiuá	sao-joao-do-caiua	2013-09-03 15:30:12.389704	1
4553	16	São João do Ivaí	sao-joao-do-ivai	2013-09-03 15:30:12.389704	1
4554	16	São João do Triunfo	sao-joao-do-triunfo	2013-09-03 15:30:12.389704	1
4555	16	São Jorge d`Oeste	sao-jorge-d-oeste	2013-09-03 15:30:12.389704	1
4556	16	São Jorge do Ivaí	sao-jorge-do-ivai	2013-09-03 15:30:12.389704	1
4557	16	São Jorge do Patrocínio	sao-jorge-do-patrocinio	2013-09-03 15:30:12.389704	1
4558	16	São José da Boa Vista	sao-jose-da-boa-vista	2013-09-03 15:30:12.389704	1
4559	16	São José\ndas Palmeiras	sao-jose-das-palmeiras	2013-09-03 15:30:12.389704	1
4560	16	São José dos Pinhais	sao-jose-dos-pinhais	2013-09-03 15:30:12.389704	1
4561	16	São Manoel do Paraná	sao-manoel-do-parana	2013-09-03 15:30:12.389704	1
4562	16	São Mateus do Sul	sao-mateus-do-sul	2013-09-03 15:30:12.389704	1
4563	16	São Miguel do Iguaçu	sao-miguel-do-iguacu	2013-09-03 15:30:12.389704	1
4564	16	São Pedro do Iguaçu	sao-pedro-do-iguacu	2013-09-03 15:30:12.389704	1
4565	16	São Pedro do Ivaí	sao-pedro-do-ivai	2013-09-03 15:30:12.389704	1
4566	16	São Pedro do Paraná	sao-pedro-do-parana	2013-09-03 15:30:12.389704	1
4567	16	São Sebastião da Amoreira	sao-sebastiao-da-amoreira	2013-09-03 15:30:12.389704	1
4568	16	São Tomé	sao-tome	2013-09-03 15:30:12.389704	1
4569	16	Sapopema	sapopema	2013-09-03 15:30:12.389704	1
4570	16	Sarandi	sarandi	2013-09-03 15:30:12.389704	1
4571	16	Saudade do Iguaçu	saudade-do-iguacu	2013-09-03 15:30:12.389704	1
4572	16	Sengés	senges	2013-09-03 15:30:12.389704	1
4573	16	Serranópolis do Iguaçu	serranopolis-do-iguacu	2013-09-03 15:30:12.389704	1
4574	16	Sertaneja	sertaneja	2013-09-03 15:30:12.389704	1
4575	16	Sertanópolis	sertanopolis	2013-09-03 15:30:12.389704	1
4576	16	Siqueira Campos	siqueira-campos	2013-09-03 15:30:12.389704	1
4577	16	Sulina	sulina	2013-09-03 15:30:12.389704	1
4578	16	Tamarana	tamarana	2013-09-03 15:30:12.389704	1
4579	16	Tamboara	tamboara	2013-09-03 15:30:12.389704	1
4580	16	Tapejara	tapejara	2013-09-03 15:30:12.389704	1
4581	16	Tapira	tapira	2013-09-03 15:30:12.389704	1
4582	16	Teixeira Soares	teixeira-soares	2013-09-03 15:30:12.389704	1
4583	16	Telêmaco Borba	telemaco-borba	2013-09-03 15:30:12.389704	1
4584	16	Terra Boa	terra-boa	2013-09-03 15:30:12.389704	1
4585	16	Terra Rica	terra-rica	2013-09-03 15:30:12.389704	1
4586	16	Terra Roxa	terra-roxa	2013-09-03 15:30:12.389704	1
4587	16	Tibagi	tibagi	2013-09-03 15:30:12.389704	1
4588	16	Tijucas do Sul	tijucas-do-sul	2013-09-03 15:30:12.389704	1
4589	16	Toledo	toledo	2013-09-03 15:30:12.389704	1
4590	16	Tomazina	tomazina	2013-09-03 15:30:12.389704	1
4591	16	Três Barras do Paraná	tres-barras-do-parana	2013-09-03 15:30:12.389704	1
4592	16	Tunas do Paraná	tunas-do-parana	2013-09-03 15:30:12.389704	1
4593	16	Tuneiras do Oeste	tuneiras-do-oeste	2013-09-03 15:30:12.389704	1
4594	16	Tupãssi	tupassi	2013-09-03 15:30:12.389704	1
4595	16	Turvo	turvo	2013-09-03 15:30:12.389704	1
4596	16	Ubiratã	ubirata	2013-09-03 15:30:12.389704	1
4597	16	Umuarama	umuarama	2013-09-03 15:30:12.389704	1
4598	16	União da Vitória	uniao-da-vitoria	2013-09-03 15:30:12.389704	1
4599	16	Uniflor	uniflor	2013-09-03 15:30:12.389704	1
4600	16	Uraí	urai	2013-09-03 15:30:12.389704	1
4601	16	Ventania	ventania	2013-09-03 15:30:12.389704	1
4602	16	Vera Cruz do Oeste	vera-cruz-do-oeste	2013-09-03 15:30:12.389704	1
4603	16	Verê	vere	2013-09-03 15:30:12.389704	1
4604	16	Virmond	virmond	2013-09-03 15:30:12.389704	1
4605	16	Vitorino	vitorino	2013-09-03 15:30:12.389704	1
4606	16	Wenceslau Braz	wenceslau-braz	2013-09-03 15:30:12.389704	1
4607	16	Xambrê	xambre	2013-09-03 15:30:12.389704	1
4720	17	Abreu e Lima	abreu-e-lima	2013-09-03 15:30:12.389704	1
4721	17	Afogados da Ingazeira	afogados-da-ingazeira	2013-09-03 15:30:12.389704	1
4722	17	Afrânio	afranio	2013-09-03 15:30:12.389704	1
4723	17	Agrestina	agrestina	2013-09-03 15:30:12.389704	1
4724	17	Água Preta	agua-preta	2013-09-03 15:30:12.389704	1
4725	17	Águas Belas	aguas-belas	2013-09-03 15:30:12.389704	1
4726	17	Alagoinha	alagoinha	2013-09-03 15:30:12.389704	1
4727	17	Aliança	alianca	2013-09-03 15:30:12.389704	1
4728	17	Altinho	altinho	2013-09-03 15:30:12.389704	1
4729	17	Amaraji	amaraji	2013-09-03 15:30:12.389704	1
4730	17	Angelim	angelim	2013-09-03 15:30:12.389704	1
4731	17	Araçoiaba	aracoiaba	2013-09-03 15:30:12.389704	1
4732	17	Araripina	araripina	2013-09-03 15:30:12.389704	1
4733	17	Arcoverde	arcoverde	2013-09-03 15:30:12.389704	1
4734	17	Barra de Guabiraba	barra-de-guabiraba	2013-09-03 15:30:12.389704	1
4735	17	Barreiros	barreiros	2013-09-03 15:30:12.389704	1
4736	17	Belém de Maria	belem-de-maria	2013-09-03 15:30:12.389704	1
4737	17	Belém de São Francisco	belem-de-sao-francisco	2013-09-03 15:30:12.389704	1
4738	17	Belo Jardim	belo-jardim	2013-09-03 15:30:12.389704	1
4739	17	Betânia	betania	2013-09-03 15:30:12.389704	1
4740	17	Bezerros	bezerros	2013-09-03 15:30:12.389704	1
4741	17	Bodocó	bodoco	2013-09-03 15:30:12.389704	1
4742	17	Bom Conselho	bom-conselho	2013-09-03 15:30:12.389704	1
4743	17	Bom Jardim	bom-jardim	2013-09-03 15:30:12.389704	1
4744	17	Bonito	bonito	2013-09-03 15:30:12.389704	1
4745	17	Brejão	brejao	2013-09-03 15:30:12.389704	1
4746	17	Brejinho	brejinho	2013-09-03 15:30:12.389704	1
4747	17	Brejo da Madre de Deus	brejo-da-madre-de-deus	2013-09-03 15:30:12.389704	1
4748	17	Buenos Aires	buenos-aires	2013-09-03 15:30:12.389704	1
4750	17	Cabo de Santo Agostinho	cabo-de-santo-agostinho	2013-09-03 15:30:12.389704	1
4751	17	Cabrobó	cabrobo	2013-09-03 15:30:12.389704	1
4752	17	Cachoeirinha	cachoeirinha	2013-09-03 15:30:12.389704	1
4753	17	Caetés	caetes	2013-09-03 15:30:12.389704	1
4754	17	Calçado	calcado	2013-09-03 15:30:12.389704	1
4756	17	Camaragibe	camaragibe	2013-09-03 15:30:12.389704	1
4757	17	Camocim de São Félix	camocim-de-sao-felix	2013-09-03 15:30:12.389704	1
4758	17	Camutanga	camutanga	2013-09-03 15:30:12.389704	1
4759	17	Canhotinho	canhotinho	2013-09-03 15:30:12.389704	1
4760	17	Capoeiras	capoeiras	2013-09-03 15:30:12.389704	1
4761	17	Carnaíba	carnaiba	2013-09-03 15:30:12.389704	1
4762	17	Carnaubeira da Penha	carnaubeira-da-penha	2013-09-03 15:30:12.389704	1
4763	17	Carpina	carpina	2013-09-03 15:30:12.389704	1
4764	17	Caruaru	caruaru	2013-09-03 15:30:12.389704	1
4765	17	Casinhas	casinhas	2013-09-03 15:30:12.389704	1
4766	17	Catende	catende	2013-09-03 15:30:12.389704	1
4767	17	Cedro	cedro	2013-09-03 15:30:12.389704	1
4768	17	Chã de Alegria	cha-de-alegria	2013-09-03 15:30:12.389704	1
4769	17	Chã Grande	cha-grande	2013-09-03 15:30:12.389704	1
4770	17	Condado	condado	2013-09-03 15:30:12.389704	1
4771	17	Correntes	correntes	2013-09-03 15:30:12.389704	1
4772	17	Cortês	cortes	2013-09-03 15:30:12.389704	1
4773	17	Cumaru	cumaru	2013-09-03 15:30:12.389704	1
4774	17	Cupira	cupira	2013-09-03 15:30:12.389704	1
4775	17	Custódia	custodia	2013-09-03 15:30:12.389704	1
4776	17	Dormentes	dormentes	2013-09-03 15:30:12.389704	1
4777	17	Escada	escada	2013-09-03 15:30:12.389704	1
4778	17	Exu	exu	2013-09-03 15:30:12.389704	1
4779	17	Feira Nova	feira-nova	2013-09-03 15:30:12.389704	1
4780	17	Fernando de Noronha	fernando-de-noronha	2013-09-03 15:30:12.389704	1
4781	17	Ferreiros	ferreiros	2013-09-03 15:30:12.389704	1
4782	17	Flores	flores	2013-09-03 15:30:12.389704	1
4783	17	Floresta	floresta	2013-09-03 15:30:12.389704	1
4784	17	Frei Miguelinho	frei-miguelinho	2013-09-03 15:30:12.389704	1
4785	17	Gameleira	gameleira	2013-09-03 15:30:12.389704	1
4786	17	Garanhuns	garanhuns	2013-09-03 15:30:12.389704	1
4787	17	Glória do Goitá	gloria-do-goita	2013-09-03 15:30:12.389704	1
4788	17	Goiana	goiana	2013-09-03 15:30:12.389704	1
4789	17	Granito	granito	2013-09-03 15:30:12.389704	1
4790	17	Gravatá	gravata	2013-09-03 15:30:12.389704	1
4791	17	Iati	iati	2013-09-03 15:30:12.389704	1
4792	17	Ibimirim	ibimirim	2013-09-03 15:30:12.389704	1
4793	17	Ibirajuba	ibirajuba	2013-09-03 15:30:12.389704	1
4794	17	Igarassu	igarassu	2013-09-03 15:30:12.389704	1
4795	17	Iguaraci	iguaraci	2013-09-03 15:30:12.389704	1
4796	17	Ilha de Itamaracá	ilha-de-itamaraca	2013-09-03 15:30:12.389704	1
4797	17	Inajá	inaja	2013-09-03 15:30:12.389704	1
4798	17	Ingazeira	ingazeira	2013-09-03 15:30:12.389704	1
4799	17	Ipojuca	ipojuca	2013-09-03 15:30:12.389704	1
4800	17	Ipubi	ipubi	2013-09-03 15:30:12.389704	1
4801	17	Itacuruba	itacuruba	2013-09-03 15:30:12.389704	1
4802	17	Itaíba	itaiba	2013-09-03 15:30:12.389704	1
4803	17	Itambé	itambe	2013-09-03 15:30:12.389704	1
4804	17	Itapetim	itapetim	2013-09-03 15:30:12.389704	1
4805	17	Itapissuma	itapissuma	2013-09-03 15:30:12.389704	1
4806	17	Itaquitinga	itaquitinga	2013-09-03 15:30:12.389704	1
4807	17	Jaboatão dos Guararapes	jaboatao-dos-guararapes	2013-09-03 15:30:12.389704	1
4808	17	Jaqueira	jaqueira	2013-09-03 15:30:12.389704	1
4809	17	Jataúba	jatauba	2013-09-03 15:30:12.389704	1
4810	17	Jatobá	jatoba	2013-09-03 15:30:12.389704	1
4811	17	João Alfredo	joao-alfredo	2013-09-03 15:30:12.389704	1
4812	17	Joaquim Nabuco	joaquim-nabuco	2013-09-03 15:30:12.389704	1
4813	17	Jucati	jucati	2013-09-03 15:30:12.389704	1
4814	17	Jupi	jupi	2013-09-03 15:30:12.389704	1
4815	17	Jurema	jurema	2013-09-03 15:30:12.389704	1
4816	17	Lagoa do Carro	lagoa-do-carro	2013-09-03 15:30:12.389704	1
4817	17	Lagoa do Itaenga	lagoa-do-itaenga	2013-09-03 15:30:12.389704	1
4818	17	Lagoa do Ouro	lagoa-do-ouro	2013-09-03 15:30:12.389704	1
4819	17	Lagoa dos Gatos	lagoa-dos-gatos	2013-09-03 15:30:12.389704	1
4820	17	Lagoa Grande	lagoa-grande	2013-09-03 15:30:12.389704	1
4821	17	Lajedo	lajedo	2013-09-03 15:30:12.389704	1
4822	17	Limoeiro	limoeiro	2013-09-03 15:30:12.389704	1
4823	17	Macaparana	macaparana	2013-09-03 15:30:12.389704	1
4824	17	Machados	machados	2013-09-03 15:30:12.389704	1
4825	17	Manari	manari	2013-09-03 15:30:12.389704	1
4826	17	Maraial	maraial	2013-09-03 15:30:12.389704	1
4827	17	Mirandiba	mirandiba	2013-09-03 15:30:12.389704	1
4828	17	Moreilândia	moreilandia	2013-09-03 15:30:12.389704	1
4829	17	Moreno	moreno	2013-09-03 15:30:12.389704	1
4830	17	Nazaré da Mata	nazare-da-mata	2013-09-03 15:30:12.389704	1
4831	17	Olinda	olinda	2013-09-03 15:30:12.389704	1
4832	17	Orobó	orobo	2013-09-03 15:30:12.389704	1
4833	17	Orocó	oroco	2013-09-03 15:30:12.389704	1
4834	17	Ouricuri	ouricuri	2013-09-03 15:30:12.389704	1
4835	17	Palmares	palmares	2013-09-03 15:30:12.389704	1
4836	17	Palmeirina	palmeirina	2013-09-03 15:30:12.389704	1
4837	17	Panelas	panelas	2013-09-03 15:30:12.389704	1
4838	17	Paranatama	paranatama	2013-09-03 15:30:12.389704	1
4839	17	Parnamirim	parnamirim	2013-09-03 15:30:12.389704	1
4840	17	Passira	passira	2013-09-03 15:30:12.389704	1
4841	17	Paudalho	paudalho	2013-09-03 15:30:12.389704	1
4842	17	Paulista	paulista	2013-09-03 15:30:12.389704	1
4843	17	Pedra	pedra	2013-09-03 15:30:12.389704	1
4844	17	Pesqueira	pesqueira	2013-09-03 15:30:12.389704	1
4845	17	Petrolândia	petrolandia	2013-09-03 15:30:12.389704	1
4846	17	Petrolina	petrolina	2013-09-03 15:30:12.389704	1
4847	17	Poção	pocao	2013-09-03 15:30:12.389704	1
4848	17	Pombos	pombos	2013-09-03 15:30:12.389704	1
4849	17	Primavera	primavera	2013-09-03 15:30:12.389704	1
4850	17	Quipapá	quipapa	2013-09-03 15:30:12.389704	1
4851	17	Quixaba	quixaba	2013-09-03 15:30:12.389704	1
4852	17	Recife	recife	2013-09-03 15:30:12.389704	1
4853	17	Riacho das Almas	riacho-das-almas	2013-09-03 15:30:12.389704	1
4854	17	Ribeirão	ribeirao	2013-09-03 15:30:12.389704	1
4855	17	Rio Formoso	rio-formoso	2013-09-03 15:30:12.389704	1
4856	17	Sairé	saire	2013-09-03 15:30:12.389704	1
4857	17	Salgadinho	salgadinho	2013-09-03 15:30:12.389704	1
4858	17	Salgueiro	salgueiro	2013-09-03 15:30:12.389704	1
4859	17	Saloá	saloa	2013-09-03 15:30:12.389704	1
4860	17	Sanharó	sanharo	2013-09-03 15:30:12.389704	1
4861	17	Santa Cruz	santa-cruz	2013-09-03 15:30:12.389704	1
4862	17	Santa Cruz da Baixa Verde	santa-cruz-da-baixa-verde	2013-09-03 15:30:12.389704	1
4863	17	Santa Cruz do Capibaribe	santa-cruz-do-capibaribe	2013-09-03 15:30:12.389704	1
4864	17	Santa Filomena	santa-filomena	2013-09-03 15:30:12.389704	1
4865	17	Santa Maria da Boa Vista	santa-maria-da-boa-vista	2013-09-03 15:30:12.389704	1
4866	17	Santa Maria do Cambucá	santa-maria-do-cambuca	2013-09-03 15:30:12.389704	1
4867	17	Santa Terezinha	santa-terezinha	2013-09-03 15:30:12.389704	1
4868	17	São Benedito do Sul	sao-benedito-do-sul	2013-09-03 15:30:12.389704	1
4869	17	São Bento do Una	sao-bento-do-una	2013-09-03 15:30:12.389704	1
4870	17	São Caitano	sao-caitano	2013-09-03 15:30:12.389704	1
4871	17	São João	sao-joao	2013-09-03 15:30:12.389704	1
4872	17	São Joaquim do Monte	sao-joaquim-do-monte	2013-09-03 15:30:12.389704	1
4873	17	São José da Coroa Grande	sao-jose-da-coroa-grande	2013-09-03 15:30:12.389704	1
4874	17	São José do Belmonte	sao-jose-do-belmonte	2013-09-03 15:30:12.389704	1
4875	17	São José do Egito	sao-jose-do-egito	2013-09-03 15:30:12.389704	1
4876	17	São Lourenço da Mata	sao-lourenco-da-mata	2013-09-03 15:30:12.389704	1
4877	17	São Vicente Ferrer	sao-vicente-ferrer	2013-09-03 15:30:12.389704	1
4878	17	Serra Talhada	serra-talhada	2013-09-03 15:30:12.389704	1
4879	17	Serrita	serrita	2013-09-03 15:30:12.389704	1
4880	17	Sertânia	sertania	2013-09-03 15:30:12.389704	1
4881	17	Sirinhaém	sirinhaem	2013-09-03 15:30:12.389704	1
4882	17	Solidão	solidao	2013-09-03 15:30:12.389704	1
4883	17	Surubim	surubim	2013-09-03 15:30:12.389704	1
4884	17	Tabira	tabira	2013-09-03 15:30:12.389704	1
4885	17	Tacaimbó	tacaimbo	2013-09-03 15:30:12.389704	1
4886	17	Tacaratu	tacaratu	2013-09-03 15:30:12.389704	1
4887	17	Tamandaré	tamandare	2013-09-03 15:30:12.389704	1
4888	17	Taquaritinga do Norte	taquaritinga-do-norte	2013-09-03 15:30:12.389704	1
4889	17	Terezinha	terezinha	2013-09-03 15:30:12.389704	1
4890	17	Terra Nova	terra-nova	2013-09-03 15:30:12.389704	1
4891	17	Timbaúba	timbauba	2013-09-03 15:30:12.389704	1
4892	17	Toritama	toritama	2013-09-03 15:30:12.389704	1
4893	17	Tracunhaém	tracunhaem	2013-09-03 15:30:12.389704	1
4894	17	Trindade	trindade	2013-09-03 15:30:12.389704	1
4895	17	Triunfo	triunfo	2013-09-03 15:30:12.389704	1
4896	17	Tupanatinga	tupanatinga	2013-09-03 15:30:12.389704	1
4897	17	Tuparetama	tuparetama	2013-09-03 15:30:12.389704	1
4898	17	Venturosa	venturosa	2013-09-03 15:30:12.389704	1
4899	17	Verdejante	verdejante	2013-09-03 15:30:12.389704	1
4900	17	Vertente do Lério	vertente-do-lerio	2013-09-03 15:30:12.389704	1
4901	17	Vertentes	vertentes	2013-09-03 15:30:12.389704	1
4902	17	Vicência	vicencia	2013-09-03 15:30:12.389704	1
4903	17	Vitória de Santo Antão	vitoria-de-santo-antao	2013-09-03 15:30:12.389704	1
4904	17	Xexéu	xexeu	2013-09-03 15:30:12.389704	1
4975	18	Acauã	acaua	2013-09-03 15:30:12.389704	1
4976	18	Agricolândia	agricolandia	2013-09-03 15:30:12.389704	1
4977	18	Água Branca	agua-branca	2013-09-03 15:30:12.389704	1
4978	18	Alagoinha do Piauí	alagoinha-do-piaui	2013-09-03 15:30:12.389704	1
4979	18	Alegrete do Piauí	alegrete-do-piaui	2013-09-03 15:30:12.389704	1
4980	18	Alto Longá	alto-longa	2013-09-03 15:30:12.389704	1
4981	18	Altos	altos	2013-09-03 15:30:12.389704	1
4982	18	Alvorada do Gurguéia	alvorada-do-gurgueia	2013-09-03 15:30:12.389704	1
4983	18	Amarante	amarante	2013-09-03 15:30:12.389704	1
4984	18	Angical do Piauí	angical-do-piaui	2013-09-03 15:30:12.389704	1
4985	18	Anísio de Abreu	anisio-de-abreu	2013-09-03 15:30:12.389704	1
4986	18	Antônio Almeida	antonio-almeida	2013-09-03 15:30:12.389704	1
4987	18	Aroazes	aroazes	2013-09-03 15:30:12.389704	1
4988	18	Aroeiras do Itaim	aroeiras-do-itaim	2013-09-03 15:30:12.389704	1
4989	18	Arraial	arraial	2013-09-03 15:30:12.389704	1
4990	18	Assunção do Piauí	assuncao-do-piaui	2013-09-03 15:30:12.389704	1
4991	18	Avelino Lopes	avelino-lopes	2013-09-03 15:30:12.389704	1
4992	18	Baixa Grande do Ribeiro	baixa-grande-do-ribeiro	2013-09-03 15:30:12.389704	1
4993	18	Barra d`Alcântara	barra-d-alcantara	2013-09-03 15:30:12.389704	1
4994	18	Barras	barras	2013-09-03 15:30:12.389704	1
4995	18	Barreiras do Piauí	barreiras-do-piaui	2013-09-03 15:30:12.389704	1
4996	18	Barro Duro	barro-duro	2013-09-03 15:30:12.389704	1
4997	18	Batalha	batalha	2013-09-03 15:30:12.389704	1
4998	18	Bela Vista do Piauí	bela-vista-do-piaui	2013-09-03 15:30:12.389704	1
4999	18	Belém do Piauí	belem-do-piaui	2013-09-03 15:30:12.389704	1
5000	18	Beneditinos	beneditinos	2013-09-03 15:30:12.389704	1
5001	18	Bertolínia	bertolinia	2013-09-03 15:30:12.389704	1
5002	18	Betânia do Piauí	betania-do-piaui	2013-09-03 15:30:12.389704	1
5003	18	Boa Hora	boa-hora	2013-09-03 15:30:12.389704	1
5004	18	Bocaina	bocaina	2013-09-03 15:30:12.389704	1
5005	18	Bom Jesus	bom-jesus	2013-09-03 15:30:12.389704	1
5006	18	Bom Princípio do Piauí	bom-principio-do-piaui	2013-09-03 15:30:12.389704	1
5007	18	Bonfim do Piauí	bonfim-do-piaui	2013-09-03 15:30:12.389704	1
5008	18	Boqueirão do Piauí	boqueirao-do-piaui	2013-09-03 15:30:12.389704	1
5009	18	Brasileira	brasileira	2013-09-03 15:30:12.389704	1
5010	18	Brejo do Piauí	brejo-do-piaui	2013-09-03 15:30:12.389704	1
5011	18	Buriti dos Lopes	buriti-dos-lopes	2013-09-03 15:30:12.389704	1
5012	18	Buriti dos Montes	buriti-dos-montes	2013-09-03 15:30:12.389704	1
5013	18	Cabeceiras do Piauí	cabeceiras-do-piaui	2013-09-03 15:30:12.389704	1
5014	18	Cajazeiras do Piauí	cajazeiras-do-piaui	2013-09-03 15:30:12.389704	1
5015	18	Cajueiro da Praia	cajueiro-da-praia	2013-09-03 15:30:12.389704	1
5016	18	Caldeirão Grande do Piauí	caldeirao-grande-do-piaui	2013-09-03 15:30:12.389704	1
5017	18	Campinas do Piauí	campinas-do-piaui	2013-09-03 15:30:12.389704	1
5018	18	Campo Alegre do Fidalgo	campo-alegre-do-fidalgo	2013-09-03 15:30:12.389704	1
5019	18	Campo Grande do Piauí	campo-grande-do-piaui	2013-09-03 15:30:12.389704	1
5020	18	Campo Largo do Piauí	campo-largo-do-piaui	2013-09-03 15:30:12.389704	1
5021	18	Campo Maior	campo-maior	2013-09-03 15:30:12.389704	1
5022	18	Canavieira	canavieira	2013-09-03 15:30:12.389704	1
5023	18	Canto do Buriti	canto-do-buriti	2013-09-03 15:30:12.389704	1
5024	18	Capitão de Campos	capitao-de-campos	2013-09-03 15:30:12.389704	1
5025	18	Capitão Gervásio Oliveira	capitao-gervasio-oliveira	2013-09-03 15:30:12.389704	1
5026	18	Caracol	caracol	2013-09-03 15:30:12.389704	1
5027	18	Caraúbas do Piauí	caraubas-do-piaui	2013-09-03 15:30:12.389704	1
5028	18	Caridade do Piauí	caridade-do-piaui	2013-09-03 15:30:12.389704	1
5029	18	Castelo do Piauí	castelo-do-piaui	2013-09-03 15:30:12.389704	1
5030	18	Caxingó	caxingo	2013-09-03 15:30:12.389704	1
5031	18	Cocal	cocal	2013-09-03 15:30:12.389704	1
5032	18	Cocal de Telha	cocal-de-telha	2013-09-03 15:30:12.389704	1
5033	18	Cocal dos Alves	cocal-dos-alves	2013-09-03 15:30:12.389704	1
5034	18	Coivaras	coivaras	2013-09-03 15:30:12.389704	1
5035	18	Colônia do Gurguéia	colonia-do-gurgueia	2013-09-03 15:30:12.389704	1
5036	18	Colônia do Piauí	colonia-do-piaui	2013-09-03 15:30:12.389704	1
5037	18	Conceição do Canindé	conceicao-do-caninde	2013-09-03 15:30:12.389704	1
5038	18	Coronel José Dias	coronel-jose-dias	2013-09-03 15:30:12.389704	1
5039	18	Corrente	corrente	2013-09-03 15:30:12.389704	1
5040	18	Cristalândia do Piauí	cristalandia-do-piaui	2013-09-03 15:30:12.389704	1
5041	18	Cristino Castro	cristino-castro	2013-09-03 15:30:12.389704	1
5042	18	Curimatá	curimata	2013-09-03 15:30:12.389704	1
5043	18	Currais	currais	2013-09-03 15:30:12.389704	1
5044	18	Curral Novo do Piauí	curral-novo-do-piaui	2013-09-03 15:30:12.389704	1
5045	18	Curralinhos	curralinhos	2013-09-03 15:30:12.389704	1
5046	18	Demerval Lobão	demerval-lobao	2013-09-03 15:30:12.389704	1
5047	18	Dirceu Arcoverde	dirceu-arcoverde	2013-09-03 15:30:12.389704	1
5048	18	Dom Expedito Lopes	dom-expedito-lopes	2013-09-03 15:30:12.389704	1
5049	18	Dom Inocêncio	dom-inocencio	2013-09-03 15:30:12.389704	1
5050	18	Domingos Mourão	domingos-mourao	2013-09-03 15:30:12.389704	1
5051	18	Elesbão Veloso	elesbao-veloso	2013-09-03 15:30:12.389704	1
5052	18	Eliseu Martins	eliseu-martins	2013-09-03 15:30:12.389704	1
5053	18	Esperantina	esperantina	2013-09-03 15:30:12.389704	1
5054	18	Fartura do Piauí	fartura-do-piaui	2013-09-03 15:30:12.389704	1
5055	18	Flores do Piauí	flores-do-piaui	2013-09-03 15:30:12.389704	1
5056	18	Floresta do Piauí	floresta-do-piaui	2013-09-03 15:30:12.389704	1
5057	18	Floriano	floriano	2013-09-03 15:30:12.389704	1
5058	18	Francinópolis	francinopolis	2013-09-03 15:30:12.389704	1
5059	18	Francisco Ayres	francisco-ayres	2013-09-03 15:30:12.389704	1
5060	18	Francisco Macedo	francisco-macedo	2013-09-03 15:30:12.389704	1
5061	18	Francisco Santos	francisco-santos	2013-09-03 15:30:12.389704	1
5062	18	Fronteiras	fronteiras	2013-09-03 15:30:12.389704	1
5063	18	Geminiano	geminiano	2013-09-03 15:30:12.389704	1
5064	18	Gilbués	gilbues	2013-09-03 15:30:12.389704	1
5065	18	Guadalupe	guadalupe	2013-09-03 15:30:12.389704	1
5066	18	Guaribas	guaribas	2013-09-03 15:30:12.389704	1
5067	18	Hugo Napoleão	hugo-napoleao	2013-09-03 15:30:12.389704	1
5068	18	Ilha Grande	ilha-grande	2013-09-03 15:30:12.389704	1
5069	18	Inhuma	inhuma	2013-09-03 15:30:12.389704	1
5070	18	Ipiranga do Piauí	ipiranga-do-piaui	2013-09-03 15:30:12.389704	1
5071	18	Isaías Coelho	isaias-coelho	2013-09-03 15:30:12.389704	1
5072	18	Itainópolis	itainopolis	2013-09-03 15:30:12.389704	1
5073	18	Itaueira	itaueira	2013-09-03 15:30:12.389704	1
5074	18	Jacobina do Piauí	jacobina-do-piaui	2013-09-03 15:30:12.389704	1
5075	18	Jaicós	jaicos	2013-09-03 15:30:12.389704	1
5076	18	Jardim do Mulato	jardim-do-mulato	2013-09-03 15:30:12.389704	1
5077	18	Jatobá do Piauí	jatoba-do-piaui	2013-09-03 15:30:12.389704	1
5078	18	Jerumenha	jerumenha	2013-09-03 15:30:12.389704	1
5079	18	João Costa	joao-costa	2013-09-03 15:30:12.389704	1
5080	18	Joaquim Pires	joaquim-pires	2013-09-03 15:30:12.389704	1
5081	18	Joca Marques	joca-marques	2013-09-03 15:30:12.389704	1
5082	18	José de Freitas	jose-de-freitas	2013-09-03 15:30:12.389704	1
5083	18	Juazeiro do Piauí	juazeiro-do-piaui	2013-09-03 15:30:12.389704	1
5084	18	Júlio Borges	julio-borges	2013-09-03 15:30:12.389704	1
5085	18	Jurema	jurema	2013-09-03 15:30:12.389704	1
5086	18	Lagoa Alegre	lagoa-alegre	2013-09-03 15:30:12.389704	1
5087	18	Lagoa de São Francisco	lagoa-de-sao-francisco	2013-09-03 15:30:12.389704	1
5088	18	Lagoa do Barro do Piauí	lagoa-do-barro-do-piaui	2013-09-03 15:30:12.389704	1
5089	18	Lagoa do Piauí	lagoa-do-piaui	2013-09-03 15:30:12.389704	1
5090	18	Lagoa do Sítio	lagoa-do-sitio	2013-09-03 15:30:12.389704	1
5091	18	Lagoinha do Piauí	lagoinha-do-piaui	2013-09-03 15:30:12.389704	1
5092	18	Landri Sales	landri-sales	2013-09-03 15:30:12.389704	1
5093	18	Luís Correia	luis-correia	2013-09-03 15:30:12.389704	1
5094	18	Luzilândia	luzilandia	2013-09-03 15:30:12.389704	1
5095	18	Madeiro	madeiro	2013-09-03 15:30:12.389704	1
5096	18	Manoel Emídio	manoel-emidio	2013-09-03 15:30:12.389704	1
5097	18	Marcolândia	marcolandia	2013-09-03 15:30:12.389704	1
5098	18	Marcos Parente	marcos-parente	2013-09-03 15:30:12.389704	1
5099	18	Massapê do Piauí	massape-do-piaui	2013-09-03 15:30:12.389704	1
5100	18	Matias Olímpio	matias-olimpio	2013-09-03 15:30:12.389704	1
5101	18	Miguel Alves	miguel-alves	2013-09-03 15:30:12.389704	1
5102	18	Miguel Leão	miguel-leao	2013-09-03 15:30:12.389704	1
5103	18	Milton Brandão	milton-brandao	2013-09-03 15:30:12.389704	1
5104	18	Monsenhor Gil	monsenhor-gil	2013-09-03 15:30:12.389704	1
5105	18	Monsenhor Hipólito	monsenhor-hipolito	2013-09-03 15:30:12.389704	1
5106	18	Monte Alegre do Piauí	monte-alegre-do-piaui	2013-09-03 15:30:12.389704	1
5107	18	Morro Cabeça no Tempo	morro-cabeca-no-tempo	2013-09-03 15:30:12.389704	1
5108	18	Morro do Chapéu do Piauí	morro-do-chapeu-do-piaui	2013-09-03 15:30:12.389704	1
5109	18	Murici dos Portelas	murici-dos-portelas	2013-09-03 15:30:12.389704	1
5110	18	Nazaré do Piauí	nazare-do-piaui	2013-09-03 15:30:12.389704	1
5111	18	Nossa Senhora de Nazaré	nossa-senhora-de-nazare	2013-09-03 15:30:12.389704	1
5112	18	Nossa Senhora dos Remédios	nossa-senhora-dos-remedios	2013-09-03 15:30:12.389704	1
5113	18	Nova Santa Rita	nova-santa-rita	2013-09-03 15:30:12.389704	1
5114	18	Novo Oriente do Piauí	novo-oriente-do-piaui	2013-09-03 15:30:12.389704	1
5115	18	Novo Santo Antônio	novo-santo-antonio	2013-09-03 15:30:12.389704	1
5116	18	Oeiras	oeiras	2013-09-03 15:30:12.389704	1
5117	18	Olho d`Água do Piauí	olho-d-agua-do-piaui	2013-09-03 15:30:12.389704	1
5118	18	Padre Marcos	padre-marcos	2013-09-03 15:30:12.389704	1
5243	19	Cambuci	cambuci	2013-09-03 15:30:12.389704	1
5119	18	Paes Landim	paes-landim	2013-09-03 15:30:12.389704	1
5120	18	Pajeú do Piauí	pajeu-do-piaui	2013-09-03 15:30:12.389704	1
5121	18	Palmeira do Piauí	palmeira-do-piaui	2013-09-03 15:30:12.389704	1
5122	18	Palmeirais	palmeirais	2013-09-03 15:30:12.389704	1
5123	18	Paquetá	paqueta	2013-09-03 15:30:12.389704	1
5124	18	Parnaguá	parnagua	2013-09-03 15:30:12.389704	1
5125	18	Parnaíba	parnaiba	2013-09-03 15:30:12.389704	1
5126	18	Passagem Franca do Piauí	passagem-franca-do-piaui	2013-09-03 15:30:12.389704	1
5127	18	Patos do Piauí	patos-do-piaui	2013-09-03 15:30:12.389704	1
5128	18	Pau d`Arco do Piauí	pau-d-arco-do-piaui	2013-09-03 15:30:12.389704	1
5129	18	Paulistana	paulistana	2013-09-03 15:30:12.389704	1
5130	18	Pavussu	pavussu	2013-09-03 15:30:12.389704	1
5131	18	Pedro II	pedro-ii	2013-09-03 15:30:12.389704	1
5132	18	Pedro Laurentino	pedro-laurentino	2013-09-03 15:30:12.389704	1
5133	18	Picos	picos	2013-09-03 15:30:12.389704	1
5134	18	Pimenteiras	pimenteiras	2013-09-03 15:30:12.389704	1
5135	18	Pio IX	pio-ix	2013-09-03 15:30:12.389704	1
5136	18	Piracuruca	piracuruca	2013-09-03 15:30:12.389704	1
5137	18	Piripiri	piripiri	2013-09-03 15:30:12.389704	1
5138	18	Porto	porto	2013-09-03 15:30:12.389704	1
5139	18	Porto Alegre do Piauí	porto-alegre-do-piaui	2013-09-03 15:30:12.389704	1
5140	18	Prata do Piauí	prata-do-piaui	2013-09-03 15:30:12.389704	1
5141	18	Queimada Nova	queimada-nova	2013-09-03 15:30:12.389704	1
5142	18	Redenção do Gurguéia	redencao-do-gurgueia	2013-09-03 15:30:12.389704	1
5143	18	Regeneração	regeneracao	2013-09-03 15:30:12.389704	1
5144	18	Riacho Frio	riacho-frio	2013-09-03 15:30:12.389704	1
5145	18	Ribeira do Piauí	ribeira-do-piaui	2013-09-03 15:30:12.389704	1
5146	18	Ribeiro Gonçalves	ribeiro-goncalves	2013-09-03 15:30:12.389704	1
5147	18	Rio Grande do Piauí	rio-grande-do-piaui	2013-09-03 15:30:12.389704	1
5148	18	Santa Cruz do Piauí	santa-cruz-do-piaui	2013-09-03 15:30:12.389704	1
5149	18	Santa Cruz dos Milagres	santa-cruz-dos-milagres	2013-09-03 15:30:12.389704	1
5150	18	Santa Filomena	santa-filomena	2013-09-03 15:30:12.389704	1
5151	18	Santa Luz	santa-luz	2013-09-03 15:30:12.389704	1
5152	18	Santa Rosa do Piauí	santa-rosa-do-piaui	2013-09-03 15:30:12.389704	1
5153	18	Santana do Piauí	santana-do-piaui	2013-09-03 15:30:12.389704	1
5154	18	Santo Antônio de Lisboa	santo-antonio-de-lisboa	2013-09-03 15:30:12.389704	1
5155	18	Santo Antônio dos Milagres	santo-antonio-dos-milagres	2013-09-03 15:30:12.389704	1
5156	18	Santo Inácio do Piauí	santo-inacio-do-piaui	2013-09-03 15:30:12.389704	1
5157	18	São Braz do Piauí	sao-braz-do-piaui	2013-09-03 15:30:12.389704	1
5158	18	São Félix do Piauí	sao-felix-do-piaui	2013-09-03 15:30:12.389704	1
5159	18	São Francisco de Assis do Piauí	sao-francisco-de-assis-do-piaui	2013-09-03 15:30:12.389704	1
5160	18	São Francisco do Piauí	sao-francisco-do-piaui	2013-09-03 15:30:12.389704	1
5161	18	São Gonçalo do Gurguéia	sao-goncalo-do-gurgueia	2013-09-03 15:30:12.389704	1
5162	18	São Gonçalo do Piauí	sao-goncalo-do-piaui	2013-09-03 15:30:12.389704	1
5163	18	São João da Canabrava	sao-joao-da-canabrava	2013-09-03 15:30:12.389704	1
5164	18	São João da Fronteira	sao-joao-da-fronteira	2013-09-03 15:30:12.389704	1
5165	18	São João da Serra	sao-joao-da-serra	2013-09-03 15:30:12.389704	1
5166	18	São João da Varjota	sao-joao-da-varjota	2013-09-03 15:30:12.389704	1
5167	18	São João do Arraial	sao-joao-do-arraial	2013-09-03 15:30:12.389704	1
5168	18	São João do Piauí	sao-joao-do-piaui	2013-09-03 15:30:12.389704	1
5169	18	São José do Divino	sao-jose-do-divino	2013-09-03 15:30:12.389704	1
5170	18	São José do Peixe	sao-jose-do-peixe	2013-09-03 15:30:12.389704	1
5171	18	São José do Piauí	sao-jose-do-piaui	2013-09-03 15:30:12.389704	1
5172	18	São Julião	sao-juliao	2013-09-03 15:30:12.389704	1
5173	18	São Lourenço do Piauí	sao-lourenco-do-piaui	2013-09-03 15:30:12.389704	1
5174	18	São Luis do Piauí	sao-luis-do-piaui	2013-09-03 15:30:12.389704	1
5175	18	São Miguel da Baixa Grande	sao-miguel-da-baixa-grande	2013-09-03 15:30:12.389704	1
5176	18	São Miguel do Fidalgo	sao-miguel-do-fidalgo	2013-09-03 15:30:12.389704	1
5177	18	São Miguel do Tapuio	sao-miguel-do-tapuio	2013-09-03 15:30:12.389704	1
5178	18	São Pedro do Piauí	sao-pedro-do-piaui	2013-09-03 15:30:12.389704	1
5179	18	São Raimundo Nonato	sao-raimundo-nonato	2013-09-03 15:30:12.389704	1
5180	18	Sebastião Barros	sebastiao-barros	2013-09-03 15:30:12.389704	1
5181	18	Sebastião Leal	sebastiao-leal	2013-09-03 15:30:12.389704	1
5182	18	Sigefredo Pacheco	sigefredo-pacheco	2013-09-03 15:30:12.389704	1
5183	18	Simões	simoes	2013-09-03 15:30:12.389704	1
5184	18	Simplício Mendes	simplicio-mendes	2013-09-03 15:30:12.389704	1
5185	18	Socorro do Piauí	socorro-do-piaui	2013-09-03 15:30:12.389704	1
5186	18	Sussuapara	sussuapara	2013-09-03 15:30:12.389704	1
5187	18	Tamboril do Piauí	tamboril-do-piaui	2013-09-03 15:30:12.389704	1
5188	18	Tanque do Piauí	tanque-do-piaui	2013-09-03 15:30:12.389704	1
5189	18	Teresina	teresina	2013-09-03 15:30:12.389704	1
5190	18	União	uniao	2013-09-03 15:30:12.389704	1
5191	18	Uruçuí	urucui	2013-09-03 15:30:12.389704	1
5192	18	Valença do Piauí	valenca-do-piaui	2013-09-03 15:30:12.389704	1
5193	18	Várzea Branca	varzea-branca	2013-09-03 15:30:12.389704	1
5194	18	Várzea Grande	varzea-grande	2013-09-03 15:30:12.389704	1
5195	18	Vera Mendes	vera-mendes	2013-09-03 15:30:12.389704	1
5196	18	Vila Nova do Piauí	vila-nova-do-piaui	2013-09-03 15:30:12.389704	1
5197	18	Wall Ferraz	wall-ferraz	2013-09-03 15:30:12.389704	1
5230	19	Angra dos Reis	angra-dos-reis	2013-09-03 15:30:12.389704	1
5231	19	Aperibé	aperibe	2013-09-03 15:30:12.389704	1
5232	19	Araruama	araruama	2013-09-03 15:30:12.389704	1
5233	19	Areal	areal	2013-09-03 15:30:12.389704	1
5234	19	Armação dos Búzios	armacao-dos-buzios	2013-09-03 15:30:12.389704	1
5235	19	Arraial do Cabo	arraial-do-cabo	2013-09-03 15:30:12.389704	1
5236	19	Barra do Piraí	barra-do-pirai	2013-09-03 15:30:12.389704	1
5237	19	Barra Mansa	barra-mansa	2013-09-03 15:30:12.389704	1
5238	19	Belford Roxo	belford-roxo	2013-09-03 15:30:12.389704	1
5239	19	Bom Jardim	bom-jardim	2013-09-03 15:30:12.389704	1
5240	19	Bom Jesus do Itabapoana	bom-jesus-do-itabapoana	2013-09-03 15:30:12.389704	1
5241	19	Cabo Frio	cabo-frio	2013-09-03 15:30:12.389704	1
5242	19	Cachoeiras de Macacu	cachoeiras-de-macacu	2013-09-03 15:30:12.389704	1
5244	19	Campos dos Goytacazes	campos-dos-goytacazes	2013-09-03 15:30:12.389704	1
5245	19	Cantagalo	cantagalo	2013-09-03 15:30:12.389704	1
5246	19	Carapebus	carapebus	2013-09-03 15:30:12.389704	1
5247	19	Cardoso Moreira	cardoso-moreira	2013-09-03 15:30:12.389704	1
5248	19	Carmo	carmo	2013-09-03 15:30:12.389704	1
5249	19	Casimiro de Abreu	casimiro-de-abreu	2013-09-03 15:30:12.389704	1
5250	19	Comendador Levy Gasparian	comendador-levy-gasparian	2013-09-03 15:30:12.389704	1
5251	19	Conceição de Macabu	conceicao-de-macabu	2013-09-03 15:30:12.389704	1
5252	19	Cordeiro	cordeiro	2013-09-03 15:30:12.389704	1
5253	19	Duas Barras	duas-barras	2013-09-03 15:30:12.389704	1
5254	19	Duque de Caxias	duque-de-caxias	2013-09-03 15:30:12.389704	1
5255	19	Engenheiro Paulo de Frontin	engenheiro-paulo-de-frontin	2013-09-03 15:30:12.389704	1
5256	19	Guapimirim	guapimirim	2013-09-03 15:30:12.389704	1
5257	19	Iguaba Grande	iguaba-grande	2013-09-03 15:30:12.389704	1
5258	19	Itaboraí	itaborai	2013-09-03 15:30:12.389704	1
5259	19	Itaguaí	itaguai	2013-09-03 15:30:12.389704	1
5260	19	Italva	italva	2013-09-03 15:30:12.389704	1
5261	19	Itaocara	itaocara	2013-09-03 15:30:12.389704	1
5262	19	Itaperuna	itaperuna	2013-09-03 15:30:12.389704	1
5263	19	Itatiaia	itatiaia	2013-09-03 15:30:12.389704	1
5264	19	Japeri	japeri	2013-09-03 15:30:12.389704	1
5265	19	Laje do Muriaé	laje-do-muriae	2013-09-03 15:30:12.389704	1
5266	19	Macaé	macae	2013-09-03 15:30:12.389704	1
5267	19	Macuco	macuco	2013-09-03 15:30:12.389704	1
5268	19	Magé	mage	2013-09-03 15:30:12.389704	1
5269	19	Mangaratiba	mangaratiba	2013-09-03 15:30:12.389704	1
5270	19	Maricá	marica	2013-09-03 15:30:12.389704	1
5271	19	Mendes	mendes	2013-09-03 15:30:12.389704	1
5272	19	Mesquita	mesquita	2013-09-03 15:30:12.389704	1
5273	19	Miguel Pereira	miguel-pereira	2013-09-03 15:30:12.389704	1
5274	19	Miracema	miracema	2013-09-03 15:30:12.389704	1
5275	19	Natividade	natividade	2013-09-03 15:30:12.389704	1
5276	19	Nilópolis	nilopolis	2013-09-03 15:30:12.389704	1
5277	19	Niterói	niteroi	2013-09-03 15:30:12.389704	1
5278	19	Nova Friburgo	nova-friburgo	2013-09-03 15:30:12.389704	1
5279	19	Nova Iguaçu	nova-iguacu	2013-09-03 15:30:12.389704	1
5280	19	Paracambi	paracambi	2013-09-03 15:30:12.389704	1
5281	19	Paraíba do Sul	paraiba-do-sul	2013-09-03 15:30:12.389704	1
5282	19	Parati	parati	2013-09-03 15:30:12.389704	1
5283	19	Paty do Alferes	paty-do-alferes	2013-09-03 15:30:12.389704	1
5284	19	Petrópolis	petropolis	2013-09-03 15:30:12.389704	1
5285	19	Pinheiral	pinheiral	2013-09-03 15:30:12.389704	1
5286	19	Piraí	pirai	2013-09-03 15:30:12.389704	1
5287	19	Porciúncula	porciuncula	2013-09-03 15:30:12.389704	1
5288	19	Porto Real	porto-real	2013-09-03 15:30:12.389704	1
5289	19	Quatis	quatis	2013-09-03 15:30:12.389704	1
5290	19	Queimados	queimados	2013-09-03 15:30:12.389704	1
5291	19	Quissamã	quissama	2013-09-03 15:30:12.389704	1
5292	19	Resende	resende	2013-09-03 15:30:12.389704	1
5293	19	Rio Bonito	rio-bonito	2013-09-03 15:30:12.389704	1
5294	19	Rio Claro	rio-claro	2013-09-03 15:30:12.389704	1
5295	19	Rio das Flores	rio-das-flores	2013-09-03 15:30:12.389704	1
5296	19	Rio das Ostras	rio-das-ostras	2013-09-03 15:30:12.389704	1
5297	19	Rio de Janeiro	rio-de-janeiro	2013-09-03 15:30:12.389704	1
5298	19	Santa Maria Madalena	santa-maria-madalena	2013-09-03 15:30:12.389704	1
5299	19	Santo Antônio de Pádua	santo-antonio-de-padua	2013-09-03 15:30:12.389704	1
5300	19	São Fidélis	sao-fidelis	2013-09-03 15:30:12.389704	1
5301	19	São Francisco de Itabapoana	sao-francisco-de-itabapoana	2013-09-03 15:30:12.389704	1
5302	19	São Gonçalo	sao-goncalo	2013-09-03 15:30:12.389704	1
5303	19	São João da Barra	sao-joao-da-barra	2013-09-03 15:30:12.389704	1
5304	19	São João de Meriti	sao-joao-de-meriti	2013-09-03 15:30:12.389704	1
5305	19	São José de Ubá	sao-jose-de-uba	2013-09-03 15:30:12.389704	1
5306	19	São José do Vale do Rio Pret	sao-jose-do-vale-do-rio-pret	2013-09-03 15:30:12.389704	1
5307	19	São Pedro da Aldeia	sao-pedro-da-aldeia	2013-09-03 15:30:12.389704	1
5308	19	São Sebastião do Alto	sao-sebastiao-do-alto	2013-09-03 15:30:12.389704	1
5309	19	Sapucaia	sapucaia	2013-09-03 15:30:12.389704	1
5310	19	Saquarema	saquarema	2013-09-03 15:30:12.389704	1
5311	19	Seropédica	seropedica	2013-09-03 15:30:12.389704	1
5312	19	Silva Jardim	silva-jardim	2013-09-03 15:30:12.389704	1
5313	19	Sumidouro	sumidouro	2013-09-03 15:30:12.389704	1
5314	19	Tanguá	tangua	2013-09-03 15:30:12.389704	1
5315	19	Teresópolis	teresopolis	2013-09-03 15:30:12.389704	1
5316	19	Trajano de Morais	trajano-de-morais	2013-09-03 15:30:12.389704	1
5317	19	Três Rios	tres-rios	2013-09-03 15:30:12.389704	1
5318	19	Valença	valenca	2013-09-03 15:30:12.389704	1
5319	19	Varre-Sai	varre-sai	2013-09-03 15:30:12.389704	1
5320	19	Vassouras	vassouras	2013-09-03 15:30:12.389704	1
5321	19	Volta Redonda	volta-redonda	2013-09-03 15:30:12.389704	1
5357	20	Acari	acari	2013-09-03 15:30:12.389704	1
5358	20	Açu	acu	2013-09-03 15:30:12.389704	1
5359	20	Afonso Bezerra	afonso-bezerra	2013-09-03 15:30:12.389704	1
5360	20	Água Nova	agua-nova	2013-09-03 15:30:12.389704	1
5361	20	Alexandria	alexandria	2013-09-03 15:30:12.389704	1
5362	20	Almino Afonso	almino-afonso	2013-09-03 15:30:12.389704	1
5363	20	Alto do Rodrigues	alto-do-rodrigues	2013-09-03 15:30:12.389704	1
5364	20	Angicos	angicos	2013-09-03 15:30:12.389704	1
5365	20	Antônio Martins	antonio-martins	2013-09-03 15:30:12.389704	1
5366	20	Apodi	apodi	2013-09-03 15:30:12.389704	1
5367	20	Areia Branca	areia-branca	2013-09-03 15:30:12.389704	1
5368	20	Arês	ares	2013-09-03 15:30:12.389704	1
5369	20	Augusto Severo	augusto-severo	2013-09-03 15:30:12.389704	1
5370	20	Baía Formosa	baia-formosa	2013-09-03 15:30:12.389704	1
5371	20	Baraúna	barauna	2013-09-03 15:30:12.389704	1
5372	20	Barcelona	barcelona	2013-09-03 15:30:12.389704	1
5373	20	Bento Fernandes	bento-fernandes	2013-09-03 15:30:12.389704	1
5374	20	Bodó	bodo	2013-09-03 15:30:12.389704	1
5375	20	Bom Jesus	bom-jesus	2013-09-03 15:30:12.389704	1
5376	20	Brejinho	brejinho	2013-09-03 15:30:12.389704	1
5377	20	Caiçara do Norte	caicara-do-norte	2013-09-03 15:30:12.389704	1
5378	20	Caiçara do Rio do Vento	caicara-do-rio-do-vento	2013-09-03 15:30:12.389704	1
5379	20	Caicó	caico	2013-09-03 15:30:12.389704	1
5380	20	Campo Redondo	campo-redondo	2013-09-03 15:30:12.389704	1
5381	20	Canguaretama	canguaretama	2013-09-03 15:30:12.389704	1
5382	20	Caraúbas	caraubas	2013-09-03 15:30:12.389704	1
5383	20	Carnaúba dos Dantas	carnauba-dos-dantas	2013-09-03 15:30:12.389704	1
5384	20	Carnaubais	carnaubais	2013-09-03 15:30:12.389704	1
5385	20	Ceará-Mirim	ceara-mirim	2013-09-03 15:30:12.389704	1
5386	20	Cerro Corá	cerro-cora	2013-09-03 15:30:12.389704	1
5387	20	Coronel Ezequiel	coronel-ezequiel	2013-09-03 15:30:12.389704	1
5388	20	Coronel João Pessoa	coronel-joao-pessoa	2013-09-03 15:30:12.389704	1
5389	20	Cruzeta	cruzeta	2013-09-03 15:30:12.389704	1
5390	20	Currais Novos	currais-novos	2013-09-03 15:30:12.389704	1
5391	20	Doutor Severiano	doutor-severiano	2013-09-03 15:30:12.389704	1
5392	20	Encanto	encanto	2013-09-03 15:30:12.389704	1
5393	20	Equador	equador	2013-09-03 15:30:12.389704	1
5394	20	Espírito Santo	espirito-santo	2013-09-03 15:30:12.389704	1
5395	20	Extremoz	extremoz	2013-09-03 15:30:12.389704	1
5396	20	Felipe Guerra	felipe-guerra	2013-09-03 15:30:12.389704	1
5397	20	Fernando Pedroza	fernando-pedroza	2013-09-03 15:30:12.389704	1
5398	20	Florânia	florania	2013-09-03 15:30:12.389704	1
5399	20	Francisco Dantas	francisco-dantas	2013-09-03 15:30:12.389704	1
5400	20	Frutuoso Gomes	frutuoso-gomes	2013-09-03 15:30:12.389704	1
5401	20	Galinhos	galinhos	2013-09-03 15:30:12.389704	1
5402	20	Goianinha	goianinha	2013-09-03 15:30:12.389704	1
5403	20	Governador Dix-Sept Rosado	governador-dix-sept-rosado	2013-09-03 15:30:12.389704	1
5404	20	Grossos	grossos	2013-09-03 15:30:12.389704	1
5405	20	Guamaré	guamare	2013-09-03 15:30:12.389704	1
5406	20	Ielmo Marinho	ielmo-marinho	2013-09-03 15:30:12.389704	1
5407	20	Ipanguaçu	ipanguacu	2013-09-03 15:30:12.389704	1
5408	20	Ipueira	ipueira	2013-09-03 15:30:12.389704	1
5409	20	Itajá	itaja	2013-09-03 15:30:12.389704	1
5410	20	Itaú	itau	2013-09-03 15:30:12.389704	1
5411	20	Jaçanã	jacana	2013-09-03 15:30:12.389704	1
5412	20	Jandaíra	jandaira	2013-09-03 15:30:12.389704	1
5413	20	Janduís	janduis	2013-09-03 15:30:12.389704	1
5414	20	Januário Cicco	januario-cicco	2013-09-03 15:30:12.389704	1
5415	20	Japi	japi	2013-09-03 15:30:12.389704	1
5416	20	Jardim de Angicos	jardim-de-angicos	2013-09-03 15:30:12.389704	1
5417	20	Jardim de Piranhas	jardim-de-piranhas	2013-09-03 15:30:12.389704	1
5418	20	Jardim do Seridó	jardim-do-serido	2013-09-03 15:30:12.389704	1
5419	20	João Câmara	joao-camara	2013-09-03 15:30:12.389704	1
5420	20	João Dias	joao-dias	2013-09-03 15:30:12.389704	1
5421	20	José da Penha	jose-da-penha	2013-09-03 15:30:12.389704	1
5422	20	Jucurutu	jucurutu	2013-09-03 15:30:12.389704	1
5423	20	Jundiá	jundia	2013-09-03 15:30:12.389704	1
5424	20	Lagoa d`Anta	lagoa-d-anta	2013-09-03 15:30:12.389704	1
5425	20	Lagoa de Pedras	lagoa-de-pedras	2013-09-03 15:30:12.389704	1
5426	20	Lagoa de Velhos	lagoa-de-velhos	2013-09-03 15:30:12.389704	1
5427	20	Lagoa Nova	lagoa-nova	2013-09-03 15:30:12.389704	1
5428	20	Lagoa Salgada	lagoa-salgada	2013-09-03 15:30:12.389704	1
5429	20	Lajes	lajes	2013-09-03 15:30:12.389704	1
5430	20	Lajes Pintadas	lajes-pintadas	2013-09-03 15:30:12.389704	1
5431	20	Lucrécia	lucrecia	2013-09-03 15:30:12.389704	1
5432	20	Luís Gomes	luis-gomes	2013-09-03 15:30:12.389704	1
5433	20	Macaíba	macaiba	2013-09-03 15:30:12.389704	1
5434	20	Macau	macau	2013-09-03 15:30:12.389704	1
5435	20	Major Sales	major-sales	2013-09-03 15:30:12.389704	1
5436	20	Marcelino Vieira	marcelino-vieira	2013-09-03 15:30:12.389704	1
5437	20	Martins	martins	2013-09-03 15:30:12.389704	1
5438	20	Maxaranguape	maxaranguape	2013-09-03 15:30:12.389704	1
5439	20	Messias Targino	messias-targino	2013-09-03 15:30:12.389704	1
5440	20	Montanhas	montanhas	2013-09-03 15:30:12.389704	1
5441	20	Monte Alegre	monte-alegre	2013-09-03 15:30:12.389704	1
5442	20	Monte das Gameleiras	monte-das-gameleiras	2013-09-03 15:30:12.389704	1
5443	20	Mossoró	mossoro	2013-09-03 15:30:12.389704	1
5444	20	Natal	natal	2013-09-03 15:30:12.389704	1
5445	20	Nísia Floresta	nisia-floresta	2013-09-03 15:30:12.389704	1
5446	20	Nova Cruz	nova-cruz	2013-09-03 15:30:12.389704	1
5659	21	Boa Vista do Sul	boa-vista-do-sul	2013-09-03 15:30:12.389704	1
5447	20	Olho-d`Água do Borges	olho-d-agua-do-borges	2013-09-03 15:30:12.389704	1
5448	20	Ouro Branco	ouro-branco	2013-09-03 15:30:12.389704	1
5449	20	Paraná	parana	2013-09-03 15:30:12.389704	1
5450	20	Paraú	parau	2013-09-03 15:30:12.389704	1
5451	20	Parazinho	parazinho	2013-09-03 15:30:12.389704	1
5452	20	Parelhas	parelhas	2013-09-03 15:30:12.389704	1
5453	20	Parnamirim	parnamirim	2013-09-03 15:30:12.389704	1
5454	20	Passa e Fica	passa-e-fica	2013-09-03 15:30:12.389704	1
5455	20	Passagem	passagem	2013-09-03 15:30:12.389704	1
5456	20	Patu	patu	2013-09-03 15:30:12.389704	1
5457	20	Pau dos Ferros	pau-dos-ferros	2013-09-03 15:30:12.389704	1
5458	20	Pedra Grande	pedra-grande	2013-09-03 15:30:12.389704	1
5459	20	Pedra Preta	pedra-preta	2013-09-03 15:30:12.389704	1
5460	20	Pedro Avelino	pedro-avelino	2013-09-03 15:30:12.389704	1
5461	20	Pedro Velho	pedro-velho	2013-09-03 15:30:12.389704	1
5462	20	Pendências	pendencias	2013-09-03 15:30:12.389704	1
5463	20	Pilões	piloes	2013-09-03 15:30:12.389704	1
5464	20	Poço Branco	poco-branco	2013-09-03 15:30:12.389704	1
5465	20	Portalegre	portalegre	2013-09-03 15:30:12.389704	1
5466	20	Porto do Mangue	porto-do-mangue	2013-09-03 15:30:12.389704	1
5467	20	Presidente Juscelino	presidente-juscelino	2013-09-03 15:30:12.389704	1
5468	20	Pureza	pureza	2013-09-03 15:30:12.389704	1
5469	20	Rafael Fernandes	rafael-fernandes	2013-09-03 15:30:12.389704	1
5470	20	Rafael Godeiro	rafael-godeiro	2013-09-03 15:30:12.389704	1
5471	20	Riacho da Cruz	riacho-da-cruz	2013-09-03 15:30:12.389704	1
5472	20	Riacho de Santana	riacho-de-santana	2013-09-03 15:30:12.389704	1
5473	20	Riachuelo	riachuelo	2013-09-03 15:30:12.389704	1
5474	20	Rio do Fogo	rio-do-fogo	2013-09-03 15:30:12.389704	1
5475	20	Rodolfo Fernandes	rodolfo-fernandes	2013-09-03 15:30:12.389704	1
5476	20	Ruy Barbosa	ruy-barbosa	2013-09-03 15:30:12.389704	1
5477	20	Santa Cruz	santa-cruz	2013-09-03 15:30:12.389704	1
5478	20	Santa Maria	santa-maria	2013-09-03 15:30:12.389704	1
5479	20	Santana do Matos	santana-do-matos	2013-09-03 15:30:12.389704	1
5480	20	Santana do Seridó	santana-do-serido	2013-09-03 15:30:12.389704	1
5481	20	Santo Antônio	santo-antonio	2013-09-03 15:30:12.389704	1
5482	20	São Bento do Norte	sao-bento-do-norte	2013-09-03 15:30:12.389704	1
5483	20	São Bento do Trairí	sao-bento-do-trairi	2013-09-03 15:30:12.389704	1
5484	20	São Fernando	sao-fernando	2013-09-03 15:30:12.389704	1
5485	20	São Francisco do Oeste	sao-francisco-do-oeste	2013-09-03 15:30:12.389704	1
5486	20	São Gonçalo do Amarante	sao-goncalo-do-amarante	2013-09-03 15:30:12.389704	1
5487	20	São João do Sabugi	sao-joao-do-sabugi	2013-09-03 15:30:12.389704	1
5488	20	São José de Mipibu	sao-jose-de-mipibu	2013-09-03 15:30:12.389704	1
5489	20	São José do Campestre	sao-jose-do-campestre	2013-09-03 15:30:12.389704	1
5490	20	São José do Seridó	sao-jose-do-serido	2013-09-03 15:30:12.389704	1
5491	20	São Miguel	sao-miguel	2013-09-03 15:30:12.389704	1
5492	20	São Miguel do Gostoso	sao-miguel-do-gostoso	2013-09-03 15:30:12.389704	1
5493	20	São Paulo do Potengi	sao-paulo-do-potengi	2013-09-03 15:30:12.389704	1
5494	20	São Pedro	sao-pedro	2013-09-03 15:30:12.389704	1
5495	20	São Rafael	sao-rafael	2013-09-03 15:30:12.389704	1
5496	20	São Tomé	sao-tome	2013-09-03 15:30:12.389704	1
5497	20	São Vicente	sao-vicente	2013-09-03 15:30:12.389704	1
5498	20	Senador Elói de Souza	senador-eloi-de-souza	2013-09-03 15:30:12.389704	1
5499	20	Senador Georgino Avelino	senador-georgino-avelino	2013-09-03 15:30:12.389704	1
5500	20	Serra de São Bento	serra-de-sao-bento	2013-09-03 15:30:12.389704	1
5501	20	Serra do Mel	serra-do-mel	2013-09-03 15:30:12.389704	1
5502	20	Serra Negra do Norte	serra-negra-do-norte	2013-09-03 15:30:12.389704	1
5503	20	Serrinha	serrinha	2013-09-03 15:30:12.389704	1
5504	20	Serrinha dos Pintos	serrinha-dos-pintos	2013-09-03 15:30:12.389704	1
5505	20	Severiano Melo	severiano-melo	2013-09-03 15:30:12.389704	1
5506	20	Sítio Novo	sitio-novo	2013-09-03 15:30:12.389704	1
5507	20	Taboleiro Grande	taboleiro-grande	2013-09-03 15:30:12.389704	1
5508	20	Taipu	taipu	2013-09-03 15:30:12.389704	1
5509	20	Tangará	tangara	2013-09-03 15:30:12.389704	1
5510	20	Tenente Ananias	tenente-ananias	2013-09-03 15:30:12.389704	1
5511	20	Tenente Laurentino Cruz	tenente-laurentino-cruz	2013-09-03 15:30:12.389704	1
5512	20	Tibau	tibau	2013-09-03 15:30:12.389704	1
5513	20	Tibau do Sul	tibau-do-sul	2013-09-03 15:30:12.389704	1
5514	20	Timbaúba dos Batistas	timbauba-dos-batistas	2013-09-03 15:30:12.389704	1
5515	20	Touros	touros	2013-09-03 15:30:12.389704	1
5516	20	Triunfo Potiguar	triunfo-potiguar	2013-09-03 15:30:12.389704	1
5517	20	Umarizal	umarizal	2013-09-03 15:30:12.389704	1
5518	20	Upanema	upanema	2013-09-03 15:30:12.389704	1
5519	20	Várzea	varzea	2013-09-03 15:30:12.389704	1
5520	20	Venha-Ver	venha-ver	2013-09-03 15:30:12.389704	1
5521	20	Vera Cruz	vera-cruz	2013-09-03 15:30:12.389704	1
5522	20	Viçosa	vicosa	2013-09-03 15:30:12.389704	1
5523	20	Vila Flor	vila-flor	2013-09-03 15:30:12.389704	1
5612	21	Aceguá	acegua	2013-09-03 15:30:12.389704	1
5613	21	Água Santa	agua-santa	2013-09-03 15:30:12.389704	1
5614	21	Agudo	agudo	2013-09-03 15:30:12.389704	1
5615	21	Ajuricaba	ajuricaba	2013-09-03 15:30:12.389704	1
5616	21	Alecrim	alecrim	2013-09-03 15:30:12.389704	1
5617	21	Alegrete	alegrete	2013-09-03 15:30:12.389704	1
5618	21	Alegria	alegria	2013-09-03 15:30:12.389704	1
5619	21	Almirante Tamandaré do Sul	almirante-tamandare-do-sul	2013-09-03 15:30:12.389704	1
5620	21	Alpestre	alpestre	2013-09-03 15:30:12.389704	1
5621	21	Alto Alegre	alto-alegre	2013-09-03 15:30:12.389704	1
5622	21	Alto Feliz	alto-feliz	2013-09-03 15:30:12.389704	1
5623	21	Alvorada	alvorada	2013-09-03 15:30:12.389704	1
5624	21	Amaral Ferrador	amaral-ferrador	2013-09-03 15:30:12.389704	1
5625	21	Ametista do Sul	ametista-do-sul	2013-09-03 15:30:12.389704	1
5626	21	André da Rocha	andre-da-rocha	2013-09-03 15:30:12.389704	1
5627	21	Anta Gorda	anta-gorda	2013-09-03 15:30:12.389704	1
5628	21	Antônio Prado	antonio-prado	2013-09-03 15:30:12.389704	1
5629	21	Arambaré	arambare	2013-09-03 15:30:12.389704	1
5630	21	Araricá	ararica	2013-09-03 15:30:12.389704	1
5631	21	Aratiba	aratiba	2013-09-03 15:30:12.389704	1
5632	21	Arroio do Meio	arroio-do-meio	2013-09-03 15:30:12.389704	1
5633	21	Arroio do Padre	arroio-do-padre	2013-09-03 15:30:12.389704	1
5634	21	Arroio do Sal	arroio-do-sal	2013-09-03 15:30:12.389704	1
5635	21	Arroio do Tigre	arroio-do-tigre	2013-09-03 15:30:12.389704	1
5636	21	Arroio dos Ratos	arroio-dos-ratos	2013-09-03 15:30:12.389704	1
5637	21	Arroio Grande	arroio-grande	2013-09-03 15:30:12.389704	1
5638	21	Arvorezinha	arvorezinha	2013-09-03 15:30:12.389704	1
5639	21	Augusto Pestana	augusto-pestana	2013-09-03 15:30:12.389704	1
5640	21	Áurea	aurea	2013-09-03 15:30:12.389704	1
5641	21	Bagé	bage	2013-09-03 15:30:12.389704	1
5642	21	Balneário Pinhal	balneario-pinhal	2013-09-03 15:30:12.389704	1
5643	21	Barão	barao	2013-09-03 15:30:12.389704	1
5644	21	Barão de Cotegipe	barao-de-cotegipe	2013-09-03 15:30:12.389704	1
5645	21	Barão do Triunfo	barao-do-triunfo	2013-09-03 15:30:12.389704	1
5646	21	Barra do Guarita	barra-do-guarita	2013-09-03 15:30:12.389704	1
5647	21	Barra do Quaraí	barra-do-quarai	2013-09-03 15:30:12.389704	1
5648	21	Barra do Ribeiro	barra-do-ribeiro	2013-09-03 15:30:12.389704	1
5649	21	Barra do Rio Azul	barra-do-rio-azul	2013-09-03 15:30:12.389704	1
5650	21	Barra Funda	barra-funda	2013-09-03 15:30:12.389704	1
5651	21	Barracão	barracao	2013-09-03 15:30:12.389704	1
5652	21	Barros Cassal	barros-cassal	2013-09-03 15:30:12.389704	1
5653	21	Benjamin Constant do Sul	benjamin-constant-do-sul	2013-09-03 15:30:12.389704	1
5654	21	Bento Gonçalves	bento-goncalves	2013-09-03 15:30:12.389704	1
5655	21	Boa Vista das Missões	boa-vista-das-missoes	2013-09-03 15:30:12.389704	1
5656	21	Boa Vista do Buricá	boa-vista-do-burica	2013-09-03 15:30:12.389704	1
5657	21	Boa Vista do Cadeado	boa-vista-do-cadeado	2013-09-03 15:30:12.389704	1
5658	21	Boa Vista do Incra	boa-vista-do-incra	2013-09-03 15:30:12.389704	1
5660	21	Bom Jesus	bom-jesus	2013-09-03 15:30:12.389704	1
5661	21	Bom Princípio	bom-principio	2013-09-03 15:30:12.389704	1
5662	21	Bom Progresso	bom-progresso	2013-09-03 15:30:12.389704	1
5663	21	Bom Retiro do Sul	bom-retiro-do-sul	2013-09-03 15:30:12.389704	1
5664	21	Boqueirão do Leão	boqueirao-do-leao	2013-09-03 15:30:12.389704	1
5665	21	Bossoroca	bossoroca	2013-09-03 15:30:12.389704	1
5666	21	Bozano	bozano	2013-09-03 15:30:12.389704	1
5667	21	Braga	braga	2013-09-03 15:30:12.389704	1
5668	21	Brochier	brochier	2013-09-03 15:30:12.389704	1
5669	21	Butiá	butia	2013-09-03 15:30:12.389704	1
5670	21	Caçapava do Sul	cacapava-do-sul	2013-09-03 15:30:12.389704	1
5671	21	Cacequi	cacequi	2013-09-03 15:30:12.389704	1
5672	21	Cachoeira do Sul	cachoeira-do-sul	2013-09-03 15:30:12.389704	1
5673	21	Cachoeirinha	cachoeirinha	2013-09-03 15:30:12.389704	1
5674	21	Cacique Doble	cacique-doble	2013-09-03 15:30:12.389704	1
5675	21	Caibaté	caibate	2013-09-03 15:30:12.389704	1
5676	21	Caiçara	caicara	2013-09-03 15:30:12.389704	1
5677	21	Camaquã	camaqua	2013-09-03 15:30:12.389704	1
5678	21	Camargo	camargo	2013-09-03 15:30:12.389704	1
5679	21	Cambará do Sul	cambara-do-sul	2013-09-03 15:30:12.389704	1
5680	21	Campestre da Serra	campestre-da-serra	2013-09-03 15:30:12.389704	1
5681	21	Campina das Missões	campina-das-missoes	2013-09-03 15:30:12.389704	1
5682	21	Campinas do Sul	campinas-do-sul	2013-09-03 15:30:12.389704	1
5683	21	Campo Bom	campo-bom	2013-09-03 15:30:12.389704	1
5684	21	Campo Novo	campo-novo	2013-09-03 15:30:12.389704	1
5685	21	Campos Borges	campos-borges	2013-09-03 15:30:12.389704	1
5686	21	Candelária	candelaria	2013-09-03 15:30:12.389704	1
5687	21	Cândido Godói	candido-godoi	2013-09-03 15:30:12.389704	1
5688	21	Candiota	candiota	2013-09-03 15:30:12.389704	1
5689	21	Canela	canela	2013-09-03 15:30:12.389704	1
5690	21	Canguçu	cangucu	2013-09-03 15:30:12.389704	1
5691	21	Canoas	canoas	2013-09-03 15:30:12.389704	1
5692	21	Canudos do Vale	canudos-do-vale	2013-09-03 15:30:12.389704	1
5693	21	Capão Bonito do Sul	capao-bonito-do-sul	2013-09-03 15:30:12.389704	1
5694	21	Capão da Canoa	capao-da-canoa	2013-09-03 15:30:12.389704	1
5695	21	Capão do Cipó	capao-do-cipo	2013-09-03 15:30:12.389704	1
5696	21	Capão do Leão	capao-do-leao	2013-09-03 15:30:12.389704	1
5697	21	Capela de Santana	capela-de-santana	2013-09-03 15:30:12.389704	1
5698	21	Capitão	capitao	2013-09-03 15:30:12.389704	1
5699	21	Capivari do Sul	capivari-do-sul	2013-09-03 15:30:12.389704	1
5700	21	Caraá	caraa	2013-09-03 15:30:12.389704	1
5701	21	Carazinho	carazinho	2013-09-03 15:30:12.389704	1
5702	21	Carlos Barbosa	carlos-barbosa	2013-09-03 15:30:12.389704	1
5703	21	Carlos Gomes	carlos-gomes	2013-09-03 15:30:12.389704	1
5704	21	Casca	casca	2013-09-03 15:30:12.389704	1
5705	21	Caseiros	caseiros	2013-09-03 15:30:12.389704	1
5706	21	Catuípe	catuipe	2013-09-03 15:30:12.389704	1
5707	21	Caxias do Sul	caxias-do-sul	2013-09-03 15:30:12.389704	1
5708	21	Centenário	centenario	2013-09-03 15:30:12.389704	1
5709	21	Cerrito	cerrito	2013-09-03 15:30:12.389704	1
5710	21	Cerro Branco	cerro-branco	2013-09-03 15:30:12.389704	1
5711	21	Cerro Grande	cerro-grande	2013-09-03 15:30:12.389704	1
5712	21	Cerro Grande do Sul	cerro-grande-do-sul	2013-09-03 15:30:12.389704	1
5713	21	Cerro Largo	cerro-largo	2013-09-03 15:30:12.389704	1
5714	21	Chapada	chapada	2013-09-03 15:30:12.389704	1
5715	21	Charqueadas	charqueadas	2013-09-03 15:30:12.389704	1
5716	21	Charrua	charrua	2013-09-03 15:30:12.389704	1
5717	21	Chiapeta	chiapeta	2013-09-03 15:30:12.389704	1
5718	21	Chuí	chui	2013-09-03 15:30:12.389704	1
5719	21	Chuvisca	chuvisca	2013-09-03 15:30:12.389704	1
5720	21	Cidreira	cidreira	2013-09-03 15:30:12.389704	1
5721	21	Ciríaco	ciriaco	2013-09-03 15:30:12.389704	1
5722	21	Colinas	colinas	2013-09-03 15:30:12.389704	1
5723	21	Colorado	colorado	2013-09-03 15:30:12.389704	1
5724	21	Condor	condor	2013-09-03 15:30:12.389704	1
5725	21	Constantina	constantina	2013-09-03 15:30:12.389704	1
5726	21	Coqueiro Baixo	coqueiro-baixo	2013-09-03 15:30:12.389704	1
5727	21	Coqueiros do Sul	coqueiros-do-sul	2013-09-03 15:30:12.389704	1
5728	21	Coronel Barros	coronel-barros	2013-09-03 15:30:12.389704	1
5729	21	Coronel Bicaco	coronel-bicaco	2013-09-03 15:30:12.389704	1
5730	21	Coronel Pilar	coronel-pilar	2013-09-03 15:30:12.389704	1
5731	21	Cotiporã	cotipora	2013-09-03 15:30:12.389704	1
5732	21	Coxilha	coxilha	2013-09-03 15:30:12.389704	1
5733	21	Crissiumal	crissiumal	2013-09-03 15:30:12.389704	1
5734	21	Cristal	cristal	2013-09-03 15:30:12.389704	1
5735	21	Cristal do Sul	cristal-do-sul	2013-09-03 15:30:12.389704	1
5736	21	Cruz Alta	cruz-alta	2013-09-03 15:30:12.389704	1
5737	21	Cruzaltense	cruzaltense	2013-09-03 15:30:12.389704	1
5738	21	Cruzeiro do Sul	cruzeiro-do-sul	2013-09-03 15:30:12.389704	1
5739	21	David Canabarro	david-canabarro	2013-09-03 15:30:12.389704	1
5740	21	Derrubadas	derrubadas	2013-09-03 15:30:12.389704	1
5741	21	Dezesseis de Novembro	dezesseis-de-novembro	2013-09-03 15:30:12.389704	1
5742	21	Dilermando de Aguiar	dilermando-de-aguiar	2013-09-03 15:30:12.389704	1
5743	21	Dois Irmãos	dois-irmaos	2013-09-03 15:30:12.389704	1
5744	21	Dois Irmãos das Missões	dois-irmaos-das-missoes	2013-09-03 15:30:12.389704	1
5745	21	Dois Lajeados	dois-lajeados	2013-09-03 15:30:12.389704	1
5746	21	Dom Feliciano	dom-feliciano	2013-09-03 15:30:12.389704	1
5747	21	Dom Pedrito	dom-pedrito	2013-09-03 15:30:12.389704	1
5748	21	Dom Pedro de Alcântara	dom-pedro-de-alcantara	2013-09-03 15:30:12.389704	1
5749	21	Dona Francisca	dona-francisca	2013-09-03 15:30:12.389704	1
5750	21	Doutor Maurício Cardoso	doutor-mauricio-cardoso	2013-09-03 15:30:12.389704	1
5751	21	Doutor Ricardo	doutor-ricardo	2013-09-03 15:30:12.389704	1
5752	21	Eldorado do Sul	eldorado-do-sul	2013-09-03 15:30:12.389704	1
5753	21	Encantado	encantado	2013-09-03 15:30:12.389704	1
5754	21	Encruzilhada do Sul	encruzilhada-do-sul	2013-09-03 15:30:12.389704	1
5755	21	Engenho Velho	engenho-velho	2013-09-03 15:30:12.389704	1
5756	21	Entre Rios do Sul	entre-rios-do-sul	2013-09-03 15:30:12.389704	1
5757	21	Entre-Ijuís	entre-ijuis	2013-09-03 15:30:12.389704	1
5758	21	Erebango	erebango	2013-09-03 15:30:12.389704	1
5759	21	Erechim	erechim	2013-09-03 15:30:12.389704	1
5760	21	Ernestina	ernestina	2013-09-03 15:30:12.389704	1
5761	21	Erval Grande	erval-grande	2013-09-03 15:30:12.389704	1
5762	21	Erval Seco	erval-seco	2013-09-03 15:30:12.389704	1
5763	21	Esmeralda	esmeralda	2013-09-03 15:30:12.389704	1
5764	21	Esperança do Sul	esperanca-do-sul	2013-09-03 15:30:12.389704	1
5765	21	Espumoso	espumoso	2013-09-03 15:30:12.389704	1
5766	21	Estação	estacao	2013-09-03 15:30:12.389704	1
5767	21	Estância Velha	estancia-velha	2013-09-03 15:30:12.389704	1
5768	21	Esteio	esteio	2013-09-03 15:30:12.389704	1
5769	21	Estrela	estrela	2013-09-03 15:30:12.389704	1
5770	21	Estrela Velha	estrela-velha	2013-09-03 15:30:12.389704	1
5771	21	Eugênio de Castro	eugenio-de-castro	2013-09-03 15:30:12.389704	1
5772	21	Fagundes Varela	fagundes-varela	2013-09-03 15:30:12.389704	1
5773	21	Farroupilha	farroupilha	2013-09-03 15:30:12.389704	1
5774	21	Faxinal do Soturno	faxinal-do-soturno	2013-09-03 15:30:12.389704	1
5775	21	Faxinalzinho	faxinalzinho	2013-09-03 15:30:12.389704	1
5776	21	Fazenda Vilanova	fazenda-vilanova	2013-09-03 15:30:12.389704	1
5777	21	Feliz	feliz	2013-09-03 15:30:12.389704	1
5778	21	Flores da Cunha	flores-da-cunha	2013-09-03 15:30:12.389704	1
5779	21	Floriano Peixoto	floriano-peixoto	2013-09-03 15:30:12.389704	1
5780	21	Fontoura Xavier	fontoura-xavier	2013-09-03 15:30:12.389704	1
5781	21	Formigueiro	formigueiro	2013-09-03 15:30:12.389704	1
5782	21	Forquetinha	forquetinha	2013-09-03 15:30:12.389704	1
5783	21	Fortaleza dos Valos	fortaleza-dos-valos	2013-09-03 15:30:12.389704	1
5784	21	Frederico Westphalen	frederico-westphalen	2013-09-03 15:30:12.389704	1
5785	21	Garibaldi	garibaldi	2013-09-03 15:30:12.389704	1
5786	21	Garruchos	garruchos	2013-09-03 15:30:12.389704	1
5787	21	Gaurama	gaurama	2013-09-03 15:30:12.389704	1
5788	21	General Câmara	general-camara	2013-09-03 15:30:12.389704	1
5789	21	Gentil	gentil	2013-09-03 15:30:12.389704	1
5790	21	Getúlio Vargas	getulio-vargas	2013-09-03 15:30:12.389704	1
5791	21	Giruá	girua	2013-09-03 15:30:12.389704	1
5792	21	Glorinha	glorinha	2013-09-03 15:30:12.389704	1
5793	21	Gramado	gramado	2013-09-03 15:30:12.389704	1
5794	21	Gramado dos Loureiros	gramado-dos-loureiros	2013-09-03 15:30:12.389704	1
5795	21	Gramado Xavier	gramado-xavier	2013-09-03 15:30:12.389704	1
5796	21	Gravataí	gravatai	2013-09-03 15:30:12.389704	1
5797	21	Guabiju	guabiju	2013-09-03 15:30:12.389704	1
5798	21	Guaíba	guaiba	2013-09-03 15:30:12.389704	1
5799	21	Guaporé	guapore	2013-09-03 15:30:12.389704	1
5800	21	Guarani das Missões	guarani-das-missoes	2013-09-03 15:30:12.389704	1
5801	21	Harmonia	harmonia	2013-09-03 15:30:12.389704	1
5802	21	Herval	herval	2013-09-03 15:30:12.389704	1
5803	21	Herveiras	herveiras	2013-09-03 15:30:12.389704	1
5804	21	Horizontina	horizontina	2013-09-03 15:30:12.389704	1
5805	21	Hulha Negra	hulha-negra	2013-09-03 15:30:12.389704	1
5806	21	Humaitá	humaita	2013-09-03 15:30:12.389704	1
5807	21	Ibarama	ibarama	2013-09-03 15:30:12.389704	1
5808	21	Ibiaçá	ibiaca	2013-09-03 15:30:12.389704	1
5809	21	Ibiraiaras	ibiraiaras	2013-09-03 15:30:12.389704	1
5810	21	Ibirapuitã	ibirapuita	2013-09-03 15:30:12.389704	1
5811	21	Ibirubá	ibiruba	2013-09-03 15:30:12.389704	1
5812	21	Igrejinha	igrejinha	2013-09-03 15:30:12.389704	1
5813	21	Ijuí	ijui	2013-09-03 15:30:12.389704	1
5814	21	Ilópolis	ilopolis	2013-09-03 15:30:12.389704	1
5815	21	Imbé	imbe	2013-09-03 15:30:12.389704	1
5816	21	Imigrante	imigrante	2013-09-03 15:30:12.389704	1
5817	21	Independência	independencia	2013-09-03 15:30:12.389704	1
5818	21	Inhacorá	inhacora	2013-09-03 15:30:12.389704	1
5819	21	Ipê	ipe	2013-09-03 15:30:12.389704	1
5820	21	Ipiranga do Sul	ipiranga-do-sul	2013-09-03 15:30:12.389704	1
5821	21	Iraí	irai	2013-09-03 15:30:12.389704	1
5822	21	Itaara	itaara	2013-09-03 15:30:12.389704	1
5823	21	Itacurubi	itacurubi	2013-09-03 15:30:12.389704	1
5824	21	Itapuca	itapuca	2013-09-03 15:30:12.389704	1
5825	21	Itaqui	itaqui	2013-09-03 15:30:12.389704	1
5826	21	Itati	itati	2013-09-03 15:30:12.389704	1
5827	21	Itatiba do Sul	itatiba-do-sul	2013-09-03 15:30:12.389704	1
5828	21	Ivorá	ivora	2013-09-03 15:30:12.389704	1
5829	21	Ivoti	ivoti	2013-09-03 15:30:12.389704	1
5830	21	Jaboticaba	jaboticaba	2013-09-03 15:30:12.389704	1
5831	21	Jacuizinho	jacuizinho	2013-09-03 15:30:12.389704	1
5832	21	Jacutinga	jacutinga	2013-09-03 15:30:12.389704	1
5833	21	Jaguarão	jaguarao	2013-09-03 15:30:12.389704	1
5834	21	Jaguari	jaguari	2013-09-03 15:30:12.389704	1
5835	21	Jaquirana	jaquirana	2013-09-03 15:30:12.389704	1
5836	21	Jari	jari	2013-09-03 15:30:12.389704	1
5837	21	Jóia	joia	2013-09-03 15:30:12.389704	1
5838	21	Júlio de Castilhos	julio-de-castilhos	2013-09-03 15:30:12.389704	1
5839	21	Lagoa Bonita do Sul	lagoa-bonita-do-sul	2013-09-03 15:30:12.389704	1
5840	21	Lagoa dos Três Cantos	lagoa-dos-tres-cantos	2013-09-03 15:30:12.389704	1
5841	21	Lagoa Vermelha	lagoa-vermelha	2013-09-03 15:30:12.389704	1
5842	21	Lagoão	lagoao	2013-09-03 15:30:12.389704	1
5843	21	Lajeado	lajeado	2013-09-03 15:30:12.389704	1
5844	21	Lajeado do Bugre	lajeado-do-bugre	2013-09-03 15:30:12.389704	1
5845	21	Lavras do Sul	lavras-do-sul	2013-09-03 15:30:12.389704	1
5846	21	Liberato Salzano	liberato-salzano	2013-09-03 15:30:12.389704	1
5847	21	Lindolfo Collor	lindolfo-collor	2013-09-03 15:30:12.389704	1
5848	21	Linha Nova	linha-nova	2013-09-03 15:30:12.389704	1
5849	21	Maçambara	macambara	2013-09-03 15:30:12.389704	1
5850	21	Machadinho	machadinho	2013-09-03 15:30:12.389704	1
5851	21	Mampituba	mampituba	2013-09-03 15:30:12.389704	1
5852	21	Manoel Viana	manoel-viana	2013-09-03 15:30:12.389704	1
5853	21	Maquiné	maquine	2013-09-03 15:30:12.389704	1
5854	21	Maratá	marata	2013-09-03 15:30:12.389704	1
5855	21	Marau	marau	2013-09-03 15:30:12.389704	1
5856	21	Marcelino Ramos	marcelino-ramos	2013-09-03 15:30:12.389704	1
5857	21	Mariana Pimentel	mariana-pimentel	2013-09-03 15:30:12.389704	1
5858	21	Mariano Moro	mariano-moro	2013-09-03 15:30:12.389704	1
5859	21	Marques de Souza	marques-de-souza	2013-09-03 15:30:12.389704	1
5860	21	Mata	mata	2013-09-03 15:30:12.389704	1
5861	21	Mato Castelhano	mato-castelhano	2013-09-03 15:30:12.389704	1
5862	21	Mato Leitão	mato-leitao	2013-09-03 15:30:12.389704	1
5863	21	Mato Queimado	mato-queimado	2013-09-03 15:30:12.389704	1
5864	21	Maximiliano de Almeida	maximiliano-de-almeida	2013-09-03 15:30:12.389704	1
5865	21	Minas do Leão	minas-do-leao	2013-09-03 15:30:12.389704	1
5866	21	Miraguaí	miraguai	2013-09-03 15:30:12.389704	1
5867	21	Montauri	montauri	2013-09-03 15:30:12.389704	1
5868	21	Monte Alegre dos Campos	monte-alegre-dos-campos	2013-09-03 15:30:12.389704	1
6187	23	Amajari	amajari	2013-09-03 15:30:12.389704	1
5869	21	Monte Belo do Sul	monte-belo-do-sul	2013-09-03 15:30:12.389704	1
5870	21	Montenegro	montenegro	2013-09-03 15:30:12.389704	1
5871	21	Mormaço	mormaco	2013-09-03 15:30:12.389704	1
5872	21	Morrinhos do Sul	morrinhos-do-sul	2013-09-03 15:30:12.389704	1
5873	21	Morro Redondo	morro-redondo	2013-09-03 15:30:12.389704	1
5874	21	Morro Reuter	morro-reuter	2013-09-03 15:30:12.389704	1
5875	21	Mostardas	mostardas	2013-09-03 15:30:12.389704	1
5876	21	Muçum	mucum	2013-09-03 15:30:12.389704	1
5877	21	Muitos Capões	muitos-capoes	2013-09-03 15:30:12.389704	1
5878	21	Muliterno	muliterno	2013-09-03 15:30:12.389704	1
5879	21	Não-Me-Toque	nao-me-toque	2013-09-03 15:30:12.389704	1
5880	21	Nicolau Vergueiro	nicolau-vergueiro	2013-09-03 15:30:12.389704	1
5881	21	Nonoai	nonoai	2013-09-03 15:30:12.389704	1
5882	21	Nova Alvorada	nova-alvorada	2013-09-03 15:30:12.389704	1
5883	21	Nova Araçá	nova-araca	2013-09-03 15:30:12.389704	1
5884	21	Nova Bassano	nova-bassano	2013-09-03 15:30:12.389704	1
5885	21	Nova Boa Vista	nova-boa-vista	2013-09-03 15:30:12.389704	1
5886	21	Nova Bréscia	nova-brescia	2013-09-03 15:30:12.389704	1
5887	21	Nova Candelária	nova-candelaria	2013-09-03 15:30:12.389704	1
5888	21	Nova Esperança do Sul	nova-esperanca-do-sul	2013-09-03 15:30:12.389704	1
5889	21	Nova Hartz	nova-hartz	2013-09-03 15:30:12.389704	1
5890	21	Nova Pádua	nova-padua	2013-09-03 15:30:12.389704	1
5891	21	Nova Palma	nova-palma	2013-09-03 15:30:12.389704	1
5892	21	Nova Petrópolis	nova-petropolis	2013-09-03 15:30:12.389704	1
5893	21	Nova Prata	nova-prata	2013-09-03 15:30:12.389704	1
5894	21	Nova Ramada	nova-ramada	2013-09-03 15:30:12.389704	1
5895	21	Nova Roma do Sul	nova-roma-do-sul	2013-09-03 15:30:12.389704	1
5896	21	Nova Santa Rita	nova-santa-rita	2013-09-03 15:30:12.389704	1
5897	21	Novo Barreiro	novo-barreiro	2013-09-03 15:30:12.389704	1
5898	21	Novo Cabrais	novo-cabrais	2013-09-03 15:30:12.389704	1
5899	21	Novo Hamburgo	novo-hamburgo	2013-09-03 15:30:12.389704	1
5900	21	Novo Machado	novo-machado	2013-09-03 15:30:12.389704	1
5901	21	Novo Tiradentes	novo-tiradentes	2013-09-03 15:30:12.389704	1
5902	21	Novo Xingu	novo-xingu	2013-09-03 15:30:12.389704	1
5903	21	Osório	osorio	2013-09-03 15:30:12.389704	1
5904	21	Paim Filho	paim-filho	2013-09-03 15:30:12.389704	1
5905	21	Palmares do Sul	palmares-do-sul	2013-09-03 15:30:12.389704	1
5906	21	Palmeira das Missões	palmeira-das-missoes	2013-09-03 15:30:12.389704	1
5907	21	Palmitinho	palmitinho	2013-09-03 15:30:12.389704	1
5908	21	Panambi	panambi	2013-09-03 15:30:12.389704	1
5909	21	Pantano Grande	pantano-grande	2013-09-03 15:30:12.389704	1
5910	21	Paraí	parai	2013-09-03 15:30:12.389704	1
5911	21	Paraíso do Sul	paraiso-do-sul	2013-09-03 15:30:12.389704	1
5912	21	Pareci Novo	pareci-novo	2013-09-03 15:30:12.389704	1
5913	21	Parobé	parobe	2013-09-03 15:30:12.389704	1
5914	21	Passa Sete	passa-sete	2013-09-03 15:30:12.389704	1
5915	21	Passo do Sobrado	passo-do-sobrado	2013-09-03 15:30:12.389704	1
5916	21	Passo Fundo	passo-fundo	2013-09-03 15:30:12.389704	1
5917	21	Paulo Bento	paulo-bento	2013-09-03 15:30:12.389704	1
5918	21	Paverama	paverama	2013-09-03 15:30:12.389704	1
5919	21	Pedras Altas	pedras-altas	2013-09-03 15:30:12.389704	1
5920	21	Pedro Osório	pedro-osorio	2013-09-03 15:30:12.389704	1
5921	21	Pejuçara	pejucara	2013-09-03 15:30:12.389704	1
5922	21	Pelotas	pelotas	2013-09-03 15:30:12.389704	1
5923	21	Picada Café	picada-cafe	2013-09-03 15:30:12.389704	1
5924	21	Pinhal	pinhal	2013-09-03 15:30:12.389704	1
5925	21	Pinhal da Serra	pinhal-da-serra	2013-09-03 15:30:12.389704	1
5926	21	Pinhal Grande	pinhal-grande	2013-09-03 15:30:12.389704	1
5927	21	Pinheirinho do Vale	pinheirinho-do-vale	2013-09-03 15:30:12.389704	1
5928	21	Pinheiro Machado	pinheiro-machado	2013-09-03 15:30:12.389704	1
5929	21	Pirapó	pirapo	2013-09-03 15:30:12.389704	1
5930	21	Piratini	piratini	2013-09-03 15:30:12.389704	1
5931	21	Planalto	planalto	2013-09-03 15:30:12.389704	1
5932	21	Poço das Antas	poco-das-antas	2013-09-03 15:30:12.389704	1
5933	21	Pontão	pontao	2013-09-03 15:30:12.389704	1
5934	21	Ponte Preta	ponte-preta	2013-09-03 15:30:12.389704	1
5935	21	Portão	portao	2013-09-03 15:30:12.389704	1
5936	21	Porto Alegre	porto-alegre	2013-09-03 15:30:12.389704	1
5937	21	Porto Lucena	porto-lucena	2013-09-03 15:30:12.389704	1
5938	21	Porto Mauá	porto-maua	2013-09-03 15:30:12.389704	1
5939	21	Porto Vera Cruz	porto-vera-cruz	2013-09-03 15:30:12.389704	1
5940	21	Porto Xavier	porto-xavier	2013-09-03 15:30:12.389704	1
5941	21	Pouso Novo	pouso-novo	2013-09-03 15:30:12.389704	1
5942	21	Presidente Lucena	presidente-lucena	2013-09-03 15:30:12.389704	1
5943	21	Progresso	progresso	2013-09-03 15:30:12.389704	1
5944	21	Protásio Alves	protasio-alves	2013-09-03 15:30:12.389704	1
5945	21	Putinga	putinga	2013-09-03 15:30:12.389704	1
5946	21	Quaraí	quarai	2013-09-03 15:30:12.389704	1
5947	21	Quatro Irmãos	quatro-irmaos	2013-09-03 15:30:12.389704	1
5948	21	Quevedos	quevedos	2013-09-03 15:30:12.389704	1
5949	21	Quinze de Novembro	quinze-de-novembro	2013-09-03 15:30:12.389704	1
5950	21	Redentora	redentora	2013-09-03 15:30:12.389704	1
5951	21	Relvado	relvado	2013-09-03 15:30:12.389704	1
5952	21	Restinga Seca	restinga-seca	2013-09-03 15:30:12.389704	1
5953	21	Rio dos Índios	rio-dos-indios	2013-09-03 15:30:12.389704	1
5954	21	Rio Grande	rio-grande	2013-09-03 15:30:12.389704	1
5955	21	Rio Pardo	rio-pardo	2013-09-03 15:30:12.389704	1
5956	21	Riozinho	riozinho	2013-09-03 15:30:12.389704	1
5957	21	Roca Sales	roca-sales	2013-09-03 15:30:12.389704	1
5958	21	Rodeio Bonito	rodeio-bonito	2013-09-03 15:30:12.389704	1
5959	21	Rolador	rolador	2013-09-03 15:30:12.389704	1
5960	21	Rolante	rolante	2013-09-03 15:30:12.389704	1
5961	21	Ronda Alta	ronda-alta	2013-09-03 15:30:12.389704	1
5962	21	Rondinha	rondinha	2013-09-03 15:30:12.389704	1
5963	21	Roque Gonzales	roque-gonzales	2013-09-03 15:30:12.389704	1
5964	21	Rosário do Sul	rosario-do-sul	2013-09-03 15:30:12.389704	1
5965	21	Sagrada Família	sagrada-familia	2013-09-03 15:30:12.389704	1
5966	21	Saldanha Marinho	saldanha-marinho	2013-09-03 15:30:12.389704	1
5967	21	Salto do Jacuí	salto-do-jacui	2013-09-03 15:30:12.389704	1
5968	21	Salvador das Missões	salvador-das-missoes	2013-09-03 15:30:12.389704	1
5969	21	Salvador do Sul	salvador-do-sul	2013-09-03 15:30:12.389704	1
5970	21	Sananduva	sananduva	2013-09-03 15:30:12.389704	1
5971	21	Santa Bárbara do Sul	santa-barbara-do-sul	2013-09-03 15:30:12.389704	1
5972	21	Santa Cecília do Sul	santa-cecilia-do-sul	2013-09-03 15:30:12.389704	1
5973	21	Santa Clara do Sul	santa-clara-do-sul	2013-09-03 15:30:12.389704	1
5974	21	Santa Cruz do Sul	santa-cruz-do-sul	2013-09-03 15:30:12.389704	1
5975	21	Santa Margarida do Sul	santa-margarida-do-sul	2013-09-03 15:30:12.389704	1
5976	21	Santa Maria	santa-maria	2013-09-03 15:30:12.389704	1
5977	21	Santa Maria do Herval	santa-maria-do-herval	2013-09-03 15:30:12.389704	1
5978	21	Santa Rosa	santa-rosa	2013-09-03 15:30:12.389704	1
5979	21	Santa Tereza	santa-tereza	2013-09-03 15:30:12.389704	1
5980	21	Santa Vitória do Palmar	santa-vitoria-do-palmar	2013-09-03 15:30:12.389704	1
5981	21	Santana da Boa Vista	santana-da-boa-vista	2013-09-03 15:30:12.389704	1
5982	21	Santana do Livramento	santana-do-livramento	2013-09-03 15:30:12.389704	1
5983	21	Santiago	santiago	2013-09-03 15:30:12.389704	1
5984	21	Santo Ângelo	santo-angelo	2013-09-03 15:30:12.389704	1
5985	21	Santo Antônio da Patrulha	santo-antonio-da-patrulha	2013-09-03 15:30:12.389704	1
5986	21	Santo Antônio das Missões	santo-antonio-das-missoes	2013-09-03 15:30:12.389704	1
5987	21	Santo Antônio do Palma	santo-antonio-do-palma	2013-09-03 15:30:12.389704	1
5988	21	Santo Antônio do Planalto	santo-antonio-do-planalto	2013-09-03 15:30:12.389704	1
5989	21	Santo Augusto	santo-augusto	2013-09-03 15:30:12.389704	1
5990	21	Santo Cristo	santo-cristo	2013-09-03 15:30:12.389704	1
5991	21	Santo Expedito do Sul	santo-expedito-do-sul	2013-09-03 15:30:12.389704	1
5992	21	São Borja	sao-borja	2013-09-03 15:30:12.389704	1
5993	21	São Domingos do Sul	sao-domingos-do-sul	2013-09-03 15:30:12.389704	1
5994	21	São Francisco de Assis	sao-francisco-de-assis	2013-09-03 15:30:12.389704	1
5995	21	São Francisco de Paula	sao-francisco-de-paula	2013-09-03 15:30:12.389704	1
5996	21	São Gabriel	sao-gabriel	2013-09-03 15:30:12.389704	1
5997	21	São Jerônimo	sao-jeronimo	2013-09-03 15:30:12.389704	1
5998	21	São João da Urtiga	sao-joao-da-urtiga	2013-09-03 15:30:12.389704	1
5999	21	São João do Polêsine	sao-joao-do-polesine	2013-09-03 15:30:12.389704	1
6000	21	São Jorge	sao-jorge	2013-09-03 15:30:12.389704	1
6001	21	São José das Missões	sao-jose-das-missoes	2013-09-03 15:30:12.389704	1
6002	21	São José do Herval	sao-jose-do-herval	2013-09-03 15:30:12.389704	1
6003	21	São José do Hortêncio	sao-jose-do-hortencio	2013-09-03 15:30:12.389704	1
6004	21	São José do Inhacorá	sao-jose-do-inhacora	2013-09-03 15:30:12.389704	1
6005	21	São José do Norte	sao-jose-do-norte	2013-09-03 15:30:12.389704	1
6006	21	São José do Ouro	sao-jose-do-ouro	2013-09-03 15:30:12.389704	1
6007	21	São José do Sul	sao-jose-do-sul	2013-09-03 15:30:12.389704	1
6008	21	São José dos Ausentes	sao-jose-dos-ausentes	2013-09-03 15:30:12.389704	1
6009	21	São Leopoldo	sao-leopoldo	2013-09-03 15:30:12.389704	1
6010	21	São Lourenço do Sul	sao-lourenco-do-sul	2013-09-03 15:30:12.389704	1
6011	21	São Luiz Gonzaga	sao-luiz-gonzaga	2013-09-03 15:30:12.389704	1
6012	21	São Marcos	sao-marcos	2013-09-03 15:30:12.389704	1
6013	21	São Martinho	sao-martinho	2013-09-03 15:30:12.389704	1
6014	21	São Martinho da Serra	sao-martinho-da-serra	2013-09-03 15:30:12.389704	1
6015	21	São Miguel das Missões	sao-miguel-das-missoes	2013-09-03 15:30:12.389704	1
6016	21	São Nicolau	sao-nicolau	2013-09-03 15:30:12.389704	1
6017	21	São Paulo das Missões	sao-paulo-das-missoes	2013-09-03 15:30:12.389704	1
6018	21	São Pedro da Serra	sao-pedro-da-serra	2013-09-03 15:30:12.389704	1
6019	21	São Pedro das Missões	sao-pedro-das-missoes	2013-09-03 15:30:12.389704	1
6020	21	São\nPedro do Butiá	sao-pedro-do-butia	2013-09-03 15:30:12.389704	1
6021	21	São Pedro do Sul	sao-pedro-do-sul	2013-09-03 15:30:12.389704	1
6022	21	São Sebastião do Caí	sao-sebastiao-do-cai	2013-09-03 15:30:12.389704	1
6023	21	São Sepé	sao-sepe	2013-09-03 15:30:12.389704	1
6024	21	São Valentim	sao-valentim	2013-09-03 15:30:12.389704	1
6025	21	São Valentim do Sul	sao-valentim-do-sul	2013-09-03 15:30:12.389704	1
6026	21	São Valério do Sul	sao-valerio-do-sul	2013-09-03 15:30:12.389704	1
6027	21	São Vendelino	sao-vendelino	2013-09-03 15:30:12.389704	1
6028	21	São Vicente do Sul	sao-vicente-do-sul	2013-09-03 15:30:12.389704	1
6029	21	Sapiranga	sapiranga	2013-09-03 15:30:12.389704	1
6030	21	Sapucaia do Sul	sapucaia-do-sul	2013-09-03 15:30:12.389704	1
6031	21	Sarandi	sarandi	2013-09-03 15:30:12.389704	1
6032	21	Seberi	seberi	2013-09-03 15:30:12.389704	1
6033	21	Sede Nova	sede-nova	2013-09-03 15:30:12.389704	1
6034	21	Segredo	segredo	2013-09-03 15:30:12.389704	1
6035	21	Selbach	selbach	2013-09-03 15:30:12.389704	1
6036	21	Senador Salgado Filho	senador-salgado-filho	2013-09-03 15:30:12.389704	1
6037	21	Sentinela do Sul	sentinela-do-sul	2013-09-03 15:30:12.389704	1
6038	21	Serafina Corrêa	serafina-correa	2013-09-03 15:30:12.389704	1
6039	21	Sério	serio	2013-09-03 15:30:12.389704	1
6040	21	Sertão	sertao	2013-09-03 15:30:12.389704	1
6041	21	Sertão Santana	sertao-santana	2013-09-03 15:30:12.389704	1
6042	21	Sete de Setembro	sete-de-setembro	2013-09-03 15:30:12.389704	1
6043	21	Severiano de Almeida	severiano-de-almeida	2013-09-03 15:30:12.389704	1
6044	21	Silveira Martins	silveira-martins	2013-09-03 15:30:12.389704	1
6045	21	Sinimbu	sinimbu	2013-09-03 15:30:12.389704	1
6046	21	Sobradinho	sobradinho	2013-09-03 15:30:12.389704	1
6047	21	Soledade	soledade	2013-09-03 15:30:12.389704	1
6048	21	Tabaí	tabai	2013-09-03 15:30:12.389704	1
6049	21	Tapejara	tapejara	2013-09-03 15:30:12.389704	1
6050	21	Tapera	tapera	2013-09-03 15:30:12.389704	1
6051	21	Tapes	tapes	2013-09-03 15:30:12.389704	1
6052	21	Taquara	taquara	2013-09-03 15:30:12.389704	1
6053	21	Taquari	taquari	2013-09-03 15:30:12.389704	1
6054	21	Taquaruçu do Sul	taquarucu-do-sul	2013-09-03 15:30:12.389704	1
6055	21	Tavares	tavares	2013-09-03 15:30:12.389704	1
6056	21	Tenente Portela	tenente-portela	2013-09-03 15:30:12.389704	1
6057	21	Terra de Areia	terra-de-areia	2013-09-03 15:30:12.389704	1
6058	21	Teutônia	teutonia	2013-09-03 15:30:12.389704	1
6059	21	Tio Hugo	tio-hugo	2013-09-03 15:30:12.389704	1
6060	21	Tiradentes do Sul	tiradentes-do-sul	2013-09-03 15:30:12.389704	1
6061	21	Toropi	toropi	2013-09-03 15:30:12.389704	1
6062	21	Torres	torres	2013-09-03 15:30:12.389704	1
8660	25	Santos	santos	2013-09-03 15:30:12.389704	1
6063	21	Tramandaí	tramandai	2013-09-03 15:30:12.389704	1
6064	21	Travesseiro	travesseiro	2013-09-03 15:30:12.389704	1
6065	21	Três Arroios	tres-arroios	2013-09-03 15:30:12.389704	1
6066	21	Três Cachoeiras	tres-cachoeiras	2013-09-03 15:30:12.389704	1
6067	21	Três Coroas	tres-coroas	2013-09-03 15:30:12.389704	1
6068	21	Três de Maio	tres-de-maio	2013-09-03 15:30:12.389704	1
6069	21	Três Forquilhas	tres-forquilhas	2013-09-03 15:30:12.389704	1
6070	21	Três Palmeiras	tres-palmeiras	2013-09-03 15:30:12.389704	1
6071	21	Três Passos	tres-passos	2013-09-03 15:30:12.389704	1
6072	21	Trindade do Sul	trindade-do-sul	2013-09-03 15:30:12.389704	1
6073	21	Triunfo	triunfo	2013-09-03 15:30:12.389704	1
6074	21	Tucunduva	tucunduva	2013-09-03 15:30:12.389704	1
6075	21	Tunas	tunas	2013-09-03 15:30:12.389704	1
6076	21	Tupanci do Sul	tupanci-do-sul	2013-09-03 15:30:12.389704	1
6077	21	Tupanciretã	tupancireta	2013-09-03 15:30:12.389704	1
6078	21	Tupandi	tupandi	2013-09-03 15:30:12.389704	1
6079	21	Tuparendi	tuparendi	2013-09-03 15:30:12.389704	1
6080	21	Turuçu	turucu	2013-09-03 15:30:12.389704	1
6081	21	Ubiretama	ubiretama	2013-09-03 15:30:12.389704	1
6082	21	União da Serra	uniao-da-serra	2013-09-03 15:30:12.389704	1
6083	21	Unistalda	unistalda	2013-09-03 15:30:12.389704	1
6084	21	Uruguaiana	uruguaiana	2013-09-03 15:30:12.389704	1
6085	21	Vacaria	vacaria	2013-09-03 15:30:12.389704	1
6086	21	Vale do Sol	vale-do-sol	2013-09-03 15:30:12.389704	1
6087	21	Vale Real	vale-real	2013-09-03 15:30:12.389704	1
6088	21	Vale Verde	vale-verde	2013-09-03 15:30:12.389704	1
6089	21	Vanini	vanini	2013-09-03 15:30:12.389704	1
6090	21	Venâncio Aires	venancio-aires	2013-09-03 15:30:12.389704	1
6091	21	Vera Cruz	vera-cruz	2013-09-03 15:30:12.389704	1
6092	21	Veranópolis	veranopolis	2013-09-03 15:30:12.389704	1
6093	21	Vespasiano Correa	vespasiano-correa	2013-09-03 15:30:12.389704	1
6094	21	Viadutos	viadutos	2013-09-03 15:30:12.389704	1
6095	21	Viamão	viamao	2013-09-03 15:30:12.389704	1
6096	21	Vicente Dutra	vicente-dutra	2013-09-03 15:30:12.389704	1
6097	21	Victor Graeff	victor-graeff	2013-09-03 15:30:12.389704	1
6098	21	Vila Flores	vila-flores	2013-09-03 15:30:12.389704	1
6099	21	Vila Lângaro	vila-langaro	2013-09-03 15:30:12.389704	1
6100	21	Vila Maria	vila-maria	2013-09-03 15:30:12.389704	1
6101	21	Vila Nova do Sul	vila-nova-do-sul	2013-09-03 15:30:12.389704	1
6102	21	Vista Alegre	vista-alegre	2013-09-03 15:30:12.389704	1
6103	21	Vista Alegre do Prata	vista-alegre-do-prata	2013-09-03 15:30:12.389704	1
6104	21	Vista Gaúcha	vista-gaucha	2013-09-03 15:30:12.389704	1
6105	21	Vitória das Missões	vitoria-das-missoes	2013-09-03 15:30:12.389704	1
6106	21	Westfália	westfalia	2013-09-03 15:30:12.389704	1
6107	21	Xangri-lá	xangri-la	2013-09-03 15:30:12.389704	1
6123	22	Alta Floresta d`Oeste	alta-floresta-d-oeste	2013-09-03 15:30:12.389704	1
6124	22	Alto Alegre dos Parecis	alto-alegre-dos-parecis	2013-09-03 15:30:12.389704	1
6125	22	Alto Paraíso	alto-paraiso	2013-09-03 15:30:12.389704	1
6126	22	Alvorada d`Oeste	alvorada-d-oeste	2013-09-03 15:30:12.389704	1
6127	22	Ariquemes	ariquemes	2013-09-03 15:30:12.389704	1
6128	22	Buritis	buritis	2013-09-03 15:30:12.389704	1
6129	22	Cabixi	cabixi	2013-09-03 15:30:12.389704	1
6130	22	Cacaulândia	cacaulandia	2013-09-03 15:30:12.389704	1
6131	22	Cacoal	cacoal	2013-09-03 15:30:12.389704	1
6132	22	Campo Novo de Rondônia	campo-novo-de-rondonia	2013-09-03 15:30:12.389704	1
6133	22	Candeias do Jamari	candeias-do-jamari	2013-09-03 15:30:12.389704	1
6134	22	Castanheiras	castanheiras	2013-09-03 15:30:12.389704	1
6135	22	Cerejeiras	cerejeiras	2013-09-03 15:30:12.389704	1
6136	22	Chupinguaia	chupinguaia	2013-09-03 15:30:12.389704	1
6137	22	Colorado do Oeste	colorado-do-oeste	2013-09-03 15:30:12.389704	1
6138	22	Corumbiara	corumbiara	2013-09-03 15:30:12.389704	1
6139	22	Costa Marques	costa-marques	2013-09-03 15:30:12.389704	1
6140	22	Cujubim	cujubim	2013-09-03 15:30:12.389704	1
6141	22	Espigão d`Oeste	espigao-d-oeste	2013-09-03 15:30:12.389704	1
6142	22	Governador Jorge Teixeira	governador-jorge-teixeira	2013-09-03 15:30:12.389704	1
6143	22	Guajará-Mirim	guajara-mirim	2013-09-03 15:30:12.389704	1
6144	22	Itapuã do Oeste	itapua-do-oeste	2013-09-03 15:30:12.389704	1
6145	22	Jaru	jaru	2013-09-03 15:30:12.389704	1
6146	22	Ji-Paraná	ji-parana	2013-09-03 15:30:12.389704	1
6147	22	Machadinho d`Oeste	machadinho-d-oeste	2013-09-03 15:30:12.389704	1
6148	22	Ministro Andreazza	ministro-andreazza	2013-09-03 15:30:12.389704	1
6149	22	Mirante da Serra	mirante-da-serra	2013-09-03 15:30:12.389704	1
6150	22	Monte Negro	monte-negro	2013-09-03 15:30:12.389704	1
6151	22	Nova Brasilândia d`Oeste	nova-brasilandia-d-oeste	2013-09-03 15:30:12.389704	1
6152	22	Nova Mamoré	nova-mamore	2013-09-03 15:30:12.389704	1
6153	22	Nova União	nova-uniao	2013-09-03 15:30:12.389704	1
6154	22	Novo Horizonte do Oeste	novo-horizonte-do-oeste	2013-09-03 15:30:12.389704	1
6155	22	Ouro Preto do Oeste	ouro-preto-do-oeste	2013-09-03 15:30:12.389704	1
6156	22	Parecis	parecis	2013-09-03 15:30:12.389704	1
6157	22	Pimenta Bueno	pimenta-bueno	2013-09-03 15:30:12.389704	1
6158	22	Pimenteiras do Oeste	pimenteiras-do-oeste	2013-09-03 15:30:12.389704	1
6159	22	Porto Velho	porto-velho	2013-09-03 15:30:12.389704	1
6160	22	Presidente Médici	presidente-medici	2013-09-03 15:30:12.389704	1
6161	22	Primavera de Rondônia	primavera-de-rondonia	2013-09-03 15:30:12.389704	1
6162	22	Rio Crespo	rio-crespo	2013-09-03 15:30:12.389704	1
6163	22	Rolim de Moura	rolim-de-moura	2013-09-03 15:30:12.389704	1
6164	22	Santa Luzia d`Oeste	santa-luzia-d-oeste	2013-09-03 15:30:12.389704	1
6165	22	São Felipe d`Oeste	sao-felipe-d-oeste	2013-09-03 15:30:12.389704	1
6166	22	São Francisco do Guaporé	sao-francisco-do-guapore	2013-09-03 15:30:12.389704	1
6167	22	São Miguel do Guaporé	sao-miguel-do-guapore	2013-09-03 15:30:12.389704	1
6168	22	Seringueiras	seringueiras	2013-09-03 15:30:12.389704	1
6169	22	Teixeirópolis	teixeiropolis	2013-09-03 15:30:12.389704	1
6170	22	Theobroma	theobroma	2013-09-03 15:30:12.389704	1
6171	22	Urupá	urupa	2013-09-03 15:30:12.389704	1
6172	22	Vale do Anari	vale-do-anari	2013-09-03 15:30:12.389704	1
6173	22	Vale do Paraíso	vale-do-paraiso	2013-09-03 15:30:12.389704	1
6174	22	Vilhena	vilhena	2013-09-03 15:30:12.389704	1
6186	23	Alto Alegre	alto-alegre	2013-09-03 15:30:12.389704	1
6188	23	Boa Vista	boa-vista	2013-09-03 15:30:12.389704	1
6189	23	Bonfim	bonfim	2013-09-03 15:30:12.389704	1
6190	23	Cantá	canta	2013-09-03 15:30:12.389704	1
6191	23	Caracaraí	caracarai	2013-09-03 15:30:12.389704	1
6192	23	Caroebe	caroebe	2013-09-03 15:30:12.389704	1
6193	23	Iracema	iracema	2013-09-03 15:30:12.389704	1
6194	23	Mucajaí	mucajai	2013-09-03 15:30:12.389704	1
6195	23	Normandia	normandia	2013-09-03 15:30:12.389704	1
6196	23	Pacaraima	pacaraima	2013-09-03 15:30:12.389704	1
6197	23	Rorainópolis	rorainopolis	2013-09-03 15:30:12.389704	1
6198	23	São João da Baliza	sao-joao-da-baliza	2013-09-03 15:30:12.389704	1
6199	23	São Luiz	sao-luiz	2013-09-03 15:30:12.389704	1
6200	23	Uiramutã	uiramuta	2013-09-03 15:30:12.389704	1
6201	24	Abdon Batista	abdon-batista	2013-09-03 15:30:12.389704	1
6202	24	Abelardo Luz	abelardo-luz	2013-09-03 15:30:12.389704	1
6203	24	Agrolândia	agrolandia	2013-09-03 15:30:12.389704	1
6204	24	Agronômica	agronomica	2013-09-03 15:30:12.389704	1
6205	24	Água Doce	agua-doce	2013-09-03 15:30:12.389704	1
6206	24	Águas de Chapecó	aguas-de-chapeco	2013-09-03 15:30:12.389704	1
6207	24	Águas Frias	aguas-frias	2013-09-03 15:30:12.389704	1
6208	24	Águas Mornas	aguas-mornas	2013-09-03 15:30:12.389704	1
6209	24	Alfredo Wagner	alfredo-wagner	2013-09-03 15:30:12.389704	1
6210	24	Alto Bela Vista	alto-bela-vista	2013-09-03 15:30:12.389704	1
6211	24	Anchieta	anchieta	2013-09-03 15:30:12.389704	1
6212	24	Angelina	angelina	2013-09-03 15:30:12.389704	1
6213	24	Anita Garibaldi	anita-garibaldi	2013-09-03 15:30:12.389704	1
6214	24	Anitápolis	anitapolis	2013-09-03 15:30:12.389704	1
6215	24	Antônio Carlos	antonio-carlos	2013-09-03 15:30:12.389704	1
6216	24	Apiúna	apiuna	2013-09-03 15:30:12.389704	1
6217	24	Arabutã	arabuta	2013-09-03 15:30:12.389704	1
6218	24	Araquari	araquari	2013-09-03 15:30:12.389704	1
6219	24	Araranguá	ararangua	2013-09-03 15:30:12.389704	1
6220	24	Armazém	armazem	2013-09-03 15:30:12.389704	1
6221	24	Arroio Trinta	arroio-trinta	2013-09-03 15:30:12.389704	1
6222	24	Arvoredo	arvoredo	2013-09-03 15:30:12.389704	1
6223	24	Ascurra	ascurra	2013-09-03 15:30:12.389704	1
6224	24	Atalanta	atalanta	2013-09-03 15:30:12.389704	1
6225	24	Aurora	aurora	2013-09-03 15:30:12.389704	1
6226	24	Balneário Arroio do Silva	balneario-arroio-do-silva	2013-09-03 15:30:12.389704	1
6227	24	Balneário Barra do Sul	balneario-barra-do-sul	2013-09-03 15:30:12.389704	1
6228	24	Balneário Camboriú	balneario-camboriu	2013-09-03 15:30:12.389704	1
6229	24	Balneário Gaivota	balneario-gaivota	2013-09-03 15:30:12.389704	1
6230	24	Bandeirante	bandeirante	2013-09-03 15:30:12.389704	1
6231	24	Barra Bonita	barra-bonita	2013-09-03 15:30:12.389704	1
6232	24	Barra Velha	barra-velha	2013-09-03 15:30:12.389704	1
6233	24	Bela Vista do Toldo	bela-vista-do-toldo	2013-09-03 15:30:12.389704	1
6234	24	Belmonte	belmonte	2013-09-03 15:30:12.389704	1
6235	24	Benedito Novo	benedito-novo	2013-09-03 15:30:12.389704	1
6236	24	Biguaçu	biguacu	2013-09-03 15:30:12.389704	1
6237	24	Blumenau	blumenau	2013-09-03 15:30:12.389704	1
6238	24	Bocaina do Sul	bocaina-do-sul	2013-09-03 15:30:12.389704	1
6239	24	Bom Jardim da Serra	bom-jardim-da-serra	2013-09-03 15:30:12.389704	1
6240	24	Bom Jesus	bom-jesus	2013-09-03 15:30:12.389704	1
6241	24	Bom Jesus do Oeste	bom-jesus-do-oeste	2013-09-03 15:30:12.389704	1
6242	24	Bom Retiro	bom-retiro	2013-09-03 15:30:12.389704	1
6243	24	Bombinhas	bombinhas	2013-09-03 15:30:12.389704	1
6244	24	Botuverá	botuvera	2013-09-03 15:30:12.389704	1
6245	24	Braço do Norte	braco-do-norte	2013-09-03 15:30:12.389704	1
6246	24	Braço do Trombudo	braco-do-trombudo	2013-09-03 15:30:12.389704	1
6247	24	Brunópolis	brunopolis	2013-09-03 15:30:12.389704	1
6248	24	Brusque	brusque	2013-09-03 15:30:12.389704	1
6249	24	Caçador	cacador	2013-09-03 15:30:12.389704	1
6250	24	Caibi	caibi	2013-09-03 15:30:12.389704	1
6251	24	Calmon	calmon	2013-09-03 15:30:12.389704	1
6252	24	Camboriú	camboriu	2013-09-03 15:30:12.389704	1
6253	24	Campo Alegre	campo-alegre	2013-09-03 15:30:12.389704	1
6254	24	Campo Belo do Sul	campo-belo-do-sul	2013-09-03 15:30:12.389704	1
6255	24	Campo Erê	campo-ere	2013-09-03 15:30:12.389704	1
6256	24	Campos Novos	campos-novos	2013-09-03 15:30:12.389704	1
6257	24	Canelinha	canelinha	2013-09-03 15:30:12.389704	1
6258	24	Canoinhas	canoinhas	2013-09-03 15:30:12.389704	1
6259	24	Capão Alto	capao-alto	2013-09-03 15:30:12.389704	1
6260	24	Capinzal	capinzal	2013-09-03 15:30:12.389704	1
6261	24	Capivari de Baixo	capivari-de-baixo	2013-09-03 15:30:12.389704	1
6262	24	Catanduvas	catanduvas	2013-09-03 15:30:12.389704	1
6263	24	Caxambu do Sul	caxambu-do-sul	2013-09-03 15:30:12.389704	1
6264	24	Celso Ramos	celso-ramos	2013-09-03 15:30:12.389704	1
6265	24	Cerro Negro	cerro-negro	2013-09-03 15:30:12.389704	1
6266	24	Chapadão do Lageado	chapadao-do-lageado	2013-09-03 15:30:12.389704	1
6267	24	Chapecó	chapeco	2013-09-03 15:30:12.389704	1
6268	24	Cocal do Sul	cocal-do-sul	2013-09-03 15:30:12.389704	1
6269	24	Concórdia	concordia	2013-09-03 15:30:12.389704	1
6270	24	Cordilheira Alta	cordilheira-alta	2013-09-03 15:30:12.389704	1
6271	24	Coronel Freitas	coronel-freitas	2013-09-03 15:30:12.389704	1
6272	24	Coronel Martins	coronel-martins	2013-09-03 15:30:12.389704	1
6273	24	Correia Pinto	correia-pinto	2013-09-03 15:30:12.389704	1
6274	24	Corupá	corupa	2013-09-03 15:30:12.389704	1
6275	24	Criciúma	criciuma	2013-09-03 15:30:12.389704	1
6276	24	Cunha Porã	cunha-pora	2013-09-03 15:30:12.389704	1
6277	24	Cunhataí	cunhatai	2013-09-03 15:30:12.389704	1
6278	24	Curitibanos	curitibanos	2013-09-03 15:30:12.389704	1
6279	24	Descanso	descanso	2013-09-03 15:30:12.389704	1
6280	24	Dionísio Cerqueira	dionisio-cerqueira	2013-09-03 15:30:12.389704	1
6281	24	Dona Emma	dona-emma	2013-09-03 15:30:12.389704	1
6282	24	Doutor Pedrinho	doutor-pedrinho	2013-09-03 15:30:12.389704	1
6283	24	Entre Rios	entre-rios	2013-09-03 15:30:12.389704	1
6284	24	Ermo	ermo	2013-09-03 15:30:12.389704	1
6285	24	Erval Velho	erval-velho	2013-09-03 15:30:12.389704	1
6286	24	Faxinal dos Guedes	faxinal-dos-guedes	2013-09-03 15:30:12.389704	1
6287	24	Flor do Sertão	flor-do-sertao	2013-09-03 15:30:12.389704	1
6288	24	Florianópolis	florianopolis	2013-09-03 15:30:12.389704	1
6289	24	Formosa do Sul	formosa-do-sul	2013-09-03 15:30:12.389704	1
6290	24	Forquilhinha	forquilhinha	2013-09-03 15:30:12.389704	1
6291	24	Fraiburgo	fraiburgo	2013-09-03 15:30:12.389704	1
6292	24	Frei Rogério	frei-rogerio	2013-09-03 15:30:12.389704	1
6293	24	Galvão	galvao	2013-09-03 15:30:12.389704	1
6294	24	Garopaba	garopaba	2013-09-03 15:30:12.389704	1
6295	24	Garuva	garuva	2013-09-03 15:30:12.389704	1
6296	24	Gaspar	gaspar	2013-09-03 15:30:12.389704	1
6297	24	Governador Celso Ramos	governador-celso-ramos	2013-09-03 15:30:12.389704	1
6298	24	Grão Pará	grao-para	2013-09-03 15:30:12.389704	1
6299	24	Gravatal	gravatal	2013-09-03 15:30:12.389704	1
6300	24	Guabiruba	guabiruba	2013-09-03 15:30:12.389704	1
6301	24	Guaraciaba	guaraciaba	2013-09-03 15:30:12.389704	1
6302	24	Guaramirim	guaramirim	2013-09-03 15:30:12.389704	1
6303	24	Guarujá do Sul	guaruja-do-sul	2013-09-03 15:30:12.389704	1
6304	24	Guatambú	guatambu	2013-09-03 15:30:12.389704	1
6305	24	Herval d`Oeste	herval-d-oeste	2013-09-03 15:30:12.389704	1
6306	24	Ibiam	ibiam	2013-09-03 15:30:12.389704	1
6307	24	Ibicaré	ibicare	2013-09-03 15:30:12.389704	1
6308	24	Ibirama	ibirama	2013-09-03 15:30:12.389704	1
6309	24	Içara	icara	2013-09-03 15:30:12.389704	1
6310	24	Ilhota	ilhota	2013-09-03 15:30:12.389704	1
6311	24	Imaruí	imarui	2013-09-03 15:30:12.389704	1
6312	24	Imbituba	imbituba	2013-09-03 15:30:12.389704	1
6313	24	Imbuia	imbuia	2013-09-03 15:30:12.389704	1
6314	24	Indaial	indaial	2013-09-03 15:30:12.389704	1
6315	24	Iomerê	iomere	2013-09-03 15:30:12.389704	1
6316	24	Ipira	ipira	2013-09-03 15:30:12.389704	1
6317	24	Iporã do Oeste	ipora-do-oeste	2013-09-03 15:30:12.389704	1
6318	24	Ipuaçu	ipuacu	2013-09-03 15:30:12.389704	1
6319	24	Ipumirim	ipumirim	2013-09-03 15:30:12.389704	1
6320	24	Iraceminha	iraceminha	2013-09-03 15:30:12.389704	1
6321	24	Irani	irani	2013-09-03 15:30:12.389704	1
6322	24	Irati	irati	2013-09-03 15:30:12.389704	1
6323	24	Irineópolis	irineopolis	2013-09-03 15:30:12.389704	1
6324	24	Itá	ita	2013-09-03 15:30:12.389704	1
6325	24	Itaiópolis	itaiopolis	2013-09-03 15:30:12.389704	1
6326	24	Itajaí	itajai	2013-09-03 15:30:12.389704	1
6327	24	Itapema	itapema	2013-09-03 15:30:12.389704	1
6328	24	Itapiranga	itapiranga	2013-09-03 15:30:12.389704	1
6329	24	Itapoá	itapoa	2013-09-03 15:30:12.389704	1
6330	24	Ituporanga	ituporanga	2013-09-03 15:30:12.389704	1
6331	24	Jaborá	jabora	2013-09-03 15:30:12.389704	1
6332	24	Jacinto Machado	jacinto-machado	2013-09-03 15:30:12.389704	1
6333	24	Jaguaruna	jaguaruna	2013-09-03 15:30:12.389704	1
6334	24	Jaraguá do Sul	jaragua-do-sul	2013-09-03 15:30:12.389704	1
6335	24	Jardinópolis	jardinopolis	2013-09-03 15:30:12.389704	1
6336	24	Joaçaba	joacaba	2013-09-03 15:30:12.389704	1
6337	24	Joinville	joinville	2013-09-03 15:30:12.389704	1
6338	24	José Boiteux	jose-boiteux	2013-09-03 15:30:12.389704	1
6339	24	Jupiá	jupia	2013-09-03 15:30:12.389704	1
6340	24	Lacerdópolis	lacerdopolis	2013-09-03 15:30:12.389704	1
6341	24	Lages	lages	2013-09-03 15:30:12.389704	1
6342	24	Laguna	laguna	2013-09-03 15:30:12.389704	1
6343	24	Lajeado Grande	lajeado-grande	2013-09-03 15:30:12.389704	1
6344	24	Laurentino	laurentino	2013-09-03 15:30:12.389704	1
6345	24	Lauro Muller	lauro-muller	2013-09-03 15:30:12.389704	1
6346	24	Lebon Régis	lebon-regis	2013-09-03 15:30:12.389704	1
6347	24	Leoberto Leal	leoberto-leal	2013-09-03 15:30:12.389704	1
6348	24	Lindóia do Sul	lindoia-do-sul	2013-09-03 15:30:12.389704	1
6349	24	Lontras	lontras	2013-09-03 15:30:12.389704	1
6350	24	Luiz Alves	luiz-alves	2013-09-03 15:30:12.389704	1
6351	24	Luzerna	luzerna	2013-09-03 15:30:12.389704	1
6352	24	Macieira	macieira	2013-09-03 15:30:12.389704	1
6353	24	Mafra	mafra	2013-09-03 15:30:12.389704	1
6354	24	Major Gercino	major-gercino	2013-09-03 15:30:12.389704	1
6355	24	Major Vieira	major-vieira	2013-09-03 15:30:12.389704	1
6356	24	Maracajá	maracaja	2013-09-03 15:30:12.389704	1
6357	24	Maravilha	maravilha	2013-09-03 15:30:12.389704	1
6358	24	Marema	marema	2013-09-03 15:30:12.389704	1
6359	24	Massaranduba	massaranduba	2013-09-03 15:30:12.389704	1
6360	24	Matos Costa	matos-costa	2013-09-03 15:30:12.389704	1
6361	24	Meleiro	meleiro	2013-09-03 15:30:12.389704	1
6362	24	Mirim Doce	mirim-doce	2013-09-03 15:30:12.389704	1
6363	24	Modelo	modelo	2013-09-03 15:30:12.389704	1
6364	24	Mondaí	mondai	2013-09-03 15:30:12.389704	1
6365	24	Monte Carlo	monte-carlo	2013-09-03 15:30:12.389704	1
6366	24	Monte Castelo	monte-castelo	2013-09-03 15:30:12.389704	1
6367	24	Morro da Fumaça	morro-da-fumaca	2013-09-03 15:30:12.389704	1
6368	24	Morro Grande	morro-grande	2013-09-03 15:30:12.389704	1
6369	24	Navegantes	navegantes	2013-09-03 15:30:12.389704	1
6370	24	Nova Erechim	nova-erechim	2013-09-03 15:30:12.389704	1
6371	24	Nova Itaberaba	nova-itaberaba	2013-09-03 15:30:12.389704	1
6372	24	Nova Trento	nova-trento	2013-09-03 15:30:12.389704	1
6373	24	Nova Veneza	nova-veneza	2013-09-03 15:30:12.389704	1
6374	24	Novo Horizonte	novo-horizonte	2013-09-03 15:30:12.389704	1
6375	24	Orleans	orleans	2013-09-03 15:30:12.389704	1
6376	24	Otacílio Costa	otacilio-costa	2013-09-03 15:30:12.389704	1
6377	24	Ouro	ouro	2013-09-03 15:30:12.389704	1
6378	24	Ouro Verde	ouro-verde	2013-09-03 15:30:12.389704	1
6379	24	Paial	paial	2013-09-03 15:30:12.389704	1
6380	24	Painel	painel	2013-09-03 15:30:12.389704	1
6381	24	Palhoça	palhoca	2013-09-03 15:30:12.389704	1
6382	24	Palma Sola	palma-sola	2013-09-03 15:30:12.389704	1
6383	24	Palmeira	palmeira	2013-09-03 15:30:12.389704	1
6384	24	Palmitos	palmitos	2013-09-03 15:30:12.389704	1
6385	24	Papanduva	papanduva	2013-09-03 15:30:12.389704	1
6386	24	Paraíso	paraiso	2013-09-03 15:30:12.389704	1
6387	24	Passo de Torres	passo-de-torres	2013-09-03 15:30:12.389704	1
6388	24	Passos Maia	passos-maia	2013-09-03 15:30:12.389704	1
6389	24	Paulo Lopes	paulo-lopes	2013-09-03 15:30:12.389704	1
6390	24	Pedras Grandes	pedras-grandes	2013-09-03 15:30:12.389704	1
6391	24	Penha	penha	2013-09-03 15:30:12.389704	1
6392	24	Peritiba	peritiba	2013-09-03 15:30:12.389704	1
6393	24	Petrolândia	petrolandia	2013-09-03 15:30:12.389704	1
6394	24	Piçarras	picarras	2013-09-03 15:30:12.389704	1
6395	24	Pinhalzinho	pinhalzinho	2013-09-03 15:30:12.389704	1
6396	24	Pinheiro Preto	pinheiro-preto	2013-09-03 15:30:12.389704	1
6397	24	Piratuba	piratuba	2013-09-03 15:30:12.389704	1
6398	24	Planalto Alegre	planalto-alegre	2013-09-03 15:30:12.389704	1
6399	24	Pomerode	pomerode	2013-09-03 15:30:12.389704	1
6400	24	Ponte Alta	ponte-alta	2013-09-03 15:30:12.389704	1
6401	24	Ponte Alta do Norte	ponte-alta-do-norte	2013-09-03 15:30:12.389704	1
6402	24	Ponte Serrada	ponte-serrada	2013-09-03 15:30:12.389704	1
6403	24	Porto Belo	porto-belo	2013-09-03 15:30:12.389704	1
6404	24	Porto União	porto-uniao	2013-09-03 15:30:12.389704	1
6405	24	Pouso Redondo	pouso-redondo	2013-09-03 15:30:12.389704	1
6406	24	Praia Grande	praia-grande	2013-09-03 15:30:12.389704	1
6407	24	Presidente Castelo Branco	presidente-castelo-branco	2013-09-03 15:30:12.389704	1
6408	24	Presidente Getúlio	presidente-getulio	2013-09-03 15:30:12.389704	1
6409	24	Presidente Nereu	presidente-nereu	2013-09-03 15:30:12.389704	1
6410	24	Princesa	princesa	2013-09-03 15:30:12.389704	1
6411	24	Quilombo	quilombo	2013-09-03 15:30:12.389704	1
6412	24	Rancho Queimado	rancho-queimado	2013-09-03 15:30:12.389704	1
6413	24	Rio das Antas	rio-das-antas	2013-09-03 15:30:12.389704	1
6414	24	Rio do Campo	rio-do-campo	2013-09-03 15:30:12.389704	1
6415	24	Rio do Oeste	rio-do-oeste	2013-09-03 15:30:12.389704	1
6416	24	Rio do Sul	rio-do-sul	2013-09-03 15:30:12.389704	1
6417	24	Rio dos Cedros	rio-dos-cedros	2013-09-03 15:30:12.389704	1
6418	24	Rio Fortuna	rio-fortuna	2013-09-03 15:30:12.389704	1
6419	24	Rio Negrinho	rio-negrinho	2013-09-03 15:30:12.389704	1
6420	24	Rio Rufino	rio-rufino	2013-09-03 15:30:12.389704	1
6421	24	Riqueza	riqueza	2013-09-03 15:30:12.389704	1
6422	24	Rodeio	rodeio	2013-09-03 15:30:12.389704	1
6423	24	Romelândia	romelandia	2013-09-03 15:30:12.389704	1
6424	24	Salete	salete	2013-09-03 15:30:12.389704	1
6425	24	Saltinho	saltinho	2013-09-03 15:30:12.389704	1
6426	24	Salto Veloso	salto-veloso	2013-09-03 15:30:12.389704	1
6427	24	Sangão	sangao	2013-09-03 15:30:12.389704	1
6428	24	Santa Cecília	santa-cecilia	2013-09-03 15:30:12.389704	1
6429	24	Santa Helena	santa-helena	2013-09-03 15:30:12.389704	1
6430	24	Santa Rosa de Lima	santa-rosa-de-lima	2013-09-03 15:30:12.389704	1
6431	24	Santa Rosa do Sul	santa-rosa-do-sul	2013-09-03 15:30:12.389704	1
6432	24	Santa Terezinha	santa-terezinha	2013-09-03 15:30:12.389704	1
6433	24	Santa Terezinha do Progresso	santa-terezinha-do-progresso	2013-09-03 15:30:12.389704	1
6434	24	Santiago do Sul	santiago-do-sul	2013-09-03 15:30:12.389704	1
6435	24	Santo Amaro da Imperatriz	santo-amaro-da-imperatriz	2013-09-03 15:30:12.389704	1
6436	24	São Bento do Sul	sao-bento-do-sul	2013-09-03 15:30:12.389704	1
6437	24	São Bernardino	sao-bernardino	2013-09-03 15:30:12.389704	1
6438	24	São Bonifácio	sao-bonifacio	2013-09-03 15:30:12.389704	1
6439	24	São Carlos	sao-carlos	2013-09-03 15:30:12.389704	1
6440	24	São Cristovão do Sul	sao-cristovao-do-sul	2013-09-03 15:30:12.389704	1
6441	24	São Domingos	sao-domingos	2013-09-03 15:30:12.389704	1
6785	26	Tomar do Geru	tomar-do-geru	2013-09-03 15:30:12.389704	1
6442	24	São Francisco do Sul	sao-francisco-do-sul	2013-09-03 15:30:12.389704	1
6443	24	São João Batista	sao-joao-batista	2013-09-03 15:30:12.389704	1
6444	24	São João do Itaperiú	sao-joao-do-itaperiu	2013-09-03 15:30:12.389704	1
6445	24	São João do Oeste	sao-joao-do-oeste	2013-09-03 15:30:12.389704	1
6446	24	São João do Sul	sao-joao-do-sul	2013-09-03 15:30:12.389704	1
6447	24	São Joaquim	sao-joaquim	2013-09-03 15:30:12.389704	1
6448	24	São José	sao-jose	2013-09-03 15:30:12.389704	1
6449	24	São José do Cedro	sao-jose-do-cedro	2013-09-03 15:30:12.389704	1
6450	24	São José do Cerrito	sao-jose-do-cerrito	2013-09-03 15:30:12.389704	1
6451	24	São Lourenço do Oeste	sao-lourenco-do-oeste	2013-09-03 15:30:12.389704	1
6452	24	São Ludgero	sao-ludgero	2013-09-03 15:30:12.389704	1
6453	24	São Martinho	sao-martinho	2013-09-03 15:30:12.389704	1
6454	24	São Miguel da Boa Vista	sao-miguel-da-boa-vista	2013-09-03 15:30:12.389704	1
6455	24	São Miguel do Oeste	sao-miguel-do-oeste	2013-09-03 15:30:12.389704	1
6456	24	São Pedro de Alcântara	sao-pedro-de-alcantara	2013-09-03 15:30:12.389704	1
6457	24	Saudades	saudades	2013-09-03 15:30:12.389704	1
6458	24	Schroeder	schroeder	2013-09-03 15:30:12.389704	1
6459	24	Seara	seara	2013-09-03 15:30:12.389704	1
6460	24	Serra Alta	serra-alta	2013-09-03 15:30:12.389704	1
6461	24	Siderópolis	sideropolis	2013-09-03 15:30:12.389704	1
6462	24	Sombrio	sombrio	2013-09-03 15:30:12.389704	1
6463	24	Sul Brasil	sul-brasil	2013-09-03 15:30:12.389704	1
6464	24	Taió	taio	2013-09-03 15:30:12.389704	1
6465	24	Tangará	tangara	2013-09-03 15:30:12.389704	1
6466	24	Tigrinhos	tigrinhos	2013-09-03 15:30:12.389704	1
6467	24	Tijucas	tijucas	2013-09-03 15:30:12.389704	1
6468	24	Timbé do Sul	timbe-do-sul	2013-09-03 15:30:12.389704	1
6469	24	Timbó	timbo	2013-09-03 15:30:12.389704	1
6470	24	Timbó Grande	timbo-grande	2013-09-03 15:30:12.389704	1
6471	24	Três Barras	tres-barras	2013-09-03 15:30:12.389704	1
6472	24	Treviso	treviso	2013-09-03 15:30:12.389704	1
6473	24	Treze de Maio	treze-de-maio	2013-09-03 15:30:12.389704	1
6474	24	Treze Tílias	treze-tilias	2013-09-03 15:30:12.389704	1
6475	24	Trombudo Central	trombudo-central	2013-09-03 15:30:12.389704	1
6476	24	Tubarão	tubarao	2013-09-03 15:30:12.389704	1
6477	24	Tunápolis	tunapolis	2013-09-03 15:30:12.389704	1
6478	24	Turvo	turvo	2013-09-03 15:30:12.389704	1
6479	24	União do Oeste	uniao-do-oeste	2013-09-03 15:30:12.389704	1
6480	24	Urubici	urubici	2013-09-03 15:30:12.389704	1
6481	24	Urupema	urupema	2013-09-03 15:30:12.389704	1
6482	24	Urussanga	urussanga	2013-09-03 15:30:12.389704	1
6483	24	Vargeão	vargeao	2013-09-03 15:30:12.389704	1
6484	24	Vargem	vargem	2013-09-03 15:30:12.389704	1
6485	24	Vargem Bonita	vargem-bonita	2013-09-03 15:30:12.389704	1
6486	24	Vidal Ramos	vidal-ramos	2013-09-03 15:30:12.389704	1
6487	24	Videira	videira	2013-09-03 15:30:12.389704	1
6488	24	Vitor Meireles	vitor-meireles	2013-09-03 15:30:12.389704	1
6489	24	Witmarsum	witmarsum	2013-09-03 15:30:12.389704	1
6490	24	Xanxerê	xanxere	2013-09-03 15:30:12.389704	1
6491	24	Xavantina	xavantina	2013-09-03 15:30:12.389704	1
6492	24	Xaxim	xaxim	2013-09-03 15:30:12.389704	1
6493	24	Zortéa	zortea	2013-09-03 15:30:12.389704	1
6712	26	Amparo de São Francisco	amparo-de-sao-francisco	2013-09-03 15:30:12.389704	1
6713	26	Aquidabã	aquidaba	2013-09-03 15:30:12.389704	1
6714	26	Aracaju	aracaju	2013-09-03 15:30:12.389704	1
6715	26	Arauá	araua	2013-09-03 15:30:12.389704	1
6716	26	Areia Branca	areia-branca	2013-09-03 15:30:12.389704	1
6717	26	Barra dos Coqueiros	barra-dos-coqueiros	2013-09-03 15:30:12.389704	1
6718	26	Boquim	boquim	2013-09-03 15:30:12.389704	1
6719	26	Brejo Grande	brejo-grande	2013-09-03 15:30:12.389704	1
6720	26	Campo do Brito	campo-do-brito	2013-09-03 15:30:12.389704	1
6721	26	Canhoba	canhoba	2013-09-03 15:30:12.389704	1
6722	26	Canindé de São Francisco	caninde-de-sao-francisco	2013-09-03 15:30:12.389704	1
6723	26	Capela	capela	2013-09-03 15:30:12.389704	1
6724	26	Carira	carira	2013-09-03 15:30:12.389704	1
6725	26	Carmópolis	carmopolis	2013-09-03 15:30:12.389704	1
6726	26	Cedro de São João	cedro-de-sao-joao	2013-09-03 15:30:12.389704	1
6727	26	Cristinápolis	cristinapolis	2013-09-03 15:30:12.389704	1
6728	26	Cumbe	cumbe	2013-09-03 15:30:12.389704	1
6729	26	Divina Pastora	divina-pastora	2013-09-03 15:30:12.389704	1
6730	26	Estância	estancia	2013-09-03 15:30:12.389704	1
6731	26	Feira Nova	feira-nova	2013-09-03 15:30:12.389704	1
6732	26	Frei Paulo	frei-paulo	2013-09-03 15:30:12.389704	1
6733	26	Gararu	gararu	2013-09-03 15:30:12.389704	1
6734	26	General Maynard	general-maynard	2013-09-03 15:30:12.389704	1
6735	26	Gracho Cardoso	gracho-cardoso	2013-09-03 15:30:12.389704	1
6736	26	Ilha das Flores	ilha-das-flores	2013-09-03 15:30:12.389704	1
6737	26	Indiaroba	indiaroba	2013-09-03 15:30:12.389704	1
6738	26	Itabaiana	itabaiana	2013-09-03 15:30:12.389704	1
6739	26	Itabaianinha	itabaianinha	2013-09-03 15:30:12.389704	1
6740	26	Itabi	itabi	2013-09-03 15:30:12.389704	1
6741	26	Itaporanga d`Ajuda	itaporanga-d-ajuda	2013-09-03 15:30:12.389704	1
6742	26	Japaratuba	japaratuba	2013-09-03 15:30:12.389704	1
6743	26	Japoatã	japoata	2013-09-03 15:30:12.389704	1
6744	26	Lagarto	lagarto	2013-09-03 15:30:12.389704	1
6745	26	Laranjeiras	laranjeiras	2013-09-03 15:30:12.389704	1
6746	26	Macambira	macambira	2013-09-03 15:30:12.389704	1
6747	26	Malhada dos Bois	malhada-dos-bois	2013-09-03 15:30:12.389704	1
6748	26	Malhador	malhador	2013-09-03 15:30:12.389704	1
6749	26	Maruim	maruim	2013-09-03 15:30:12.389704	1
6750	26	Moita Bonita	moita-bonita	2013-09-03 15:30:12.389704	1
6751	26	Monte Alegre de Sergipe	monte-alegre-de-sergipe	2013-09-03 15:30:12.389704	1
6752	26	Muribeca	muribeca	2013-09-03 15:30:12.389704	1
6753	26	Neópolis	neopolis	2013-09-03 15:30:12.389704	1
6754	26	Nossa Senhora Aparecida	nossa-senhora-aparecida	2013-09-03 15:30:12.389704	1
6755	26	Nossa Senhora da Glória	nossa-senhora-da-gloria	2013-09-03 15:30:12.389704	1
6756	26	Nossa Senhora das Dores	nossa-senhora-das-dores	2013-09-03 15:30:12.389704	1
6757	26	Nossa Senhora de Lourdes	nossa-senhora-de-lourdes	2013-09-03 15:30:12.389704	1
6758	26	Nossa Senhora do Socorro	nossa-senhora-do-socorro	2013-09-03 15:30:12.389704	1
6759	26	Pacatuba	pacatuba	2013-09-03 15:30:12.389704	1
6760	26	Pedra Mole	pedra-mole	2013-09-03 15:30:12.389704	1
6761	26	Pedrinhas	pedrinhas	2013-09-03 15:30:12.389704	1
6762	26	Pinhão	pinhao	2013-09-03 15:30:12.389704	1
6763	26	Pirambu	pirambu	2013-09-03 15:30:12.389704	1
6764	26	Poço Redondo	poco-redondo	2013-09-03 15:30:12.389704	1
6765	26	Poço Verde	poco-verde	2013-09-03 15:30:12.389704	1
6766	26	Porto da Folha	porto-da-folha	2013-09-03 15:30:12.389704	1
6767	26	Propriá	propria	2013-09-03 15:30:12.389704	1
6768	26	Riachão do Dantas	riachao-do-dantas	2013-09-03 15:30:12.389704	1
6769	26	Riachuelo	riachuelo	2013-09-03 15:30:12.389704	1
6770	26	Ribeirópolis	ribeiropolis	2013-09-03 15:30:12.389704	1
6771	26	Rosário do Catete	rosario-do-catete	2013-09-03 15:30:12.389704	1
6772	26	Salgado	salgado	2013-09-03 15:30:12.389704	1
6773	26	Santa Luzia do Itanhy	santa-luzia-do-itanhy	2013-09-03 15:30:12.389704	1
6774	26	Santa Rosa de Lima	santa-rosa-de-lima	2013-09-03 15:30:12.389704	1
6775	26	Santana do São Francisco	santana-do-sao-francisco	2013-09-03 15:30:12.389704	1
6776	26	Santo Amaro das Brotas	santo-amaro-das-brotas	2013-09-03 15:30:12.389704	1
6777	26	São Cristóvão	sao-cristovao	2013-09-03 15:30:12.389704	1
6778	26	São Domingos	sao-domingos	2013-09-03 15:30:12.389704	1
6779	26	São Francisco	sao-francisco	2013-09-03 15:30:12.389704	1
6780	26	São Miguel do Aleixo	sao-miguel-do-aleixo	2013-09-03 15:30:12.389704	1
6781	26	Simão Dias	simao-dias	2013-09-03 15:30:12.389704	1
6782	26	Siriri	siriri	2013-09-03 15:30:12.389704	1
6783	26	Telha	telha	2013-09-03 15:30:12.389704	1
6784	26	Tobias Barreto	tobias-barreto	2013-09-03 15:30:12.389704	1
6786	26	Umbaúba	umbauba	2013-09-03 15:30:12.389704	1
8116	25	Adamantina	adamantina	2013-09-03 15:30:12.389704	1
8117	25	Adolfo	adolfo	2013-09-03 15:30:12.389704	1
8118	25	Aguaí	aguai	2013-09-03 15:30:12.389704	1
8119	25	Águas da Prata	aguas-da-prata	2013-09-03 15:30:12.389704	1
8120	25	Águas de Lindóia	aguas-de-lindoia	2013-09-03 15:30:12.389704	1
8121	25	Águas de Santa Bárbara	aguas-de-santa-barbara	2013-09-03 15:30:12.389704	1
8122	25	Águas de São Pedro	aguas-de-sao-pedro	2013-09-03 15:30:12.389704	1
8123	25	Agudos	agudos	2013-09-03 15:30:12.389704	1
8124	25	Alambari	alambari	2013-09-03 15:30:12.389704	1
8125	25	Alfredo Marcondes	alfredo-marcondes	2013-09-03 15:30:12.389704	1
8126	25	Altair	altair	2013-09-03 15:30:12.389704	1
8127	25	Altinópolis	altinopolis	2013-09-03 15:30:12.389704	1
8128	25	Alto Alegre	alto-alegre	2013-09-03 15:30:12.389704	1
8129	25	Alumínio	aluminio	2013-09-03 15:30:12.389704	1
8130	25	Álvares Florence	alvares-florence	2013-09-03 15:30:12.389704	1
8131	25	Álvares Machado	alvares-machado	2013-09-03 15:30:12.389704	1
8132	25	Álvaro de Carvalho	alvaro-de-carvalho	2013-09-03 15:30:12.389704	1
8133	25	Alvinlândia	alvinlandia	2013-09-03 15:30:12.389704	1
8134	25	Americana	americana	2013-09-03 15:30:12.389704	1
8135	25	Américo Brasiliense	americo-brasiliense	2013-09-03 15:30:12.389704	1
8136	25	Américo de Campos	americo-de-campos	2013-09-03 15:30:12.389704	1
8137	25	Amparo	amparo	2013-09-03 15:30:12.389704	1
8138	25	Analândia	analandia	2013-09-03 15:30:12.389704	1
8139	25	Andradina	andradina	2013-09-03 15:30:12.389704	1
8140	25	Angatuba	angatuba	2013-09-03 15:30:12.389704	1
8141	25	Anhembi	anhembi	2013-09-03 15:30:12.389704	1
8142	25	Anhumas	anhumas	2013-09-03 15:30:12.389704	1
8143	25	Aparecida	aparecida	2013-09-03 15:30:12.389704	1
8144	25	Aparecida d`Oeste	aparecida-d-oeste	2013-09-03 15:30:12.389704	1
8145	25	Apiaí	apiai	2013-09-03 15:30:12.389704	1
8146	25	Araçariguama	aracariguama	2013-09-03 15:30:12.389704	1
8147	25	Araçatuba	aracatuba	2013-09-03 15:30:12.389704	1
8148	25	Araçoiaba da Serra	aracoiaba-da-serra	2013-09-03 15:30:12.389704	1
8149	25	Aramina	aramina	2013-09-03 15:30:12.389704	1
8150	25	Arandu	arandu	2013-09-03 15:30:12.389704	1
8151	25	Arapeí	arapei	2013-09-03 15:30:12.389704	1
8152	25	Araraquara	araraquara	2013-09-03 15:30:12.389704	1
8153	25	Araras	araras	2013-09-03 15:30:12.389704	1
8154	25	Arco-Íris	arco-iris	2013-09-03 15:30:12.389704	1
8155	25	Arealva	arealva	2013-09-03 15:30:12.389704	1
8156	25	Areias	areias	2013-09-03 15:30:12.389704	1
8157	25	Areiópolis	areiopolis	2013-09-03 15:30:12.389704	1
8158	25	Ariranha	ariranha	2013-09-03 15:30:12.389704	1
8159	25	Artur Nogueira	artur-nogueira	2013-09-03 15:30:12.389704	1
8160	25	Arujá	aruja	2013-09-03 15:30:12.389704	1
8161	25	Aspásia	aspasia	2013-09-03 15:30:12.389704	1
8162	25	Assis	assis	2013-09-03 15:30:12.389704	1
8163	25	Atibaia	atibaia	2013-09-03 15:30:12.389704	1
8164	25	Auriflama	auriflama	2013-09-03 15:30:12.389704	1
8165	25	Avaí	avai	2013-09-03 15:30:12.389704	1
8166	25	Avanhandava	avanhandava	2013-09-03 15:30:12.389704	1
8167	25	Avaré	avare	2013-09-03 15:30:12.389704	1
8168	25	Bady Bassitt	bady-bassitt	2013-09-03 15:30:12.389704	1
8169	25	Balbinos	balbinos	2013-09-03 15:30:12.389704	1
8170	25	Bálsamo	balsamo	2013-09-03 15:30:12.389704	1
8171	25	Bananal	bananal	2013-09-03 15:30:12.389704	1
8172	25	Barão de Antonina	barao-de-antonina	2013-09-03 15:30:12.389704	1
8173	25	Barbosa	barbosa	2013-09-03 15:30:12.389704	1
8174	25	Bariri	bariri	2013-09-03 15:30:12.389704	1
8175	25	Barra Bonita	barra-bonita	2013-09-03 15:30:12.389704	1
8176	25	Barra do Chapéu	barra-do-chapeu	2013-09-03 15:30:12.389704	1
8177	25	Barra do Turvo	barra-do-turvo	2013-09-03 15:30:12.389704	1
8178	25	Barretos	barretos	2013-09-03 15:30:12.389704	1
8179	25	Barrinha	barrinha	2013-09-03 15:30:12.389704	1
8180	25	Barueri	barueri	2013-09-03 15:30:12.389704	1
8181	25	Bastos	bastos	2013-09-03 15:30:12.389704	1
8182	25	Batatais	batatais	2013-09-03 15:30:12.389704	1
8183	25	Bauru	bauru	2013-09-03 15:30:12.389704	1
8184	25	Bebedouro	bebedouro	2013-09-03 15:30:12.389704	1
8185	25	Bento de Abreu	bento-de-abreu	2013-09-03 15:30:12.389704	1
8186	25	Bernardino de Campos	bernardino-de-campos	2013-09-03 15:30:12.389704	1
8187	25	Bertioga	bertioga	2013-09-03 15:30:12.389704	1
8188	25	Bilac	bilac	2013-09-03 15:30:12.389704	1
8189	25	Birigui	birigui	2013-09-03 15:30:12.389704	1
8190	25	Biritiba-Mirim	biritiba-mirim	2013-09-03 15:30:12.389704	1
8191	25	Boa Esperança do Sul	boa-esperanca-do-sul	2013-09-03 15:30:12.389704	1
8192	25	Bocaina	bocaina	2013-09-03 15:30:12.389704	1
8193	25	Bofete	bofete	2013-09-03 15:30:12.389704	1
8194	25	Boituva	boituva	2013-09-03 15:30:12.389704	1
8195	25	Bom Jesus dos Perdões	bom-jesus-dos-perdoes	2013-09-03 15:30:12.389704	1
8196	25	Bom Sucesso de Itararé	bom-sucesso-de-itarare	2013-09-03 15:30:12.389704	1
8197	25	Borá	bora	2013-09-03 15:30:12.389704	1
8198	25	Boracéia	boraceia	2013-09-03 15:30:12.389704	1
8199	25	Borborema	borborema	2013-09-03 15:30:12.389704	1
8200	25	Borebi	borebi	2013-09-03 15:30:12.389704	1
8201	25	Botucatu	botucatu	2013-09-03 15:30:12.389704	1
8202	25	Bragança Paulista	braganca-paulista	2013-09-03 15:30:12.389704	1
8203	25	Braúna	brauna	2013-09-03 15:30:12.389704	1
8204	25	Brejo Alegre	brejo-alegre	2013-09-03 15:30:12.389704	1
8205	25	Brodowski	brodowski	2013-09-03 15:30:12.389704	1
8206	25	Brotas	brotas	2013-09-03 15:30:12.389704	1
8207	25	Buri	buri	2013-09-03 15:30:12.389704	1
8208	25	Buritama	buritama	2013-09-03 15:30:12.389704	1
8209	25	Buritizal	buritizal	2013-09-03 15:30:12.389704	1
8210	25	Cabrália Paulista	cabralia-paulista	2013-09-03 15:30:12.389704	1
8211	25	Cabreúva	cabreuva	2013-09-03 15:30:12.389704	1
8212	25	Caçapava	cacapava	2013-09-03 15:30:12.389704	1
8213	25	Cachoeira Paulista	cachoeira-paulista	2013-09-03 15:30:12.389704	1
8214	25	Caconde	caconde	2013-09-03 15:30:12.389704	1
8215	25	Cafelândia	cafelandia	2013-09-03 15:30:12.389704	1
8216	25	Caiabu	caiabu	2013-09-03 15:30:12.389704	1
8217	25	Caieiras	caieiras	2013-09-03 15:30:12.389704	1
8218	25	Caiuá	caiua	2013-09-03 15:30:12.389704	1
8219	25	Cajamar	cajamar	2013-09-03 15:30:12.389704	1
8220	25	Cajati	cajati	2013-09-03 15:30:12.389704	1
8221	25	Cajobi	cajobi	2013-09-03 15:30:12.389704	1
8222	25	Cajuru	cajuru	2013-09-03 15:30:12.389704	1
8223	25	Campina do Monte Alegre	campina-do-monte-alegre	2013-09-03 15:30:12.389704	1
8224	25	Campinas	campinas	2013-09-03 15:30:12.389704	1
8225	25	Campo Limpo Paulista	campo-limpo-paulista	2013-09-03 15:30:12.389704	1
8226	25	Campos do Jordão	campos-do-jordao	2013-09-03 15:30:12.389704	1
8227	25	Campos Novos Paulista	campos-novos-paulista	2013-09-03 15:30:12.389704	1
8228	25	Cananéia	cananeia	2013-09-03 15:30:12.389704	1
8229	25	Canas	canas	2013-09-03 15:30:12.389704	1
8230	25	Cândido Mota	candido-mota	2013-09-03 15:30:12.389704	1
8231	25	Cândido Rodrigues	candido-rodrigues	2013-09-03 15:30:12.389704	1
8232	25	Canitar	canitar	2013-09-03 15:30:12.389704	1
8233	25	Capão Bonito	capao-bonito	2013-09-03 15:30:12.389704	1
8234	25	Capela do Alto	capela-do-alto	2013-09-03 15:30:12.389704	1
8235	25	Capivari	capivari	2013-09-03 15:30:12.389704	1
8236	25	Caraguatatuba	caraguatatuba	2013-09-03 15:30:12.389704	1
8237	25	Carapicuíba	carapicuiba	2013-09-03 15:30:12.389704	1
8238	25	Cardoso	cardoso	2013-09-03 15:30:12.389704	1
8239	25	Casa Branca	casa-branca	2013-09-03 15:30:12.389704	1
8240	25	Cássia dos Coqueiros	cassia-dos-coqueiros	2013-09-03 15:30:12.389704	1
8241	25	Castilho	castilho	2013-09-03 15:30:12.389704	1
8242	25	Catanduva	catanduva	2013-09-03 15:30:12.389704	1
8243	25	Catiguá	catigua	2013-09-03 15:30:12.389704	1
8244	25	Cedral	cedral	2013-09-03 15:30:12.389704	1
8245	25	Cerqueira César	cerqueira-cesar	2013-09-03 15:30:12.389704	1
8246	25	Cerquilho	cerquilho	2013-09-03 15:30:12.389704	1
8247	25	Cesário Lange	cesario-lange	2013-09-03 15:30:12.389704	1
8248	25	Charqueada	charqueada	2013-09-03 15:30:12.389704	1
8249	25	Chavantes	chavantes	2013-09-03 15:30:12.389704	1
8250	25	Clementina	clementina	2013-09-03 15:30:12.389704	1
8251	25	Colina	colina	2013-09-03 15:30:12.389704	1
8252	25	Colômbia	colombia	2013-09-03 15:30:12.389704	1
8253	25	Conchal	conchal	2013-09-03 15:30:12.389704	1
8254	25	Conchas	conchas	2013-09-03 15:30:12.389704	1
8255	25	Cordeirópolis	cordeiropolis	2013-09-03 15:30:12.389704	1
8256	25	Coroados	coroados	2013-09-03 15:30:12.389704	1
8257	25	Coronel Macedo	coronel-macedo	2013-09-03 15:30:12.389704	1
8258	25	Corumbataí	corumbatai	2013-09-03 15:30:12.389704	1
8259	25	Cosmópolis	cosmopolis	2013-09-03 15:30:12.389704	1
8260	25	Cosmorama	cosmorama	2013-09-03 15:30:12.389704	1
8261	25	Cotia	cotia	2013-09-03 15:30:12.389704	1
8262	25	Cravinhos	cravinhos	2013-09-03 15:30:12.389704	1
8263	25	Cristais Paulista	cristais-paulista	2013-09-03 15:30:12.389704	1
8264	25	Cruzália	cruzalia	2013-09-03 15:30:12.389704	1
8265	25	Cruzeiro	cruzeiro	2013-09-03 15:30:12.389704	1
8266	25	Cubatão	cubatao	2013-09-03 15:30:12.389704	1
8267	25	Cunha	cunha	2013-09-03 15:30:12.389704	1
8268	25	Descalvado	descalvado	2013-09-03 15:30:12.389704	1
8269	25	Diadema	diadema	2013-09-03 15:30:12.389704	1
8270	25	Dirce Reis	dirce-reis	2013-09-03 15:30:12.389704	1
8271	25	Divinolândia	divinolandia	2013-09-03 15:30:12.389704	1
8272	25	Dobrada	dobrada	2013-09-03 15:30:12.389704	1
8273	25	Dois Córregos	dois-corregos	2013-09-03 15:30:12.389704	1
8274	25	Dolcinópolis	dolcinopolis	2013-09-03 15:30:12.389704	1
8275	25	Dourado	dourado	2013-09-03 15:30:12.389704	1
8276	25	Dracena	dracena	2013-09-03 15:30:12.389704	1
8277	25	Duartina	duartina	2013-09-03 15:30:12.389704	1
8278	25	Dumont	dumont	2013-09-03 15:30:12.389704	1
8279	25	Echaporã	echapora	2013-09-03 15:30:12.389704	1
8280	25	Eldorado	eldorado	2013-09-03 15:30:12.389704	1
8281	25	Elias Fausto	elias-fausto	2013-09-03 15:30:12.389704	1
8282	25	Elisiário	elisiario	2013-09-03 15:30:12.389704	1
8283	25	Embaúba	embauba	2013-09-03 15:30:12.389704	1
8284	25	Embu	embu	2013-09-03 15:30:12.389704	1
8285	25	Embu-Guaçu	embu-guacu	2013-09-03 15:30:12.389704	1
8286	25	Emilianópolis	emilianopolis	2013-09-03 15:30:12.389704	1
8287	25	Engenheiro Coelho	engenheiro-coelho	2013-09-03 15:30:12.389704	1
8288	25	Espírito Santo do Pinhal	espirito-santo-do-pinhal	2013-09-03 15:30:12.389704	1
8289	25	Espírito Santo do Turvo	espirito-santo-do-turvo	2013-09-03 15:30:12.389704	1
8290	25	Estiva Gerbi	estiva-gerbi	2013-09-03 15:30:12.389704	1
8291	25	Estrela d`Oeste	estrela-d-oeste	2013-09-03 15:30:12.389704	1
8292	25	Estrela do Norte	estrela-do-norte	2013-09-03 15:30:12.389704	1
8293	25	Euclides da Cunha Paulista	euclides-da-cunha-paulista	2013-09-03 15:30:12.389704	1
8294	25	Fartura	fartura	2013-09-03 15:30:12.389704	1
8295	25	Fernando Prestes	fernando-prestes	2013-09-03 15:30:12.389704	1
8296	25	Fernandópolis	fernandopolis	2013-09-03 15:30:12.389704	1
8297	25	Fernão	fernao	2013-09-03 15:30:12.389704	1
8298	25	Ferraz de Vasconcelos	ferraz-de-vasconcelos	2013-09-03 15:30:12.389704	1
8299	25	Flora Rica	flora-rica	2013-09-03 15:30:12.389704	1
8300	25	Floreal	floreal	2013-09-03 15:30:12.389704	1
8301	25	Flórida Paulista	florida-paulista	2013-09-03 15:30:12.389704	1
8302	25	Florínia	florinia	2013-09-03 15:30:12.389704	1
8303	25	Franca	franca	2013-09-03 15:30:12.389704	1
8304	25	Francisco Morato	francisco-morato	2013-09-03 15:30:12.389704	1
8305	25	Franco da Rocha	franco-da-rocha	2013-09-03 15:30:12.389704	1
8306	25	Gabriel Monteiro	gabriel-monteiro	2013-09-03 15:30:12.389704	1
8307	25	Gália	galia	2013-09-03 15:30:12.389704	1
8308	25	Garça	garca	2013-09-03 15:30:12.389704	1
8309	25	Gastão Vidigal	gastao-vidigal	2013-09-03 15:30:12.389704	1
8310	25	Gavião Peixoto	gaviao-peixoto	2013-09-03 15:30:12.389704	1
8311	25	General Salgado	general-salgado	2013-09-03 15:30:12.389704	1
8312	25	Getulina	getulina	2013-09-03 15:30:12.389704	1
8313	25	Glicério	glicerio	2013-09-03 15:30:12.389704	1
8314	25	Guaiçara	guaicara	2013-09-03 15:30:12.389704	1
8315	25	Guaimbê	guaimbe	2013-09-03 15:30:12.389704	1
8316	25	Guaíra	guaira	2013-09-03 15:30:12.389704	1
8317	25	Guapiaçu	guapiacu	2013-09-03 15:30:12.389704	1
8318	25	Guapiara	guapiara	2013-09-03 15:30:12.389704	1
8319	25	Guará	guara	2013-09-03 15:30:12.389704	1
8320	25	Guaraçaí	guaracai	2013-09-03 15:30:12.389704	1
8321	25	Guaraci	guaraci	2013-09-03 15:30:12.389704	1
8322	25	Guarani d`Oeste	guarani-d-oeste	2013-09-03 15:30:12.389704	1
8323	25	Guarantã	guaranta	2013-09-03 15:30:12.389704	1
8324	25	Guararapes	guararapes	2013-09-03 15:30:12.389704	1
8325	25	Guararema	guararema	2013-09-03 15:30:12.389704	1
8326	25	Guaratinguetá	guaratingueta	2013-09-03 15:30:12.389704	1
8327	25	Guareí	guarei	2013-09-03 15:30:12.389704	1
8328	25	Guariba	guariba	2013-09-03 15:30:12.389704	1
8329	25	Guarujá	guaruja	2013-09-03 15:30:12.389704	1
8330	25	Guarulhos	guarulhos	2013-09-03 15:30:12.389704	1
8331	25	Guatapará	guatapara	2013-09-03 15:30:12.389704	1
8332	25	Guzolândia	guzolandia	2013-09-03 15:30:12.389704	1
8333	25	Herculândia	herculandia	2013-09-03 15:30:12.389704	1
8334	25	Holambra	holambra	2013-09-03 15:30:12.389704	1
8335	25	Hortolândia	hortolandia	2013-09-03 15:30:12.389704	1
8336	25	Iacanga	iacanga	2013-09-03 15:30:12.389704	1
8337	25	Iacri	iacri	2013-09-03 15:30:12.389704	1
8338	25	Iaras	iaras	2013-09-03 15:30:12.389704	1
8339	25	Ibaté	ibate	2013-09-03 15:30:12.389704	1
8340	25	Ibirá	ibira	2013-09-03 15:30:12.389704	1
8341	25	Ibirarema	ibirarema	2013-09-03 15:30:12.389704	1
8342	25	Ibitinga	ibitinga	2013-09-03 15:30:12.389704	1
8343	25	Ibiúna	ibiuna	2013-09-03 15:30:12.389704	1
8344	25	Icém	icem	2013-09-03 15:30:12.389704	1
8345	25	Iepê	iepe	2013-09-03 15:30:12.389704	1
8346	25	Igaraçu do Tietê	igaracu-do-tiete	2013-09-03 15:30:12.389704	1
8347	25	Igarapava	igarapava	2013-09-03 15:30:12.389704	1
8348	25	Igaratá	igarata	2013-09-03 15:30:12.389704	1
8349	25	Iguape	iguape	2013-09-03 15:30:12.389704	1
8350	25	Ilha Comprida	ilha-comprida	2013-09-03 15:30:12.389704	1
8351	25	Ilha Solteira	ilha-solteira	2013-09-03 15:30:12.389704	1
8352	25	Ilhabela	ilhabela	2013-09-03 15:30:12.389704	1
8353	25	Indaiatuba	indaiatuba	2013-09-03 15:30:12.389704	1
8354	25	Indiana	indiana	2013-09-03 15:30:12.389704	1
8355	25	Indiaporã	indiapora	2013-09-03 15:30:12.389704	1
8356	25	Inúbia Paulista	inubia-paulista	2013-09-03 15:30:12.389704	1
8357	25	Ipaussu	ipaussu	2013-09-03 15:30:12.389704	1
8358	25	Iperó	ipero	2013-09-03 15:30:12.389704	1
8359	25	Ipeúna	ipeuna	2013-09-03 15:30:12.389704	1
8360	25	Ipiguá	ipigua	2013-09-03 15:30:12.389704	1
8361	25	Iporanga	iporanga	2013-09-03 15:30:12.389704	1
8362	25	Ipuã	ipua	2013-09-03 15:30:12.389704	1
8363	25	Iracemápolis	iracemapolis	2013-09-03 15:30:12.389704	1
8364	25	Irapuã	irapua	2013-09-03 15:30:12.389704	1
8365	25	Irapuru	irapuru	2013-09-03 15:30:12.389704	1
8366	25	Itaberá	itabera	2013-09-03 15:30:12.389704	1
8367	25	Itaí	itai	2013-09-03 15:30:12.389704	1
8368	25	Itajobi	itajobi	2013-09-03 15:30:12.389704	1
8369	25	Itaju	itaju	2013-09-03 15:30:12.389704	1
8370	25	Itanhaém	itanhaem	2013-09-03 15:30:12.389704	1
8371	25	Itaóca	itaoca	2013-09-03 15:30:12.389704	1
8372	25	Itapecerica da Serra	itapecerica-da-serra	2013-09-03 15:30:12.389704	1
8373	25	Itapetininga	itapetininga	2013-09-03 15:30:12.389704	1
8374	25	Itapeva	itapeva	2013-09-03 15:30:12.389704	1
8375	25	Itapevi	itapevi	2013-09-03 15:30:12.389704	1
8376	25	Itapira	itapira	2013-09-03 15:30:12.389704	1
8377	25	Itapirapuã Paulista	itapirapua-paulista	2013-09-03 15:30:12.389704	1
8378	25	\nItápolis	itapolis	2013-09-03 15:30:12.389704	1
8379	25	Itaporanga	itaporanga	2013-09-03 15:30:12.389704	1
8380	25	Itapuí	itapui	2013-09-03 15:30:12.389704	1
8381	25	Itapura	itapura	2013-09-03 15:30:12.389704	1
8382	25	Itaquaquecetuba	itaquaquecetuba	2013-09-03 15:30:12.389704	1
8383	25	Itararé	itarare	2013-09-03 15:30:12.389704	1
8384	25	Itariri	itariri	2013-09-03 15:30:12.389704	1
8385	25	Itatiba	itatiba	2013-09-03 15:30:12.389704	1
8386	25	Itatinga	itatinga	2013-09-03 15:30:12.389704	1
8387	25	Itirapina	itirapina	2013-09-03 15:30:12.389704	1
8388	25	Itirapuã	itirapua	2013-09-03 15:30:12.389704	1
8389	25	Itobi	itobi	2013-09-03 15:30:12.389704	1
8390	25	Itu	itu	2013-09-03 15:30:12.389704	1
8391	25	Itupeva	itupeva	2013-09-03 15:30:12.389704	1
8392	25	Ituverava	ituverava	2013-09-03 15:30:12.389704	1
8393	25	Jaborandi	jaborandi	2013-09-03 15:30:12.389704	1
8394	25	Jaboticabal	jaboticabal	2013-09-03 15:30:12.389704	1
8395	25	Jacareí	jacarei	2013-09-03 15:30:12.389704	1
8396	25	Jaci	jaci	2013-09-03 15:30:12.389704	1
8397	25	Jacupiranga	jacupiranga	2013-09-03 15:30:12.389704	1
8398	25	Jaguariúna	jaguariuna	2013-09-03 15:30:12.389704	1
8399	25	Jales	jales	2013-09-03 15:30:12.389704	1
8400	25	Jambeiro	jambeiro	2013-09-03 15:30:12.389704	1
8401	25	Jandira	jandira	2013-09-03 15:30:12.389704	1
8402	25	Jardinópolis	jardinopolis	2013-09-03 15:30:12.389704	1
8403	25	Jarinu	jarinu	2013-09-03 15:30:12.389704	1
8404	25	Jaú	jau	2013-09-03 15:30:12.389704	1
8405	25	Jeriquara	jeriquara	2013-09-03 15:30:12.389704	1
8406	25	Joanópolis	joanopolis	2013-09-03 15:30:12.389704	1
8407	25	João Ramalho	joao-ramalho	2013-09-03 15:30:12.389704	1
8408	25	José Bonifácio	jose-bonifacio	2013-09-03 15:30:12.389704	1
8409	25	Júlio Mesquita	julio-mesquita	2013-09-03 15:30:12.389704	1
8410	25	Jumirim	jumirim	2013-09-03 15:30:12.389704	1
8411	25	Jundiaí	jundiai	2013-09-03 15:30:12.389704	1
8412	25	Junqueirópolis	junqueiropolis	2013-09-03 15:30:12.389704	1
8413	25	Juquiá	juquia	2013-09-03 15:30:12.389704	1
8414	25	Juquitiba	juquitiba	2013-09-03 15:30:12.389704	1
8415	25	Lagoinha	lagoinha	2013-09-03 15:30:12.389704	1
8416	25	Laranjal Paulista	laranjal-paulista	2013-09-03 15:30:12.389704	1
8417	25	Lavínia	lavinia	2013-09-03 15:30:12.389704	1
8418	25	Lavrinhas	lavrinhas	2013-09-03 15:30:12.389704	1
8419	25	Leme	leme	2013-09-03 15:30:12.389704	1
8420	25	Lençóis Paulista	lencois-paulista	2013-09-03 15:30:12.389704	1
8421	25	Limeira	limeira	2013-09-03 15:30:12.389704	1
8422	25	Lindóia	lindoia	2013-09-03 15:30:12.389704	1
8423	25	Lins	lins	2013-09-03 15:30:12.389704	1
8424	25	Lorena	lorena	2013-09-03 15:30:12.389704	1
8425	25	Lourdes	lourdes	2013-09-03 15:30:12.389704	1
8426	25	Louveira	louveira	2013-09-03 15:30:12.389704	1
8427	25	Lucélia	lucelia	2013-09-03 15:30:12.389704	1
8428	25	Lucianópolis	lucianopolis	2013-09-03 15:30:12.389704	1
8429	25	Luís Antônio	luis-antonio	2013-09-03 15:30:12.389704	1
8430	25	Luiziânia	luiziania	2013-09-03 15:30:12.389704	1
8431	25	Lupércio	lupercio	2013-09-03 15:30:12.389704	1
8432	25	Lutécia	lutecia	2013-09-03 15:30:12.389704	1
8433	25	Macatuba	macatuba	2013-09-03 15:30:12.389704	1
8434	25	Macaubal	macaubal	2013-09-03 15:30:12.389704	1
8435	25	Macedônia	macedonia	2013-09-03 15:30:12.389704	1
8436	25	Magda	magda	2013-09-03 15:30:12.389704	1
8437	25	Mairinque	mairinque	2013-09-03 15:30:12.389704	1
8438	25	Mairiporã	mairipora	2013-09-03 15:30:12.389704	1
8439	25	Manduri	manduri	2013-09-03 15:30:12.389704	1
8440	25	Marabá Paulista	maraba-paulista	2013-09-03 15:30:12.389704	1
8441	25	Maracaí	maracai	2013-09-03 15:30:12.389704	1
8442	25	Marapoama	marapoama	2013-09-03 15:30:12.389704	1
8443	25	Mariápolis	mariapolis	2013-09-03 15:30:12.389704	1
8444	25	Marília	marilia	2013-09-03 15:30:12.389704	1
8445	25	Marinópolis	marinopolis	2013-09-03 15:30:12.389704	1
8446	25	Martinópolis	martinopolis	2013-09-03 15:30:12.389704	1
8447	25	Matão	matao	2013-09-03 15:30:12.389704	1
8448	25	Mauá	maua	2013-09-03 15:30:12.389704	1
8449	25	Mendonça	mendonca	2013-09-03 15:30:12.389704	1
8450	25	Meridiano	meridiano	2013-09-03 15:30:12.389704	1
8451	25	Mesópolis	mesopolis	2013-09-03 15:30:12.389704	1
8452	25	Miguelópolis	miguelopolis	2013-09-03 15:30:12.389704	1
8453	25	Mineiros do Tietê	mineiros-do-tiete	2013-09-03 15:30:12.389704	1
8454	25	Mira Estrela	mira-estrela	2013-09-03 15:30:12.389704	1
8455	25	Miracatu	miracatu	2013-09-03 15:30:12.389704	1
8456	25	Mirandópolis	mirandopolis	2013-09-03 15:30:12.389704	1
8457	25	Mirante do Paranapanema	mirante-do-paranapanema	2013-09-03 15:30:12.389704	1
8458	25	Mirassol	mirassol	2013-09-03 15:30:12.389704	1
8459	25	Mirassolândia	mirassolandia	2013-09-03 15:30:12.389704	1
8460	25	Mococa	mococa	2013-09-03 15:30:12.389704	1
8461	25	Mogi das Cruzes	mogi-das-cruzes	2013-09-03 15:30:12.389704	1
8462	25	Mogi Guaçu	mogi-guacu	2013-09-03 15:30:12.389704	1
8463	25	Moji Mirim	moji-mirim	2013-09-03 15:30:12.389704	1
8464	25	Mombuca	mombuca	2013-09-03 15:30:12.389704	1
8465	25	Monções	moncoes	2013-09-03 15:30:12.389704	1
8466	25	Mongaguá	mongagua	2013-09-03 15:30:12.389704	1
8467	25	Monte Alegre do Sul	monte-alegre-do-sul	2013-09-03 15:30:12.389704	1
8468	25	Monte Alto	monte-alto	2013-09-03 15:30:12.389704	1
8469	25	Monte Aprazível	monte-aprazivel	2013-09-03 15:30:12.389704	1
8470	25	Monte Azul Paulista	monte-azul-paulista	2013-09-03 15:30:12.389704	1
8471	25	Monte Castelo	monte-castelo	2013-09-03 15:30:12.389704	1
8472	25	Monte Mor	monte-mor	2013-09-03 15:30:12.389704	1
8473	25	Monteiro Lobato	monteiro-lobato	2013-09-03 15:30:12.389704	1
8474	25	Morro Agudo	morro-agudo	2013-09-03 15:30:12.389704	1
8475	25	Morungaba	morungaba	2013-09-03 15:30:12.389704	1
8476	25	Motuca	motuca	2013-09-03 15:30:12.389704	1
8477	25	Murutinga do Sul	murutinga-do-sul	2013-09-03 15:30:12.389704	1
8478	25	Nantes	nantes	2013-09-03 15:30:12.389704	1
8479	25	Narandiba	narandiba	2013-09-03 15:30:12.389704	1
8480	25	Natividade da Serra	natividade-da-serra	2013-09-03 15:30:12.389704	1
8481	25	Nazaré Paulista	nazare-paulista	2013-09-03 15:30:12.389704	1
8482	25	Neves Paulista	neves-paulista	2013-09-03 15:30:12.389704	1
8483	25	Nhandeara	nhandeara	2013-09-03 15:30:12.389704	1
8484	25	Nipoã	nipoa	2013-09-03 15:30:12.389704	1
8485	25	Nova Aliança	nova-alianca	2013-09-03 15:30:12.389704	1
8486	25	Nova Campina	nova-campina	2013-09-03 15:30:12.389704	1
8487	25	Nova Canaã Paulista	nova-canaa-paulista	2013-09-03 15:30:12.389704	1
8488	25	Nova Castilho	nova-castilho	2013-09-03 15:30:12.389704	1
8489	25	Nova Europa	nova-europa	2013-09-03 15:30:12.389704	1
8490	25	Nova Granada	nova-granada	2013-09-03 15:30:12.389704	1
8491	25	Nova Guataporanga	nova-guataporanga	2013-09-03 15:30:12.389704	1
8492	25	Nova Independência	nova-independencia	2013-09-03 15:30:12.389704	1
8493	25	Nova Luzitânia	nova-luzitania	2013-09-03 15:30:12.389704	1
8494	25	Nova Odessa	nova-odessa	2013-09-03 15:30:12.389704	1
8495	25	Novais	novais	2013-09-03 15:30:12.389704	1
8496	25	Novo Horizonte	novo-horizonte	2013-09-03 15:30:12.389704	1
8497	25	Nuporanga	nuporanga	2013-09-03 15:30:12.389704	1
8498	25	Ocauçu	ocaucu	2013-09-03 15:30:12.389704	1
8499	25	Óleo	oleo	2013-09-03 15:30:12.389704	1
8500	25	Olímpia	olimpia	2013-09-03 15:30:12.389704	1
8501	25	Onda Verde	onda-verde	2013-09-03 15:30:12.389704	1
8502	25	Oriente	oriente	2013-09-03 15:30:12.389704	1
8503	25	Orindiúva	orindiuva	2013-09-03 15:30:12.389704	1
8504	25	Orlândia	orlandia	2013-09-03 15:30:12.389704	1
8505	25	Osasco	osasco	2013-09-03 15:30:12.389704	1
8506	25	Oscar Bressane	oscar-bressane	2013-09-03 15:30:12.389704	1
8507	25	Osvaldo Cruz	osvaldo-cruz	2013-09-03 15:30:12.389704	1
8508	25	Ourinhos	ourinhos	2013-09-03 15:30:12.389704	1
8509	25	Ouro Verde	ouro-verde	2013-09-03 15:30:12.389704	1
8510	25	Ouroeste	ouroeste	2013-09-03 15:30:12.389704	1
8511	25	Pacaembu	pacaembu	2013-09-03 15:30:12.389704	1
8512	25	Palestina	palestina	2013-09-03 15:30:12.389704	1
8513	25	Palmares Paulista	palmares-paulista	2013-09-03 15:30:12.389704	1
8514	25	Palmeira d`Oeste	palmeira-d-oeste	2013-09-03 15:30:12.389704	1
8515	25	Palmital	palmital	2013-09-03 15:30:12.389704	1
8516	25	Panorama	panorama	2013-09-03 15:30:12.389704	1
8517	25	Paraguaçu Paulista	paraguacu-paulista	2013-09-03 15:30:12.389704	1
8518	25	Paraibuna	paraibuna	2013-09-03 15:30:12.389704	1
8519	25	Paraíso	paraiso	2013-09-03 15:30:12.389704	1
8520	25	Paranapanema	paranapanema	2013-09-03 15:30:12.389704	1
8521	25	Paranapuã	paranapua	2013-09-03 15:30:12.389704	1
8522	25	Parapuã	parapua	2013-09-03 15:30:12.389704	1
8523	25	Pardinho	pardinho	2013-09-03 15:30:12.389704	1
8524	25	Pariquera-Açu	pariquera-acu	2013-09-03 15:30:12.389704	1
8525	25	Parisi	parisi	2013-09-03 15:30:12.389704	1
8526	25	Patrocínio Paulista	patrocinio-paulista	2013-09-03 15:30:12.389704	1
8527	25	Paulicéia	pauliceia	2013-09-03 15:30:12.389704	1
8528	25	Paulínia	paulinia	2013-09-03 15:30:12.389704	1
8529	25	Paulistânia	paulistania	2013-09-03 15:30:12.389704	1
8530	25	Paulo de Faria	paulo-de-faria	2013-09-03 15:30:12.389704	1
8531	25	Pederneiras	pederneiras	2013-09-03 15:30:12.389704	1
8532	25	Pedra Bela	pedra-bela	2013-09-03 15:30:12.389704	1
8533	25	Pedranópolis	pedranopolis	2013-09-03 15:30:12.389704	1
8534	25	Pedregulho	pedregulho	2013-09-03 15:30:12.389704	1
8535	25	Pedreira	pedreira	2013-09-03 15:30:12.389704	1
8536	25	Pedrinhas Paulista	pedrinhas-paulista	2013-09-03 15:30:12.389704	1
8537	25	Pedro de Toledo	pedro-de-toledo	2013-09-03 15:30:12.389704	1
8538	25	Penápolis	penapolis	2013-09-03 15:30:12.389704	1
8539	25	Pereira Barreto	pereira-barreto	2013-09-03 15:30:12.389704	1
8540	25	Pereiras	pereiras	2013-09-03 15:30:12.389704	1
8541	25	Peruíbe	peruibe	2013-09-03 15:30:12.389704	1
8542	25	Piacatu	piacatu	2013-09-03 15:30:12.389704	1
8543	25	Piedade	piedade	2013-09-03 15:30:12.389704	1
8544	25	Pilar do Sul	pilar-do-sul	2013-09-03 15:30:12.389704	1
8545	25	Pindamonhangaba	pindamonhangaba	2013-09-03 15:30:12.389704	1
8546	25	Pindorama	pindorama	2013-09-03 15:30:12.389704	1
8547	25	Pinhalzinho	pinhalzinho	2013-09-03 15:30:12.389704	1
8548	25	Piquerobi	piquerobi	2013-09-03 15:30:12.389704	1
8549	25	Piquete	piquete	2013-09-03 15:30:12.389704	1
8550	25	Piracaia	piracaia	2013-09-03 15:30:12.389704	1
8551	25	Piracicaba	piracicaba	2013-09-03 15:30:12.389704	1
8552	25	Piraju	piraju	2013-09-03 15:30:12.389704	1
8553	25	Pirajuí	pirajui	2013-09-03 15:30:12.389704	1
8554	25	Pirangi	pirangi	2013-09-03 15:30:12.389704	1
8555	25	Pirapora do Bom Jesus	pirapora-do-bom-jesus	2013-09-03 15:30:12.389704	1
8556	25	Pirapozinho	pirapozinho	2013-09-03 15:30:12.389704	1
8557	25	Pirassununga	pirassununga	2013-09-03 15:30:12.389704	1
8558	25	Piratininga	piratininga	2013-09-03 15:30:12.389704	1
8559	25	Pitangueiras	pitangueiras	2013-09-03 15:30:12.389704	1
8560	25	Planalto	planalto	2013-09-03 15:30:12.389704	1
8561	25	Platina	platina	2013-09-03 15:30:12.389704	1
8562	25	Poá	poa	2013-09-03 15:30:12.389704	1
8563	25	Poloni	poloni	2013-09-03 15:30:12.389704	1
8564	25	Pompéia	pompeia	2013-09-03 15:30:12.389704	1
8565	25	Pongaí	pongai	2013-09-03 15:30:12.389704	1
8566	25	Pontal	pontal	2013-09-03 15:30:12.389704	1
8567	25	Pontalinda	pontalinda	2013-09-03 15:30:12.389704	1
8568	25	Pontes Gestal	pontes-gestal	2013-09-03 15:30:12.389704	1
8569	25	Populina	populina	2013-09-03 15:30:12.389704	1
8570	25	Porangaba	porangaba	2013-09-03 15:30:12.389704	1
8571	25	Porto Feliz	porto-feliz	2013-09-03 15:30:12.389704	1
8572	25	Porto Ferreira	porto-ferreira	2013-09-03 15:30:12.389704	1
8573	25	Potim	potim	2013-09-03 15:30:12.389704	1
8574	25	Potirendaba	potirendaba	2013-09-03 15:30:12.389704	1
8575	25	Pracinha	pracinha	2013-09-03 15:30:12.389704	1
8576	25	Pradópolis	pradopolis	2013-09-03 15:30:12.389704	1
8577	25	Praia Grande	praia-grande	2013-09-03 15:30:12.389704	1
8578	25	Pratânia	pratania	2013-09-03 15:30:12.389704	1
8579	25	Presidente Alves	presidente-alves	2013-09-03 15:30:12.389704	1
8580	25	Presidente Bernardes	presidente-bernardes	2013-09-03 15:30:12.389704	1
8581	25	Presidente Epitácio	presidente-epitacio	2013-09-03 15:30:12.389704	1
8582	25	Presidente Prudente	presidente-prudente	2013-09-03 15:30:12.389704	1
8583	25	Presidente Venceslau	presidente-venceslau	2013-09-03 15:30:12.389704	1
8584	25	Promissão	promissao	2013-09-03 15:30:12.389704	1
8585	25	Quadra	quadra	2013-09-03 15:30:12.389704	1
8586	25	Quatá	quata	2013-09-03 15:30:12.389704	1
8587	25	Queiroz	queiroz	2013-09-03 15:30:12.389704	1
8588	25	Queluz	queluz	2013-09-03 15:30:12.389704	1
8589	25	Quintana	quintana	2013-09-03 15:30:12.389704	1
8590	25	Rafard	rafard	2013-09-03 15:30:12.389704	1
8591	25	Rancharia	rancharia	2013-09-03 15:30:12.389704	1
8592	25	Redenção da Serra	redencao-da-serra	2013-09-03 15:30:12.389704	1
8593	25	Regente Feijó	regente-feijo	2013-09-03 15:30:12.389704	1
8594	25	Reginópolis	reginopolis	2013-09-03 15:30:12.389704	1
8595	25	Registro	registro	2013-09-03 15:30:12.389704	1
8596	25	Restinga	restinga	2013-09-03 15:30:12.389704	1
8597	25	Ribeira	ribeira	2013-09-03 15:30:12.389704	1
8598	25	Ribeirão Bonito	ribeirao-bonito	2013-09-03 15:30:12.389704	1
8599	25	Ribeirão Branco	ribeirao-branco	2013-09-03 15:30:12.389704	1
8600	25	Ribeirão Corrente	ribeirao-corrente	2013-09-03 15:30:12.389704	1
8601	25	Ribeirão do Sul	ribeirao-do-sul	2013-09-03 15:30:12.389704	1
8602	25	Ribeirão dos Índios	ribeirao-dos-indios	2013-09-03 15:30:12.389704	1
8603	25	Ribeirão Grande	ribeirao-grande	2013-09-03 15:30:12.389704	1
8604	25	Ribeirão Pires	ribeirao-pires	2013-09-03 15:30:12.389704	1
8605	25	Ribeirão Preto	ribeirao-preto	2013-09-03 15:30:12.389704	1
8606	25	Rifaina	rifaina	2013-09-03 15:30:12.389704	1
8607	25	Rincão	rincao	2013-09-03 15:30:12.389704	1
8608	25	Rinópolis	rinopolis	2013-09-03 15:30:12.389704	1
8609	25	Rio Claro	rio-claro	2013-09-03 15:30:12.389704	1
8610	25	Rio das Pedras	rio-das-pedras	2013-09-03 15:30:12.389704	1
8611	25	Rio Grande da Serra	rio-grande-da-serra	2013-09-03 15:30:12.389704	1
8612	25	Riolândia	riolandia	2013-09-03 15:30:12.389704	1
8613	25	Riversul	riversul	2013-09-03 15:30:12.389704	1
8614	25	Rosana	rosana	2013-09-03 15:30:12.389704	1
8615	25	Roseira	roseira	2013-09-03 15:30:12.389704	1
8616	25	Rubiácea	rubiacea	2013-09-03 15:30:12.389704	1
8617	25	Rubinéia	rubineia	2013-09-03 15:30:12.389704	1
8618	25	Sabino	sabino	2013-09-03 15:30:12.389704	1
8619	25	Sagres	sagres	2013-09-03 15:30:12.389704	1
8620	25	Sales	sales	2013-09-03 15:30:12.389704	1
8621	25	Sales Oliveira	sales-oliveira	2013-09-03 15:30:12.389704	1
8622	25	Salesópolis	salesopolis	2013-09-03 15:30:12.389704	1
8623	25	Salmourão	salmourao	2013-09-03 15:30:12.389704	1
8624	25	Saltinho	saltinho	2013-09-03 15:30:12.389704	1
8625	25	Salto	salto	2013-09-03 15:30:12.389704	1
8626	25	Salto de Pirapora	salto-de-pirapora	2013-09-03 15:30:12.389704	1
8627	25	Salto Grande	salto-grande	2013-09-03 15:30:12.389704	1
8628	25	Sandovalina	sandovalina	2013-09-03 15:30:12.389704	1
8629	25	Santa Adélia	santa-adelia	2013-09-03 15:30:12.389704	1
8630	25	Santa Albertina	santa-albertina	2013-09-03 15:30:12.389704	1
8631	25	Santa Bárbara d`Oeste	santa-barbara-d-oeste	2013-09-03 15:30:12.389704	1
8632	25	Santa Branca	santa-branca	2013-09-03 15:30:12.389704	1
8633	25	Santa Clara d`Oeste	santa-clara-d-oeste	2013-09-03 15:30:12.389704	1
8634	25	Santa Cruz da Conceição	santa-cruz-da-conceicao	2013-09-03 15:30:12.389704	1
8635	25	Santa Cruz da Esperança	santa-cruz-da-esperanca	2013-09-03 15:30:12.389704	1
8636	25	Santa Cruz das Palmeiras	santa-cruz-das-palmeiras	2013-09-03 15:30:12.389704	1
8637	25	Santa Cruz do Rio Pardo	santa-cruz-do-rio-pardo	2013-09-03 15:30:12.389704	1
8638	25	Santa Ernestina	santa-ernestina	2013-09-03 15:30:12.389704	1
8639	25	Santa Fé do Sul	santa-fe-do-sul	2013-09-03 15:30:12.389704	1
8640	25	Santa Gertrudes	santa-gertrudes	2013-09-03 15:30:12.389704	1
8641	25	Santa Isabel	santa-isabel	2013-09-03 15:30:12.389704	1
8642	25	Santa Lúcia	santa-lucia	2013-09-03 15:30:12.389704	1
8643	25	Santa Maria da Serra	santa-maria-da-serra	2013-09-03 15:30:12.389704	1
8644	25	Santa Mercedes	santa-mercedes	2013-09-03 15:30:12.389704	1
8645	25	Santa Rita d`Oeste	santa-rita-d-oeste	2013-09-03 15:30:12.389704	1
8646	25	Santa Rita do Passa Quatro	santa-rita-do-passa-quatro	2013-09-03 15:30:12.389704	1
8647	25	Santa Rosa de Viterbo	santa-rosa-de-viterbo	2013-09-03 15:30:12.389704	1
8648	25	Santa Salete	santa-salete	2013-09-03 15:30:12.389704	1
8649	25	Santana da Ponte Pensa	santana-da-ponte-pensa	2013-09-03 15:30:12.389704	1
8650	25	Santana de Parnaíba	santana-de-parnaiba	2013-09-03 15:30:12.389704	1
8651	25	Santo Anastácio	santo-anastacio	2013-09-03 15:30:12.389704	1
8652	25	Santo André	santo-andre	2013-09-03 15:30:12.389704	1
8653	25	Santo Antônio da Alegria	santo-antonio-da-alegria	2013-09-03 15:30:12.389704	1
8654	25	Santo Antônio de Posse	santo-antonio-de-posse	2013-09-03 15:30:12.389704	1
8655	25	Santo Antônio do Aracanguá	santo-antonio-do-aracangua	2013-09-03 15:30:12.389704	1
8656	25	Santo Antônio do Jardim	santo-antonio-do-jardim	2013-09-03 15:30:12.389704	1
8657	25	Santo Antônio do Pinhal	santo-antonio-do-pinhal	2013-09-03 15:30:12.389704	1
8658	25	Santo Expedito	santo-expedito	2013-09-03 15:30:12.389704	1
8659	25	Santópolis do Aguapeí	santopolis-do-aguapei	2013-09-03 15:30:12.389704	1
8661	25	São Bento do Sapucaí	sao-bento-do-sapucai	2013-09-03 15:30:12.389704	1
8662	25	São Bernardo do Campo	sao-bernardo-do-campo	2013-09-03 15:30:12.389704	1
8663	25	São Caetano do Sul	sao-caetano-do-sul	2013-09-03 15:30:12.389704	1
8664	25	São Carlos	sao-carlos	2013-09-03 15:30:12.389704	1
8665	25	São Francisco	sao-francisco	2013-09-03 15:30:12.389704	1
8666	25	São João da Boa Vista	sao-joao-da-boa-vista	2013-09-03 15:30:12.389704	1
8667	25	São João das Duas Pontes	sao-joao-das-duas-pontes	2013-09-03 15:30:12.389704	1
8668	25	São João de Iracema	sao-joao-de-iracema	2013-09-03 15:30:12.389704	1
8669	25	São João do Pau d`Alho	sao-joao-do-pau-d-alho	2013-09-03 15:30:12.389704	1
8670	25	São Joaquim da Barra	sao-joaquim-da-barra	2013-09-03 15:30:12.389704	1
8671	25	São José da Bela Vista	sao-jose-da-bela-vista	2013-09-03 15:30:12.389704	1
8672	25	São José do Barreiro	sao-jose-do-barreiro	2013-09-03 15:30:12.389704	1
8673	25	São José do Rio Pardo	sao-jose-do-rio-pardo	2013-09-03 15:30:12.389704	1
8674	25	São José do Rio Preto	sao-jose-do-rio-preto	2013-09-03 15:30:12.389704	1
8675	25	São José dos Campos	sao-jose-dos-campos	2013-09-03 15:30:12.389704	1
8676	25	São Lourenço da Serra	sao-lourenco-da-serra	2013-09-03 15:30:12.389704	1
8677	25	São Luís do Paraitinga	sao-luis-do-paraitinga	2013-09-03 15:30:12.389704	1
8678	25	São Manuel	sao-manuel	2013-09-03 15:30:12.389704	1
8679	25	São Miguel Arcanjo	sao-miguel-arcanjo	2013-09-03 15:30:12.389704	1
8681	25	São Pedro	sao-pedro	2013-09-03 15:30:12.389704	1
8682	25	São Pedro do Turvo	sao-pedro-do-turvo	2013-09-03 15:30:12.389704	1
8683	25	São Roque	sao-roque	2013-09-03 15:30:12.389704	1
8684	25	São Sebastião	sao-sebastiao	2013-09-03 15:30:12.389704	1
8685	25	São Sebastião da Grama	sao-sebastiao-da-grama	2013-09-03 15:30:12.389704	1
8686	25	São Simão	sao-simao	2013-09-03 15:30:12.389704	1
8687	25	São Vicente	sao-vicente	2013-09-03 15:30:12.389704	1
8688	25	Sarapuí	sarapui	2013-09-03 15:30:12.389704	1
8689	25	Sarutaiá	sarutaia	2013-09-03 15:30:12.389704	1
8690	25	Sebastianópolis do Sul	sebastianopolis-do-sul	2013-09-03 15:30:12.389704	1
8691	25	Serra Azul	serra-azul	2013-09-03 15:30:12.389704	1
8692	25	Serra Negra	serra-negra	2013-09-03 15:30:12.389704	1
8693	25	Serrana	serrana	2013-09-03 15:30:12.389704	1
8694	25	Sertãozinho	sertaozinho	2013-09-03 15:30:12.389704	1
8695	25	Sete Barras	sete-barras	2013-09-03 15:30:12.389704	1
8696	25	Severínia	severinia	2013-09-03 15:30:12.389704	1
8697	25	Silveiras	silveiras	2013-09-03 15:30:12.389704	1
8698	25	Socorro	socorro	2013-09-03 15:30:12.389704	1
8699	25	Sorocaba	sorocaba	2013-09-03 15:30:12.389704	1
8700	25	Sud Mennucci	sud-mennucci	2013-09-03 15:30:12.389704	1
8701	25	Sumaré	sumare	2013-09-03 15:30:12.389704	1
8702	25	Suzanápolis	suzanapolis	2013-09-03 15:30:12.389704	1
8703	25	Suzano	suzano	2013-09-03 15:30:12.389704	1
8704	25	Tabapuã	tabapua	2013-09-03 15:30:12.389704	1
8705	25	Tabatinga	tabatinga	2013-09-03 15:30:12.389704	1
8706	25	Taboão da Serra	taboao-da-serra	2013-09-03 15:30:12.389704	1
8707	25	Taciba	taciba	2013-09-03 15:30:12.389704	1
8708	25	Taguaí	taguai	2013-09-03 15:30:12.389704	1
8709	25	Taiaçu	taiacu	2013-09-03 15:30:12.389704	1
8710	25	Taiúva	taiuva	2013-09-03 15:30:12.389704	1
8711	25	Tambaú	tambau	2013-09-03 15:30:12.389704	1
8712	25	Tanabi	tanabi	2013-09-03 15:30:12.389704	1
8713	25	Tapiraí	tapirai	2013-09-03 15:30:12.389704	1
8714	25	Tapiratiba	tapiratiba	2013-09-03 15:30:12.389704	1
8715	25	Taquaral	taquaral	2013-09-03 15:30:12.389704	1
8716	25	Taquaritinga	taquaritinga	2013-09-03 15:30:12.389704	1
8717	25	Taquarituba	taquarituba	2013-09-03 15:30:12.389704	1
8718	25	Taquarivaí	taquarivai	2013-09-03 15:30:12.389704	1
8719	25	Tarabai	tarabai	2013-09-03 15:30:12.389704	1
8720	25	Tarumã	taruma	2013-09-03 15:30:12.389704	1
8721	25	Tatuí	tatui	2013-09-03 15:30:12.389704	1
8722	25	Taubaté	taubate	2013-09-03 15:30:12.389704	1
8723	25	Tejupá	tejupa	2013-09-03 15:30:12.389704	1
8724	25	Teodoro Sampaio	teodoro-sampaio	2013-09-03 15:30:12.389704	1
8725	25	Terra Roxa	terra-roxa	2013-09-03 15:30:12.389704	1
8726	25	Tietê	tiete	2013-09-03 15:30:12.389704	1
8727	25	Timburi	timburi	2013-09-03 15:30:12.389704	1
8728	25	Torre de Pedra	torre-de-pedra	2013-09-03 15:30:12.389704	1
8729	25	Torrinha	torrinha	2013-09-03 15:30:12.389704	1
8730	25	Trabiju	trabiju	2013-09-03 15:30:12.389704	1
8731	25	Tremembé	tremembe	2013-09-03 15:30:12.389704	1
8732	25	Três Fronteiras	tres-fronteiras	2013-09-03 15:30:12.389704	1
8733	25	Tuiuti	tuiuti	2013-09-03 15:30:12.389704	1
8734	25	Tupã	tupa	2013-09-03 15:30:12.389704	1
8735	25	Tupi Paulista	tupi-paulista	2013-09-03 15:30:12.389704	1
8736	25	Turiúba	turiuba	2013-09-03 15:30:12.389704	1
8737	25	Turmalina	turmalina	2013-09-03 15:30:12.389704	1
8738	25	Ubarana	ubarana	2013-09-03 15:30:12.389704	1
8739	25	Ubatuba	ubatuba	2013-09-03 15:30:12.389704	1
8740	25	Ubirajara	ubirajara	2013-09-03 15:30:12.389704	1
8741	25	Uchoa	uchoa	2013-09-03 15:30:12.389704	1
8742	25	União Paulista	uniao-paulista	2013-09-03 15:30:12.389704	1
8743	25	Urânia	urania	2013-09-03 15:30:12.389704	1
8744	25	Uru	uru	2013-09-03 15:30:12.389704	1
8745	25	Urupês	urupes	2013-09-03 15:30:12.389704	1
8746	25	Valentim Gentil	valentim-gentil	2013-09-03 15:30:12.389704	1
8747	25	Valinhos	valinhos	2013-09-03 15:30:12.389704	1
8748	25	Valparaíso	valparaiso	2013-09-03 15:30:12.389704	1
8749	25	Vargem	vargem	2013-09-03 15:30:12.389704	1
8750	25	Vargem Grande do Sul	vargem-grande-do-sul	2013-09-03 15:30:12.389704	1
8751	25	Vargem Grande Paulista	vargem-grande-paulista	2013-09-03 15:30:12.389704	1
8752	25	Várzea Paulista	varzea-paulista	2013-09-03 15:30:12.389704	1
8753	25	Vera Cruz	vera-cruz	2013-09-03 15:30:12.389704	1
8754	25	Vinhedo	vinhedo	2013-09-03 15:30:12.389704	1
8755	25	Viradouro	viradouro	2013-09-03 15:30:12.389704	1
8756	25	Vista Alegre do Alto	vista-alegre-do-alto	2013-09-03 15:30:12.389704	1
8757	25	Vitória Brasil	vitoria-brasil	2013-09-03 15:30:12.389704	1
8758	25	Votorantim	votorantim	2013-09-03 15:30:12.389704	1
8759	25	Votuporanga	votuporanga	2013-09-03 15:30:12.389704	1
8760	25	Zacarias	zacarias	2013-09-03 15:30:12.389704	1
9139	27	Abreulândia	abreulandia	2013-09-03 15:30:12.389704	1
9140	27	Aguiarnópolis	aguiarnopolis	2013-09-03 15:30:12.389704	1
9141	27	Aliança do Tocantins	alianca-do-tocantins	2013-09-03 15:30:12.389704	1
9142	27	Almas	almas	2013-09-03 15:30:12.389704	1
9143	27	Alvorada	alvorada	2013-09-03 15:30:12.389704	1
9144	27	Ananás	ananas	2013-09-03 15:30:12.389704	1
9145	27	Angico	angico	2013-09-03 15:30:12.389704	1
9146	27	Aparecida do Rio\nNegro	aparecida-do-rio-negro	2013-09-03 15:30:12.389704	1
9147	27	Aragominas	aragominas	2013-09-03 15:30:12.389704	1
9148	27	Araguacema	araguacema	2013-09-03 15:30:12.389704	1
9149	27	Araguaçu	araguacu	2013-09-03 15:30:12.389704	1
9150	27	Araguaína	araguaina	2013-09-03 15:30:12.389704	1
9151	27	Araguanã	araguana	2013-09-03 15:30:12.389704	1
9152	27	Araguatins	araguatins	2013-09-03 15:30:12.389704	1
9153	27	Arapoema	arapoema	2013-09-03 15:30:12.389704	1
9154	27	Arraias	arraias	2013-09-03 15:30:12.389704	1
9155	27	Augustinópolis	augustinopolis	2013-09-03 15:30:12.389704	1
9156	27	Aurora do Tocantins	aurora-do-tocantins	2013-09-03 15:30:12.389704	1
9157	27	Axixá do Tocantins	axixa-do-tocantins	2013-09-03 15:30:12.389704	1
9158	27	Babaçulândia	babaculandia	2013-09-03 15:30:12.389704	1
9159	27	Bandeirantes do Tocantins	bandeirantes-do-tocantins	2013-09-03 15:30:12.389704	1
9160	27	Barra do Ouro	barra-do-ouro	2013-09-03 15:30:12.389704	1
9161	27	Barrolândia	barrolandia	2013-09-03 15:30:12.389704	1
9162	27	Bernardo Sayão	bernardo-sayao	2013-09-03 15:30:12.389704	1
9163	27	Bom Jesus do Tocantins	bom-jesus-do-tocantins	2013-09-03 15:30:12.389704	1
9164	27	Brasilândia do Tocantins	brasilandia-do-tocantins	2013-09-03 15:30:12.389704	1
9165	27	Brejinho de Nazaré	brejinho-de-nazare	2013-09-03 15:30:12.389704	1
9166	27	Buriti do Tocantins	buriti-do-tocantins	2013-09-03 15:30:12.389704	1
9167	27	Cachoeirinha	cachoeirinha	2013-09-03 15:30:12.389704	1
9168	27	Campos Lindos	campos-lindos	2013-09-03 15:30:12.389704	1
9169	27	Cariri do Tocantins	cariri-do-tocantins	2013-09-03 15:30:12.389704	1
9170	27	Carmolândia	carmolandia	2013-09-03 15:30:12.389704	1
9171	27	Carrasco Bonito	carrasco-bonito	2013-09-03 15:30:12.389704	1
9172	27	Caseara	caseara	2013-09-03 15:30:12.389704	1
9173	27	Centenário	centenario	2013-09-03 15:30:12.389704	1
9174	27	Chapada da Natividade	chapada-da-natividade	2013-09-03 15:30:12.389704	1
9175	27	Chapada de Areia	chapada-de-areia	2013-09-03 15:30:12.389704	1
9176	27	Colinas do Tocantins	colinas-do-tocantins	2013-09-03 15:30:12.389704	1
9177	27	Colméia	colmeia	2013-09-03 15:30:12.389704	1
9178	27	Combinado	combinado	2013-09-03 15:30:12.389704	1
9179	27	Conceição do Tocantins	conceicao-do-tocantins	2013-09-03 15:30:12.389704	1
9180	27	Couto de Magalhães	couto-de-magalhaes	2013-09-03 15:30:12.389704	1
9181	27	Cristalândia	cristalandia	2013-09-03 15:30:12.389704	1
9182	27	Crixás do Tocantins	crixas-do-tocantins	2013-09-03 15:30:12.389704	1
9183	27	Darcinópolis	darcinopolis	2013-09-03 15:30:12.389704	1
9184	27	Dianópolis	dianopolis	2013-09-03 15:30:12.389704	1
9185	27	Divinópolis do Tocantins	divinopolis-do-tocantins	2013-09-03 15:30:12.389704	1
9186	27	Dois Irmãos do Tocantins	dois-irmaos-do-tocantins	2013-09-03 15:30:12.389704	1
9187	27	Dueré	duere	2013-09-03 15:30:12.389704	1
9188	27	Esperantina	esperantina	2013-09-03 15:30:12.389704	1
9189	27	Fátima	fatima	2013-09-03 15:30:12.389704	1
9190	27	Figueirópolis	figueiropolis	2013-09-03 15:30:12.389704	1
9191	27	Filadélfia	filadelfia	2013-09-03 15:30:12.389704	1
9192	27	Formoso do Araguaia	formoso-do-araguaia	2013-09-03 15:30:12.389704	1
9193	27	Fortaleza do Tabocão	fortaleza-do-tabocao	2013-09-03 15:30:12.389704	1
9194	27	Goianorte	goianorte	2013-09-03 15:30:12.389704	1
9195	27	Goiatins	goiatins	2013-09-03 15:30:12.389704	1
9196	27	Guaraí	guarai	2013-09-03 15:30:12.389704	1
9197	27	Gurupi	gurupi	2013-09-03 15:30:12.389704	1
9198	27	Ipueiras	ipueiras	2013-09-03 15:30:12.389704	1
9199	27	Itacajá	itacaja	2013-09-03 15:30:12.389704	1
9200	27	Itaguatins	itaguatins	2013-09-03 15:30:12.389704	1
9201	27	Itapiratins	itapiratins	2013-09-03 15:30:12.389704	1
9202	27	Itaporã do Tocantins	itapora-do-tocantins	2013-09-03 15:30:12.389704	1
9203	27	Jaú do Tocantins	jau-do-tocantins	2013-09-03 15:30:12.389704	1
9204	27	Juarina	juarina	2013-09-03 15:30:12.389704	1
9205	27	Lagoa da Confusão	lagoa-da-confusao	2013-09-03 15:30:12.389704	1
9206	27	Lagoa do Tocantins	lagoa-do-tocantins	2013-09-03 15:30:12.389704	1
9207	27	Lajeado	lajeado	2013-09-03 15:30:12.389704	1
9208	27	Lavandeira	lavandeira	2013-09-03 15:30:12.389704	1
9209	27	Lizarda	lizarda	2013-09-03 15:30:12.389704	1
9210	27	Luzinópolis	luzinopolis	2013-09-03 15:30:12.389704	1
9211	27	Marianópolis do Tocantins	marianopolis-do-tocantins	2013-09-03 15:30:12.389704	1
9212	27	Mateiros	mateiros	2013-09-03 15:30:12.389704	1
9213	27	Maurilândia do Tocantins	maurilandia-do-tocantins	2013-09-03 15:30:12.389704	1
9214	27	Miracema do Tocantins	miracema-do-tocantins	2013-09-03 15:30:12.389704	1
9215	27	Miranorte	miranorte	2013-09-03 15:30:12.389704	1
9216	27	Monte do Carmo	monte-do-carmo	2013-09-03 15:30:12.389704	1
9217	27	Monte Santo do Tocantins	monte-santo-do-tocantins	2013-09-03 15:30:12.389704	1
9218	27	Muricilândia	muricilandia	2013-09-03 15:30:12.389704	1
9219	27	Natividade	natividade	2013-09-03 15:30:12.389704	1
9220	27	Nazaré	nazare	2013-09-03 15:30:12.389704	1
9221	27	Nova Olinda	nova-olinda	2013-09-03 15:30:12.389704	1
9222	27	Nova Rosalândia	nova-rosalandia	2013-09-03 15:30:12.389704	1
9223	27	Novo Acordo	novo-acordo	2013-09-03 15:30:12.389704	1
9224	27	Novo Alegre	novo-alegre	2013-09-03 15:30:12.389704	1
9225	27	Novo Jardim	novo-jardim	2013-09-03 15:30:12.389704	1
9226	27	Oliveira de Fátima	oliveira-de-fatima	2013-09-03 15:30:12.389704	1
9227	27	Palmas	palmas	2013-09-03 15:30:12.389704	1
9228	27	Palmeirante	palmeirante	2013-09-03 15:30:12.389704	1
9229	27	Palmeiras do Tocantins	palmeiras-do-tocantins	2013-09-03 15:30:12.389704	1
9230	27	Palmeirópolis	palmeiropolis	2013-09-03 15:30:12.389704	1
9231	27	Paraíso do Tocantins	paraiso-do-tocantins	2013-09-03 15:30:12.389704	1
9232	27	Paranã	parana	2013-09-03 15:30:12.389704	1
9233	27	Pau d`Arco	pau-d-arco	2013-09-03 15:30:12.389704	1
9234	27	Pedro Afonso	pedro-afonso	2013-09-03 15:30:12.389704	1
9235	27	Peixe	peixe	2013-09-03 15:30:12.389704	1
9236	27	Pequizeiro	pequizeiro	2013-09-03 15:30:12.389704	1
9237	27	Pindorama do Tocantins	pindorama-do-tocantins	2013-09-03 15:30:12.389704	1
9238	27	Piraquê	piraque	2013-09-03 15:30:12.389704	1
9239	27	Pium	pium	2013-09-03 15:30:12.389704	1
9240	27	Ponte Alta do Bom Jesus	ponte-alta-do-bom-jesus	2013-09-03 15:30:12.389704	1
9241	27	Ponte Alta do Tocantins	ponte-alta-do-tocantins	2013-09-03 15:30:12.389704	1
9242	27	Porto Alegre do Tocantins	porto-alegre-do-tocantins	2013-09-03 15:30:12.389704	1
9243	27	Porto Nacional	porto-nacional	2013-09-03 15:30:12.389704	1
9244	27	Praia Norte	praia-norte	2013-09-03 15:30:12.389704	1
9245	27	Presidente Kennedy	presidente-kennedy	2013-09-03 15:30:12.389704	1
9246	27	Pugmil	pugmil	2013-09-03 15:30:12.389704	1
9247	27	Recursolândia	recursolandia	2013-09-03 15:30:12.389704	1
9248	27	Riachinho	riachinho	2013-09-03 15:30:12.389704	1
9249	27	Rio da Conceição	rio-da-conceicao	2013-09-03 15:30:12.389704	1
9250	27	Rio dos Bois	rio-dos-bois	2013-09-03 15:30:12.389704	1
9251	27	Rio Sono	rio-sono	2013-09-03 15:30:12.389704	1
9252	27	Sampaio	sampaio	2013-09-03 15:30:12.389704	1
9253	27	Sandolândia	sandolandia	2013-09-03 15:30:12.389704	1
9254	27	Santa Fé do Araguaia	santa-fe-do-araguaia	2013-09-03 15:30:12.389704	1
9255	27	Santa Maria do Tocantins	santa-maria-do-tocantins	2013-09-03 15:30:12.389704	1
9256	27	Santa Rita do Tocantins	santa-rita-do-tocantins	2013-09-03 15:30:12.389704	1
9257	27	Santa Rosa do Tocantins	santa-rosa-do-tocantins	2013-09-03 15:30:12.389704	1
9258	27	Santa Tereza do Tocantins	santa-tereza-do-tocantins	2013-09-03 15:30:12.389704	1
9259	27	Santa Terezinha do Tocantins	santa-terezinha-do-tocantins	2013-09-03 15:30:12.389704	1
9260	27	São Bento do Tocantins	sao-bento-do-tocantins	2013-09-03 15:30:12.389704	1
9261	27	São Félix do Tocantins	sao-felix-do-tocantins	2013-09-03 15:30:12.389704	1
9262	27	São Miguel do Tocantins	sao-miguel-do-tocantins	2013-09-03 15:30:12.389704	1
9263	27	São Salvador do Tocantins	sao-salvador-do-tocantins	2013-09-03 15:30:12.389704	1
9264	27	São Sebastião do Tocantins	sao-sebastiao-do-tocantins	2013-09-03 15:30:12.389704	1
9265	27	São Valério da Natividade	sao-valerio-da-natividade	2013-09-03 15:30:12.389704	1
9266	27	Silvanópolis	silvanopolis	2013-09-03 15:30:12.389704	1
9267	27	Sítio Novo do Tocantins	sitio-novo-do-tocantins	2013-09-03 15:30:12.389704	1
9268	27	Sucupira	sucupira	2013-09-03 15:30:12.389704	1
9269	27	Taguatinga	taguatinga	2013-09-03 15:30:12.389704	1
9270	27	Taipas do Tocantins	taipas-do-tocantins	2013-09-03 15:30:12.389704	1
9271	27	Talismã	talisma	2013-09-03 15:30:12.389704	1
9272	27	Tocantínia	tocantinia	2013-09-03 15:30:12.389704	1
9273	27	Tocantinópolis	tocantinopolis	2013-09-03 15:30:12.389704	1
9274	27	Tupirama	tupirama	2013-09-03 15:30:12.389704	1
9275	27	Tupiratins	tupiratins	2013-09-03 15:30:12.389704	1
9276	27	Wanderlândia	wanderlandia	2013-09-03 15:30:12.389704	1
9277	27	Xambioá	xambioa	2013-09-03 15:30:12.389704	1
\.


--
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('city_id_seq', 2, true);


--
-- Data for Name: fuel_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY fuel_type (id, type) FROM stdin;
1	Gasolina
2	Álcool
3	Flex
4	Diesel
5	Outro
\.


--
-- Name: fuel_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('fuel_type_id_seq', 5, true);


--
-- PostgreSQL database dump complete
--



COMMIT;
