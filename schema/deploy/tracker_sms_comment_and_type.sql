-- Deploy trackware:tracker_sms_comment_and_type to pg

BEGIN;

ALTER TABLE tracker_sms_alert
  ADD COLUMN note TEXT,
  ADD COLUMN alert_type TEXT;
      

COMMIT;
