-- Deploy area-notify

BEGIN;
CREATE EXTENSION IF NOT EXISTS postgis ;
CREATE TABLE notification  (
  id SERIAL PRIMARY KEY,
  message TEXT NOT NULL,
  rule JSON NOT NULL,
  create_ts TIMESTAMP WITHOUT TIME ZONE DEFAULT now()
  );

CREATE TABLE notification_area  (
  notification_id INTEGER NOT NULL REFERENCES notification(id),
  center geography(Point) NOT NULL,
  radius INTEGER NOT NULL
);


CREATE TABLE tracker_notification (
  notification_id INTEGER NOT NULL REFERENCES notification(id),
  tracker_id INTEGER NOT NULL REFERENCES tracker(id),
  create_ts TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  UNIQUE(notification_id, tracker_id)
);

CREATE TABLE tracker_notification_sms (
  notification_id INTEGER NOT NULL REFERENCES notification(id),
  tracker_id INTEGER NOT NULL REFERENCES tracker(id),
  sms_tracker_id INTEGER NOT NULL REFERENCES sms_tracker(id),
 FOREIGN KEY (notification_id , tracker_id) REFERENCES tracker_notification (notification_id , tracker_id)
);

COMMIT;
