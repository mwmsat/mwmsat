-- Deploy trackware:pre-registration-network-supplier to pg

BEGIN;

ALTER TABLE business_unity_pre_registration
  ADD COLUMN network_supplier_id INTEGER REFERENCES network_supplier(id);

COMMIT;
