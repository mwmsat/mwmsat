-- Deploy trackware:tracker-position-ts-desc-index to pg

BEGIN;

CREATE INDEX tracker_position_tracker_id_event_date__desc_idx ON tracker_position(tracker_id, event_date DESC);

COMMIT;
