-- Deploy tracker-code

BEGIN;

-- XXX Add DDLs here.

ALTER TABLE tracker ADD COLUMN code TEXT;

CREATE UNIQUE INDEX tracker_code_unique__idx ON tracker(code) WHERE code IS NOT NULL;


COMMIT;
