-- Deploy trackware:tracker_chip_iccid_unique to pg

BEGIN;

ALTER TABLE tracker ALTER COLUMN phone_number SET NOT NULL;
CREATE UNIQUE INDEX tracker_phone_number_active  ON tracker(phone_number) WHERE active IS TRUE;

COMMIT;
