-- Deploy trackware:tracker-position-add-interval to pg

BEGIN;

ALTER TABLE tracker_position ADD COLUMN interval INTEGER;

COMMIT;
