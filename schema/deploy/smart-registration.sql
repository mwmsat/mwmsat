-- Deploy trackware:smart-registration to pg

BEGIN;


ALTER TABLE business_unity ADD COLUMN code TEXT UNIQUE;

CREATE TABLE tracker_profile (
  id SERIAL PRIMARY KEY NOT NULL,
  name TEXT NOT NULL UNIQUE,
  description TEXT NOT NULL,
  created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now()
);

ALTER TABLE tracker ADD COLUMN profile_id INTEGER REFERENCES tracker_profile(id);

CREATE TABLE business_unity_pre_registration (
  business_unity_id INTEGER REFERENCES business_unity(id),
  seq INTEGER NOT NULL,
  used_by INTEGER REFERENCES tracker(id),
  primary key(business_unity_id, seq)
);

CREATE OR REPLACE FUNCTION make_pre_registration_seq() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  execute format('CREATE SEQUENCE IF NOT EXISTS pre_registration_seq_%s', NEW.business_unity_id);
  return NEW;
end
$$;

CREATE TRIGGER make_pre_registration_seq_f
  BEFORE INSERT ON business_unity_pre_registration
  FOR EACH ROW EXECUTE PROCEDURE make_pre_registration_seq();


CREATE OR REPLACE FUNCTION fill_in_stuff_seq() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  NEW.seq := nextval('pre_registration_seq_' || NEW.business_unity_id);
  RETURN NEW;
end
$$;

CREATE TRIGGER fill_in_stuff_seq_f
  BEFORE INSERT ON business_unity_pre_registration
  FOR EACH ROW EXECUTE PROCEDURE fill_in_stuff_seq();

-- XXX Add DDLs here.

COMMIT;
