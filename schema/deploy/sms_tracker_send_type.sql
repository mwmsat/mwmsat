-- Deploy sms_tracker_send_type

BEGIN;

ALTER TABLE sms_tracker DROP COLUMN send_type;
ALTER TABLE sms_tracker ADD COLUMN direction TEXT CHECK (direction IN ('to-tracker', 'from-tracker')) DEFAULT 'from-tracker';

COMMIT;
