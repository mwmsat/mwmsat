-- Deploy trackware:business_user_tracker to pg

BEGIN;

CREATE TABLE business_user_tracker (
  user_id INTEGER REFERENCES "user"(id) NOT NULL,
  tracker_id INTEGER REFERENCES tracker(id) NOT NULL,
  create_ts TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  UNIQUE(user_id, tracker_id)
); 

COMMIT;
