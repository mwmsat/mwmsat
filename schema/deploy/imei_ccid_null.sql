-- Deploy trackware:imei_ccid_null to pg

BEGIN;

ALTER TABLE tracker ALTER COLUMN imei DROP NOT NULL;
ALTER TABLE tracker ALTER COLUMN iccid DROP NOT NULL;

COMMIT;
