-- Deploy trackware:tracker-position-remove-rf to pg

BEGIN;

ALTER TABLE tracker_position DROP COLUMN rf;

COMMIT;
