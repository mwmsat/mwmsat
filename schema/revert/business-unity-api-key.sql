-- Revert trackware:business-unity-api-key from pg

BEGIN;

ALTER TABLE business_unity DROP COLUMN api_key;

COMMIT;
